REM Run this batch file from source root.
REM Make sure you run yarn first, and has minikube (kubernetes) running
cd src\backend
go build -ldflags "-w -s" -v -o .\dist\backend.exe
.\dist\backend.exe \backend --kubeconfig=%userprofile%\.kube\config --insecure-port=9091 --enable-anonymous=true
