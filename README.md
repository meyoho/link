# Link, Alauda Kubernetes Engine (AKE) Dashboard

![link](link.jpg)


> 此项目由 [alauda-ui-starter](https://bitbucket.org/mathildetech/alauda-ui-starter) fork而来。

## Link项目简介
Link 项目后端最初由[Kubernetes的Dashboard项目](https://github.com/kubernetes/dashboard)改动而来。
移植大量代码逻辑的同时，前后端代码架构上也从K8S Dashboard中引入了很多相同的设计思路和原则。

### Link 项目是什么？
- 一个以Golang作为后端语言，Angular作为前端框架的全栈应用。
- 最初设计为单个Kubernetes使用的UI界面（类似于运行在浏览器内的kubectl），并考虑到便携性（Serverless, 减少状态管理、保证多平台可用），能比较方便的进行开发、测试、CICD。
- Golang后端应只为Link的前端页面使用，做为UI的中间层去调用Kubernetes API，服务UI页面
- 前端应尽量减少API请求个数。单个页面的请求应由后端进行多个结果的拼接，以更高效的利用Golang的并发性能、减少因多次请求产生的数据延迟。
- 前端应减少逻辑复杂度，并且考虑到页面的高度相似性，应尽量提升代码的复用性。

### Link 项目不是什么？
- Golang后端作为独立的API网关为其他应用使用

## Getting Started
为了减少额外的配置项，Link对于前端、后端、自动化测试的需求都是一致的，每个角色在开始项目的时候都应准备相似的开发环境。

### 依赖准备（以MacOS为例）
* Golang 1.8+: 
* Node.js 8+
* Yarn 1.3+
* 为了保证运行Link，你还需要在本机准备一个Kubernetes环境。你有两个选择：
   * minikube: 可参考 https://kubernetes.io/docs/getting-started-guides/minikube/
   * Docker for Mac (注意要选择Edge版本): https://store.docker.com/editions/community/docker-ce-desktop-mac
   * 注：由于K8S集群需要在启动时在gcr.io拉取Docker镜像，两种环境都可能需要挂代理才能让K8S集群在本地跑起来。


> 注意，由于 Go 语言对于项目管理的机制，**本地开发时请将 alauda-ui-starter 放到$GOPATH/src 下，默认为${HOME}/go/src**
>
> 具体原因可参考[这里](https://groups.google.com/forum/#!topic/golang-nuts/6orXabMNivE)

### 配置本地开发环境

请在开发 frontend/e2e/backend 前，首先执行`yarn`。
安装之后，在两个命令行窗口分别执行 `yarn start:backend` 和 `yarn start:frontend`

> 注：yarn命令实际上会执行列在 `package.json` 文件的 `scripts` 里面的脚本命令。以上命令分别执行了后端无auth程序和本地前端开发模式。

### 安装 Metrics Server
Workload相关的路由需要 Metrics Server 展示资源使用状况. 安装方法如下:
https://github.com/kubernetes-incubator/metrics-server/tree/master/deploy/1.8%2B


```
kubectl apply -f https://raw.githubusercontent.com/kubernetes-incubator/metrics-server/master/deploy/1.8%2B/auth-delegator.yaml 
kubectl apply -f https://raw.githubusercontent.com/kubernetes-incubator/metrics-server/master/deploy/1.8%2B/auth-reader.yaml 
kubectl apply -f https://raw.githubusercontent.com/kubernetes-incubator/metrics-server/master/deploy/1.8%2B/metrics-apiservice.yaml 
kubectl apply -f https://raw.githubusercontent.com/kubernetes-incubator/metrics-server/master/deploy/1.8%2B/metrics-server-deployment.yaml 
kubectl apply -f https://raw.githubusercontent.com/kubernetes-incubator/metrics-server/master/deploy/1.8%2B/metrics-server-service.yaml 
kubectl apply -f https://raw.githubusercontent.com/kubernetes-incubator/metrics-server/master/deploy/1.8%2B/resource-reader.yaml
```

### 使用远程 Kubernetes 集群
除了使用本地的 Kubernetes 集群进行开发以外, 你还可以改动本地的 `~/.kube/config` 里的 context, 接入远程 Kubernetes 集群 API.
为了接入集群, 你需要准备如下信息:

- **k8s-api**: 例如 `https://140.143.182.24:6443` (这个链接是int环境)
- **remote-k8s-name**, **remote-k8s-name-user**:: 自己起一个好听的名字
- **remote-k8s-token**: 准备好一个可用的高权限 Service Account Token.

```yaml
apiVersion: v1
clusters:
- cluster:
    insecure-skip-tls-verify: true
    server: <k8s-api>
  name: <remote-k8s-name>
contexts:
- context:
    cluster: <remote-k8s-name>
    user: <remote-k8s-name-user>
  name: <remote-k8s-name>
current-context: <remote-k8s-name>
kind: Config
users:
- name: <remote-k8s-name-user>
  user:
    token: <remote-k8s-token>
```

#### 本地执行e2e
在本地环境执行e2e实际上是以本地的Link版本运行本机Protractor程序。开始运行前，需要保证前端程序跑起来，之后再执行如下命令：

```
# 安装ChromeDriver
yarn e2e:update-webdriver-alternative

# 运行E2E测试
yarn e2e:test-only
```

#### 以Headless模式执行e2e（一般用于无桌面环境的 CI 环境）

```
yarn e2e:integration
```

## 开始开发

### 进度追踪
项目的进度由 [https://bitbucket.org/mathildetech/link/addon/trello/trello-board](Trello面板) 追踪，同时走松散的Kanban管理模式。

### 需求管理
项目的需求文档可以从 http://confluence.alaudatech.com/display/DEV/Alauda+Kubernetes+UI 这里找到

### 提交代码规范
代码提交的commit message应遵循一定的规范。message的基本信息格式为：

```
<类型>: <主题>
```
例如: `feat: add node page routings`
#### 类型

必须为以下的一种

* **feat**: 新特性
* **fix**: bug修复
* **docs**: 只包含文档的修改
* **style**: 不影响代码含义的更改 (例如: 空格, 代码格式, 补充缺失的分号等)
* **refactor**: 重构性代码
* **perf**: 提升性能的代码修改
* **test**: 增加新的或是修复测试用例
* **build**: 影响构建系统、持续集成配置或是更新外部依赖的更新
* **chore**: 不影响源码或是测试的维护性更新

#### 主题

主题包含对变更的简要描述：

* 使用命令式、现在时：使用 "change"，不要使用 "changed" 或是 "changes"
* 不要大写第一个字母
* 不要以点 `.` 作为结尾

### 提交合并请求
由于创建新的Pull Request或是更新PR都会会启动新的流水线任务，所以你需要保证代码质量足够之后再提交。原则上，代码提交后应保证获得一个非机器人的Approval后，并且e2e通过后才能合并PR。
> 注：Jenkins 流水线地址 http://jenkins-alaudak8s.myalauda.cn/

## 根目录简介
- e2e: E2E相关文件夹
- src/backend: Golang后端代码
- src/frontend: Angular前端代码

## Q & A

### 为什么目录结构嵌套层级很深？
由于业务的相似性，组件的文件名规范不再要求完整定位一个组件，而是利用其所在的目录层级确定它的身份。
比如部署组件 `DeploymentForm` 的目录结构是 `deployment/form/component.ts`。 

这样带来的好处主要是，方便用户更容易的进行复制粘贴和重构，同时也能减少长文件名的维护性。
