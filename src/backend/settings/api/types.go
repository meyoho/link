// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"github.com/json-iterator/go"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	//authApi "link/src/backend/auth/api"
)

const (
	// SettingsConfigMapName contains a name of config map, that stores settings.
	SettingsConfigMapName = "global-configmap"

	// SettingsConfigMapNamespace contains a namespace of config map, that stores settings.
	SettingsConfigMapNamespace = "alauda-system"

	// ConfigMapKindName is a name of config map kind.
	ConfigMapKindName = "ConfigMap"

	// ConfigMapAPIVersion is a API version of config map.
	ConfigMapAPIVersion = "v1"

	// GlobalSettingsKey is a settings map key which maps to current global settings.
	GlobalSettingsKey = "_global"

	// ConcurrentSettingsChangeError occurs during settings save if settings were modified concurrently.
	// Keep it in sync with CONCURRENT_CHANGE_ERROR constant from the frontend.
	ConcurrentSettingsChangeError = "settings changed since last reload"
)

// SettingsManager is used for user settings management.
type SettingsManager interface {
	// GetGlobalSettings gets current global settings from config map.
	GetGlobalSettings(client kubernetes.Interface) (s *Settings)
	// SaveGlobalSettings saves provided global settings in config map.
	SaveGlobalSettings(client kubernetes.Interface, s *Settings) error

	// GetAuthSettings gets current global settings from config map.
	GetAuthSettings(client kubernetes.Interface) (s *AuthSettings)
	// SaveAuthSettings saves provided global settings in config map.
	SaveAuthSettings(client kubernetes.Interface, s *AuthSettings) error
}

// Settings is a single instance of settings without context.
type Settings struct {
	//ClusterName             string `json:"clusterName"`
	//ItemsPerPage            int    `json:"itemsPerPage"`
	//AutoRefreshTimeInterval int    `json:"autoRefreshTimeInterval"`
	DexHost          string `json:"dex_host"`
	DashboardHost    string `json:"dashboard_host"`
	DevopsHost       string `json:"devops_host"`
	AuthAppState     string `json:"auth_app_state"`
	AuthClientID     string `json:"auth_client_id"`
	AuthClientSecret string `json:"auth_client_secret"`
}

// Marshal settings into Yaml object.
func (s Settings) Marshal() map[string]string {
	bytes, _ := jsoniter.ConfigCompatibleWithStandardLibrary.Marshal(&s)
	sm := make(map[string]string)
	_ = jsoniter.ConfigCompatibleWithStandardLibrary.Unmarshal(bytes, &sm)
	return sm
}

// Unmarshal settings from map into object.
func Unmarshal(data map[string]string) (*Settings, error) {
	dataByte, err := jsoniter.ConfigCompatibleWithStandardLibrary.Marshal(&data)
	if err != nil {
		return &defaultSettings, err
	}
	err = jsoniter.ConfigCompatibleWithStandardLibrary.Unmarshal(dataByte, &defaultSettings)
	return &defaultSettings, err
}

// defaultSettings contains default values for every setting.
var defaultSettings = Settings{
	//ClusterName:             "",
	//ItemsPerPage:            10,
	//AutoRefreshTimeInterval: 5,
	DexHost:          "localhost:32000",
	DashboardHost:    "localhost:4200",
	DevopsHost:       "localhost:4200",
	AuthAppState:     "alauda-auth",
	AuthClientID:     "alauda-auth",
	AuthClientSecret: "ZXhhbXBsZS1hcHAtc2VjcmV0",
}

// GetDefaultSettings returns settings structure, that should be used if there are no
// global or local settings overriding them. It should not change during runtime.
func GetDefaultSettings() Settings {
	return defaultSettings
}

// GetDefaultSettingsConfigMap returns config map with default settings.
func GetDefaultSettingsConfigMap() *corev1.ConfigMap {
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      SettingsConfigMapName,
			Namespace: SettingsConfigMapNamespace,
		},
		TypeMeta: metav1.TypeMeta{
			Kind:       ConfigMapKindName,
			APIVersion: ConfigMapAPIVersion,
		},
		Data: GetDefaultSettings().Marshal(),
	}
}
