.PHONY: init build-base build unitest test deploy
PROJ?=link
PACKAGES = $(shell go list ./... | grep -v vendor )
PWD=$(shell pwd)

init:
	sed -i '' -e "s|alauda-ui-starter|${PROJ}|g" `grep -rl alauda-ui-starter * | grep -v Makefile | grep -v README`

build-base:
	cd images; docker build -t gobuild:1.9.4-alpine -f Dockerfile.base .

build:
	CGO_ENABLED=0 GOOS=linux go build -ldflags "-w -s" -v -o ${PWD}/dist/backend
	upx dist/backend

build-dev:
	CGO_ENABLED=0 go build -v -o ${PWD}/dist/backend

test:
	go test -v `go list ./... | grep -v /vendor/`

build-image: build
	cd dist; cp ../images/Dockerfile . ; docker build -t link-backend .
	rm dist/Dockerfile

run: build-image
	docker run --name alauda-ui-backend --rm -p 9091 -v ${HOME}/.kube/config:/config  backend /backend --kubeconfig=/config --port=9091

run-dev: build-dev
	cp -r swagger-ui dist/
	./dist/backend /backend --kubeconfig=${HOME}/.kube/config --insecure-port=9091 -stderrthreshold=ERROR

run-dev-no-auth:
	cp -r swagger-ui dist/
	./dist/backend /backend --kubeconfig=${HOME}/.kube/config --insecure-port=9091 --enable-anonymous=true

setup-kubectl:
	kubectl config use-context docker-for-desktop

clean:
	rm -rf .tmp
	rm -rf dist/*

deploy:
	kubectl apply -f deploy/deployment.yaml
	kubectl apply -f deploy/service.yaml
	kubectl apply -f deploy/ingress.yaml

update: build-image
	kubectl delete pod -n kube-system -lapp=link-backend
	sleep 5
	kubectl logs -f deployment/link-backend -n kube-system

test-result:
	go test -cover -v $(PACKAGES) -json > test.json

cover-result:
	echo "mode: count" > coverage-all.out
	@$(foreach pkg,$(PACKAGES),\
		go test -v -coverprofile=coverage.out -covermode=count $(pkg);\
		if [ -f coverage.out ]; then\
			tail -n +2 coverage.out >> coverage-all.out;\
		fi;)
