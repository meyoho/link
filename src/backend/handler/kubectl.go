package handler

import (
	"github.com/json-iterator/go"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/tools/clientcmd"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
	"os"
	"os/exec"
)

const (
	tmpDir = "./alauda/tmp"
)

func Apply(payloads []unstructured.Unstructured, kubeConfig *clientcmdapi.Config) ([]byte, error) {
	kubeConfigFile, err := tempFile("kubeconfig-")
	if err != nil {
		return nil, err
	}
	defer os.Remove(kubeConfigFile.Name())

	yamlFile, err := tempFile("yaml-")
	if err != nil {
		return nil, err
	}
	defer os.Remove(yamlFile.Name())

	fileContent := []byte{}
	for _, p := range payloads {
		raw, _ := jsoniter.ConfigCompatibleWithStandardLibrary.Marshal(p.Object)
		fileContent = append(fileContent, raw...)
	}

	if err := ioutil.WriteFile(yamlFile.Name(), fileContent, 0600); err != nil {
		return nil, err
	}

	if err := clientcmd.WriteToFile(*kubeConfig, kubeConfigFile.Name()); err != nil {
		return nil, err
	}

	cmd := exec.Command("kubectl",
		"--kubeconfig",
		kubeConfigFile.Name(),
		"apply",
		"-f",
		yamlFile.Name())
	return cmd.CombinedOutput()
}

func tempFile(prefix string) (*os.File, error) {
	if _, err := os.Stat(tmpDir); os.IsNotExist(err) {
		if err = os.MkdirAll(tmpDir, 0755); err != nil {
			return nil, err
		}
	}

	f, err := ioutil.TempFile(tmpDir, prefix)
	if err != nil {
		return nil, err
	}

	return f, f.Close()
}

type kubectlApplyResponse struct {
	Message string `json:"message"`
}
