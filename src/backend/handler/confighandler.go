// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package handler

import (
	"github.com/json-iterator/go"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	clientapi "link/src/backend/client/api"
	"net/http"
	"text/template"
	"time"
)

// AppConfigHandler manages all endpoints related to  management.
type AppConfigHandler struct {
	manager clientapi.ClientManager
}

// AppConfig is a global configuration of application.
type AppConfig struct {
	// ServerTime is current server time.
	ServerTime int64 `json:"serverTime"`

	EnableAnounymous    bool `json:"enableAnounymous"`
	EnableMetricsServer bool `json:"enableMetricsServer"`
}

const (
	// ConfigTemplateName is a name of config template
	ConfigTemplateName = "appConfig"
	// ConfigTemplate is a template of a config
	ConfigTemplate = "{{.}}"
)

// ServeHTTP serves HTTP endpoint with application configuration.
func (handler AppConfigHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if _, err := handler.configHandler(w, r); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError)
	}
}

func getAppConfigJSON(client clientapi.ClientManager) string {
	config := &AppConfig{
		ServerTime:          time.Now().UTC().UnixNano() / 1e6,
		EnableAnounymous:    client.IsAnounymousEnabled(),
		EnableMetricsServer: isMetricsServerEnabled(client),
	}

	jsonConfig, _ := jsoniter.ConfigCompatibleWithStandardLibrary.Marshal(config)
	return string(jsonConfig)
}

func (appConfigHandler AppConfigHandler) configHandler(w http.ResponseWriter, r *http.Request) (int, error) {
	configTemplate, err := template.New(ConfigTemplateName).Parse(ConfigTemplate)
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		return http.StatusInternalServerError, err
	}
	return http.StatusOK, configTemplate.Execute(w, getAppConfigJSON(appConfigHandler.manager))
}

func isMetricsServerEnabled(client clientapi.ClientManager) bool {
	c, err := client.Client(nil)
	if err != nil {
		return false
	}
	ss, err := c.CoreV1().Services(v1.NamespaceAll).List(v1.ListOptions{})
	if err != nil {
		return false
	}
	for _, s := range ss.Items {
		if s.Name == "metrics-server" {
			return true
		}
	}
	return false
}

// NewAppConfigHandler creates AppConfigHandler.
func NewAppConfigHandler(manager clientapi.ClientManager) AppConfigHandler {
	return AppConfigHandler{manager: manager}
}
