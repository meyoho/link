// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package handler

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
	"link/src/backend/args"
	"link/src/backend/resource/serviceaccount"
	"net/http"
	"strconv"
	"strings"

	"fmt"
	"link/src/backend/auth"
	authApi "link/src/backend/auth/api"
	clientapi "link/src/backend/client/api"
	kdErrors "link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/configmap"
	"link/src/backend/resource/container"
	"link/src/backend/resource/controller"
	"link/src/backend/resource/cronjob"
	"link/src/backend/resource/daemonset"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/deployment"
	"link/src/backend/resource/ingress"
	"link/src/backend/resource/job"
	"link/src/backend/resource/logs"
	ns "link/src/backend/resource/namespace"
	"link/src/backend/resource/networkpolicy"
	"link/src/backend/resource/node"
	"link/src/backend/resource/other"
	"link/src/backend/resource/overview"
	"link/src/backend/resource/persistentvolume"
	"link/src/backend/resource/persistentvolumeclaim"
	"link/src/backend/resource/pod"
	"link/src/backend/resource/rbacrolebindings"
	"link/src/backend/resource/rbacroles"
	"link/src/backend/resource/replicaset"
	"link/src/backend/resource/secret"
	resourceService "link/src/backend/resource/service"
	"link/src/backend/resource/statefulset"
	"link/src/backend/resource/storageclass"
	"link/src/backend/settings"
	"link/src/backend/validation"

	thandler "bitbucket.org/mathildetech/themex/handler"
	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/tools/remotecommand"
)

const (
	// RequestLogString is a template for request log message.
	RequestLogString = "[%s] Incoming %s %s %s request from %s"

	// ResponseLogString is a template for response log message.
	ResponseLogString = "[%s] Outcoming response to %s %s %s with %d status code in %vms"
)

// APIHandler is a representation of API handler. Structure contains clientapi configuration.
type APIHandler struct {
	cManager clientapi.ClientManager
	sManager settings.SettingsManager
}

// TerminalResponse is sent by handleExecShell. The Id is a random session id that binds the original REST request and the SockJS connection.
// Any clientapi in possession of this Id can hijack the terminal session.
type TerminalResponse struct {
	Id string `json:"id"`
}

// CreateHTTPAPIHandler creates a new HTTP handler that handles all requests to the API of the backend.
func CreateHTTPAPIHandler(cManager clientapi.ClientManager,
	authManager authApi.Manager) (

	http.Handler, error) {
	apiHandler := APIHandler{cManager: cManager}
	wsContainer := restful.NewContainer()
	wsContainer.EnableContentEncoding(true)

	apiV1Ws := new(restful.WebService)

	InstallFilters(apiV1Ws, cManager)

	apiV1Ws.Path("/api/v1").
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)
	wsContainer.Add(apiV1Ws)

	oidcAppState := args.Holder.GetAuthAppState()
	authHandler := auth.NewAuthHandler(authManager, oidcAppState)

	authHandler.Install(apiV1Ws)

	configurationHandler := thandler.NewAPIHandler("configuration")
	configurationHandler.Install(apiV1Ws)

	apiV1Ws.Route(
		apiV1Ws.GET("/replicasets").
			To(apiHandler.handleGetReplicaSets).
			Writes(replicaset.ReplicaSetList{}).
			Doc("return all replicasets in all namespaces").
			Returns(200, "OK", replicaset.ReplicaSetList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/replicasets/{namespace}").
			To(apiHandler.handleGetReplicaSets).
			Writes(replicaset.ReplicaSetList{}).
			Doc("return all replicaset in a specific namespace").
			Param(restful.PathParameter("namespace", "namespace")).
			Returns(200, "OK", replicaset.ReplicaSetList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/replicasets/{namespace}/{replicaSet}").
			To(apiHandler.handleGetReplicaSetDetail).
			Writes(replicaset.ReplicaSetDetail{}).
			Doc("return a replicaset detail").
			Param(restful.PathParameter("namespace", "namespace")).
			Param(restful.PathParameter("replicaSet", "replicaSet name")).
			Returns(200, "OK", replicaset.ReplicaSetDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/replicasets/{namespace}/{replicaSet}/pod").
			To(apiHandler.handleGetReplicaSetPods).
			Writes(pod.PodList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/pods").
			To(apiHandler.handleGetPods).
			Writes(pod.PodList{}).
			Doc("return all pods in all namespaces").
			Returns(200, "OK", pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/pods/{namespace}").
			To(apiHandler.handleGetPods).
			Writes(pod.PodList{}).
			Doc("return all pods in a specific namespace").
			Param(restful.PathParameter("namespace", "namespace")).
			Returns(200, "OK", pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/pods/{namespace}/{pod}").
			To(apiHandler.handleGetPodDetail).
			Writes(pod.PodDetail{}).
			Doc("return a pod detail").
			Param(restful.PathParameter("namespace", "namespace")).
			Param(restful.PathParameter("pod", "pod name")).
			Returns(200, "OK", pod.PodDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/pods/{namespace}/{pod}/shell/{container}").
			To(apiHandler.handleExecShell).
			Writes(TerminalResponse{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/kubectl/shell").
			To(apiHandler.handleKubectlShell).
			Writes(TerminalResponse{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/kubectl/apply").
			To(apiHandler.handleKubectlApply).
			Reads([]unstructured.Unstructured{}).
			Consumes(restful.MIME_JSON))
	apiV1Ws.Route(
		apiV1Ws.GET("/deployments").
			To(apiHandler.handleGetDeployments).
			Writes(deployment.DeploymentList{}).
			Doc("return all deployments in all namespaces").
			Returns(200, "OK", deployment.DeploymentList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/deployments/{namespace}").
			To(apiHandler.handleGetDeployments).
			Writes(deployment.DeploymentList{}).
			Doc("return all deployments in a specific namespace").
			Param(restful.PathParameter("namespace", "namespace")).
			Returns(200, "OK", deployment.DeploymentList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/deployments/{namespace}/{deployment}").
			To(apiHandler.handleGetDeploymentDetail).
			Writes(deployment.DeploymentDetail{}).
			Doc("return a deployment detail").
			Param(restful.PathParameter("namespace", "namespace")).
			Param(restful.PathParameter("deployment", "deployment name")).
			Returns(200, "OK", deployment.DeploymentDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/deployments/{namespace}/{deployment}/pod").
			To(apiHandler.handleGetDeploymentPods).
			Writes(pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/deployments/{namespace}/{deployment}/rollback/{revision}").
			To(apiHandler.handleRollbackDeployment))
	apiV1Ws.Route(
		apiV1Ws.GET("/daemonsets").
			To(apiHandler.handleGetDaemonSetList).
			Writes(daemonset.DaemonSetList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/daemonsets/{namespace}").
			To(apiHandler.handleGetDaemonSetList).
			Writes(daemonset.DaemonSetList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/daemonsets/{namespace}/{daemonSet}").
			To(apiHandler.handleGetDaemonSetDetail).
			Writes(daemonset.DaemonSetDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/daemonsets/{namespace}/{daemonSet}/pod").
			To(apiHandler.handleGetDaemonSetPods).
			Writes(pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/jobs").
			To(apiHandler.handleGetJobList).
			Writes(job.JobList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/jobs/{namespace}").
			To(apiHandler.handleGetJobList).
			Writes(job.JobList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/jobs/{namespace}/{name}").
			To(apiHandler.handleGetJobDetail).
			Writes(job.JobDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/jobs/{namespace}/{name}/pod").
			To(apiHandler.handleGetJobPods).
			Writes(pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/cronjobs").
			To(apiHandler.handleGetCronJobList).
			Writes(cronjob.CronJobList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/cronjobs/{namespace}").
			To(apiHandler.handleGetCronJobList).
			Writes(cronjob.CronJobList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/cronjobs/{namespace}/{name}").
			To(apiHandler.handleGetCronJobDetail).
			Writes(cronjob.CronJobDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/cronjobs/{namespace}/{name}/job").
			To(apiHandler.handleGetCronJobJobs).
			Writes(job.JobList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/cronjobs/{namespace}/{name}/completedjob").
			To(apiHandler.handleGetCronJobCompletedJobs).
			Writes(job.JobList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/namespaces").
			To(apiHandler.handleGetNamespaces).
			Writes(ns.NamespaceList{}).
			Doc("get namespaces list").
			Returns(200, "OK", ns.NamespaceList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/permission_namespaces").
			To(apiHandler.handleGetPermissionNamespaces).
			Returns(200, "OK", []string{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/namespaces/{name}").
			To(apiHandler.handleGetNamespaceDetail).
			Writes(ns.NamespaceDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/secrets").
			To(apiHandler.handleGetSecretList).
			Writes(secret.SecretList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/secrets/{namespace}").
			To(apiHandler.handleGetSecretList).
			Writes(secret.SecretList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/secrets/{namespace}/{name}").
			To(apiHandler.handleGetSecretDetail).
			Writes(secret.SecretDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/configmaps").
			To(apiHandler.handleGetConfigMapList).
			Writes(configmap.ConfigMapList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/configmaps/{namespace}").
			To(apiHandler.handleGetConfigMapList).
			Writes(configmap.ConfigMapList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/configmaps/{namespace}/{configmap}").
			To(apiHandler.handleGetConfigMapDetail).
			Writes(configmap.ConfigMapDetail{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/services").
			To(apiHandler.handleGetServiceList).
			Writes(common.ServiceList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/services/{namespace}").
			To(apiHandler.handleGetServiceList).
			Writes(common.ServiceList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/services/{namespace}/{service}").
			To(apiHandler.handleGetServiceDetail).
			Writes(resourceService.ServiceDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/services/{namespace}/{service}/pod").
			To(apiHandler.handleGetServicePods).
			Writes(pod.PodList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/ingresses").
			To(apiHandler.handleGetIngressList).
			Writes(ingress.IngressList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/ingresses/{namespace}").
			To(apiHandler.handleGetIngressList).
			Writes(ingress.IngressList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/ingresses/{namespace}/{name}").
			To(apiHandler.handleGetIngressDetail).
			Writes(ingress.IngressDetail{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/networkpolicies").
			To(apiHandler.handleGetNetworkPolicyList).
			Writes(networkpolicy.NetworkPolicyList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/networkpolicies/{namespace}").
			To(apiHandler.handleGetNetworkPolicyList).
			Writes(networkpolicy.NetworkPolicyList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/networkpolicies/{namespace}/{name}").
			To(apiHandler.handleGetNetworkPolicyDetail).
			Writes(networkpolicy.Detail{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/statefulsets").
			To(apiHandler.handleGetStatefulSetList).
			Writes(statefulset.StatefulSetList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/statefulsets/{namespace}").
			To(apiHandler.handleGetStatefulSetList).
			Writes(statefulset.StatefulSetList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/statefulsets/{namespace}/{statefulset}").
			To(apiHandler.handleGetStatefulSetDetail).
			Writes(statefulset.StatefulSetDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/statefulsets/{namespace}/{statefulset}/pod").
			To(apiHandler.handleGetStatefulSetPods).
			Writes(pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/nodes").
			To(apiHandler.handleGetNodeList).
			Writes(node.NodeList{}))
	apiV1Ws.Route(
		apiV1Ws.POST("/nodes").
			To(apiHandler.handleAddNodes).
			Writes(node.AddResponse{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/nodes/{name}").
			To(apiHandler.handleGetNodeDetail).
			Writes(node.NodeDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/nodes/{name}/pod").
			To(apiHandler.handleGetNodePods).
			Writes(pod.PodList{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/nodes/{name}/{action}").
			To(apiHandler.handleManageNodeSchedule))
	apiV1Ws.Route(
		apiV1Ws.GET("/serviceaccounts/").
			To(apiHandler.handleGetServiceAccountList).
			Writes(serviceaccount.ServiceAccountList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/serviceaccounts/{namespace}").
			To(apiHandler.handleGetServiceAccountList).
			Writes(serviceaccount.ServiceAccountList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/persistentvolumes").
			To(apiHandler.handleGetPersistentVolumeList).
			Writes(persistentvolume.PersistentVolumeList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/persistentvolumes/{persistentvolume}").
			To(apiHandler.handleGetPersistentVolumeDetail).
			Writes(persistentvolume.PersistentVolumeDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/persistentvolumes/namespace/{namespace}/name/{persistentvolume}").
			To(apiHandler.handleGetPersistentVolumeDetail).
			Writes(persistentvolume.PersistentVolumeDetail{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/persistentvolumeclaims/").
			To(apiHandler.handleGetPersistentVolumeClaimList).
			Writes(persistentvolumeclaim.PersistentVolumeClaimList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/persistentvolumeclaims/{namespace}").
			To(apiHandler.handleGetPersistentVolumeClaimList).
			Writes(persistentvolumeclaim.PersistentVolumeClaimList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/persistentvolumeclaims/{namespace}/{name}").
			To(apiHandler.handleGetPersistentVolumeClaimDetail).
			Writes(persistentvolumeclaim.PersistentVolumeClaimDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/persistentvolumeclaims/{namespace}/{name}/pod").
			To(apiHandler.handleGetPersistentVolumeClaimPod).
			Writes(pod.PodList{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/storageclasses").
			To(apiHandler.handleGetStorageClassList).
			Writes(storageclass.StorageClassList{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/storageclasses/{storageclass}").
			To(apiHandler.handleGetStorageClass).
			Writes(storageclass.StorageClass{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/storageclass/{storageclass}/persistentvolumes").
			To(apiHandler.handleGetStorageClassPersistentVolumes).
			Writes(persistentvolume.PersistentVolumeList{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/storageclasses/{storageclass}/default/{action}").
			To(apiHandler.handleStorageClassDefault))
	apiV1Ws.Route(
		apiV1Ws.GET("/log/source/{namespace}/{resourceType}/{resourceName}").
			To(apiHandler.handleLogSource).
			Writes(controller.LogSources{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/log/{namespace}/{pod}/{container}").
			To(apiHandler.handleLogs).
			Writes(logs.LogDetails{}).
			Doc("get logs of a container in a pod, detail usage see: http://confluence.alaudatech.com/pages/viewpage.action?pageId=24222821").
			Param(restful.PathParameter("namespace", "namespace pod belong to")).
			Param(restful.PathParameter("pod", "pod name")).
			Param(restful.PathParameter("container", "container name")).
			Param(restful.QueryParameter("referenceTimestamp", "locate log entry context by timestamp. Use oldest and newest to refer the first and last log entry")).
			Param(restful.QueryParameter("offsetFrom", "first index of log entry relative to reference log entry.(this one will be include)")).
			Param(restful.QueryParameter("offsetTo", "Last index of log entry relatively to the reference log entry.(this one will NOT be included)")).
			Returns(200, "OK", logs.LogDetails{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/log/file/{namespace}/{pod}/{container}").
			To(apiHandler.handleLogFile).
			Writes(logs.LogDetails{}).
			Doc("download log files").
			Returns(200, "OK", logs.LogDetails{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/overview/").
			To(apiHandler.handleOverview).
			Writes(overview.Overview{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/others").
			To(apiHandler.handleOtherResourcesList).
			Writes(other.ResourceList{}).
			Doc("get all resources").
			Param(restful.QueryParameter("filterBy", "filter option separated by comma. For example parameter1,value1,parameter2,value2 - means that the data should be filtered by parameter1 equals value1 and parameter2 equals value2").
				DataType("string").
				AllowableValues(map[string]string{
					"name":      "search by name partial match",
					"namespace": "filter by namespace",
					"kind":      "filter by kind",
					"scope":     "allowed value `namespaced` and `clustered` filter by if a resource is namespaced",
				})).
			Param(restful.QueryParameter("sortBy", "sort option separated by comma. For example a,parameter1,d,parameter2 - means that the data should be sorted by parameter1 (ascending) and later sort by parameter2 (descending)").
				DataType("string").
				AllowableValues(map[string]string{
					"name":              "",
					"namespace":         "",
					"kind":              "",
					"creationTimestamp": "",
				})).
			Param(restful.QueryParameter("itemsPerPage", "items per page").
				DataType("integer")).
			Param(restful.QueryParameter("page", "page number").DataType("integer")).
			Returns(200, "OK", other.ResourceList{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/others").
			To(apiHandler.handleOtherResourceCreate).
			Doc("create a resource").
			Reads([]unstructured.Unstructured{}).
			Consumes(restful.MIME_JSON).
			Returns(200, "OK", CreateResponse{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/others/{group}/{version}/{kind}/{namespace}/{name}").
			To(apiHandler.handleOtherResourceDetail).
			Writes(other.OtherResourceDetail{}).
			Doc("get a resource detail with events").
			Returns(200, "OK", other.OtherResourceDetail{}))

	apiV1Ws.Route(
		apiV1Ws.DELETE("/others/{group}/{version}/{kind}/{namespace}/{name}").
			To(apiHandler.handleOtherResourceDetail).
			Doc("delete a resource"))

	apiV1Ws.Route(
		apiV1Ws.PUT("/others/{group}/{version}/{kind}/{namespace}/{name}").
			To(apiHandler.handleOtherResourceDetail).
			Doc("update a resource with whole resource json").
			Reads(unstructured.Unstructured{}).
			Consumes(restful.MIME_JSON))

	apiV1Ws.Route(
		apiV1Ws.PATCH("/others/{group}/{version}/{kind}/{namespace}/{name}/{field}").
			To(apiHandler.handleOtherResourcePatch).
			Doc("update resource annotations or labels").
			Reads(other.FieldPayload{}).
			Consumes(restful.MIME_JSON))

	// list roles
	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/roles").
			To(apiHandler.handleGetRbacRoleList).
			Writes(rbacroles.RbacRoleList{}).
			Param(restful.QueryParameter("filterBy", "filter option separated by comma. For example parameter1,value1,parameter2,value2 - means that the data should be filtered by parameter1 equals value1 and parameter2 equals value2").
				DataType("string").
				AllowableValues(map[string]string{
					"name":      "search by name partial match",
					"namespace": "filter by namespace",
					"kind":      "filter by kind",
				})).
			Param(restful.QueryParameter("sortBy", "sort option separated by comma. For example a,parameter1,d,parameter2 - means that the data should be sorted by parameter1 (ascending) and later sort by parameter2 (descending)").
				DataType("string").
				AllowableValues(map[string]string{
					"name":              "",
					"namespace":         "",
					"creationTimestamp": "",
				})).
			Param(restful.QueryParameter("itemsPerPage", "items per page").DataType("integer")).
			Param(restful.QueryParameter("page", "page number").DataType("integer")).
			Returns(200, "OK", rbacroles.RbacRoleList{}),
	)

	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/rolebindings").
			To(apiHandler.handleGetRbacRoleBindingList).
			Writes(rbacrolebindings.RbacRoleBindingList{}).
			Param(restful.QueryParameter("filterBy", "filter option separated by comma. For example parameter1,value1,parameter2,value2 - means that the data should be filtered by parameter1 equals value1 and parameter2 equals value2").
				DataType("string").
				AllowableValues(map[string]string{
					"name":      "search by name partial match",
					"namespace": "filter by namespace",
					"kind":      "filter by kind",
				})).
			Param(restful.QueryParameter("sortBy", "sort option separated by comma. For example a,parameter1,d,parameter2 - means that the data should be sorted by parameter1 (ascending) and later sort by parameter2 (descending)").
				DataType("string").
				AllowableValues(map[string]string{
					"name":              "",
					"namespace":         "",
					"creationTimestamp": "",
				})).
			Param(restful.QueryParameter("itemsPerPage", "items per page").DataType("integer")).
			Param(restful.QueryParameter("page", "page number").DataType("integer")).
			Returns(200, "OK", rbacroles.RbacRoleList{}),
	)

	// role api
	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/status").
			To(apiHandler.handleRbacStatus).
			Writes(validation.RbacStatus{}))

	apiV1Ws.Route(
		apiV1Ws.POST("/rbac/namespaces/{namespace}/roles").
			To(apiHandler.handleCreateRbacRole).
			Writes(rbacroles.RbacRoleDetail{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/namespaces/{namespace}/roles/{name}").
			To(apiHandler.handleRbacRoleDetail).
			Writes(rbacroles.RbacRoleDetail{}))

	apiV1Ws.Route(
		apiV1Ws.PUT("/rbac/namespaces/{namespace}/roles/{name}").
			To(apiHandler.handleRbacRoleDetail).
			Writes(rbacroles.RbacRoleDetail{}))

	apiV1Ws.Route(
		apiV1Ws.DELETE("/rbac/namespaces/{namespace}/roles/{name}").
			To(apiHandler.handleRbacRoleDetail))

	// clsuterRole api
	apiV1Ws.Route(
		apiV1Ws.POST("/rbac/clusterroles").
			To(apiHandler.handleCreateRbacClusterRole).
			Writes(rbacroles.RbacRoleDetail{}))

	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/clusterroles/{name}").
			To(apiHandler.handleRbacClusterRoleDetail).
			Writes(rbacroles.RbacRoleDetail{}))

	apiV1Ws.Route(
		apiV1Ws.PUT("/rbac/clusterroles/{name}").
			To(apiHandler.handleRbacClusterRoleDetail).
			Writes(rbacroles.RbacRoleDetail{}))

	apiV1Ws.Route(
		apiV1Ws.DELETE("/rbac/clusterroles/{name}").
			To(apiHandler.handleRbacClusterRoleDetail))

	// roleBindings api
	apiV1Ws.Route(
		apiV1Ws.POST("/rbac/namespaces/{namespace}/rolebindings").
			To(apiHandler.handleCreateRbacRoleBinding).
			Writes(rbacrolebindings.RbacRoleBindingDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/namespaces/{namespace}/rolebindings/{name}").
			To(apiHandler.handleRbacRoleBindingDetail).
			Writes(rbacrolebindings.RbacRoleBindingDetail{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/rbac/namespaces/{namespace}/rolebindings/{name}").
			To(apiHandler.handleRbacRoleBindingDetail).
			Writes(rbacrolebindings.RbacRoleBindingDetail{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE("/rbac/namespaces/{namespace}/rolebindings/{name}").
			To(apiHandler.handleRbacRoleBindingDetail))

	// clusterRoleBindings api
	apiV1Ws.Route(
		apiV1Ws.POST("/rbac/clusterrolebindings").
			To(apiHandler.handleCreateRbacClusterRoleBinding).
			Writes(rbacrolebindings.RbacRoleBindingDetail{}))
	apiV1Ws.Route(
		apiV1Ws.GET("/rbac/clusterrolebindings/{name}").
			To(apiHandler.handleRbacClusterRoleBindingDetail).
			Writes(rbacrolebindings.RbacRoleBindingDetail{}))
	apiV1Ws.Route(
		apiV1Ws.PUT("/rbac/clusterrolebindings/{name}").
			To(apiHandler.handleRbacClusterRoleBindingDetail).
			Writes(rbacrolebindings.RbacRoleBindingDetail{}))
	apiV1Ws.Route(
		apiV1Ws.DELETE("/rbac/clusterrolebindings/{name}").
			To(apiHandler.handleRbacClusterRoleBindingDetail))

	apiV1Ws.Route(
		apiV1Ws.GET("/resources").
			To(apiHandler.handleGetK8SResourcesList))

	return wsContainer, nil
}

//handleGetK8SResourcesList list all k8s resources
func (apiHandler *APIHandler) handleGetK8SResourcesList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	resources, err := other.GetCanListResource(k8sClient, true)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result := struct {
		Kind      string                        `json:"kind"`
		Resources []schema.GroupVersionResource `json:"resources"`
	}{}
	result.Kind = "APIResourceList"
	for _, r := range resources {
		result.Resources = append(result.Resources, r)
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// TODO: Handle case in which RBAC feature is not enabled in API server. Currently returns 404 resource not found
func (apiHandler *APIHandler) handleGetRbacRoleList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := rbacroles.GetRbacRoleList(k8sClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// TODO: Handle case in which RBAC feature is not enabled in API server. Currently returns 404 resource not found
func (apiHandler *APIHandler) handleGetRbacRoleBindingList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := rbacrolebindings.GetRbacRoleBindingList(k8sClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleRbacStatus(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := validation.ValidateRbacStatus(k8sClient)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetStatefulSetList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := statefulset.GetStatefulSetList(k8sClient, mclient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetStatefulSetDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("statefulset")
	result, err := statefulset.GetStatefulSetDetail(k8sClient, mclient, namespace, name)

	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetStatefulSetPods(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("statefulset")
	dataSelect := parseDataSelectPathParameter(request)

	result, err := statefulset.GetStatefulSetPods(k8sClient, mclient, dataSelect, name, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetServiceList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := resourceService.GetServiceList(k8sClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetServiceDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("service")
	result, err := resourceService.GetServiceDetail(k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetIngressDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := ingress.GetIngressDetail(k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetIngressList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := ingress.GetIngressList(k8sClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetNetworkPolicyDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := networkpolicy.GetNetworkPolicyDetail(k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetNetworkPolicyList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := networkpolicy.GetNetworkPolicyList(k8sClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetServicePods(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	namespace := request.PathParameter("namespace")
	name := request.PathParameter("service")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := resourceService.GetServicePods(k8sClient, mclient, namespace, name, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetNodeList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	metricsClient, _ := apiHandler.cManager.MetricsClient(request)

	dataSelect := parseDataSelectPathParameter(request)
	result, err := node.GetNodeList(k8sClient, metricsClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleAddNodes(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	nodeConfig := node.AddNodeConfig{}
	err = request.ReadEntity(&nodeConfig)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := node.AddNodes(k8sClient, nodeConfig)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetNodeDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	metricsClient, _ := apiHandler.cManager.MetricsClient(request)

	name := request.PathParameter("name")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := node.GetNodeDetail(k8sClient, metricsClient, name, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetNodePods(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := node.GetNodePods(k8sClient, mclient, dataSelect, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleManageNodeSchedule(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	nodeName := request.PathParameter("name")
	action := request.PathParameter("action")

	switch action {
	case "cordon":
		err = node.Cordon(k8sClient, nodeName)
	case "uncordon":
		err = node.UnCordon(k8sClient, nodeName)
	case "drain":
		err = node.Drain(k8sClient, nodeName)
	case "taint":
		payload := []node.Taint{}
		err := request.ReadEntity(&payload)
		if err != nil {
			kdErrors.HandleInternalError(response, err)
			return
		}
		err = node.Taints(k8sClient, nodeName, payload)
	default:
		kdErrors.HandleInternalError(response, fmt.Errorf("unsupport node action %s", action))
		return
	}
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeader(http.StatusOK)
}

func (apiHandler *APIHandler) handleOverview(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	metricsClient, _ := apiHandler.cManager.MetricsClient(request)

	result, err := overview.GetOverview(k8sClient, metricsClient)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetReplicaSets(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := replicaset.GetReplicaSetList(k8sClient, mclient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetReplicaSetDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	replicaSet := request.PathParameter("replicaSet")
	result, err := replicaset.GetReplicaSetDetail(k8sClient, mclient, namespace, replicaSet)

	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetReplicaSetPods(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	replicaSet := request.PathParameter("replicaSet")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := replicaset.GetReplicaSetPods(k8sClient, mclient, dataSelect, replicaSet, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// Handles execute shell API call
func (apiHandler *APIHandler) handleExecShell(request *restful.Request, response *restful.Response) {
	sessionId, err := genTerminalSessionId()
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	cfg, err := apiHandler.cManager.Config(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	terminalSessions.Set(sessionId, TerminalSession{
		id:       sessionId,
		bound:    make(chan error),
		sizeChan: make(chan remotecommand.TerminalSize),
	})
	go WaitForTerminal(k8sClient, cfg, request, sessionId)
	response.WriteHeaderAndEntity(http.StatusOK, TerminalResponse{Id: sessionId})
}

func (apiHandler *APIHandler) handleKubectlApply(request *restful.Request, response *restful.Response) {
	cfg, err := apiHandler.cManager.ClientCmdConfig(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	cmdcfg, err := cfg.RawConfig()
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	payload := []unstructured.Unstructured{}
	err = request.ReadEntity(&payload)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	message, err := Apply(payload, &cmdcfg)
	rtn := kubectlApplyResponse{Message: string(message)}

	if err == nil {
		response.WriteHeaderAndEntity(http.StatusOK, rtn)
	} else {
		if rtn.Message == "" {
			rtn.Message = err.Error()
		}
		response.WriteHeaderAndEntity(http.StatusBadRequest, rtn)
	}
}

func (apiHandler *APIHandler) handleKubectlShell(request *restful.Request, response *restful.Response) {
	cfg, err := apiHandler.cManager.Config(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	token := cfg.BearerToken
	if len(token) == 0 {
		response.WriteError(http.StatusBadRequest, fmt.Errorf("token needed"))
		return
	}
	sessionId, err := genTerminalSessionId()
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	k8sClient, err := apiHandler.cManager.Client(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	cfg, err = apiHandler.cManager.Config(nil)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	terminalSessions.Set(sessionId, TerminalSession{
		id:       sessionId,
		bound:    make(chan error),
		sizeChan: make(chan remotecommand.TerminalSize),
	})

	pods, err := k8sClient.CoreV1().Pods("alauda-system").List(metav1.ListOptions{
		LabelSelector: "apps=alauda-kubectl",
	})
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	targetPod := ""
	for _, pod := range pods.Items {
		ready := false
		for _, status := range pod.Status.ContainerStatuses {
			if !status.Ready {
				ready = false
				break
			} else {
				ready = true
			}
		}

		if ready {
			targetPod = pod.Name
			break
		}
	}
	if targetPod == "" {
		response.WriteHeaderAndEntity(http.StatusNotFound, fmt.Errorf("no ready kubectl"))
		return
	}

	go WaitForKubectlTerminal(k8sClient, cfg, targetPod, token, sessionId)
	response.WriteHeaderAndEntity(http.StatusOK, TerminalResponse{Id: sessionId})
}

func (apiHandler *APIHandler) handleGetDeployments(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := deployment.GetDeploymentList(k8sClient, mclient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetDeploymentDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("deployment")
	result, err := deployment.GetDeploymentDetail(k8sClient, mclient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetDeploymentPods(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	namespace := request.PathParameter("namespace")
	name := request.PathParameter("deployment")
	dataSelect := parseDataSelectPathParameter(request)

	result, err := deployment.GetDeploymentPods(k8sClient, mclient, dataSelect, name, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleRollbackDeployment(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	namespace := request.PathParameter("namespace")
	name := request.PathParameter("deployment")
	revision, err := strconv.Atoi(request.PathParameter("revision"))
	if err != nil {
		kdErrors.HandleInternalError(response, fmt.Errorf("%s is not a valid revision", request.PathParameter("revision")))
		return
	}
	err = deployment.RollbackDeployment(k8sClient, namespace, name, int64(revision))
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeader(http.StatusCreated)
}

func (apiHandler *APIHandler) handleGetPods(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := pod.GetPodList(k8sClient, mclient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPodDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("pod")
	result, err := pod.GetPodDetail(k8sClient, mclient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetNamespaces(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := ns.GetNamespaceList(k8sClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPermissionNamespaces(request *restful.Request, response *restful.Response) {
    userClient, err := apiHandler.cManager.Client(request)
    if err != nil {
        kdErrors.HandleInternalError(response, err)
        return
    }
    admitClient, err := apiHandler.cManager.Client(nil)
    if err != nil {
        kdErrors.HandleInternalError(response, err)
        return
    }

    result, err := ns.GetNamespaceListFromPermission(userClient, admitClient)
    if err != nil {
        kdErrors.HandleInternalError(response, err)
        return
    }
    response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetNamespaceDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	result, err := ns.GetNamespaceDetail(k8sClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetSecretDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := secret.GetSecretDetail(k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetSecretList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	namespace := parseNamespacePathParameter(request)
	result, err := secret.GetSecretList(k8sClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetConfigMapList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := configmap.GetConfigMapList(k8sClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetConfigMapDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("configmap")
	result, err := configmap.GetConfigMapDetail(k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPersistentVolumeList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := persistentvolume.GetPersistentVolumeList(k8sClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPersistentVolumeDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("persistentvolume")
	result, err := persistentvolume.GetPersistentVolumeDetail(k8sClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPersistentVolumeClaimList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := persistentvolumeclaim.GetPersistentVolumeClaimList(k8sClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPersistentVolumeClaimDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := persistentvolumeclaim.GetPersistentVolumeClaimDetail(k8sClient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetPersistentVolumeClaimPod(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("name")
	namespace := request.PathParameter("namespace")
	dataSelect := parseDataSelectPathParameter(request)

	result, err := persistentvolumeclaim.GetPersistentVolumeClaimPods(k8sClient, mclient, dataSelect, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetDaemonSetList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := daemonset.GetDaemonSetList(k8sClient, mclient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetDaemonSetDetail(
	request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("daemonSet")
	result, err := daemonset.GetDaemonSetDetail(k8sClient, mclient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetDaemonSetPods(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("daemonSet")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := daemonset.GetDaemonSetPods(k8sClient, mclient, dataSelect, name, namespace)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetJobList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := job.GetJobList(k8sClient, mclient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetJobDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	result, err := job.GetJobDetail(k8sClient, mclient, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetJobPods(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := job.GetJobPods(k8sClient, mclient, dataSelect, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCronJobList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := parseNamespacePathParameter(request)
	dataSelect := parseDataSelectPathParameter(request)
	result, err := cronjob.GetCronJobList(k8sClient, namespace, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCronJobDetail(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := cronjob.GetCronJobDetail(k8sClient, dataSelect, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCronJobJobs(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := cronjob.GetCronJobJobs(k8sClient, mclient, dataSelect, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetCronJobCompletedJobs(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	mclient, err := apiHandler.cManager.MetricsClient(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := cronjob.GetCronJobCompletedJobs(k8sClient, mclient, dataSelect, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetStorageClassList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	dataSelect := parseDataSelectPathParameter(request)
	result, err := storageclass.GetStorageClassList(k8sClient, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetStorageClass(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("storageclass")
	result, err := storageclass.GetStorageClass(k8sClient, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleGetStorageClassPersistentVolumes(request *restful.Request,
	response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("storageclass")
	dataSelect := parseDataSelectPathParameter(request)
	result, err := persistentvolume.GetStorageClassPersistentVolumes(k8sClient,
		name, dataSelect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleStorageClassDefault(request *restful.Request,
	response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	name := request.PathParameter("storageclass")
	action := request.PathParameter("action")

	switch action {
	case "set":
		err = storageclass.SetDefaul(k8sClient, name)
	case "cancel":
		err = storageclass.CancelDefault(k8sClient, name)
	default:
		err = fmt.Errorf("%s is not a valid action", action)
	}
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeader(http.StatusOK)
}

func (apiHandler *APIHandler) handleGetServiceAccountList(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	namespace := parseNamespacePathParameter(request)
	dataselect := parseDataSelectPathParameter(request)
	result, err := serviceaccount.GetServiceAccountList(k8sClient, namespace, dataselect)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleLogSource(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	resourceName := request.PathParameter("resourceName")
	resourceType := request.PathParameter("resourceType")
	namespace := request.PathParameter("namespace")
	logSources, err := logs.GetLogSources(k8sClient, namespace, resourceName, resourceType)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, logSources)
}

func (apiHandler *APIHandler) handleLogs(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	namespace := request.PathParameter("namespace")
	podID := request.PathParameter("pod")
	containerID := request.PathParameter("container")

	refTimestamp := request.QueryParameter("referenceTimestamp")
	if refTimestamp == "" {
		refTimestamp = logs.NewestTimestamp
	}

	refLineNum, err := strconv.Atoi(request.QueryParameter("referenceLineNum"))
	if err != nil {
		refLineNum = 0
	}
	usePreviousLogs := request.QueryParameter("previous") == "true"
	offsetFrom, err1 := strconv.Atoi(request.QueryParameter("offsetFrom"))
	offsetTo, err2 := strconv.Atoi(request.QueryParameter("offsetTo"))
	logFilePosition := request.QueryParameter("logFilePosition")

	logSelector := logs.DefaultSelection
	if err1 == nil && err2 == nil {
		logSelector = &logs.Selection{
			ReferencePoint: logs.LogLineId{
				LogTimestamp: logs.LogTimestamp(refTimestamp),
				LineNum:      refLineNum,
			},
			OffsetFrom:      offsetFrom,
			OffsetTo:        offsetTo,
			LogFilePosition: logFilePosition,
		}
	}

	result, err := container.GetLogDetails(k8sClient, namespace, podID, containerID, logSelector, usePreviousLogs)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (apiHandler *APIHandler) handleLogFile(request *restful.Request, response *restful.Response) {
	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	namespace := request.PathParameter("namespace")
	podID := request.PathParameter("pod")
	containerID := request.PathParameter("container")
	usePreviousLogs := request.QueryParameter("previous") == "true"

	logStream, err := container.GetLogFile(k8sClient, namespace, podID, containerID, usePreviousLogs)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	handleDownload(response, logStream)
}

// parseNamespacePathParameter parses namespace selector for list pages in path parameter.
// The namespace selector is a comma separated list of namespaces that are trimmed.
// No namespaces means "view all user namespaces", i.e., everything except kube-system.
func parseNamespacePathParameter(request *restful.Request) *common.NamespaceQuery {
	namespace := request.PathParameter("namespace")
	namespaces := strings.Split(namespace, ",")
	var nonEmptyNamespaces []string
	for _, n := range namespaces {
		n = strings.Trim(n, " ")
		if len(n) > 0 {
			nonEmptyNamespaces = append(nonEmptyNamespaces, n)
		}
	}
	return common.NewNamespaceQuery(nonEmptyNamespaces)
}

func parsePaginationPathParameter(request *restful.Request) *dataselect.PaginationQuery {
	itemsPerPage, err := strconv.ParseInt(request.QueryParameter("itemsPerPage"), 10, 0)
	if err != nil {
		return dataselect.NoPagination
	}

	page, err := strconv.ParseInt(request.QueryParameter("page"), 10, 0)
	if err != nil {
		return dataselect.NoPagination
	}

	// Frontend pages start from 1 and backend starts from 0
	return dataselect.NewPaginationQuery(int(itemsPerPage), int(page-1))
}

func parseFilterPathParameter(request *restful.Request) *dataselect.FilterQuery {
	return dataselect.NewFilterQuery(strings.Split(request.QueryParameter("filterBy"), ","))
}

// Parses query parameters of the request and returns a SortQuery object
func parseSortPathParameter(request *restful.Request) *dataselect.SortQuery {
	return dataselect.NewSortQuery(strings.Split(request.QueryParameter("sortBy"), ","))
}

// Parses query parameters of the request and returns a DataSelectQuery object
func parseDataSelectPathParameter(request *restful.Request) *dataselect.DataSelectQuery {
	paginationQuery := parsePaginationPathParameter(request)
	sortQuery := parseSortPathParameter(request)
	filterQuery := parseFilterPathParameter(request)
	return dataselect.NewDataSelectQuery(paginationQuery, sortQuery, filterQuery)
}
