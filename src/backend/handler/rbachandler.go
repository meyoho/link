package handler

import (
	"errors"
	"fmt"
	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	authApi "link/src/backend/auth/api"
	clientapi "link/src/backend/client/api"
	kdErrors "link/src/backend/errors"
	"link/src/backend/resource/rbacrolebindings"
	"link/src/backend/resource/rbacroles"
	"net/http"
	"strings"
)

var ResourceApiVersion map[string]string = map[string]string{
	"Role":               "rbac.authorization.k8s.io/v1beta1",
	"ClusterRole":        "rbac.authorization.k8s.io/v1beta1",
	"RoleBinding":        "rbac.authorization.k8s.io/v1beta1",
	"ClusterRoleBinding": "rbac.authorization.k8s.io/v1beta1",
}

// rbac Roles
//handleCreateRbacRole create the role
func (apiHandler *APIHandler) handleCreateRbacRole(request *restful.Request, response *restful.Response) {
	namespace := request.PathParameter("namespace")
	payload := unstructured.Unstructured{}
	err := request.ReadEntity(&payload)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	kind := rbacroles.KindRbacRole

	if field, badValue, err := validateRbacRolePayload(&payload, kind, namespace); err != nil {
		kdErrors.HandleInvalidError(response, field, kind, badValue, err)
		return
	}
	resource, err := getAPIResource(kind, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := createRbacResource(&payload, resource, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	roleDetail := result.(*rbacroles.RbacRoleDetail)
	response.WriteHeaderAndEntity(http.StatusCreated, roleDetail)
}

func (apiHandler *APIHandler) handleRbacRoleDetail(request *restful.Request, response *restful.Response) {
	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	kind := rbacroles.KindRbacRole

	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	resource, err := getAPIResource(kind, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	resource.Namespaced = true

	gv := strings.Split(ResourceApiVersion[kind], "/")
	if len(gv) == 1 {
		resource.Group, resource.Version = "", gv[0]
	} else {
		resource.Group, resource.Version = gv[0], gv[1]
	}

	dynamicClient, err := apiHandler.cManager.DynamicClient(request, &schema.GroupVersion{Group: resource.Group, Version: resource.Version})
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := HandleRbacResource(schema.GroupVersionResource{
		Resource: resource.Name,
		Group:    resource.Group,
		Version:  resource.Version}, dynamicClient, k8sClient, request, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	if result == nil {
		response.WriteHeader(http.StatusOK)
		return
	}
	roleDetail := result.(*rbacroles.RbacRoleDetail)
	response.WriteHeaderAndEntity(http.StatusOK, roleDetail)
}

// rbac ClusterRole
func (apiHandler *APIHandler) handleCreateRbacClusterRole(request *restful.Request, response *restful.Response) {
	payload := unstructured.Unstructured{}
	err := request.ReadEntity(&payload)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	kind := rbacroles.KindRbacClusterRole

	if field, badValue, err := validateRbacRolePayload(&payload, kind, ""); err != nil {
		kdErrors.HandleInvalidError(response, field, kind, badValue, err)
		return
	}
	resource, err := getAPIResource(kind, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := createRbacResource(&payload, resource, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	clusterRoleDetail := result.(*rbacroles.RbacRoleDetail)
	response.WriteHeaderAndEntity(http.StatusCreated, clusterRoleDetail)
}

func (apiHandler *APIHandler) handleRbacClusterRoleDetail(request *restful.Request, response *restful.Response) {
	name := request.PathParameter("name")
	kind := rbacroles.KindRbacClusterRole

	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	resource, err := getAPIResource(kind, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	resource.Namespaced = false

	gv := strings.Split(ResourceApiVersion[kind], "/")
	if len(gv) == 1 {
		resource.Group, resource.Version = "", gv[0]
	} else {
		resource.Group, resource.Version = gv[0], gv[1]
	}

	dynamicClient, err := apiHandler.cManager.DynamicClient(request, &schema.GroupVersion{Group: resource.Group, Version: resource.Version})
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := HandleRbacResource(schema.GroupVersionResource{
		Resource: resource.Name,
		Group:    resource.Group,
		Version:  resource.Version}, dynamicClient, k8sClient, request, "", name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	if result == nil {
		response.WriteHeader(http.StatusOK)
		return
	}
	clusterRoleDetail := result.(*rbacroles.RbacRoleDetail)
	response.WriteHeaderAndEntity(http.StatusOK, clusterRoleDetail)
}

//validateUpdateRolePayload valid update role parameter
func validateUpdateRolePayload(payload *unstructured.Unstructured, name, namespace, resourceKind string) (field string, badValue interface{}, err error) {
	if field, badValue, err = validateRbacRolePayload(payload, resourceKind, namespace); err != nil {
		return field, badValue, err
	}

	if payload.GetName() != name {
		return "name", name, errors.New("name can not update")
	}
	if payload.GetNamespace() != namespace {
		return "namespace", namespace, errors.New("namespace can not update")
	}
	return
}

//validateRbacRolePayload valid create role parameter
func validateRbacRolePayload(payload *unstructured.Unstructured, resourceKind, namespace string) (field string, badValue interface{}, err error) {

	if kind, ok := payload.Object["kind"]; !ok || kind != resourceKind {
		payload.Object["kind"] = resourceKind
	}
	if apiVersion, ok := payload.Object["apiVersion"]; !ok || strings.TrimSpace(apiVersion.(string)) == "" {
		payload.Object["apiVersion"] = ResourceApiVersion[rbacroles.KindRbacRole]
	}

	if metadata, ok := payload.Object["metadata"]; !ok {
		return "metadata", nil, errors.New("metadata is required")
	} else if _, ok := metadata.(map[string]interface{}); !ok {
		return "metadata", nil, errors.New("the type of metadata is wrong")
	} else {
		metadataMap := metadata.(map[string]interface{})
		if name, ok := metadataMap["name"]; !ok || strings.TrimSpace(name.(string)) == "" {
			return "name", name, errors.New("name is required")
		}

		if annotations, ok := metadataMap["annotations"]; !ok {
			return "annotations", annotations, errors.New("annotations is required")
		} else if _, ok := annotations.(map[string]interface{}); !ok {
			return "annotations", annotations, errors.New("the type of annotations is wrong")
		} else {
			annotationsMap := annotations.(map[string]interface{})
			if _, ok := annotationsMap["displayName"]; !ok {
				annotationsMap["displayName"] = ""
			}
			if _, ok := annotationsMap["description"]; !ok {
				annotationsMap["description"] = ""
			}
		}
	}

	switch resourceKind {
	case rbacroles.KindRbacRole:
		payload.Object["metadata"].(map[string]interface{})["namespace"] = namespace
	case rbacroles.KindRbacClusterRole:
		payload.Object["metadata"].(map[string]interface{})["namespace"] = ""
	}
	return
}

// rbac RoleBinding
func (apiHandler *APIHandler) handleCreateRbacRoleBinding(request *restful.Request, response *restful.Response) {
	namespace := request.PathParameter("namespace")
	payload := unstructured.Unstructured{}
	err := request.ReadEntity(&payload)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	kind := rbacrolebindings.KindRoleBinding

	if field, badValue, err := validateRbacRolebindingPayload(&payload, kind, namespace); err != nil {
		kdErrors.HandleInvalidError(response, field, kind, badValue, err)
		return
	}

	resource, err := getAPIResource(kind, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	result, err := createRbacResource(&payload, resource, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	clusterRoleBindingDetail := result.(*rbacrolebindings.RbacRoleBindingDetail)
	response.WriteHeaderAndEntity(http.StatusCreated, clusterRoleBindingDetail)
}

func (apiHandler *APIHandler) handleRbacRoleBindingDetail(request *restful.Request, response *restful.Response) {
	name := request.PathParameter("name")
	namespace := request.PathParameter("namespace")
	kind := rbacrolebindings.KindRoleBinding

	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	resource, err := getAPIResource(kind, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	resource.Namespaced = true

	gv := strings.Split(ResourceApiVersion[kind], "/")
	if len(gv) == 1 {
		resource.Group, resource.Version = "", gv[0]
	} else {
		resource.Group, resource.Version = gv[0], gv[1]
	}

	dynamicClient, err := apiHandler.cManager.DynamicClient(request, &schema.GroupVersion{Group: resource.Group, Version: resource.Version})
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := HandleRbacResource(schema.GroupVersionResource{
		Resource: resource.Name,
		Group:    resource.Group,
		Version:  resource.Version}, dynamicClient, k8sClient, request, namespace, name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	if result == nil {
		response.WriteHeader(http.StatusOK)
		return
	}
	roleBindingDetail := result.(*rbacrolebindings.RbacRoleBindingDetail)
	response.WriteHeaderAndEntity(http.StatusOK, roleBindingDetail)
}

// rbac ClusterRoleBinding
func (apiHandler *APIHandler) handleCreateRbacClusterRoleBinding(request *restful.Request, response *restful.Response) {
	payload := unstructured.Unstructured{}
	err := request.ReadEntity(&payload)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	kind := rbacrolebindings.KindClusterRoleBinding

	if field, badValue, err := validateRbacRolebindingPayload(&payload, kind, ""); err != nil {
		kdErrors.HandleInvalidError(response, field, kind, badValue, err)
		return
	}
	resource, err := getAPIResource(kind, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := createRbacResource(&payload, resource, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	clusterRoleBindingDetail := result.(*rbacrolebindings.RbacRoleBindingDetail)
	response.WriteHeaderAndEntity(http.StatusCreated, clusterRoleBindingDetail)
}

func (apiHandler *APIHandler) handleRbacClusterRoleBindingDetail(request *restful.Request, response *restful.Response) {
	name := request.PathParameter("name")
	kind := rbacrolebindings.KindClusterRoleBinding

	k8sClient, err := apiHandler.cManager.Client(request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}

	resource, err := getAPIResource(kind, apiHandler.cManager, request)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	resource.Namespaced = false

	gv := strings.Split(ResourceApiVersion[kind], "/")
	if len(gv) == 1 {
		resource.Group, resource.Version = "", gv[0]
	} else {
		resource.Group, resource.Version = gv[0], gv[1]
	}

	dynamicClient, err := apiHandler.cManager.DynamicClient(request, &schema.GroupVersion{Group: resource.Group, Version: resource.Version})
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	result, err := HandleRbacResource(schema.GroupVersionResource{
		Resource: resource.Name,
		Group:    resource.Group,
		Version:  resource.Version}, dynamicClient, k8sClient, request, "", name)
	if err != nil {
		kdErrors.HandleInternalError(response, err)
		return
	}
	if result == nil {
		response.WriteHeader(http.StatusOK)
		return
	}
	clusterRoleBindingDetail := result.(*rbacrolebindings.RbacRoleBindingDetail)
	response.WriteHeaderAndEntity(http.StatusOK, clusterRoleBindingDetail)
}

func validateRbacRolebindingPayload(payload *unstructured.Unstructured, resourceKind, namespace string) (field string, badValue interface{}, err error) {
	if kind, ok := payload.Object["kind"]; !ok || kind != resourceKind {
		payload.Object["kind"] = resourceKind
	}
	if apiVersion, ok := payload.Object["apiVersion"]; !ok || strings.TrimSpace(apiVersion.(string)) == "" {
		payload.Object["apiVersion"] = ResourceApiVersion[resourceKind]
	}

	if metadata, ok := payload.Object["metadata"]; !ok {
		return "metadata", nil, errors.New("metadata is required")
	} else if _, ok := metadata.(map[string]interface{}); !ok {
		return "metadata", nil, errors.New("the type of metadata is wrong")
	} else {
		metadataMap := metadata.(map[string]interface{})
		if name, ok := metadataMap["name"]; !ok || strings.TrimSpace(name.(string)) == "" {
			return "name", name, errors.New("name is required")
		}
	}

	switch resourceKind {
	case rbacrolebindings.KindRoleBinding:
		payload.Object["metadata"].(map[string]interface{})["namespace"] = namespace
	case rbacrolebindings.KindClusterRoleBinding:
		payload.Object["metadata"].(map[string]interface{})["namespace"] = ""
	}
	return
}

//validateUpdateRbacRolebindingPayload valid update roleBinding payload
func validateUpdateRbacRolebindingPayload(payload *unstructured.Unstructured, name, namespace, resourceKind string) (field string, badValue interface{}, err error) {
	if field, badValue, err = validateRbacRolebindingPayload(payload, resourceKind, namespace); err != nil {
		return field, badValue, err
	}
	payloadName := payload.Object["metadata"].(map[string]interface{})["name"].(string)
	payloadNamespace := payload.Object["metadata"].(map[string]interface{})["namespace"].(string)

	if payloadName != name {
		return "name", name, errors.New("name can not update")
	}
	if payloadNamespace != namespace {
		return "namespace", namespace, errors.New("namespace can not update")
	}
	return
}

func HandleRbacResource(resource schema.GroupVersionResource, dynamicClient dynamic.Interface, k8sClient kubernetes.Interface, request *restful.Request,
	namespace, name string) (interface{}, error) {

	switch method := request.Request.Method; method {

	case http.MethodGet:
		r, err := dynamicClient.Resource(resource).Namespace(namespace).Get(name, metaV1.GetOptions{})
		if err != nil {
			return nil, err
		}
		switch resource.Resource {
		case "roles":
			fallthrough
		case "clusterroles":
			result, err := rbacroles.GetRbacRoleDetail(r)
			return result, err
		case "rolebindings":
			fallthrough
		case "clusterrolebindings":
			result, err := rbacrolebindings.GetRbacRoleBindingDetail(r)
			return result, err
		}

	case http.MethodPut:
		payload := unstructured.Unstructured{}
		err := request.ReadEntity(&payload)
		if err != nil {
			return nil, err
		}

		switch resource.Resource {
		case "roles":
			fallthrough
		case "clusterroles":
			setCreatedBy(request, &payload)
			r, err := dynamicClient.Resource(resource).Namespace(namespace).Update(&payload)
			if err != nil {
				return nil, err
			}

			result, err := rbacroles.GetRbacRoleDetail(r)
			return result, err
		case "rolebindings":
			fallthrough
		case "clusterrolebindings":
			setCreatedBy(request, &payload)
			r, err := dynamicClient.Resource(resource).Namespace(namespace).Update(&payload)
			if err != nil {
				return nil, err
			}
			result, err := rbacrolebindings.GetRbacRoleBindingDetail(r)
			return result, err
		}

	case http.MethodDelete:
		err := dynamicClient.Resource(resource).Namespace(namespace).Delete(name, &metaV1.DeleteOptions{})
		return nil, err
	default:
		return nil, fmt.Errorf("method %s not allowed", method)
	}
	return nil, errors.New("unknown error")
}

func createRbacResource(raw *unstructured.Unstructured, resource *v1.APIResource, cm clientapi.ClientManager, request *restful.Request) (interface{}, error) {
	kind := resource.Kind
	setCreatedBy(request, raw)

	if raw.GetNamespace() != "" {
		resource.Namespaced = true
	}
	var group, version string
	gv := strings.Split(ResourceApiVersion[kind], "/")
	if len(gv) == 1 {
		group, version = "", gv[0]
	} else {
		group, version = gv[0], gv[1]
	}

	dynamicClient, err := cm.DynamicClient(request, &schema.GroupVersion{Group: group, Version: version})
	if err != nil {
		return nil, err
	}

	r, err := dynamicClient.Resource(schema.GroupVersionResource{
		Resource: resource.Name,
		Group:    group,
		Version:  version}).Namespace(raw.GetNamespace()).Create(raw)
	if err != nil {
		return nil, err
	}

	switch kind {
	case rbacroles.KindRbacRole:
		fallthrough
	case rbacroles.KindRbacClusterRole:
		result, err := rbacroles.GetRbacRoleDetail(r)
		if err != nil {
			return nil, err
		}
		return result, nil
	case rbacrolebindings.KindRoleBinding:
		fallthrough
	case rbacrolebindings.KindClusterRoleBinding:
		result, err := rbacrolebindings.GetRbacRoleBindingDetail(r)
		if err != nil {
			return nil, err
		}
		return result, nil
	}
	return nil, nil
}

func getAPIResource(kind string, cm clientapi.ClientManager, request *restful.Request) (*v1.APIResource, error) {
	k8sClient, err := cm.Client(request)
	if err != nil {
		return nil, err
	}
	k, err := getKindName(k8sClient, kind)
	if err != nil {
		return nil, err
	}
	resource := &v1.APIResource{
		Name: k.Name,
		Kind: kind,
	}
	return resource, nil
}

func setCreatedBy(request *restful.Request, payload *unstructured.Unstructured) {
	if _, ok := payload.Object["metadata"].(map[string]interface{})["annotations"]; !ok {
		payload.Object["metadata"].(map[string]interface{})["annotations"] = make(map[string]interface{})
	}
	annotations := payload.Object["metadata"].(map[string]interface{})["annotations"].((map[string]interface{}))
	idToken, err := authApi.ParseJWTFromHeader(request)
	if err != nil {
		fmt.Println(err)
		annotations["createdBy"] = ""
		return
	}
	fmt.Println(idToken.Email)
	annotations["createdBy"] = idToken.Email
}
