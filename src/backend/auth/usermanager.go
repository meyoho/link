package auth

import (
	"github.com/emicklei/go-restful"
	rbac "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	authApi "link/src/backend/auth/api"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/rbacrolebindings"
)

type RbacRoleType map[string][]rbac.RoleRef

func (self manager) ListUsers(request *restful.Request) ([]*authApi.User, error) {
	k8sClient, err := self.clientManager.Client(request)
	if err != nil {
		return nil, err
	}

	userResource, err := authApi.GetApiResource(authApi.CrdUserKind, false, k8sClient)
	if err != nil {
		return nil, err
	}

	dynamicClient, err := self.clientManager.DynamicClient(request,
		&schema.GroupVersion{authApi.CrdUserGroup, authApi.CrdUserVersion})
	if err != nil {
		return nil, err
	}

	userGvr := schema.GroupVersionResource{Resource: userResource.Name, Group: authApi.CrdUserGroup, Version: authApi.CrdUserVersion}
	rl, err := dynamicClient.Resource(userGvr).Namespace("").List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	emails := []string{}
	users := []*authApi.User{}
	for _, i := range rl.Items {
		user := &authApi.User{}
		spec := i.Object["spec"].(map[string]interface{})

		switch {
		case checkKeyExist(spec, "email"):
			if email, ok := spec["email"].(string); ok {
				user.Email = email
			}
			fallthrough
		case checkKeyExist(spec, "name"):
			if name, ok := spec["name"].(string); ok {
				user.Name = name
			}
			fallthrough
		case checkKeyExist(spec, "groups"):
			if groups, ok := spec["groups"].([]string); ok {
				user.Groups = groups
			}
			fallthrough
		case checkKeyExist(spec, "is_admin"):
			if isAdmin, ok := spec["is_admin"].(bool); ok {
				user.IsAdmin = isAdmin
			}
		}

		users = append(users, user)
		if user.Email != "" {
			emails = append(emails, user.Email)
		}
	}

	// Get user roles
	usersRoles, err := self.getUserRoles(emails, k8sClient, request)
	if err == nil {
		for i := 0; i < len(users); i++ {
			if _, ok := usersRoles[users[i].Email]; ok {
				users[i].Roles = usersRoles[users[i].Email]
			} else {
				users[i].Roles = []rbac.RoleRef{}
			}
		}
	}

	return users, nil
}

func (self manager) GetUserDetail(request *restful.Request, userName string) (*authApi.User, error) {
	k8sClient, err := self.clientManager.Client(request)
	if err != nil {
		return nil, err
	}

	userResource, err := authApi.GetApiResource(authApi.CrdUserKind, false, k8sClient)
	if err != nil {
		return nil, err
	}

	dynamicClient, err := self.clientManager.DynamicClient(request,
		&schema.GroupVersion{authApi.CrdUserGroup, authApi.CrdUserVersion})
	if err != nil {
		return nil, err
	}
	userGvr := schema.GroupVersionResource{Resource: userResource.Name, Group: authApi.CrdUserGroup, Version: authApi.CrdUserVersion}
	obj, err := dynamicClient.Resource(userGvr).Namespace("").Get(
		authApi.GetUserMetadataName(userName), metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	alaudaUser := &authApi.User{}
	spec := obj.Object["spec"].(map[string]interface{})
	switch {
	case checkKeyExist(spec, "name"):
		if name, ok := spec["name"].(string); ok {
			alaudaUser.Name = name
		}
		fallthrough
	case checkKeyExist(spec, "email"):
		if email, ok := spec["email"].(string); ok {
			alaudaUser.Email = email
		}
		fallthrough
	case checkKeyExist(spec, "groups"):
		if groups, ok := spec["groups"].([]string); ok {
			alaudaUser.Groups = groups
		}
		fallthrough
	case checkKeyExist(spec, "is_admin"):
		if isAdmin, ok := spec["is_admin"].(bool); ok {
			alaudaUser.IsAdmin = isAdmin
		}
	}
	return alaudaUser, nil
}

func checkKeyExist(m map[string]interface{}, k string) bool {
	_, ok := m[k]
	return ok
}

func (self manager) getUserRoles(emails []string, k8sClient kubernetes.Interface, request *restful.Request) (RbacRoleType, error) {
	ret := make(RbacRoleType, 0)
	if len(emails) == 0 {
		return ret, nil
	}

	channels := &common.ResourceChannels{
		RoleBindingList:        common.GetRoleBindingListChannel(k8sClient, 1),
		ClusterRoleBindingList: common.GetClusterRoleBindingListChannel(k8sClient, 1),
	}

	dsQuery := dataselect.NewDataSelectQuery(dataselect.NoPagination, dataselect.NoSort, dataselect.NoFilter)
	result, err := rbacrolebindings.GetRbacRoleBindingListFromChannels(channels, dsQuery)

	if err != nil {
		return ret, err
	}

	for _, v := range result.Items {
		for _, subject := range v.Subjects {
			if subject.Kind != "User" || subject.Name == "" || !stringInSlice(subject.Name, emails) {
				continue
			}
			if _, ok := ret[subject.Name]; !ok {
				ret[subject.Name] = make([]rbac.RoleRef, 0)
			}
			ret[subject.Name] = append(ret[subject.Name], v.RoleRef)
		}
	}

	return ret, nil
}
