package api

import (
	"crypto/md5"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"github.com/json-iterator/go"
	"k8s.io/klog"

	"errors"
	"fmt"
	"io/ioutil"
	"link/src/backend/resource/other"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"

	clientapi "link/src/backend/client/api"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
)

var SupportedIDPList = []string{"ldap", "oidc"}
var CustomResourceDefinition = []string{CrdUserKind, CrdAlaudaK8sKind, CrdAlaudaDevopsKind}

const (
	CrdResourceGroup   = "apiextensions.k8s.io"
	CrdResourceVersion = "v1beta1"
	CrdResourceKind    = "CustomResourceDefinition"

	CrdUserKind    = "AlaudaUser"
	CrdUserGroup   = "auth.io"
	CrdUserVersion = "v1"
	CrdUserName    = "alaudausers.auth.io"

	CrdAlaudaK8sKind    = "AlaudaK8s"
	CrdAlaudaK8sGroup   = "auth.io"
	CrdAlaudaK8sVersion = "v1"
	CrdAlaudaK8sName    = "alaudak8s.auth.io"

	CrdAlaudaDevopsKind    = "AlaudaDevops"
	CrdAlaudaDevopsGroup   = "auth.io"
	CrdAlaudaDevopsVersion = "v1"
	CrdAlaudaDevopsName    = "alaudadevops.auth.io"

	DexConfigMapVersion = "v1"
	DexConfigMapGroup   = ""
	DexConfigMapName    = "dex-configmap"

	DexDeploymentVersion = "v1beta1"
	DexDeploymentGroup   = "extensions"
	DexDeploymentName    = "alauda-dex"

	ConfigMap  = "ConfigMap"
	Deployment = "Deployment"
)

type JWEToken struct {
	Issuer        string      `json:"iss"`
	Subject       string      `json:"sub"`
	Audience      string      `json:"aud"`
	Expiry        int         `json:"exp"`
	IssuedAt      int         `json:"iat"`
	Nonce         string      `json:"nonce"`
	Email         string      `json:"email"`
	EmailVerified bool        `json:"email_verified"`
	Name          string      `json:"name"`
	Groups        []string    `json:"groups"`
	Ext           jwtTokenExt `json:"ext"`
	MetadataName  string
}

type jwtTokenExt struct {
	IsAdmin bool   `json:"is_admin"`
	ConnID  string `json:"conn_id"`
}

func ParseJWTFromHeader(request *restful.Request) (*JWEToken, error) {
	var rawToken string
	authorization := request.HeaderParameter("Authorization")
	if strings.TrimSpace(authorization) == "" {
		return nil, errors.New("Authentication head does not exist")
	}
	fmt.Printf("authorization header: %s", authorization)
	switch {
	case strings.HasPrefix(strings.TrimSpace(authorization), "Bearer"):
		rawToken = strings.TrimPrefix(strings.TrimSpace(authorization), "Bearer")
	case strings.HasPrefix(strings.TrimSpace(authorization), "bearer"):
		rawToken = strings.TrimPrefix(strings.TrimSpace(authorization), "bearer")
	}
	return ParseJWT(rawToken)
}

func ParseJWT(rawToken string) (*JWEToken, error) {
	var (
		token JWEToken
	)

	if rawToken == "" {
		return nil, errors.New("Authentication head is invalid")
	}
	parts := strings.Split(rawToken, ".")
	if len(parts) < 2 {
		return nil, fmt.Errorf("oidc: malformed jwt, expected 3 parts got %d", len(parts))
	}
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, fmt.Errorf("oidc: malformed jwt payload: %v", err)
	}
	if err := jsoniter.ConfigCompatibleWithStandardLibrary.Unmarshal(payload, &token); err != nil {
		fmt.Println(err)
		return nil, err
	}
	token.MetadataName = GetUserMetadataName(token.Email)

	return &token, nil
}

func GetUserMetadataName(userID string) string {
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(strings.TrimSpace(userID)))
	cipherStr := md5Ctx.Sum(nil)
	return hex.EncodeToString(cipherStr)
}

func CovertMap(obj interface{}) map[string]interface{} {
	newMap := make(map[string]interface{})

	switch obj := obj.(type) {
	case map[interface{}]interface{}:
		for key, value := range obj {
			switch key := key.(type) {
			case string:
				switch value := value.(type) {
				case string:
					newMap[key] = value
				case bool:
					newMap[key] = value
				case []byte:
					newMap[key] = value
				case map[interface{}]interface{}:
					newMap[key] = CovertMap(value)
				case []interface{}:
					for _, item := range value {
						newMap[key] = CovertMap(item)
					}
				}
			}
		}
	}

	return newMap
}

func GetClient(rootCAFilePath, rootCAContent string) (*http.Client, error) {
	tlsConfig := tls.Config{RootCAs: x509.NewCertPool()}
	var (
		rootCABytes []byte
		err         error
	)
	if rootCAContent != "" {
		rootCABytes = []byte(rootCAContent)

	} else if rootCAFilePath != "" {
		rootCABytes, err = ioutil.ReadFile(rootCAFilePath)
		if err != nil {
			return nil, fmt.Errorf("failed to read root-ca: %v", err)
		}
	}
	if !tlsConfig.RootCAs.AppendCertsFromPEM(rootCABytes) {
		return nil, fmt.Errorf("no certs found in root CA file %s", rootCABytes)
	}
	return &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tlsConfig,
			Proxy:           http.ProxyFromEnvironment,
			Dial: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).Dial,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}, nil
}

func InitK8sCRD(clientManager clientapi.ClientManager) error {
	dyclient, err := clientManager.DynamicClient(nil,
		&schema.GroupVersion{CrdResourceGroup, CrdResourceVersion})
	if err != nil {
		return err
	}

	k8sClient, err := clientManager.Client(nil)
	if err != nil {
		return err
	}

	for _, crd := range CustomResourceDefinition {
		switch crd {
		case CrdUserKind:
			_, err = getKindName(k8sClient, CrdUserKind)
			if err != nil {
				err = createUserCRD(dyclient, k8sClient)
				if err != nil {
					return err
				}
			}
		case CrdAlaudaK8sKind:
			_, err = getKindName(k8sClient, CrdAlaudaK8sKind)
			if err != nil {
				err = createAlaudaK8sCRD(dyclient, k8sClient)
				if err != nil {
					return err
				}
			}
		case CrdAlaudaDevopsKind:
			_, err = getKindName(k8sClient, CrdAlaudaDevopsKind)
			if err != nil {
				err = createAlaudaDevopsCRD(dyclient, k8sClient)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func createUserCRD(dyclient dynamic.Interface, k8sClient kubernetes.Interface) error {
	k, err := getKindName(k8sClient, CrdResourceKind)
	if err != nil {
		return err
	}

	resource := schema.GroupVersionResource{
		Resource: k.Name,
		Group:    CrdResourceGroup,
		Version:  CrdResourceKind,
	}

	content := generateUserCRDPayload()
	klog.Infof("createUserCRD: %v", content)

	_, err = dyclient.Resource(resource).Namespace("").Create(content)
	return err
}

func generateUserCRDPayload() *unstructured.Unstructured {
	userCRD := make(map[string]interface{})
	userCRD["apiVersion"] = fmt.Sprintf("%s/%s", CrdResourceGroup, CrdResourceVersion)
	userCRD["kind"] = CrdResourceKind

	metadata := make(map[string]interface{})
	metadata["name"] = CrdUserName
	userCRD["metadata"] = metadata

	spec := make(map[string]interface{})
	spec["group"] = CrdUserGroup
	spec["version"] = CrdUserVersion
	spec["scope"] = "Cluster"

	names := make(map[string]interface{})
	names["plural"] = fmt.Sprintf("%ss", strings.ToLower(CrdUserKind))
	names["singular"] = strings.ToLower(CrdUserKind)
	names["kind"] = CrdUserKind
	spec["names"] = names

	userCRD["spec"] = spec

	return &unstructured.Unstructured{
		Object: userCRD,
	}
}

func createAlaudaK8sCRD(dyclient dynamic.Interface, k8sClient kubernetes.Interface) error {
	k, err := getKindName(k8sClient, CrdResourceKind)
	if err != nil {
		return err
	}

	resource := schema.GroupVersionResource{
		Resource: k.Name,
		Group:    CrdResourceGroup,
		Version:  CrdResourceVersion,
	}

	content := generateAlaudaK8sCRDPayload()
	klog.Infof("createAlaudaK8sCRD: %v", content)

	_, err = dyclient.Resource(resource).Namespace("").Create(content)
	return err
}

func createAlaudaDevopsCRD(dyclient dynamic.Interface, k8sClient kubernetes.Interface) error {
	k, err := getKindName(k8sClient, CrdResourceKind)
	if err != nil {
		return err
	}

	resource := schema.GroupVersionResource{
		Resource: k.Name,
		Group:    CrdResourceGroup,
		Version:  CrdResourceVersion,
	}

	content := generateAlaudaDevopsCRDPayload()
	klog.Infof("createAlaudaDevopsCRD: %v", content)

	_, err = dyclient.Resource(resource).Namespace("").Create(content)
	return err
}

func generateAlaudaDevopsCRDPayload() *unstructured.Unstructured {
	AlaudaDevopsCRD := make(map[string]interface{})
	AlaudaDevopsCRD["apiVersion"] = fmt.Sprintf("%s/%s", CrdResourceGroup, CrdResourceVersion)
	AlaudaDevopsCRD["kind"] = CrdResourceKind

	metadata := make(map[string]interface{})
	metadata["name"] = CrdAlaudaDevopsName
	AlaudaDevopsCRD["metadata"] = metadata

	spec := make(map[string]interface{})
	spec["group"] = CrdAlaudaDevopsGroup
	spec["version"] = CrdAlaudaDevopsVersion
	spec["scope"] = "Cluster"

	names := make(map[string]interface{})
	names["plural"] = fmt.Sprintf("%s", strings.ToLower(CrdAlaudaDevopsKind))
	names["singular"] = strings.ToLower(CrdAlaudaDevopsKind)
	names["kind"] = CrdAlaudaDevopsKind
	spec["names"] = names

	AlaudaDevopsCRD["spec"] = spec

	klog.Infof("generateAlaudaDevopsCRDPayload: %v", AlaudaDevopsCRD)

	return &unstructured.Unstructured{
		Object: AlaudaDevopsCRD,
	}
}

func generateAlaudaK8sCRDPayload() *unstructured.Unstructured {
	AlaudaK8sCRD := make(map[string]interface{})
	AlaudaK8sCRD["apiVersion"] = fmt.Sprintf("%s/%s", CrdResourceGroup, CrdResourceVersion)
	AlaudaK8sCRD["kind"] = CrdResourceKind

	metadata := make(map[string]interface{})
	metadata["name"] = CrdAlaudaK8sName
	AlaudaK8sCRD["metadata"] = metadata

	spec := make(map[string]interface{})
	spec["group"] = CrdAlaudaK8sGroup
	spec["version"] = CrdAlaudaK8sVersion
	spec["scope"] = "Cluster"

	names := make(map[string]interface{})
	names["plural"] = fmt.Sprintf("%s", strings.ToLower(CrdAlaudaK8sKind))
	names["singular"] = strings.ToLower(CrdAlaudaK8sKind)
	names["kind"] = CrdAlaudaK8sKind
	spec["names"] = names

	AlaudaK8sCRD["spec"] = spec

	klog.Infof("generateAlaudaK8sCRDPayload: %v", AlaudaK8sCRD)

	return &unstructured.Unstructured{
		Object: AlaudaK8sCRD,
	}
}

func getKindName(client kubernetes.Interface, kind string) (other.KindMeta, error) {
	if k, ok := other.KindToName[kind]; ok {
		return k, nil
	}

	if _, err := other.GetCanListResource(client, false); err != nil {
		return other.KindMeta{}, err
	}

	if k, ok := other.KindToName[kind]; ok {
		return k, nil
	}
	return other.KindMeta{}, fmt.Errorf("kind %s not find in kubernetes server", kind)
}

func GetApiResource(resourceKind string, namespaced bool, k8sClient kubernetes.Interface) (*metav1.APIResource, error) {
	k, err := getKindName(k8sClient, resourceKind)
	if err != nil {
		return nil, err
	}
	resource := &metav1.APIResource{
		Name:       k.Name,
		Kind:       resourceKind,
		Namespaced: namespaced,
	}
	return resource, nil
}

// ShouldRejectRequest returns true if url contains name and namespace of resource that should be filtered out from
// dashboard.
func ShouldRejectRequest(url string) bool {
	// For now we have only one resource that should be checked
	return strings.Contains(url, EncryptionKeyHolderName) && strings.Contains(url, EncryptionKeyHolderNamespace)
}
