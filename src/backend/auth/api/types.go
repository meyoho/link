// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"context"
	"net/http"

	settingsapi "link/src/backend/settings/api"

	"github.com/emicklei/go-restful"
	"golang.org/x/oauth2"
	rbac "k8s.io/api/rbac/v1"
	"k8s.io/client-go/kubernetes"
)

const (
	// Resource information that are used as encryption key storage. Can be accessible by multiple dashboard replicas.
	EncryptionKeyHolderName      = "kubernetes-dashboard-key-holder"
	EncryptionKeyHolderNamespace = "kube-system"
)

// Manager is used for user authentication management.
type Manager interface {
	AuthManager
	IDPManager
	UserManager
}

type AuthManager interface {
	RetrieveToken(authCode string, ctx context.Context) (*AuthResponse, error)
	Refresh(refreshToken string, ctx context.Context) (*AuthResponse, error)
	SyncUserInfo(request *restful.Request, jweToken *JWEToken) error

	GetOAuth2Config(kubernetes.Interface) (*oauth2.Config, []oauth2.AuthCodeOption, error)

	GetHTTPClient(kubernetes.Interface) *http.Client

	GetAuthSettings(kubernetes.Interface) *settingsapi.AuthSettings
}

type IDPManager interface {
	GetIDPConfig(request *restful.Request) (map[interface{}]interface{}, error)
	SetIDPConfig(request *restful.Request, idpName string) error
	DeleteIDPConfig(request *restful.Request, idpName string) error
}

type UserManager interface {
	ListUsers(request *restful.Request) ([]*User, error)
	GetUserDetail(request *restful.Request, userName string) (*User, error)
}

type AuthResponse struct {
	IDToken      string `json:"id_token"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	User
}

type LoginResponse struct {
	Settings settingsapi.AuthSettings `json:"settings"`
	AuthURL  string                   `json:"auth_url"`
	Errors   []error                  `json:"errors"`
}

type User struct {
	Name    string         `json:"name"`
	Email   string         `json:"email"`
	Groups  []string       `json:"groups"`
	IsAdmin bool           `json:"is_admin"`
	Roles   []rbac.RoleRef `json:"roles"`
}

func NewAuthResponse(id_token string, jweToken *JWEToken) *AuthResponse {
	return &AuthResponse{
		IDToken: id_token,
		User: User{
			Email:   jweToken.Email,
			Name:    jweToken.Name,
			Groups:  jweToken.Groups,
			IsAdmin: jweToken.Ext.IsAdmin,
		},
	}
}
