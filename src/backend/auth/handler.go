package auth

import (
	er "errors"
	"fmt"
	"k8s.io/klog"
	authApi "link/src/backend/auth/api"
	"link/src/backend/auth/idps"
	clientapi "link/src/backend/client/api"

	"net/http"
	"strings"

	"link/src/backend/errors"

	"github.com/coreos/go-oidc"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/tools/clientcmd/api"
)

// AuthHandler manages all endpoints related to dashboard auth, such as login.
type AuthHandler struct {
	manager  authApi.Manager
	AppState string
}

// Install creates new endpoints for dashboard auth, such as login. It allows user to log in to dashboard using
// one of the supported methods. See AuthManager and Authenticator for more information.
func (self *AuthHandler) Install(ws *restful.WebService) {
	ws.Route(
		ws.GET("/login").
			To(self.handleLogin).
			Writes(authApi.LoginResponse{}))

	ws.Route(
		ws.GET("/sync-user-info").
			To(self.handleSyncUserInfo))

	ws.Route(
		ws.GET("/login/callback").
			To(self.handleCallback).
			Writes(authApi.AuthResponse{}),
	)

	ws.Route(
		ws.POST("/login/callback").
			To(self.handleCallback).
			Writes(authApi.AuthResponse{}),
	)

	ws.Route(
		ws.GET("/idps").
			To(self.ListIDPS),
	)

	ws.Route(
		ws.POST("/idps/{idp_name}").
			To(self.SetIDPConfig),
	)

	ws.Route(
		ws.PUT("/idps/{idp_name}").
			To(self.UpdateIDPConfig),
	)

	ws.Route(
		ws.DELETE("/idps/{idp_name}").
			To(self.DeleteIDPConfig),
	)

	ws.Route(
		ws.GET("/idps/configs").
			To(self.GetIDPDetails),
	)

	ws.Route(
		ws.GET("/users").
			To(self.ListUsers),
	)

	ws.Route(
		ws.GET("/users/{user_name}").
			To(self.GetUserDetails),
	)

	ws.Route(
		ws.GET("/auth/resources/{resource}/cani").
			To(self.handleResourceCanI),
	)

	ws.Route(
		ws.GET("/auth/platforms/{platform}/views").
			To(self.handlePlatformViews),
	)

}

func (self *AuthHandler) handleLogin(request *restful.Request, response *restful.Response) {
	loginResponse := authApi.LoginResponse{Errors: []error{}}
	settings := self.manager.GetAuthSettings(nil)
	if settings != nil {
		loginResponse.Settings = *settings
		if settings.Enabled {
			oauth2Config, options, err := self.manager.GetOAuth2Config(nil)
			if err != nil {
				klog.Errorf("error checking oauth2Config:", err)
				loginResponse.Errors = append(loginResponse.Errors, errors.FormatError(err))
			}
			if oauth2Config != nil {
				loginResponse.AuthURL = oauth2Config.AuthCodeURL(self.AppState, options...)
				klog.Info("Issuer Inner: ", settings.IssuerInner)
				klog.Info("Issuer Public: ", settings.Issuer)
				klog.Info("AuthURL before converting: ", loginResponse.AuthURL)
				if settings.IssuerInner != "" {
					loginResponse.AuthURL = strings.Replace(loginResponse.AuthURL, settings.IssuerInner, settings.Issuer, 1)
				}
				klog.Info("AuthURL after converting: ", loginResponse.AuthURL)
			}
		}
	}
	code := http.StatusOK
	if response.StatusCode() == http.StatusPaymentRequired {
		code = http.StatusPaymentRequired
	}

	response.WriteHeaderAndEntity(code, loginResponse)
}

func (self *AuthHandler) handleSyncUserInfo(request *restful.Request, response *restful.Response) {
	var (
		id_token         string
		token_is_invalid bool
	)

	authHeader := request.HeaderParameter("Authorization")
	if strings.HasPrefix(authHeader, "Bearer ") {
		id_token = strings.TrimPrefix(authHeader, "Bearer ")
	}

	am := self.manager.(*manager)
	err := am.clientManager.HasAccess(api.AuthInfo{Token: id_token})
	if err != nil {
		klog.Errorf("sync user info error: %v", err)
		token_is_invalid = true
	}

	jweToken, err := authApi.ParseJWT(id_token)
	if err != nil {
		klog.Errorf("sync user info error: %v", err)
		token_is_invalid = true
	}
	klog.Infof("login success: %s", id_token)

	if !token_is_invalid {
		if err := self.manager.SyncUserInfo(nil, jweToken); err != nil {
			errors.HandleInternalError(response, err)
			return
		}
	}
	response.WriteHeaderAndEntity(http.StatusOK, !token_is_invalid)
}

func (self *AuthHandler) handleCallbackLegacy(request *restful.Request, response *restful.Response) {
	var (
		authResponse *authApi.AuthResponse
		err          error
	)

	am := self.manager.(*manager)
	ctx := oidc.ClientContext(request.Request.Context(), am.HttpClient)

	switch request.Request.Method {
	case "GET":
		if errMsg := request.QueryParameter("error"); errMsg != "" {
			errors.HandleInternalError(response, er.New("errMsg"+request.QueryParameter("error_description")))
			return
		}
		code := request.QueryParameter("code")
		if code == "" {
			errors.HandleInternalError(response, er.New(fmt.Sprintf("no code in request")))
			return
		}

		if state := request.QueryParameter("state"); state != self.AppState {
			errors.HandleInternalError(response, er.New(fmt.Sprintf("expected state %q got %q", self.AppState, state)))
		}
		authResponse, err = self.manager.RetrieveToken(code, ctx)
		if err != nil {
			errors.HandleInternalError(response, err)
			return
		}

	case "POST":
		refreshToken := request.QueryParameter("refresh_token")
		authResponse, err = self.manager.Refresh(refreshToken, ctx)
		if err != nil {
			errors.HandleInternalError(response, err)
			return
		}
	default:
		errors.HandleInternalError(response, er.New(fmt.Sprintf("method not implemented: %s", request.Request.Method)))
		return
	}
	jweToken, err := authApi.ParseJWT(authResponse.IDToken)
	if err != nil {
		errors.HandleInternalError(response, err)
		return
	}
	authResponse.Email = jweToken.Email
	authResponse.Name = jweToken.Name
	authResponse.Groups = jweToken.Groups
	authResponse.IsAdmin = jweToken.Ext.IsAdmin

	if err := self.manager.SyncUserInfo(request, jweToken); err != nil {
		errors.HandleInternalError(response, err)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, authResponse)
}

func (self *AuthHandler) handleCallback(request *restful.Request, response *restful.Response) {
	var (
		authResponse *authApi.AuthResponse
		err          error
	)

	authSettings := self.manager.GetAuthSettings(nil)
	ctx := oidc.ClientContext(request.Request.Context(), self.manager.GetHTTPClient(nil))

	switch request.Request.Method {
	case http.MethodGet:
		// there are two cases to be considered:
		// 1. reponse_type code: needs to use authorization code to request the API one more time and fetch ID_TOKEN etc.
		// 2. response_type id_token: already returns the id_token and access_token
		switch authSettings.ResponseType {
		case "id_token":
			idToken := request.QueryParameter("id_token")
			// For some reason dex also returns the query parameters behind a shebang instead of
			// regular query parameters, se we will check this here
			if idToken == "" {
				url := request.Request.URL
				klog.Infof("id_token callback. raw url:", url.String())
				klog.Infof("id_token, raw query:", url.RawQuery)
				klog.Infof("id_token, raw path:", url.RawPath)
			}
			if idToken != "" {
				authResponse = &authApi.AuthResponse{
					IDToken: idToken,
				}
			}
		case "code":
			fallthrough
		default:
			if errMsg := request.QueryParameter("error"); errMsg != "" {
				errors.HandleInternalError(response, er.New("errMsg"+request.QueryParameter("error_description")))
				return
			}
			code := request.QueryParameter("code")
			if code == "" {
				errors.HandleInternalError(response, er.New(fmt.Sprintf("no code in request")))
				return
			}

			if state := request.QueryParameter("state"); state != self.AppState {
				errors.HandleInternalError(response, er.New(fmt.Sprintf("expected state %q got %q", self.AppState, state)))
			}
			authResponse, err = self.manager.RetrieveToken(code, ctx)
			if err != nil {
				errors.HandleInternalError(response, err)
				return
			}
		}

	case http.MethodPost:
		refreshToken := request.QueryParameter("refresh_token")
		authResponse, err = self.manager.Refresh(refreshToken, ctx)
		if err != nil {
			errors.HandleInternalError(response, err)
			return
		}
	default:
		errors.HandleInternalError(response, er.New(fmt.Sprintf("method not implemented: %s", request.Request.Method)))
		return
	}
	if authResponse == nil {
		errors.HandleInternalError(response, er.New(fmt.Sprintf("Request failed to parse callback data: %s", request.Request.URL)))
		return
	}
	jweToken, err := authApi.ParseJWT(authResponse.IDToken)
	if err != nil {
		errors.HandleInternalError(response, err)
		return
	}

	authResponse.Email = jweToken.Email
	authResponse.Name = jweToken.Name
	authResponse.Groups = jweToken.Groups
	authResponse.IsAdmin = jweToken.Ext.IsAdmin
	if err := self.manager.SyncUserInfo(request, jweToken); err != nil {
		errors.HandleInternalError(response, err)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, authResponse)
}

func (self AuthHandler) ListIDPS(request *restful.Request, response *restful.Response) {
	result := struct {
		List []string `json:"list"`
	}{
		List: authApi.SupportedIDPList,
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (self AuthHandler) SetIDPConfig(request *restful.Request, response *restful.Response) {
	idpName := request.PathParameter("idp_name")
	field, kind, badValue, err := idps.ValidateIDPConfig(request, idpName)
	if err != nil {
		errors.HandleInvalidError(response, field, kind, badValue, err)
		return
	}
	err = self.manager.SetIDPConfig(request, idpName)
	if err != nil {
		errors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, nil)
}

func (self AuthHandler) UpdateIDPConfig(request *restful.Request, response *restful.Response) {
	idpName := request.PathParameter("idp_name")
	field, kind, badValue, err := idps.ValidateIDPConfig(request, idpName)
	if err != nil {
		errors.HandleInvalidError(response, field, kind, badValue, err)
		return
	}
	err = self.manager.SetIDPConfig(request, idpName)
	if err != nil {
		errors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, nil)
}

func (self AuthHandler) GetIDPDetails(request *restful.Request, response *restful.Response) {
	result, err := self.manager.GetIDPConfig(request)
	if err != nil {
		errors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, authApi.CovertMap(result))
}

func (self AuthHandler) DeleteIDPConfig(request *restful.Request, response *restful.Response) {
	idpName := request.PathParameter("idp_name")
	err := self.manager.DeleteIDPConfig(request, idpName)
	if err != nil {
		errors.HandleInternalError(response, err)
		return
	}
	response.WriteHeader(http.StatusNoContent)
}

func (self AuthHandler) ListUsers(request *restful.Request, response *restful.Response) {
	users, err := self.manager.ListUsers(request)
	if err != nil {
		errors.HandleInternalError(response, err)
		return
	}

	result := struct {
		List []*authApi.User `json:"list"`
	}{}
	result.List = users

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func (self AuthHandler) GetUserDetails(request *restful.Request, response *restful.Response) {
	userName := request.PathParameter("user_name")
	user, err := self.manager.GetUserDetail(request, userName)
	if err != nil {
		errors.HandleInternalError(response, err)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, user)
}

func (self *AuthHandler) handleResourceCanI(request *restful.Request, response *restful.Response) {
	namespace := request.QueryParameter("namespace")
	verb := request.QueryParameter("verb")
	if strings.TrimSpace(verb) == "" {
		verb = "get"
	}
	resourceKind := request.PathParameter("resource")
	resourceName := request.QueryParameter("name")

	am := self.manager.(*manager)
	canI := am.clientManager.CanI(request, clientapi.ToSelfSubjectAccessReview(
		namespace,
		resourceName,
		strings.ToLower(resourceKind),
		verb,
	))

	response.WriteHeaderAndEntity(http.StatusOK, clientapi.CanIResponse{canI})
}

func (self *AuthHandler) handlePlatformViews(request *restful.Request, response *restful.Response) {
	var (
		resourceKind string
		resourceName string
	)
	redirect_url := request.QueryParameter("redirect_url")
	if len(strings.TrimSpace(redirect_url)) == 0 {
		errors.HandleInvalidError(response, "redirect_url", "auth", redirect_url,
			er.New("redirect_url is invalid"))
		return
	}

	platform := request.PathParameter("platform")
	resourceKind = authApi.CrdResourceKind
	switch platform {
	case "alaudak8s":
		resourceName = authApi.CrdAlaudaK8sName
	case "alaudadevops":
		resourceName = authApi.CrdAlaudaDevopsName
	default:
		errors.HandleInvalidError(response, "platform", "auth", platform,
			er.New("platform is invalid"))
		return
	}

	am := self.manager.(*manager)
	canI := am.clientManager.CanI(request, clientapi.ToSelfSubjectAccessReview(
		"",
		resourceName,
		strings.ToLower(resourceKind),
		"get",
	))

	if canI {
		http.Redirect(response, request.Request, redirect_url, http.StatusSeeOther)
		return
	}
	response.WriteHeaderAndEntity(http.StatusUnauthorized, clientapi.CanIResponse{canI})
}

// NewAuthHandler created AuthHandler instance.
func NewAuthHandler(manager authApi.Manager, appState string) *AuthHandler {
	return &AuthHandler{
		manager:  manager,
		AppState: appState,
	}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
