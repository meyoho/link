package auth

import (
	"github.com/emicklei/go-restful"
	"github.com/satori/go.uuid"
	"gopkg.in/yaml.v2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	authApi "link/src/backend/auth/api"
	"link/src/backend/auth/idps"
)

func (self manager) GetIDPConfig(request *restful.Request) (map[interface{}]interface{}, error) {
	result, err := self.getIDPConfigInfo(request)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (self manager) SetIDPConfig(request *restful.Request, idpName string) error {
	dexConfig, err := self.getDexConfigMap(request)
	if err != nil {
		return err
	}
	configData := dexConfig.Object["data"].(map[string]interface{})["config.yaml"]
	configDataMap := make(map[string]interface{})
	if err := yaml.Unmarshal([]byte(configData.(string)), configDataMap); err != nil {
		return err
	}
	if _, ok := configDataMap["connectors"]; ok {
		connectors := configDataMap["connectors"].([]interface{})
		self.ClearUserInfo(request, connectors)
	}

	newConfigData, err := idps.SetDexConnectors(idpName, configDataMap)
	if err != nil {
		return err
	}
	newConfigDataByte, err := yaml.Marshal(newConfigData)
	if err != nil {
		return err
	}

	dexConfig.Object["data"].(map[string]interface{})["config.yaml"] = string(newConfigDataByte)

	err = self.updateConfigMap(request, dexConfig)
	if err != nil {
		return err
	}
	err = self.updateDex(request)
	if err != nil {
		return err
	}

	return nil
}

func (self manager) DeleteIDPConfig(request *restful.Request, idpName string) error {
	dexConfig, err := self.getDexConfigMap(request)
	if err != nil {
		return err
	}
	configData := dexConfig.Object["data"].(map[string]interface{})["config.yaml"]

	configDataMap := make(map[string]interface{})
	yaml.Unmarshal([]byte(configData.(string)), configDataMap)
	if _, ok := configDataMap["connectors"]; !ok {
		return nil
	}

	connectors := configDataMap["connectors"].([]interface{})
	self.ClearUserInfo(request, connectors)

	delete(configDataMap, "connectors")

	newConfigData, err := yaml.Marshal(configDataMap)
	if err != nil {
		return nil
	}
	dexConfig.Object["data"].(map[string]interface{})["config.yaml"] = string(newConfigData)

	err = self.updateConfigMap(request, dexConfig)
	if err != nil {
		return err
	}
	err = self.updateDex(request)
	if err != nil {
		return err
	}

	return nil
}

func (self manager) getIDPConfigInfo(request *restful.Request) (map[interface{}]interface{}, error) {
	configDataMap := make(map[interface{}]interface{})

	dexConfig, err := self.getDexConfigMap(request)
	if err != nil {
		return nil, err
	}

	configData := dexConfig.Object["data"].(map[string]interface{})["config.yaml"]
	yaml.Unmarshal([]byte(configData.(string)), configDataMap)
	if _, ok := configDataMap["connectors"]; ok {
		connectors := configDataMap["connectors"].([]interface{})
		for _, connector := range connectors {
			return connector.(map[interface{}]interface{}), nil
		}
	}
	return nil, nil
}

func (self manager) updateConfigMap(request *restful.Request, dexConfigMap *unstructured.Unstructured) error {
	dyClient, err := self.clientManager.DynamicClient(request,
		&schema.GroupVersion{authApi.DexConfigMapGroup, authApi.DexConfigMapVersion})
	if err != nil {
		return err
	}
	configMap := schema.GroupVersionResource{
		Resource: authApi.ConfigMap,
		Group:    "",
		Version:  "v1",
	}

	_, err = dyClient.Resource(configMap).Namespace(self.Namespace).Update(dexConfigMap)
	if err != nil {
		return err
	}

	return nil
}

func (self manager) getDexConfigMap(request *restful.Request) (*unstructured.Unstructured, error) {
	dyClient, err := self.clientManager.DynamicClient(request,
		&schema.GroupVersion{authApi.DexConfigMapGroup, authApi.DexConfigMapVersion})
	if err != nil {
		return nil, err
	}

	configMap := schema.GroupVersionResource{
		Resource: authApi.ConfigMap,
		Group:    "",
		Version:  "v1",
	}

	dexConfig, err := dyClient.Resource(configMap).Namespace(self.Namespace).Get(authApi.DexConfigMapName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return dexConfig, nil
}

func (self manager) updateDex(request *restful.Request) error {
	dyClient, err := self.clientManager.DynamicClient(request,
		&schema.GroupVersion{authApi.DexDeploymentGroup, authApi.DexDeploymentVersion})
	if err != nil {
		return err
	}

	dexResource := schema.GroupVersionResource{
		Resource: authApi.Deployment,
		Group:    "apps",
		Version:  "v1",
	}

	dexDeployment, err := dyClient.Resource(dexResource).Namespace(self.Namespace).Get(authApi.DexDeploymentName, metav1.GetOptions{})
	if err != nil {
		return err
	}
	spec := dexDeployment.Object["spec"].(map[string]interface{})
	template := spec["template"].(map[string]interface{})
	metadata := template["metadata"].(map[string]interface{})
	labels := metadata["labels"].(map[string]interface{})
	labels["version"] = uuid.NewV4().String()

	_, err = dyClient.Resource(dexResource).Namespace(self.Namespace).Update(dexDeployment)

	return err
}
