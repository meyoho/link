package auth

import (
	"context"
	"errors"
	"fmt"
	"k8s.io/klog"
	authApi "link/src/backend/auth/api"
	clientapi "link/src/backend/client/api"
	settingsapi "link/src/backend/settings/api"
	"net/http"
	"strings"
	"time"

	"k8s.io/client-go/kubernetes"

	"github.com/coreos/go-oidc"
	"github.com/emicklei/go-restful"
	"golang.org/x/oauth2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

type AuthConfig struct {
	ClientID string
	// ClientSecret string
	// RedirectURI  string
	// IssuerURL    string
	Namespace string

	Verifier *oidc.IDTokenVerifier
	Provider *oidc.Provider
}

type manager struct {
	clientManager   clientapi.ClientManager
	settingsManager settingsapi.SettingsManager
	AuthConfig
	HttpClient *http.Client
	CAPath     string
	CACert     string
}

func (am *manager) getClient() (kubernetes.Interface, error) {
	return am.clientManager.Client(nil)
}

func (am *manager) isAuthEnabled(client kubernetes.Interface) (enabled bool, err error) {
	if client == nil {
		if client, err = am.getClient(); err != nil {
			return
		}
	}
	settings := am.settingsManager.GetAuthSettings(client)
	if settings != nil {
		enabled = settings.Enabled
	}
	return
}

func (am *manager) RetrieveToken(authCode string, ctx context.Context) (*authApi.AuthResponse, error) {
	var (
		err   error
		token *oauth2.Token
	)
	oauth2Config, _, err := am.GetOAuth2Config(nil)
	if err != nil {
		return nil, err
	}

	token, err = oauth2Config.Exchange(ctx, authCode)
	if err != nil {
		return nil, err
	}

	rawIDToken, ok := token.Extra("id_token").(string)
	if !ok {
		return nil, errors.New("no id_token in token response")
	}

	_, err = am.Verifier.Verify(ctx, rawIDToken)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Failed to verify ID token: %v", err))
	}

	authResponse := authApi.AuthResponse{
		IDToken:      rawIDToken,
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
	}
	return &authResponse, nil
}

func (am *manager) Refresh(refreshToken string, ctx context.Context) (*authApi.AuthResponse, error) {
	if strings.TrimSpace(refreshToken) == "" {
		return nil, errors.New("no refresh_token in request")
	}

	t := &oauth2.Token{
		RefreshToken: refreshToken,
		Expiry:       time.Now().Add(-time.Hour),
	}
	oauth2Config, _, err := am.GetOAuth2Config(nil)
	if err != nil {
		return nil, err
	}

	token, err := oauth2Config.TokenSource(ctx, t).Token()
	if err != nil {
		return nil, err
	}

	rawIDToken, ok := token.Extra("id_token").(string)
	if !ok {
		return nil, errors.New("no id_token in token response")
	}

	_, err = am.Verifier.Verify(ctx, rawIDToken)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Failed to verify ID token: %v", err))
	}

	authResponse := authApi.AuthResponse{
		IDToken:      rawIDToken,
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
	}
	return &authResponse, nil
}

func (am manager) GetScopes(settings *settingsapi.AuthSettings) []string {
	customScopes := []string{}
	if settings != nil && settings.Enabled && len(settings.Scopes) > 0 {
		customScopes = append(customScopes, settings.Scopes...)
	}
	return customScopes
}

func (am *manager) Oauth2Config(scopes []string, client kubernetes.Interface, authSettings *settingsapi.AuthSettings) (*oauth2.Config, []oauth2.AuthCodeOption, error) {
	var err error
	provider := am.Provider

	issuerInner := authSettings.IssuerInner
	issuerPubic := authSettings.Issuer

	if am.ClientID != authSettings.ClientID {
		httpClient := am.GetHTTPClient(client)
		ctx := oidc.ClientContext(context.Background(), httpClient)

		issuer := issuerInner
		if issuer == "" {
			issuer = issuerPubic
		}

		provider, err = oidc.NewProvider(ctx, issuer)
		if err != nil {
			return nil, nil, fmt.Errorf("Failed to query provider %q: %v", issuer, err)
		}
		am.ClientID = authSettings.ClientID
		am.Verifier = provider.Verifier(&oidc.Config{ClientID: authSettings.ClientID})
		am.Provider = provider
	}
	redirectURL := authSettings.RedirectURI
	options := []oauth2.AuthCodeOption{
		oauth2.AccessTypeOffline,
		oauth2.SetAuthURLParam("response_type", authSettings.ResponseType),
		oauth2.SetAuthURLParam("nonce", "rng"),
	}
	if len(authSettings.CustomRedirectURI) > 0 {
		if url := authSettings.CustomRedirectURI[settingsapi.SettingsCustomRedirectKey]; url != "" {
			redirectURL = url
		}
	}

	return &oauth2.Config{
		ClientID:     authSettings.ClientID,
		ClientSecret: authSettings.ClientSecret,
		Endpoint:     provider.Endpoint(),
		Scopes:       scopes,
		RedirectURL:  redirectURL,
	}, options, nil
}

// GetOAuth2Config returns the used oAuth2 configuration
func (am *manager) GetOAuth2Config(client kubernetes.Interface) (*oauth2.Config, []oauth2.AuthCodeOption, error) {
	var err error
	client, err = am.getK8sClient(client)
	if err != nil {
		return nil, nil, err
	}
	authSettings := am.settingsManager.GetAuthSettings(client)
	if authSettings == nil {
		return nil, nil, fmt.Errorf("Auth settings not available")
	}
	if !authSettings.Enabled {
		return nil, nil, fmt.Errorf("OIDC Auth is not enabled")
	}
	return am.Oauth2Config(am.GetScopes(authSettings), client, authSettings)
}

// GetHTTPClient returns a http client
func (am *manager) GetHTTPClient(client kubernetes.Interface) *http.Client {
	client, _ = am.getK8sClient(client)
	authSettings := am.settingsManager.GetAuthSettings(client)
	if authSettings == nil || !authSettings.Enabled || (authSettings.RootCAFile == "" && authSettings.RootCA == "") {
		am.HttpClient = http.DefaultClient
		am.HttpClient.Timeout = time.Second * 30
	} else if authSettings.RootCAFile != am.CAPath || authSettings.RootCA != am.CACert {
		var err error
		am.HttpClient, err = authApi.GetClient(authSettings.RootCAFile, authSettings.RootCA)
		if err != nil {
			am.HttpClient = http.DefaultClient
			klog.Errorf("Error getting HTTP client: %v", err)
		} else {
			am.CAPath = authSettings.RootCAFile
			am.CACert = authSettings.RootCA
		}
	}
	return am.HttpClient
}

func (am *manager) getK8sClient(client kubernetes.Interface) (kubernetes.Interface, error) {
	var err error
	if client == nil {
		client, err = am.getClient()
	}
	return client, err
}

func (am *manager) GetAuthSettings(client kubernetes.Interface) *settingsapi.AuthSettings {
	client, _ = am.getK8sClient(client)
	return am.settingsManager.GetAuthSettings(client)
}

func (self *manager) SyncUserInfo(request *restful.Request, jweToken *authApi.JWEToken) error {
	k8sClient, err := self.clientManager.Client(request)
	if err != nil {
		return err
	}

	userResource, err := authApi.GetApiResource(authApi.CrdUserKind, false, k8sClient)
	if err != nil {
		return err
	}

	payload := getUserPayload(jweToken)
	dynamicClient, err := self.clientManager.DynamicClient(request,
		&schema.GroupVersion{authApi.CrdUserGroup, authApi.CrdUserVersion})
	if err != nil {
		return err
	}

	userGvr := schema.GroupVersionResource{Resource: userResource.Name, Group: authApi.CrdUserGroup, Version: authApi.CrdUserVersion}
	userInfo, err := dynamicClient.Resource(userGvr).Namespace("").Get(jweToken.MetadataName, metav1.GetOptions{})
	if err != nil {
		_, err = dynamicClient.Resource(userGvr).Namespace("").Create(payload)
	} else {
		userInfo.Object["spec"] = payload.Object["spec"]
		_, err = dynamicClient.Resource(userGvr).Namespace("").Update(userInfo)
	}

	return err
}

func (self *manager) ClearUserInfo(request *restful.Request, connectors []interface{}) error {
	k8sClient, err := self.clientManager.Client(request)
	if err != nil {
		return err
	}

	userResource, err := authApi.GetApiResource(authApi.CrdUserKind, false, k8sClient)
	if err != nil {
		return err
	}
	userGvr := schema.GroupVersionResource{Resource: userResource.Name, Group: authApi.CrdUserGroup, Version: authApi.CrdUserVersion}
	dynamicClient, err := self.clientManager.DynamicClient(request,
		&schema.GroupVersion{authApi.CrdUserGroup, authApi.CrdUserVersion})
	if err != nil {
		return err
	}

	for _, connector := range connectors {
		idpID := connector.(map[interface{}]interface{})["id"].(string)
		labelSelector := fmt.Sprintf("type=%s", idpID)
		go dynamicClient.Resource(userGvr).Namespace("").DeleteCollection(&metav1.DeleteOptions{}, metav1.ListOptions{LabelSelector: labelSelector})
	}

	return nil
}

func getUserPayload(jweToken *authApi.JWEToken) *unstructured.Unstructured {
	userInfo := make(map[string]interface{})
	userInfo["apiVersion"] = fmt.Sprintf("%s/%s", authApi.CrdUserGroup, authApi.CrdUserVersion)
	userInfo["kind"] = authApi.CrdUserKind

	metadata := make(map[string]interface{})

	metadata["name"] = jweToken.MetadataName
	userInfo["metadata"] = metadata

	labels := make(map[string]interface{})
	labels["type"] = jweToken.Ext.ConnID
	metadata["labels"] = labels

	spec := make(map[string]interface{})
	spec["email"] = jweToken.Email
	spec["name"] = jweToken.Name
	spec["groups"] = jweToken.Groups
	spec["is_admin"] = jweToken.Ext.IsAdmin
	userInfo["spec"] = spec

	return &unstructured.Unstructured{
		Object: userInfo,
	}
}

// NewAuthManager creates auth manager.
func NewAuthManager(
	clientManager clientapi.ClientManager,
	settingsManager settingsapi.SettingsManager,
	client *http.Client) *manager {
	return &manager{
		clientManager:   clientManager,
		settingsManager: settingsManager,
		AuthConfig:      AuthConfig{},
		HttpClient:      client,
	}
}
