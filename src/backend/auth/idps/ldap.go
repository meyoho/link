package idps

import (
	"crypto/x509"
	"fmt"
	"github.com/json-iterator/go"
	"strings"
)

type LDAPConnectors struct {
	IDPCommonConfig
	Config ldapConfig `json:"config"`
}

type ldapConfig struct {
	Host          string `json:"host"`
	InsecureNoSSL bool   `json:"insecureNoSSL"`
	BindDN        string `json:"bindDN"`
	BindPW        string `json:"bindPW"`
	//// Path to a trusted root certificate file.
	//RootCA string `json:"rootCA"`
	// Base64 encoded PEM data containing root CAs.
	RootCAData     []byte      `json:"rootCAData"`
	UsernamePrompt string      `json:"usernamePrompt"`
	UserSearch     userSearch  `json:"userSearch"`
	GroupSearch    groupSearch `json:"groupSearch"`
}

type userSearch struct {
	BaseDN    string `json:"baseDN"`
	Filter    string `json:"filter"`
	Username  string `json:"username"`
	Scope     string `json:"scope"`
	IDAttr    string `json:"idAttr"`
	EmailAttr string `json:"emailAttr"`
	NameAttr  string `json:"nameAttr"`
}

type groupSearch struct {
	BaseDN    string `json:"baseDN"`
	Filter    string `json:"filter"`
	Scope     string `json:"scope"` // Defaults to "sub"
	UserAttr  string `json:"userAttr"`
	GroupAttr string `json:"groupAttr"`
	NameAttr  string `json:"nameAttr"`
}

var ldapConnectors = LDAPConnectors{}

func getLDAPConnectors() (map[string]interface{}, error) {
	connectors := make(map[string]interface{})
	ldapConnectorsByte, err := jsoniter.ConfigCompatibleWithStandardLibrary.Marshal(&ldapConnectors)
	if err != nil {
		return nil, err
	}
	err = jsoniter.ConfigCompatibleWithStandardLibrary.Unmarshal(ldapConnectorsByte, &connectors)
	if err != nil {
		return nil, err
	}

	return connectors, nil
}

func validateLDAPConfig(data LDAPConnectors) (field, kind, badValue string, err error) {

	requiredFields := []struct {
		name string
		val  string
	}{
		{"type", data.Type},
		{"id", data.ID},
		{"name", data.Name},
		{"host", data.Config.Host},
		{"userSearch.baseDN", data.Config.UserSearch.BaseDN},
		{"userSearch.username", data.Config.UserSearch.Username},
	}

	for _, field := range requiredFields {
		if strings.TrimSpace(field.val) == "" {
			return field.name, LDAP, field.val, fmt.Errorf("ldap: missing required field %q", field.name)
		}
	}

	if len(data.Config.RootCAData) != 0 {
		data := data.Config.RootCAData

		rootCAs := x509.NewCertPool()
		if !rootCAs.AppendCertsFromPEM(data) {
			return "rootCAData", LDAP, string(data),
				fmt.Errorf("ldap: no certs found in ca file")
		}
	}

	ok := parseScope(data.Config.UserSearch.Scope)
	if !ok {
		return "UserSearch.Scope", LDAP, data.Config.UserSearch.Scope,
			fmt.Errorf("userSearch.Scope unknown value %q", data.Config.UserSearch.Scope)
	}
	ok = parseScope(data.Config.GroupSearch.Scope)
	if !ok {
		return "GroupSearch.Scope", LDAP, data.Config.GroupSearch.Scope,
			fmt.Errorf("groupSearch.Scope unknown value %q", data.Config.GroupSearch.Scope)
	}

	ldapConnectors = data
	return
}

func parseScope(s string) bool {
	switch s {
	case "", "sub":
		return true
	case "one":
		return true
	}
	return false
}
