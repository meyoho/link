package idps

import (
	"context"
	"errors"
	"fmt"
	"github.com/coreos/go-oidc"
	"github.com/json-iterator/go"
	"github.com/spf13/viper"
	"k8s.io/klog"
	"strings"
)

type OIDCConnectors struct {
	IDPCommonConfig
	Config oidcConfig `json:"config"`
}

type oidcConfig struct {
	Issuer       string `json:"issuer"`
	ClientID     string `json:"clientID"`
	ClientSecret string `json:"clientSecret"`
	RedirectURI  string `json:"redirectURI"`
}

var oidcConnectors = OIDCConnectors{}

func getOIDCConnectors() (map[string]interface{}, error) {
	connectors := make(map[string]interface{})
	oidcConnectorsByte, err := jsoniter.ConfigCompatibleWithStandardLibrary.Marshal(&oidcConnectors)
	if err != nil {
		return nil, err
	}
	err = jsoniter.ConfigCompatibleWithStandardLibrary.Unmarshal(oidcConnectorsByte, &connectors)
	if err != nil {
		return nil, err
	}

	return connectors, nil
}

func validateOIDCConfig(data OIDCConnectors) (field, kind, badValue string, err error) {

	requiredFields := []struct {
		name string
		val  string
	}{
		{"type", data.Type},
		{"id", data.ID},
		{"name", data.Name},
		{"issuer", data.Config.Issuer},
		{"clientID", data.Config.ClientID},
		{"clientSecret", data.Config.ClientSecret},
	}

	for _, field := range requiredFields {
		if strings.TrimSpace(field.val) == "" {
			return field.name, OIDC, field.val, fmt.Errorf("oidc: missing required field %q", field.name)
		}
	}

	if strings.TrimSpace(data.Config.RedirectURI) == "" {
		data.Config.RedirectURI = viper.GetString("DEX_REDIRECT_URI")
	}

	ctx, cancel := context.WithCancel(context.Background())
	_, err = oidc.NewProvider(ctx, data.Config.Issuer)
	if err != nil {
		cancel()
		klog.Errorf("oidc config err %v", err)
		return "issuer", OIDC, data.Config.Issuer, errors.New("issuer is invalid")
	}

	oidcConnectors = data
	return
}
