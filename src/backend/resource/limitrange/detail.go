// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package limitrange

import (
	"k8s.io/api/core/v1"
	"link/src/backend/api"
)

type Detail struct {
	ObjectMeta api.ObjectMeta      `json:"objectMeta"`
	TypeMeta   api.TypeMeta        `json:"typeMeta"`
	Limits     []v1.LimitRangeItem `json:"limits"`
}

func ToDetail(limitRange *v1.LimitRange) Detail {
	return Detail{
		ObjectMeta: api.NewObjectMeta(limitRange.ObjectMeta),
		TypeMeta:   api.NewTypeMetaWithApiVersion(api.ResourceKindLimitRange, "v1"),
		Limits:     limitRange.Spec.Limits,
	}
}

func ComputeEffectiveLimitRange(lrs []v1.LimitRange) []v1.LimitRangeItem {
	lrMap := make(map[v1.LimitType]v1.LimitRangeItem)
	for _, lr := range lrs {
		for _, lri := range lr.Spec.Limits {
			if _, ok := lrMap[lri.Type]; !ok {
				lrMap[lri.Type] = lri
			} else {
				lrMap[lri.Type] = mergeLimitRangeItem(lrMap[lri.Type], lri)
			}
		}
	}

	result := make([]v1.LimitRangeItem, 0, len(lrMap))
	for _, lri := range lrMap {
		result = append(result, lri)
	}
	return result
}

func mergeLimitRangeItem(lri1 v1.LimitRangeItem, lri2 v1.LimitRangeItem) v1.LimitRangeItem {
	result := lri1.DeepCopy()

	if result.Max == nil {
		result.Max = make(v1.ResourceList)
	}
	for name, quantity := range lri2.Max {
		if _, ok := result.Max[name]; !ok {
			result.Max[name] = quantity
		} else {
			if quantity.Cmp(result.Max[name]) == -1 {
				result.Max[name] = quantity
			}
		}
	}

	if result.Min == nil {
		result.Min = make(v1.ResourceList)
	}
	for name, quantity := range lri2.Min {
		if _, ok := result.Min[name]; !ok {
			result.Min[name] = quantity
		} else {
			if quantity.Cmp(result.Min[name]) == 1 {
				result.Min[name] = quantity
			}
		}
	}

	if result.Default == nil {
		result.Default = make(v1.ResourceList)
	}
	for name, quantity := range lri2.Default {
		if _, ok := result.Default[name]; !ok {
			result.Default[name] = quantity
		} else {
			if quantity.Cmp(result.Default[name]) == -1 {
				result.Default[name] = quantity
			}
		}
	}

	if result.DefaultRequest == nil {
		result.DefaultRequest = make(v1.ResourceList)
	}
	for name, quantity := range lri2.DefaultRequest {
		if _, ok := result.DefaultRequest[name]; !ok {
			result.DefaultRequest[name] = quantity
		} else {
			if quantity.Cmp(result.DefaultRequest[name]) == -1 {
				result.DefaultRequest[name] = quantity
			}
		}
	}

	if result.MaxLimitRequestRatio == nil {
		result.MaxLimitRequestRatio = make(v1.ResourceList)
	}
	for name, quantity := range lri2.MaxLimitRequestRatio {
		if _, ok := result.MaxLimitRequestRatio[name]; !ok {
			result.MaxLimitRequestRatio[name] = quantity
		} else {
			if quantity.Cmp(result.MaxLimitRequestRatio[name]) == -1 {
				result.MaxLimitRequestRatio[name] = quantity
			}
		}
	}

	return *result
}
