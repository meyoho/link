// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	api "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"strings"
)

type PodInfoItem struct {
	Name     string  `json:"name"`
	Status   string  `json:"status"`
	Warnings []Event `json:"warnings"`
}

type PodControllerInfo struct {
	Warnings []Event       `json:"warnings"`
	Pods     []PodInfoItem `json:"pods"`

	// Number of pods that are created.
	Current int32 `json:"current"`

	// Number of pods that are desired.
	Desired int32 `json:"desired"`
}

func getPodInfoItem(pod api.Pod, events []api.Event) PodInfoItem {
	status := GetPodAggregatedStatus(&pod)
	warnings := make([]Event, 0)
	if status == FailedStatus {
		for _, event := range events {
			if event.InvolvedObject.UID == pod.UID && event.Type == api.EventTypeWarning {
				warnings = append(warnings, Event{
					Message: event.Message,
					Reason:  event.Reason,
					Type:    event.Type,
				})
			}
		}
	}

	return PodInfoItem{Name: pod.Name, Status: string(status), Warnings: warnings}
}

type AggregatedStatus string

const (
	CompletedStatus AggregatedStatus = "Completed"
	RunningStatus   AggregatedStatus = "Running"
	PendingStatus   AggregatedStatus = "Pending"
	FailedStatus    AggregatedStatus = "Failed"
	StoppedStatus   AggregatedStatus = "Stopped"
)

func GetPodAggregatedStatus(pod *api.Pod) AggregatedStatus {
	status := GetPodStatus(pod)
	if strings.Split(status, ":")[0] == Initing {
		return PendingStatus
	}

	switch status {
	case Completed:
		return CompletedStatus
	case Running:
		return RunningStatus
	case Initing, Pending, PodInitializing, ContainerCreating, Terminating:
		return PendingStatus
	}
	return FailedStatus
}

func GetPodControllerInfo(current int32, desired *int32, controller metav1.Object, pods []api.Pod, events []api.Event) PodControllerInfo {
	result := PodControllerInfo{
		Current:  current,
		Warnings: make([]Event, 0),
		Pods:     make([]PodInfoItem, 0, len(pods)),
	}
	if desired == nil {
		result.Desired = 0
	} else {
		result.Desired = *desired
	}
	for _, event := range events {
		if event.InvolvedObject.UID == controller.GetUID() && event.Type == api.EventTypeWarning {
			result.Warnings = append(result.Warnings, Event{
				Message: event.Message,
				Reason:  event.Reason,
				Type:    event.Type,
			})
		}
	}

	for _, pod := range pods {
		result.Pods = append(result.Pods, getPodInfoItem(pod, events))
	}

	return result
}
