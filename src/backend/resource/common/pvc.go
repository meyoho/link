package common

import (
	"k8s.io/api/core/v1"
	"link/src/backend/api"
)

type PersistentVolumeClaim struct {
	ObjectMeta   api.ObjectMeta                  `json:"objectMeta"`
	TypeMeta     api.TypeMeta                    `json:"typeMeta"`
	Status       string                          `json:"status"`
	Volume       string                          `json:"volume"`
	Capacity     v1.ResourceList                 `json:"capacity"`
	AccessModes  []v1.PersistentVolumeAccessMode `json:"accessModes"`
	StorageClass *string                         `json:"storageClass"`
}

type PersistentVolumeClaimList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of services.
	Items []PersistentVolumeClaim `json:"items"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

func filterPersistentVolumeClaimsByNames(pvcs []v1.PersistentVolumeClaim, names []string) []v1.PersistentVolumeClaim {
	result := make([]v1.PersistentVolumeClaim, 0, len(names))
	for _, pvc := range pvcs {
		for _, name := range names {
			if pvc.Name == name {
				result = append(result, pvc)
			}
		}
	}
	return result
}

func toPersistentVolumeClaim(pvc v1.PersistentVolumeClaim) PersistentVolumeClaim {
	return PersistentVolumeClaim{
		ObjectMeta:   api.NewObjectMeta(pvc.ObjectMeta),
		TypeMeta:     api.NewTypeMetaWithApiVersion(api.ResourceKindPersistentVolumeClaim, "v1"),
		Status:       string(pvc.Status.Phase),
		Volume:       pvc.Spec.VolumeName,
		Capacity:     pvc.Spec.Resources.Requests,
		AccessModes:  pvc.Spec.AccessModes,
		StorageClass: pvc.Spec.StorageClassName,
	}
}

func CreatePersistentVolumeClaimList(pvcs []v1.PersistentVolumeClaim, names []string) *PersistentVolumeClaimList {
	filteredPVCs := filterPersistentVolumeClaimsByNames(pvcs, names)
	pvcList := &PersistentVolumeClaimList{
		Items:    make([]PersistentVolumeClaim, 0, len(filteredPVCs)),
		ListMeta: api.ListMeta{TotalItems: len(filteredPVCs)},
	}

	for _, pvc := range filteredPVCs {
		pvcList.Items = append(pvcList.Items, toPersistentVolumeClaim(pvc))
	}
	return pvcList
}
