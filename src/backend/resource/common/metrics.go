package common

import (
	"k8s.io/api/core/v1"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
)

type Metrics struct {
	CPU    int64 `json:"cpu"`
	Memory int64 `json:"memory"`
}

func getPodMetrics(metrics *v1beta1.PodMetrics) *Metrics {
	if metrics == nil {
		return nil
	}
	m := Metrics{}
	for _, c := range metrics.Containers {
		memory := c.Usage[v1.ResourceMemory]
		m.Memory += (&memory).Value()
		cpu := c.Usage[v1.ResourceCPU]
		m.CPU += (&cpu).MilliValue()
	}
	return &m
}

func GetPodsMetrics(metrics []v1beta1.PodMetrics, namespace string, pods []PodInfoItem) *Metrics {
	match := false
	result := Metrics{}
	for _, pod := range pods {
		for _, m := range metrics {
			if m.Name == pod.Name && m.Namespace == namespace {
				match = true
				pm := getPodMetrics(&m)
				result.CPU += pm.CPU
				result.Memory += pm.Memory
				break
			}
		}
	}

	if !match {
		return nil
	}

	return &result
}
