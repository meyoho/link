// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package replicaset

import (
	"k8s.io/klog"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
	"link/src/backend/resource/pod"

	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sClient "k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
	"link/src/backend/api"
)

// GetReplicaSetPods return list of pods targeting replica set.
func GetReplicaSetPods(client k8sClient.Interface, mclient clientset.Interface, dsQuery *dataselect.DataSelectQuery, petSetName, namespace string) (*pod.PodList, error) {
	pods, err := getRawReplicaSetPods(client, petSetName, namespace)
	if err != nil {
		return pod.EmptyPodList, err
	}

	events, err := event.GetPodsEvents(client, namespace, pods)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	metricsList, err := mclient.MetricsV1beta1().PodMetricses(namespace).List(api.ListEverything)
	if err != nil {
		klog.Errorf("list metrics failed %v", err)
	}

	podList := pod.ToPodList(pods, events, metricsList.Items, nonCriticalErrors, dsQuery)
	return &podList, nil
}

func getRawReplicaSetPods(client k8sClient.Interface, petSetName, namespace string) ([]v1.Pod, error) {
	rs, err := client.AppsV1().ReplicaSets(namespace).Get(petSetName, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	channels := &common.ResourceChannels{
		PodList: common.GetPodListChannel(client, common.NewSameNamespaceQuery(namespace), 1),
	}

	podList := <-channels.PodList.List
	if err := <-channels.PodList.Error; err != nil {
		return nil, err
	}

	return common.FilterPodsByControllerRef(rs, podList.Items), nil
}
