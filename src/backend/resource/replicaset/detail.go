// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package replicaset

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	ds "link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"

	apps "k8s.io/api/apps/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	k8sClient "k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// ReplicaSetDetail is a presentation layer view of Kubernetes Replica Set resource. This means
// it is Replica Set plus additional augmented data we can get from other sources
// (like services that target the same pods).
type ReplicaSetDetail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Aggregate information about pods belonging to this Replica Set.
	PodControllerInfo common.PodControllerInfo `json:"podControllerInfo"`

	// Detailed information about service related to Replica Set.
	ServiceList common.ServiceList `json:"serviceList"`

	// Container images of the Replica Set.
	ContainerImages []string `json:"containerImages"`

	// Init Container images of the Replica Set.
	InitContainerImages []string `json:"initContainerImages"`

	// List of events related to this Replica Set.
	EventList common.EventList `json:"eventList"`

	// Selector of this replica set.
	Selector *metaV1.LabelSelector `json:"selector"`

	Data *apps.ReplicaSet `json:"data"`

	Metrics *common.Metrics `json:"metrics"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetReplicaSetDetail gets replica set details.
func GetReplicaSetDetail(client k8sClient.Interface, mclient clientset.Interface, namespace, name string) (*ReplicaSetDetail, error) {
	rs, err := client.AppsV1().ReplicaSets(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	channels := &common.ResourceChannels{
		EventList:      common.GetEventListChannel(client, common.NewSameNamespaceQuery(namespace), 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient, common.NewSameNamespaceQuery(namespace), 1),
	}

	eventList, err := event.GetResourceEvents(client, ds.DefaultDataSelect, rs.Namespace, rs.Name)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	matchingPods, err := getRawReplicaSetPods(client, name, namespace)
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	rawEvents := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	podControllerInfo := common.GetPodControllerInfo(rs.Status.Replicas,
		rs.Spec.Replicas, rs.GetObjectMeta(), matchingPods, rawEvents.Items)
	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	m := common.GetPodsMetrics(metrics.Items, namespace, podControllerInfo.Pods)

	serviceList, err := GetReplicaSetServices(client, ds.DefaultDataSelect, namespace, name)
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	rsDetail := toReplicaSetDetail(rs, *eventList, podControllerInfo, *serviceList, nonCriticalErrors)
	rsDetail.Metrics = m

	return &rsDetail, nil
}

func toReplicaSetDetail(replicaSet *apps.ReplicaSet, eventList common.EventList, podControllerInfo common.PodControllerInfo, serviceList common.ServiceList, nonCriticalErrors []error) ReplicaSetDetail {

	replicaSet.SetGroupVersionKind(schema.GroupVersionKind{Group: "apps", Version: "v1", Kind: api.ResourceKindReplicaSet})
	return ReplicaSetDetail{
		ObjectMeta:          api.NewObjectMeta(replicaSet.ObjectMeta),
		TypeMeta:            api.NewTypeMetaWithApiVersion(api.ResourceKindReplicaSet, "apps/v1"),
		ContainerImages:     common.GetContainerImages(&replicaSet.Spec.Template.Spec),
		InitContainerImages: common.GetInitContainerImages(&replicaSet.Spec.Template.Spec),
		Selector:            replicaSet.Spec.Selector,
		PodControllerInfo:   podControllerInfo,
		ServiceList:         serviceList,
		EventList:           eventList,
		Data:                replicaSet,
		Errors:              nonCriticalErrors,
	}
}
