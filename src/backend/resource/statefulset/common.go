// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package statefulset

import (
	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
)

// The code below allows to perform complex data section on []apps.StatefulSet

type StatefulSetCell struct {
	StatefulSet apps.StatefulSet
	Pods        []v1.Pod
	Events      []v1.Event
}

func (self StatefulSetCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case dataselect.NameProperty:
		return dataselect.StdComparableString(self.StatefulSet.ObjectMeta.Name)
	case dataselect.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.StatefulSet.ObjectMeta.CreationTimestamp.Time)
	case dataselect.NamespaceProperty:
		return dataselect.StdComparableString(self.StatefulSet.ObjectMeta.Namespace)
	case dataselect.StatusProperty:
		return dataselect.StdComparableString(GetStatefulSetStatus(self.StatefulSet, self.Pods, self.Events))
	default:
		// if name is not supported then just return a constant dummy value, sort will have no effect.
		return nil
	}
}

func toCells(std []apps.StatefulSet, pods []v1.Pod, events []v1.Event) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = StatefulSetCell{StatefulSet: std[i], Pods: pods, Events: events}
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []apps.StatefulSet {
	std := make([]apps.StatefulSet, len(cells))
	for i := range std {
		std[i] = apps.StatefulSet(cells[i].(StatefulSetCell).StatefulSet)
	}
	return std
}

func GetStatefulSetStatus(statefulSet apps.StatefulSet, pods []v1.Pod, events []v1.Event) common.AggregatedStatus {
	warnEvents := []v1.Event{}
	for _, event := range events {
		if event.Type == v1.EventTypeWarning && (event.InvolvedObject.Kind == "StatefulSet" || event.InvolvedObject.Kind == "Pod") {
			warnEvents = append(warnEvents, event)
		}
	}

	if statefulSet.Status.ReadyReplicas == *statefulSet.Spec.Replicas && statefulSet.Status.ReadyReplicas == statefulSet.Status.Replicas {
		if *statefulSet.Spec.Replicas == 0 {
			return common.StoppedStatus
		}
		return common.RunningStatus
	}

	if common.HasWarnEvent(statefulSet.UID, warnEvents) {
		return common.FailedStatus
	}

	statefulSetPods := common.FilterPodsByControllerRef(statefulSet.GetObjectMeta(), pods)
	for _, pod := range statefulSetPods {
		if common.GetPodAggregatedStatus(&pod) == common.FailedStatus {
			return common.FailedStatus
		}
	}

	return common.PendingStatus
}
