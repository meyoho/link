// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package statefulset

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	ds "link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"

	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

type RollingUpdateStrategy struct {
	Partition int32 `json:"partition"`
}

// StatefulSetDetail is a presentation layer view of Kubernetes Stateful Set resource. This means it is Stateful
// Set plus additional augmented data we can get from other sources (like services that target the same pods).
type StatefulSetDetail struct {
	ObjectMeta                api.ObjectMeta                     `json:"objectMeta"`
	Selector                  map[string]string                  `json:"selector"`
	Strategy                  apps.StatefulSetUpdateStrategyType `json:"strategy"`
	RollingUpdateStrategy     RollingUpdateStrategy              `json:"rollingUpdateStrategy"`
	TypeMeta                  api.TypeMeta                       `json:"typeMeta"`
	PodControllerInfo         common.PodControllerInfo           `json:"podControllerInfo"`
	ContainerImages           []string                           `json:"containerImages"`
	RevisionHistoryLimit      *int32                             `json:"revisionHistoryLimit"`
	EventList                 common.EventList                   `json:"eventList"`
	PersistentvolumeclaimList *common.PersistentVolumeClaimList  `json:"persistentVolumeClaimList"`
	ServiceList               common.ServiceList                 `json:"serviceList"`
	Metrics                   *common.Metrics                    `json:"metrics"`
	Data                      *apps.StatefulSet                  `json:"data"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetStatefulSetDetail gets Stateful Set details.
func GetStatefulSetDetail(client kubernetes.Interface, mclient clientset.Interface, namespace,
	name string) (*StatefulSetDetail, error) {
	ss, err := client.AppsV1().StatefulSets(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	selector, err := metaV1.LabelSelectorAsSelector(ss.Spec.Selector)
	if err != nil {
		return nil, err
	}
	options := metaV1.ListOptions{LabelSelector: selector.String()}

	channels := &common.ResourceChannels{
		PodList: common.GetPodListChannelWithOptions(client,
			common.NewSameNamespaceQuery(namespace), options, 1),
		EventList: common.GetEventListChannelWithUid(client,
			common.NewSameNamespaceQuery(namespace), ss.UID, 1),
		ServiceList: common.GetServiceListChannel(client,
			common.NewSameNamespaceQuery(namespace), 1),
		PersistentVolumeClaimList: common.GetPersistentVolumeClaimListChannel(client,
			common.NewSameNamespaceQuery(namespace), 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient,
			common.NewSameNamespaceQuery(namespace), 1),
	}

	events := <-channels.EventList.List
	err = <-channels.EventList.Error
	if events == nil {
		events = &v1.EventList{}
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	serviceList, err := GetStatefulSetServices(channels.ServiceList, ds.DefaultDataSelect, ss)
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	pods := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	pvcList := <-channels.PersistentVolumeClaimList.List
	err = <-channels.PersistentVolumeClaimList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	matchingPods := common.FilterPodsByControllerRef(ss, pods.Items)
	pvcNames := getPVCNames(matchingPods)
	pl := common.CreatePersistentVolumeClaimList(pvcList.Items, pvcNames)
	podControllerInfo := common.GetPodControllerInfo(ss.Status.Replicas, ss.Spec.Replicas, ss.GetObjectMeta(), matchingPods, events.Items)

	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	m := common.GetPodsMetrics(metrics.Items, namespace, podControllerInfo.Pods)

	ssDetail := getStatefulSetDetail(ss, events, podControllerInfo, m, *serviceList, pl, nonCriticalErrors)
	return &ssDetail, nil
}

func getPVCNames(pods []v1.Pod) []string {
	pvcNames := make([]string, 0, len(pods))
	for _, pod := range pods {
		for _, volume := range pod.Spec.Volumes {
			if volume.PersistentVolumeClaim != nil {
				pvcNames = append(pvcNames, volume.PersistentVolumeClaim.ClaimName)
			}
		}
	}
	return pvcNames
}

func getStatefulSetDetail(statefulSet *apps.StatefulSet, eventList *v1.EventList,
	podControllerInfo common.PodControllerInfo, metrics *common.Metrics, serviceList common.ServiceList, pvcList *common.PersistentVolumeClaimList, nonCriticalErrors []error) StatefulSetDetail {

	var updateStrategy RollingUpdateStrategy
	if statefulSet.Spec.UpdateStrategy.RollingUpdate != nil {
		updateStrategy.Partition = *statefulSet.Spec.UpdateStrategy.RollingUpdate.Partition
	}
	statefulSet.SetGroupVersionKind(schema.GroupVersionKind{Group: "apps", Version: "v1", Kind: api.ResourceKindStatefulSet})

	return StatefulSetDetail{
		ObjectMeta:                api.NewObjectMeta(statefulSet.ObjectMeta),
		Selector:                  statefulSet.Spec.Selector.MatchLabels,
		TypeMeta:                  api.NewTypeMetaWithApiVersion(api.ResourceKindStatefulSet, "apps/v1"),
		ContainerImages:           common.GetContainerImages(&statefulSet.Spec.Template.Spec),
		Strategy:                  statefulSet.Spec.UpdateStrategy.Type,
		RollingUpdateStrategy:     updateStrategy,
		PodControllerInfo:         podControllerInfo,
		RevisionHistoryLimit:      statefulSet.Spec.RevisionHistoryLimit,
		EventList:                 event.CreateEventList(eventList.Items, ds.DefaultEventSelect),
		Data:                      statefulSet,
		ServiceList:               serviceList,
		Metrics:                   metrics,
		PersistentvolumeclaimList: pvcList,
		Errors: nonCriticalErrors,
	}
}
