// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package statefulset

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// StatefulSetList contains a list of Stateful Sets in the cluster.
type StatefulSetList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Pet Sets.
	StatefulSets []StatefulSet `json:"statefulSets"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// StatefulSet is a presentation layer view of Kubernetes Stateful Set resource. This means it is
// Stateful Set plus additional augmented data we can get from other sources (like services that
// target the same pods).
type StatefulSet struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Aggregate information about pods belonging to this Stateful Set.
	PodControllerInfo common.PodControllerInfo `json:"podControllerInfo"`

	Metrics *common.Metrics `json:"metrics"`

	// Container images of the Stateful Set.
	ContainerImages []string `json:"containerImages"`

	// Init container images of the Stateful Set.
	InitContainerImages []string `json:"initContainerImages"`
}

// GetStatefulSetList returns a list of all Stateful Sets in the cluster.
func GetStatefulSetList(client kubernetes.Interface, mclient clientset.Interface, nsQuery *common.NamespaceQuery,
	dsQuery *dataselect.DataSelectQuery) (*StatefulSetList, error) {

	channels := &common.ResourceChannels{
		StatefulSetList: common.GetStatefulSetListChannel(client, nsQuery, 1),
		PodList:         common.GetPodListChannel(client, nsQuery, 1),
		EventList:       common.GetEventListChannel(client, nsQuery, 1),
		PodMetricsList:  common.GetPodMetricsListChannel(mclient, nsQuery, 1),
	}

	return GetStatefulSetListFromChannels(channels, dsQuery)
}

// GetStatefulSetListFromChannels returns a list of all Stateful Sets in the cluster reading
// required resource list once from the channels.
func GetStatefulSetListFromChannels(channels *common.ResourceChannels, dsQuery *dataselect.DataSelectQuery) (*StatefulSetList, error) {
	statefulSets := <-channels.StatefulSetList.List
	err := <-channels.StatefulSetList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	pods := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	events := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	ssList := toStatefulSetList(statefulSets.Items, pods.Items, events.Items, metrics.Items, nonCriticalErrors, dsQuery)
	return ssList, nil
}

func toStatefulSetList(statefulSets []apps.StatefulSet, pods []v1.Pod, events []v1.Event, metrics []v1beta1.PodMetrics, nonCriticalErrors []error,
	dsQuery *dataselect.DataSelectQuery) *StatefulSetList {

	statefulSetList := &StatefulSetList{
		StatefulSets: make([]StatefulSet, 0),
		ListMeta:     api.ListMeta{TotalItems: len(statefulSets)},
		Errors:       nonCriticalErrors,
	}

	ssCells, filteredTotal := dataselect.GenericDataSelectWithFilter(
		toCells(statefulSets, pods, events), dsQuery)
	statefulSets = fromCells(ssCells)
	statefulSetList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for _, statefulSet := range statefulSets {
		matchingPods := common.FilterPodsByControllerRef(&statefulSet, pods)
		statefulSetList.StatefulSets = append(statefulSetList.StatefulSets, toStatefulSet(&statefulSet, matchingPods, events, metrics))
	}

	return statefulSetList
}

func toStatefulSet(statefulSet *apps.StatefulSet, pods []v1.Pod, events []v1.Event, metrics []v1beta1.PodMetrics) StatefulSet {
	pci := common.GetPodControllerInfo(statefulSet.Status.Replicas, statefulSet.Spec.Replicas, statefulSet.GetObjectMeta(), pods, events)
	m := common.GetPodsMetrics(metrics, statefulSet.Namespace, pci.Pods)
	return StatefulSet{
		ObjectMeta:          api.NewObjectMeta(statefulSet.ObjectMeta),
		TypeMeta:            api.NewTypeMetaWithApiVersion(api.ResourceKindStatefulSet, "apps/v1"),
		ContainerImages:     common.GetContainerImages(&statefulSet.Spec.Template.Spec),
		InitContainerImages: common.GetInitContainerImages(&statefulSet.Spec.Template.Spec),
		PodControllerInfo:   pci,
		Metrics:             m,
	}
}
