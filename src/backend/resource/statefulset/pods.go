// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package statefulset

import (
	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/pod"
)

// GetStatefulSetPods return list of pods targeting pet set.
func GetStatefulSetPods(client kubernetes.Interface, mclient clientset.Interface, dsQuery *dataselect.DataSelectQuery, name, namespace string) (*pod.PodList, error) {
	statefulSet, err := client.AppsV1().StatefulSets(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	selector, err := metaV1.LabelSelectorAsSelector(statefulSet.Spec.Selector)
	if err != nil {
		return nil, err
	}
	options := metaV1.ListOptions{LabelSelector: selector.String()}
	channels := &common.ResourceChannels{
		PodList: common.GetPodListChannelWithOptions(client,
			common.NewSameNamespaceQuery(statefulSet.Namespace), options, 1),
		EventList: common.GetEventListChannelWithUid(client,
			common.NewSameNamespaceQuery(statefulSet.Namespace), statefulSet.UID, 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient, common.NewSameNamespaceQuery(statefulSet.Namespace), 1),
	}

	pods, err := getRawStatefulSetPods(channels.PodList, statefulSet)
	if err != nil {
		return pod.EmptyPodList, err
	}
	events := <-channels.EventList.List
	err = <-channels.EventList.Error
	if events == nil {
		events = &v1.EventList{}
	}
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	metricsList := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	podList := pod.ToPodList(pods, events.Items, metricsList.Items, nonCriticalErrors, dsQuery)
	return &podList, nil
}

// getRawStatefulSetPods return array of api pods targeting pet set with given name.
func getRawStatefulSetPods(channel common.PodListChannel, statefulSet *apps.StatefulSet) ([]v1.Pod, error) {
	podList := <-channel.List
	if err := <-channel.Error; err != nil {
		return nil, err
	}

	return common.FilterPodsByControllerRef(statefulSet, podList.Items), nil
}
