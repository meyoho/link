package statefulset

import (
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	"k8s.io/api/apps/v1"
)

// GetDeploymentServices returns list of services that are related to replica set targeted by given name.
func GetStatefulSetServices(channel common.ServiceListChannel, dsQuery *dataselect.DataSelectQuery, statefulSet *v1.StatefulSet) (*common.ServiceList, error) {

	services := <-channel.List
	err := <-channel.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	matchingServices := common.FilterNamespacedServicesBySelector(services.Items, statefulSet.Namespace,
		statefulSet.Spec.Selector.MatchLabels)
	return common.CreateServiceList(matchingServices, nonCriticalErrors, dsQuery), nil
}
