// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package pod

import (
	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sClient "k8s.io/client-go/kubernetes"
	"k8s.io/klog"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
)

// PodList contains a list of Pods in the cluster.
type PodList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Pods.
	Pods []Pod `json:"pods"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type PodStatus struct {
	Status          string              `json:"status"`
	PodPhase        v1.PodPhase         `json:"podPhase"`
	ContainerStates []v1.ContainerState `json:"containerStates"`
}

// Pod is a presentation layer view of Kubernetes Pod resource. This means it is Pod plus additional augmented data
// we can get from other sources (like services that target it).
type Pod struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// More info on pod status
	PodStatus PodStatus `json:"podStatus"`

	// Count of containers restarts.
	RestartCount int32 `json:"restartCount"`

	// Pod warning events
	Warnings []common.Event `json:"warnings"`

	// Name of the Node this Pod runs on.
	NodeName string `json:"nodeName"`

	Metrics *PodMetrics `json:"metrics"`
}

var EmptyPodList = &PodList{
	Pods:   make([]Pod, 0),
	Errors: make([]error, 0),
	ListMeta: api.ListMeta{
		TotalItems: 0,
	},
}

// GetPodList returns a list of all Pods in the cluster.
func GetPodList(client k8sClient.Interface, mclient clientset.Interface, nsQuery *common.NamespaceQuery,
	dsQuery *dataselect.DataSelectQuery) (*PodList, error) {
	channels := &common.ResourceChannels{
		PodList:        common.GetPodListChannelWithOptions(client, nsQuery, metaV1.ListOptions{}, 1),
		EventList:      common.GetEventListChannel(client, nsQuery, 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient, nsQuery, 1),
	}

	return GetPodListFromChannels(channels, dsQuery)
}

// GetPodListFromChannels returns a list of all Pods in the cluster
// reading required resource list once from the channels.
func GetPodListFromChannels(channels *common.ResourceChannels, dsQuery *dataselect.DataSelectQuery) (*PodList, error) {

	pods := <-channels.PodList.List
	err := <-channels.PodList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	eventList := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	metricsList := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	podList := ToPodList(pods.Items, eventList.Items, metricsList.Items, nonCriticalErrors, dsQuery)
	return &podList, nil
}

func ToPodList(pods []v1.Pod, events []v1.Event, metrics []v1beta1.PodMetrics, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) PodList {
	podList := PodList{
		Pods:   make([]Pod, 0),
		Errors: nonCriticalErrors,
	}

	podCells, filteredTotal := dataselect.
		GenericDataSelectWithFilter(toCells(pods, events), dsQuery)
	pods = fromCells(podCells)
	podList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for _, pod := range pods {
		warnings := event.GetPodsEventWarnings(events, []v1.Pod{pod})
		podDetail := toPod(&pod, warnings, metrics)
		podList.Pods = append(podList.Pods, podDetail)
	}

	return podList
}

func toPod(pod *v1.Pod, warnings []common.Event, metrics []v1beta1.PodMetrics) Pod {
	podDetail := Pod{
		ObjectMeta:   api.NewObjectMeta(pod.ObjectMeta),
		TypeMeta:     api.NewTypeMetaWithApiVersion(api.ResourceKindPod, "v1"),
		Warnings:     warnings,
		PodStatus:    getPodStatus(pod),
		RestartCount: getRestartCount(*pod),
		NodeName:     pod.Spec.NodeName,
		Metrics:      filterPodMetrics(pod, metrics),
	}
	return podDetail
}

func filterPodMetrics(pod *v1.Pod, metrics []v1beta1.PodMetrics) *PodMetrics {
	for _, pm := range metrics {
		if pm.Name == pod.Name && pm.Namespace == pod.Namespace {
			return getPodMetrics(&pm)
		}
	}
	return nil
}
