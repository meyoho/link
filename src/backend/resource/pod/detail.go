// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package pod

import (
	"encoding/base64"
	"fmt"
	"k8s.io/klog"
	"math"
	"strconv"

	"link/src/backend/api"
	errorHandler "link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/controller"
	"link/src/backend/resource/dataselect"

	"k8s.io/api/core/v1"
	res "k8s.io/apimachinery/pkg/api/resource"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// PodDetail is a presentation layer view of Kubernetes Pod resource.
type PodDetail struct {
	ObjectMeta                api.ObjectMeta                    `json:"objectMeta"`
	TypeMeta                  api.TypeMeta                      `json:"typeMeta"`
	PodStatus                 PodStatus                         `json:"podStatus"`
	Warnings                  []common.Event                    `json:"warnings"`
	PodPhase                  v1.PodPhase                       `json:"podPhase"`
	PodIP                     string                            `json:"podIP"`
	NodeName                  string                            `json:"nodeName"`
	RestartCount              int32                             `json:"restartCount"`
	QOSClass                  string                            `json:"qosClass"`
	Controller                controller.ResourceOwner          `json:"controller"`
	Containers                []Container                       `json:"containers"`
	InitContainers            []Container                       `json:"initContainers"`
	Conditions                []common.Condition                `json:"conditions"`
	EventList                 common.EventList                  `json:"eventList"`
	PersistentvolumeclaimList *common.PersistentVolumeClaimList `json:"persistentVolumeClaimList"`
	ServiceList               common.ServiceList                `json:"serviceList"`
	Metrics                   *PodMetrics                       `json:"metrics,omitempty"`
	Data                      *v1.Pod                           `json:"data"`
	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type PodMetrics struct {
	CPU    int64 `json:"cpu"`
	Memory int64 `json:"memory"`
}

// Container represents a docker/rkt/etc. container that lives in a pod.
type Container struct {
	// Name of the container.
	Name string `json:"name"`

	// Image URI of the container.
	Image string `json:"image"`

	// List of environment variables.
	Env []EnvVar `json:"env"`

	// Commands of the container
	Commands []string `json:"commands"`

	// Command arguments
	Args []string `json:"args"`
}

// EnvVar represents an environment variable of a container.
type EnvVar struct {
	// Name of the variable.
	Name string `json:"name"`

	// Value of the variable. May be empty if value from is defined.
	Value string `json:"value"`

	// Defined for derived variables. If non-null, the value is get from the reference.
	// Note that this is an API struct. This is intentional, as EnvVarSources are plain struct
	// references.
	ValueFrom *v1.EnvVarSource `json:"valueFrom"`
}

// GetPodDetail returns the details of a named Pod from a particular namespace.
func GetPodDetail(client kubernetes.Interface, mclient clientset.Interface, namespace, name string) (
	*PodDetail, error) {
	channels := &common.ResourceChannels{
		ConfigMapList:             common.GetConfigMapListChannel(client, common.NewSameNamespaceQuery(namespace), 1),
		SecretList:                common.GetSecretListChannel(client, common.NewSameNamespaceQuery(namespace), 1),
		ServiceList:               common.GetServiceListChannel(client, common.NewSameNamespaceQuery(namespace), 1),
		PersistentVolumeClaimList: common.GetPersistentVolumeClaimListChannel(client, common.NewSameNamespaceQuery(namespace), 1),
	}

	pod, err := client.CoreV1().Pods(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	controller, err := getPodController(pod)
	nonCriticalErrors, criticalError := errorHandler.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	configMapList := <-channels.ConfigMapList.List
	err = <-channels.ConfigMapList.Error
	nonCriticalErrors, criticalError = errorHandler.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	secretList := <-channels.SecretList.List
	err = <-channels.SecretList.Error
	nonCriticalErrors, criticalError = errorHandler.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	eventList, err := GetEventsForPod(client, dataselect.DefaultEventSelect, pod.Namespace, pod.Name)
	nonCriticalErrors, criticalError = errorHandler.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	pvcList := <-channels.PersistentVolumeClaimList.List
	err = <-channels.PersistentVolumeClaimList.Error
	nonCriticalErrors, criticalError = errorHandler.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	rawServiceList := <-channels.ServiceList.List
	err = <-channels.ServiceList.Error

	nonCriticalErrors, criticalError = errorHandler.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	// Filter services by selector
	matchingServices := common.FilterNamespacedServicesBySelector(rawServiceList.Items, namespace,
		pod.Labels)

	serviceList := common.CreateServiceList(matchingServices, nonCriticalErrors, dataselect.DefaultDataSelect)

	metrics, err := mclient.MetricsV1beta1().PodMetricses(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		klog.Errorf("get pod %s/%s metrics error %v", namespace, name, err)
		metrics = nil
	}

	podDetail := toPodDetail(pod, configMapList, secretList, controller,
		eventList, pvcList, *serviceList, metrics, nonCriticalErrors)
	podDetail.Data = pod
	return &podDetail, nil
}

func DeletePod(client kubernetes.Interface, namespace, name string) error {
	return client.CoreV1().Pods(namespace).Delete(name, &metaV1.DeleteOptions{})
}

func UpdatePod(client kubernetes.Interface, namespace string, pod *v1.Pod) error {
	_, err := client.CoreV1().Pods(namespace).Update(pod)
	return err
}

func getPodController(pod *v1.Pod) (controller.ResourceOwner, error) {
	ownerRef := metaV1.GetControllerOf(pod)
	if ownerRef != nil {
		return controller.ResourceOwner{
			ObjectMeta: api.ObjectMeta{Name: ownerRef.Name, Namespace: pod.Namespace},
			TypeMeta: api.TypeMeta{Kind: api.ResourceKind(ownerRef.Kind), ApiVersion: ownerRef.APIVersion},
		}, nil
	}

	return controller.ResourceOwner{}, nil
}

func extractContainerInfo(containerList []v1.Container, pod *v1.Pod, configMaps *v1.ConfigMapList, secrets *v1.SecretList) []Container {
	containers := make([]Container, 0)
	for _, container := range containerList {
		vars := make([]EnvVar, 0)
		for _, envVar := range container.Env {
			variable := EnvVar{
				Name:      envVar.Name,
				Value:     envVar.Value,
				ValueFrom: envVar.ValueFrom,
			}
			if variable.ValueFrom != nil {
				variable.Value = evalValueFrom(variable.ValueFrom, &container, pod,
					configMaps, secrets)
			}
			vars = append(vars, variable)
		}
		vars = append(vars, evalEnvFrom(container, configMaps, secrets)...)

		containers = append(containers, Container{
			Name:     container.Name,
			Image:    container.Image,
			Env:      vars,
			Commands: container.Command,
			Args:     container.Args,
		})
	}
	return containers
}

func getPodMetrics(metrics *v1beta1.PodMetrics) *PodMetrics {
	if metrics == nil {
		return nil
	}
	m := PodMetrics{}
	for _, c := range metrics.Containers {
		memory := c.Usage[v1.ResourceMemory]
		m.Memory += (&memory).Value()
		cpu := c.Usage[v1.ResourceCPU]
		m.CPU += (&cpu).MilliValue()
	}
	return &m
}

func toPodDetail(pod *v1.Pod, configMaps *v1.ConfigMapList, secrets *v1.SecretList,
	controller controller.ResourceOwner, events *common.EventList,
	persistentVolumeClaimList *v1.PersistentVolumeClaimList,
	serviceList common.ServiceList, metrics *v1beta1.PodMetrics, nonCriticalErrors []error) PodDetail {
	pvcNames := []string{}
	for _, volume := range pod.Spec.Volumes {
		if volume.PersistentVolumeClaim != nil {
			pvcNames = append(pvcNames, volume.PersistentVolumeClaim.ClaimName)
		}
	}

	warnings := make([]common.Event, 0)
	for _, event := range events.Events {
		if event.Type == v1.EventTypeWarning {
			warnings = append(warnings, event)
		}
	}

	pod.SetGroupVersionKind(schema.GroupVersionKind{Version: "v1", Kind: api.ResourceKindPod})
	return PodDetail{
		ObjectMeta:                api.NewObjectMeta(pod.ObjectMeta),
		TypeMeta:                  api.NewTypeMetaWithApiVersion(api.ResourceKindPod, "v1"),
		PodPhase:                  pod.Status.Phase,
		PodStatus:                 getPodStatus(pod),
		Warnings:                  warnings,
		PodIP:                     pod.Status.PodIP,
		RestartCount:              getRestartCount(*pod),
		QOSClass:                  string(pod.Status.QOSClass),
		NodeName:                  pod.Spec.NodeName,
		Controller:                controller,
		Containers:                extractContainerInfo(pod.Spec.Containers, pod, configMaps, secrets),
		InitContainers:            extractContainerInfo(pod.Spec.InitContainers, pod, configMaps, secrets),
		Conditions:                getPodConditions(*pod),
		EventList:                 *events,
		ServiceList:               serviceList,
		Metrics:                   getPodMetrics(metrics),
		PersistentvolumeclaimList: common.CreatePersistentVolumeClaimList(persistentVolumeClaimList.Items, pvcNames),
		Errors: nonCriticalErrors,
	}
}

func evalEnvFrom(container v1.Container, configMaps *v1.ConfigMapList, secrets *v1.SecretList) []EnvVar {
	vars := make([]EnvVar, 0)
	for _, envFromVar := range container.EnvFrom {
		switch {
		case envFromVar.ConfigMapRef != nil:
			name := envFromVar.ConfigMapRef.LocalObjectReference.Name
			for _, configMap := range configMaps.Items {
				if configMap.ObjectMeta.Name == name {
					for key, value := range configMap.Data {
						valueFrom := &v1.EnvVarSource{
							ConfigMapKeyRef: &v1.ConfigMapKeySelector{
								LocalObjectReference: v1.LocalObjectReference{
									Name: name,
								},
								Key: key,
							},
						}
						variable := EnvVar{
							Name:      envFromVar.Prefix + key,
							Value:     value,
							ValueFrom: valueFrom,
						}
						vars = append(vars, variable)
					}
					break
				}
			}
		case envFromVar.SecretRef != nil:
			name := envFromVar.SecretRef.LocalObjectReference.Name
			for _, secret := range secrets.Items {
				if secret.ObjectMeta.Name == name {
					for key, value := range secret.Data {
						valueFrom := &v1.EnvVarSource{
							SecretKeyRef: &v1.SecretKeySelector{
								LocalObjectReference: v1.LocalObjectReference{
									Name: name,
								},
								Key: key,
							},
						}
						variable := EnvVar{
							Name:      envFromVar.Prefix + key,
							Value:     base64.StdEncoding.EncodeToString(value),
							ValueFrom: valueFrom,
						}
						vars = append(vars, variable)
					}
					break
				}
			}
		}
	}
	return vars
}

// evalValueFrom evaluates environment value from given source. For more details check:
// https://github.com/kubernetes/kubernetes/blob/d82e51edc5f02bff39661203c9b503d054c3493b/pkg/kubectl/describe.go#L1056
func evalValueFrom(src *v1.EnvVarSource, container *v1.Container, pod *v1.Pod,
	configMaps *v1.ConfigMapList, secrets *v1.SecretList) string {
	switch {
	case src.ConfigMapKeyRef != nil:
		name := src.ConfigMapKeyRef.LocalObjectReference.Name
		for _, configMap := range configMaps.Items {
			if configMap.ObjectMeta.Name == name {
				return configMap.Data[src.ConfigMapKeyRef.Key]
			}
		}
	case src.SecretKeyRef != nil:
		name := src.SecretKeyRef.LocalObjectReference.Name
		for _, secret := range secrets.Items {
			if secret.ObjectMeta.Name == name {
				return base64.StdEncoding.EncodeToString([]byte(
					secret.Data[src.SecretKeyRef.Key]))
			}
		}
	case src.ResourceFieldRef != nil:
		valueFrom, err := extractContainerResourceValue(src.ResourceFieldRef, container)
		if err != nil {
			valueFrom = ""
		}
		resource := src.ResourceFieldRef.Resource
		if valueFrom == "0" && (resource == "limits.cpu" || resource == "limits.memory") {
			valueFrom = "node allocatable"
		}
		return valueFrom
	case src.FieldRef != nil:
		internalFieldPath, _, err := runtime.NewScheme().ConvertFieldLabel(src.FieldRef.APIVersion,
			"Pod", src.FieldRef.FieldPath, "")
		if err != nil {
			klog.Errorf("%v", err)
			return ""
		}
		valueFrom, err := ExtractFieldPathAsString(pod, internalFieldPath)
		if err != nil {
			klog.Errorf("%v", err)
			return ""
		}
		return valueFrom
	}
	return ""
}

// extractContainerResourceValue extracts the value of a resource in an already known container.
func extractContainerResourceValue(fs *v1.ResourceFieldSelector, container *v1.Container) (string,
	error) {
	divisor := res.Quantity{}
	if divisor.Cmp(fs.Divisor) == 0 {
		divisor = res.MustParse("1")
	} else {
		divisor = fs.Divisor
	}

	switch fs.Resource {
	case "limits.cpu":
		return strconv.FormatInt(int64(math.Ceil(float64(container.Resources.Limits.
			Cpu().MilliValue())/float64(divisor.MilliValue()))), 10), nil
	case "limits.memory":
		return strconv.FormatInt(int64(math.Ceil(float64(container.Resources.Limits.
			Memory().Value())/float64(divisor.Value()))), 10), nil
	case "requests.cpu":
		return strconv.FormatInt(int64(math.Ceil(float64(container.Resources.Requests.
			Cpu().MilliValue())/float64(divisor.MilliValue()))), 10), nil
	case "requests.memory":
		return strconv.FormatInt(int64(math.Ceil(float64(container.Resources.Requests.
			Memory().Value())/float64(divisor.Value()))), 10), nil
	}

	return "", fmt.Errorf("Unsupported container resource : %v", fs.Resource)
}
