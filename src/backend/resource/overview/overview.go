// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package overview

import (
	"k8s.io/klog"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/node"
	"sort"
	"time"

	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
	"link/src/backend/resource/daemonset"
	"link/src/backend/resource/deployment"
	"link/src/backend/resource/statefulset"
)

// Overview is a list of objects present in a given namespace.
type Overview struct {
	NodeStatus        NodeStatus       `json:"nodeStatus"`
	PodStatus         PodStatus        `json:"podStatus"`
	ResourceStatus    ResourceStatus   `json:"resourceStatus"`
	DeploymentStatus  ControllerStatus `json:"deploymentStatus"`
	DaemonSetStatus   ControllerStatus `json:"daemonSetStatus"`
	StatefulSetStatus ControllerStatus `json:"statefulSetStatus"`
	EventList         []Event          `json:"eventList"`
	NamespaceCount    int              `json:"namespaceCount"`
	Errors            []error          `json:"errors"`
}

type NodeStatus struct {
	Total   int `json:"total"`
	Running int `json:"running"`
	Failed  int `json:"failed"`
}

type PodStatus struct {
	Total     int `json:"total"`
	Running   int `json:"running"`
	Pending   int `json:"pending"`
	Completed int `json:"completed"`
	Failed    int `json:"failed"`
}

type Metrics struct {
	CPU    int64 `json:"cpu"`
	Memory int64 `json:"memory"`
}

type ResourceStatus struct {
	TotalCpu   int64    `json:"totalCpu"`
	TotalMem   int64    `json:"totalMem"`
	RequestCpu int64    `json:"requestCpu"`
	RequestMem int64    `json:"requestMem"`
	Metrics    *Metrics `json:"metrics,omitempty"`
}

type ControllerStatus struct {
	Total   int `json:"total"`
	Running int `json:"running"`
	Pending int `json:"pending"`
	Failed  int `json:"failed"`
	Stopped int `json:"stopped"`
}

type Event struct {
	Count          int32              `json:"count"`
	Message        string             `json:"message"`
	Type           string             `json:"type"`
	LastTimestamp  time.Time          `json:"lastTimestamp"`
	InvolvedObject v1.ObjectReference `json:"involvedObject"`
}

func GetOverview(client kubernetes.Interface, mclient clientset.Interface) (*Overview, error) {
	channels := common.ResourceChannels{
		NodeList:        common.GetNodeListChannel(client, 1),
		NamespaceList:   common.GetNamespaceListChannel(client, 1),
		PodList:         common.GetPodListChannel(client, &common.NamespaceQuery{}, 1),
		DeploymentList:  common.GetDeploymentListChannel(client, &common.NamespaceQuery{}, 1),
		ReplicaSetList:  common.GetReplicaSetListChannel(client, &common.NamespaceQuery{}, 1),
		DaemonSetList:   common.GetDaemonSetListChannel(client, &common.NamespaceQuery{}, 1),
		StatefulSetList: common.GetStatefulSetListChannel(client, &common.NamespaceQuery{}, 1),
		EventList:       common.GetEventListChannel(client, &common.NamespaceQuery{}, 1),
	}

	nodeList := <-channels.NodeList.List
	err := <-channels.NodeList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	namespaceList := <-channels.NamespaceList.List
	err = <-channels.NamespaceList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	eventList := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	podList := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	deploymentList := <-channels.DeploymentList.List
	err = <-channels.DeploymentList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	replicaSetList := <-channels.ReplicaSetList.List
	err = <-channels.ReplicaSetList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	statefulSetList := <-channels.StatefulSetList.List
	err = <-channels.StatefulSetList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	daemonSetList := <-channels.DaemonSetList.List
	err = <-channels.DaemonSetList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	nodeMetricsList, err := mclient.MetricsV1beta1().NodeMetricses().List(metav1.ListOptions{})
	if err != nil {
		klog.Errorf("get node list metrics error %v\n", err)
	}

	return &Overview{
		NodeStatus:        *getNodeStatus(nodeList.Items),
		ResourceStatus:    *getResourceStatus(nodeList.Items, podList.Items, nodeMetricsList.Items),
		NamespaceCount:    len(namespaceList.Items),
		PodStatus:         *GetPodStatus(podList.Items),
		DeploymentStatus:  *GetDeploymentControllerStatus(deploymentList.Items, replicaSetList.Items, podList.Items, eventList.Items),
		StatefulSetStatus: *GetStatefulSetControllerStatus(statefulSetList.Items, podList.Items, eventList.Items),
		DaemonSetStatus:   *GetDaemonSetControllerStatus(daemonSetList.Items, podList.Items, eventList.Items),
		EventList:         getEventList(eventList.Items),
		Errors:            nonCriticalErrors}, nil
}

func getNodeStatus(nodes []v1.Node) *NodeStatus {
	nodeStatus := &NodeStatus{Total: len(nodes)}
	for _, no := range nodes {
		switch node.GetNodeConditionStatus(no) {
		case common.RunningStatus:
			nodeStatus.Running += 1
		default:
			nodeStatus.Failed += 1
		}
	}
	return nodeStatus
}

func getEventList(events []v1.Event) []Event {
	eventList := make([]Event, 0, len(events))
	for _, event := range events {
		eventList = append(eventList, Event{
			Count:          event.Count,
			Message:        event.Message,
			Type:           event.Type,
			LastTimestamp:  event.LastTimestamp.Time,
			InvolvedObject: event.InvolvedObject,
		})
	}
	sort.Slice(eventList, func(i, j int) bool {
		return eventList[i].LastTimestamp.After(eventList[j].LastTimestamp)
	})
	return eventList
}

func HasWarnEvent(uid types.UID, events []v1.Event) bool {
	for _, event := range events {
		if event.InvolvedObject.UID == uid {
			return true
		}
	}
	return false
}

func GetPodStatus(pods []v1.Pod) *PodStatus {
	podStatus := &PodStatus{Total: len(pods)}
	for _, pod := range pods {
		status := common.GetPodAggregatedStatus(&pod)

		switch status {
		case common.CompletedStatus:
			podStatus.Completed += 1
		case common.RunningStatus:
			podStatus.Running += 1
		case common.PendingStatus:
			podStatus.Pending += 1
		default:
			podStatus.Failed += 1
		}
	}
	return podStatus
}

func GetDeploymentControllerStatus(deployments []apps.Deployment, rs []apps.ReplicaSet, pods []v1.Pod, events []v1.Event) *ControllerStatus {
	controllerStatus := &ControllerStatus{Total: len(deployments)}
	deploymentWarnEvents := []v1.Event{}
	for _, event := range events {
		if event.Type == v1.EventTypeWarning && (event.InvolvedObject.Kind == "Deployment" || event.InvolvedObject.Kind == "Pod") {
			deploymentWarnEvents = append(deploymentWarnEvents, event)
		}
	}

	for _, deploy := range deployments {
		status := deployment.GetDeploymentStatus(deploy, rs, pods, events)
		switch status {
		case common.RunningStatus:
			controllerStatus.Running += 1
		case common.PendingStatus:
			controllerStatus.Pending += 1
		case common.StoppedStatus:
			controllerStatus.Stopped += 1
		default:
			controllerStatus.Failed += 1
		}
	}

	return controllerStatus
}

func GetStatefulSetControllerStatus(statefulSets []apps.StatefulSet, pods []v1.Pod, events []v1.Event) *ControllerStatus {
	controllerStatus := &ControllerStatus{Total: len(statefulSets)}
	statefulSetWarnEvents := []v1.Event{}
	for _, event := range events {
		if event.Type == v1.EventTypeWarning && (event.InvolvedObject.Kind == "StatefulSet" || event.InvolvedObject.Kind == "Pod") {
			statefulSetWarnEvents = append(statefulSetWarnEvents, event)
		}
	}

	for _, ss := range statefulSets {
		status := statefulset.GetStatefulSetStatus(ss, pods, events)
		switch status {
		case common.RunningStatus:
			controllerStatus.Running += 1
		case common.PendingStatus:
			controllerStatus.Pending += 1
		case common.StoppedStatus:
			controllerStatus.Stopped += 1
		default:
			controllerStatus.Failed += 1
		}
	}
	return controllerStatus
}

func GetDaemonSetControllerStatus(daemonSets []apps.DaemonSet, pods []v1.Pod, events []v1.Event) *ControllerStatus {
	controllerStatus := &ControllerStatus{Total: len(daemonSets)}
	daemonSetWarnEvent := []v1.Event{}
	for _, event := range events {
		if event.Type == v1.EventTypeWarning && (event.InvolvedObject.Kind == "DaemonSet" || event.InvolvedObject.Kind == "Pod") {
			daemonSetWarnEvent = append(daemonSetWarnEvent, event)
		}
	}

	for _, ds := range daemonSets {
		status := daemonset.GetDaemonSetStatus(ds, pods, events)
		switch status {
		case common.RunningStatus:
			controllerStatus.Running += 1
		case common.PendingStatus:
			controllerStatus.Pending += 1
		case common.StoppedStatus:
			controllerStatus.Stopped += 1
		default:
			controllerStatus.Failed += 1
		}
	}
	return controllerStatus
}

func getResourceStatus(nodes []v1.Node, pods []v1.Pod, nodeMetricsItems []v1beta1.NodeMetrics) *ResourceStatus {
	totalCPU := resource.Quantity{}
	totalMem := resource.Quantity{}
	requestedCPU := resource.Quantity{}
	requestedMem := resource.Quantity{}

	metrics := getMetricsFromNodeMetrics(nodeMetricsItems)

	for _, node := range nodes {
		totalCPU.Add(*node.Status.Capacity.Cpu())
		totalMem.Add(*node.Status.Capacity.Memory())
	}

	for _, pod := range pods {
		if pod.Spec.NodeName != "" && pod.Status.Phase != v1.PodSucceeded && pod.Status.Phase != v1.PodFailed {
			podRequestResource, _ := node.PodRequestsAndLimits(&pod)
			requestedCPU.Add(podRequestResource[v1.ResourceCPU])
			requestedMem.Add(podRequestResource[v1.ResourceMemory])
		}
	}
	return &ResourceStatus{TotalCpu: totalCPU.MilliValue(), TotalMem: totalMem.Value(), RequestCpu: requestedCPU.MilliValue(), RequestMem: requestedMem.Value(), Metrics: metrics}
}

func getMetricsFromNodeMetrics(nodeMetricsItems []v1beta1.NodeMetrics) *Metrics {
	if len(nodeMetricsItems) == 0 {
		return nil
	}

	metricsCPU := resource.Quantity{}
	metricsMem := resource.Quantity{}

	for _, nodeMetrics := range nodeMetricsItems {
		metricsCPU.Add(nodeMetrics.Usage[v1.ResourceCPU])
		metricsMem.Add(nodeMetrics.Usage[v1.ResourceMemory])
	}
	return &Metrics{CPU: (&metricsCPU).MilliValue(), Memory: (&metricsMem).Value()}
}
