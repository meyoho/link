// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package job

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	batch "k8s.io/api/batch/v1"
	"k8s.io/api/core/v1"
	client "k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// JobList contains a list of Jobs in the cluster.
type JobList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Jobs.
	Jobs []Job `json:"jobs"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// Job is a presentation layer view of Kubernetes Job resource. This means it is Job plus additional
// augmented data we can get from other sources
type Job struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Aggregate information about pods belonging to this Job.
	PodControllerInfo common.PodControllerInfo `json:"podControllerInfo"`

	Metrics *common.Metrics `json:"metrics"`

	// Container images of the Job.
	ContainerImages []string `json:"containerImages"`

	// Init Container images of the Job.
	InitContainerImages []string `json:"initContainerImages"`

	// number of parallel jobs defined.
	Parallelism *int32 `json:"parallelism"`
}

// GetJobList returns a list of all Jobs in the cluster.
func GetJobList(client client.Interface, mclient clientset.Interface, nsQuery *common.NamespaceQuery,
	dsQuery *dataselect.DataSelectQuery) (*JobList, error) {
	channels := &common.ResourceChannels{
		JobList:        common.GetJobListChannel(client, nsQuery, 1),
		PodList:        common.GetPodListChannel(client, nsQuery, 1),
		EventList:      common.GetEventListChannel(client, nsQuery, 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient, nsQuery, 1),
	}

	return GetJobListFromChannels(channels, dsQuery)
}

// GetJobListFromChannels returns a list of all Jobs in the cluster reading required resource list once from the channels.
func GetJobListFromChannels(channels *common.ResourceChannels, dsQuery *dataselect.DataSelectQuery) (*JobList, error) {

	jobs := <-channels.JobList.List
	err := <-channels.JobList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	pods := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	events := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	jobList := ToJobList(jobs.Items, pods.Items, events.Items, metrics.Items, nonCriticalErrors, dsQuery)
	return jobList, nil
}

func ToJobList(jobs []batch.Job, pods []v1.Pod, events []v1.Event, metrics []v1beta1.PodMetrics, nonCriticalErrors []error,
	dsQuery *dataselect.DataSelectQuery) *JobList {

	jobList := &JobList{
		Jobs:     make([]Job, 0),
		ListMeta: api.ListMeta{TotalItems: len(jobs)},
		Errors:   nonCriticalErrors,
	}

	jobCells, filteredTotal := dataselect.GenericDataSelectWithFilter(ToCells(jobs),
		dsQuery)
	jobs = FromCells(jobCells)
	jobList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for _, job := range jobs {
		matchingPods := common.FilterPodsForJob(job, pods)
		pci := common.GetPodControllerInfo(job.Status.Active+job.Status.Succeeded,
			job.Spec.Completions, job.GetObjectMeta(), matchingPods, events)
		m := common.GetPodsMetrics(metrics, job.Namespace, pci.Pods)
		jobList.Jobs = append(jobList.Jobs, toJob(&job, &pci, m))
	}

	return jobList
}

func toJob(job *batch.Job, podControllerInfo *common.PodControllerInfo, metrics *common.Metrics) Job {
	return Job{
		ObjectMeta:          api.NewObjectMeta(job.ObjectMeta),
		TypeMeta:            api.NewTypeMetaWithApiVersion(api.ResourceKindJob, "batch/v1"),
		ContainerImages:     common.GetContainerImages(&job.Spec.Template.Spec),
		InitContainerImages: common.GetInitContainerImages(&job.Spec.Template.Spec),
		PodControllerInfo:   *podControllerInfo,
		Metrics:             metrics,
		Parallelism:         job.Spec.Parallelism,
	}
}
