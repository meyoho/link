// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package job

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	batch "k8s.io/api/batch/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	k8sClient "k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// JobDetail is a presentation layer view of Kubernetes Job resource. This means
// it is Job plus additional augmented data we can get from other sources
// (like services that target the same pods).
type JobDetail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Aggregate information about pods belonging to this Replica Set.
	PodControllerInfo common.PodControllerInfo `json:"podControllerInfo"`

	// Label selector of the job.
	Selector map[string]string `json:"selector"`

	// Container images of the Job.
	ContainerImages []string `json:"containerImages"`

	// Init container images of the Job.
	InitContainerImages []string `json:"initContainerImages"`

	// List of events related to this Job.
	EventList common.EventList `json:"eventList"`

	// Parallelism specifies the maximum desired number of pods the job should run at any given time.
	Parallelism *int32 `json:"parallelism"`

	// Completions specifies the desired number of successfully finished pods the job should be run with.
	Completions *int32 `json:"completions"`

	// Specifies the duration in seconds relative to the startTime that the job may be active
	// before the system tries to terminate it; value must be positive integer
	ActiveDeadlineSeconds *int64 `json:"activeDeadlineSeconds"`

	// Specifies the number of retries before marking this job failed.
	BackoffLimit *int32 `json:"backoffLimit"`

	Data *batch.Job `json:"data"`

	Metrics *common.Metrics `json:"metrics"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetJobDetail gets job details.
func GetJobDetail(client k8sClient.Interface, mclient clientset.Interface, namespace, name string) (
	*JobDetail, error) {
	jobData, err := client.BatchV1().Jobs(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	channels := &common.ResourceChannels{
		EventList:      common.GetEventListChannel(client, common.NewSameNamespaceQuery(namespace), 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient, common.NewSameNamespaceQuery(namespace), 1),
	}

	matchingPods, err := getRawJobPods(client, name, namespace)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	rawEvents := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	podControllerInfo := common.GetPodControllerInfo(jobData.Status.Active+jobData.Status.Succeeded,
		jobData.Spec.Completions, jobData.GetObjectMeta(), matchingPods, rawEvents.Items)

	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}
	m := common.GetPodsMetrics(metrics.Items, namespace, podControllerInfo.Pods)

	eventList, err := GetJobEvents(client, dataselect.DefaultDataSelect, jobData.Namespace, jobData.Name)
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	job := toJobDetail(jobData, *eventList, podControllerInfo, nonCriticalErrors)
	job.Metrics = m
	return &job, nil
}

func toJobDetail(job *batch.Job, eventList common.EventList, podControllerInfo common.PodControllerInfo,
	nonCriticalErrors []error) JobDetail {
	job.SetGroupVersionKind(schema.GroupVersionKind{Group: "batch", Version: "v1", Kind: api.ResourceKindJob})
	return JobDetail{
		ObjectMeta:            api.NewObjectMeta(job.ObjectMeta),
		TypeMeta:              api.NewTypeMetaWithApiVersion(api.ResourceKindJob, "batch/v1"),
		ContainerImages:       common.GetContainerImages(&job.Spec.Template.Spec),
		InitContainerImages:   common.GetInitContainerImages(&job.Spec.Template.Spec),
		PodControllerInfo:     podControllerInfo,
		ActiveDeadlineSeconds: job.Spec.ActiveDeadlineSeconds,
		BackoffLimit:          job.Spec.BackoffLimit,
		Selector:              job.Spec.Selector.MatchLabels,
		EventList:             eventList,
		Parallelism:           job.Spec.Parallelism,
		Completions:           job.Spec.Completions,
		Data:                  job,
		Errors:                nonCriticalErrors,
	}
}
