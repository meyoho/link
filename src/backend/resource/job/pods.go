// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package job

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
	"link/src/backend/resource/pod"

	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	k8sClient "k8s.io/client-go/kubernetes"
	metricsv1beta1 "k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// GetJobPods return list of pods targeting job.
func GetJobPods(client k8sClient.Interface, mclient clientset.Interface, dsQuery *dataselect.DataSelectQuery, namespace string, jobName string) (*pod.PodList, error) {
	pods, err := getRawJobPods(client, jobName, namespace)
	if err != nil {
		return pod.EmptyPodList, err
	}

	events, err := event.GetPodsEvents(client, namespace, pods)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return pod.EmptyPodList, criticalError
	}
	itemMetrics := []metricsv1beta1.PodMetrics{}
	if mclient != nil {
		metricsList, err := mclient.MetricsV1beta1().PodMetricses(namespace).List(api.ListEverything)
		if err != nil {
			klog.Errorf("list metrics failed %v", err)
		}
		itemMetrics = metricsList.Items
	}

	podList := pod.ToPodList(pods, events, itemMetrics, nonCriticalErrors, dsQuery)
	return &podList, nil
}

// Returns array of api pods targeting job with given name.
func getRawJobPods(client k8sClient.Interface, jobName, namespace string) ([]v1.Pod, error) {
	job, err := client.Batch().Jobs(namespace).Get(jobName, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	labelSelector := labels.SelectorFromSet(job.Spec.Selector.MatchLabels)
	channels := &common.ResourceChannels{
		PodList: common.GetPodListChannelWithOptions(client, common.NewSameNamespaceQuery(namespace),
			metaV1.ListOptions{
				LabelSelector: labelSelector.String(),
				FieldSelector: fields.Everything().String(),
			}, 1),
	}

	podList := <-channels.PodList.List
	if err := <-channels.PodList.Error; err != nil {
		return nil, err
	}

	return podList.Items, nil
}
