package node

import (
	"bytes"
	"fmt"
	"golang.org/x/crypto/ssh"
	batchv1 "k8s.io/api/batch/v1"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	client "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/klog"
	"math/rand"
	"strings"
	"text/template"
	"time"
)

type AddNodeConfig struct {
	ID             string   `json:"-"`
	NodeList       []string `json:"node_list"`
	NodeString     string   `json:"-"`
	SshPort        int      `json:"ssh_port"`
	SshUsername    string   `json:"ssh_username"`
	SshPassword    string   `json:"ssh_password"`
	SshKey         string   `json:"ssh_key"`
	SshSecretName  string   `json:"ssh_secret_name"`
	ApiServer      string   `json:"api_server"`
	BootstrapToken string   `json:"bootstrap_token"`
	AddNodeTimeout int32    `json:"add_node_timeout"`
	PkgRepo        string   `json:"pkg_repo"`
	Registry       string   `json:"registry"`
	ImageTag       string   `json:"image_tag"`
}

type AddResponse struct {
	JobName   string `json:"job_name"`
	Namespace string `json:"namespace"`
}

func AddNodes(client client.Interface, config AddNodeConfig) (AddResponse, error) {
	response := AddResponse{}
	config.ID = randSeq(7)
	err := validateConfig(config)
	if err != nil {
		return response, err
	}

	err = config.setDownloadUrl(client)
	if err != nil {
		return response, err
	}

	for _, node := range config.NodeList {
		err := validateSshAuth(node, config.SshPort, config.SshUsername, config.SshPassword, config.SshKey)
		if err != nil {
			return response, fmt.Errorf("ssh to %s failed %v", node, err)
		}
	}

	config.NodeString = strings.Join(config.NodeList, ";")
	if config.BootstrapToken == "" {
		if err := config.setBootstrapToken(client); err != nil {
			return response, fmt.Errorf("get bootstrap token failed %v", err)
		}
	}

	if config.ApiServer == "" {
		if err := config.setConfigMasterInfo(client); err != nil {
			return response, fmt.Errorf("get api server failed %v", err)
		}
	}

	if config.ImageTag == "" {
		if err := config.setImageTag(client); err != nil {
			return response, fmt.Errorf("failed to get image tag, %v", err)
		}
	}

	if config.AddNodeTimeout == 0 {
		config.AddNodeTimeout = 1000
	}
	jobTemplate, err := getJobTemplate(client)
	if err != nil {
		return response, err
	}

	if config.SshKey != "" {
		secretName, err := generateSshKeySecret(config.ID, config.SshKey, client)
		if err != nil {
			return response, fmt.Errorf("failed to create ssh key %v", err)
		}
		config.SshSecretName = secretName
	}

	job, err := generateJob(config, jobTemplate)
	if err != nil {
		return response, err
	}

	_, err = client.BatchV1().Jobs("kube-system").Create(job)
	if err != nil {
		return response, err
	}
	response.Namespace = job.Namespace
	response.JobName = job.Name
	return response, nil
}

func validateSshAuth(host string, port int, user string, password string, key string) error {
	var method ssh.AuthMethod
	if user != "" && password != "" {
		method = ssh.Password(password)
	} else {
		signer, err := ssh.ParsePrivateKey([]byte(key))
		if err != nil {
			return fmt.Errorf("invalide ssh key")
		}
		method = ssh.PublicKeys(signer)
	}
	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			method,
		},
		Timeout: time.Duration(5 * time.Second),
	}
	_, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", host, port), config)
	if err != nil {
		klog.Errorf("ssh connect to %s failed %v", host, err)
	}
	return err
}

func validateConfig(config AddNodeConfig) error {
	if config.SshUsername == "" {
		return fmt.Errorf("ssh username is required")
	}
	if config.SshPassword == "" && config.SshKey == "" {
		return fmt.Errorf("ssh key or ssh username/password is required")
	}

	if len(config.NodeList) == 0 {
		return fmt.Errorf("at least one node should be provided")
	}
	return nil
}

func (config *AddNodeConfig) setConfigMasterInfo(client client.Interface) error {
	ep, err := client.CoreV1().Endpoints("default").Get("kubernetes", metav1.GetOptions{})
	if err != nil {
		return err
	}
	var address string
	var port int32
	for _, subset := range ep.Subsets {
		for _, addr := range subset.Addresses {
			address = addr.IP
			break
		}

		for _, p := range subset.Ports {
			port = p.Port
			break
		}

		if address != "" && port != 0 {
			break
		}
	}
	config.ApiServer = fmt.Sprintf("%s:%d", address, port)
	return nil
}

func (config *AddNodeConfig) setBootstrapToken(client client.Interface) error {
	tokenSelector := fields.SelectorFromSet(
		map[string]string{
			"type": "bootstrap.kubernetes.io/token",
		},
	)
	listOptions := metav1.ListOptions{
		FieldSelector: tokenSelector.String(),
	}
	secrets, err := client.CoreV1().Secrets(metav1.NamespaceSystem).List(listOptions)
	if err != nil {
		return fmt.Errorf("failed to list bootstrap tokens [%v]", err)
	}

	var token string
	for _, secret := range secrets.Items {
		tokenID := getSecretString(&secret, "token-id")
		tokenSecret := getSecretString(&secret, "token-secret")
		if tokenID == "" || tokenSecret == "" {
			continue
		}
		token = fmt.Sprintf("%s.%s", tokenID, tokenSecret)
		break
	}
	if token == "" {
		return fmt.Errorf("no validate bootstrapp token")
	}
	config.BootstrapToken = token
	return nil
}

func (config *AddNodeConfig) setImageTag(client client.Interface) error {
	version, err := client.Discovery().ServerVersion()
	if err != nil {
		return fmt.Errorf("failed to get server version %v", err)
	}
	config.ImageTag = version.GitVersion
	return nil
}

func (config *AddNodeConfig) setDownloadUrl(client client.Interface) error {
	cm, err := client.CoreV1().ConfigMaps("kube-system").Get("ake-config", metav1.GetOptions{})
	if err != nil {
		return err
	}

	if pkgRepo, ok := cm.Data["pkg_repo"]; !ok {
		return fmt.Errorf("pkg_repo not found")
	} else {
		config.PkgRepo = pkgRepo
	}

	if registry, ok := cm.Data["registry"]; !ok {
		return fmt.Errorf("registry not found")
	} else {
		config.Registry = registry
	}
	return nil
}

func generateSshKeySecret(id string, sshKey string, client client.Interface) (string, error) {
	secretName := fmt.Sprintf("ssh-key-%s", id)
	secret := v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: secretName,
		},
		Data: map[string][]byte{"ake.pem": []byte(sshKey)},
	}
	_, err := client.CoreV1().Secrets("alauda-system").Create(&secret)
	if err != nil {
		return "", err
	}
	return secretName, nil
}

func generateJob(config AddNodeConfig, jobTemplate string) (*batchv1.Job, error) {
	tmpl, err := template.New("add-node-job.yaml").Parse(jobTemplate)
	if err != nil {
		return nil, err
	}
	var b bytes.Buffer
	err = tmpl.Execute(&b, config)
	if err != nil {
		return nil, err
	}
	obj, _, err := scheme.Codecs.UniversalDeserializer().Decode(b.Bytes(), nil, nil)
	if err != nil {
		return nil, err
	}
	job := obj.(*batchv1.Job)
	klog.Infof("generated job %s", job.String())
	return job, err
}

func getSecretString(secret *v1.Secret, key string) string {
	if secret.Data == nil {
		return ""
	}
	if val, ok := secret.Data[key]; ok {
		return string(val)
	}
	return ""
}

var letters = []rune("abcdefghijklmnopqrstuvwxyz0123456789")

func randSeq(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func getJobTemplate(client client.Interface) (string, error) {
	cm, err := client.CoreV1().ConfigMaps("alauda-system").Get("add-node-template", metav1.GetOptions{})
	if err != nil {
		return "", err
	}
	jobTemplate := cm.Data["add-node"]
	if jobTemplate == "" {
		return "", fmt.Errorf("no add-node-job.yaml field in add-node-template configmap")
	}
	return jobTemplate, nil
}
