// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package node

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	client "k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// NodeList contains a list of nodes in the cluster.
type NodeList struct {
	ListMeta api.ListMeta `json:"listMeta"`
	Nodes    []Node       `json:"nodes"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// Node is a presentation layer view of Kubernetes nodes. This means it is node plus additional
// augmented data we can get from other sources.
type Node struct {
	ObjectMeta         api.ObjectMeta         `json:"objectMeta"`
	TypeMeta           api.TypeMeta           `json:"typeMeta"`
	Ready              v1.ConditionStatus     `json:"ready"`
	AllocatedResources NodeAllocatedResources `json:"allocatedResources"`
	Addresses          []v1.NodeAddress       `json:"addresses,omitempty"`
	Unschedulable      bool                   `json:"unschedulable"`
	Metrics            *NodeMetrics           `json:"metrics,omitempty"`
	Taints             []v1.Taint             `json:"taints,omitempty"`
}

// GetNodeListFromChannels returns a list of all Nodes in the cluster.
func GetNodeListFromChannels(mclient clientset.Interface, channels *common.ResourceChannels,
	dsQuery *dataselect.DataSelectQuery) (*NodeList, error) {

	nodes := <-channels.NodeList.List
	err := <-channels.NodeList.Error

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	pods := <-channels.PodList.List
	err = <-channels.PodList.Error

	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	metrics, err := mclient.MetricsV1beta1().NodeMetricses().List(metav1.ListOptions{})
	if err != nil {
		klog.Errorf("get node list metrics error %v\n", err)
	}

	return toNodeList(nodes.Items, pods.Items, metrics.Items, nonCriticalErrors, dsQuery), nil
}

// GetNodeList returns a list of all Nodes in the cluster.
func GetNodeList(client client.Interface, mclient clientset.Interface, dsQuery *dataselect.DataSelectQuery) (*NodeList, error) {

	channels := &common.ResourceChannels{
		PodList:  common.GetPodListChannel(client, &common.NamespaceQuery{}, 1),
		NodeList: common.GetNodeListChannel(client, 1),
	}

	return GetNodeListFromChannels(mclient, channels, dsQuery)
}

func toNodeList(nodes []v1.Node, pods []v1.Pod, metrics []v1beta1.NodeMetrics, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *NodeList {
	nodeList := &NodeList{
		Nodes:    make([]Node, 0),
		ListMeta: api.ListMeta{TotalItems: len(nodes)},
		Errors:   nonCriticalErrors,
	}

	nodeCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(nodes), dsQuery)
	nodes = fromCells(nodeCells)
	nodeList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for _, node := range nodes {
		pl := filterNodePods(node.Name, pods)
		m := filterNodeMetrics(node.Name, metrics)
		nodeList.Nodes = append(nodeList.Nodes, toNode(node, &pl, m))
	}

	return nodeList
}

func filterNodeMetrics(nodeName string, metrics []v1beta1.NodeMetrics) *v1beta1.NodeMetrics {
	for _, m := range metrics {
		if m.Name == nodeName {
			return &m
		}
	}
	return nil
}

func toNode(node v1.Node, pods *v1.PodList, metrics *v1beta1.NodeMetrics) Node {
	allocatedResources := getNodeAllocatedResources(node, pods)

	return Node{
		ObjectMeta:         api.NewObjectMeta(node.ObjectMeta),
		TypeMeta:           api.NewTypeMetaWithApiVersion(api.ResourceKindNode, "v1"),
		Ready:              getNodeConditionStatus(node, v1.NodeReady),
		AllocatedResources: allocatedResources,
		Addresses:          node.Status.Addresses,
		Unschedulable:      node.Spec.Unschedulable,
		Metrics:            getNodeMetrics(metrics),
		Taints:             node.Spec.Taints,
	}
}

func getNodeConditionStatus(node v1.Node, conditionType v1.NodeConditionType) v1.ConditionStatus {
	for _, condition := range node.Status.Conditions {
		if condition.Type == conditionType {
			return condition.Status
		}
	}
	return v1.ConditionUnknown
}

func filterNodePods(nodeName string, pods []v1.Pod) v1.PodList {
	pl := v1.PodList{}
	for _, pod := range pods {
		if pod.Spec.NodeName == nodeName && pod.Status.Phase != v1.PodSucceeded && pod.Status.Phase != v1.PodFailed {
			pl.Items = append(pl.Items, pod)
		}
	}
	return pl
}
