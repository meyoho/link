package other

import (
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
)

func CreateResource(client dynamic.Interface, resource schema.GroupVersionResource, namespace string, payload *unstructured.Unstructured) error {
	_, err := client.Resource(resource).Namespace(namespace).Create(payload)
	return err
}
