package other

import (
	"k8s.io/klog"
	"link/src/backend/api"
	clientapi "link/src/backend/client/api"
	"link/src/backend/resource/dataselect"
	"strings"
	"sync"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
)

var unListedResource = []string{"Event", "Deployment", "ReplicaSet", "Pod", "ConfigMap",
	"Secret", "Service", "Node", "Role", "RoleBinding", "ClusterRole", "DaemonSet", "LimitRange", "ResourceQuota",
	"ClusterRoleBinding", "StatefulSet", "PersistentVolume", "PersistentVolumeClaim", "StorageClass",
	"Ingress", "NetworkPolicy", "Namespace", "Job", "CronJob"}

func inBlackList(kind string) bool {
	for _, r := range unListedResource {
		if r == kind {
			return true
		}
	}
	return false
}

func GetResourceList(client dynamic.Interface, resource schema.GroupVersionResource, namespace string) ([]*ResourceMeta, error) {
	rl, err := client.Resource(resource).Namespace(namespace).List(v1.ListOptions{})
	if err != nil {
		return nil, err
	}

	result := []*ResourceMeta{}
	for _, i := range rl.Items {
		resourceMeta := ResourceMeta{
			ObjectMeta: api.ObjectMeta{
				Name:              i.GetName(),
				Namespace:         i.GetNamespace(),
				Labels:            i.GetLabels(),
				Annotations:       i.GetAnnotations(),
				CreationTimestamp: i.GetCreationTimestamp(),
				Uid:               string(i.GetUID()),
			},
			TypeMeta: ResourceTypeMeta{
				Name:       resource.Resource,
				Kind:       i.GetKind(),
				ApiVersion: i.GetAPIVersion(),
			},
		}
		resourceMeta.setScope()
		result = append(result, &resourceMeta)
	}
	return result, nil
}

func GetCanListResource(client kubernetes.Interface, listAll bool) ([]schema.GroupVersionResource, error) {
	serverResourceList, err := client.Discovery().ServerResources()
	if err != nil {
		klog.Errorf("discover kubernetes resources failed %v", err)
	}
	result := []schema.GroupVersionResource{}
	for _, rl := range serverResourceList {
		for _, r := range rl.APIResources {
			if canResourceList(r) || listAll {
				gv, _ := schema.ParseGroupVersion(rl.GroupVersion)
				gvr := schema.GroupVersionResource{Resource: r.Name, Version: gv.Version, Group: gv.Group}

				if listAll || !inBlackList(r.Kind) {
					result = append(result, gvr)
				}
				setKindToName(r)
			}
		}
	}
	return result, nil
}

func setKindToName(r v1.APIResource) {
	if _, ok := KindToName[r.Kind]; !ok {
		KindToName[r.Kind] = KindMeta{Name: r.Name, Namespaced: r.Namespaced}
	}
}

func canResourceList(resource v1.APIResource) bool {
	if strings.Contains(resource.Name, "/") {
		return false
	}

	for _, v := range resource.Verbs {
		if v == "list" {
			return true
		}
	}
	return false
}

func GetAllResourceList(cm clientapi.ClientManager, req *restful.Request, dsQuery *dataselect.DataSelectQuery) (*ResourceList, error) {
	k8sClient, err := cm.Client(req)
	if err != nil {
		return nil, err
	}
	resourceTypes, err := GetCanListResource(k8sClient, false)
	if err != nil {
		return nil, err
	}
	scope := getScope(dsQuery)

	namespace := v1.NamespaceAll
	for _, filter := range dsQuery.FilterQuery.FilterByList {
		if filter.Property == dataselect.NamespaceProperty {
			ns, ok := filter.Value.(dataselect.StdComparableString)
			if ok {
				namespace = string(ns)
				break
			}
		}
	}

	var wg sync.WaitGroup
	resourceChannel := make(chan []*ResourceMeta, 1000)
	errorChannel := make(chan error, 1000)
	for _, rt := range resourceTypes {
		if scope == NamespacedScope && !namespacedResource(rt) || scope == ClusteredScope && namespacedResource(rt) {
			continue
		}

		wg.Add(1)
		dyClient, err := cm.DynamicClient(req, &schema.GroupVersion{Group: rt.Group, Version: rt.Version})
		if err != nil {
			return nil, err
		}
		go func(resource schema.GroupVersionResource) {
			resources, err := GetResourceList(dyClient, resource, namespace)
			if err != nil {
				klog.Errorf("get resource %s error %v", resource.Resource, err)
				errorChannel <- err
			} else {
				resourceChannel <- resources
			}
			wg.Done()
		}(rt)
	}
	wg.Wait()
	close(resourceChannel)
	close(errorChannel)

	result := ResourceList{}
	rawResources := make([]*ResourceMeta, 0, len(resourceChannel))
	for resources := range resourceChannel {
		rawResources = append(rawResources, resources...)
	}

	rawResources = ResourceMetaList(rawResources).unique()

	rCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(rawResources), dsQuery)
	for err := range errorChannel {
		result.Errors = append(result.Errors, err)
	}
	result.ListMeta.TotalItems = filteredTotal
	result.Resources = fromCells(rCells)
	return &result, nil
}

func namespacedResource(gvr schema.GroupVersionResource) bool {
	for _, km := range KindToName {
		if km.Name == gvr.Resource {
			return km.Namespaced
		}
	}
	return true
}

func getScope(dsQuery *dataselect.DataSelectQuery) string {
	for _, filter := range dsQuery.FilterQuery.FilterByList {
		if filter.Property == dataselect.ScopeProperty {
			if filter.Value.Contains(dataselect.StdComparableString(NamespacedScope)) {
				return NamespacedScope
			}
			return ClusteredScope
		}
	}
	return ClusteredScope
}