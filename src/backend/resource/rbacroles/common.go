// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rbacroles

import (
	"github.com/json-iterator/go"
	"k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"link/src/backend/api"
	"link/src/backend/resource/dataselect"
)

const (
	KindRbacRole        = "Role"
	KindRbacClusterRole = "ClusterRole"
)

type RbacRoleDetail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   RoleTypeMeta   `json:"typeMeta"`
	Data       *v1.Role       `json:"data"`
}

type RoleTypeMeta struct {
	Kind         string `json:"kind"`
	GroupVersion string `json:"groupVersion"`
}

func GetRbacRoleDetail(res *unstructured.Unstructured) (*RbacRoleDetail, error) {
	role := &v1.Role{}
	data, err := jsoniter.ConfigCompatibleWithStandardLibrary.Marshal(res.Object)
	if err != nil {
		return nil, err
	}
	if err := jsoniter.ConfigCompatibleWithStandardLibrary.Unmarshal(data, role); err != nil {
		return nil, err
	}

	result := RbacRoleDetail{
		ObjectMeta: api.ObjectMeta{
			Name:              res.GetName(),
			Namespace:         res.GetNamespace(),
			Labels:            res.GetLabels(),
			Annotations:       res.GetAnnotations(),
			CreationTimestamp: res.GetCreationTimestamp(),
		},
		TypeMeta: RoleTypeMeta{
			Kind:         res.GetKind(),
			GroupVersion: res.GetAPIVersion(),
		},
		Data: role,
	}

	if result.Data.Rules == nil {
		result.Data.Rules = make([]v1.PolicyRule, 0)
	}
	return &result, nil
}

// The code below allows to perform complex data section on []RbacRole
type RoleCell RbacRole

func (self RoleCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case dataselect.NameProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Name)
	case dataselect.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.ObjectMeta.CreationTimestamp.Time)
	case dataselect.NamespaceProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Namespace)
	case dataselect.KindProperty:
		return dataselect.StdComparableString(self.TypeMeta.Kind)
	default:
		// if name is not supported then just return a constant dummy value, sort will have no effect.
		return nil
	}
}

func toCells(std []RbacRole) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = RoleCell(std[i])
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []RbacRole {
	std := make([]RbacRole, len(cells))
	for i := range std {
		std[i] = RbacRole(cells[i].(RoleCell))
	}
	return std
}
