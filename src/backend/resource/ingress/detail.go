// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ingress

import (
	"k8s.io/api/core/v1"
	extensions "k8s.io/api/extensions/v1beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	client "k8s.io/client-go/kubernetes"
	"link/src/backend/api"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
)

// IngressDetail API resource provides mechanisms to inject containers with configuration data while keeping
// containers agnostic of Kubernetes
type IngressDetail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// TODO(bryk): replace this with UI specific fields.
	// Spec is the desired state of the Ingress.
	Spec extensions.IngressSpec `json:"spec"`

	// Status is the current state of the Ingress.
	Status extensions.IngressStatus `json:"status"`

	Data *extensions.Ingress `json:"data"`

	EventList common.EventList `json:"eventList"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetIngressDetail returns returns detailed information about an ingress
func GetIngressDetail(client client.Interface, namespace, name string) (*IngressDetail, error) {
	rawIngress, err := client.ExtensionsV1beta1().Ingresses(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	eventChannel := common.GetEventListChannelWithUid(client, common.NewSameNamespaceQuery(namespace), rawIngress.UID, 1)
	eventList := <-eventChannel.List
	err = <-eventChannel.Error
	if err != nil {
		return nil, err
	}

	return toIngressDetail(rawIngress, eventList.Items), nil
}

func toIngressDetail(rawIngress *extensions.Ingress, events []v1.Event) *IngressDetail {
	rawIngress.SetGroupVersionKind(schema.GroupVersionKind{Group: "extensions", Version: "v1beta1", Kind: api.ResourceKindIngress})
	return &IngressDetail{
		ObjectMeta: api.NewObjectMeta(rawIngress.ObjectMeta),
		TypeMeta:   api.NewTypeMetaWithApiVersion(api.ResourceKindIngress, "extensions/v1beta1"),
		Spec:       rawIngress.Spec,
		Status:     rawIngress.Status,
		EventList:  event.CreateEventList(events, dataselect.DefaultDataSelect),
		Data:       rawIngress,
	}
}
