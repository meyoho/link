// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rbacrolebindings

import (
	rbac "k8s.io/api/rbac/v1"
	"k8s.io/client-go/kubernetes"
	"link/src/backend/api"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"strings"
)

// RbacRoleBindingList contains a list of Roles and ClusterRoles in the cluster.
type RbacRoleBindingList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of RbacRoleBindings
	Items []RbacRoleBinding `json:"items"`
}

// RbacRoleBinding provides the simplified, combined presentation layer view of Kubernetes' RBAC RoleBindings and ClusterRoleBindings.
// ClusterRoleBindings will be referred to as RoleBindings for the namespace "all namespaces".
type RbacRoleBinding struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`
	Subjects   []rbac.Subject `json:"subjects"`
	RoleRef    rbac.RoleRef   `json:"roleRef"`
	Name       string         `json:"name"`
	Namespace  string         `json:"namespace"`
}

// GetRbacRoleBindingList returns a list of all RBAC Role Bindings in the cluster.
func GetRbacRoleBindingList(client kubernetes.Interface, dsQuery *dataselect.DataSelectQuery) (*RbacRoleBindingList, error) {
	var channels *common.ResourceChannels

	for _, filter := range dsQuery.FilterQuery.FilterByList {
		if string(filter.Property) == dataselect.KindProperty {
			switch value := filter.Value.(dataselect.StdComparableString); string(value) {
			case strings.ToLower(KindRoleBinding):
				channels = &common.ResourceChannels{
					RoleBindingList: common.GetRoleBindingListChannel(client, 1),
				}
				return GetRbacRoleBindingListFromChannels(channels, dsQuery)
			case strings.ToLower(KindClusterRoleBinding):
				channels = &common.ResourceChannels{
					ClusterRoleBindingList: common.GetClusterRoleBindingListChannel(client, 1),
				}
				return GetRbacRoleBindingListFromChannels(channels, dsQuery)
			}
			break
		}
	}
	channels = &common.ResourceChannels{
		RoleBindingList:        common.GetRoleBindingListChannel(client, 1),
		ClusterRoleBindingList: common.GetClusterRoleBindingListChannel(client, 1),
	}
	return GetRbacRoleBindingListFromChannels(channels, dsQuery)
}

// GetRbacRoleBindingListFromChannels returns a list of all RoleBindings in the cluster
// reading required resource list once from the channels.
func GetRbacRoleBindingListFromChannels(channels *common.ResourceChannels, dsQuery *dataselect.DataSelectQuery) (
	*RbacRoleBindingList, error) {
	var (
		roleBindings        *rbac.RoleBindingList
		clusterRoleBindings *rbac.ClusterRoleBindingList
	)
	if channels.RoleBindingList.List != nil {
		roleBindings = <-channels.RoleBindingList.List
		if err := <-channels.RoleBindingList.Error; err != nil {
			return nil, err
		}
	}
	if channels.ClusterRoleBindingList.List != nil {
		clusterRoleBindings = <-channels.ClusterRoleBindingList.List
		if err := <-channels.ClusterRoleBindingList.Error; err != nil {
			return nil, err
		}
	}
	result := SimplifyRbacRoleBindingLists(roleBindings, clusterRoleBindings, dsQuery)

	return result, nil
}

// SimplifyRbacRoleBindingLists merges a list of RoleBindings with a list of ClusterRoleBindings to create a simpler, unified list
func SimplifyRbacRoleBindingLists(roleBindings *rbac.RoleBindingList, clusterRoleBindings *rbac.ClusterRoleBindingList, dsQuery *dataselect.DataSelectQuery) *RbacRoleBindingList {
	items := make([]RbacRoleBinding, 0)

	if roleBindings != nil {
		for _, item := range roleBindings.Items {
			items = append(items,
				RbacRoleBinding{
					ObjectMeta: api.NewObjectMeta(item.ObjectMeta),
					TypeMeta:   api.NewTypeMeta(api.ResourceKindRbacRoleBinding),
					Name:       item.ObjectMeta.Name,
					Namespace:  item.ObjectMeta.Namespace,
					RoleRef:    item.RoleRef,
					Subjects:   item.Subjects,
				})
		}
	}
	if clusterRoleBindings != nil {
		for _, item := range clusterRoleBindings.Items {
			items = append(items,
				RbacRoleBinding{
					ObjectMeta: api.NewObjectMeta(item.ObjectMeta),
					TypeMeta:   api.NewTypeMeta(api.ResourceKindRbacClusterRoleBinding),
					Name:       item.ObjectMeta.Name,
					Namespace:  "",
					RoleRef:    item.RoleRef,
					Subjects:   item.Subjects,
				})
		}
	}

	roleBindingCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(items), dsQuery)

	result := &RbacRoleBindingList{
		Items:    fromCells(roleBindingCells),
		ListMeta: api.ListMeta{TotalItems: filteredTotal},
	}
	return result
}
