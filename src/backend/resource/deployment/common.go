// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package deployment

import (
	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
)

// The code below allows to perform complex data section on Deployment

type DeploymentCell struct {
	Deployment  apps.Deployment
	ReplicaSets []apps.ReplicaSet
	Pods        []v1.Pod
	Events      []v1.Event
}

func (self DeploymentCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case dataselect.NameProperty:
		return dataselect.StdComparableString(self.Deployment.ObjectMeta.Name)
	case dataselect.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.Deployment.ObjectMeta.CreationTimestamp.Time)
	case dataselect.NamespaceProperty:
		return dataselect.StdComparableString(self.Deployment.ObjectMeta.Namespace)
	case dataselect.StatusProperty:
		return dataselect.StdComparableString(
			GetDeploymentStatus(self.Deployment, self.ReplicaSets, self.Pods, self.Events))
	default:
		// if name is not supported then just return a constant dummy value, sort will have no effect.
		return nil
	}
}

func toCells(std []apps.Deployment, rs []apps.ReplicaSet, pods []v1.Pod, events []v1.Event) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = DeploymentCell{
			Deployment:  std[i],
			ReplicaSets: rs,
			Pods:        pods,
			Events:      events,
		}
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []apps.Deployment {
	std := make([]apps.Deployment, len(cells))
	for i := range std {
		std[i] = apps.Deployment(cells[i].(DeploymentCell).Deployment)
	}
	return std
}

func GetDeploymentStatus(deploy apps.Deployment, rs []apps.ReplicaSet, pods []v1.Pod, events []v1.Event) common.AggregatedStatus {
	warnEvents := []v1.Event{}
	for _, event := range events {
		if event.Type == v1.EventTypeWarning && (event.InvolvedObject.Kind == "Deployment" || event.InvolvedObject.Kind == "Pod") {
			warnEvents = append(warnEvents, event)
		}
	}

	if deploy.Status.UpdatedReplicas == deploy.Status.ReadyReplicas &&
		deploy.Status.ReadyReplicas == *deploy.Spec.Replicas &&
		deploy.Status.ReadyReplicas == deploy.Status.Replicas {
		if *deploy.Spec.Replicas == 0 {
			return common.StoppedStatus
		}
		return common.RunningStatus
	}

	if common.HasWarnEvent(deploy.UID, warnEvents) {
		return common.FailedStatus
	}

	deployPods := common.FilterDeploymentPodsByOwnerReference(deploy, rs, pods)
	for _, pod := range deployPods {
		if common.GetPodAggregatedStatus(&pod) == common.FailedStatus {
			return common.FailedStatus
		}
	}

	return common.PendingStatus
}
