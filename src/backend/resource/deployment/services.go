// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package deployment

import (
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	"k8s.io/api/apps/v1"
)

// GetDeploymentServices returns list of services that are related to replica set targeted by given name.
func GetDeploymentServices(channel common.ServiceListChannel, dsQuery *dataselect.DataSelectQuery, deployment *v1.Deployment) (*common.ServiceList, error) {

	services := <-channel.List
	err := <-channel.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	matchingServices := common.FilterNamespacedServicesBySelector(services.Items, deployment.Namespace,
		deployment.Spec.Selector.MatchLabels)
	return common.CreateServiceList(matchingServices, nonCriticalErrors, dsQuery), nil
}
