// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package deployment

import (
	"k8s.io/api/extensions/v1beta1"
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
	"link/src/backend/resource/replicaset"

	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/intstr"
	client "k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// RollingUpdateStrategy is behavior of a rolling update. See RollingUpdateDeployment K8s object.
type RollingUpdateStrategy struct {
	MaxSurge       *intstr.IntOrString `json:"maxSurge"`
	MaxUnavailable *intstr.IntOrString `json:"maxUnavailable"`
}

type StatusInfo struct {
	// Total number of desired replicas on the deployment
	Replicas int32 `json:"replicas"`

	// Number of non-terminated pods that have the desired template spec
	Updated int32 `json:"updated"`

	// Number of available pods (ready for at least minReadySeconds)
	// targeted by this deployment
	Available int32 `json:"available"`

	// Total number of unavailable pods targeted by this deployment.
	Unavailable int32 `json:"unavailable"`
}

// DeploymentDetail is a presentation layer view of Kubernetes Deployment resource.
type DeploymentDetail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Label selector of the service.
	Selector map[string]string `json:"selector"`

	// Detailed information about service related to Replica Set.
	ServiceList common.ServiceList `json:"serviceList"`

	// Status information on the deployment
	StatusInfo `json:"statusInfo"`

	// Aggregate information about pods belonging to this Deployment.
	PodControllerInfo common.PodControllerInfo `json:"podControllerInfo"`

	Conditions []common.Condition `json:"conditions"`

	// The deployment strategy to use to replace existing pods with new ones.
	// Valid options: Recreate, RollingUpdate
	Strategy apps.DeploymentStrategyType `json:"strategy"`

	// Min ready seconds
	MinReadySeconds int32 `json:"minReadySeconds"`

	// Rolling update strategy containing maxSurge and maxUnavailable
	RollingUpdateStrategy *RollingUpdateStrategy `json:"rollingUpdateStrategy,omitempty"`

	// ReplicaSetList containing old replica sets from the deployment
	OldReplicaSetList replicaset.ReplicaSetList `json:"oldReplicaSetList"`

	// New replica set used by this deployment
	NewReplicaSet replicaset.ReplicaSet `json:"newReplicaSet"`

	// Optional field that specifies the number of old Replica Sets to retain to allow rollback.
	RevisionHistoryLimit *int32 `json:"revisionHistoryLimit"`

	// List of events related to this Deployment
	EventList common.EventList `json:"eventList"`

	// Container images of the Deployment.
	ContainerImages []string `json:"containerImages"`

	PersistentVolumeClaimList *common.PersistentVolumeClaimList `json:"persistentVolumeClaimList"`

	Metrics *common.Metrics `json:"metrics"`

	Data *apps.Deployment `json:"data"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetDeploymentDetail returns model object of deployment and error, if any.
func GetDeploymentDetail(client client.Interface, mclient clientset.Interface, namespace string,
	deploymentName string) (*DeploymentDetail, error) {
	deployment, err := client.AppsV1().Deployments(namespace).Get(deploymentName, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	selector, err := metaV1.LabelSelectorAsSelector(deployment.Spec.Selector)
	if err != nil {
		return nil, err
	}
	options := metaV1.ListOptions{LabelSelector: selector.String()}

	channels := &common.ResourceChannels{
		ReplicaSetList: common.GetReplicaSetListChannelWithOptions(client,
			common.NewSameNamespaceQuery(namespace), options, 1),
		PodList: common.GetPodListChannelWithOptions(client,
			common.NewSameNamespaceQuery(namespace), options, 1),
		EventList: common.GetEventListChannel(client,
			common.NewSameNamespaceQuery(namespace), 1),
		ServiceList: common.GetServiceListChannel(client,
			common.NewSameNamespaceQuery(namespace), 1),
		PersistentVolumeClaimList: common.GetPersistentVolumeClaimListChannel(client,
			common.NewSameNamespaceQuery(namespace), 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient,
			common.NewSameNamespaceQuery(namespace), 1),
	}

	rawRs := <-channels.ReplicaSetList.List
	err = <-channels.ReplicaSetList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	rawPods := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	eventList := <-channels.EventList.List
	err = <-channels.EventList.Error
	if eventList == nil {
		eventList = &v1.EventList{}
	}
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	pvcList := <-channels.PersistentVolumeClaimList.List
	err = <-channels.PersistentVolumeClaimList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	oldReplicaSetList, err := GetDeploymentOldReplicaSets(client, dataselect.DefaultDataSelect, deployment)
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	rawRepSets := make([]*apps.ReplicaSet, 0)
	for i := range rawRs.Items {
		rawRepSets = append(rawRepSets, &rawRs.Items[i])
	}
	newRs, err := FindNewReplicaSet(deployment, rawRepSets)
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	var newReplicaSet replicaset.ReplicaSet
	if newRs != nil {
		newReplicaSet = replicaset.ToReplicaSet(newRs, &common.PodControllerInfo{}, nil)
	}

	// Extra Info
	var rollingUpdateStrategy *RollingUpdateStrategy
	if deployment.Spec.Strategy.RollingUpdate != nil {
		rollingUpdateStrategy = &RollingUpdateStrategy{
			MaxSurge:       deployment.Spec.Strategy.RollingUpdate.MaxSurge,
			MaxUnavailable: deployment.Spec.Strategy.RollingUpdate.MaxUnavailable,
		}
	}

	serviceList, err := GetDeploymentServices(channels.ServiceList, dataselect.DefaultDataSelect, deployment)
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	matchingPods := common.FilterDeploymentPodsByOwnerReference(*deployment, rawRs.Items, rawPods.Items)

	pvcNames := []string{}
	for _, pv := range deployment.Spec.Template.Spec.Volumes {
		if pv.PersistentVolumeClaim != nil {
			pvcNames = append(pvcNames, pv.PersistentVolumeClaim.ClaimName)
		}
	}

	deploymentEvents := []v1.Event{}
	for _, event := range eventList.Items {
		if event.InvolvedObject.UID == deployment.UID {
			deploymentEvents = append(deploymentEvents, event)
		}
	}

	deployment.SetGroupVersionKind(schema.GroupVersionKind{Group: "apps", Version: "v1", Kind: api.ResourceKindDeployment})
	pci := common.GetPodControllerInfo(deployment.Status.Replicas, deployment.Spec.Replicas, deployment.GetObjectMeta(), matchingPods, eventList.Items)
	m := common.GetPodsMetrics(metrics.Items, deployment.Namespace, pci.Pods)
	return &DeploymentDetail{
		ObjectMeta:            api.NewObjectMeta(deployment.ObjectMeta),
		TypeMeta:              api.NewTypeMetaWithApiVersion(api.ResourceKindDeployment, "apps/v1"),
		Selector:              deployment.Spec.Selector.MatchLabels,
		StatusInfo:            GetStatusInfo(&deployment.Status),
		PodControllerInfo:     pci,
		Metrics:               m,
		Conditions:            getDeploymentConditions(deployment),
		Strategy:              deployment.Spec.Strategy.Type,
		MinReadySeconds:       deployment.Spec.MinReadySeconds,
		RollingUpdateStrategy: rollingUpdateStrategy,
		OldReplicaSetList:     *oldReplicaSetList,
		NewReplicaSet:         newReplicaSet,
		RevisionHistoryLimit:  deployment.Spec.RevisionHistoryLimit,
		EventList:             event.CreateEventList(deploymentEvents, dataselect.DefaultEventSelect),
		ServiceList:           *serviceList,
		ContainerImages:       common.GetContainerImages(&deployment.Spec.Template.Spec),
		Data:                  deployment,
		PersistentVolumeClaimList: common.CreatePersistentVolumeClaimList(pvcList.Items, pvcNames),
		Errors: nonCriticalErrors,
	}, nil

}

// DeleteDeployment delete a deployment
func DeleteDeployment(client client.Interface, namespace, deploymentName string) error {
	propagationPolicy := metaV1.DeletePropagationBackground
	return client.AppsV1().Deployments(namespace).Delete(deploymentName, &metaV1.DeleteOptions{PropagationPolicy: &propagationPolicy})
}

func UpdateDeployment(client client.Interface, namespace string, deployment *apps.Deployment) error {
	_, err := client.AppsV1().Deployments(namespace).Update(deployment)
	return err
}

func GetStatusInfo(deploymentStatus *apps.DeploymentStatus) StatusInfo {
	return StatusInfo{
		Replicas:    deploymentStatus.Replicas,
		Updated:     deploymentStatus.UpdatedReplicas,
		Available:   deploymentStatus.AvailableReplicas,
		Unavailable: deploymentStatus.UnavailableReplicas,
	}
}

func getDeploymentConditions(deployment *apps.Deployment) []common.Condition {
	var conditions []common.Condition
	for _, condition := range deployment.Status.Conditions {
		conditions = append(conditions, common.Condition{
			Type:               string(condition.Type),
			Status:             condition.Status,
			LastTransitionTime: condition.LastTransitionTime,
			Reason:             condition.Reason,
			Message:            condition.Message,
		})
	}
	return conditions
}

func RollbackDeployment(client client.Interface, namespace, deployment string, revision int64) error {
	err := client.ExtensionsV1beta1().Deployments(namespace).Rollback(&v1beta1.DeploymentRollback{
		Name: deployment,
		RollbackTo: v1beta1.RollbackConfig{Revision: revision}})
	return err
}