// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package deployment

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	client "k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// DeploymentList contains a list of Deployments in the cluster.
type DeploymentList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Deployments.
	Deployments []Deployment `json:"deployments"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// Deployment is a presentation layer view of Kubernetes Deployment resource. This means
// it is Deployment plus additional augmented data we can get from other sources
// (like services that target the same pods).
type Deployment struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Aggregate information about pods belonging to this Deployment.
	PodControllerInfo common.PodControllerInfo `json:"podControllerInfo"`

	// Container images of the Deployment.
	ContainerImages []string `json:"containerImages"`

	// Init Container images of the Deployment.
	InitContainerImages []string `json:"initContainerImages"`

	Metrics *common.Metrics `json:"metrics"`
}

// GetDeploymentList returns a list of all Deployments in the cluster.
func GetDeploymentList(client client.Interface, mclient clientset.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (*DeploymentList, error) {
	channels := &common.ResourceChannels{
		DeploymentList: common.GetDeploymentListChannel(client, nsQuery, 1),
		PodList:        common.GetPodListChannel(client, nsQuery, 1),
		EventList:      common.GetEventListChannel(client, nsQuery, 1),
		ReplicaSetList: common.GetReplicaSetListChannel(client, nsQuery, 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient, nsQuery, 1),
	}

	return GetDeploymentListFromChannels(channels, dsQuery)
}

// GetDeploymentList returns a list of all Deployments in the cluster
// reading required resource list once from the channels.
func GetDeploymentListFromChannels(channels *common.ResourceChannels, dsQuery *dataselect.DataSelectQuery) (*DeploymentList, error) {

	deployments := <-channels.DeploymentList.List
	err := <-channels.DeploymentList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	pods := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	events := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	rs := <-channels.ReplicaSetList.List
	err = <-channels.ReplicaSetList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	deploymentList := toDeploymentList(deployments.Items, pods.Items, events.Items, rs.Items, metrics.Items, nonCriticalErrors,
		dsQuery)
	return deploymentList, nil
}

func toDeploymentList(deployments []apps.Deployment, pods []v1.Pod, events []v1.Event, rs []apps.ReplicaSet, metrics []v1beta1.PodMetrics,
	nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *DeploymentList {

	deploymentList := &DeploymentList{
		Deployments: make([]Deployment, 0),
		ListMeta:    api.ListMeta{TotalItems: len(deployments)},
		Errors:      nonCriticalErrors,
	}

	deploymentCells, filteredTotal := dataselect.GenericDataSelectWithFilter(
		toCells(deployments, rs, pods, events), dsQuery)
	deployments = fromCells(deploymentCells)
	deploymentList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for _, deployment := range deployments {
		matchingPods := common.FilterDeploymentPodsByOwnerReference(deployment, rs, pods)
		pci := common.GetPodControllerInfo(deployment.Status.Replicas, deployment.Spec.Replicas, deployment.GetObjectMeta(), matchingPods, events)
		deploymentList.Deployments = append(deploymentList.Deployments,
			Deployment{
				ObjectMeta:          api.NewObjectMeta(deployment.ObjectMeta),
				TypeMeta:            api.NewTypeMetaWithApiVersion(api.ResourceKindDeployment, "apps/v1"),
				ContainerImages:     common.GetContainerImages(&deployment.Spec.Template.Spec),
				InitContainerImages: common.GetInitContainerImages(&deployment.Spec.Template.Spec),
				PodControllerInfo:   pci,
				Metrics:             common.GetPodsMetrics(metrics, deployment.Namespace, pci.Pods),
			})
	}

	return deploymentList
}
