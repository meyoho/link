// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cronjob

import (
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	batch2 "k8s.io/api/batch/v1beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	k8sClient "k8s.io/client-go/kubernetes"
)

type JobTemplate struct {
	// Container images of the Job.
	ContainerImages []string `json:"containerImages"`

	// Init container images of the Job.
	InitContainerImages []string `json:"initContainerImages"`

	// List of events related to this Job.
	EventList common.EventList `json:"eventList"`

	// Parallelism specifies the maximum desired number of pods the job should run at any given time.
	Parallelism *int32 `json:"parallelism"`

	// Completions specifies the desired number of successfully finished pods the job should be run with.
	Completions *int32 `json:"completions"`

	// Specifies the duration in seconds relative to the startTime that the job may be active
	// before the system tries to terminate it; value must be positive integer
	ActiveDeadlineSeconds *int64 `json:"activeDeadlineSeconds"`

	// Specifies the number of retries before marking this job failed.
	BackoffLimit *int32 `json:"backoffLimit"`
}

// CronJobDetail contains Cron Job details.
type CronJobDetail struct {
	ConcurrencyPolicy       string           `json:"concurrencyPolicy"`
	StartingDeadLineSeconds *int64           `json:"startingDeadlineSeconds"`
	Events                  common.EventList `json:"events"`

	// Extends list item structure.
	CronJob `json:",inline"`

	SuccessfulJobsHistoryLimit *int32 `json:"successfulJobsHistoryLimit"`
	FailedJobsHistoryLimit     *int32 `json:"failedJobsHistoryLimit"`

	JobTemplate JobTemplate `json:"jobTemplate"`

	Data *batch2.CronJob `json:"data"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetCronJobDetail gets Cron Job details.
func GetCronJobDetail(client k8sClient.Interface, dsQuery *dataselect.DataSelectQuery, namespace, name string) (*CronJobDetail, error) {

	rawObject, err := client.BatchV1beta1().CronJobs(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	events, err := GetCronJobEvents(client, dsQuery, namespace, name)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	cj := toCronJobDetail(rawObject, *events, nonCriticalErrors)
	return &cj, nil
}

func toCronJobDetail(cj *batch2.CronJob, events common.EventList,
	nonCriticalErrors []error) CronJobDetail {
	cj.SetGroupVersionKind(schema.GroupVersionKind{Group: "batch", Version: "v1beta1", Kind: api.ResourceKindCronJob})

	return CronJobDetail{
		CronJob:                    toCronJob(cj),
		ConcurrencyPolicy:          string(cj.Spec.ConcurrencyPolicy),
		StartingDeadLineSeconds:    cj.Spec.StartingDeadlineSeconds,
		SuccessfulJobsHistoryLimit: cj.Spec.SuccessfulJobsHistoryLimit,
		FailedJobsHistoryLimit:     cj.Spec.FailedJobsHistoryLimit,

		JobTemplate: getCronJobJobTemplate(cj),
		Events:      events,
		Data:        cj,
		Errors:      nonCriticalErrors,
	}
}

func getCronJobJobTemplate(cj *batch2.CronJob) JobTemplate {
	job := cj.Spec.JobTemplate
	return JobTemplate{
		ContainerImages:       common.GetContainerImages(&job.Spec.Template.Spec),
		InitContainerImages:   common.GetInitContainerImages(&job.Spec.Template.Spec),
		ActiveDeadlineSeconds: job.Spec.ActiveDeadlineSeconds,
		BackoffLimit:          job.Spec.BackoffLimit,
		Parallelism:           job.Spec.Parallelism,
		Completions:           job.Spec.Completions,
	}
}
