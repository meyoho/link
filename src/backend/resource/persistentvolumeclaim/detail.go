// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package persistentvolumeclaim

import (
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
	"link/src/backend/resource/pod"

	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// PersistentVolumeClaimDetail provides the presentation layer view of Kubernetes Persistent Volume Claim resource.
type PersistentVolumeClaimDetail struct {
	ObjectMeta   api.ObjectMeta                  `json:"objectMeta"`
	TypeMeta     api.TypeMeta                    `json:"typeMeta"`
	Status       v1.PersistentVolumeClaimPhase   `json:"status"`
	Volume       string                          `json:"volume"`
	Capacity     v1.ResourceList                 `json:"capacity"`
	AccessModes  []v1.PersistentVolumeAccessMode `json:"accessModes"`
	StorageClass *string                         `json:"storageClass"`
	EventList    common.EventList                `json:"eventList"`
	Data         *v1.PersistentVolumeClaim       `json:"data"`
}

// GetPersistentVolumeClaimDetail returns detailed information about a persistent volume claim
func GetPersistentVolumeClaimDetail(client kubernetes.Interface, namespace string, name string) (*PersistentVolumeClaimDetail, error) {
	rawPersistentVolumeClaim, err := client.CoreV1().PersistentVolumeClaims(namespace).Get(name, metaV1.GetOptions{})

	if err != nil {
		return nil, err
	}
	eventChannel := common.GetEventListChannelWithUid(client, common.NewSameNamespaceQuery(namespace), rawPersistentVolumeClaim.UID, 1)
	eventList := <-eventChannel.List
	err = <-eventChannel.Error
	if err != nil {
		return nil, err
	}
	return getPersistentVolumeClaimDetail(rawPersistentVolumeClaim, eventList.Items), nil
}

func getPersistentVolumeClaimDetail(persistentVolumeClaim *v1.PersistentVolumeClaim, events []v1.Event) *PersistentVolumeClaimDetail {
	persistentVolumeClaim.SetGroupVersionKind(schema.GroupVersionKind{Version: "v1", Kind: api.ResourceKindPersistentVolumeClaim})
	return &PersistentVolumeClaimDetail{
		ObjectMeta:   api.NewObjectMeta(persistentVolumeClaim.ObjectMeta),
		TypeMeta:     api.NewTypeMetaWithApiVersion(api.ResourceKindPersistentVolumeClaim, "v1"),
		Status:       persistentVolumeClaim.Status.Phase,
		Volume:       persistentVolumeClaim.Spec.VolumeName,
		Capacity:     persistentVolumeClaim.Spec.Resources.Requests,
		AccessModes:  persistentVolumeClaim.Spec.AccessModes,
		StorageClass: persistentVolumeClaim.Spec.StorageClassName,
		EventList:    event.CreateEventList(events, dataselect.DefaultEventSelect),
		Data:         persistentVolumeClaim,
	}
}

func GetPersistentVolumeClaimPods(client kubernetes.Interface, mclient clientset.Interface, dsQuery *dataselect.DataSelectQuery, namespace string, name string) (*pod.PodList, error) {
	channels := &common.ResourceChannels{
		PodList: common.GetPodListChannel(client, common.NewSameNamespaceQuery(namespace), 1),
	}
	podList := <-channels.PodList.List
	if err := <-channels.PodList.Error; err != nil {
		return pod.EmptyPodList, err
	}

	if len(podList.Items) == 0 {
		return pod.EmptyPodList, nil
	}

	var matchingPods []v1.Pod
	for _, pod := range podList.Items {
		for _, volume := range pod.Spec.Volumes {
			if volume.PersistentVolumeClaim != nil && volume.PersistentVolumeClaim.ClaimName == name {
				matchingPods = append(matchingPods, pod)
			}
		}
	}

	events, err := event.GetPodsEvents(client, namespace, matchingPods)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	metrics, err := mclient.MetricsV1beta1().PodMetricses(v1.NamespaceAll).List(api.ListEverything)
	result := pod.ToPodList(matchingPods, events, metrics.Items, nonCriticalErrors, dsQuery)
	return &result, nil
}
