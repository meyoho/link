// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package daemonset

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/apis/metrics/v1beta1"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

// DaemonSetList contains a list of Daemon Sets in the cluster.
type DaemonSetList struct {
	ListMeta   api.ListMeta `json:"listMeta"`
	DaemonSets []DaemonSet  `json:"daemonSets"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// DaemonSet plus zero or more Kubernetes services that target the Daemon Set.
type DaemonSet struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Aggregate information about pods belonging to this Daemon Set.
	PodControllerInfo common.PodControllerInfo `json:"podControllerInfo"`

	Metrics *common.Metrics `json:"metrics"`

	// Container images of the Daemon Set.
	ContainerImages []string `json:"containerImages"`

	// InitContainer images of the Daemon Set.
	InitContainerImages []string `json:"initContainerImages"`
}

// GetDaemonSetList returns a list of all Daemon Set in the cluster.
func GetDaemonSetList(client kubernetes.Interface, mclient clientset.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (*DaemonSetList, error) {
	channels := &common.ResourceChannels{
		DaemonSetList:  common.GetDaemonSetListChannel(client, nsQuery, 1),
		ServiceList:    common.GetServiceListChannel(client, nsQuery, 1),
		PodList:        common.GetPodListChannel(client, nsQuery, 1),
		EventList:      common.GetEventListChannel(client, nsQuery, 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient, nsQuery, 1),
	}

	return GetDaemonSetListFromChannels(channels, dsQuery)
}

// GetDaemonSetListFromChannels returns a list of all Daemon Set in the cluster
// reading required resource list once from the channels.
func GetDaemonSetListFromChannels(channels *common.ResourceChannels, dsQuery *dataselect.DataSelectQuery) (*DaemonSetList, error) {

	daemonSets := <-channels.DaemonSetList.List
	err := <-channels.DaemonSetList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	pods := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	events := <-channels.EventList.List
	err = <-channels.EventList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	dsList := toDaemonSetList(daemonSets.Items, pods.Items, events.Items, metrics.Items, nonCriticalErrors, dsQuery)
	return dsList, nil
}

func toDaemonSetList(daemonSets []apps.DaemonSet, pods []v1.Pod, events []v1.Event, metrics []v1beta1.PodMetrics, nonCriticalErrors []error,
	dsQuery *dataselect.DataSelectQuery) *DaemonSetList {

	daemonSetList := &DaemonSetList{
		DaemonSets: make([]DaemonSet, 0),
		ListMeta:   api.ListMeta{TotalItems: len(daemonSets)},
		Errors:     nonCriticalErrors,
	}

	dsCells, filteredTotal := dataselect.GenericDataSelectWithFilter(ToCells(daemonSets, pods, events),
		dsQuery)
	daemonSets = FromCells(dsCells)
	daemonSetList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for _, daemonSet := range daemonSets {
		matchingPods := common.FilterPodsByControllerRef(&daemonSet, pods)

		pci := getDaemonSetPodControllerInfo(&daemonSet, &matchingPods, &events)
		m := common.GetPodsMetrics(metrics, daemonSet.Namespace, pci.Pods)
		daemonSetList.DaemonSets = append(daemonSetList.DaemonSets, DaemonSet{
			ObjectMeta:          api.NewObjectMeta(daemonSet.ObjectMeta),
			TypeMeta:            api.NewTypeMetaWithApiVersion(api.ResourceKindDaemonSet, "apps/v1"),
			PodControllerInfo:   pci,
			ContainerImages:     common.GetContainerImages(&daemonSet.Spec.Template.Spec),
			InitContainerImages: common.GetInitContainerImages(&daemonSet.Spec.Template.Spec),
			Metrics:             m,
		})
	}

	return daemonSetList
}
