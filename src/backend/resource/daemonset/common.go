// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package daemonset

import (
	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
)

type DaemonSetCell struct {
	DaemonSet apps.DaemonSet
	Pods      []v1.Pod
	Events    []v1.Event
}

func (self DaemonSetCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case dataselect.NameProperty:
		return dataselect.StdComparableString(self.DaemonSet.ObjectMeta.Name)
	case dataselect.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.DaemonSet.ObjectMeta.CreationTimestamp.Time)
	case dataselect.NamespaceProperty:
		return dataselect.StdComparableString(self.DaemonSet.ObjectMeta.Namespace)
	case dataselect.StatusProperty:
		return dataselect.StdComparableString(GetDaemonSetStatus(self.DaemonSet, self.Pods, self.Events))
	default:
		// if name is not supported then just return a constant dummy value, sort will have no effect.
		return nil
	}
}

func ToCells(std []apps.DaemonSet, pods []v1.Pod, events []v1.Event) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = DaemonSetCell{DaemonSet: std[i], Pods: pods, Events: events}
	}
	return cells
}

func FromCells(cells []dataselect.DataCell) []apps.DaemonSet {
	std := make([]apps.DaemonSet, len(cells))
	for i := range std {
		std[i] = apps.DaemonSet(cells[i].(DaemonSetCell).DaemonSet)
	}
	return std
}

func GetDaemonSetStatus(daemonSet apps.DaemonSet, pods []v1.Pod, events []v1.Event) common.AggregatedStatus {
	warnEvents := []v1.Event{}
	for _, event := range events {
		if event.Type == v1.EventTypeWarning && (event.InvolvedObject.Kind == "DaemonSet" || event.InvolvedObject.Kind == "Pod") {
			warnEvents = append(warnEvents, event)
		}
	}

	if daemonSet.Status.DesiredNumberScheduled == daemonSet.Status.NumberReady {
		if daemonSet.Status.DesiredNumberScheduled == 0 {
			return common.StoppedStatus
		}
		return common.RunningStatus
	}

	if common.HasWarnEvent(daemonSet.UID, warnEvents) {
		return common.FailedStatus
	}

	statefulSetPods := common.FilterPodsByControllerRef(daemonSet.GetObjectMeta(), pods)
	for _, pod := range statefulSetPods {
		if common.GetPodAggregatedStatus(&pod) == common.FailedStatus {
			return common.FailedStatus
		}
	}

	return common.PendingStatus
}
