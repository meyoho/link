// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package daemonset

import (
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"

	"k8s.io/api/apps/v1"
)

// GetDaemonSetServices returns list of services that are related to daemon set targeted by given
// name.
func GetDaemonSetServices(channel common.ServiceListChannel, dsQuery *dataselect.DataSelectQuery, daemonSet *v1.DaemonSet) (*common.ServiceList, error) {

	services := <-channel.List
	err := <-channel.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	matchingServices := common.FilterNamespacedServicesBySelector(services.Items, daemonSet.Namespace,
		daemonSet.Spec.Selector.MatchLabels)
	return common.CreateServiceList(matchingServices, nonCriticalErrors, dsQuery), nil
}
