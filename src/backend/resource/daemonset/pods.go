// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package daemonset

import (
	apps "k8s.io/api/apps/v1"
	api "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sClient "k8s.io/client-go/kubernetes"
	"k8s.io/klog"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
	"link/src/backend/resource/pod"
)

// GetDaemonSetPods return list of pods targeting daemon set.
func GetDaemonSetPods(client k8sClient.Interface, mclient clientset.Interface,
	dsQuery *dataselect.DataSelectQuery, name, namespace string) (*pod.PodList, error) {
	daemonSet, err := client.AppsV1().DaemonSets(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	selector, err := metaV1.LabelSelectorAsSelector(daemonSet.Spec.Selector)
	if err != nil {
		return nil, err
	}
	options := metaV1.ListOptions{LabelSelector: selector.String()}

	channels := &common.ResourceChannels{
		PodList: common.GetPodListChannelWithOptions(client,
			common.NewSameNamespaceQuery(daemonSet.Namespace), options, 1),
		EventList: common.GetEventListChannelWithUid(client,
			common.NewSameNamespaceQuery(daemonSet.Namespace), daemonSet.UID, 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient, common.NewSameNamespaceQuery(daemonSet.Namespace), 1),
	}
	pods, err := getRawDaemonSetPods(channels.PodList, daemonSet)
	if err != nil {
		return pod.EmptyPodList, err
	}

	events, err := event.GetPodsEvents(client, daemonSet.Namespace, pods)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	metricsList := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	podList := pod.ToPodList(pods, events, metricsList.Items, nonCriticalErrors, dsQuery)
	return &podList, nil
}

// Returns array of api pods targeting daemon set with given name.
func getRawDaemonSetPods(channel common.PodListChannel, daemonSet *apps.DaemonSet) ([]api.Pod, error) {

	podList := <-channel.List
	if err := <-channel.Error; err != nil {
		return nil, err
	}

	matchingPods := common.FilterPodsByControllerRef(daemonSet, podList.Items)
	return matchingPods, nil
}

// Returns simple info about pods(running, desired, failing, etc.) related to given daemon set.
func getDaemonSetPodControllerInfo(daemonSet *apps.DaemonSet, pods *[]api.Pod, events *[]api.Event) common.PodControllerInfo {
	podControllerInfo := common.GetPodControllerInfo(daemonSet.Status.CurrentNumberScheduled,
		&daemonSet.Status.DesiredNumberScheduled, daemonSet.GetObjectMeta(), *pods, *events)
	return podControllerInfo
}
