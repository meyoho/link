// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package daemonset

import (
	"k8s.io/klog"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	ds "link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
	"link/src/backend/resource/pod"

	apps "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/intstr"
	k8sClient "k8s.io/client-go/kubernetes"
	"k8s.io/metrics/pkg/client/clientset_generated/clientset"
)

type RollingUpdateStrategy struct {
	MaxUnavailable *intstr.IntOrString `json:"maxUnavailable"`
}

// DaemonSeDetail represents detailed information about a Daemon Set.
type DaemonSetDetail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Selector of the Daemon Set.
	Selector map[string]string `json:"selector,omitempty"`

	// Container image list of the pod template specified by this Daemon Set.
	ContainerImages []string `json:"containerImages"`

	// Init Container image list of the pod template specified by this Daemon Set.
	InitContainerImages []string `json:"initContainerImages"`

	RevisionHistoryLimit *int32 `json:"revisionHistoryLimit"`

	MinReadySeconds int32 `json:"minReadySeconds"`

	Strategy apps.DaemonSetUpdateStrategyType `json:"strategy"`

	RollingUpdateStrategy *RollingUpdateStrategy `json:"rollingUpdateStrategy"`

	// Aggregate information about pods belonging to this Daemon Set.
	PodControllerInfo common.PodControllerInfo `json:"podControllerInfo"`

	// Detailed information about Pods belonging to this Daemon Set.
	PodList pod.PodList `json:"podList"`

	// Detailed information about service related to Daemon Set.
	ServiceList common.ServiceList `json:"serviceList"`

	// List of events related to this daemon set
	EventList common.EventList `json:"eventList"`

	PersistentvolumeclaimList *common.PersistentVolumeClaimList `json:"persistentVolumeClaimList"`

	Data *apps.DaemonSet `json:"data"`

	Metrics *common.Metrics `json:"metrics"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// Returns detailed information about the given daemon set in the given namespace.
func GetDaemonSetDetail(client k8sClient.Interface, mclient clientset.Interface, namespace, name string) (*DaemonSetDetail, error) {
	daemonSet, err := client.AppsV1().DaemonSets(namespace).Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	selector, err := metaV1.LabelSelectorAsSelector(daemonSet.Spec.Selector)
	if err != nil {
		return nil, err
	}
	options := metaV1.ListOptions{LabelSelector: selector.String()}

	channels := &common.ResourceChannels{
		PodList: common.GetPodListChannelWithOptions(client,
			common.NewSameNamespaceQuery(namespace), options, 1),
		EventList: common.GetEventListChannelWithUid(client,
			common.NewSameNamespaceQuery(namespace), daemonSet.UID, 1),
		ServiceList: common.GetServiceListChannel(client,
			common.NewSameNamespaceQuery(namespace), 1),
		PersistentVolumeClaimList: common.GetPersistentVolumeClaimListChannel(client,
			common.NewSameNamespaceQuery(namespace), 1),
		PodMetricsList: common.GetPodMetricsListChannel(mclient,
			common.NewSameNamespaceQuery(namespace), 1),
	}

	serviceList, err := GetDaemonSetServices(channels.ServiceList, ds.DefaultDataSelect, daemonSet)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	eventList := <-channels.EventList.List
	err = <-channels.EventList.Error
	if eventList == nil {
		eventList = &v1.EventList{}
	}
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	pods := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	pvcList := <-channels.PersistentVolumeClaimList.List
	err = <-channels.PersistentVolumeClaimList.Error
	nonCriticalErrors, criticalError = errors.AppendError(err, nonCriticalErrors)
	if criticalError != nil {
		return nil, criticalError
	}

	matchingPods := common.FilterPodsByControllerRef(daemonSet, pods.Items)
	podControllerInfo := getDaemonSetPodControllerInfo(daemonSet, &matchingPods, &eventList.Items)

	metrics := <-channels.PodMetricsList.List
	err = <-channels.PodMetricsList.Error
	if err != nil {
		klog.Errorf("get pod metrics failed %v", err)
	}

	m := common.GetPodsMetrics(metrics.Items, namespace, podControllerInfo.Pods)

	var rollingUpdateStrategy *RollingUpdateStrategy
	if daemonSet.Spec.UpdateStrategy.RollingUpdate != nil {
		rollingUpdateStrategy = &RollingUpdateStrategy{
			MaxUnavailable: daemonSet.Spec.UpdateStrategy.RollingUpdate.MaxUnavailable,
		}
	} else {
		rollingUpdateStrategy = &RollingUpdateStrategy{
			MaxUnavailable: &intstr.IntOrString{Type: intstr.Int, IntVal: 0},
		}
	}

	pvcNames := []string{}
	for _, volume := range daemonSet.Spec.Template.Spec.Volumes {
		if volume.PersistentVolumeClaim != nil {
			pvcNames = append(pvcNames, volume.PersistentVolumeClaim.ClaimName)
		}
	}
	daemonSet.SetGroupVersionKind(schema.GroupVersionKind{Group: "apps", Version: "v1", Kind: api.ResourceKindDaemonSet})
	daemonSetDetail := &DaemonSetDetail{
		ObjectMeta:                api.NewObjectMeta(daemonSet.ObjectMeta),
		TypeMeta:                  api.NewTypeMetaWithApiVersion(api.ResourceKindDaemonSet, "apps/v1"),
		Selector:                  daemonSet.Spec.Selector.MatchLabels,
		Strategy:                  daemonSet.Spec.UpdateStrategy.Type,
		RollingUpdateStrategy:     rollingUpdateStrategy,
		RevisionHistoryLimit:      daemonSet.Spec.RevisionHistoryLimit,
		PodControllerInfo:         podControllerInfo,
		ServiceList:               *serviceList,
		PersistentvolumeclaimList: common.CreatePersistentVolumeClaimList(pvcList.Items, pvcNames),
		EventList:                 event.CreateEventList(eventList.Items, ds.DefaultEventSelect),
		Metrics:                   m,
		Data:                      daemonSet,
		Errors:                    nonCriticalErrors,
	}

	for _, container := range daemonSet.Spec.Template.Spec.Containers {
		daemonSetDetail.ContainerImages = append(daemonSetDetail.ContainerImages, container.Image)
	}

	for _, initContainer := range daemonSet.Spec.Template.Spec.InitContainers {
		daemonSetDetail.InitContainerImages = append(daemonSetDetail.InitContainerImages, initContainer.Image)
	}

	return daemonSetDetail, nil
}
