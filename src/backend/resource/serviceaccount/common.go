package serviceaccount

import (
	"k8s.io/api/core/v1"
	"link/src/backend/resource/dataselect"
)

type ServiceAccountCell struct {
	ServiceAccount v1.ServiceAccount
}

func (self ServiceAccountCell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case dataselect.NameProperty:
		return dataselect.StdComparableString(self.ServiceAccount.ObjectMeta.Name)
	case dataselect.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.ServiceAccount.ObjectMeta.CreationTimestamp.Time)
	case dataselect.NamespaceProperty:
		return dataselect.StdComparableString(self.ServiceAccount.ObjectMeta.Namespace)
	default:
		// if name is not supported then just return a constant dummy value, sort will have no effect.
		return nil
	}
}

func toCells(std []v1.ServiceAccount) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = ServiceAccountCell{ServiceAccount: std[i]}
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []v1.ServiceAccount {
	std := make([]v1.ServiceAccount, len(cells))
	for i := range std {
		std[i] = v1.ServiceAccount(cells[i].(ServiceAccountCell).ServiceAccount)
	}
	return std
}
