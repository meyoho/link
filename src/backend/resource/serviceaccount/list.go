package serviceaccount

import (
	"k8s.io/api/core/v1"
	client "k8s.io/client-go/kubernetes"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
)

type ServiceAccountList struct {
	ListMeta        api.ListMeta     `json:"listMeta"`
	ServiceAccounts []ServiceAccount `json:"serviceAccounts"`
	Errors          []error          `json:"errors"`
}

type ServiceAccount struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`
}

func GetServiceAccountList(client client.Interface, nsQuery *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (*ServiceAccountList, error) {
	channels := &common.ResourceChannels{
		ServiceAccountList: common.GetServiceAccountChannel(client, nsQuery, 1),
	}
	return GetServiceAccountListFromChannels(channels, dsQuery)
}

func GetServiceAccountListFromChannels(channels *common.ResourceChannels, dsQuery *dataselect.DataSelectQuery) (*ServiceAccountList, error) {
	serviceAccountList := <-channels.ServiceAccountList.List
	err := <-channels.ServiceAccountList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	saList := toServiceAccountList(serviceAccountList.Items, nonCriticalErrors, dsQuery)
	return saList, nil
}

func toServiceAccountList(serviceAccounts []v1.ServiceAccount, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *ServiceAccountList {
	serviceAccountList := &ServiceAccountList{
		ServiceAccounts: make([]ServiceAccount, 0),
		ListMeta:        api.ListMeta{TotalItems: len(serviceAccounts)},
		Errors:          nonCriticalErrors,
	}
	saCells, filterdTotal := dataselect.GenericDataSelectWithFilter(toCells(serviceAccounts), dsQuery)
	serviceAccounts = fromCells(saCells)
	serviceAccountList.ListMeta = api.ListMeta{TotalItems: filterdTotal}
	for _, sa := range serviceAccounts {
		serviceAccountList.ServiceAccounts = append(serviceAccountList.ServiceAccounts, toServiceAccount(&sa))
	}
	return serviceAccountList
}

func toServiceAccount(serviceAccount *v1.ServiceAccount) ServiceAccount {
	return ServiceAccount{
		ObjectMeta: api.NewObjectMeta(serviceAccount.ObjectMeta),
		TypeMeta:   api.NewTypeMetaWithApiVersion(api.ResourceKindServiceAccount, "v1"),
	}
}
