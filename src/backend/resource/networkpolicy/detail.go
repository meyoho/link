package networkpolicy

import (
	network "k8s.io/api/networking/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	client "k8s.io/client-go/kubernetes"
	"link/src/backend/api"
)

type Detail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	Data *network.NetworkPolicy `json:"data"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// GetIngressDetail returns returns detailed information about an ingress
func GetNetworkPolicyDetail(client client.Interface, namespace, name string) (*Detail, error) {
	rawPolicy, err := client.NetworkingV1().NetworkPolicies(namespace).Get(name, metaV1.GetOptions{})

	if err != nil {
		return nil, err
	}

	return toNetworkPolicyDetail(rawPolicy), nil
}

func toNetworkPolicyDetail(rawPolicy *network.NetworkPolicy) *Detail {
	rawPolicy.SetGroupVersionKind(schema.GroupVersionKind{Group: "networking.k8s.io", Version: "v1", Kind: api.ResourceKindNetworkPolicy})
	return &Detail{
		ObjectMeta: api.NewObjectMeta(rawPolicy.ObjectMeta),
		TypeMeta:   api.NewTypeMetaWithApiVersion(api.ResourceKindNetworkPolicy, "networking.k8s.io/v1"),
		Data:       rawPolicy,
	}
}
