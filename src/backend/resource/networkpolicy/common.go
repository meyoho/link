package networkpolicy

import (
	network "k8s.io/api/networking/v1"
	"link/src/backend/resource/dataselect"
)

// The code below allows to perform complex data section on []extensions.Ingress

type Cell network.NetworkPolicy

func (self Cell) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case dataselect.NameProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Name)
	case dataselect.CreationTimestampProperty:
		return dataselect.StdComparableTime(self.ObjectMeta.CreationTimestamp.Time)
	case dataselect.NamespaceProperty:
		return dataselect.StdComparableString(self.ObjectMeta.Namespace)
	default:
		// if name is not supported then just return a constant dummy value, sort will have no effect.
		return nil
	}
}

func toCells(std []network.NetworkPolicy) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = Cell(std[i])
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []network.NetworkPolicy {
	std := make([]network.NetworkPolicy, len(cells))
	for i := range std {
		std[i] = network.NetworkPolicy(cells[i].(Cell))
	}
	return std
}
