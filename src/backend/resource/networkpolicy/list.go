package networkpolicy

import (
	network "k8s.io/api/networking/v1"
	client "k8s.io/client-go/kubernetes"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
)

type NetworkPolicy struct {
	api.ObjectMeta `json:"objectMeta"`
	api.TypeMeta   `json:"typeMeta"`

	PolicyTypes []network.PolicyType `json:"policyTypes"`
}

type NetworkPolicyList struct {
	api.ListMeta `json:"listMeta"`
	Items        []NetworkPolicy `json:"items"`
	Errors       []error         `json:"errors"`
}

func GetNetworkPolicyList(client client.Interface, namespace *common.NamespaceQuery, dsQuery *dataselect.DataSelectQuery) (*NetworkPolicyList, error) {
	networkPolicyList, err := client.NetworkingV1().NetworkPolicies(namespace.ToRequestParam()).List(api.ListEverything)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	return toNetworkPolicyList(networkPolicyList.Items, nonCriticalErrors, dsQuery), nil
}

func toNetworkPolicy(networkPolicy *network.NetworkPolicy) *NetworkPolicy {
	policy := &NetworkPolicy{
		ObjectMeta:  api.NewObjectMeta(networkPolicy.ObjectMeta),
		TypeMeta:    api.NewTypeMetaWithApiVersion(api.ResourceKindNetworkPolicy, "networking.k8s.io/v1"),
		PolicyTypes: networkPolicy.Spec.PolicyTypes,
	}
	return policy
}

func toNetworkPolicyList(networkPolicies []network.NetworkPolicy, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *NetworkPolicyList {
	newNetworkPolicyList := &NetworkPolicyList{
		Errors: nonCriticalErrors,
	}

	cells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(networkPolicies), dsQuery)
	networkPolicies = fromCells(cells)
	newNetworkPolicyList.ListMeta = api.ListMeta{TotalItems: filteredTotal}

	for _, ingress := range networkPolicies {
		newNetworkPolicyList.Items = append(newNetworkPolicyList.Items, *toNetworkPolicy(&ingress))
	}
	newNetworkPolicyList.ListMeta = api.ListMeta{TotalItems: len(newNetworkPolicyList.Items)}

	return newNetworkPolicyList
}
