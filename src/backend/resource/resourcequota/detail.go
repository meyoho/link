// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package resourcequota

import (
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"link/src/backend/api"
)

// ResourceStatus provides the status of the resource defined by a resource quota.
type ResourceStatus struct {
	Used string `json:"used,omitempty"`
	Hard string `json:"hard,omitempty"`
}

// ResourceQuotaDetail provides the presentation layer view of Kubernetes Resource Quotas resource.
type ResourceQuotaDetail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Scopes defines quota scopes
	Scopes []v1.ResourceQuotaScope `json:"scopes,omitempty"`

	// StatusList is a set of (resource name, Used, Hard) tuple.
	StatusList map[v1.ResourceName]ResourceStatus `json:"statusList,omitempty"`
}

// ResourceQuotaDetailList
type ResourceQuotaDetailList struct {
	ListMeta api.ListMeta          `json:"listMeta"`
	Items    []ResourceQuotaDetail `json:"items"`
}

func ToResourceQuotaDetail(rawResourceQuota *v1.ResourceQuota) *ResourceQuotaDetail {
	statusList := make(map[v1.ResourceName]ResourceStatus)

	for key, value := range rawResourceQuota.Status.Hard {
		used := rawResourceQuota.Status.Used[key]
		statusList[key] = ResourceStatus{
			Used: used.String(),
			Hard: value.String(),
		}
	}
	return &ResourceQuotaDetail{
		ObjectMeta: api.NewObjectMeta(rawResourceQuota.ObjectMeta),
		TypeMeta:   api.NewTypeMeta(api.ResourceKindResourceQuota),
		Scopes:     rawResourceQuota.Spec.Scopes,
		StatusList: statusList,
	}
}

type EffectiveQuota struct {
	Scope        v1.ResourceQuotaScope `json:"scope"`
	ResourceName string                `json:"resourceName"`
	Hard         resource.Quantity     `json:"hard"`
	Used         resource.Quantity     `json:"used"`
}

func ComputeEffectiveQuota(quotas []v1.ResourceQuota) []EffectiveQuota {
	quotaMap := make(map[v1.ResourceQuotaScope]v1.ResourceQuotaStatus)
	for _, quota := range quotas {
		if len(quota.Spec.Scopes) == 0 {
			if _, ok := quotaMap[""]; !ok {
				quotaMap[""] = quota.Status
			} else {
				quotaMap[""] = mergeQuotas(quotaMap[""], quota.Status)
			}
			continue
		}

		for _, scope := range quota.Spec.Scopes {
			if _, ok := quotaMap[scope]; !ok {
				quotaMap[scope] = quota.Status
			} else {
				quotaMap[scope] = mergeQuotas(quotaMap[scope], quota.Status)
			}
		}
	}

	result := make([]EffectiveQuota, 0)
	for scope, status := range quotaMap {
		for name, quantity := range status.Hard {
			result = append(result, EffectiveQuota{Scope: scope, ResourceName: string(name), Hard: quantity, Used: status.Used[name]})
		}
	}
	return result
}

func mergeQuotas(q1 v1.ResourceQuotaStatus, q2 v1.ResourceQuotaStatus) v1.ResourceQuotaStatus {
	result := q1.DeepCopy()
	for name, quantity := range q2.Hard {
		if _, ok := result.Hard[name]; !ok {
			result.Hard[name] = quantity
			result.Used[name] = q2.Used[name]
		} else {
			if quantity.Cmp(result.Hard[name]) == -1 {
				result.Hard[name] = quantity
			}
		}
	}
	return *result
}
