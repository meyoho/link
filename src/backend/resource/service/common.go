// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"link/src/backend/api"
	"link/src/backend/resource/common"
)

// ToServiceDetail returns api service objnect based on kubernetes service object
func ToServiceDetail(service *v1.Service, nonCriticalErrors []error) ServiceDetail {
	service.SetGroupVersionKind(schema.GroupVersionKind{Version: "v1", Kind: api.ResourceKindService})
	return ServiceDetail{
		ObjectMeta:        api.NewObjectMeta(service.ObjectMeta),
		TypeMeta:          api.NewTypeMetaWithApiVersion(api.ResourceKindService, "v1"),
		InternalEndpoint:  common.GetInternalEndpoint(service.Name, service.Namespace, service.Spec.Ports),
		ExternalEndpoints: common.GetExternalEndpoints(service),
		Ports:             service.Spec.Ports,
		Selector:          service.Spec.Selector,
		ClusterIP:         service.Spec.ClusterIP,
		Type:              service.Spec.Type,
		SessionAffinity:   service.Spec.SessionAffinity,
		Errors:            nonCriticalErrors,
		Data:              service,
	}
}
