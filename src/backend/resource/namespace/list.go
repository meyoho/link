// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package namespace

import (
	"sync"

	authorizationapi "k8s.io/api/authorization/v1"
	"k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/dataselect"
)

// NamespaceList contains a list of namespaces in the cluster.
type NamespaceList struct {
	ListMeta api.ListMeta `json:"listMeta"`

	// Unordered list of Namespaces.
	Namespaces []Namespace `json:"namespaces"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

// Namespace is a presentation layer view of Kubernetes namespaces. This means it is namespace plus
// additional augmented data we can get from other sources.
type Namespace struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Phase is the current lifecycle phase of the namespace.
	Phase v1.NamespacePhase `json:"phase"`
}

// GetNamespaceList returns a list of all namespaces in the cluster.
func GetNamespaceList(client kubernetes.Interface, dsQuery *dataselect.DataSelectQuery) (*NamespaceList, error) {
	namespaces, err := client.CoreV1().Namespaces().List(api.ListEverything)

	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	return toNamespaceList(namespaces.Items, nonCriticalErrors, dsQuery), nil
}

func toNamespaceList(namespaces []v1.Namespace, nonCriticalErrors []error, dsQuery *dataselect.DataSelectQuery) *NamespaceList {
	namespaceList := &NamespaceList{
		Namespaces: make([]Namespace, 0),
		ListMeta:   api.ListMeta{TotalItems: len(namespaces)},
	}

	namespaceCells, filteredTotal := dataselect.GenericDataSelectWithFilter(toCells(namespaces), dsQuery)
	namespaces = fromCells(namespaceCells)
	namespaceList.ListMeta = api.ListMeta{TotalItems: filteredTotal}
	namespaceList.Errors = nonCriticalErrors

	for _, namespace := range namespaces {
		namespaceList.Namespaces = append(namespaceList.Namespaces, toNamespace(namespace))
	}

	return namespaceList
}

func toNamespace(namespace v1.Namespace) Namespace {
	return Namespace{
		ObjectMeta: api.NewObjectMeta(namespace.ObjectMeta),
		TypeMeta:   api.NewTypeMetaWithApiVersion(api.ResourceKindNamespace, "v1"),
		Phase:      namespace.Status.Phase,
	}
}

func GetNamespaceListFromPermission(userClient, adminClient kubernetes.Interface) (*NamespaceList, error) {
    // 1. If user can list namespaces return all namespaces directly
    ns, _ := userClient.CoreV1().Namespaces().List(api.ListEverything)
    if len(ns.Items) > 0 {
    	return toNamespaceList(ns.Items, nil, dataselect.DefaultDataSelect), nil
	}

    // 2. If user can get namespace return all namespaces directly
	allNs, err := adminClient.CoreV1().Namespaces().List(api.ListEverything)
	if err != nil {
		return nil, err
	}
	ar, _ := userClient.AuthorizationV1().SelfSubjectAccessReviews().Create(
		&authorizationapi.SelfSubjectAccessReview{Spec:authorizationapi.SelfSubjectAccessReviewSpec{
			ResourceAttributes: &authorizationapi.ResourceAttributes{Resource: "namespace", Verb: "get"},
	}})
	if ar.Status.Allowed {
		return toNamespaceList(allNs.Items, nil, dataselect.DefaultDataSelect), nil
	}

	// 3.  user permission to filter namespaces
	userNamespaces := getPermissionNamespaces(allNs.Items, userClient)

	return toNamespaceList(userNamespaces, nil, dataselect.DefaultDataSelect), nil
}

func getPermissionNamespaces(namespaces []v1.Namespace, userClient kubernetes.Interface) []v1.Namespace {
	var wg sync.WaitGroup
	namespaceChannel := make(chan v1.Namespace, 1000)
	for _, n := range namespaces {
		wg.Add(1)
		go func(namespace v1.Namespace) {
			defer wg.Done()
			rr, err := userClient.AuthorizationV1().SelfSubjectRulesReviews().
				Create(&authorizationapi.SelfSubjectRulesReview{Spec:authorizationapi.SelfSubjectRulesReviewSpec{Namespace: namespace.Name}})
			if err != nil {
				return
			}
			if hasListPermission(rr.Status.ResourceRules) {
				namespaceChannel <- namespace
			}
		}(n)
	}
	wg.Wait()
	close(namespaceChannel)
	userNamespaces := make([]v1.Namespace, 0, len(namespaceChannel))
	for ns := range namespaceChannel {
		userNamespaces = append(userNamespaces, ns)
	}
	return userNamespaces
}

func hasListPermission(resourceRules []authorizationapi.ResourceRule) bool {
	for _, rule := range resourceRules {
		for _, v := range rule.Verbs {
			if v == "*" || v == "list" {
				return true
			}
		}
	}
	return false
}