// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package namespace

import (
	"link/src/backend/api"
	"link/src/backend/errors"
	"link/src/backend/resource/common"
	"link/src/backend/resource/dataselect"
	"link/src/backend/resource/event"
	"link/src/backend/resource/limitrange"
	rq "link/src/backend/resource/resourcequota"

	"link/src/backend/resource/overview"

	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sClient "k8s.io/client-go/kubernetes"
)

// NamespaceDetail is a presentation layer view of Kubernetes Namespace resource. This means it is Namespace plus
// additional augmented data we can get from other sources.
type NamespaceDetail struct {
	ObjectMeta api.ObjectMeta `json:"objectMeta"`
	TypeMeta   api.TypeMeta   `json:"typeMeta"`

	// Phase is the current lifecycle phase of the namespace.
	Phase v1.NamespacePhase `json:"phase"`

	// Events is list of events associated to the namespace.
	EventList common.EventList `json:"eventList"`

	PVCStatus         PVCStatus                 `json:"pvcStatus"`
	PodStatus         overview.PodStatus        `json:"podStatus"`
	DeploymentStatus  overview.ControllerStatus `json:"deploymentStatus"`
	DaemonSetStatus   overview.ControllerStatus `json:"daemonSetStatus"`
	StatefulSetStatus overview.ControllerStatus `json:"statefulSetStatus"`

	IngressCount    int `json:"ingressCount"`
	ServiceCount    int `json:"serviceCount"`
	ConfigMapCount  int `json:"configMapCount"`
	SecretCount     int `json:"secretCount"`
	ReplicaSetCount int `json:"replicaSetCount"`
	JobCount        int `json:"jobCount"`
	CronJobCount    int `json:"cronJobCount"`

	EffectiveQuotas      []rq.EffectiveQuota `json:"effectiveQuotas"`
	EffectiveLimitRanges []v1.LimitRangeItem `json:"effectiveLimitRanges"`

	// ResourceQuotaList is list of resource quotas associated to the namespace
	ResourceQuotas []rq.ResourceQuotaDetail `json:"resourceQuotas"`

	// ResourceLimits is list of limit ranges associated to the namespace
	ResourceLimits []limitrange.Detail `json:"resourceLimits"`

	// Data is the raw namespace in json
	Data *v1.Namespace `json:"data"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type PVCStatus struct {
	Total   int `json:"total"`
	Pending int `json:"pending"`
	Bound   int `json:"bound"`
	Failed  int `json:"failed"`
	Lost    int `json:"lost"`
}

func getPVCStatus(pvcs []v1.PersistentVolumeClaim, events []v1.Event) PVCStatus {
	result := PVCStatus{Total: len(pvcs)}
	for _, pvc := range pvcs {
		switch pvc.Status.Phase {
		case v1.ClaimLost:
			result.Lost += 1
		case v1.ClaimBound:
			result.Bound += 1
		case v1.ClaimPending:
			error := false
			for _, event := range events {
				if event.InvolvedObject.UID == pvc.UID && event.Type == v1.EventTypeWarning {
					result.Failed += 1
					error = true
					break
				}
			}
			if !error {
				result.Pending += 1
			}
		}
	}
	return result
}

// GetNamespaceDetail gets namespace details.
func GetNamespaceDetail(client k8sClient.Interface, name string) (*NamespaceDetail, error) {
	namespace, err := client.CoreV1().Namespaces().Get(name, metaV1.GetOptions{})
	if err != nil {
		return nil, err
	}

	channels := common.ResourceChannels{
		PodList:                   common.GetPodListChannel(client, common.NewSameNamespaceQuery(name), 1),
		ReplicaSetList:            common.GetReplicaSetListChannel(client, common.NewSameNamespaceQuery(name), 1),
		DeploymentList:            common.GetDeploymentListChannel(client, common.NewSameNamespaceQuery(name), 1),
		DaemonSetList:             common.GetDaemonSetListChannel(client, common.NewSameNamespaceQuery(name), 1),
		JobList:                   common.GetJobListChannel(client, common.NewSameNamespaceQuery(name), 1),
		CronJobList:               common.GetCronJobListChannel(client, common.NewSameNamespaceQuery(name), 1),
		StatefulSetList:           common.GetStatefulSetListChannel(client, common.NewSameNamespaceQuery(name), 1),
		PersistentVolumeClaimList: common.GetPersistentVolumeClaimListChannel(client, common.NewSameNamespaceQuery(name), 1),
		IngressList:               common.GetIngressListChannel(client, common.NewSameNamespaceQuery(name), 1),
		ServiceList:               common.GetServiceListChannel(client, common.NewSameNamespaceQuery(name), 1),
		ConfigMapList:             common.GetConfigMapListChannel(client, common.NewSameNamespaceQuery(name), 1),
		SecretList:                common.GetSecretListChannel(client, common.NewSameNamespaceQuery(name), 1),
		EventList:                 common.GetEventListChannel(client, common.NewSameNamespaceQuery(name), 1),
		ResourceQuotaList:         common.GetResourceQuotaListChannel(client, common.NewSameNamespaceQuery(name), 1),
		LimitRangeList:            common.GetLimitRangeListChannel(client, common.NewSameNamespaceQuery(name), 1),
	}

	eventList := <-channels.EventList.List
	err = <-channels.EventList.Error
	if err != nil {
		return nil, err
	}

	podList := <-channels.PodList.List
	err = <-channels.PodList.Error
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	deploymentList := <-channels.DeploymentList.List
	err = <-channels.DeploymentList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	replicaSetList := <-channels.ReplicaSetList.List
	err = <-channels.ReplicaSetList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	statefulSetList := <-channels.StatefulSetList.List
	err = <-channels.StatefulSetList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	daemonSetList := <-channels.DaemonSetList.List
	err = <-channels.DaemonSetList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	jobList := <-channels.JobList.List
	err = <-channels.JobList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	cronJobList := <-channels.CronJobList.List
	err = <-channels.CronJobList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	pvcList := <-channels.PersistentVolumeClaimList.List
	err = <-channels.PersistentVolumeClaimList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	ingressList := <-channels.IngressList.List
	err = <-channels.IngressList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	serviceList := <-channels.ServiceList.List
	err = <-channels.ServiceList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	configMapList := <-channels.ConfigMapList.List
	err = <-channels.ConfigMapList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	secretList := <-channels.SecretList.List
	err = <-channels.SecretList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	quotaList := <-channels.ResourceQuotaList.List
	err = <-channels.ResourceQuotaList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	limitRangeList := <-channels.LimitRangeList.List
	err = <-channels.LimitRangeList.Error
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	namespaceEvents := make([]v1.Event, 0)
	for _, e := range eventList.Items {
		if e.InvolvedObject.UID == namespace.UID {
			namespaceEvents = append(namespaceEvents, e)
		}
	}

	resourceQuotas := getResourceQuotas(quotaList.Items)
	effectiveQuotas := rq.ComputeEffectiveQuota(quotaList.Items)

	resourceLimits := getLimitRanges(limitRangeList.Items)
	effectiveLimitRanges := limitrange.ComputeEffectiveLimitRange(limitRangeList.Items)

	return &NamespaceDetail{
		ObjectMeta:           api.NewObjectMeta(namespace.ObjectMeta),
		TypeMeta:             api.NewTypeMetaWithApiVersion(api.ResourceKindNamespace, "v1"),
		Phase:                namespace.Status.Phase,
		EventList:            event.CreateEventList(namespaceEvents, dataselect.DefaultDataSelect),
		IngressCount:         len(ingressList.Items),
		ServiceCount:         len(serviceList.Items),
		ConfigMapCount:       len(configMapList.Items),
		SecretCount:          len(secretList.Items),
		PodStatus:            *overview.GetPodStatus(podList.Items),
		DeploymentStatus:     *overview.GetDeploymentControllerStatus(deploymentList.Items, replicaSetList.Items, podList.Items, eventList.Items),
		StatefulSetStatus:    *overview.GetStatefulSetControllerStatus(statefulSetList.Items, podList.Items, eventList.Items),
		DaemonSetStatus:      *overview.GetDaemonSetControllerStatus(daemonSetList.Items, podList.Items, eventList.Items),
		PVCStatus:            getPVCStatus(pvcList.Items, eventList.Items),
		ReplicaSetCount:      len(replicaSetList.Items),
		JobCount:             len(jobList.Items),
		CronJobCount:         len(cronJobList.Items),
		ResourceQuotas:       resourceQuotas,
		EffectiveQuotas:      effectiveQuotas,
		EffectiveLimitRanges: effectiveLimitRanges,
		ResourceLimits:       resourceLimits,
		Data:                 namespace,
		Errors:               nonCriticalErrors,
	}, nil
}

func getResourceQuotas(quotas []v1.ResourceQuota) []rq.ResourceQuotaDetail {
	result := make([]rq.ResourceQuotaDetail, 0, len(quotas))
	for _, item := range quotas {
		detail := rq.ToResourceQuotaDetail(&item)
		result = append(result, *detail)
	}
	return result
}

func getLimitRanges(limitRanges []v1.LimitRange) []limitrange.Detail {
	resourceLimits := make([]limitrange.Detail, 0)
	for _, item := range limitRanges {
		resourceLimits = append(resourceLimits, limitrange.ToDetail(&item))
	}
	return resourceLimits
}
