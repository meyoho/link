// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"crypto/elliptic"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"

	_ "net/http/pprof"

	"k8s.io/klog"
	"link/src/backend/args"
	"link/src/backend/auth"
	authApi "link/src/backend/auth/api"
	"link/src/backend/cert"
	"link/src/backend/cert/ecdsa"
	"link/src/backend/client"
	clientapi "link/src/backend/client/api"
	"link/src/backend/handler"
	"link/src/backend/settings"
	settingApi "link/src/backend/settings/api"

	"bitbucket.org/mathildetech/themex"
	thandler "bitbucket.org/mathildetech/themex/handler"
	"github.com/emicklei/go-restful"
	"github.com/emicklei/go-restful-openapi"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/pflag"
)

var (
	argInsecurePort        = pflag.Int("insecure-port", 9090, "The port to listen to for incoming HTTP requests.")
	argPort                = pflag.Int("port", 8443, "The secure port to listen to for incoming HTTPS requests.")
	argInsecureBindAddress = pflag.IP("insecure-bind-address", net.IPv4(0, 0, 0, 0), "The IP address on which to serve the --port (set to 0.0.0.0 for all interfaces).")
	argBindAddress         = pflag.IP("bind-address", net.IPv4(0, 0, 0, 0), "The IP address on which to serve the --secure-port (set to 0.0.0.0 for all interfaces).")
	argDefaultCertDir      = pflag.String("default-cert-dir", "/certs", "Directory path containing '--tls-cert-file' and '--tls-key-file' files. Used also when auto-generating certificates flag is set.")
	argCertFile            = pflag.String("tls-cert-file", "", "File containing the default x509 Certificate for HTTPS.")
	argKeyFile             = pflag.String("tls-key-file", "", "File containing the default x509 private key matching --tls-cert-file.")
	argApiserverHost       = pflag.String("apiserver-host", "", "The address of the Kubernetes Apiserver "+
		"to connect to in the format of protocol://address:port, e.g., "+
		"http://localhost:8080. If not specified, the assumption is that the binary runs inside a "+
		"Kubernetes cluster and local discovery is attempted.")
	argKubeConfigFile            = pflag.String("kubeconfig", "", "Path to kubeconfig file with authorization and master location information.")
	argAutoGenerateCertificates  = pflag.Bool("auto-generate-certificates", false, "When set to true, Dashboard will automatically generate certificates used to serve HTTPS. Default: false.")
	argEnableAnonymous           = pflag.Bool("enable-anonymous", false, "When enabled this settings will use the kubeconfig auth info or service account info instead of user login")
	argEnableTheme               = pflag.Bool("enable-theme", true, "When enabled this settings will be able to customize the theme")
)

func main() {
	klogFlags := flag.NewFlagSet("klog", flag.ExitOnError)
	klog.InitFlags(klogFlags)
	klog.SetOutputBySeverity("INFO", os.Stdout)
	defer klog.Flush()

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	flag.CommandLine.Parse(make([]string, 0)) // Init for glog calls in kubernetes packages

	// Initializes dashboard arguments holder so we can read them in other packages
	initArgHolder()

	if args.Holder.GetApiServerHost() != "" {
		klog.Infof("Using apiserver-host location: %s", args.Holder.GetApiServerHost())
	}
	if args.Holder.GetKubeConfigFile() != "" {
		klog.Infof("Using kubeconfig file: %s", args.Holder.GetKubeConfigFile())
	}

	if args.Holder.GetEnableTheme() {
		go InitTheme(args.Holder.GetKubeConfigFile())
	}

	clientManager := client.NewClientManager(args.Holder.GetKubeConfigFile(), args.Holder.GetApiServerHost(), args.Holder.GetEnableAnonymous())
	versionInfo, err := clientManager.InsecureClient().Discovery().ServerVersion()
	if err != nil {
		klog.Fatalf("initial request to the apiserver failed %v", err)
	}

	klog.Infof("Successful initial request to the apiserver, version: %s", versionInfo.String())

	// Init settings manager
	settingsManager := settings.NewSettingsManager(clientManager)

	// Init K8s CustomResourceDefinition
	if err := authApi.InitK8sCRD(clientManager); err != nil {
		klog.Errorf("initial crd failed %v", err)
	}

	// TODO: Change to use a independent approach
	// Init auth manager
	authManager, err := initAuthManager(clientManager, settingsManager)
	if err != nil {
		klog.Errorf("initial auth client failed %v", err)
	}

	apiHandler, err := handler.CreateHTTPAPIHandler(
		clientManager,
		authManager)
	if err != nil {
		klog.Fatalf("initial http handler failed %v", err)
	}

	if args.Holder.GetAutoGenerateCertificates() {
		klog.Infof("Auto-generating certificates")
		certCreator := ecdsa.NewECDSACreator(args.Holder.GetKeyFile(), args.Holder.GetCertFile(), elliptic.P256())
		certManager := cert.NewCertManager(certCreator, args.Holder.GetDefaultCertDir())
		certManager.GenerateCertificates()
	}

	// Run a HTTP server that serves static public files from './public' and handles API calls.
	http.Handle("/", handler.MakeGzipHandler(thandler.CreateLocaleHandler()))
	http.Handle("/api/", apiHandler)
	http.Handle("/api/appConfig.json", handler.NewAppConfigHandler(clientManager))
	http.Handle("/api/sockjs/", handler.CreateAttachHandler("/api/sockjs"))
	http.Handle("/metrics", promhttp.Handler())

	config := restfulspec.Config{
		WebServices: apiHandler.(*restful.Container).RegisteredWebServices(),
		APIPath:     "/apidocs.json"}
	restful.DefaultContainer.Add(restfulspec.NewOpenAPIService(config))
	http.Handle("/apidocs/", http.StripPrefix("/apidocs/", http.FileServer(http.Dir("swagger-ui"))))

	// Listen for http or https
	if args.Holder.GetCertFile() != "" && args.Holder.GetKeyFile() != "" {
		certFilePath := args.Holder.GetDefaultCertDir() + string(os.PathSeparator) + args.Holder.GetCertFile()
		keyFilePath := args.Holder.GetDefaultCertDir() + string(os.PathSeparator) + args.Holder.GetKeyFile()
		klog.Infof("Serving securely on HTTPS port: %d", args.Holder.GetPort())
		secureAddr := fmt.Sprintf("%s:%d", args.Holder.GetBindAddress(), args.Holder.GetPort())
		go func() { klog.Fatal(http.ListenAndServeTLS(secureAddr, certFilePath, keyFilePath, nil)) }()
	} else {
		klog.Infof("Serving insecurely on HTTP port: %d", args.Holder.GetInsecurePort())
		addr := fmt.Sprintf("%s:%d", args.Holder.GetInsecureBindAddress(), args.Holder.GetInsecurePort())
		go func() { klog.Fatal(http.ListenAndServe(addr, nil)) }()
	}
	select {}
}

func initAuthManager(clientManager clientapi.ClientManager, settingsManager settingApi.SettingsManager) (authApi.Manager, error) {
	// TODO: refactor
	authManager := auth.NewAuthManager(clientManager, settingsManager, nil)
	return authManager, nil
}

func initArgHolder() {
	builder := args.GetHolderBuilder()
	builder.SetInsecurePort(*argInsecurePort)
	builder.SetPort(*argPort)
	builder.SetInsecureBindAddress(*argInsecureBindAddress)
	builder.SetBindAddress(*argBindAddress)
	builder.SetDefaultCertDir(*argDefaultCertDir)
	builder.SetCertFile(*argCertFile)
	builder.SetKeyFile(*argKeyFile)
	builder.SetApiServerHost(*argApiserverHost)
	builder.SetKubeConfigFile(*argKubeConfigFile)
	builder.SetAutoGenerateCertificates(*argAutoGenerateCertificates)
	builder.SetEnableAnonymous(*argEnableAnonymous)
	builder.SetEnableTheme(*argEnableTheme)
}

func InitTheme(configPath string) {
	err := themex.StartWithConfigure(&themex.Config{KubeConfigFile: configPath})
	if err != nil {
		klog.Errorf("err: %v", err)
	}
	return
}
