// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"bitbucket.org/mathildetech/themex"
)

// ListConfigruation returns a list of all configuation.
func ListConfiguration(configType string) (result interface{}, err error) {
	switch configType {
	case TypeTheme:
    result, err = themex.RetrieveTheme()
	case TypePlatform:
		// platform configuration does not support list, just retrieve
		err = themex.ErrNotSupportedAction
	case TypeLogo:
    result, err = themex.ListLogos()
	case TypeLocalization:
    result, err = themex.ListLocalizations()
	case TypeProduct:
    result, err = themex.ListProducts()
	case TypeBanner:
    result, err = themex.ListBanners()
	default :
		err = themex.ErrNotSupportedType
	}

	return
}
