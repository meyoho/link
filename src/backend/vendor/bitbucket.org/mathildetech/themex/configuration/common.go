package configuration

const (
	TypeTheme        = "theme"
	TypePlatform     = "platform"
	TypeLogo         = "logo"
	TypeLocalization = "localization"
	TypeProduct      = "product"
	TypeBanner       = "banner"
)

type Configuration struct {
	Name string `json:"name"`
	Type string `json:"type"`
	Data interface{} `json:"data"`

	Errors  []error `json:"errors"`
}
