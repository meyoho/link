// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"bitbucket.org/mathildetech/themex"
)

// GetConfiguration returns specified configuration with name.
func GetConfiguration(configType, name string) (result interface{}, err error) {
	switch configType {
	case TypeTheme:
    result, err = themex.RetrieveTheme()
	case TypePlatform:
    result, err = themex.RetrievePlatformConfigurations(name)
	case TypeLogo:
    result, err = themex.RetrieveLogo(name)
	case TypeLocalization:
		err = themex.ErrNotSupportedAction
		// config, err := themex.RetrieveLocalization(name)
	case TypeProduct:
    result, err = themex.RetrieveProduct(name)
	case TypeBanner:
		err = themex.ErrNotSupportedAction
		// config, err := themex.RetrieveBanner(name)
	default :
		err = themex.ErrNotSupportedType
	}

	return
}
