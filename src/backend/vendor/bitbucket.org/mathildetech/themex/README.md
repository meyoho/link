AlaudaCustomThemeClient
==============
AlaudaCustomThemeClient 是提供Alauda小平台自定义主题配置的组件。项目架构拷贝自[pass](https://bitbucket.org/mathildetech/pass)

为了尽量复用代码，在这个项目中与下列类库有强的依赖关系。

* [Kubernetes go-client](https://github.com/kubernetes/client-go) — k8s 客户端。


安装
==============

### 手动安装

1. 执行 `go get -u bitbucket.org/mathildetech/themex` 将代码包下载到你的工作目录。
2. 导入 `bitbucket.org/mathildetech/themex`。
3. 执行 `govendor add +external` 将代码包加入工程vendor目录


文档
==============
你可以在 [Confluence](http://confluence.alaudatech.com/pages/viewpage.action?pageId=33294890) 查看产品需求及技术方案


Client内配置项类型
==============
Client支持以下配置项类型:

- Theme
- Logo
- Localization
- Image
- Banner
- AlaudaProduct


错误类型
==============
业务逻辑中可以用以下错误类型做判断:

```
ErrClientNil            // Fail to init client

ErrNotSupportedAction   // Not supported action on configuration
ErrNotSupportedType     // Specified type isn't supported

ErrThemeNil             // Theme does not exist
ErrThemeInvalid         // Theme provided is invalid(used when update)

ErrLogoNotExist         // Specified logo does not exist
ErrLocalizationNotExist // Specified localization does not exist
ErrImageNoData          // Specified image does not exist
ErrBannerNotExist       // Specified banner does not exist
ErrProductNotExist      // Specified product does not exist
```

如何使用
==============

- 初始化client, 获取acp-devops平台配置

```
    import "bitbucket.org/mathildetech/themex"
    ...
    // init custom theme client
	err := themex.StartWithConfigure(&themex.Config{})
	if err != nil {
		log.Println("err:", err)
	}

	// retrieve platform configuration
	platform, err := themex.RetrievePlatformConfigurations("acp-devops)
	if err != nil {
		log.Println("err:", err)
	} else {
		log.Println("get platform configuration success")
	}
```

- 自定义开发环境的配置，指定k8s config file，或指定minikube环境

```
	err := themex.StartWithConfigure(&pass.Config{AppID: pass.AlaudaKubernetes,  KubeConfigFile: args.Holder.GetKubeConfigFile(), InMinikube: true})
```


方法
==============
客户端初始化方法
```
StartWithConfigure(config *Config) error
```
获取主题配置
```
RetrieveTheme() (*Theme, error)
```
更新主题配置
```
UpdateTheme(theme *Theme) (*Theme, error)
```
获得所有logo数据
```
ListLogos() ([]*Logo, error)
```
获得指定name的logo数据
```
RetrieveLogo(name string) (*Logo, error)
```
获取所有localization数据
```
ListLocalizations() ([]*Localization, error)
```
更新指定localization数据
```
UpdateLocalization(local *Localization) (*Localization, error)
```
获取指定image信息
```
RetrieveImage(imgType, name string) (*Image, error)
```
更新指定image信息
```
UpdateImage(img *Image) (*Image, error)
```
删除指定图片信息
```
DeleteImage(imgType, name string) error
```
获取所有banner信息
```
ListBanners() ([]*Banner, error)
```
更新指定banner
```
UpdateBanner(banner *Banner) (*Banner, error)
```
删除指定name的banner
```
DeleteBanner(name string) error
```
获取所有product信息
```
ListProducts() ([]*AlaudaProduct, error)
```
获取指定product
```
RetrieveProduct(name string) (*AlaudaProduct, error)
```
更新product
```
UpdateProduct(product *AlaudaProduct) (*AlaudaProduct, error)
```


Note： Logo，AlaudaProduct（icon）中只保存图片的引用信息，图片的二进制数据需要通过RetrieveImage获取string后自行转换下
Note： 更新AlaudaProduct时，暂时不支持更新label