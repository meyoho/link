package api

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"bitbucket.org/mathildetech/themex/products/clientset/v1alpha1"
)

type Manager struct {
	kubeConfig *rest.Config
	inMinikube bool
}

func NewForConfig(c *rest.Config, inMinikube bool) *Manager {
	return &Manager{kubeConfig: c, inMinikube: inMinikube}
}

func (c *Manager) Client() (kubernetes.Interface, error) {
	client, err := kubernetes.NewForConfig(c.kubeConfig)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (c *Manager) ProductClient() (*v1alpha1.PortalV1Alpha1Client, error) {
	client, err := v1alpha1.NewForConfig(c.kubeConfig)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (c *Manager) DynamicClient(groupVersion *schema.GroupVersion) (*rest.RESTClient, error) {
	client, err := newClient(c.kubeConfig, groupVersion)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (c *Manager) GetConfigMap(k8sClient kubernetes.Interface, namespace, name string, options metav1.GetOptions) (*corev1.ConfigMap, error) {
	cm, err := k8sClient.CoreV1().ConfigMaps(namespace).Get(name, options)

	if err != nil {
		return nil, err
	}

	return cm, nil
}

func (c *Manager) UpdateConfigMap(k8sClient kubernetes.Interface, namespace string, cm *corev1.ConfigMap) (*corev1.ConfigMap, error) {
	result, err := k8sClient.CoreV1().ConfigMaps(namespace).Update(cm)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (c *Manager) DeleteConfigMap(k8sClient kubernetes.Interface, namespace, name string, options *metav1.DeleteOptions) error {
	return k8sClient.CoreV1().ConfigMaps(namespace).Delete(name, options)
}