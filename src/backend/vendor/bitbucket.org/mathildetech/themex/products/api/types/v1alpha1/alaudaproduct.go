package v1alpha1

import (
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
  "strconv"
)

// Definition of our CRD Example class
type AlaudaProduct struct {
  metav1.TypeMeta   `json:",inline"`
  metav1.ObjectMeta `json:"metadata,omitempty"`
  Spec               AlaudaProductSpec   `json:"spec"`
}

type AlaudaProductSpec struct {
  Displayname AlaudaProductLang `json:"displayname"`
  Description AlaudaProductLang `json:"description"`
  CrdVersion string    `json:"crdversion"`
  Icon string     `json:"icon"`
  Homepage string   `json:"homepage"`
  Target string   `json:"target"`
}

type AlaudaProductLang struct {
  CN string `json:"cn,omitempty"`
  EN string `json:"en,omitempty"`
}

type AlaudaProductList struct {
  metav1.TypeMeta `json:",inline"`
  metav1.ListMeta `json:"metadata,omitempty"`
  Items            AlaudaProductItems `json:"items"`
}

type AlaudaProductItems []AlaudaProduct

func (items AlaudaProductItems) Len() int {
  return len(items)
}

func (items AlaudaProductItems) Less(i, j int) bool {
  index_i,_ := strconv.Atoi(items[i].Labels["alauda.io/product.index"])
  index_j,_ := strconv.Atoi(items[j].Labels["alauda.io/product.index"])

  if index_i < index_j {
    return true
  } else if index_i > index_j {
    return false
  } else {
    return items[i].Name < items[j].Name
  }
}

func (items AlaudaProductItems) Swap(i, j int) {
  var temp AlaudaProduct = items[i]
  items[i] = items[j]
  items[j] = temp
}

