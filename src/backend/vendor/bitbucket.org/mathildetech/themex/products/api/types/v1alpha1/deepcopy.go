package v1alpha1

import "k8s.io/apimachinery/pkg/runtime"

// DeepCopyInto copies all properties of this object into another object of the
// same type that is provided as a pointer.
func (in *AlaudaProduct) DeepCopyInto(out *AlaudaProduct) {
  out.TypeMeta = in.TypeMeta
  out.ObjectMeta = in.ObjectMeta
  out.Spec = AlaudaProductSpec{
    Displayname: in.Spec.Displayname,
    Description: in.Spec.Description,
    Icon: in.Spec.Icon,
    Homepage: in.Spec.Homepage,
    Target: in.Spec.Target,
    CrdVersion: in.Spec.CrdVersion,
  }
}

// DeepCopyObject returns a generically typed copy of an object
func (in *AlaudaProduct) DeepCopyObject() runtime.Object {
  out := AlaudaProduct{}
  in.DeepCopyInto(&out)
  return &out
}

func (in *AlaudaProductList)DeepCopyObject() runtime.Object {
  out := AlaudaProductList{}
  out.TypeMeta = in.TypeMeta
  out.ListMeta = in.ListMeta

  if in.Items != nil {
    out.Items = make(AlaudaProductItems, len(in.Items))
    for i := range in.Items {
      in.Items[i].DeepCopyInto(&out.Items[i])
    }
  }
  return &out
}
