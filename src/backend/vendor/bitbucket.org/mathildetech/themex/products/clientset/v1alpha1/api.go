package v1alpha1

import (
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"bitbucket.org/mathildetech/themex/products/api/types/v1alpha1"
)

type PortalV1Alpha1Interface interface {
	Projects(namespace string) ProjectInterface
	NewListWatch() *cache.ListWatch
}

type PortalV1Alpha1Client struct {
	restClient rest.Interface
}

func NewForConfig(c *rest.Config) (*PortalV1Alpha1Client, error) {
	config := *c
	config.ContentConfig.GroupVersion = &schema.GroupVersion{Group: v1alpha1.GroupName, Version: v1alpha1.GroupVersion}
	config.APIPath = "/apis"
	if config.NegotiatedSerializer == nil {
		config.NegotiatedSerializer = serializer.DirectCodecFactory{CodecFactory: scheme.Codecs}
	}
	if len(config.UserAgent) == 0 {
		config.UserAgent = rest.DefaultKubernetesUserAgent()
	}
	config.ContentType = runtime.ContentTypeJSON

	client, err := rest.RESTClientFor(&config)
	if err != nil {
		return nil, err
	}

	return &PortalV1Alpha1Client{restClient: client}, nil
}

func (c *PortalV1Alpha1Client) Projects(namespace string) ProjectInterface {
	return &projectClient{
		restClient: c.restClient,
		ns:         namespace,
	}
}

// Create a new List watch for our TPR
func (c *PortalV1Alpha1Client) NewListWatch() *cache.ListWatch {
	return cache.NewListWatchFromClient(c.restClient, "alaudaproducts", "", fields.Everything())
}
