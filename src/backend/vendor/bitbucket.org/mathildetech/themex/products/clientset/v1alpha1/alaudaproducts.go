package v1alpha1


import (
  "bitbucket.org/mathildetech/themex/products/api/types/v1alpha1"
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
  "k8s.io/apimachinery/pkg/watch"
  "k8s.io/client-go/kubernetes/scheme"
  "k8s.io/client-go/rest"
)

type ProjectInterface interface {
  List(opts metav1.ListOptions) (*v1alpha1.AlaudaProductList, error)
  Get(name string, options metav1.GetOptions) (*v1alpha1.AlaudaProduct, error)
  Create(*v1alpha1.AlaudaProduct) (*v1alpha1.AlaudaProduct, error)
  Watch(opts metav1.ListOptions) (watch.Interface, error)
  Update(obj *v1alpha1.AlaudaProduct) (*v1alpha1.AlaudaProduct, error)
  // ...
}

type projectClient struct {
  restClient rest.Interface
  ns         string
}

func (c *projectClient) List(opts metav1.ListOptions) (*v1alpha1.AlaudaProductList, error) {
  result := v1alpha1.AlaudaProductList{}
  err := c.restClient.
    Get().
    Namespace(c.ns).
    Resource("alaudaproducts").
    VersionedParams(&opts, scheme.ParameterCodec).
    Do().
    Into(&result)

  return &result, err
}

func (c *projectClient) Get(name string, opts metav1.GetOptions) (*v1alpha1.AlaudaProduct, error) {
  result := v1alpha1.AlaudaProduct{}
  err := c.restClient.
    Get().
    Namespace(c.ns).
    Resource("alaudaproducts").
    Name(name).
    VersionedParams(&opts, scheme.ParameterCodec).
    Do().
    Into(&result)

  return &result, err
}

func (c *projectClient) Create(project *v1alpha1.AlaudaProduct) (*v1alpha1.AlaudaProduct, error) {
  result := v1alpha1.AlaudaProduct{}
  err := c.restClient.
    Post().
    Namespace(c.ns).
    Resource("alaudaproducts").
    Body(project).
    Do().
    Into(&result)

  return &result, err
}

func (c *projectClient) Watch(opts metav1.ListOptions) (watch.Interface, error) {
  opts.Watch = true
  return c.restClient.
    Get().
    Namespace(c.ns).
    Resource("alaudaproducts").
    VersionedParams(&opts, scheme.ParameterCodec).
    Watch()
}

func (c *projectClient) Update(obj *v1alpha1.AlaudaProduct) (*v1alpha1.AlaudaProduct, error) {
  var result v1alpha1.AlaudaProduct
  err := c.restClient.Put().
    Namespace(c.ns).
    Resource("alaudaproducts").
    Body(obj).
    Do().
    Into(&result)
  return &result, err
}
