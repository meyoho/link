package handler

import (
	"net/http"
	"github.com/emicklei/go-restful"
	"bitbucket.org/mathildetech/themex/configuration"
)

// APIHandler manages all endpoints related to configuration.
type APIHandler struct {
	Module string
}

func NewAPIHandler(module string) *APIHandler{
	return &APIHandler{
		Module: module,
	}
}

// Install creates new endpoints for dashboard auth, such as login. It allows user to log in to dashboard using
// one of the supported methods. See AuthManager and Authenticator for more information.
func (apiHandler *APIHandler) Install(ws *restful.WebService) {
	ws.Route(
		ws.GET("/configuration").
			To(apiHandler.handleConfiguration).
			Writes(configuration.Configuration{}))

}

// Configuration diagnose
func (apiHandler *APIHandler) handleConfiguration(request *restful.Request, response *restful.Response) {
	configType := request.QueryParameter("type")
	name := request.QueryParameter("name")

	var result configuration.Configuration
	result.Name = name
	result.Type = configType

	var (
		data interface{}
		err  error
	)

	if name != "" {
		data, err = configuration.GetConfiguration(configType, name)
	} else {
		data, err = configuration.ListConfiguration(configType)
	}
	result.Data = data
	result.Errors = []error{}
	if err != nil {
		result.Errors = []error{err}
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}
