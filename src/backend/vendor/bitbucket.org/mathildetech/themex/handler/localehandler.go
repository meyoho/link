// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/text/language"
	"log"

	"bitbucket.org/mathildetech/themex"
	"encoding/base64"
)

const defaultLocaleDir = "en"
const assetsDir = "public"

// Localization is a spec for the localization configuration of dashboard.
type Localization struct {
	Translations []Translation `json:"translations"`
}

// Translation is a single translation definition spec.
type Translation struct {
	File string `json:"file"`
	Key  string `json:"key"`
}

// LocaleHandler serves different localized versions of the frontend application
// based on the Accept-Language header.
type LocaleHandler struct {
	SupportedLocales []language.Tag
}

// CreateLocaleHandler loads the localization configuration and constructs a LocaleHandler.
func CreateLocaleHandler() *LocaleHandler {
	locales, err := getSupportedLocales("./locale_conf.json")
	if err != nil {
		log.Printf("Error when loading the localization configuration. Dashboard will not be localized. %s", err)
		locales = []language.Tag{}
	}
	return &LocaleHandler{SupportedLocales: locales}
}

func getSupportedLocales(configFile string) ([]language.Tag, error) {
	// read config file
	localesFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return []language.Tag{}, err
	}

	// unmarshall
	localization := Localization{}
	err = json.Unmarshal(localesFile, &localization)
	if err != nil {
		log.Printf("%s %s", string(localesFile), err)
	}

	// filter locale keys
	result := []language.Tag{}
	for _, translation := range localization.Translations {
		result = append(result, language.Make(translation.Key))
	}
	return result, nil
}

// getAssetsDir determines the absolute path to the localized frontend assets
func getAssetsDir() string {
	path, err := os.Executable()
	if err != nil {
		log.Fatalf("Error determining path to executable: %#v", err)
	}
	path, err = filepath.EvalSymlinks(path)
	if err != nil {
		log.Fatalf("Error evaluating symlinks for path '%s': %#v", path, err)
	}
	return filepath.Join(filepath.Dir(path), assetsDir)
}

func dirExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			log.Printf(name)
			return false
		}
	}
	return true
}

// LocaleHandler serves different html versions based on the Accept-Language header.
func (handler *LocaleHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.EscapedPath() == "/" || r.URL.EscapedPath() == "/index.html" {
		// Do not store the html page in the cache. If the user is to click on 'switch language',
		// we want a different index.html (for the right locale) to be served when the page refreshes.
		w.Header().Add("Cache-Control", "no-store")
	} else {
		w.Header().Add("Cache-Control", "public, max-age=31536000")
	}
	acceptLanguage := os.Getenv("ACCEPT_LANGUAGE")
	if acceptLanguage == "" {
		acceptLanguage = r.Header.Get("Accept-Language")
	}
	dirName := handler.determineLocalizedDir(acceptLanguage)

	// 1. check if request image(logo/banner/icon)
	// 2. if so, try get data from configmap
	// 3. if success, then write image data to response
	// 3. if failed, then using the default http.FileServer

	imgType, imgName := getImgTypeName(r.URL.Path)
	log.Println("imgType, imgName", imgType, imgName)

	if imgType != "" && imgName != "" {
		image, err := themex.RetrieveImage(imgType, imgName)
		log.Println("Get image from configmap: ", image, err)
		if err == nil {
			w.Header().Set("Content-Type", image.ContentType)

			if image.ContentType != themex.ImageContentTypeSvg {
				imgData, err := base64.StdEncoding.DecodeString(image.Data)
				if err == nil {
					w.Write(imgData)
					return
				} else {
					log.Println("Invalid base64 encoded data from themex: ", err)
				}
			} else {
				w.Write([]byte(image.Data))
				return
			}
		}
	}

	http.FileServer(http.Dir(dirName)).ServeHTTP(w, r)
}

func (handler *LocaleHandler) determineLocalizedDir(locale string) string {
	assetsDir := getAssetsDir()
	defaultDir := filepath.Join(assetsDir, defaultLocaleDir)
	tags, _, err := language.ParseAcceptLanguage(locale)
	if (err != nil) || (len(tags) == 0) {
		return defaultDir
	}

	locales := handler.SupportedLocales
	tag, _, confidence := language.NewMatcher(locales).Match(tags...)
	matchedLocale := strings.ToLower(tag.String())
	if confidence != language.Exact {
		matchedLocale = ""
		for _, l := range locales {
			base, _ := tag.Base()
			if l.String() == base.String() {
				matchedLocale = l.String()
			}
		}
	}

	localeDir := filepath.Join(assetsDir, matchedLocale)
	if matchedLocale != "" && dirExists(localeDir) {
		return localeDir
	}
	return defaultDir
}

func getImgTypeName(upath string) (imgType, imgName string) {
	// valid upath = "/assets/logos/aks.timestamp"
	// upaths = [ "assets", "logs", "aks.timestamp" ]
	upaths := strings.Split(strings.Trim(upath, "/"), "/")

	log.Println("upaths: ", upaths, len(upaths))

	if len(upaths) < 2 {
		return
	}

	imgType = strings.ToLower(upaths[len(upaths)-2])
	// imgType in singular format: banners, logos, products
	imgType = strings.TrimSuffix(imgType, "s")
	if !contains(imgType, themex.SupportedImgType) {
		return
	}

	imgName = strings.ToLower(upaths[len(upaths)-1])
	if !strings.Contains(imgName, ".") {
		return
	}

	// aks.timestamp will only return aks
	imgName = imgName[:strings.LastIndex(imgName, ".")]

	return

}

func contains(str string, items []string) bool{
	for _, item := range items {
		if item == str {
			return true
		}
	}

	return false
}
