package themex

import (
	"errors"
)

const (
	// app names definition
	AlaudaPortal          = "alauda-portal"
	AlaudaKubernetes      = "alauda-kubernetes"
	ACPGroup              = "acp-group"
	AlaudaMachineLearning = "alauda-machine-learning"

	// Portal
	PortalNamespace       = "alauda-system"
	PortalConfigName      = "portal-configmap"
	PortalTitle           = "Products Portal"

	// Dex
	DexTitle              = "Product Login"

	ThemeKey              = "theme.yaml"
	LogoKey               = "logos.yaml"
	LocalizationKey       = "localization.yaml"
	BannerKey             = "configBanners.yaml"

	ImageKey              = "image"
	ImageContentTypeKey   = "content_type"
	ImageContentTypeSvg   = "image/svg+xml"
	ImageContentTypeOther = "application/octet-stream"

	// Client Version
	ClientVersion = "v1.0.0"

)


var (
	// SupportedImgType
	SupportedImgType      = []string{"banner", "logo", "product", "icon"}
)

// errors definition
var (
	ErrClientNil            = errors.New("Fail to init client")

	ErrNotSupportedAction   = errors.New("Not supported action on configuration")
	ErrNotSupportedType     = errors.New("Specified type isn't supported")

	ErrThemeNil             = errors.New("Theme is not configured")
	ErrThemeInvalid         = errors.New("Theme provided is invalid")
	ErrLogoNotExist         = errors.New("Specified logo does not exist")
	ErrLocalizationNotExist = errors.New("Specified localization does not exist")
	ErrImageNotExist         = errors.New("Specified image does not exist")
	ErrBannerNotExist       = errors.New("Specified banner does not exist")
	ErrProductNotExist      = errors.New("Specified product does not exist")
)

// theme info will be stored in the 「portal-configmap」 with key theme
type Theme struct {
	Name  string `json:"name",yaml:"name"`
	Color string `json:"color",yaml:"color"`
}

type Logo struct {
	Name string `json:"name",yaml:"name"`
	Url  string `json:"url",yaml:"url"`
}

type Localization struct {
	Name string `json:"name",yaml:"name"`
	ZH   string `json:"zh",yaml:"zh"`
	EN   string `json:"en",yaml:"en"`
}

type AlaudaProduct struct {
	Name          string            `json:"name"`
	Icon          string            `json:"icon"`
	Description   AlaudaProductLang `json:"description"`
	Version       string            `json:"version"`
	DisplayName   AlaudaProductLang `json:"displayname"`
	Homepage      string            `json:"homepage"`
	Target        string            `json:"target"`
	IsDeployed    bool              `json:"is_deployed"`
	Sort          string            `json:"sort"`
	Provider      string            `json:"provider"`
	LicenseStatus string            `json:"license_status"`
}

type AlaudaProductLang struct {
	CN string `json:"zh"`
	EN string `json:"en"`
}

// banner struct
type Banner struct {
	Name        string `json:"name",yaml:"name"`
	Image       string `json:"image",yaml:"image"`
	Url         string `json:"url",yaml:"url"`
	Description string `json:"description",yaml:"url"`
}

type Image struct {
	Type        string `json:"type"`
	Name        string `json:"name"`
	ContentType string `json:"content_type"`
	Data        string `json:"data"`
}

func NewImage(imgType, name, data, contentType string) *Image {
	if data == "" {
		return nil
	}

	return &Image{
		Type:        imgType,
		Data:        data,
		Name:        name,
		ContentType: contentType,
	}
}

type Platform struct {
	Name string `json:"name"`  // name of the platform
	Logo string `json:"logo"`  // path of the logo
	ThemeName string `json:"theme_name"`  // name of the theme
	ThemeColor string `json:"theme_color"`  // color of the theme
	Title string `json:"title"`             // title of the platform
	Favicon string `json:"favicon"`         // path of favicon
}

type Config struct {
	KubeConfigFile string
	InMinikube     bool
}
