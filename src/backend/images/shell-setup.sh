#!/bin/bash
set -e

token=$1

mkdir -p /nonexistent
mount -t tmpfs tmpfs /nonexistent
cd /nonexistent

mkdir -p .kube/certs

cat <<EOF > .kube/config
apiVersion: v1
kind: Config
clusters:
- cluster:
    api-version: v1
    server: https://10.96.0.1
    insecure-skip-tls-verify: true
  name: "Default"
contexts:
- context:
    cluster: "Default"
    user: "Default"
  name: "Default"
current-context: "Default"
users:
- name: "Default"
  user:
    token: "${token}"
EOF

cat >> .bashrc <<EOF
PS1="cluster-console> "
. /usr/share/bash-completion/bash_completion
alias k="kubectl"
alias ks="kubectl -n kube-system"

source <(kubectl completion bash)
source <(helm completion bash)
EOF

chmod 777 .kube .bashrc
chmod 666 .kube/config

usermod -d /nonexistent nobody > /dev/null 2>&1
exec su -s /bin/bash nobody
