#!/usr/bin/env bash

namespaces=$(kubectl get ns -l gc!=disabled --no-headers | awk '{print $1}')
now=$(date +%s)
max_age=10800
for ns in $namespaces
do
  ns_create_time=$(date +%s -d $(kubectl get ns $ns -o jsonpath={.metadata.creationTimestamp}))
  exist_time=$(($now - $ns_create_time))
  if [ $exist_time -gt $max_age ];then
  echo $ns exists too long
  kubectl delete ns $ns || true
  fi
done
