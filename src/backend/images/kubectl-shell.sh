#!/bin/bash
set -e

token=$1

if [ -z "${token}" ]; then
    echo No token provided
    exit 1
fi

echo "# Run kubectl/helm commands inside here"
echo "# e.g. kubectl get all"
echo "# e.g. helm list"
export TERM=screen-256color

unshare --fork --pid --mount-proc --mount shell-setup.sh ${token}
