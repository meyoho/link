import { HttpErrorResponse } from '@angular/common/http';

import {
  Container,
  Deployment as K8SDeployment,
  IngressRule,
  IngressSpec,
  IngressStatus,
  KubernetesResource,
  LimitRangeItem,
  PodSpec,
  PodStatusEnum,
} from './raw-k8s';

export interface ListMeta {
  totalItems: number;
}

export interface ObjectMeta {
  name: string;
  namespace?: string;
  labels?: StringMap;
  annotations?: StringMap;
  creationTimestamp?: string;
  uid?: string;
}

export interface Resource {
  objectMeta: ObjectMeta;
  typeMeta: TypeMeta;
}

export interface Event extends Resource {
  message: string;
  sourceComponent: string;
  sourceHost: string;
  source: any;
  object: string;
  count: number;
  firstSeen: string;
  lastSeen: string;
  reason: string;
  type: string;
}

export interface ContainerState {
  waiting: ContainerStateWaiting;
  terminated: ContainerStateTerminated;
}

export interface ContainerStateWaiting {
  reason: string;
}

export interface ContainerStateTerminated {
  reason: string;
  signal: number;
  exitCode: number;
}

export interface PodStatus {
  podPhase: string;
  status: PodStatusEnum;
  containerStates: ContainerState[];
}

export interface ObjectReference {
  kind: string;
  namespace: string;
  name: string;
  uid: string;
  apiVersion: string;
  resourceVersion: string;
  fieldPath: string;
}

export interface DetailEvent extends Event {
  involvedObject: ObjectReference;
}

export interface ResourceList {
  listMeta: ListMeta;
  errors?: any;
}

export interface ResourceDetail {
  objectMeta: ObjectMeta;
  typeMeta: TypeMeta;
  errors: K8sError[];
  data: KubernetesResource; // YAML representation of the resource
}

export interface TypeMeta {
  kind: string;
  apiVersion: string;
}

export interface ErrStatus {
  message: string;
  code: number;
  status: string;
  reason: string;
}

export type OtherResourcesScope = 'clustered' | 'namespaced';

export interface OtherResource extends Resource {
  scope: string;
}

export interface NodeMetrics {
  cpu?: number;
  memory?: number;
}

export interface Node extends Resource {
  ready: string;
  unschedulable: boolean;
  addresses: NodeAddress[];
  metrics: NodeMetrics;
}
export interface NodeList extends ResourceList {
  nodes: Node[];
}

export interface NodeAllocatedResources {
  cpuRequests: number;
  cpuRequestsFraction: number;
  cpuLimits: number;
  cpuLimitsFraction: number;
  cpuCapacity: number;
  memoryRequests: number;
  memoryRequestsFraction: number;
  memoryLimits: number;
  memoryLimitsFraction: number;
  memoryCapacity: number;
  allocatedPods: number;
  podCapacity: number;
  podFraction: number;
}

export interface NodeInfo {
  machineID: string;
  systemUUID: string;
  bootID: string;
  kernelVersion: string;
  osImage: string;
  containerRuntimeVersion: string;
  kubeletVersion: string;
  kubeProxyVersion: string;
  operatingSystem: string;
  architecture: string;
}

export interface NodeAddress {
  type: string;
  address: string;
}

export interface NodeTaint {
  key: string;
  effect: string;
  value?: string;
  timeAdded?: number;
}

export interface NodeDetail extends ResourceDetail {
  ready: string;
  phase: string;
  podCIDR: string;
  providerID: string;
  unschedulable: boolean;
  allocatedResources: NodeAllocatedResources;
  nodeInfo: NodeInfo;
  containerImages: string[];
  initContainerImages: string[];
  addresses: NodeAddress[];
  taints: NodeTaint[];
  conditions: Condition[];
  metrics: NodeMetrics;
  eventList: EventList;
}

export interface AddNodeConfig {
  node_list: string[];
  ssh_port: number;
  ssh_username: string;
  ssh_password: string;

  // Following are reserved fields
  ssh_key?: string;
  ssh_secret_name?: string;
  api_server?: string;
  bootstrap_token?: string;
  add_node_timeout?: number;
}

export interface OtherResourceDetail extends ResourceDetail {
  scope: OtherResourcesScope;
  events: DetailEvent[];
}

export interface OtherResourceList extends ResourceList {
  resources: OtherResource[];
  errors: any;
}

// The message to be returned when other resource is created.
export interface OtherResourceCreationResult {
  create_messages: Array<{
    message: string;
    resource: string;
    success: boolean;
  }>;
  failed_resource_count: number;
  success_resource_count: number;
  total_resource_count: number;
}

export interface Pod extends Resource {
  podStatus: PodStatus;
  warnings: Event[];
  podIP: string;
  restartCount: number;
  qosClass: string;
  nodeName: string;
  metrics?: PodMetrics;
}

// tslint:disable-next-line:no-empty-interface
export interface ConfigMap extends Resource {
  keys: string[];
}

export interface ConfigMapList extends ResourceList {
  items: ConfigMap[];
}

export interface ConfigMapDetail extends ResourceDetail {
  configData: StringMap;
}

export interface Secret extends Resource {
  type: string;
  keys: string[];
}

export interface SecretList extends ResourceList {
  secrets: Secret[];
}

export interface ServiceAccountList extends ResourceList {
  serviceAccounts: Resource[];
}

export interface SecretDetail extends ResourceDetail {
  type: string;
  secretData: StringMap;
}

export interface MetricResult {
  timestamp: string;
  value: number;
}

export interface PodMetrics {
  cpu?: number;
  memory?: number;
}

export interface Condition {
  type: string;
  status: string;
  lastProbeTime: string;
  lastTransitionTime: string;
  reason: string;
  message: string;
}

export interface EventList extends ResourceList {
  events: Event[];
}

export interface PersistentVolume extends Resource {
  status: string;
  claim: string;
  storageClass: string;
  capacity: any;
  warnings: Event[];
}
export interface PersistentVolumeList extends ResourceList {
  items: PersistentVolume[];
}

export interface PersistentVolumeDetail extends ResourceDetail {
  status: string;
  claim: string;
  capacity: any;
  accessModes: string;
  reclaimPolicy: string;
  storageClass: string;
}

export interface PersistentVolumeClaim extends Resource {
  status: string;
  volume: string;
  storageClass: string;
  capacity: any;
  warnings: Event[];
}
export interface PersistentVolumeClaimList extends ResourceList {
  items: PersistentVolumeClaim[];
}

export interface PersistentVolumeClaimDetail extends ResourceDetail {
  status: string;
  volume: string;
  capacity: any;
  accessModes: string;
  storageClass: string;
  eventList: EventList;
}

export interface StorageClass extends Resource {
  provisioner: string;
  reclaimPolicy: string;
  parameters: StringMap;
  isDefault: boolean;
}

export interface StorageClassList extends ResourceList {
  items: StorageClass[];
}

export interface StorageClassDetail extends ResourceDetail {
  provisioner: string;
  reclaimPolicy: string;
  parameters: StringMap;
  isDefault: boolean;
  persistentVolumeList: PersistentVolumeList;
  persistentVolumeClaimList: PersistentVolumeClaimList;
}

export interface PodDetail extends ResourceDetail {
  initContainers: Container[];
  containers: Container[];
  podPhase: string;
  podStatus: PodStatus;
  warnings: Event[];
  podIP: string;
  nodeName: string;
  restartCount: number;
  qosClass: string;
  metrics: PodMetrics;
  conditions: Condition[];
  controller: Resource;
  eventList: EventList;
  persistentVolumeClaimList: PersistentVolumeClaimList;
  serviceList: ServiceList;
  data: KubernetesResource & { spec: PodSpec };
}

export interface Status {
  running: number;
  failed: number;
  pending: number;
  succeeded: number;
}

export interface PodInfoItem {
  name: string;
  status: string;
  warnings: Event[];
}

export interface PodControllerInfo {
  pods: PodInfoItem[];
  warnings: Event[];
  current: number;
  desired: number;
}

export interface PodList extends ResourceList {
  pods: Pod[];
  status: Status;
}

export interface ReplicaSet extends Resource {
  containerImages: string[];
  initContainerImages: string[];
  podControllerInfo: PodControllerInfo;
  metrics?: PodMetrics;
}

export interface LabelSelector {
  matchLabels: StringMap;
}

export interface Port {
  port: number;
  protocol: string;
  nodePort: number;
}

export interface Endpoint {
  host: string;
  ports: Port[];
}

export interface EndpointResourceList extends ResourceList {
  endpoints: EndpointResource[];
}

export interface EndpointResource extends Resource {
  host: string;
  nodeName: string;
  ready: boolean;
  ports: EndpointResourcePort[];
}

export interface EndpointResourcePort {
  name: string;
  port: number;
  protocol: string;
}

export interface Service extends Resource {
  internalEndpoint: Endpoint;
  externalEndpoints: Endpoint[];
  selector: StringMap;
  type: string;
  clusterIP: string;
}

export interface ServiceList extends ResourceList {
  services: Service[];
}

export interface ServiceDetail extends ResourceDetail {
  internalEndpoint: Endpoint;
  externalEndpoints: Endpoint[];
  endpointList: EndpointResourceList;
  selector: StringMap;
  type: string;
  clusterIP: string;
  sessionAffinity: string;
}

export interface Ingress extends Resource {
  rules: IngressRule[];
}

export interface IngressList extends ResourceList {
  items: Ingress[];
}

export interface IngressDetail extends ResourceDetail {
  spec: IngressSpec;
  status: IngressStatus;
}

export interface NetworkPolicy extends Resource {
  rules: string;
}

export interface NetworkPolicyList extends ResourceList {
  items: NetworkPolicy[];
}

export interface NetworkPolicyDetail extends ResourceDetail {
  rules: string;
}

export interface ReplicaSetDetail extends ResourceDetail {
  selector: LabelSelector;
  podControllerInfo: PodControllerInfo;
  podList: PodList;
  containerImages: string[];
  initContainerImages: string[];
  eventList: EventList;
  serviceList: ServiceList;
}

export interface ReplicaSetList extends ResourceList {
  replicaSets: ReplicaSet[];
  status: Status;
}

export interface Deployment extends Resource {
  podControllerInfo: PodControllerInfo;
  containerImages: string[];
  initContainerImages: string[];
  metrics?: PodMetrics;
}

export interface RollingUpdateDeployment {
  maxSurge: number | string;
  maxUnavailable: number | string;
}

export interface RollingUpdateStatefulSet {
  partition: number | string;
}

export interface RollingUpdateDaemonSet {
  maxUnavailable: number | string;
}

export interface DeploymentInfo {
  replicas: number;
  updated: number;
  available: number;
  unavailable: number;
}

export interface Label {
  key: string;
  value: string;
}

export interface DeploymentDetail extends ResourceDetail {
  selector: Label[];
  statusInfo: DeploymentInfo;
  podControllerInfo: PodControllerInfo;
  conditions: Condition[];
  strategy: string;
  minReadySeconds: number;
  revisionHistoryLimit?: number;
  rollingUpdateStrategy?: RollingUpdateDeployment;
  oldReplicaSetList: ReplicaSetList;
  newReplicaSet: ReplicaSet;
  eventList: EventList;
  podList: PodList;
  serviceList: ServiceList;
  containerImages: string[];
  metrics: PodMetrics;
  persistentVolumeClaimList: PersistentVolumeClaimList;
  data: K8SDeployment;
}

export interface DeploymentList extends ResourceList {
  deployments: Deployment[];
  status: Status;
}

export interface ServiceList extends ResourceList {
  services: Service[];
}

export interface RolesList extends ResourceList {
  items: Resource[];
  errors: any[];
}

export interface RoleDetail extends ResourceDetail {
  events: DetailEvent[];
  data: KubernetesResource;
}

export interface RoleBindingsList extends ResourceList {
  items: RoleBindingListItem[];
}

export interface RoleRef {
  kind: string;
  name: string;
  namespace?: string;
  apiGroup?: string;
}

export interface RoleBindingListItem extends Resource {
  roleRef: RoleRef;
}

export interface RoleBindingDetail extends ResourceDetail {
  events: DetailEvent[];

  data: KubernetesResource & { subjects: any; roleRef: any };
}

export interface UsersListItem {
  name: string;
  email: string;
  group?: string;
  is_admin?: boolean;
}

export interface UsersList {
  list: UsersListItem[];
}

export function isApiResult(res: any): res is OtherResourceCreationResult {
  return res.create_messages;
}

export interface PageParams {
  pageIndex: number; // 0 based
  pageSize: number;
}

export interface StringMap {
  [key: string]: string;
}

/* tslint:disable */
export interface K8sError {
  ErrStatus: ErrStatus;
}
/* tslint:enable */

export function isHttpErrorResponse(res: any): res is HttpErrorResponse {
  return res instanceof HttpErrorResponse;
}

export function isNotHttpErrorResponse<T>(
  res: T | HttpErrorResponse,
): res is T {
  return !isHttpErrorResponse(res);
}

export interface LogSources {
  podNames: string[];
  containerNames: string[];
  initContainerNames: string[];
}

export interface LogDetails {
  info: LogInfo;
  logs: LogLine[];
  selection: LogSelection;
}

export interface LogInfo {
  podName: string;
  containerName: string;
  initContainerName: string;
  fromDate: string;
  toDate: string;
  truncated: boolean;
}

export interface LogLine {
  timestamp: string;
  content: string;
}

export interface LogSelection {
  logFilePosition: string;
  referencePoint: LogLineReference;
  offsetFrom: number;
  offsetTo: number;
}

export interface LogLineReference {
  timestamp: string;
  lineNum: number;
}

export interface Namespace extends Resource {
  phase: string;
}

export interface NamespaceList extends ResourceList {
  namespaces: Namespace[];
}

export interface ResourceQuotaStatus {
  used: string;
  hard: string;
}

export interface ResourceQuota extends Resource {
  scopes: string[];
  statusList: { [resourceName: string]: ResourceQuotaStatus };
}

export interface LimitRange extends Resource {
  limits: LimitRangeItem[];
}

/**
 * The converted view model for LimitRangeItem
 */
export interface LimitRangeListItem {
  resourceType?: string;
  resourceName?: string;
  max?: string;
  min?: string;
  default?: string;
  defaultRequest?: string;
  maxLimitRequestRatio?: string;
}

export interface NamespaceEffectiveQuota extends ResourceQuotaStatus {
  scope: string;
  resourceName: string;
}

export interface NamespaceResourceStatus {
  total: number;
  running?: number;
  warning?: number;
  failed?: number;
  completed?: number;
  pending?: number;

  // PVC specific
  bound?: number;
  lost?: number;
}

export interface NamespaceDetail extends ResourceDetail {
  phase: string;
  eventList: EventList;

  // Resource status:
  deploymentStatus: NamespaceResourceStatus;
  daemonSetStatus: NamespaceResourceStatus;
  statefulSetStatus: NamespaceResourceStatus;
  podStatus: NamespaceResourceStatus;
  pvcStatus: NamespaceResourceStatus;

  // Counts:
  jobCount: number;
  cronJobCount: number;
  replicaSetCount: number;
  configMapCount: number;
  secretCount: number;
  ingressCount: number;
  serviceCount: number;

  // Quota and limits
  effectiveQuotas?: NamespaceEffectiveQuota[];
  effectiveLimitRanges?: LimitRangeItem[];
  resourceQuotas?: ResourceQuota[];
  resourceLimits?: LimitRange[];
}

export interface IdpListApiResource {
  list: Array<string>;
}

export interface OidcApiResource {
  type: 'ldap' | 'oidc';
  id: string;
  name: string;
  config: {
    issuer: string;
    clientID: string;
    clientSecret: string;
  };
}

export interface LdapApiResource {
  type: 'ldap' | 'oidc';
  id: string;
  name: string;
  config: {
    host: string;
    insecureNoSSL: boolean;
    rootCA: string;
    bindDN: string;
    bindPW: string;
    usernamePrompt: string;
    userSearch: {
      baseDN?: string;
      filter?: string;
      username?: string;
      idAttr?: string;
      emailAttr?: string;
      nameAttr?: string;
    };
    groupSearch: {
      baseDN?: string;
      filter?: string;
      userAttr?: string;
      groupAttr?: string;
      nameAttr?: string;
    };
  };
}

export interface PlatformCani {
  allowed: boolean;
}

export interface PlatformCanV {
  allowed: boolean;
}

export interface ClientHost {
  dex_host: string;
  dashboard_host: string;
  devops_host: string;
}

export interface StatefulSet extends Resource {
  podControllerInfo: PodControllerInfo;
  containerImages: string[];
  initContainerImages: string[];
  metrics?: PodMetrics;
}

export interface StatefulSetList extends ResourceList {
  statefulSets: StatefulSet[];
  status: Status;
}

export interface StatefulSetInfo {
  replicas: number;
  updated: number;
  available: number;
  unavailable: number;
}

export interface StatefulSetDetail extends ResourceDetail {
  selector: Label[];
  statusInfo: StatefulSetInfo;
  strategy: string;
  rollingUpdateStrategy?: RollingUpdateStatefulSet;
  revisionHistoryLimit?: number;
  eventList: EventList;
  podList: PodList;
  serviceList: ServiceList;
  containerImages: string[];
  podControllerInfo: PodControllerInfo;
  metrics: PodMetrics;
  persistentVolumeClaimList: PersistentVolumeClaimList;
}

export interface DaemonSet extends Resource {
  podControllerInfo: PodControllerInfo;
  containerImages: string[];
  initContainerImages: string[];
  metrics?: PodMetrics;
}

export interface DaemonSetList extends ResourceList {
  daemonSets: DaemonSet[];
  status: Status;
}

export interface DaemonSetInfo {
  replicas: number;
  updated: number;
  available: number;
  unavailable: number;
}

export interface DaemonSetDetail extends ResourceDetail {
  selector: Label[];
  statusInfo: StatefulSetInfo;
  strategy: string;
  rollingUpdateStrategy?: RollingUpdateDaemonSet;
  minReadySeconds: number;
  revisionHistoryLimit?: number;
  events: EventList;
  podList: PodList;
  serviceList: ServiceList;
  containerImages: string[];
  podControllerInfo: PodControllerInfo;
  metrics: PodMetrics;
  eventList: EventList;
  persistentVolumeClaimList: PersistentVolumeClaimList;
}

export interface OverviewStatus {
  total: number;
  running?: number;
  warning?: number;
  failed?: number;
}

export interface OverviewPodStatus {
  total: number;
  running?: number;
  pending?: number;
  completed?: number;
  failed?: number;
}

export interface OverviewResourceStatus {
  totalCpu: number;
  totalMem: number;
  requestCpu: number;
  requestMem: number;
  metrics?: {
    cpu: number;
    memory: number;
  };
}

export interface OverviewEvent {
  count: number;
  message: string;
  type: 'Warning' | 'Normal';
  lastTimestamp: string;
  involvedObject: ObjectReference;
}

export interface OverviewDetail {
  nodeStatus: OverviewStatus;
  deploymentStatus: OverviewStatus;
  daemonSetStatus: OverviewStatus;
  statefulSetStatus: OverviewStatus;
  namespaceCount: number;

  podStatus: OverviewPodStatus;
  resourceStatus: OverviewResourceStatus;
  eventList: OverviewEvent[];

  errors?: K8sError[];
}

export interface Job extends Resource {
  podControllerInfo: PodControllerInfo;
  containerImages: string[];
  initContainerImages: string[];
}

export interface JobList extends ResourceList {
  jobs: Job[];
  status: Status;
}

export interface JobTemplate {
  parallelism: number;
  completions: number;
  activeDeadlineSeconds: number;
  backoffLimit: number;
  containerImages: string[];
}

export interface JobDetail extends ResourceDetail, JobTemplate {
  metrics: PodMetrics;
  podControllerInfo: PodControllerInfo;
  selector: StringMap;
}

export interface CronJob extends Resource {
  schedule: string;
  suspend: boolean;
  active: number;
  lastSchedule: string;
}

export interface CronJobList extends ResourceList {
  items: CronJob[];
  status: Status;
}

export interface CronJobDetail extends ResourceDetail, CronJob {
  concurrencyPolicy: string;
  startingDeadlineSeconds: number;
  failedJobsHistoryLimit: number;
  successfulJobsHistoryLimit: number;
  jobTemplate: JobTemplate;
}
