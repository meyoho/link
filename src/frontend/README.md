# 此文件夹包含了Link的前端开发文档

## Alauda UI
此项目使用 [Alauda UI](https://bitbucket.org/mathildetech/alauda-ui) 作为主要的第三方 UI 库。

## 国际化翻译

Starter 提供了一个按模块加载翻译文件的方案，具体方式是：

* `TranslateModule`提供公共的翻译文件，放在`translate/i18n.ts`里，在 App 初始化时载入到 App 中。
* `AppModule`使用`TranslateModule.forRoot`提供`TranslateService`给全局使用
* `ShareModule`导出跟翻译有关的 pipe 和 directive，供 feature 模块使用
* feature 模块（可参考`NodeModule`）的构造函数使用`translate.setTranslations(modulePrefix, i18n)`载入到全局。modulePrefix 会作为前缀加到业务模块的翻译 key 上。

也就是说，全局的翻译文件内容是通过一个全局服务提供给不同业务模块的，但每个业务模块的翻译文件将跟着自己模块的初始化进行加载。

### 注

* 尽量不要在代码里再使用`translate.get`方法，而是转为`translate.stream`，这样切换语言时可以不用刷新页面，也就提供更优雅的语言切换方式。
* i18n 文件是普通的 TypeScript 文件，并且需要同时提供多种语言的翻译，这样考虑是：
* 多语言放到一个文件后，由于大部分的翻译文件是随着模块异步加载的，所以并不会显著增加初始翻译文件的大小
* 现在 Angular CLI 不支持自定义 Webpack loader，导致按模块的翻译文件不能通过比较优雅的方式异步载入
    * 假设支持自定义 loader 后，我们可以通过 loader 来拆分一个文件内的多语言内容
* 放到同一个文件时，比较容易交给专业人员进行翻译
* 本地开发友好
* 使用 TypeScript 后，能比较容易做一些代码级别的翻译字符串生成
* modulePrefix 会加到每个翻译的 key 作为前缀，比较松散的方式提供翻译的命名空间实现
    * 也就是说，在使用业务模块翻译时，应手动添加模块的 prefix
* 不同的业务模块之间不能相互使用翻译文件。假如需要共享翻译字符串，则需要放到公共的翻译文件中


## 单独更新 alauda-ui 依赖

以下命令会更新alauda-ui至最新代码，并且更新`yarn.lock`文件。
```
yarn upgrade alauda-ui 
```
