/// <reference path="../../node_modules/monaco-editor/monaco.d.ts" />
/// <reference path="../../node_modules/monaco-yaml/monaco.d.ts" />

/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
  hot: boolean;
}

declare module '*.svg' {
  const content: string;
  export = content;
}

declare let SockJS: any;

declare module 'css-element-queries';
declare class ResizeSensor {
  constructor(element: Element | Element[], callback: (...args: any[]) => any);
  detach(callback: (...args: any[]) => any): void;
}
