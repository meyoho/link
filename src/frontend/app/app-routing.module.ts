import { Injectable, NgModule } from '@angular/core';
import {
  PreloadingStrategy,
  Route,
  RouterModule,
  Routes,
} from '@angular/router';
import { Observable } from 'rxjs';
import { delay, filter, first, switchMap } from 'rxjs/operators';

import { AuthGuardService, UiStateService } from './services';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/console/console.module#ConsoleModule',
    canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    data: {
      auth: true,
    },
  },
  {
    path: 'terminal',
    loadChildren: 'app/terminal/module#TerminalModule',
    canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    data: {
      auth: true,
    },
  },
];

@Injectable()
export class CustomPreloadModuleStrategy implements PreloadingStrategy {
  private loadCounter = 2; // starting from 2 to unblock other xhr.

  preload(_route: Route, fn: () => Observable<any>): Observable<any> {
    return this.uiState.activeItemInfo$.pipe(
      filter(activeItem => activeItem && activeItem.length > 0),
      first(),
      delay(this.loadCounter++ * 1000),
      switchMap(() => fn()),
    );
  }

  constructor(private uiState: UiStateService) {}
}

@NgModule({
  // Since app usually deployed with path redirecting,
  // we may disable HTML5 mode for ease of deployment for the moment
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      // preloadingStrategy: CustomPreloadModuleStrategy,
    }),
  ],
  exports: [RouterModule],
  providers: [CustomPreloadModuleStrategy],
})
export class AppRoutingModule {}
