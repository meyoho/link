import { Injectable } from '@angular/core';
import { TranslateService as NgxTranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Language, LanguageBlob } from './translate.types';

/**
 * Translate service.
 */
@Injectable({
  providedIn: 'root',
})
export class TranslateService {
  currentLang$ = new BehaviorSubject<string>(this.translateService.currentLang);
  constructor(private translateService: NgxTranslateService) {
    this.init();
  }

  init() {
    // 对 window 对象的直接访问在 Angular 2 里面是不被推荐的
    // 如果有空的话，最好还是把 window reference 放到 DI 里比较好
    // 不同标准定义locale的字符串形式不同,
    // 为了统一成目前项目中使用 zh 和 en 两种locale, 需要做一次简单的映射。
    const searchQuery =
      window.location.href.includes('?') && window.location.href.split('?')[1];
    const localeByUrl: string =
      searchQuery && this.getLocaleFromUrlString(searchQuery);
    let locale =
      localeByUrl ||
      localStorage.getItem('alauda_locale') ||
      this.translateService.getBrowserLang() ||
      'en';
    locale = locale.toLowerCase();

    // 注意这里没有用startsWith('zh'), 是考虑繁体中文用户可能会更倾向于用英语
    // 默认语言为en, 当用户浏览器语言为zh-cn或者zh时, 改为使用zh_cn。
    locale = ['zh_cn', 'zh-cn', 'zh'].includes(locale) ? 'zh' : 'en';
    localStorage.setItem('alauda_locale', locale);
    this.translateService.addLangs(Object.values(Language));
    // Preload all languages
    Object.values(Language).forEach(lang =>
      this.translateService.getTranslation(lang),
    );
    this.translateService.use(locale);
    this.currentLang$.next(this.translateService.currentLang);
  }

  /**
   * All supported locales for this app.
   * Should be able to generate from manifest instead.
   */
  get supportedLocales() {
    return ['en', 'zh'];
  }

  /**
   * Get the translated string instantly.
   * @param key
   * @param params
   * @returns {string|any}
   */
  get(key: string, params?: any): string {
    return this.translateService.instant(key, params);
  }

  /**
   * Get the translated string as an observable.
   * @param key
   * @param params
   * @returns {string|any}
   */
  stream(key: string, params?: any): Observable<string> {
    return this.translateService.stream(key, params);
  }

  get currentLang(): string {
    return this.translateService.currentLang;
  }

  get otherLang(): string {
    return this.currentLang === 'en' ? 'zh' : 'en';
  }

  get onLangChange() {
    return this.translateService.onLangChange;
  }

  changeLanguage(language: string) {
    // Change language only if it is different
    // from the current language
    if (this.currentLang !== language) {
      // Currently we does not support change language asynchronously
      // Below is a hack for changing language ...
      localStorage.setItem('alauda_locale', language);
      this.translateService.use(language);
      this.currentLang$.next(this.translateService.currentLang);
    }
  }

  /**
   *
   */
  setTranslations(prefix: string, glob: LanguageBlob) {
    Object.entries(glob).forEach(([lang, translations]) => {
      // For ease of identifying the strings, we better add a prefix for feature translation blob.
      const prefixedTranslations = Object.entries(translations).reduce(
        (accum, [key, value]) => {
          return { ...accum, [`${prefix}.${key}`]: value };
        },
        {},
      );
      this.translateService.setTranslation(lang, prefixedTranslations, true);
    });
  }

  private getLocaleFromUrlString(partial: string) {
    const localeUrlParamKey = 'locale=';
    const localeParam =
      partial.split('&').find(param => param.startsWith(localeUrlParamKey)) ||
      '';
    return localeParam.substr(localeUrlParamKey.length);
  }
}
