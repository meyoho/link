import { NgModule } from '@angular/core';
import {
  TranslateLoader,
  TranslateModule as NgxTranslateModule,
} from '@ngx-translate/core';

import { Observable, of } from 'rxjs';

// Universal translation file
import universalI18n from './i18n';
import { Language } from './translate.types';

export class CoreTranslateLoader implements TranslateLoader {
  getTranslation(lang: Language): Observable<any> {
    return of((universalI18n as any)[lang]);
  }
}

/**
 * Wraps translate module for ease of use.
 */
@NgModule({
  exports: [NgxTranslateModule],
})
export class TranslateModule {}

@NgModule({
  imports: [
    NgxTranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CoreTranslateLoader,
      },
    }),
  ],
  exports: [NgxTranslateModule],
})
export class GlobalTranslateModule {}
