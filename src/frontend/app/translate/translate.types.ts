/**
 * Languages in the app.
 */
export enum Language {
  Chinese = 'zh',
  English = 'en',
}

export interface Translations {
  [key: string]: string;
}

/**
 * Language pack type.
 */
export type LanguageBlob = { [l in Language]?: Translations };
