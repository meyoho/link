import { CodeEditorModule, MonacoProviderService } from '@alauda/code-editor';
import { THEME_SERVICE_CONFIG } from '@alauda/theme';
import {
  IconRegistryService,
  MessageModule,
  NotificationModule,
  PaginatorIntl,
  TooltipCopyIntl,
} from '@alauda/ui';
import * as basicIconsUrl from '@alauda/ui/assets/basic-icons.svg';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { themeServiceConfig } from './constant';
import {
  AppConfigService,
  AuthInterceptor,
  CustomMonacoProviderService,
  CustomPaginatorIntlService,
  CustomTooltipIntlService,
  GenericHttpInterceptor,
  HistoryService,
} from './services';
import { GlobalTranslateModule, TranslateService } from './translate';

const DEFAULT_MONACO_OPTIONS: monaco.editor.IEditorConstructionOptions = {
  fontSize: 12,
  folding: true,
  scrollBeyondLastLine: false,
  minimap: { enabled: false },
  mouseWheelZoom: true,
  scrollbar: {
    vertical: 'visible',
    horizontal: 'visible',
  },
  fixedOverflowWidgets: true,
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    GlobalTranslateModule,

    // AUI NotificationModule, MessageModule need to be imported here since it provides a service
    NotificationModule,
    MessageModule,

    // MonacoProvider must be provided here to overwrite the one defined in CodeEditorModule.
    CodeEditorModule.forRoot({
      // Angular CLI currently does not handle assets with hashes. We manage it by manually adding
      // version numbers to force library updates:
      baseUrl: 'lib/v1.2',
      defaultOptions: DEFAULT_MONACO_OPTIONS,
    }),
    // App routing module should stay at the bottom
    AppRoutingModule,
  ],
  providers: [
    {
      provide: MonacoProviderService,
      useClass: CustomMonacoProviderService,
    },
    {
      provide: TooltipCopyIntl,
      useClass: CustomTooltipIntlService,
    },
    {
      provide: PaginatorIntl,
      useClass: CustomPaginatorIntlService,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GenericHttpInterceptor,
      multi: true,
    },
    {
      provide: THEME_SERVICE_CONFIG,
      useValue: themeServiceConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(
    monacoProvider: MonacoProviderService,
    // Inject the following to make sure they are loaded ahead of other components.
    _appConfig: AppConfigService,
    _translate: TranslateService,
    _historyService: HistoryService,
    _iconService: IconRegistryService,
  ) {
    _appConfig.init();

    setTimeout(() => {
      // Preload monaco.
      monacoProvider.initMonaco();
    }, 3000);

    _iconService.registrySvgSymbolsByUrl(basicIconsUrl);
  }
}
