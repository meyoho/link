import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { DeploymentDetail } from '~api/backendapi';
import { PodSpec } from '~api/raw-k8s';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-deployment-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeploymentDetailPageComponent
  extends BaseDetailPageComponent<DeploymentDetail>
  implements OnInit {
  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml', 'config_management', 'logs', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<DeploymentDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'deployment' });
  }

  async onPodSpecUpdate(podSpec: PodSpec) {
    const detail = await this.detail$.pipe(first()).toPromise();
    const detailParams = this.resourceData.getResourceDetailParams(detail);

    try {
      await this.resourceData.updateDetailAtPath(
        detailParams,
        'spec.template.spec',
        podSpec,
      );
      this.updated$.next(null);
    } catch (error) {}
  }

  ngOnInit() {
    super.ngOnInit();
    this.shellParams$ = this.detail$.pipe(
      map(detail => {
        const newReplicaSet = detail.newReplicaSet;
        if (newReplicaSet) {
          return {
            resourceName: newReplicaSet.objectMeta.name,
            resourceKind: 'ReplicaSet',
            namespace: detail.objectMeta.namespace,
          };
        }
      }),
    );
  }
}
