import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ResourceMutatePageDeactivateGuard } from '../../services';

import { DeploymentDetailPageComponent } from './detail-page.component';
import { DeploymentListPageComponent } from './list-page.component';
import { DeploymentMutatePageComponent } from './mutate-page/mutate-page.component';

// TODO: move somewhere else, like a universal sample provider?
const sample = `apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: default
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      apps.deployment: nginx
  template:
    metadata:
      labels:
        apps.deployment: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: DeploymentListPageComponent,
      },
      {
        path: 'detail',
        component: DeploymentDetailPageComponent,
      },
      {
        path: 'update',
        component: DeploymentMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
      },
      {
        path: 'create',
        component: DeploymentMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class DeploymentRoutingModule {}
