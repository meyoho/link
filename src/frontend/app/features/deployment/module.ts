import { FormModule } from '@alauda/ui';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { FeaturesSharedWorkloadModule } from '../../features-shared/workload-module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { DeploymentActionMenuComponent } from './action-menu.component';
import { DeploymentDetailBasicInfoComponent } from './detail-basic-info.component';
import { DeploymentDetailPageComponent } from './detail-page.component';
import { DeploymentFormComponent } from './form/component';
import i18n from './i18n';
import { DeploymentListPageComponent } from './list-page.component';
import { DeploymentMutatePageComponent } from './mutate-page/mutate-page.component';
import { DeploymentRoutingModule } from './routing.module';
import { DeploymentStrategyFormComponent } from './strategy-form/component';

const MODULE_PREFIX = 'deploy';

@NgModule({
  imports: [
    SharedModule,
    DeploymentRoutingModule,
    FormModule,
    ReactiveFormsModule,
    FeaturesSharedWorkloadModule,
  ],
  declarations: [
    DeploymentListPageComponent,
    DeploymentDetailPageComponent,
    DeploymentActionMenuComponent,
    DeploymentDetailBasicInfoComponent,
    DeploymentFormComponent,
    DeploymentMutatePageComponent,
    DeploymentStrategyFormComponent,
  ],
  providers: [],
})
export class DeploymentModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
