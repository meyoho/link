export const en = {
  old_replicaset: 'Old Replica Sets',
  create_deployment: 'Create Deployment',
};

export const zh = {
  old_replicaset: '旧副本集',
  create_deployment: '创建部署',
};

export default {
  en,
  zh,
};
