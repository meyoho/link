import { Component, Injector, Input } from '@angular/core';
import { set } from 'lodash';
import { Deployment } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';
import { UpdateReplicasComponent } from '~app/features-shared/common/update-replicas/component';

@Component({
  selector: 'alk-deployment-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: [],
})
export class DeploymentActionMenuComponent extends BaseActionMenuComponent<
  Deployment
> {
  @Input()
  actions = [
    'update',
    'scale',
    'view_logs',
    'update_labels',
    'update_annotations',
    'delete',
  ];
  constructor(injector: Injector) {
    super(injector);
  }

  showUpdateReplicas() {
    const dialogRef = this.dialog.open(UpdateReplicasComponent);
    const podInfo = this.resource.podControllerInfo;

    dialogRef.componentInstance.desired = podInfo.desired;
    dialogRef.componentInstance.current = podInfo.current;
    dialogRef.componentInstance.name = this.resource.objectMeta.name;
    dialogRef.componentInstance.kind = 'deployment';

    dialogRef.afterClosed().subscribe(async replicas => {
      if (!Number.isInteger(replicas)) {
        return;
      }
      const detailParams = this.resourceData.getResourceDetailParams(
        this.resource,
      );
      const { data } = await this.resourceData
        .getRawDetail(detailParams)
        .toPromise();
      set(data, 'spec.replicas', replicas);
      try {
        this.resourceData.updateDetail(detailParams, data).toPromise();
        this.updated.next(null);
        this.auiMessageService.success({
          content: this.translate.get('scale_succeeded'),
        });
      } catch (error) {
        this.auiNotificationService.error({ content: error.error || error });
      }
    });
  }
}
