import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';
import { Deployment, DeploymentTypeMeta } from '~api/raw-k8s';

import { BasePodControllerFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-deployment-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeploymentFormComponent
  extends BasePodControllerFormComponent<Deployment>
  implements OnInit {
  kind = 'Deployment';
  namespaced = true;
  replicasControl: AbstractControl;

  getDefaultFormModel() {
    return DeploymentTypeMeta;
  }

  createSpecForm() {
    const specForm = super.createSpecForm();
    specForm.addControl('strategy', this.fb.control({}));
    specForm.addControl('replicas', this.fb.control(1, [Validators.min(0)]));
    this.replicasControl = specForm.get('replicas');
    return specForm;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
