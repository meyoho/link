import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { NetworkPolicy, NetworkPolicyList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-network-policy-list-page',
  templateUrl: './network-policy-list-page.component.html',
  styleUrls: ['./network-policy-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NetworkPolicyListPageComponent
  extends BaseResourceListPageComponent<NetworkPolicyList, NetworkPolicy>
  implements OnInit {
  map(value: NetworkPolicyList): NetworkPolicy[] {
    return value.items;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<NetworkPolicyList> {
    return this.http.get<NetworkPolicyList>(
      `api/v1/networkpolicies/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  getCellValue(item: NetworkPolicy, colName: string) {
    return super.getCellValue(item, colName);
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
