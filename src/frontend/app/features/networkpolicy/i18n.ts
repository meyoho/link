export const en = {
  'create_network-policy': 'Create Network Policy',
};

export const zh = {
  'create_network-policy': '创建网络策略',
};

export default {
  en,
  zh,
};
