import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { NetworkPolicyDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-network-policy-detail-page',
  templateUrl: './network-policy-detail-page.component.html',
  styleUrls: ['./network-policy-detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NetworkPolicyDetailPageComponent
  extends BaseDetailPageComponent<NetworkPolicyDetail>
  implements OnInit {
  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<NetworkPolicyDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'networkpolicy' });
  }
}
