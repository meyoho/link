import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import i18n from './i18n';
import { NetworkPolicyActionMenuComponent } from './network-policy-action-menu.component';
import { NetworkPolicyDetailBasicInfoComponent } from './network-policy-detail-basic-info.component';
import { NetworkPolicyDetailPageComponent } from './network-policy-detail-page.component';
import { NetworkPolicyListPageComponent } from './network-policy-list-page.component';
import { NetworkPolicyRoutingModule } from './network-policy-routing.module';
const MODULE_PREFIX = 'network-policy';

@NgModule({
  imports: [
    SharedModule,
    FeatureSharedCommonModule,
    NetworkPolicyRoutingModule,
  ],
  declarations: [
    NetworkPolicyListPageComponent,
    NetworkPolicyActionMenuComponent,
    NetworkPolicyDetailPageComponent,
    NetworkPolicyDetailBasicInfoComponent,
  ],
  providers: [],
})
export class NetworkPolicyModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
