import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { DaemonSet } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-daemon-set-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: ['./action-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaemonSetActionMenuComponent extends BaseActionMenuComponent<
  DaemonSet
> {
  @Input()
  actions = [
    'update',
    'view_logs',
    'update_labels',
    'update_annotations',
    'delete',
  ];
  constructor(injector: Injector) {
    super(injector);
  }
}
