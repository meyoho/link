import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { DaemonSet } from '~api/raw-k8s';

import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaemonSetMutatePageComponent extends BaseResourceMutatePageComponent<
  DaemonSet
> {
  kind = 'DaemonSet';
  constructor(injector: Injector) {
    super(injector);
  }
}
