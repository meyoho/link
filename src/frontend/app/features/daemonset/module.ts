import { FormModule } from '@alauda/ui';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { FeaturesSharedWorkloadModule } from '../../features-shared/workload-module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { DaemonSetActionMenuComponent } from './action-menu.component';
import { DaemonSetDetailBasicInfoComponent } from './detail-basic-info.component';
import { DaemonSetDetailPageComponent } from './detail-page.component';
import { DaemonSetFormComponent } from './form/component';
import i18n from './i18n';
import { DaemonSetListPageComponent } from './list-page.component';
import { DaemonSetMutatePageComponent } from './mutate-page/component';
import { DaemonSetRoutingModule } from './routing.module';
import { DaemonSetUpdateStrategyFormComponent } from './update-strategy-form/component';

const MODULE_PREFIX = 'daemonset';

@NgModule({
  imports: [
    SharedModule,
    DaemonSetRoutingModule,
    FeaturesSharedWorkloadModule,
    FormModule,
    ReactiveFormsModule,
  ],
  declarations: [
    DaemonSetListPageComponent,
    DaemonSetDetailPageComponent,
    DaemonSetDetailBasicInfoComponent,
    DaemonSetActionMenuComponent,
    DaemonSetFormComponent,
    DaemonSetMutatePageComponent,
    DaemonSetUpdateStrategyFormComponent,
  ],
})
export class DaemonSetModule {
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
