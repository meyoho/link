import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { DaemonSet, DaemonSetTypeMeta } from '~api/raw-k8s';

import { BasePodControllerFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-daemon-set-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaemonSetFormComponent
  extends BasePodControllerFormComponent<DaemonSet>
  implements OnInit {
  kind = 'DaemonSet';
  namespaces$: Observable<string[]>;

  constructor(injector: Injector) {
    super(injector);
  }

  getDefaultFormModel() {
    return DaemonSetTypeMeta;
  }

  createSpecForm() {
    const specForm = super.createSpecForm();
    specForm.addControl('updateStrategy', this.fb.control({}));
    return specForm;
  }
}
