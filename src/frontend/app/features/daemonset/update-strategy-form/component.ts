import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alk-daemon-set-update-strategy-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DaemonSetUpdateStrategyFormComponent
  extends BaseResourceFormGroupComponent
  implements OnInit, AfterViewInit {
  types = ['RollingUpdate', 'OnDelete'];

  constructor(injector: Injector) {
    super(injector);
  }

  getResourceMergeStrategy() {
    return false;
  }

  getDefaultFormModel() {
    return { type: 'RollingUpdate' };
  }

  adaptFormModel(value: any) {
    if (value && value.rollingUpdate) {
      const rollingUpdate = value.rollingUpdate;
      Object.keys(rollingUpdate).forEach(key => {
        rollingUpdate[key] = this.adaptNumbers(rollingUpdate[key]);
      });
    }
    return value;
  }

  createForm() {
    return this.fb.group({
      type: [],
      rollingUpdate: this.fb.group({
        maxUnavailable: [1],
      }),
    });
  }

  get typeControl() {
    return this.form.get('type');
  }

  ngAfterViewInit() {
    this.typeControl.valueChanges.subscribe(value => {
      const rollingUpdateControl = this.form.get('rollingUpdate');
      if (value !== 'RollingUpdate') {
        rollingUpdateControl.disable({ emitEvent: false });
      } else {
        rollingUpdateControl.enable({ emitEvent: false });
        if (!rollingUpdateControl.get('maxUnavailable').value) {
          rollingUpdateControl.setValue({
            maxUnavailable: 1,
          });
        }
      }
    });
  }

  private adaptNumbers(value: string) {
    if (Number.isInteger(+value)) {
      return +value;
    } else {
      return value;
    }
  }
}
