export const en = {
  create_daemonset: 'Create Daemon Set',
};

export const zh = {
  create_daemonset: '创建守护进程集',
};

export default {
  en,
  zh,
};
