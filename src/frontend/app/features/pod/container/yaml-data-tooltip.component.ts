import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { safeDump } from 'js-yaml';

@Component({
  selector: 'alk-yaml-data-tooltip',
  template: `
    <aui-icon
      *ngIf="shouldShowData()"
      icon="basic:eye_s"
      [auiTooltip]="yamlTemplate"
      auiTooltipType="info"
      auiTooltipPosition="start"
    >
    </aui-icon>
    <span *ngIf="!shouldShowData()">-</span>

    <ng-template #yamlTemplate
      ><pre ngCodeColorize="yaml" [innerHTML]="yaml"></pre
    ></ng-template>
  `,
  styleUrls: ['./yaml-data-tooltip.component.scss'],
})
export class YamlDataTooltipComponent implements OnChanges {
  @Input()
  data: any;
  yaml = '';

  shouldShowData() {
    return Array.isArray(this.data) ? this.data.length > 0 : this.data;
  }

  ngOnChanges({ data }: SimpleChanges) {
    if (data) {
      this.yaml = safeDump(this.data).trim();
    }
  }
}
