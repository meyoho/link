import { Component, Input } from '@angular/core';
import { Container } from '~api/raw-k8s';

@Component({
  selector: 'alk-container-list',
  templateUrl: './container-list.component.html',
})
export class ContainerListComponent {
  @Input()
  containers: Container[] = [];

  columns = ['name', 'image', 'env', 'commands', 'args'];

  get showZeroState() {
    return !this.containers || this.containers.length === 0;
  }

  trackByFn(index: number, item: Container) {
    return index + '.' + item.name;
  }
}
