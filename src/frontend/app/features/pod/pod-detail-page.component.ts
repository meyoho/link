import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PodDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-pod-detail-page',
  templateUrl: './pod-detail-page.component.html',
  styleUrls: ['./pod-detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodDetailPageComponent extends BaseDetailPageComponent<PodDetail> {
  execSelectedContainer = '';

  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml', 'config_management', 'logs', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<PodDetail> {
    return this.resourceData
      .getDetail<PodDetail>({ ...params, kind: 'pod' })
      .pipe(
        tap(podDetail => {
          if (
            podDetail &&
            podDetail.containers &&
            podDetail.containers.length &&
            !this.execSelectedContainer
          ) {
            this.execSelectedContainer = podDetail.containers[0].name;
          }
        }),
      );
  }
}
