import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { PodDetail, Resource } from '~api/backendapi';

import { getOwnerResource } from '../../features-shared/utils/owner-ref';
import { AppConfigService } from '../../services';

@Component({
  selector: 'alk-pod-detail-basic-info',
  templateUrl: './pod-detail-basic-info.component.html',
  styleUrls: [],
})
export class PodDetailBasicInfoComponent implements OnChanges {
  controller: Resource;

  @Input()
  detail: PodDetail;

  @Output()
  updated = new EventEmitter();

  ngOnChanges({ detail }: SimpleChanges) {
    if (detail && detail.currentValue) {
      this.controller = getOwnerResource(this.detail.data);
    }
  }

  getNodeResource() {
    if (this.detail) {
      const node: Resource = {
        typeMeta: {
          apiVersion: 'v1',
          kind: 'Node',
        },
        objectMeta: {
          name: this.detail.nodeName,
        },
      };
      return node;
    } else {
      return undefined;
    }
  }

  constructor(public appConfig: AppConfigService) {}
}
