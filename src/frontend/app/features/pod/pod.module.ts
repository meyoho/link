import { NgModule } from '@angular/core';

import { FeaturesSharedWorkloadModule } from '../../features-shared/workload-module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { ContainerListComponent } from './container/container-list.component';
import { YamlDataTooltipComponent } from './container/yaml-data-tooltip.component';
import i18n from './i18n';
import { PodDetailBasicInfoComponent } from './pod-detail-basic-info.component';
import { PodDetailPageComponent } from './pod-detail-page.component';
import { PodListPageComponent } from './pod-list-page.component';
import { PodRoutingModule } from './pod-routing.module';

const MODULE_PREFIX = 'pod';

@NgModule({
  imports: [SharedModule, PodRoutingModule, FeaturesSharedWorkloadModule],
  declarations: [
    PodListPageComponent,
    PodDetailPageComponent,
    PodDetailBasicInfoComponent,
    ContainerListComponent,
    YamlDataTooltipComponent,
  ],
  providers: [],
})
export class PodModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
