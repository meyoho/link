import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Pod, PodList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'status',
    label: 'status',
    sortable: true,
  },
  {
    name: 'metrics',
    label: 'used_resources',
  },
  {
    name: 'restarts',
    label: 'restarts',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-pod-list-page',
  templateUrl: './pod-list-page.component.html',
  styleUrls: ['./pod-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodListPageComponent
  extends BaseResourceListPageComponent<PodList, Pod>
  implements OnInit {
  map(value: PodList): Pod[] {
    return value.pods;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<PodList> {
    return this.http.get<PodList>(`api/v1/pods/${params.namespace || ''}`, {
      params: getQuery(
        filterBy('name', params.search),
        sortBy(params.sort.active, params.sort.direction === 'desc'),
        pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
      ),
    });
  }

  getCellValue(item: Pod, colName: string) {
    switch (colName) {
      case 'restarts':
        return item.restartCount;
      default:
        return super.getCellValue(item, colName);
    }
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
