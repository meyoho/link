import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { StorageClass, StorageClassList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'provisioner',
    label: 'storageclass.provisioner',
  },
  {
    name: 'type',
    label: 'type',
  },
  {
    name: 'reclaimPolicy',
    label: 'storageclass.reclaimPolicy',
  },
  {
    name: 'isDefault',
    label: 'storageclass.default',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-storage-class-list-page',
  templateUrl: './storage-class-list-page.component.html',
  styleUrls: ['./storage-class-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StorageClassListPageComponent
  extends BaseResourceListPageComponent<StorageClassList, StorageClass>
  implements OnInit {
  map(value: StorageClassList): StorageClass[] {
    return value.items;
  }
  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }
  fetchResources(params: FetchParams): Observable<StorageClassList | Error> {
    return this.http.get<StorageClassList>('api/v1/storageclasses', {
      params: getQuery(
        // Currently filter by name means "search"
        filterBy('name', params.search),
        sortBy(params.sort.active, params.sort.direction === 'desc'),
        pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
      ),
    });
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
