import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { StorageClass } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-storage-class-action-menu',
  templateUrl: './storage-class-action-menu.component.html',
  styleUrls: ['./storage-class-action-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StorageClassActionMenuComponent extends BaseActionMenuComponent<
  StorageClass
> {
  @Input()
  actions = [
    'set_default',
    'unset_default',
    'update',
    'update_labels',
    'update_annotations',
    'delete',
  ];
  constructor(private http: HttpClient, injector: Injector) {
    super(injector);
  }

  async setDefaultClass() {
    try {
      const confirmed = await this.confirmBox.confirm({
        title: this.translate.get('storageclass.set_default'),
        content: this.translate.get('storageclass.set_default_confirm', {
          storageClassName: this.resource.objectMeta.name,
        }),
      });
      if (confirmed) {
        await this.http
          .put(
            `api/v1/storageclasses/${
              this.resource.objectMeta.name
            }/default/set`,
            {},
          )
          .toPromise();
        this.updated.next(this.resource);
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }
  async unsetDefaultClass() {
    try {
      const confirmed = await this.confirmBox.confirm({
        title: this.translate.get('storageclass.unset_default'),
        content: this.translate.get('storageclass.unset_default_confirm', {
          storageClassName: this.resource.objectMeta.name,
        }),
      });
      if (confirmed) {
        await this.http
          .put(
            `api/v1/storageclasses/${
              this.resource.objectMeta.name
            }/default/cancel`,
            {},
          )
          .toPromise();
        this.updated.next(this.resource);
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }
}
