import { Component, Injector, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageClassDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-storage-class-detail-page',
  templateUrl: './storage-class-detail-page.component.html',
  styleUrls: ['./storage-class-detail-page.component.scss'],
})
export class StorageClassDetailPageComponent
  extends BaseDetailPageComponent<StorageClassDetail>
  implements OnInit {
  podNames$: Observable<string[]>;
  tabs = ['basic_info', 'yaml'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<StorageClassDetail> {
    return this.resourceData.getDetail({
      ...params,
      kind: 'StorageClass',
    });
  }

  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
