export const en = {
  create_storageclass: 'Create StorageClass',
  default: 'Default',
  provisioner: 'Provisioner',
  reclaimPolicy: 'ReclaimPolicy',
  true: 'True',
  false: 'False',
  set_default: 'Set Default',
  unset_default: 'Cancel Default',
  set_default_confirm:
    'Confirm to set {{ storageClassName }} to be default storageclass ? \n This will cancel the existent default storageclasses.',
  unset_default_confirm:
    'Confirm to cancel the default storageclass {{ storageClassName }} ?',
};

export const zh = {
  create_storageclass: '创建存储类',
  default: '是否默认',
  provisioner: 'Provisioner',
  reclaimPolicy: '回收策略',
  true: '是',
  false: '否',
  set_default: '设为默认',
  unset_default: '取消默认',
  set_default_confirm:
    '确定要将该 {{ storageClassName }} 设置为默认使用的存储类吗？\n 修改后，原有的默认存储类将会被取消默认设置',
  unset_default_confirm: '确定要取消默认存储类 {{ storageClassName }}  吗？',
};

export default {
  en,
  zh,
};
