import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import i18n from './i18n';
import { StorageClassActionMenuComponent } from './storage-class-action-menu.component';
import { StorageClassDetailBasicInfoComponent } from './storage-class-detail-basic-info.component';
import { StorageClassDetailPageComponent } from './storage-class-detail-page.component';
import { StorageClassListPageComponent } from './storage-class-list-page.component';
import { StorageClassRoutingModule } from './storage-class-routing.module';

const MODULE_PREFIX = 'storageclass';

@NgModule({
  imports: [SharedModule, FeatureSharedCommonModule, StorageClassRoutingModule],
  declarations: [
    StorageClassListPageComponent,
    StorageClassDetailPageComponent,
    StorageClassActionMenuComponent,
    StorageClassDetailBasicInfoComponent,
  ],
})
export class StorageClassModule {
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
