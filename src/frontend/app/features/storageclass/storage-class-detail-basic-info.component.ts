import { Component, EventEmitter, Input, Output } from '@angular/core';
import { StorageClassDetail } from '~api/backendapi';

@Component({
  selector: 'alk-storage-class-detail-basic-info',
  templateUrl: './storage-class-detail-basic-info.component.html',
  styleUrls: ['./storage-class-detail-basic-info.component.scss'],
})
export class StorageClassDetailBasicInfoComponent {
  @Input()
  detail: StorageClassDetail;
  @Output()
  updated = new EventEmitter();
}
