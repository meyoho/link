import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';

import { Secret } from '~api/raw-k8s';
import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  selector: 'alk-secret-mutate-page',
  templateUrl: './mutate-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretMutatePageComponent extends BaseResourceMutatePageComponent<
  Secret
> {
  get kind() {
    return 'Secret';
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
