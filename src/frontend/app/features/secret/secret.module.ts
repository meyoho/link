import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';
import { CustomTypeComponent } from '~app/features/secret/form/add-type-dialog/component';
import { BasicAuthEditorComponent } from '~app/features/secret/form/basic-auth-editor/component';
import { SecretFormComponent } from '~app/features/secret/form/component';
import { DockerconfigEditorComponent } from '~app/features/secret/form/dockerconfig-editor/component';
import { SSHAuthEditorComponent } from '~app/features/secret/form/ssh-auth-editor/component';
import { TLSEditorComponent } from '~app/features/secret/form/tls-editor/component';
import { SecretMutatePageComponent } from '~app/features/secret/mutate-page/mutate-page.component';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { SecretDataViwerComponent } from './data-viewer/component';
import i18n from './i18n';
import { SecretActionMenuComponent } from './secret-action-menu.component';
import { SecretDetailBasicInfoComponent } from './secret-detail-basic-info.component';
import { SecretDetailPageComponent } from './secret-detail-page.component';
import { SecretListPageComponent } from './secret-list-page.component';
import { SecretRoutingModule } from './secret-routing.module';
const MODULE_PREFIX = 'secret';

@NgModule({
  imports: [SharedModule, FeatureSharedCommonModule, SecretRoutingModule],
  declarations: [
    SecretListPageComponent,
    SecretActionMenuComponent,
    SecretDetailPageComponent,
    SecretDetailBasicInfoComponent,
    SecretDataViwerComponent,
    SecretMutatePageComponent,
    SecretFormComponent,
    TLSEditorComponent,
    CustomTypeComponent,
    DockerconfigEditorComponent,
    BasicAuthEditorComponent,
    SSHAuthEditorComponent,
  ],
  providers: [],
  entryComponents: [CustomTypeComponent],
})
export class SecretModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
