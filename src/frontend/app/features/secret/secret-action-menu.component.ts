import { Component, Injector, Input } from '@angular/core';
import { Secret } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-secret-action-menu',
  templateUrl: './secret-action-menu.component.html',
  styleUrls: [],
})
export class SecretActionMenuComponent extends BaseActionMenuComponent<Secret> {
  @Input()
  actions = ['update', 'update_labels', 'update_annotations', 'delete'];
  constructor(injector: Injector) {
    super(injector);
  }
}
