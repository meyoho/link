import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SecretMutatePageComponent } from '~app/features/secret/mutate-page/mutate-page.component';
import { ResourceMutatePageDeactivateGuard } from '~app/services';

import { SecretDetailPageComponent } from './secret-detail-page.component';
import { SecretListPageComponent } from './secret-list-page.component';

// TODO: move somewhere else, like a universal sample provider or laoder?
const sample = `apiVersion: v1
kind: Secret
metadata:
  name: mysecret
  namespace: default
type: Opaque
data: {}
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: SecretListPageComponent,
      },
      {
        path: 'detail',
        component: SecretDetailPageComponent,
      },
      {
        path: 'update',
        component: SecretMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
      },
      {
        path: 'create',
        component: SecretMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class SecretRoutingModule {}
