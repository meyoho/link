import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { Observable, fromEvent } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'alk-secret-data-viewer',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class SecretDataViwerComponent implements OnInit, OnChanges {
  @Input()
  data: { [key: string]: string };
  pageScroll$: Observable<number>;

  secrets: [string, string][] = [];

  visibleMap: { [key: string]: boolean } = {};

  constructor() {}

  ngOnInit(): void {
    const layoutPageEl = document.querySelector('.aui-layout__page');
    this.pageScroll$ = fromEvent(layoutPageEl, 'scroll').pipe(
      startWith({}),
      map(() => layoutPageEl.scrollTop),
    );
  }

  ngOnChanges({ data }: SimpleChanges) {
    if (data) {
      this.secrets = Object.entries(this.data || {});
    }
  }

  toggleCollapse(key: string) {
    this.visibleMap[key] = !this.visibleMap[key];
  }

  decode(s: string): string {
    return atob(s);
  }
}
