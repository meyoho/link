import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { DialogRef, DialogService } from '@alauda/ui';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Secret, SecretTypeMeta, StringMap } from '~api/raw-k8s';
import { BaseKubernetesResourceFormComponent } from '~app/abstract';
import { CustomTypeComponent } from '~app/features/secret/form/add-type-dialog/component';

const originTypeMap: StringMap = {
  Opaque: 'Opaque',
  TLS: 'kubernetes.io/tls',
  dockerconfig: 'kubernetes.io/dockercfg',
  dockerconfigjson: 'kubernetes.io/dockerconfigjson',
  'basic-auth': 'kubernetes.io/basic-auth',
  'ssh-auth': 'kubernetes.io/ssh-auth',
};

@Component({
  selector: 'alk-secret-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretFormComponent
  extends BaseKubernetesResourceFormComponent<Secret>
  implements OnInit, OnDestroy {
  kind = 'Secret';
  type = originTypeMap.Opaque;
  typeSub: Subscription;
  dialogRef: DialogRef<CustomTypeComponent>;
  originTypes: string[] = Object.keys(originTypeMap).map(
    (type: string) => originTypeMap[type],
  );
  customTypes: string[] = [];

  getDefaultFormModel() {
    return SecretTypeMeta;
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(/^[a-z0-9^-]*[a-z0-9-]*[a-z0-9]$/),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    return this.fb.group({
      metadata: metadataForm,
      data: this.fb.control({}),
      type: this.fb.control(
        { value: originTypeMap.Opaque, disabled: this.updateMode },
        [Validators.required],
      ),
    });
  }

  adaptResourceModel(resource: Secret) {
    if (!resource || !resource.data) {
      return super.adaptResourceModel(resource);
    }
    const decodedData = Object.entries(resource.data).reduce(
      (accum, [key, value]) => ({ ...accum, [key]: atob(value) }),
      {},
    );
    return { ...resource, data: decodedData };
  }

  adaptFormModel(form: Secret) {
    form = super.adaptResourceModel(form);
    const decodedData = Object.entries(form.data).reduce(
      (accum, [key, value]) => ({ ...accum, [key]: btoa(value) }),
      {},
    );
    return { ...form, data: decodedData };
  }

  addCustomType() {
    this.dialogRef = this.dialogService.open(CustomTypeComponent);
    this.dialogRef.componentInstance.close.subscribe((type: string) => {
      if (!this.customTypes.includes(type)) {
        this.customTypes.push(type);
        this.form.get('type').setValue(type);
      }
      this.dialogRef.close();
    });
  }

  getTypeDisplayName(type: string) {
    return type.replace('kubernetes.io/', '');
  }

  typeChange() {
    this.form.get('data').setValue({});
  }

  ngOnInit() {
    super.ngOnInit();

    this.typeSub = this.form
      .get('type')
      .valueChanges.pipe(filter(value => !!value))
      .subscribe(value => {
        if (value === this.type) {
          return;
        }
        this.type = value;
      });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.typeSub.unsubscribe();
  }

  constructor(injector: Injector, private dialogService: DialogService) {
    super(injector);
  }
}
