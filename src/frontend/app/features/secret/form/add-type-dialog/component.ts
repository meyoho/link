import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';

@Component({
  selector: 'alk-custom-type',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomTypeComponent {
  type: string;

  @Output()
  close = new EventEmitter();

  constructor() {}

  confirm() {
    this.close.next(this.type);
  }
}
