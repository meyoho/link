import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { SecretDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-secret-detail-page',
  templateUrl: './secret-detail-page.component.html',
  styleUrls: ['./secret-detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretDetailPageComponent
  extends BaseDetailPageComponent<SecretDetail>
  implements OnInit {
  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
    this.debouncedUpdate = () => {};
  }

  tabs = ['basic_info', 'yaml'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<SecretDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'secret' });
  }
}
