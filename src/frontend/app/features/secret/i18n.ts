export const en = {
  create_secret: 'Create Secret',
  ca: 'Certificate',
  private_key: 'Private key',
  add_custom_type: 'Add custom type',
  type_exist: 'Type already exists',
  'ssh-privatekey': 'SSH Private key',
  dockerServiceAddress: 'dockerServiceAddress',
};

export const zh = {
  create_secret: '创建保密字典',
  ca: '证书',
  private_key: '私钥',
  add_custom_type: '添加自定义类型',
  type_exist: '该类型已存在',
  'ssh-privatekey': 'SSH 私钥',
  dockerServiceAddress: '镜像服务地址',
};

export default {
  en,
  zh,
};
