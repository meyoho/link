import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NamespaceDetail } from '~api/backendapi';

import { TranslateService } from '../../translate';

// Following is to make sure the charts could show as empty instead of hidden
// before initialized.
const EMPTY_NAMESPACE_DETAIL: NamespaceDetail = {
  objectMeta: { name: '-' },
  typeMeta: { kind: 'Namespace', apiVersion: 'v1' },
  phase: 'Active',
  eventList: {
    listMeta: {
      totalItems: 0,
    },
    events: [],
  },
  pvcStatus: {
    total: 0,
    pending: 0,
    bound: 0,
    failed: 0,
    lost: 0,
  },
  podStatus: {
    total: 0,
    running: 0,
    pending: 0,
    completed: 0,
    failed: 0,
  },
  deploymentStatus: {
    total: 0,
    running: 0,
    pending: 0,
    failed: 0,
  },
  daemonSetStatus: {
    total: 0,
    running: 0,
    pending: 0,
    failed: 0,
  },
  statefulSetStatus: {
    total: 0,
    running: 0,
    pending: 0,
    failed: 0,
  },
  ingressCount: 0,
  serviceCount: 0,
  configMapCount: 0,
  secretCount: 1,
  replicaSetCount: 0,
  jobCount: 0,
  cronJobCount: 0,
  data: {},
  errors: [],
};

@Component({
  selector: 'alk-namespace-detail-charts',
  templateUrl: './detail-charts.component.html',
  styleUrls: ['./detail-charts.component.scss'],
})
export class NamespaceDetailChartsComponent implements OnInit {
  @Input()
  namespace: NamespaceDetail;
  constructor(private router: Router, private translate: TranslateService) {}

  ngOnInit(): void {
    this.namespace = this.namespace || EMPTY_NAMESPACE_DETAIL;
  }

  onResourceClicked(type: string, status: string = '') {
    this.router.navigate([
      `/${type}/list`,
      { namespace: this.namespace.objectMeta.name, status },
    ]);
  }

  pvcStatusLabelRender = (key: string) => this.translate.get('pv_c_' + key);
}
