import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { Namespace, NamespaceTypeMeta } from '~api/raw-k8s';
import { BaseKubernetesResourceFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-namespace-form',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceFormComponent extends BaseKubernetesResourceFormComponent<
  Namespace
> {
  kind = 'Namespace';

  createForm() {
    const metadata = this.fb.group({
      name: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    return this.fb.group({
      metadata,
    });
  }

  getDefaultFormModel(): Namespace {
    return NamespaceTypeMeta;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
