import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { PieArcDatum, arc, pie } from 'd3-shape';

import { NamespaceDetail, NamespaceResourceStatus } from '~api/backendapi';

const WORKLOADS: ['deployment', 'statefulset', 'daemonset'] = [
  'deployment',
  'statefulset',
  'daemonset',
];

const WORKLOAD_TO_STATUS_KEY = {
  deployment: 'deploymentStatus',
  statefulset: 'statefulSetStatus',
  daemonset: 'daemonSetStatus',
};

const WORKLOAD_TO_COLOR = {
  deployment: '#66c3ee',
  statefulset: '#33AFE8',
  daemonset: '#007cb5',
};

interface WorkloadLegend {
  color: string;
  status: NamespaceResourceStatus;
  type?: string;
  routerLink?: (string | any)[];
}

interface WorkloadArc extends WorkloadLegend {
  path: string;
}

@Component({
  selector: 'alk-namespace-workload-pie',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class NamespaceWorkloadPieComponent implements OnChanges {
  private pieSize = 10;
  private margin = 0;

  @Input()
  namespace: NamespaceDetail;

  @Input()
  width = 120;

  @Input()
  height = 120;

  arcs: WorkloadArc[];
  legends: WorkloadLegend[];

  get transform() {
    return `translate(${this.width / 2}, ${this.height / 2})`;
  }

  onArcClicked(workloadArc: WorkloadArc) {
    if (workloadArc.routerLink) {
      this.router.navigate(workloadArc.routerLink);
    }
  }

  trackByFn(index: number) {
    return index;
  }

  ngOnChanges({ namespace }: SimpleChanges) {
    if (namespace) {
      this.legends = this.updateLegends();
      this.arcs = this.updateArcs();
    }
  }

  constructor(private router: Router) {}

  private get outerRadius() {
    return Math.min(this.width, this.height) / 2 - this.margin;
  }

  private get innerRadius() {
    return this.outerRadius - this.pieSize;
  }

  private getStatus(
    name: 'deployment' | 'statefulset' | 'daemonset',
  ): NamespaceResourceStatus {
    return (this.namespace as any)[
      WORKLOAD_TO_STATUS_KEY[name]
    ] as NamespaceResourceStatus;
  }

  private updateArcs() {
    const arcFactory = arc<PieArcDatum<WorkloadLegend>>()
      .innerRadius(this.innerRadius)
      .outerRadius(this.outerRadius);

    let legends = this.legends.filter(legend => legend.status.total);

    if (legends.length === 0) {
      legends = [{ color: '#eee', status: { total: 1 } }];
    }

    const pies = pie<WorkloadLegend>()
      .value(data => data.status.total)
      .sort(undefined)
      .padAngle(0.04)(legends);

    return pies.map(_pie => ({
      ..._pie.data,
      path: arcFactory(_pie),
    }));
  }

  private updateLegends(): WorkloadLegend[] {
    return WORKLOADS.map(workload => ({
      type: workload,
      color: WORKLOAD_TO_COLOR[workload],
      status: this.getStatus(workload),
      routerLink: [
        `/${workload}/list`,
        { namespace: this.namespace.objectMeta.name },
      ],
    }));
  }
}
