import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ResourceQuota } from '~api/raw-k8s';

import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  selector: 'alk-resource-quota-mutate-page',
  templateUrl: './mutate-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceQuotaMutatePageComponent
  extends BaseResourceMutatePageComponent<ResourceQuota>
  implements OnInit {
  namespace$: Observable<string>;

  get kind() {
    return 'ResourceQuota';
  }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.namespace$ = this.activatedRoute.params.pipe(
      startWith(this.activatedRoute.snapshot.params),
      map(params => params.namespace || 'default'),
    );
  }

  afterMutation(resource: ResourceQuota) {
    this.router.navigate([
      '/namespace/detail',
      { name: resource.metadata.namespace },
    ]);
  }
}
