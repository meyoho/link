import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { merge } from 'lodash';
import { ResourceQuota, ResourceQuotaTypeMeta } from '~api/raw-k8s';
import { BaseKubernetesResourceFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-resource-quota-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceQuotaFormComponent extends BaseKubernetesResourceFormComponent<
  ResourceQuota
> {
  kind = 'ResourceQuota';

  @Input()
  namespace: string;

  createForm() {
    const metadata = this.fb.group({
      name: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    const spec = this.fb.group({
      hard: this.fb.group({
        pods: this.fb.control(''),
        persistentvolumeclaims: this.fb.control(''),

        'requests.cpu': this.fb.control(''),
        'requests.memory': this.fb.control(''),
        'requests.storage': this.fb.control(''),
        'limits.cpu': this.fb.control(''),
        'limits.memory': this.fb.control(''),
      }),
    });

    return this.fb.group({
      metadata,
      spec,
    });
  }

  getDefaultFormModel(): ResourceQuota {
    return {
      ...ResourceQuotaTypeMeta,
      metadata: { namespace: this.namespace },
    };
  }

  adaptFormModel(resource: ResourceQuota) {
    const model = merge({}, resource, this.getDefaultFormModel());

    if (model.spec && model.spec.hard) {
      // Filter out undefined values:
      Object.keys(model.spec.hard).forEach(key => {
        if (model.spec.hard[key] === '') {
          delete model.spec.hard[key];
        }
      });
    }

    if (resource.metadata) {
      delete resource.metadata.resourceVersion;
    }

    return model;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
