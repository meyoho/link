import { Component, Input, OnChanges } from '@angular/core';
import { sortBy } from 'lodash';
import { ResourceQuotaStatus } from '~api/backendapi';

@Component({
  selector: 'alk-resource-quota-status-list',
  templateUrl: './template.html',
})
export class ResourceQuotaStatusListComponent implements OnChanges {
  @Input()
  statusList: { [resourceName: string]: ResourceQuotaStatus };

  list: [string, ResourceQuotaStatus][];

  columns = ['resourceName', 'hard', 'used'];

  ngOnChanges() {
    this.list = Object.entries(this.statusList || {});
    this.list = sortBy(
      this.list,
      [
        (item: [string, ResourceQuotaStatus]) => item[0].split('.')[1],
        (item: [string, ResourceQuotaStatus]) => item[0].split('.')[0],
      ],
      ['asc', 'desc'],
    );
  }

  splitName(name: string) {
    return name.split('.').reverse();
  }
}
