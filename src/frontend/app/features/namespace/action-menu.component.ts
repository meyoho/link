import { Component, Injector, Input } from '@angular/core';
import { Namespace } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';
import { FeatureStateService } from '~app/services';

@Component({
  selector: 'alk-namespace-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: [],
})
export class NamespaceActionMenuComponent extends BaseActionMenuComponent<
  Namespace
> {
  @Input()
  actions = [
    'update_labels',
    'update_annotations',
    'create_resource_quota',
    'create_limit_range',
    'delete',
  ];
  constructor(injector: Injector, private featureState: FeatureStateService) {
    super(injector);
  }

  createLimitRange() {
    this.createSubResource('limitrange');
  }

  createResourceQuota() {
    this.createSubResource('resourcequota');
  }

  createSubResource(type: string) {
    this.router.navigate([
      `/namespace/${type}/create`,
      { namespace: this.resource.objectMeta.name },
    ]);
  }

  async doDelete() {
    try {
      const confirmed = await this.confirmBox.delete({
        kind: this.resource.typeMeta.kind,
        name: this.resource.objectMeta.name,
      });
      if (confirmed) {
        await this.resourceData.deleteDetail(this.getUrlParams()).toPromise();
        this.auiMessageService.success({
          content: this.translate.get('namespace.deleting'),
        });
        this.deleted.next(this.resource);
        if (
          this.featureState.get('namespace') === this.resource.objectMeta.name
        ) {
          this.featureState.set('namespace', '');
        }
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }
}
