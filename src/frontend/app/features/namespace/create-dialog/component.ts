import { DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { NamespaceTypeMeta } from '~api/raw-k8s';

import { ResourceDataService } from '../../../services';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceCreateDialogComponent {
  // namespace form control
  nfc = new FormControl(NamespaceTypeMeta);
  loading = false;

  constructor(
    private dialogRef: DialogRef,
    private resourceData: ResourceDataService,
    private cdr: ChangeDetectorRef,
  ) {}

  async onConfirm() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    try {
      await this.resourceData.create(this.nfc.value);
      this.dialogRef.close(this.nfc.value);
    } catch (err) {}
    this.loading = false;
    this.cdr.markForCheck();
  }
}
