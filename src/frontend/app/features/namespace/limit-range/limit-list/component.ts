import { Component, Input, OnChanges } from '@angular/core';
import { LimitRangeListItem } from '~api/backendapi';
import { LimitRangeItem } from '~api/raw-k8s';

import { flattenAndMergeLimitRangeItemList } from '../limit-range';

@Component({
  selector: 'alk-limit-range-limit-list',
  templateUrl: './template.html',
})
export class LimitRangeLimitListComponent implements OnChanges {
  @Input()
  limitRangeList: LimitRangeItem[];
  lists: LimitRangeListItem[] = [];

  columns = [
    'resourceType',
    'resourceName',
    'min',
    'max',
    'default',
    'defaultRequest',
    'maxLimitRequestRatio',
  ];

  ngOnChanges() {
    this.lists = flattenAndMergeLimitRangeItemList(this.limitRangeList);
  }
}
