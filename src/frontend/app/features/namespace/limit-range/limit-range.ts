import { groupBy, merge } from 'lodash';
import { LimitRangeListItem } from '~api/backendapi';
import { LimitRangeItem, LimitType } from '~api/raw-k8s';

// Convert a single LimitRangeItem by limit type
export function flattenItem(
  limitRangeItem: LimitRangeItem,
): LimitRangeListItem[] {
  const type = limitRangeItem.type;
  return Object.entries(limitRangeItem)
    .filter(([key]) => key !== 'type')
    .reduce((accum, [metricKey, metricValue]) => {
      const newItems = Object.entries(metricValue).map(
        ([resourceName, value]) => {
          return { resourceType: type, resourceName, [metricKey]: value };
        },
      );

      return [...accum, ...newItems];
    }, []);
}

// Flatten limitRangeList first and then merge by grouping with container + limit type
export function flattenAndMergeLimitRangeItemList(
  limitRangeList: LimitRangeItem[],
): LimitRangeListItem[] {
  // Firstly, flatten the items:
  const flattenedItems = (limitRangeList || []).reduce<LimitRangeListItem[]>(
    (accum, item) => [...accum, ...flattenItem(item)],
    [],
  );

  const lists: LimitRangeListItem[] = [];

  flattenedItems.forEach(toBeMergedItem => {
    let mergedItem = lists.find(
      item =>
        item.resourceName === toBeMergedItem.resourceName &&
        item.resourceType === toBeMergedItem.resourceType,
    );

    if (!mergedItem) {
      mergedItem = {
        resourceName: toBeMergedItem.resourceName,
        resourceType: toBeMergedItem.resourceType,
      };
      lists.push(mergedItem);
    }

    Object.assign(mergedItem, toBeMergedItem);
  });

  return lists;
}

export function recoverFromLimitRangeListItem(
  limitRangeListItems: LimitRangeListItem[],
): LimitRangeItem[] {
  // Firstly, group by resourceType:
  const items = groupBy<LimitRangeListItem>(
    limitRangeListItems,
    item => item.resourceType,
  );
  return Object.entries(items).map(([limitType, lrli]) =>
    lrli.reduce(
      (accum, curr) =>
        merge({}, accum, convertFromLimitRangeListItemToLimitRangeItem(curr)),
      { type: limitType as LimitType },
    ),
  );
}

export function convertFromLimitRangeListItemToLimitRangeItem(
  lrli: LimitRangeListItem,
): LimitRangeItem {
  const res: LimitRangeItem = { type: lrli.resourceType as LimitType };
  const resourceName = lrli.resourceName;

  Object.keys(lrli)
    .filter(key => !['resourceName', 'resourceType'].includes(key))
    .forEach((key: keyof LimitRangeItem) => {
      res[key] = {
        [resourceName]: lrli[key as keyof LimitRangeListItem],
      };
    });

  return res;
}
