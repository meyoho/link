import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';
import { LimitRangeListItem } from '~api/backendapi';

const RESOUECE_TYPES = ['Pod', 'Container', 'PersistentVolumeClaim']; // AKA limittype
const RESOUECE_NAMES = ['cpu', 'memory'];

@Component({
  selector: 'alk-limit-range-list-item-form',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LimitRangeListItemFormComponent
  extends BaseResourceFormGroupComponent<LimitRangeListItem>
  implements OnInit {
  @Input()
  excludedItems: LimitRangeListItem[] = [];

  resourceNames$: Observable<string[]>;
  resourceTypes: string[];

  createForm() {
    return this.fb.group({
      resourceType: this.fb.control(''),
      resourceName: this.fb.control(''),
      min: this.fb.control(''),
      max: this.fb.control(''),
      defaultRequest: this.fb.control(''),
      default: this.fb.control(''),
      maxLimitRequestRatio: this.fb.control(''),
    });
  }

  getDefaultFormModel(): LimitRangeListItem {
    return {};
  }

  ngOnInit() {
    super.ngOnInit();

    const resourceTypeControl = this.form.get('resourceType');
    const resourceNameControl = this.form.get('resourceName');

    this.resourceTypes = RESOUECE_TYPES.filter(
      resourceType => this.getResourceNameForType(resourceType).length,
    );

    this.resourceNames$ = resourceTypeControl.valueChanges.pipe(
      startWith(resourceTypeControl.value),
      map(resourceType => this.getResourceNameForType(resourceType)),
      tap(names => {
        if (names && names.length === 0) {
          resourceNameControl.disable({ emitEvent: false });
        } else {
          resourceNameControl.enable({ emitEvent: false });
        }
      }),
    );

    resourceTypeControl.valueChanges.subscribe(value => {
      // some fields should be disabled depending on different types:
      if (value === 'Pod') {
        this.form.get('defaultRequest').disable({ emitEvent: false });
        this.form.get('default').disable({ emitEvent: false });
      } else {
        this.form.get('defaultRequest').enable({ emitEvent: false });
        this.form.get('default').enable({ emitEvent: false });
      }
      if (resourceNameControl.dirty) {
        resourceNameControl.setValue('');
      }
    });
  }

  getResourceNameForType(resourceType: string) {
    const resourceNames =
      resourceType === 'PersistentVolumeClaim' ? ['storage'] : RESOUECE_NAMES;

    return resourceNames.filter(
      resourceName =>
        !this.excludedItems.some(
          item =>
            item.resourceName === resourceName &&
            item.resourceType === resourceType,
        ),
    );
  }

  trackByFn(index: number) {
    return index;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
