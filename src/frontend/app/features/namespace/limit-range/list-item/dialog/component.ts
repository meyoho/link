import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { LimitRangeListItem } from '~api/backendapi';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LimitRangeListItemDialogComponent {
  limitRangeListItem: LimitRangeListItem = {};
  excludedItems: LimitRangeListItem[] = [];

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      item: LimitRangeListItem;
      excludedItems: LimitRangeListItem[];
    },
    private dialogRef: DialogRef,
  ) {
    if (data) {
      this.limitRangeListItem = this.data.item;
      this.excludedItems = this.data.excludedItems;
    }
  }

  onConfirm() {
    this.dialogRef.close(this.limitRangeListItem);
  }
}
