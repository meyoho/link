import { LimitRangeListItem } from '~api/backendapi';
import { LimitRangeItem } from '~api/raw-k8s';

import {
  flattenAndMergeLimitRangeItemList,
  flattenItem,
  recoverFromLimitRangeListItem,
} from './limit-range';

describe('LimitRange utility', () => {
  beforeEach(() => {});

  it('flatten a single LimitRangeItem', () => {
    const input: LimitRangeItem = {
      type: 'Container',
      min: {
        cpu: '123',
        memory: '456',
      },
      default: {
        cpu: '111',
      },
      defaultRequest: {
        memory: '222',
        pod: '4',
      },
    };

    const expected: LimitRangeListItem[] = [
      { min: '123', resourceName: 'cpu', resourceType: 'Container' },
      { min: '456', resourceName: 'memory', resourceType: 'Container' },
      { default: '111', resourceName: 'cpu', resourceType: 'Container' },
      {
        defaultRequest: '222',
        resourceName: 'memory',
        resourceType: 'Container',
      },
      { defaultRequest: '4', resourceName: 'pod', resourceType: 'Container' },
    ];

    expect(flattenItem(input)).toEqual(expected);
  });

  it('flatten a single LimitRangeItem and then merge', () => {
    const input: LimitRangeItem = {
      type: 'Container',
      min: {
        cpu: '123',
        memory: '456',
      },
      default: {
        cpu: '111',
      },
      defaultRequest: {
        memory: '222',
        pod: '4',
      },
    };

    const expected: LimitRangeListItem[] = [
      {
        default: '111',
        min: '123',
        resourceName: 'cpu',
        resourceType: 'Container',
      },
      {
        defaultRequest: '222',
        min: '456',
        resourceName: 'memory',
        resourceType: 'Container',
      },
      { defaultRequest: '4', resourceName: 'pod', resourceType: 'Container' },
    ];
    expect(flattenAndMergeLimitRangeItemList([input])).toEqual(expected);
  });

  it('flatten a list of LimitRangeItem and then merge', () => {
    const input: LimitRangeItem[] = [
      {
        type: 'Container',
        min: {
          cpu: '123',
          memory: '456',
        },
        default: {
          cpu: '111',
        },
        defaultRequest: {
          memory: '222',
          pod: '4',
        },
      },
      {
        type: 'Pod',
        min: {
          cpu: '123',
          memory: '456',
        },
        default: {
          cpu: '111',
        },
        defaultRequest: {
          memory: '222',
          pod: '4',
        },
      },
    ];

    const expected: LimitRangeListItem[] = [
      {
        default: '111',
        min: '123',
        resourceName: 'cpu',
        resourceType: 'Container',
      },
      {
        defaultRequest: '222',
        min: '456',
        resourceName: 'memory',
        resourceType: 'Container',
      },
      { defaultRequest: '4', resourceName: 'pod', resourceType: 'Container' },
      { default: '111', min: '123', resourceName: 'cpu', resourceType: 'Pod' },
      {
        defaultRequest: '222',
        min: '456',
        resourceName: 'memory',
        resourceType: 'Pod',
      },
      { defaultRequest: '4', resourceName: 'pod', resourceType: 'Pod' },
    ];
    expect(flattenAndMergeLimitRangeItemList(input)).toEqual(expected);
  });

  it('A list of LimitRangeListItem could be converted to list of LimitRangeItem', () => {
    const input: LimitRangeListItem[] = [
      {
        default: '111',
        min: '123',
        resourceName: 'cpu',
        resourceType: 'Container',
      },
      {
        defaultRequest: '222',
        min: '456',
        resourceName: 'memory',
        resourceType: 'Container',
      },
      { defaultRequest: '4', resourceName: 'pod', resourceType: 'Container' },
      { default: '111', min: '123', resourceName: 'cpu', resourceType: 'Pod' },
      {
        defaultRequest: '222',
        min: '456',
        resourceName: 'memory',
        resourceType: 'Pod',
      },
      { defaultRequest: '4', resourceName: 'pod', resourceType: 'Pod' },
    ];

    const expected: LimitRangeItem[] = [
      {
        type: 'Container',
        min: {
          cpu: '123',
          memory: '456',
        },
        default: {
          cpu: '111',
        },
        defaultRequest: {
          memory: '222',
          pod: '4',
        },
      },
      {
        type: 'Pod',
        min: {
          cpu: '123',
          memory: '456',
        },
        default: {
          cpu: '111',
        },
        defaultRequest: {
          memory: '222',
          pod: '4',
        },
      },
    ];

    expect(recoverFromLimitRangeListItem(input)).toEqual(expected);
  });
});
