import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { merge } from 'lodash';
import { LimitRange, LimitRangeTypeMeta } from '~api/raw-k8s';
import { BaseKubernetesResourceFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-limit-range-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LimitRangeFormComponent extends BaseKubernetesResourceFormComponent<
  LimitRange
> {
  namespaced = true;
  kind = 'LimitRange';

  @Input()
  namespace: string;

  createForm() {
    const metadata = this.fb.group({
      name: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    const spec = this.fb.group({
      limits: this.fb.control([]),
    });

    return this.fb.group({
      metadata,
      spec,
    });
  }

  getDefaultFormModel(): LimitRange {
    return { ...LimitRangeTypeMeta, metadata: { namespace: this.namespace } };
  }

  adaptFormModel(resource: LimitRange) {
    const model = merge({}, resource, this.getDefaultFormModel());
    delete model.metadata.resourceVersion;
    return model;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
