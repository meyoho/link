import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { NamespaceDetail, Resource } from '~api/backendapi';

@Component({
  selector: 'alk-namespace-detail-basic-info',
  templateUrl: './detail-basic-info.component.html',
  styleUrls: ['./detail-basic-info.component.scss'],
})
export class NamespaceDetailBasicInfoComponent implements OnChanges {
  @Input()
  detail: NamespaceDetail;

  @Output()
  updated = new EventEmitter();

  selectedQuotaName = '';
  selectedLimitRangeName = '';

  get hideEffectiveResourceQuota() {
    return this.detail && this.detail.resourceQuotas.length === 1;
  }

  get hideEffectiveLimitRange() {
    return this.detail && this.detail.resourceLimits.length === 1;
  }

  get selectedQuota() {
    if (this.selectedQuotaName && this.detail) {
      return this.detail.resourceQuotas.find(
        quota => quota.objectMeta.name === this.selectedQuotaName,
      );
    }
  }

  get selectedLimitRange() {
    if (this.selectedLimitRangeName && this.detail) {
      return this.detail.resourceLimits.find(
        limit => limit.objectMeta.name === this.selectedLimitRangeName,
      );
    }
  }

  ngOnChanges(): void {
    if (!this.detail) {
      return;
    }

    if (
      this.hideEffectiveResourceQuota ||
      (this.detail.resourceQuotas.length &&
        this.selectedQuotaName &&
        !this.detail.resourceQuotas.some(
          quota => quota.objectMeta.name === this.selectedQuotaName,
        ))
    ) {
      this.selectedQuotaName = this.detail.resourceQuotas[0].objectMeta.name;
    }

    if (
      this.hideEffectiveLimitRange ||
      (this.detail.resourceLimits.length &&
        this.selectedLimitRangeName &&
        !this.detail.resourceLimits.some(
          limit => limit.objectMeta.name === this.selectedLimitRangeName,
        ))
    ) {
      this.selectedLimitRangeName = this.detail.resourceLimits[0].objectMeta.name;
    }

    this.cdr.markForCheck();
  }

  trackByFn(_index: number, item: Resource) {
    return item.objectMeta.name;
  }

  constructor(private cdr: ChangeDetectorRef) {}
}
