import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Namespace, NamespaceList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

import { NamespaceCreateDialogComponent } from './create-dialog/component';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'phase',
    label: 'status',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-namespace-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceListPageComponent
  extends BaseResourceListPageComponent<NamespaceList, Namespace>
  implements OnInit {
  map(value: NamespaceList): Namespace[] {
    return value.namespaces;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<NamespaceList> {
    return this.http.get<NamespaceList>('api/v1/namespaces', {
      params: getQuery(
        // Currently filter by name means "search"
        filterBy('name', params.search),
        sortBy(params.sort.active, params.sort.direction === 'desc'),
        pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
      ),
    });
  }

  async onCreateNamespace() {
    await this.dialog
      .open(NamespaceCreateDialogComponent, { size: DialogSize.Big })
      .afterClosed()
      .toPromise();
    this.updated$.next(null);
  }

  constructor(injector: Injector, private dialog: DialogService) {
    super(injector);
  }
}
