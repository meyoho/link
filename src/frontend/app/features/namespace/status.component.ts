import { Component, Input } from '@angular/core';
import { GenericStatus } from '~api/status';

const ICONS = {
  Active: GenericStatus.Success,
  Terminating: GenericStatus.Terminating,
};

@Component({
  selector: 'alk-namespace-status',
  template: `
    <ng-container *ngIf="phase">
      <alk-status-icon [status]="iconStatus"></alk-status-icon>
      {{ phase | lowercase | translate }}
    </ng-container>
    <ng-container *ngIf="!phase">-</ng-container>
  `,
  styles: [
    `
      :host {
        display: inline-flex;
        align-items: center;
      }
      alk-status-icon {
        margin-right: 4px;
      }
    `,
  ],
})
export class NamespaceStatusComponent {
  @Input()
  phase: 'Active' | 'Terminating';

  get iconStatus() {
    return ICONS[this.phase];
  }
}
