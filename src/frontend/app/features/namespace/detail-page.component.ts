import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { NamespaceDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { FeatureStateService, ResourceDataService } from '../../services';

@Component({
  selector: 'alk-namespace-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceDetailPageComponent
  extends BaseDetailPageComponent<NamespaceDetail>
  implements OnInit {
  constructor(
    private resourceData: ResourceDataService,
    private featureState: FeatureStateService,
    injector: Injector,
  ) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<NamespaceDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'namespace' });
  }

  ngOnInit() {
    super.ngOnInit();

    this.featureState.set(
      'namespace',
      this.activatedRoute.snapshot.params.name,
    );
  }

  namespaceChanged(name: string) {
    this.doNavigate({ name });
  }
}
