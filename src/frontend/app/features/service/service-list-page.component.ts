import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Service, ServiceList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'type',
    label: 'type',
  },
  {
    name: 'cluster_ip',
    label: 'cluster_ip',
  },
  {
    name: 'internal_endpoints',
    label: 'internal_endpoints',
  },
  {
    name: 'selectors',
    label: 'selectors',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-service-list-page',
  templateUrl: './service-list-page.component.html',
  styleUrls: ['./service-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListPageComponent
  extends BaseResourceListPageComponent<ServiceList, Service>
  implements OnInit {
  map(value: ServiceList): Service[] {
    return value.services;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<ServiceList> {
    return this.http.get<ServiceList>(
      `api/v1/services/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  getCellValue(item: Service, colName: string) {
    switch (colName) {
      case 'cluster_ip':
        return item.clusterIP;
      case 'internal_endpoints':
        return item.internalEndpoint;
      case 'selectors':
        return item.selector;
      default:
        return super.getCellValue(item, colName);
    }
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
