import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { cloneDeep } from 'lodash';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Port, Service, ServiceTypeMeta } from '~api/raw-k8s';
import { BaseKubernetesResourceFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-service-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceFormComponent
  extends BaseKubernetesResourceFormComponent<Service>
  implements OnInit, OnDestroy {
  kind = 'Service';
  typeSub: Subscription;
  curServiceType: string;
  clusterIPSub: Subscription;
  clusterIP: string;
  originTypes = ['ClusterIP', 'NodePort', 'LoadBalancer', 'ExternalName'];
  clusterIPTypes = ['auto', 'manual', 'None'];
  clusterIPType: string;

  getDefaultFormModel() {
    return ServiceTypeMeta;
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(/^[a-z0-9^-]*[a-z0-9-]*[a-z0-9]$/),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    const specForm = this.fb.group({
      ports: this.fb.control([]),
      type: this.fb.control('', [Validators.required]),
      selector: this.fb.control({}),
      externalName: this.fb.control(''),
      clusterIP: this.fb.control(''),
      loadBalancerIP: this.fb.control(''),
      sessionAffinity: this.fb.control(''),
    });
    return this.fb.group({
      metadata: metadataForm,
      spec: specForm,
    });
  }

  CluterIPTypeChange(type: string) {
    if (type === 'None') {
      this.form.get('spec.clusterIP').setValue('None');
    } else {
      this.form.get('spec.clusterIP').setValue('');
    }
  }

  ngOnInit() {
    super.ngOnInit();
    this.typeSub = this.form
      .get('spec.type')
      .valueChanges.pipe(filter(value => !!value))
      .subscribe(type => {
        if (type !== 'ClusterIP') {
          this.clusterIPTypes = ['auto', 'manual'];
          if (this.clusterIP === 'None') {
            this.form.get('spec.clusterIP').setValue('');
          }
          if (type === 'ExternalName') {
            this.form.get('spec.ports').setValue([]);
            this.form.get('spec.clusterIP').setValue('');
          }
        } else {
          this.clusterIPTypes = ['auto', 'manual', 'None'];
        }
        this.curServiceType = type;
      });
    this.clusterIPSub = this.form
      .get('spec.clusterIP')
      .valueChanges.subscribe(value => {
        this.clusterIP = value;
        if (!value && this.clusterIPType !== 'manual') {
          this.clusterIPType = 'auto';
        } else if (value === 'None') {
          this.clusterIPType = 'None';
        } else {
          this.clusterIPType = 'manual';
        }
      });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.typeSub.unsubscribe();
    this.clusterIPSub.unsubscribe();
  }

  adaptFormModel(form: { [key: string]: any }) {
    const spec = cloneDeep(form.spec);
    if (spec.ports.length) {
      spec.ports.forEach((port: Port) => {
        if (!port.nodePort) {
          delete port.nodePort;
        }
      });
    }
    if (!spec.clusterIP) {
      delete spec.clusterIP;
    }
    return { ...form, spec };
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
