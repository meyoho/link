import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Port } from '~api/raw-k8s';

@Component({
  selector: 'alk-ports-item-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortsItemsFormComponent extends BaseResourceFormArrayComponent<
  Port
> {
  @Input()
  clusterIP: boolean;

  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): Port[] {
    return [
      {
        name: 'http',
        protocol: 'TCP',
        port: 80,
        targetPort: 12345,
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      name: this.fb.control(''),
      protocol: this.fb.control('TCP'),
      port: this.fb.control('', [Validators.min(1), Validators.max(65535)]),
      nodePort: this.fb.control('', [Validators.min(1), Validators.max(65535)]),
      targetPort: this.fb.control('', [
        Validators.min(1),
        Validators.max(65535),
      ]),
    });
  }
}
