import { Component, Injector, Input } from '@angular/core';
import { set } from 'lodash';
import { Service, StringMap } from '~api/backendapi';
import { BaseActionMenuComponent } from '~app/abstract';
import { UpdateLabelsComponent } from '~app/features-shared/common';

@Component({
  selector: 'alk-service-action-menu',
  templateUrl: './service-action-menu.component.html',
  styleUrls: [],
})
export class ServiceActionMenuComponent extends BaseActionMenuComponent<
  Service
> {
  @Input()
  actions = [
    'update',
    'update_labels',
    'update_annotations',
    'update_selectors',
    'delete',
  ];
  constructor(injector: Injector) {
    super(injector);
  }

  showUpdateSelectors() {
    const dialogRef = this.dialog.open(UpdateLabelsComponent);

    dialogRef.componentInstance.labels = this.resource.selector;
    dialogRef.componentInstance.title = this.translate.get('update_selectors');

    dialogRef.componentInstance.onUpdate = async (labels: StringMap) => {
      const detailParams = this.resourceData.getResourceDetailParams(
        this.resource,
      );

      const { data } = await this.resourceData
        .getRawDetail(detailParams)
        .toPromise();
      set(data, 'spec.selector', labels);

      try {
        this.resourceData.updateDetail(detailParams, data).toPromise();
        this.updated.next(null);
        this.auiMessageService.success({
          content: this.translate.get('update_succeeded'),
        });
        dialogRef.close();
      } catch (error) {
        this.auiNotificationService.error({ content: error.error || error });
      }
    };
  }
}
