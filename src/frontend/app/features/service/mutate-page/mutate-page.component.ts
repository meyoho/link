import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';

import { Service } from '~api/raw-k8s';
import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  selector: 'alk-service-mutate-page',
  templateUrl: './mutate-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMutatePageComponent extends BaseResourceMutatePageComponent<
  Service
> {
  get kind() {
    return 'Service';
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
