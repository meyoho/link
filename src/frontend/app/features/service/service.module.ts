import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { PodListModule } from '../../features-shared/pod-list/module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { EndpointListComponent } from './endpoint-list.component';
import { ServiceFormComponent } from './form/component';
import { PortsItemsFormComponent } from './form/ports-form/component';
import i18n from './i18n';
import { ServiceMutatePageComponent } from './mutate-page/mutate-page.component';
import { ServiceActionMenuComponent } from './service-action-menu.component';
import { ServiceDetailBasicInfoComponent } from './service-detail-basic-info.component';
import { ServiceDetailPageComponent } from './service-detail-page.component';
import { ServiceListPageComponent } from './service-list-page.component';
import { ServiceRoutingModule } from './service-routing.module';
const MODULE_PREFIX = 'service';

@NgModule({
  imports: [
    SharedModule,
    FeatureSharedCommonModule,
    ServiceRoutingModule,
    PodListModule,
  ],
  declarations: [
    ServiceListPageComponent,
    ServiceActionMenuComponent,
    ServiceDetailPageComponent,
    ServiceDetailBasicInfoComponent,
    EndpointListComponent,
    ServiceMutatePageComponent,
    ServiceFormComponent,
    PortsItemsFormComponent,
  ],
  providers: [],
})
export class ServiceModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
