import { Component, Input, OnInit } from '@angular/core';
import { Port } from '~api/raw-k8s';

@Component({
  selector: 'alk-endpoint-list',
  templateUrl: `./endpoint-list.component.html`,
})
export class EndpointListComponent implements OnInit {
  columns = ['name', 'protocol', 'port', 'targetPort', 'nodePort'];

  @Input()
  clusterIP: boolean;
  @Input()
  ports: Port[];

  ngOnInit() {
    if (this.clusterIP) {
      this.columns = this.columns.filter(cl => cl !== 'nodePort');
    }
  }

  get showZeroState() {
    return !this.ports || this.ports.length === 0;
  }
}
