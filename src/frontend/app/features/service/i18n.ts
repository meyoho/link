export const en = {
  create_service: 'Create Service',
  session_affinity: 'Session Affinity',
  ClusterIP: 'Cluster IP',
  sessionAffinity: 'Session Affinity',
  port: 'Port',
  nodePort: 'Node port',
  targetPort: 'Target Port',
  ports_config: 'Ports config',
  loadBalancerIP: 'loadBalancer IP',
};

export const zh = {
  create_service: '创建服务',
  session_affinity: '会话保持',
  ClusterIP: '集群IP',
  sessionAffinity: '会话保持',
  port: '服务端口',
  nodePort: '主机端口',
  targetPort: 'Pod端口',
  ports_config: '端口配置',
  loadBalancerIP: '负载均衡器IP',
};

export default {
  en,
  zh,
};
