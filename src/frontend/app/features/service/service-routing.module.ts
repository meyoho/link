import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ResourceMutatePageDeactivateGuard } from '~app/services';

import { ServiceMutatePageComponent } from './mutate-page/mutate-page.component';
import { ServiceDetailPageComponent } from './service-detail-page.component';
import { ServiceListPageComponent } from './service-list-page.component';

// TODO: move somewhere else, like a universal sample provider or laoder?
const sample = `kind: Service
apiVersion: v1
metadata:
  name: my-service
  namespace: default
spec:
  type: ClusterIP
  ports:
  - name: http
    protocol: TCP
    port: 80
    targetPort: 12345
  sessionAffinity: None`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: ServiceListPageComponent,
      },
      {
        path: 'detail',
        component: ServiceDetailPageComponent,
      },
      {
        path: 'update',
        component: ServiceMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
      },
      {
        path: 'create',
        component: ServiceMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class ServiceRoutingModule {}
