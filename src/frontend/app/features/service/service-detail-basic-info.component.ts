import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ServiceDetail } from '~api/backendapi';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-service-detail-basic-info',
  templateUrl: './service-detail-basic-info.component.html',
  styleUrls: [],
})
export class ServiceDetailBasicInfoComponent {
  @Input()
  detail: ServiceDetail;
  @Output()
  updated = new EventEmitter();

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'service',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }

  get internalEndpoint(): string[] {
    if (
      this.detail &&
      this.detail.internalEndpoint &&
      this.detail.internalEndpoint.ports
    ) {
      return this.detail.internalEndpoint.ports.map(
        port =>
          `${this.detail.internalEndpoint.host}:${port.port} ${port.protocol}`,
      );
    } else {
      return [];
    }
  }

  constructor(private resourceData: ResourceDataService) {}
}
