import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { ConfigMapActionMenuComponent } from './action-menu.component';
import { ConfigMapDataViwerComponent } from './data-viewer/component';
import { ConfigMapDetailBasicInfoComponent } from './detail-basic-info.component';
import { ConfigMapDetailPageComponent } from './detail-page.component';
import { ConfigMapFormComponent } from './form/component';
import i18n from './i18n';
import { ConfigMapListPageComponent } from './list-page.component';
import { ConfigMapMutatePageComponent } from './mutate-page/mutate-page.component';
import { ConfigMapRoutingModule } from './routing.module';
const MODULE_PREFIX = 'configmap';

@NgModule({
  imports: [SharedModule, FeatureSharedCommonModule, ConfigMapRoutingModule],
  declarations: [
    ConfigMapListPageComponent,
    ConfigMapActionMenuComponent,
    ConfigMapDetailPageComponent,
    ConfigMapDetailBasicInfoComponent,
    ConfigMapDataViwerComponent,
    ConfigMapFormComponent,
    ConfigMapMutatePageComponent,
  ],
  providers: [],
})
export class ConfigMapModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
