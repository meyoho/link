import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { Observable, fromEvent } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'alk-configmap-data-viewer',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class ConfigMapDataViwerComponent implements OnInit, OnChanges {
  @Input()
  data: { [key: string]: string };
  pageScroll$: Observable<number>;

  configs: [string, string][] = [];

  constructor() {}

  ngOnInit(): void {
    const layoutPageEl = document.querySelector('.aui-layout__page');
    this.pageScroll$ = fromEvent(layoutPageEl, 'scroll').pipe(
      startWith({}),
      map(() => layoutPageEl.scrollTop),
    );
  }

  ngOnChanges({ data }: SimpleChanges) {
    if (data) {
      this.configs = Object.entries(this.data || {});
    }
  }
}
