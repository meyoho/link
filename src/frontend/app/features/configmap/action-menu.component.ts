import { Component, Injector, Input } from '@angular/core';
import { ConfigMap } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-configmap-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: [],
})
export class ConfigMapActionMenuComponent extends BaseActionMenuComponent<
  ConfigMap
> {
  @Input()
  actions = ['update', 'update_labels', 'update_annotations', 'delete'];
  constructor(injector: Injector) {
    super(injector);
  }
}
