import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { ConfigMap } from '~api/raw-k8s';

import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  selector: 'alk-configmap-mutate-page',
  templateUrl: './mutate-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapMutatePageComponent extends BaseResourceMutatePageComponent<
  ConfigMap
> {
  get kind() {
    return 'ConfigMap';
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
