import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ResourceMutatePageDeactivateGuard } from '~app/services';

import { ConfigMapDetailPageComponent } from './detail-page.component';
import { ConfigMapListPageComponent } from './list-page.component';
import { ConfigMapMutatePageComponent } from './mutate-page/mutate-page.component';

// TODO: move somewhere else, like a universal sample provider or laoder?
const sample = `apiVersion: v1
data:
  special-how: very
  special-type: charm
kind: ConfigMap
metadata:
  name: special-config
  namespace: default`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: ConfigMapListPageComponent,
      },
      {
        path: 'detail',
        component: ConfigMapDetailPageComponent,
      },
      {
        path: 'update',
        component: ConfigMapMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
      },
      {
        path: 'create',
        component: ConfigMapMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class ConfigMapRoutingModule {}
