export const en = {
  create_configmap: 'Create Config Map',
};

export const zh = {
  create_configmap: '创建配置字典',
};

export default {
  en,
  zh,
};
