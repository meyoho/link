import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ConfigMapDetail } from '~api/backendapi';

@Component({
  selector: 'alk-configmap-detail-basic-info',
  templateUrl: './detail-basic-info.component.html',
  styleUrls: ['./detail-basic-info.component.scss'],
})
export class ConfigMapDetailBasicInfoComponent {
  @Input()
  detail: ConfigMapDetail;
  @Output()
  updated = new EventEmitter();
}
