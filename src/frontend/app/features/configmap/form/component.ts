import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { ConfigMap, ConfigMapTypeMeta } from '~api/raw-k8s';

import { BaseKubernetesResourceFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-configmap-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigMapFormComponent
  extends BaseKubernetesResourceFormComponent<ConfigMap>
  implements OnInit {
  kind = 'ConfigMap';

  getDefaultFormModel() {
    return ConfigMapTypeMeta;
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(/^[a-z0-9^-]*[a-z0-9-]*[a-z0-9]$/),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    return this.fb.group({
      metadata: metadataForm,
      data: this.fb.control({}),
    });
  }

  ngOnInit() {
    super.ngOnInit();
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
