import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { JobDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-job-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobDetailPageComponent extends BaseDetailPageComponent<JobDetail>
  implements OnInit {
  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml', 'config_management', 'logs', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<JobDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'job' });
  }
}
