import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  SimpleResourceCreatePageComponent,
  SimpleResourceUpdatePageComponent,
} from '~app/features-shared/common';

import { JobDetailPageComponent } from './detail-page.component';
import { JobListPageComponent } from './list-page.component';

// TODO: move somewhere else, like a universal sample provider or laoder?
const sample = `apiVersion: batch/v1
kind: Job
metadata:
  name: countdown-example
  namespace: default
spec:
  completions: 3
  template:
    metadata:
      name: countdown-example
    spec:
      containers:
      - name: counter
        image: busybox
        command:
         - "/bin/sh"
         - "-c"
         - "for i in \`seq 1 100\`; do echo $i; sleep 1 ; done"
      restartPolicy: Never
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: JobListPageComponent,
      },
      {
        path: 'detail',
        component: JobDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
      {
        path: 'create',
        component: SimpleResourceCreatePageComponent,
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class JobRoutingModule {}
