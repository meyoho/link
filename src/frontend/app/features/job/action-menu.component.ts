import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Job, JobDetail } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-job-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobActionMenuComponent extends BaseActionMenuComponent<
  Job | JobDetail
> {
  @Input()
  actions = [
    'update',
    'view_logs',
    'update_labels',
    'update_annotations',
    'delete',
  ];
  constructor(injector: Injector) {
    super(injector);
  }
}
