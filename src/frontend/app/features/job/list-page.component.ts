import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Job, JobList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'pods',
    label: 'status',
  },
  {
    name: 'containerImages',
    label: 'images',
  },
  {
    name: 'metrics',
    label: 'used_resources',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-job-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobListPageComponent
  extends BaseResourceListPageComponent<JobList, Job>
  implements OnInit {
  map(value: JobList): Job[] {
    return value.jobs;
  }

  getCellValue(item: Job, colName: string) {
    switch (colName) {
      default:
        return super.getCellValue(item, colName);
    }
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<JobList> {
    return this.http.get<JobList>(`api/v1/jobs/${params.namespace || ''}`, {
      params: getQuery(
        filterBy('name', params.search),
        sortBy(params.sort.active, params.sort.direction === 'desc'),
        pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
      ),
    });
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
