import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { BaseDetailPageComponent } from '~app/abstract';

@Component({
  selector: 'alk-user-detail-page',
  templateUrl: './user-detail-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserDetailPageComponent extends BaseDetailPageComponent<any>
  implements OnInit {
  tabs: string[];

  fetchData(params: { name: string }): Observable<any> {
    return this.http.get<any>('api/v1/users/' + params.name);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  constructor(private http: HttpClient, injector: Injector) {
    super(injector);
  }
}
