import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UsersList, UsersListItem } from '~api/backendapi';

import { ColumnDef } from '~app/abstract';

@Component({
  selector: 'alk-users-list-page',
  templateUrl: './users-list-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UsersListPageComponent {
  list$: Observable<UsersListItem[]> = this.fetchResources();
  zeroState$: Observable<boolean> = this.list$.pipe(
    map(list => list.length === 0),
  );
  columnDefs: ColumnDef[] = [
    {
      name: 'email',
      label: 'email',
    },
    {
      name: 'name',
      label: 'name',
    },
    {
      name: 'group',
      label: 'group',
    },
  ];
  columns = this.columnDefs.map(columnDef => columnDef.name);

  fetchResources(): Observable<UsersListItem[]> {
    return this.http
      .get<UsersList>('api/v1/users')
      .pipe(map(({ list }) => list));
  }

  constructor(private http: HttpClient) {}
}
