import { MessageService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NamespaceList, Resource, RolesList } from '~api/backendapi';

import { TranslateService } from '../../../translate';
import { filterBy, getQuery } from '../../../utils';

@Component({
  selector: 'alk-role-bindings-create',
  templateUrl: './role-bindings-create-page.component.html',
  styleUrls: ['./role-bindings-create-page.component.scss'],
})
export class RoleBindingsCreatePageComponent implements OnInit {
  roles$: Observable<Resource[]>;
  namespaces$: Observable<string[]>;
  roleType: 'ClusterRole' | 'Role' = 'Role';
  data: any = {
    apiVersion: 'rbac.authorization.k8s.io/v1beta1',
    kind: 'Rolebinding',
    metadata: {
      name: '',
      namespace: '',
      annotations: {
        displayName: '',
        description: '',
      },
    },
    roleRef: {
      kind: 'Role',
      name: '',
      apiGroup: 'rbac.authorization.k8s.io',
    },
    subjects: [
      {
        kind: '',
        name: '',
        namespace: '',
      },
    ],
  };
  nameRequired: boolean;
  namespaceRequired: boolean;
  roleRequired: boolean;
  subjectsError: boolean;

  constructor(
    private http: HttpClient,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.namespaces$ = this.http
      .get<NamespaceList>('api/v1/permission_namespaces')
      .pipe(
        map(({ namespaces }) =>
          namespaces.map(namespaceMeta => namespaceMeta.objectMeta.name),
        ),
      );
    this.roles$ = this.fetchRoles(this.data.metadata.namespace, this.roleType);
  }

  onNamespaceChange(namespace: string) {
    this.namespaceRequired = !namespace;
    this.data.roleRef.name = '';
    this.roles$ = this.fetchRoles(this.data.metadata.namespace, this.roleType);
  }

  onRoleTypeChange(type: string) {
    if (type === 'ClusterRole') {
      this.data.metadata.namespace = '';
    }
    this.data.kind = type + 'binding';
    this.data.roleRef.kind = type;
    this.data.roleRef.name = '';
    this.roles$ = this.fetchRoles(this.data.metadata.namespace, this.roleType);
  }

  roleFilterChange(type: string) {
    const namespace = type === 'Role' ? this.data.metadata.namespace : '';
    this.data.roleRef.kind = type;
    this.roles$ = this.fetchRoles(namespace, type);
  }

  fetchRoles(namespace: string, roleType: string) {
    return this.http
      .get<RolesList>('api/v1/rbac/roles', {
        params: getQuery(
          filterBy('namespace', namespace),
          filterBy('kind', roleType),
        ),
      })
      .pipe(map(({ items }) => items || []));
  }

  onNameChange(name: string) {
    this.nameRequired = !name;
  }

  onRoleChange(role: string) {
    this.roleRequired = !role;
  }

  subjectsChange(subjects: any[]) {
    this.data.subjects = [...subjects];
    const err = this.data.subjects.some(
      (sub: any) =>
        !sub.name ||
        !sub.kind ||
        (sub.kind === 'ServiceAccount' && !sub.namespace),
    );
    this.subjectsError = !!err;
  }

  submit() {
    const data = cloneDeep(this.data);
    this.nameRequired = !data.metadata.name;
    this.roleRequired = !data.roleRef.name;
    this.namespaceRequired =
      this.roleType === 'Role' && !data.metadata.namespace;
    this.subjectsError = this.data.subjects.some(
      (sub: any) =>
        !sub.name ||
        !sub.kind ||
        (sub.kind === 'ServiceAccount' && !sub.namespace),
    );
    if (
      this.nameRequired ||
      this.namespaceRequired ||
      this.roleRequired ||
      this.subjectsError
    ) {
      return;
    }

    const endpoint =
      this.roleType === 'Role'
        ? `api/v1/rbac/namespaces/${data.metadata.namespace}/rolebindings/`
        : `api/v1/rbac/clusterrolebindings/`;

    this.http
      .post(endpoint, this.data)
      .toPromise()
      .then(() => {
        this.auiMessageService.success({
          content:
            this.translate.get('create') + this.translate.get('succeeded'),
        });
        this.router.navigate([
          '/administrator/role-bindings/detail',
          {
            name: data.metadata.name,
            namespace: this.roleType === 'Role' ? data.metadata.namespace : '',
          },
        ]);
      })
      .catch(error => {
        this.auiNotificationService.error({ content: error.error || error });
      });
  }

  cancel() {
    this.router.navigate(['/administrator/role-bindings']);
  }
}
