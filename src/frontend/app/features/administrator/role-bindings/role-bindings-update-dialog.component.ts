import { MessageService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { cloneDeep } from 'lodash';
import { RoleBindingDetail } from '~api/backendapi';

import { TranslateService } from '../../../translate';

@Component({
  selector: 'alk-role-bindings-update-dialog',
  templateUrl: './role-bindings-update-dialog.component.html',
  styleUrls: ['./role-bindings-create-page.component.scss'],
})
export class RoleBindingsUpdateDialogComponent implements OnInit {
  @Input()
  detail: RoleBindingDetail;
  @Output()
  confirmed = new EventEmitter<boolean | RoleBindingDetail>();
  subjectsError: boolean;

  constructor(
    private http: HttpClient,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
  ) {}

  ngOnInit(): void {
    if (!this.detail.data.metadata.annotations) {
      this.detail.data.metadata.annotations = {
        displayName: '',
        description: '',
      };
    }
  }

  subjectsChange(subjects: any[]) {
    this.detail.data.subjects = [...subjects];
    const err = this.detail.data.subjects.some(
      (sub: any) =>
        !sub.name ||
        !sub.kind ||
        (sub.kind === 'ServiceAccount' && !sub.namespace),
    );
    if (!err) {
      this.subjectsError = false;
    }
  }

  submit() {
    const data = cloneDeep(this.detail.data);
    this.subjectsError = this.detail.data.subjects.some(
      (sub: any) =>
        !sub.name ||
        !sub.kind ||
        (sub.kind === 'ServiceAccount' && !sub.namespace),
    );
    if (this.subjectsError) {
      return;
    }

    const endpoint =
      this.detail.data.kind === 'RoleBinding'
        ? `api/v1/rbac/namespaces/${data.metadata.namespace}/rolebindings/${
            this.detail.data.metadata.name
          }`
        : `api/v1/rbac/clusterrolebindings/${this.detail.data.metadata.name}`;

    this.http
      .put(endpoint, this.detail.data)
      .toPromise()
      .then(() => {
        this.auiMessageService.success({
          content:
            this.translate.get('update') + this.translate.get('succeeded'),
        });
        this.confirmed.next(this.detail);
      })
      .catch(error => {
        this.auiNotificationService.error({ content: error.error || error });
      });
  }

  cancel() {
    this.confirmed.next(false);
  }
}
