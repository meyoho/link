import {
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import {
  Resource,
  RoleBindingListItem,
  RoleBindingsList,
} from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { ConfirmBoxService } from '../../../services';
import { TranslateService } from '../../../translate';
import {
  filterBy,
  getDetailRoutes,
  getQuery,
  pageBy,
  sortBy,
} from '../../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'administrator.role-bindings_name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'role_name',
    label: 'administrator.role_name',
  },
  {
    name: 'sbname',
    label: 'administrator.subject_names',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
  {
    name: 'action',
    label: '',
  },
];

@Component({
  selector: 'alk-role-bindings-list-page',
  templateUrl: './role-bindings-list-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoleBindingsListPageComponent
  extends BaseResourceListPageComponent<RoleBindingsList, Resource>
  implements OnInit {
  updateDialogRef: DialogRef<any>;
  detail: any;

  map(value: RoleBindingsList): Resource[] {
    return value.items.map(resource => ({
      ...resource,
    }));
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<RoleBindingsList> {
    return this.http.get<RoleBindingsList>(
      `api/v1/rbac/rolebindings/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  getSubjectsName(subjects: any) {
    if (subjects) {
      return subjects.map((sub: any) => sub.name).join(', ');
    }
  }

  async delete(item: any) {
    const endpoint = item.objectMeta.namespace
      ? `api/v1/rbac/namespaces/${item.objectMeta.namespace}/rolebindings/${
          item.objectMeta.name
        }`
      : `api/v1/rbac/clusterrolebindings/${item.objectMeta.name}`;
    try {
      const confirmed = await this.confirmBox.delete({
        kind: this.translate.get('role-bindings'),
        name: item.objectMeta.name,
      });
      if (confirmed) {
        await this.http
          .delete(endpoint, {
            responseType: 'text',
          })
          .toPromise();
        this.auiMessageService.success({
          content:
            this.translate.get('delete') + this.translate.get('succeeded'),
        });
        this.onUpdate(item);
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }

  async update(item: any, templateRef: TemplateRef<any>) {
    const endpoint = item.objectMeta.namespace
      ? `api/v1/rbac/namespaces/${item.objectMeta.namespace}/rolebindings/${
          item.objectMeta.name
        }`
      : `api/v1/rbac/clusterrolebindings/${item.objectMeta.name}`;
    this.detail = await this.http.get(endpoint).toPromise();
    this.updateDialogRef = this.dialog.open(templateRef, {
      data: cloneDeep(this.detail),
      size: DialogSize.Big,
    });
  }

  updated(item: any) {
    this.updateDialogRef.close();
    this.onUpdate(item);
  }

  getRoleDetailRoutes(item: RoleBindingListItem) {
    const roleRef = {
      kind: item.roleRef.kind,
      name: item.roleRef.name,
      namespace: item.objectMeta.namespace,
      apiGroup: item.roleRef.apiGroup,
    };
    return getDetailRoutes(roleRef);
  }

  getDetailRoutes(item: Resource) {
    return getDetailRoutes(item);
  }

  constructor(
    injector: Injector,
    private confirmBox: ConfirmBoxService,
    private translate: TranslateService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private dialog: DialogService,
  ) {
    super(injector);
  }
}
