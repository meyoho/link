import {
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Input,
  TemplateRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash';
import { RoleBindingDetail } from '~api/backendapi';
import { ColumnDef } from '~app/abstract';

import { ConfirmBoxService } from '../../../services';
import { TranslateService } from '../../../translate';
import { getDetailRoutes } from '../../../utils/detail-route-builder';

@Component({
  selector: 'alk-role-bindings-detail-basic-info',
  templateUrl: './role-bindings-detail-basic-info.component.html',
  styleUrls: ['./role-bindings-detail-basic-info.component.scss'],
})
export class RoleBindingsDetailBasicInfoComponent {
  @Input()
  detail: RoleBindingDetail;

  updateDialogRef: DialogRef<any>;

  columnDefs: ColumnDef[] = [
    {
      name: 'name',
      label: 'name',
    },
    {
      name: 'kind',
      label: 'kind',
    },
    {
      name: 'namespace',
      label: 'namespace',
    },
  ];
  columns = this.columnDefs.map(columnDef => columnDef.name);

  getRoleDetailRoutes(item: RoleBindingDetail) {
    const roleRef = {
      kind: item.data.roleRef.kind,
      name: item.data.roleRef.name,
      namespace: item.objectMeta.namespace,
      apiGroup: item.data.roleRef.apiGroup,
    };
    return getDetailRoutes(roleRef);
  }

  async delete() {
    const endpoint = this.detail.objectMeta.namespace
      ? `api/v1/rbac/namespaces/${
          this.detail.objectMeta.namespace
        }/rolebindings/${this.detail.objectMeta.name}`
      : `api/v1/rbac/clusterrolebindings/${this.detail.objectMeta.name}`;
    try {
      const confirmed = await this.confirmBox.delete({
        kind: this.detail.typeMeta.kind,
        name: this.detail.objectMeta.name,
      });
      if (confirmed) {
        await this.http
          .delete(endpoint, {
            responseType: 'text',
          })
          .toPromise();
        this.auiMessageService.success({
          content:
            this.translate.get('delete') + this.translate.get('succeeded'),
        });
        this.router.navigate(['/administrator/role-bindings']);
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }

  upadte(templateRef: TemplateRef<any>) {
    this.updateDialogRef = this.dialog.open(templateRef, {
      data: cloneDeep(this.detail),
      size: DialogSize.Big,
    });
  }

  onUpdate(res: any) {
    this.updateDialogRef.close(res);
    if (res) {
      this.detail = cloneDeep(res);
      this.cdr.detectChanges();
    }
  }

  constructor(
    private dialog: DialogService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private http: HttpClient,
    private confirmBox: ConfirmBoxService,
    private router: Router,
    private cdr: ChangeDetectorRef,
  ) {}
}
