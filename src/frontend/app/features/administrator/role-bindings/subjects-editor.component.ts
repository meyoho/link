import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { map } from 'rxjs/operators';
import { NamespaceList } from '~api/backendapi';

import { ColumnDef } from '~app/abstract';

interface Subject {
  kind: string;
  name: string;
  namespace?: string;
}

@Component({
  selector: 'alk-subjects-editor',
  templateUrl: './subjects-editor.component.html',
  styleUrls: ['./subjects-editor.component.scss'],
})
export class SubjectsEditorComponent {
  @Input()
  subjects: Subject[] = [
    {
      kind: '',
      name: '',
      namespace: '',
    },
  ];
  @Output()
  updated = new EventEmitter();

  namespaces$ = this.http
    .get<NamespaceList>('api/v1/permission_namespaces')
    .pipe(
      map(({ namespaces }) =>
        namespaces.map(namespaceMeta => namespaceMeta.objectMeta.name),
      ),
    );

  columnDefs: ColumnDef[] = [
    {
      name: 'kind',
      label: 'kind',
    },
    {
      name: 'name',
      label: 'name',
    },
    {
      name: 'namespace',
      label: 'namespace',
    },
    {
      name: 'delete',
      label: '',
    },
  ];
  columns = this.columnDefs.map(columnDef => columnDef.name);
  kinds: string[] = ['ServiceAccount', 'User', 'Group'];

  kindChange(kind: string, subject: Subject) {
    if (kind !== 'ServiceAccount') {
      subject.namespace = '';
    }
    this.emit();
  }

  addSubject() {
    this.subjects = [
      ...this.subjects,
      {
        kind: '',
        name: '',
        namespace: '',
      },
    ];
    this.emit();
  }

  deleteSubject(sub: Subject) {
    if (this.subjects.length === 1) {
      return;
    }
    this.subjects = this.subjects.filter(_sub => _sub !== sub);
    this.emit();
  }

  emit() {
    this.updated.next(this.subjects);
  }

  constructor(private http: HttpClient) {}
}
