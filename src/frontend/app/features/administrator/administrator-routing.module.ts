import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RoleBindingsCreatePageComponent } from './role-bindings/role-bindings-create-page.component';
import { RoleBindingsDetailPageComponent } from './role-bindings/role-bindings-detail-page.component';
import { RoleBindingsListPageComponent } from './role-bindings/role-bindings-list-page.component';
import { RoleCreatePageComponent } from './roles/role-create-page.component';
import { RoleDetailPageComponent } from './roles/role-detail-page.component';
import { RoleRuleAddComponent } from './roles/role-rule-add.component';
import { RolesListPageComponent } from './roles/roles-list-page.component';
import { UserDetailPageComponent } from './users/user-detail-page.component';
import { UsersListPageComponent } from './users/users-list-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'roles',
      },
      {
        path: 'roles',
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full',
          },
          {
            path: 'list',
            component: RolesListPageComponent,
          },
          {
            path: 'create',
            component: RoleCreatePageComponent,
          },
          {
            path: 'detail',
            component: RoleDetailPageComponent,
          },
          {
            path: 'rule',
            component: RoleRuleAddComponent,
          },
        ],
      },
      {
        path: 'role-bindings',
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full',
          },
          {
            path: 'list',
            component: RoleBindingsListPageComponent,
          },
          {
            path: 'detail',
            component: RoleBindingsDetailPageComponent,
          },
          {
            path: 'create',
            component: RoleBindingsCreatePageComponent,
          },
        ],
      },
      {
        path: 'users',
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full',
          },
          {
            path: 'list',
            component: UsersListPageComponent,
          },
          {
            path: 'detail',
            component: UserDetailPageComponent,
          },
        ],
      },
    ]),
  ],
  exports: [RouterModule],
})
export class AdministratorRoutingModule {}
