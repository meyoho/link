import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Resource, RolesList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import {
  filterBy,
  getDetailRoutes,
  getQuery,
  pageBy,
  sortBy,
} from '../../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'kind',
    label: 'administrator.role_type',
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
  },
  {
    name: 'action',
    label: '',
  },
];

@Component({
  selector: 'alk-roles-list-page',
  templateUrl: './roles-list-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RolesListPageComponent
  extends BaseResourceListPageComponent<RolesList, Resource>
  implements OnInit {
  map(value: RolesList): Resource[] {
    return value.items.map(resource => ({
      ...resource,
    }));
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<RolesList> {
    return this.http.get<RolesList>(
      `api/v1/rbac/roles/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  getDetailRoutes(item: Resource) {
    return getDetailRoutes(item);
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
