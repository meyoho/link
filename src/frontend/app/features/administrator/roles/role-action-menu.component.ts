import { DialogRef } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  Output,
  TemplateRef,
} from '@angular/core';
import { RoleDetail } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-role-action-menu',
  templateUrl: './role-action-menu.component.html',
  styleUrls: [],
})
export class RoleActionMenuComponent extends BaseActionMenuComponent<
  RoleDetail
> {
  @Input()
  resource: any;
  @Output()
  updated = new EventEmitter();
  @Output()
  deleted = new EventEmitter();

  updateDialogRef: DialogRef<any>;

  constructor(private http: HttpClient, injector: Injector) {
    super(injector);
  }

  async update(templateRef: TemplateRef<any>) {
    const params = this.resourceData.getResourceDetailParams(this.resource);
    const endpoint = params.namespace
      ? `api/v1/rbac/namespaces/${params.namespace || ''}/roles/${params.name}`
      : `api/v1/rbac/clusterroles/${params.name}`;
    this.updateDialogRef = this.dialog.open(templateRef, {
      data: await this.http.get<RoleDetail>(endpoint).toPromise(),
    });
  }

  onConfirm(res: boolean) {
    if (res) {
      this.updateDialogRef.close(res);
      this.updated.next(null);
      this.auiMessageService.success({
        content: this.translate.get('update') + this.translate.get('succeeded'),
      });
    } else if (res === false) {
      this.updateDialogRef.close(res);
    }
  }

  async delete() {
    const params = this.resourceData.getResourceDetailParams(this.resource);
    const endpoint = params.namespace
      ? `api/v1/rbac/namespaces/${params.namespace || ''}/roles/${params.name}`
      : `api/v1/rbac/clusterroles/${params.name}`;
    try {
      const confirmed = await this.confirmBox.delete({
        kind: params.kind,
        name: params.name,
      });
      if (confirmed) {
        await this.http
          .delete(endpoint, {
            responseType: 'text',
          })
          .toPromise();
        this.updated.next(null);
        this.auiMessageService.success({
          content:
            this.translate.get('delete') + this.translate.get('succeeded'),
        });
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }
}
