import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { RoleDetail } from '~api/backendapi';

import { BaseDetailPageComponent } from '~app/abstract';

@Component({
  selector: 'alk-role-detail-page',
  templateUrl: './role-detail-page.component.html',
  styleUrls: ['./role-detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoleDetailPageComponent extends BaseDetailPageComponent<RoleDetail>
  implements OnInit {
  tabs: string[];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<RoleDetail> {
    const endpoint = params.namespace
      ? `api/v1/rbac/namespaces/${params.namespace || ''}/roles/${params.name}`
      : `api/v1/rbac/clusterroles/${params.name}`;
    return this.http.get<RoleDetail>(endpoint);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  get addRuleParams() {
    const params = { name: '', namespace: '', data: '' };
    this.detail$.subscribe((detail: RoleDetail) => {
      params.name = detail.objectMeta.name;
      params.namespace = detail.objectMeta.namespace || '';
      params.data = JSON.stringify(detail.data);
    });
    return params;
  }

  constructor(private http: HttpClient, injector: Injector) {
    super(injector);
  }
}
