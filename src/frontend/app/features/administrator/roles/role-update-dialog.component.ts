import { NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { get } from 'lodash';
import { RoleDetail } from '~api/backendapi';

@Component({
  selector: 'alk-role-update-dialog',
  templateUrl: './role-update-dialog.component.html',
  styleUrls: ['./role-update-dialog.component.scss'],
})
export class RoleUpdateDialogComponent implements OnInit {
  @Input()
  detail: RoleDetail;
  @Output()
  confirmed = new EventEmitter<boolean | RoleDetail>();
  data = {
    displayName: '',
    description: '',
  };

  ngOnInit() {
    this.data.displayName =
      get(this.detail, 'data.metadata.annotations.displayName') || '';
    this.data.description =
      get(this.detail, 'data.metadata.annotations.description') || '';
  }

  confirm() {
    const endpoint = this.detail.objectMeta.namespace
      ? `api/v1/rbac/namespaces/${this.detail.objectMeta.namespace}/roles/${
          this.detail.objectMeta.name
        }`
      : `api/v1/rbac/clusterroles/${this.detail.objectMeta.name}`;
    this.detail.data.metadata.annotations = {};
    this.detail.data.metadata.annotations.displayName = this.data.displayName;
    this.detail.data.metadata.annotations.description = this.data.description;
    this.http
      .put<RoleDetail>(endpoint, this.detail.data)
      .toPromise()
      .then(() => {
        this.confirmed.next(this.detail);
      })
      .catch(error => {
        this.auiNotificationService.error({ content: error.error || error });
      });
  }

  cancel() {
    this.confirmed.next(false);
  }

  constructor(
    private http: HttpClient,
    private auiNotificationService: NotificationService,
  ) {}
}
