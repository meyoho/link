import { MessageService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { assign, cloneDeep, intersection, isEqual, orderBy } from 'lodash';
import { map } from 'rxjs/operators';
import { RoleDetail } from '~api/backendapi';

import { TranslateService } from '../../../translate';

@Component({
  selector: 'alk-role-rule-add',
  templateUrl: './role-rule-add.component.html',
  styleUrls: ['./role-rule-add.component.scss'],
})
export class RoleRuleAddComponent implements OnInit {
  name: string;
  namespace: string;
  data: any;
  rule: any; // isUpdate
  ruleIndex: number;
  newRule: any = {
    verbs: [],
    resources: [],
    apiGroups: ['*'],
  };
  apiGroupsStr = '*';
  actionsType: 'readonly' | 'all' | 'custom' = 'readonly';
  actionsReadonly = {
    get: true,
    list: true,
    proxy: true,
    watch: true,
    create: false,
    update: false,
    patch: false,
    delete: false,
    deletecollection: false,
    use: false,
    bind: false,
    impersonate: false,
  };
  actionsAll = {
    get: true,
    list: true,
    proxy: true,
    watch: true,
    create: true,
    update: true,
    patch: true,
    delete: true,
    deletecollection: true,
    use: true,
    bind: true,
    impersonate: true,
  };
  curActions: any = cloneDeep(this.actionsReadonly);
  resourceType: 'recommended' | 'all' | 'custom' = 'recommended';
  resourceDefault = {};
  administratorResources = [
    'roles',
    'rolebindings',
    'secrets',
    'clusterroles',
    'clusterrolebindings',
  ];
  administratorResourcesMap: any = {};
  normalResourcesMap: any = {};
  recommendedResources: any = {
    normal: {},
    admin: {},
  };
  allResources: any = {
    normal: {},
    admin: {},
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
  ) {}

  async ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.name = params.name;
      this.namespace = params.namespace;
      this.data = JSON.parse(params.data);
      // if update, rule & ruleIndex is required
      this.ruleIndex = params.ruleIndex;
    });
    // normal resources
    await this.http
      .get('api/v1/resources')
      .pipe(
        map((res: any) =>
          res.resources
            .filter(
              (resource: any) =>
                !this.administratorResources.includes(resource.Resource),
            )
            .forEach((resource: any) => {
              this.normalResourcesMap[resource.Resource] = true;
              this.recommendedResources.normal[resource.Resource] = true;
              this.allResources.normal[resource.Resource] = true;
            }),
        ),
      )
      .toPromise();
    // administrator resources
    orderBy(
      this.namespace
        ? this.administratorResources.splice(0, 3)
        : this.administratorResources.splice(2, 3),
    ).forEach(resource => {
      this.administratorResourcesMap[resource] = false;
      this.recommendedResources.admin[resource] = false;
      this.allResources.admin[resource] = true;
    });
    this.apiGroupsChange('*');
    // update
    if (this.ruleIndex) {
      this.updateView();
    }
    this.cdr.detectChanges();
  }

  updateView() {
    this.rule = this.data.rules[this.ruleIndex];
    // load actions
    const actMap: any = {};
    const actions: any[] = intersection(
      Object.keys(this.actionsAll),
      this.rule.verbs,
    );
    Object.keys(this.actionsAll).forEach(act => {
      if (actions.includes(act)) {
        actMap[act] = true;
      } else {
        actMap[act] = false;
      }
    });
    this.curActions = actMap;
    if (this.rule.verbs.includes('*')) {
      this.curActions = cloneDeep(this.actionsAll);
    }
    // load resources
    const adminResources = intersection(
      this.rule.resources || [],
      Object.keys(this.administratorResourcesMap),
    );
    Object.keys(this.administratorResourcesMap).forEach(resource => {
      if (adminResources.includes(resource)) {
        this.administratorResourcesMap[resource] = true;
      } else {
        this.administratorResourcesMap[resource] = false;
      }
    });
    const normalResources = intersection(
      this.rule.resources || [],
      Object.keys(this.normalResourcesMap),
    );
    Object.keys(this.normalResourcesMap).forEach(resource => {
      if (normalResources.includes(resource)) {
        this.normalResourcesMap[resource] = true;
      } else {
        this.normalResourcesMap[resource] = false;
      }
    });
    if ((this.rule.resources || []).includes('*')) {
      this.resourceTypeChange('all');
    }
    // update radio status
    this.actionsChange();
    this.resourcesChange();
    // load apiGroups
    this.newRule.apiGroups = cloneDeep(this.rule.apiGroups) || [];
    this.apiGroupsStr = (this.rule.apiGroups || []).join(',');
  }

  actionTypeChange(type: string) {
    if (type === 'readonly') {
      this.curActions = cloneDeep(this.actionsReadonly);
    } else if (type === 'all') {
      this.curActions = cloneDeep(this.actionsAll);
    }
  }

  actionsChange() {
    const isReadonly = isEqual(this.actionsReadonly, this.curActions);
    const isAll = isEqual(this.actionsAll, this.curActions);
    if (isReadonly) {
      this.actionsType = 'readonly';
    } else if (isAll) {
      this.actionsType = 'all';
    } else {
      this.actionsType = 'custom';
    }
  }

  resourceTypeChange(type: string) {
    if (type === 'recommended') {
      this.normalResourcesMap = cloneDeep(this.recommendedResources.normal);
      this.administratorResourcesMap = cloneDeep(
        this.recommendedResources.admin,
      );
    } else if (type === 'all') {
      this.normalResourcesMap = cloneDeep(this.allResources.normal);
      this.administratorResourcesMap = cloneDeep(this.allResources.admin);
    }
  }

  resourcesChange() {
    const curResources = assign(
      {},
      this.normalResourcesMap,
      this.administratorResourcesMap,
    );
    const isRec = isEqual(
      curResources,
      assign(
        {},
        this.recommendedResources.normal,
        this.recommendedResources.admin,
      ),
    );
    const isAll = isEqual(
      curResources,
      assign({}, this.allResources.normal, this.allResources.admin),
    );
    if (isRec) {
      this.resourceType = 'recommended';
    } else if (isAll) {
      this.resourceType = 'all';
    } else {
      this.resourceType = 'custom';
    }
  }

  apiGroupsChange(str: string) {
    this.newRule.apiGroups = str
      .replace(/\s+/g, '')
      .split(',')
      .filter(item => !!item);
  }

  async submit() {
    const endpoint = this.namespace
      ? `api/v1/rbac/namespaces/${this.namespace}/roles/${this.name}`
      : `api/v1/rbac/clusterroles/${this.name}`;
    this.data = await this.http
      .get<RoleDetail>(endpoint)
      .toPromise()
      .then(({ data }) => data);
    const curResources = assign(
      {},
      this.normalResourcesMap,
      this.administratorResourcesMap,
    );
    this.newRule.verbs = Object.keys(this.curActions).filter(
      act => this.curActions[act],
    );
    this.newRule.resources = Object.keys(curResources).filter(
      resource => curResources[resource],
    );
    if (this.resourceType === 'all') {
      this.newRule.resources = ['*'];
    }
    if (this.actionsType === 'all') {
      this.newRule.verbs = ['*'];
    }

    if (!this.rule || (this.rule && !this.data.rules[this.ruleIndex])) {
      this.data.rules.push(this.newRule);
    } else {
      this.data.rules[this.ruleIndex] = cloneDeep(this.newRule);
    }

    this.http
      .put<RoleDetail>(endpoint, this.data)
      .toPromise()
      .then(() => {
        this.router.navigate([
          '/administrator/roles/detail',
          { name: this.name, namespace: this.namespace },
        ]);
        this.auiMessageService.success({
          content:
            this.translate.get('update') + this.translate.get('succeeded'),
        });
      })
      .catch(error => {
        this.auiNotificationService.error({ content: error.error || error });
      });
  }

  cancel() {
    this.router.navigate([
      '/administrator/roles/detail',
      { name: this.name, namespace: this.namespace },
    ]);
  }

  keys(object: object): string[] {
    return Object.keys(object);
  }
}
