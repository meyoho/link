import {
  DialogRef,
  DialogService,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Input,
  TemplateRef,
} from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash';
import { RoleDetail } from '~api/backendapi';

import { ConfirmBoxService } from '../../../services';
import { TranslateService } from '../../../translate';

@Component({
  selector: 'alk-role-detail-basic-info',
  templateUrl: './role-detail-basic-info.component.html',
})
export class RoleDetailBasicInfoComponent {
  @Input()
  detail: RoleDetail;

  updateDialogRef: DialogRef<any>;

  async deleteRole() {
    const endpoint = this.detail.objectMeta.namespace
      ? `api/v1/rbac/namespaces/${this.detail.objectMeta.namespace}/roles/${
          this.detail.objectMeta.name
        }`
      : `api/v1/rbac/clusterroles/${this.detail.objectMeta.name}`;
    try {
      const confirmed = await this.confirmBox.delete({
        kind: this.detail.typeMeta.kind,
        name: this.detail.objectMeta.name,
      });
      if (confirmed) {
        await this.http
          .delete(endpoint, {
            responseType: 'text',
          })
          .toPromise();
        this.auiMessageService.success({
          content:
            this.translate.get('delete') + this.translate.get('succeeded'),
        });
        this.router.navigate(['/administrator/roles']);
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }

  upadteRole(templateRef: TemplateRef<any>) {
    this.updateDialogRef = this.dialog.open(templateRef, {
      data: cloneDeep(this.detail),
    });
  }

  onUpdate(res: RoleDetail) {
    if (res) {
      this.updateDialogRef.close(res);
      this.auiMessageService.success({
        content: this.translate.get('update') + this.translate.get('succeeded'),
      });
      this.detail = cloneDeep(res);
      this.cdr.detectChanges();
    } else {
      this.updateDialogRef.close(res);
    }
  }

  constructor(
    private dialog: DialogService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private http: HttpClient,
    private confirmBox: ConfirmBoxService,
    private router: Router,
    private cdr: ChangeDetectorRef,
  ) {}
}
