import { MessageService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash';
import { ColumnDef } from '~app/abstract';

import { ConfirmBoxService } from '../../../services';
import { TranslateService } from '../../../translate';

@Component({
  selector: 'alk-role-permissions-list',
  templateUrl: './role-permissions-list.component.html',
})
export class RolePermissionsListComponent {
  constructor(
    private router: Router,
    private confirmBox: ConfirmBoxService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private http: HttpClient,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  @Input()
  detail: any;

  columnDefs: ColumnDef[] = [
    {
      name: 'verbs',
      label: 'action',
    },
    {
      name: 'resources',
      label: 'resources',
    },
    {
      name: 'apiGroups',
      label: 'apigroups',
    },
    {
      name: 'action',
      label: '',
    },
  ];

  columns = this.columnDefs.map(columnDef => columnDef.name);

  get showZeroState() {
    return (
      !this.detail ||
      !this.detail.data.rules ||
      this.detail.data.rules.length === 0
    );
  }

  updateRule(rule: any) {
    const ruleIndex = this.detail.data.rules.indexOf(rule);
    this.router.navigate([
      '/administrator/roles/rule',
      {
        name: this.detail.objectMeta.name,
        namespace: this.detail.objectMeta.namespace || '',
        data: JSON.stringify(this.detail.data),
        rule,
        ruleIndex,
      },
    ]);
  }

  async deleteRule(rule: any) {
    const ruleIndex = this.detail.data.rules.indexOf(rule);
    const endpoint = this.detail.objectMeta.namespace
      ? `api/v1/rbac/namespaces/${this.detail.objectMeta.namespace}/roles/${
          this.detail.objectMeta.name
        }`
      : `api/v1/rbac/clusterroles/${this.detail.objectMeta.name}`;
    try {
      const confirmed = await this.confirmBox.confirm({
        title: this.translate.get('administrator.delete_rule_confirm'),
        content: `
          ${this.translate.get('actions')}: ${rule.verbs.join(', ')}
          ${this.translate.get('resources')}: ${rule.resources.join(', ')}`,
      });
      if (confirmed) {
        const newRules = cloneDeep(this.detail.data.rules);
        newRules.splice(ruleIndex, 1);
        this.detail.data.rules = newRules;
        await this.http
          .put(endpoint, this.detail.data, {
            responseType: 'text',
          })
          .toPromise();
        this.auiMessageService.success({
          content:
            this.translate.get('delete') + this.translate.get('succeeded'),
        });
        this.cdr.detectChanges();
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }
}
