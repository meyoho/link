import { MessageService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NamespaceList } from '~api/backendapi';

import { TranslateService } from '../../../translate';

@Component({
  selector: 'alk-role-create',
  templateUrl: './role-create-page.component.html',
  styleUrls: ['./role-create-page.component.scss'],
})
export class RoleCreatePageComponent implements OnInit {
  namespaces$: Observable<string[]>;
  data: any = {
    name: '',
    displayName: '',
    description: '',
    namespace: '',
  };
  nameRequired: boolean;
  namespaceRequired: boolean;
  roleType: 'namespace' | 'cluster' = 'namespace';

  constructor(
    private http: HttpClient,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.namespaces$ = this.http
      .get<NamespaceList>('api/v1/permission_namespaces')
      .pipe(
        map(({ namespaces }) =>
          namespaces.map(namespaceMeta => namespaceMeta.objectMeta.name),
        ),
      );
  }

  onNamespaceChange(namespace: string) {
    if (namespace) {
      this.namespaceRequired = false;
    } else {
      this.namespaceRequired = true;
    }
  }

  onNameChange(name: string) {
    if (name) {
      this.nameRequired = false;
    } else {
      this.nameRequired = true;
    }
  }

  submit() {
    const data = cloneDeep(this.data);
    this.nameRequired = !data.name;
    this.namespaceRequired = this.roleType === 'namespace' && !data.namespace;
    if (this.nameRequired || this.namespaceRequired) {
      return;
    }

    const endpoint =
      this.roleType === 'namespace'
        ? `api/v1/rbac/namespaces/${data.namespace}/roles/`
        : `api/v1/rbac/clusterroles/`;
    const body = {
      apiVersion: 'rbac.authorization.k8s.io/v1beta1',
      kind: data.namespace ? 'Role' : 'ClusterRole',
      metadata: {
        name: data.name,
        namespace: data.namespace,
        annotations: {
          displayName: data.displayName,
          description: data.description,
        },
      },
      rules: [{ apiGroups: ['*'], resources: ['*'], verbs: ['*'] }],
    };
    if (!data.namespace) {
      delete body.metadata.namespace;
    }
    this.http
      .post(endpoint, body)
      .toPromise()
      .then(() => {
        this.auiMessageService.success({
          content:
            this.translate.get('create') + this.translate.get('succeeded'),
        });
        this.router.navigate([
          '/administrator/roles/detail',
          {
            name: data.name,
            namespace: this.roleType === 'namespace' ? data.namespace : '',
          },
        ]);
      })
      .catch(error => {
        this.auiNotificationService.error({ content: error.error || error });
      });
  }

  cancel() {
    this.router.navigate(['/administrator/roles']);
  }
}
