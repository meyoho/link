import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { JobDetail } from '~api/backendapi';
import { PodSpec } from '~api/raw-k8s';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-cron-job-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronJobDetailPageComponent
  extends BaseDetailPageComponent<JobDetail>
  implements OnInit {
  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<JobDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'cronjob' });
  }

  async onPodSpecUpdate(podSpec: PodSpec) {
    const detail = await this.detail$.pipe(first()).toPromise();
    const detailParams = this.resourceData.getResourceDetailParams(detail);

    try {
      await this.resourceData.updateDetailAtPath(
        detailParams,
        'spec.template.spec',
        podSpec,
      );
      this.updated$.next(null);
    } catch (error) {}
  }
}
