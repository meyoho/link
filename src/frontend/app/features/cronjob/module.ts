import { NgModule } from '@angular/core';

import { FeaturesSharedWorkloadModule } from '../../features-shared/workload-module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { CronJobDetailPageComponent } from './detail-page.component';
import { CronJobListPageComponent } from './list-page.component';

import { CronJobActionMenuComponent } from './action-menu.component';
import { CronJobDetailBasicInfoComponent } from './detail-basic-info.component';
import i18n from './i18n';
import { JobListComponent } from './job-list/component';
import { CronJobRoutingModule } from './routing.module';

@NgModule({
  imports: [CronJobRoutingModule, SharedModule, FeaturesSharedWorkloadModule],
  declarations: [
    JobListComponent,
    CronJobListPageComponent,
    CronJobDetailPageComponent,
    CronJobActionMenuComponent,
    CronJobDetailBasicInfoComponent,
  ],
  providers: [],
})
export class CronJobModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations('cronjob', i18n);
  }
}
