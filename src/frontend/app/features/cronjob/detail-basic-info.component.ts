import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CronJobDetail } from '~api/backendapi';

import { AppConfigService, ResourceDataService } from '../../services';

@Component({
  selector: 'alk-cron-job-detail-basic-info',
  templateUrl: './detail-basic-info.component.html',
  styleUrls: ['./detail-basic-info.component.scss'],
})
export class CronJobDetailBasicInfoComponent {
  @Input()
  detail: CronJobDetail;

  @Output()
  updated = new EventEmitter();

  get podActiveJobEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'cronjob',
        }) + '/job'
      );
    } else {
      return '';
    }
  }

  get podCompletedJobEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'cronjob',
        }) + '/completedjob'
      );
    } else {
      return '';
    }
  }

  constructor(
    private resourceData: ResourceDataService,
    public appConfig: AppConfigService,
  ) {}
}
