import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { CronJob, CronJobList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'schedule',
    label: 'cronjob.schedule',
  },
  {
    name: 'active',
    label: 'running',
  },
  {
    name: 'lastSchedule',
    label: 'cronjob.last_schedule',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-cron-job-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronJobListPageComponent
  extends BaseResourceListPageComponent<CronJobList, CronJob>
  implements OnInit {
  map(value: CronJobList): CronJob[] {
    return value.items;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<CronJobList> {
    return this.http.get<CronJobList>(
      `api/v1/cronjobs/${params.namespace || ''}`,
      {
        params: getQuery(
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
