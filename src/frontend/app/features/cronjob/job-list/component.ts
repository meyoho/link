import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Job, JobList } from '~api/backendapi';
import {
  BaseResourceListComponent,
  ColumnDef,
  FetchParams,
  TableState,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'pods',
    label: 'status',
  },
  {
    name: 'containerImages',
    label: 'images',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-job-list',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobListComponent extends BaseResourceListComponent<JobList, Job>
  implements OnInit {
  @Input()
  endpoint: string;

  tableState$ = new BehaviorSubject<TableState>({});
  initialized = false;

  map(value: JobList): Job[] {
    return value.jobs;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  getTableState$(): Observable<TableState> {
    return this.tableState$;
  }

  fetchResources(params: FetchParams): Observable<JobList | Error> {
    return this.http.get<JobList>(this.endpoint, {
      params: getQuery(
        filterBy('name', params.search),
        sortBy(params.sort.active, params.sort.direction === 'desc'),
        pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
      ),
    });
  }

  setTableState(newState: TableState): Promise<boolean> {
    this.tableState$.next(newState);
    return Promise.resolve(true);
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
