import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  SimpleResourceCreatePageComponent,
  SimpleResourceUpdatePageComponent,
} from '~app/features-shared/common';

import { CronJobDetailPageComponent } from './detail-page.component';
import { CronJobListPageComponent } from './list-page.component';

// TODO: move somewhere else, like a universal sample provider or laoder?
const sample = `apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: example-cron-countdown
  namespace: default
spec:
  # Go to https://crontab.guru/#*/1_*/1_*_*_*
  schedule: "0 */1 * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: counter
            image: busybox
            command:
            - "/bin/sh"
            - "-c"
            - "for i in \`seq 1 100\`; do echo $i; sleep 1 ; done"
          restartPolicy: OnFailure
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: CronJobListPageComponent,
      },
      {
        path: 'detail',
        component: CronJobDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
      {
        path: 'create',
        component: SimpleResourceCreatePageComponent,
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class CronJobRoutingModule {}
