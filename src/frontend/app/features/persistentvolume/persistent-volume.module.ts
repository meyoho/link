import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import i18n from './i18n';
import { PersistentVolumeActionMenuComponent } from './persistent-volume-action-menu.component';
import { PersistentVolumeDetailBasicInfoComponent } from './persistent-volume-detail-basic-info.component';
import { PersistentVolumeDetailPageComponent } from './persistent-volume-detail-page.component';
import { PersistentVolumeListPageComponent } from './persistent-volume-list-page.component';
import { PersistentVolumeRoutingModule } from './persistent-volume-routing.module';

const MODULE_PREFIX = 'persistentvolume';

@NgModule({
  imports: [
    SharedModule,
    FeatureSharedCommonModule,
    PersistentVolumeRoutingModule,
  ],
  declarations: [
    PersistentVolumeListPageComponent,
    PersistentVolumeDetailPageComponent,
    PersistentVolumeActionMenuComponent,
    PersistentVolumeDetailBasicInfoComponent,
  ],
})
export class PersistentVolumeModule {
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
