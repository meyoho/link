import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { PersistentVolume } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-persistent-volume-action-menu',
  templateUrl: './persistent-volume-action-menu.component.html',
  styleUrls: ['./persistent-volume-action-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersistentVolumeActionMenuComponent extends BaseActionMenuComponent<
  PersistentVolume
> {
  @Input()
  actions = ['update', 'update_labels', 'update_annotations', 'delete'];
  constructor(injector: Injector) {
    super(injector);
  }
}
