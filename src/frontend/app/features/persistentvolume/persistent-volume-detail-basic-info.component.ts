import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PersistentVolumeDetail, Resource } from '~api/backendapi';

@Component({
  selector: 'alk-persistent-volume-detail-basic-info',
  templateUrl: './persistent-volume-detail-basic-info.component.html',
  styleUrls: ['./persistent-volume-detail-basic-info.component.scss'],
})
export class PersistentVolumeDetailBasicInfoComponent {
  @Input()
  detail: PersistentVolumeDetail;
  @Output()
  updated = new EventEmitter();

  getStorageClassResource(storageClassName: string) {
    if (storageClassName) {
      const storageClass: Resource = {
        typeMeta: {
          apiVersion: 'storage.k8s.io/v1',
          kind: 'StorageClass',
        },
        objectMeta: {
          name: storageClassName,
        },
      };
      return storageClass;
    } else {
      return undefined;
    }
  }

  getPersistentVolumeClaimResource(claimName: string) {
    if (claimName) {
      const claim: Resource = {
        typeMeta: {
          apiVersion: 'v1',
          kind: 'PersistentVolumeClaim',
        },
        objectMeta: {
          name: claimName.split('/')[1],
          namespace: claimName.split('/')[0],
        },
      };
      return claim;
    } else {
      return undefined;
    }
  }
}
