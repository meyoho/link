import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import {
  PersistentVolume,
  PersistentVolumeList,
  Resource,
} from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'status',
    label: 'status',
  },
  {
    name: 'claim',
    label: 'related_pvc',
  },
  {
    name: 'storageClass',
    label: 'storageclasses',
  },
  {
    name: 'capacity',
    label: 'capacity',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-persistent-volume-list-page',
  templateUrl: './persistent-volume-list-page.component.html',
  styleUrls: ['./persistent-volume-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersistentVolumeListPageComponent
  extends BaseResourceListPageComponent<PersistentVolumeList, PersistentVolume>
  implements OnInit {
  map(value: PersistentVolumeList): PersistentVolume[] {
    return value.items;
  }
  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }
  fetchResources(
    params: FetchParams,
  ): Observable<PersistentVolumeList | Error> {
    return this.http.get<PersistentVolumeList>(
      `api/v1/persistentvolumes/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  getStorageClassResource(storageClassName: string) {
    if (storageClassName) {
      const storageClass: Resource = {
        typeMeta: {
          apiVersion: 'storage.k8s.io/v1',
          kind: 'StorageClass',
        },
        objectMeta: {
          name: storageClassName,
        },
      };
      return storageClass;
    } else {
      return undefined;
    }
  }

  getPersistentVolumeClaimResource(claimName: string) {
    if (claimName) {
      const claim: Resource = {
        typeMeta: {
          apiVersion: 'v1',
          kind: 'PersistentVolumeClaim',
        },
        objectMeta: {
          name: claimName.split('/')[1],
          namespace: claimName.split('/')[0],
        },
      };
      return claim;
    } else {
      return undefined;
    }
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
