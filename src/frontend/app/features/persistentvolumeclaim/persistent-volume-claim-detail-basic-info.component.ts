import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PersistentVolumeClaimDetail, Resource } from '~api/backendapi';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-persistent-volume-claim-detail-basic-info',
  templateUrl: './persistent-volume-claim-detail-basic-info.component.html',
  styleUrls: ['./persistent-volume-claim-detail-basic-info.component.scss'],
})
export class PersistentVolumeClaimDetailBasicInfoComponent {
  @Input()
  detail: PersistentVolumeClaimDetail;
  @Output()
  updated = new EventEmitter();

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'PersistentVolumeClaim',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }

  getStorageClassResource(storageClassName: string) {
    if (storageClassName) {
      const storageClass: Resource = {
        typeMeta: {
          apiVersion: 'storage.k8s.io/v1',
          kind: 'StorageClass',
        },
        objectMeta: {
          name: storageClassName,
        },
      };
      return storageClass;
    } else {
      return undefined;
    }
  }

  getPersistentVolumeResource(volumeName: string) {
    if (volumeName) {
      const volume: Resource = {
        typeMeta: {
          apiVersion: 'v1',
          kind: 'PersistentVolume',
        },
        objectMeta: {
          name: volumeName,
        },
      };
      return volume;
    } else {
      return undefined;
    }
  }

  constructor(private resourceData: ResourceDataService) {}
}
