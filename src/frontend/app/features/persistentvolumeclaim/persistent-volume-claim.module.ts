import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';
import { CustomClassComponent } from '~app/features/persistentvolumeclaim/form/add-class-dialog/component';
import { PersistentVolumeClaimFormComponent } from '~app/features/persistentvolumeclaim/form/component';
import { PersistentVolumeClaimMutatePageComponent } from '~app/features/persistentvolumeclaim/mutate-page/mutate-page.component';

import { PodListModule } from '../../features-shared/pod-list/module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import i18n from './i18n';
import { PersistentVolumeClaimActionMenuComponent } from './persistent-volume-claim-action-menu.component';
import { PersistentVolumeClaimDetailBasicInfoComponent } from './persistent-volume-claim-detail-basic-info.component';
import { PersistentVolumeClaimDetailPageComponent } from './persistent-volume-claim-detail-page.component';
import { PersistentVolumeClaimListPageComponent } from './persistent-volume-claim-list-page.component';
import { PersistentVolumeClaimRoutingModule } from './persistent-volume-claim-routing.module';

const MODULE_PREFIX = 'persistentvolumeclaim';

@NgModule({
  imports: [
    SharedModule,
    PodListModule,
    FeatureSharedCommonModule,
    PersistentVolumeClaimRoutingModule,
  ],
  declarations: [
    PersistentVolumeClaimListPageComponent,
    PersistentVolumeClaimDetailPageComponent,
    PersistentVolumeClaimActionMenuComponent,
    PersistentVolumeClaimDetailBasicInfoComponent,
    PersistentVolumeClaimMutatePageComponent,
    PersistentVolumeClaimFormComponent,
    CustomClassComponent,
  ],
  entryComponents: [PersistentVolumeClaimFormComponent, CustomClassComponent],
})
export class PersistentVolumeClaimModule {
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
