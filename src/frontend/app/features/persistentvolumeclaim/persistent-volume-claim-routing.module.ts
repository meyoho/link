import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PersistentVolumeClaimMutatePageComponent } from '~app/features/persistentvolumeclaim/mutate-page/mutate-page.component';
import { ResourceMutatePageDeactivateGuard } from '~app/services';

import { PersistentVolumeClaimDetailPageComponent } from './persistent-volume-claim-detail-page.component';
import { PersistentVolumeClaimListPageComponent } from './persistent-volume-claim-list-page.component';

// TODO: move somewhere else, like a universal sample provider?
const sample = `apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
  namespace: default
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: PersistentVolumeClaimListPageComponent,
      },
      {
        path: 'detail',
        component: PersistentVolumeClaimDetailPageComponent,
      },
      {
        path: 'update',
        component: PersistentVolumeClaimMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
      },
      {
        path: 'create',
        component: PersistentVolumeClaimMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class PersistentVolumeClaimRoutingModule {}
