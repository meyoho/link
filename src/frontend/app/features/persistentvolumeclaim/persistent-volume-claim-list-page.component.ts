import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import {
  PersistentVolumeClaim,
  PersistentVolumeClaimList,
  Resource,
} from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'status',
    label: 'status',
  },
  {
    name: 'volume',
    label: 'related_pv',
  },
  {
    name: 'storageClass',
    label: 'storageclasses',
  },
  {
    name: 'capacity',
    label: 'capacity',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-persistent-volume-claim-list-page',
  templateUrl: './persistent-volume-claim-list-page.component.html',
  styleUrls: ['./persistent-volume-claim-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersistentVolumeClaimListPageComponent
  extends BaseResourceListPageComponent<
    PersistentVolumeClaimList,
    PersistentVolumeClaim
  >
  implements OnInit {
  map(value: PersistentVolumeClaimList): PersistentVolumeClaim[] {
    return value.items;
  }
  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }
  fetchResources(
    params: FetchParams,
  ): Observable<PersistentVolumeClaimList | Error> {
    return this.http.get<PersistentVolumeClaimList>(
      `api/v1/persistentvolumeclaims/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  getStorageClassResource(storageClassName: string) {
    if (storageClassName) {
      const storageClass: Resource = {
        typeMeta: {
          apiVersion: 'storage.k8s.io/v1',
          kind: 'StorageClass',
        },
        objectMeta: {
          name: storageClassName,
        },
      };
      return storageClass;
    } else {
      return undefined;
    }
  }

  getPersistentVolumeResource(volumeName: string) {
    if (volumeName) {
      const volume: Resource = {
        typeMeta: {
          apiVersion: 'v1',
          kind: 'PersistentVolume',
        },
        objectMeta: {
          name: volumeName,
        },
      };
      return volume;
    } else {
      return undefined;
    }
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
