export const en = {
  create_persistentvolumeclaim: 'Create PVC',
  ReadWriteMany: 'ReadWriteMany',
  ReadWriteOnce: 'ReadWriteOnce',
  ReadOnlyMany: 'ReadOnlyMany',
  accessMode: 'Access mode',
  volumeMode: 'Volume mode',
  advanced: 'PVC-Advanced',
  basic: 'PVC-Basic',
  add_custom_class: 'Add custom storageClass',
};

export const zh = {
  create_persistentvolumeclaim: '创建持久卷声明',
  ReadWriteMany: '共享',
  ReadWriteOnce: '读写',
  ReadOnlyMany: '只读',
  accessMode: '访问模式',
  volumeMode: '存储卷模式',
  advanced: '持久卷声明-高级',
  basic: '持久卷声明-基本',
  add_custom_class: '添加自定义存储类',
};

export default {
  en,
  zh,
};
