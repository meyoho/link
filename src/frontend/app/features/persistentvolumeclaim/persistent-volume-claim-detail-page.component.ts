import { Component, Injector, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PersistentVolumeClaimDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-persistent-volume-claim-detail-page',
  templateUrl: './persistent-volume-claim-detail-page.component.html',
  styleUrls: ['./persistent-volume-claim-detail-page.component.scss'],
})
export class PersistentVolumeClaimDetailPageComponent
  extends BaseDetailPageComponent<PersistentVolumeClaimDetail>
  implements OnInit {
  podNames$: Observable<string[]>;
  tabs = ['basic_info', 'yaml', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<PersistentVolumeClaimDetail> {
    return this.resourceData.getDetail({
      ...params,
      kind: 'PersistentVolumeClaim',
    });
  }

  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
