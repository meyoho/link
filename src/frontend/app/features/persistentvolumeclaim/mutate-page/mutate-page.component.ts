import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';

import { PersistentVolumeClaim } from '~api/backendapi';
import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  selector: 'alk-configmap-mutate-page',
  templateUrl: './mutate-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersistentVolumeClaimMutatePageComponent extends BaseResourceMutatePageComponent<
  PersistentVolumeClaim
> {
  get kind() {
    return 'PersistentVolumeClaim';
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
