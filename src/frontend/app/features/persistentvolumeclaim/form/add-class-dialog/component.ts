import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomClassComponent {
  className: string;

  @Output()
  close = new EventEmitter();

  constructor() {}

  confirm() {
    this.close.next(this.className);
  }
}
