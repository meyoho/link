import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import {
  PersistentVolumeClaim,
  PersistentVolumeClaimTypeMeta,
} from '~api/raw-k8s';

import { DialogRef, DialogService } from '@alauda/ui';
import { cloneDeep } from 'lodash';
import { map } from 'rxjs/operators';
import { StorageClassList } from '~api/backendapi';
import { BaseKubernetesResourceFormComponent } from '~app/abstract';
import { CustomClassComponent } from '~app/features/persistentvolumeclaim/form/add-class-dialog/component';

@Component({
  selector: 'alk-pvc-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersistentVolumeClaimFormComponent
  extends BaseKubernetesResourceFormComponent<PersistentVolumeClaim>
  implements OnInit {
  kind = 'PersistentVolumeClaim';
  accessModes = ['ReadWriteOnce', 'ReadOnlyMany', 'ReadWriteMany'];
  volumeModes = ['Filesystem', 'Block'];
  dialogRef: DialogRef<CustomClassComponent>;
  storageClassNames: string[] = [];

  getDefaultFormModel() {
    return {
      ...PersistentVolumeClaimTypeMeta,
      accessModes: ['ReadWriteOnce'],
      volumeMode: 'Filesystem',
      resources: { requests: { storage: '8Gi' } },
      selector: { matchLabels: {} },
    };
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(/^[a-z0-9^-]*[a-z0-9-]*[a-z0-9]$/),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    const specForm = this.fb.group({
      accessModes: this.fb.control([]),
      volumeMode: this.fb.control(''),
      storageClassName: this.fb.control(''),
      resources: this.fb.control({}),
      selector: this.fb.control({}),
    });
    return this.fb.group({
      metadata: metadataForm,
      spec: specForm,
    });
  }

  async ngOnInit() {
    super.ngOnInit();
    this.storageClassNames = await this.http
      .get<StorageClassList>('api/v1/storageclasses')
      .pipe(map(({ items }) => items.map(item => item.objectMeta.name)))
      .toPromise();
    this.cdr.markForCheck();
  }

  adaptFormModel(form: { [key: string]: any }) {
    const spec = cloneDeep(form.spec);
    if (spec.selector) {
      if (!Object.keys(spec.selector).length) {
        delete spec.selector;
      } else {
        spec.selector = { matchLabels: spec.selector };
      }
    }
    spec.accessModes = [spec.accessModes];
    spec.resources = { requests: { storage: spec.resources } };
    return { ...form, spec };
  }

  addClass() {
    this.dialogRef = this.dialogService.open(CustomClassComponent);
    this.dialogRef.componentInstance.close.subscribe((type: string) => {
      if (!this.storageClassNames.includes(type)) {
        this.storageClassNames.push(type);
        this.form.get('spec.storageClassName').setValue(type);
      }
      this.dialogRef.close();
    });
  }

  adaptResourceModel(resource: any) {
    if (!resource || !resource.spec) {
      return super.adaptResourceModel(resource);
    }
    resource = cloneDeep(resource);
    const spec = resource.spec;
    if (spec.selector && spec.selector.matchLabels) {
      const labels = spec.selector.matchLabels;
      delete resource.spec.selector.matchLabels;
      Object.keys(labels).forEach(key => {
        spec.selector[key] = labels[key];
      });
    }
    if (spec.selector && !Object.keys(spec.selector).length) {
      delete spec.selector;
    }
    spec.accessModes = spec.accessModes[0];
    spec.resources = spec.resources.requests.storage;
    return { ...resource, spec };
  }

  constructor(injector: Injector, private dialogService: DialogService) {
    super(injector);
  }
}
