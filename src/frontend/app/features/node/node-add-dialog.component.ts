import { DialogRef, MessageService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AddNodeConfig } from '~api/backendapi';

import { TranslateService } from '../../translate';

@Component({
  selector: 'alk-node-add-dialog',
  templateUrl: './node-add-dialog.component.html',
  styleUrls: ['./node-add-dialog.component.scss'],
})
export class NodeAddDialogComponent implements OnInit {
  addNodeConfig: AddNodeConfig = {
    node_list: [],
    ssh_port: 22,
    ssh_username: '',
    ssh_password: '',
  };
  updating: boolean;

  constructor(
    private dialogRef: DialogRef<NodeAddDialogComponent>,
    private httpClient: HttpClient,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
  ) {}

  ngOnInit(): void {}

  set nodeList(nodeList: string) {
    this.addNodeConfig.node_list = nodeList
      .split(',')
      .map(node => node.trim())
      .filter(node => !!node);
  }

  get nodeList() {
    return this.addNodeConfig.node_list.join(', ');
  }

  async onConfirm(form: NgForm) {
    try {
      form.onSubmit(null);
      this.updating = true;
      const res = await this.httpClient
        .post(`api/v1/nodes`, this.addNodeConfig)
        .toPromise();
      this.dialogRef.close(res);
      this.auiMessageService.success({
        content: this.translate.get('node.node_add_succeeded'),
      });
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
    this.updating = false;
  }

  getFormControlByName(form: NgForm, name: string) {
    return form && form.controls[name];
  }

  isFormControlInvalid(form: NgForm, name: string) {
    const control = this.getFormControlByName(form, name);
    if (control) {
      return control.invalid && control.dirty;
    } else {
      return null;
    }
  }

  getNodeListError(form: NgForm) {
    return (
      this.getFormControlByName(form, 'node_list') &&
      this.getFormControlByName(form, 'node_list').getError(
        'alkNodeIpListValidator',
      )
    );
  }
}
