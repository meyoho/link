import { Component, Input } from '@angular/core';
import { GenericStatus } from '~api/status';

const ICONS = {
  False: GenericStatus.Error,
  True: GenericStatus.Success,
  Unknown: GenericStatus.Unknown,
};

@Component({
  selector: 'alk-node-status',
  templateUrl: './node-status.component.html',
  styles: [
    `
      :host {
        display: inline-flex;
        align-items: center;
      }
    `,
  ],
})
export class NodeStatusComponent {
  @Input()
  ready: 'False' | 'True' | 'Unknown';

  get iconStatus() {
    return ICONS[this.ready];
  }

  get statusLabel() {
    if (this.ready === 'True') {
      return 'node.status_ready';
    } else if (this.ready === 'False') {
      return 'node.status_not_ready';
    } else if (this.ready === 'Unknown') {
      return 'node.status_unknown';
    }
  }
}
