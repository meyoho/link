import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { PodListModule } from '../../features-shared/pod-list/module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { NodeAllocatedResourcesBarComponent } from './allocated-resources/bar/component';
import { NodeAllocatedResourcesPieChartComponent } from './allocated-resources/pie-chart/component';
import i18n from './i18n';
import { NodeActionMenuComponent } from './node-action-menu.component';
import { NodeAddDialogComponent } from './node-add-dialog.component';
import { NodeAllocatedPodComponent } from './node-allocated-pod.component';
import { NodeDetailBasicInfoComponent } from './node-detail-basic-info.component';
import { NodeDetailPageComponent } from './node-detail-page.component';
import { NodeIpListValidatorDirective } from './node-ip-list-validator.directive';
import { NodeListPageComponent } from './node-list-page.component';
import { NodeRoutingModule } from './node-routing.module';
import { NodeStatusComponent } from './node-status.component';
import { NodeTaintButtonComponent } from './taint/button/component';
import { NodeTaintFieldComponent } from './taint/field/component';
import { NodeTaintFormComponent } from './taint/form/component';
import { NodeTaintUpdateDialogComponent } from './taint/update-dialog/component';
const MODULE_PREFIX = 'node';

@NgModule({
  imports: [
    SharedModule,
    FeatureSharedCommonModule,
    PodListModule,
    NodeRoutingModule,
  ],
  declarations: [
    NodeListPageComponent,
    NodeActionMenuComponent,
    NodeDetailPageComponent,
    NodeDetailBasicInfoComponent,
    NodeStatusComponent,
    NodeAllocatedResourcesBarComponent,
    NodeAllocatedResourcesPieChartComponent,
    NodeAllocatedPodComponent,
    NodeAddDialogComponent,
    NodeIpListValidatorDirective,
    NodeTaintFieldComponent,
    NodeTaintButtonComponent,
    NodeTaintFormComponent,
    NodeTaintUpdateDialogComponent,
  ],
  providers: [],
  entryComponents: [NodeAddDialogComponent, NodeTaintUpdateDialogComponent],
})
export class NodeModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
