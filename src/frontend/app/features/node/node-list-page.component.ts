import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Node, NodeList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

import { NodeAddDialogComponent } from './node-add-dialog.component';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'status',
    label: 'status',
    sortable: true,
  },
  {
    name: 'node_role',
    label: 'node.node_role',
  },
  {
    name: 'node_addresses',
    label: 'node.node_addresses',
  },
  {
    name: 'metrics',
    label: 'node.allocated_resources',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-node-list-page',
  templateUrl: './node-list-page.component.html',
  styleUrls: ['./node-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeListPageComponent
  extends BaseResourceListPageComponent<NodeList, Node>
  implements OnInit {
  map(value: NodeList): Node[] {
    return value.nodes;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<NodeList> {
    return this.http.get<NodeList>('api/v1/nodes', {
      params: getQuery(
        // Currently filter by name means "search"
        filterBy('name', params.search),
        sortBy(params.sort.active, params.sort.direction === 'desc'),
        pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
      ),
    });
  }

  getCellValue(item: Node, colName: string) {
    switch (colName) {
      case 'node_role': {
        const nodeRolePrefix = 'node-role.kubernetes.io/';
        return Object.keys(item.objectMeta.labels)
          .filter(key => key.startsWith(nodeRolePrefix))
          .map(key => key.substring(nodeRolePrefix.length));
      }
      default:
        return super.getCellValue(item, colName);
    }
  }

  showAddNodeDialog() {
    this.dialog.open(NodeAddDialogComponent);
  }

  get metricsServerEnabled() {
    return this.appConfig.metricsServerEnabled;
  }

  constructor(private dialog: DialogService, injector: Injector) {
    super(injector);
  }
}
