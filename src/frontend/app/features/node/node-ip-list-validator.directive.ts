import { Directive, Input } from '@angular/core';
import { FormControl, NG_VALIDATORS, Validator } from '@angular/forms';

const IP_REGEX = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

@Directive({
  selector: '[alkNodeIpListValidator][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: NodeIpListValidatorDirective,
      multi: true,
    },
  ],
})
export class NodeIpListValidatorDirective implements Validator {
  // Existing IPs for the current cluster
  @Input()
  existingIps: string[];
  validate(c: FormControl): { [key: string]: any } {
    if (!c.value) {
      return null;
    }

    let ips: string[] = c.value.split(',');
    ips = ips.map((ip: string) => ip.trim()).filter(ip => !!ip);

    // Check malformatted ips.
    const malformattedIp = ips.find(ip => !IP_REGEX.test(ip));

    // Check redundant ips.
    let redundantIp: string = null;
    for (let i = 1; i < ips.length && !redundantIp; i++) {
      for (let j = 0; j < i; j++) {
        if (ips[i] === ips[j]) {
          redundantIp = ips[j];
          break;
        }
      }
    }

    if (malformattedIp || redundantIp) {
      return {
        alkNodeIpListValidator: {
          malformattedIp,
          redundantIp,
        },
      };
    } else {
      return null;
    }
  }
}
