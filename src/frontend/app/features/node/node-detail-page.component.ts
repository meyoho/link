import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { debounce } from 'lodash';
import { Observable } from 'rxjs';
import { NodeDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-node-detail-page',
  templateUrl: './node-detail-page.component.html',
  styleUrls: ['./node-detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeDetailPageComponent extends BaseDetailPageComponent<NodeDetail>
  implements OnInit {
  // We do not need to update node info so often
  debouncedUpdate = debounce(() => {
    this.poll$.next(null);
  }, 60000);

  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<NodeDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'node' });
  }
}
