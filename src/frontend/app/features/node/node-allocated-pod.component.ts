import { Component, Input } from '@angular/core';
import { NodeAllocatedResources } from '~api/backendapi';

@Component({
  selector: 'alk-node-allocated-pod',
  templateUrl: './node-allocated-pod.component.html',
  styleUrls: ['./node-allocated-pod.component.scss'],
})
export class NodeAllocatedPodComponent {
  @Input()
  allocatedResources: NodeAllocatedResources;

  get fraction() {
    return this.allocatedResources && this.allocatedResources.podFraction;
  }

  get capacity() {
    return this.allocatedResources && this.allocatedResources.podCapacity;
  }

  get allocated() {
    return this.allocatedResources && this.allocatedResources.allocatedPods;
  }
}
