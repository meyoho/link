import { Input } from '@angular/core';
import { NodeMetrics } from '~api/backendapi';

export class NodeAllocatedResourcesComponentBase {
  @Input()
  allocatedResources: any; // Refer to NodeAllocatedResources
  @Input()
  metrics: NodeMetrics;
  @Input()
  resourceType: 'cpu' | 'memory';

  get usage(): number {
    return this.metrics && this.metrics[this.resourceType];
  }

  get usageFraction() {
    return (this.usage / this.capacity) * 100;
  }

  get requests() {
    return (
      this.allocatedResources &&
      this.allocatedResources[this.resourceType + 'Requests']
    );
  }

  get requestsFraction() {
    return (
      this.allocatedResources &&
      this.allocatedResources[this.resourceType + 'RequestsFraction']
    );
  }

  get limits() {
    return (
      this.allocatedResources &&
      this.allocatedResources[this.resourceType + 'Limits']
    );
  }

  get limitsFraction() {
    return (
      this.allocatedResources &&
      this.allocatedResources[this.resourceType + 'LimitsFraction']
    );
  }

  get capacity(): number {
    return (
      this.allocatedResources &&
      this.allocatedResources[this.resourceType + 'Capacity']
    );
  }

  get unit() {
    if (this.resourceType === 'cpu') {
      return 'cpu';
    } else if (this.resourceType === 'memory') {
      return 'bytes';
    }
  }
}
