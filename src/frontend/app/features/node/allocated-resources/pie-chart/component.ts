import { ChangeDetectionStrategy, Component, OnChanges } from '@angular/core';
import { arc } from 'd3-shape';

import { TranslateService } from '../../../../translate';
import { NodeAllocatedResourcesComponentBase } from '../base';

interface NodeFractionArc {
  backgroundArcPath: any;
  fractionArcPath: any;
  labels: string[];
  labelCoord: any;
  fractionArcOpacity: number;
  fraction: any;
  color: any;
}

@Component({
  selector: 'alk-node-allocated-resources-pie-chart',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeAllocatedResourcesPieChartComponent
  extends NodeAllocatedResourcesComponentBase
  implements OnChanges {
  readonly size = 200;
  private pieSize = 10;
  private margin = 25;
  private innerShrink = 15;

  arcs: NodeFractionArc[];

  constructor(private translate: TranslateService) {
    super();
  }

  ngOnChanges(): void {
    this.arcs = this.getArcs();
  }

  getColor(fraction: number) {
    if (fraction >= 100) {
      return '#ef8181';
    } else if (this.resourceType === 'cpu') {
      return '#7b6fe2';
    } else {
      return '#33AFE8';
    }
  }

  get transform() {
    return `translate(${(this.size + 20) / 2}, ${this.size / 2})`;
  }

  get outerRadius() {
    return this.size / 2 - this.margin;
  }

  get innerRadius() {
    return this.outerRadius - this.pieSize;
  }

  arcTrackByFn(index: number) {
    return index;
  }

  private getArcs(): NodeFractionArc[] {
    let arcs = [];
    const getArc = (
      level: number,
      fraction: number,
      label: string,
      opacity: number,
      color = '',
    ): NodeFractionArc => {
      return {
        backgroundArcPath: this.getBackgroundArcPath(level),
        fractionArcPath: this.getFractionArcPath(fraction, level),
        labels: [`${fraction.toFixed(2)}%`, label],
        labelCoord: this.getTextCoord(level),
        fractionArcOpacity: opacity,
        fraction: fraction,
        color: color || this.getColor(fraction),
      };
    };

    const limitsArc = getArc(
      0,
      this.limitsFraction,
      this.translate.get('node.limits'),
      1,
    );
    const requestsArc = getArc(
      1,
      this.requestsFraction,
      this.translate.get('node.requests'),
      0.5,
    );

    arcs = [requestsArc, limitsArc];
    if (this.metrics) {
      const usageArc = getArc(
        2,
        this.usageFraction,
        this.translate.get('node.usage'),
        1,
        '#9ece4f',
      );
      arcs = [...arcs, usageArc];
    }

    return arcs;
  }

  private getBackgroundArcPath(level = 0) {
    const shrink = level * this.innerShrink;
    return arc().cornerRadius(20)({
      startAngle: 0,
      endAngle: Math.PI * 1.5,
      innerRadius: this.innerRadius - shrink,
      outerRadius: this.outerRadius - shrink,
    });
  }

  private getFractionArcPath(fraction: number, level = 0) {
    const shrink = level * this.innerShrink;
    fraction = Math.min(100, fraction);
    return arc().cornerRadius(20)({
      startAngle: 0,
      endAngle: (Math.PI * 1.5 * fraction) / 100,
      innerRadius: this.innerRadius - shrink,
      outerRadius: this.outerRadius - shrink,
    });
  }

  private getTextCoord(level = 0): { dx: number; dy: number } {
    const shrink = level * this.innerShrink;
    return {
      dx: -4,
      dy: -(this.outerRadius - shrink - 5),
    };
  }
}
