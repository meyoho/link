import { Component } from '@angular/core';

import { NodeAllocatedResourcesComponentBase } from '../base';

@Component({
  selector: 'alk-node-allocated-resources-bar',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class NodeAllocatedResourcesBarComponent extends NodeAllocatedResourcesComponentBase {}
