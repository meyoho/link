import { Component } from '@angular/core';

import { NodeActionMenuComponent } from '../../node-action-menu.component';

@Component({
  selector: 'alk-node-taint-button',
  template: `
    <aui-icon
      *ngIf="resource"
      (click)="showUpdateNodeTaint()"
      icon="basic:pencil_s"
    ></aui-icon>
  `,
  styles: [
    `
      :host {
        color: #33afe8;
        cursor: pointer;
      }
    `,
  ],
})
export class NodeTaintButtonComponent extends NodeActionMenuComponent {}
