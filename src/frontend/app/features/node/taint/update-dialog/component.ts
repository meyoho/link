import { MessageService } from '@alauda/ui';
import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { NodeTaint } from '~api/backendapi';
import { TranslateService } from '~app/translate';

import { NodeTaintFormComponent } from '../form/component';

@Component({
  selector: 'alk-node-taint-update-dialog',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition(':enter', [
        style({ transform: 'scale(0.95)', opacity: '0' }),
        animate(200, style({ transform: 'scale(1)', opacity: '1' })),
      ]),
    ]),
  ],
})
export class NodeTaintUpdateDialogComponent {
  constructor(
    private cdr: ChangeDetectorRef,
    private auiMessageService: MessageService,
    private translate: TranslateService,
  ) {}
  updating = false;

  @Input()
  title: string;
  @Input()
  taints: NodeTaint[] = [];

  @ViewChild('ntfForm')
  ntfComponent: NodeTaintFormComponent;
  @Input()
  onUpdate: (taints: NodeTaint[]) => Promise<any> = async _taints => {};

  async onConfirm() {
    if (this.ntfComponent.form.invalid) {
      this.auiMessageService.error(
        this.translate.get('node.key_effect_required'),
      );
      return;
    }
    this.updating = true;
    await this.onUpdate(this.taints);
    this.updating = false;
    this.cdr.markForCheck();
  }
}
