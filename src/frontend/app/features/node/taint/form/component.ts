import { Component, Injector } from '@angular/core';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alk-node-taint-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class NodeTaintFormComponent extends BaseResourceFormArrayComponent {
  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultResource(): any[] {
    return [];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      key: [],
      value: [],
      effect: [],
    });
  }
}
