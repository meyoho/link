import { HttpClient } from '@angular/common/http';
import { Component, Injector, Input } from '@angular/core';
import { NodeDetail } from '~api/backendapi';
import { BaseActionMenuComponent } from '~app/abstract';

import { NodeTaintUpdateDialogComponent } from './taint/update-dialog/component';

export enum NodeAction {
  Cordon = 'cordon',
  Uncordon = 'uncordon',
  Drain = 'drain',
}

export const i18nLabels = {
  [NodeAction.Cordon]: {
    title: 'node.cordon',
    message: 'node.message_do_cordon',
  },
  [NodeAction.Uncordon]: {
    title: 'node.uncordon',
    message: 'node.message_do_uncordon',
  },
  [NodeAction.Drain]: {
    title: 'node.drain',
    message: 'node.message_do_drain',
  },
};

@Component({
  selector: 'alk-node-action-menu',
  templateUrl: './node-action-menu.component.html',
  styleUrls: [],
})
export class NodeActionMenuComponent extends BaseActionMenuComponent<
  NodeDetail
> {
  @Input()
  actions = [
    'update',
    'update_labels',
    'update_annotations',
    'update_node_taint',
    'cordon',
    'uncordon',
    'drain',
  ];
  constructor(injector: Injector, private http: HttpClient) {
    super(injector);
  }

  NodeAction = NodeAction;

  get isSchedulable() {
    return this.resource && !this.resource.unschedulable;
  }

  async doNodeAction(action: NodeAction) {
    try {
      const { title, message } = i18nLabels[action];
      const confirmed = await this.confirmBox.confirm({
        title: this.translate.get(title),
        content: this.translate.get(message, {
          node: this.resource.objectMeta.name,
        }),
      });
      if (confirmed) {
        await this.http
          .put(`api/v1/nodes/${this.resource.objectMeta.name}/${action}`, {})
          .toPromise();
        this.auiMessageService.success({
          content: this.translate.get('node.node_action_succeeded', {
            action: this.translate.get(title),
          }),
        });
        this.updated.next(this.resource);
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }

  showUpdateNodeTaint() {
    const dialogRef = this.dialog.open(NodeTaintUpdateDialogComponent);
    dialogRef.componentInstance.taints = this.resource.taints;
    dialogRef.componentInstance.title = this.translate.get('update_node_taint');

    dialogRef.componentInstance.onUpdate = async (taints: any) => {
      try {
        await this.http
          .put(`api/v1/nodes/${this.resource.objectMeta.name}/taint`, taints)
          .toPromise();

        this.updated.next(this.resource);
        dialogRef.close();
        this.auiMessageService.success({
          content: this.translate.get('node.node_action_succeeded', {
            action: this.translate.get('update_node_taint'),
          }),
        });
      } catch (error) {
        this.auiNotificationService.error({ content: error.error || error });
      }
    };
  }
}
