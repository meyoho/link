import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ResourceMutatePageDeactivateGuard } from '../../services';

import { StatefulSetDetailPageComponent } from './detail-page.component';
import { StatefulSetListPageComponent } from './list-page.component';
import { StatefulSetMutatePageComponent } from './mutate-page/component';

// TODO: move somewhere else, like a universal sample provider?
const sample = `apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
  namespace: default
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        ports:
        - containerPort: 80
          name: web
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: StatefulSetListPageComponent,
      },
      {
        path: 'detail',
        component: StatefulSetDetailPageComponent,
      },
      {
        path: 'update',
        component: StatefulSetMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
      },
      {
        path: 'create',
        component: StatefulSetMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class StatefulSetRoutingModule {}
