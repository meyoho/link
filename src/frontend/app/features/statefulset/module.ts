import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { FeaturesSharedWorkloadModule } from '../../features-shared/workload-module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { StatefulSetActionMenuComponent } from './action-menu.component';
import { StatefulSetDetailBasicInfoComponent } from './detail-basic-info.component';
import { StatefulSetDetailPageComponent } from './detail-page.component';
import { StatefulSetFormComponent } from './form/component';
import i18n from './i18n';
import { StatefulSetListPageComponent } from './list-page.component';
import { StatefulSetMutatePageComponent } from './mutate-page/component';
import { StatefulSetRoutingModule } from './routing.module';
import { StatefulSetUpdateStrategyFormComponent } from './update-strategy-form/component';

const MODULE_PREFIX = 'statefulset';

@NgModule({
  imports: [
    SharedModule,
    StatefulSetRoutingModule,
    FeatureSharedCommonModule,
    FeaturesSharedWorkloadModule,
  ],
  declarations: [
    StatefulSetListPageComponent,
    StatefulSetDetailPageComponent,
    StatefulSetActionMenuComponent,
    StatefulSetDetailBasicInfoComponent,
    StatefulSetMutatePageComponent,
    StatefulSetFormComponent,
    StatefulSetUpdateStrategyFormComponent,
  ],
})
export class StatefulSetModule {
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
