import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alk-stateful-set-update-strategy-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatefulSetUpdateStrategyFormComponent
  extends BaseResourceFormGroupComponent
  implements OnInit, AfterViewInit {
  types = ['RollingUpdate', 'OnDelete'];

  constructor(injector: Injector) {
    super(injector);
  }

  getResourceMergeStrategy() {
    return false;
  }

  getDefaultFormModel() {
    return { type: 'RollingUpdate' };
  }

  createForm() {
    return this.fb.group({
      type: [],
      rollingUpdate: this.fb.group({
        partition: [0],
      }),
    });
  }

  get typeControl() {
    return this.form.get('type');
  }

  ngAfterViewInit() {
    this.typeControl.valueChanges.subscribe(value => {
      const rollingUpdateControl = this.form.get('rollingUpdate');
      if (value !== 'RollingUpdate') {
        rollingUpdateControl.disable({ emitEvent: false });
      } else {
        rollingUpdateControl.enable({ emitEvent: false });
        if (!rollingUpdateControl.get('partition').value) {
          rollingUpdateControl.setValue(
            {
              partition: 0,
            },
            { emitEvent: false },
          );
        }
      }
    });
  }
}
