import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';
import { StatefulSet, StatefulSetTypeMeta } from '~api/raw-k8s';

import { BasePodControllerFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-stateful-set-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatefulSetFormComponent
  extends BasePodControllerFormComponent<StatefulSet>
  implements OnInit {
  kind = 'StatefulSet';
  replicasControl: AbstractControl;

  constructor(injector: Injector) {
    super(injector);
  }

  getDefaultFormModel() {
    return StatefulSetTypeMeta;
  }

  createSpecForm() {
    const specForm = super.createSpecForm();
    specForm.addControl('updateStrategy', this.fb.control({}));
    specForm.addControl('replicas', this.fb.control(1, [Validators.min(0)]));
    this.replicasControl = specForm.get('replicas');
    return specForm;
  }
}
