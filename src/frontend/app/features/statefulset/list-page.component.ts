import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { StatefulSet, StatefulSetList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'pods',
    label: 'status',
  },
  {
    name: 'containerImages',
    label: 'images',
  },
  {
    name: 'metrics',
    label: 'used_resources',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-stateful-set-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatefulSetListPageComponent
  extends BaseResourceListPageComponent<StatefulSetList, StatefulSet>
  implements OnInit {
  map(value: StatefulSetList): StatefulSet[] {
    return value.statefulSets;
  }
  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }
  fetchResources(params: FetchParams): Observable<StatefulSetList | Error> {
    return this.http.get<StatefulSetList>(
      `api/v1/statefulsets/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
