import { Component, Injector, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { StatefulSetDetail } from '~api/backendapi';
import { PodSpec } from '~api/raw-k8s';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-stateful-set-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss'],
})
export class StatefulSetDetailPageComponent
  extends BaseDetailPageComponent<StatefulSetDetail>
  implements OnInit {
  tabs = ['basic_info', 'yaml', 'config_management', 'logs', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<StatefulSetDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'StatefulSet' });
  }

  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  async onPodSpecUpdate(podSpec: PodSpec) {
    const detail = await this.detail$.pipe(first()).toPromise();
    const detailParams = this.resourceData.getResourceDetailParams(detail);

    try {
      await this.resourceData.updateDetailAtPath(
        detailParams,
        'spec.template.spec',
        podSpec,
      );
      this.updated$.next(null);
    } catch (error) {}
  }
}
