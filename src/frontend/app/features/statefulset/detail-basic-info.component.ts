import { Component, EventEmitter, Input, Output } from '@angular/core';
import { StatefulSetDetail } from '~api/backendapi';

import { AppConfigService, ResourceDataService } from '../../services';

@Component({
  selector: 'alk-stateful-set-detail-basic-info',
  templateUrl: './detail-basic-info.component.html',
  styleUrls: ['./detail-basic-info.component.scss'],
})
export class StatefulSetDetailBasicInfoComponent {
  @Input()
  detail: StatefulSetDetail;
  @Output()
  updated = new EventEmitter();

  get podListEndpoint() {
    if (this.detail) {
      return (
        this.resourceData.generateDetailUrl({
          name: this.detail.objectMeta.name,
          namespace: this.detail.objectMeta.namespace,
          kind: 'StatefulSet',
        }) + '/pod'
      );
    } else {
      return '';
    }
  }

  constructor(
    private resourceData: ResourceDataService,
    public appConfig: AppConfigService,
  ) {}
}
