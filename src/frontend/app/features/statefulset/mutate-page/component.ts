import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { StatefulSet } from '~api/raw-k8s';

import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatefulSetMutatePageComponent extends BaseResourceMutatePageComponent<
  StatefulSet
> {
  get kind() {
    return 'StatefulSet';
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
