import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PieArcDatum, arc, pie } from 'd3-shape';
import { GenericStatus, GenericStatusList } from '~api/status';

import { TranslateService } from '../../../translate';

const EventTypeToGenericStatus: { [key: string]: GenericStatus } = {
  Normal: GenericStatus.Success,
  Warning: GenericStatus.Error,
};

const EventTypeToGenericStatusColor: { [key: string]: string } = {
  Normal: '#48c2a8',
  Warning: '#ef8181',
};

interface StatusItem {
  label: string;
  value: number;
  status: string;
  key: string;
  fraction: string;
}

@Component({
  selector: 'alk-status-gauge-pie',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class StatusGaugePieComponent implements OnChanges {
  private pieSize = 16;
  private margin = 25;
  private textOffset = 10;

  @Input()
  width = 250;
  @Input()
  height = 200;
  @Input()
  statusInfo: { [key: string]: number; total?: number } = {};

  private statusItems: StatusItem[];
  arcs: { path: string; color: string }[];

  get transform() {
    return `translate(${this.width / 2}, ${this.height / 2})`;
  }

  get outerRadius() {
    return Math.min(this.width, this.height) / 2 - this.margin;
  }

  get textRadius() {
    return this.outerRadius + this.margin / 2;
  }

  get innerRadius() {
    return this.outerRadius - this.pieSize;
  }

  get total() {
    return Object.keys(this.statusInfo).reduce(
      (accum, key) => accum + this.statusInfo[key],
      0,
    );
  }

  updateArcs() {
    const arcFactory = arc();
    const pies = pie().sort(undefined)(
      this.statusItems.map(item => item.value),
    );
    const arcs = pies.map((_pie, index) => {
      const path = arcFactory({
        startAngle: _pie.startAngle,
        endAngle: _pie.endAngle,
        innerRadius: this.innerRadius,
        outerRadius: this.outerRadius,
      });

      const statusItem = this.statusItems[index];

      return {
        path,
        color: EventTypeToGenericStatusColor[statusItem.key],
        status: statusItem.status,
        value: statusItem.value,
        label: statusItem.label,
        fraction: statusItem.fraction,
        textLink: this.getTextLink(_pie),
      };
    });

    return arcs;
  }

  constructor(private translate: TranslateService) {}

  ngOnChanges({ statusInfo }: SimpleChanges) {
    if (statusInfo) {
      this.statusItems = this.updateStatuItems();
      this.arcs = this.updateArcs();
    }
  }

  trackByFn(index: number) {
    return index;
  }

  private updateStatuItems() {
    const total = this.total;
    return Object.keys(this.statusInfo)
      .map(key => ({
        key,
        label: this.translate.get(
          'overview.event_type_' + key.toLocaleLowerCase(),
        ),
        value: this.statusInfo[key],
        status: EventTypeToGenericStatus[key],
        fraction: (+((this.statusInfo[key] / total) * 100).toFixed(
          2,
        )).toString(),
      }))
      .filter(item => !!item.value)
      .sort(
        (a, b) =>
          GenericStatusList.indexOf(a.status) -
          GenericStatusList.indexOf(b.status),
      );
  }

  getTextLink(item: PieArcDatum<any>) {
    const orignalAngle = (item.startAngle + item.endAngle) / 2;
    const angle = orignalAngle - Math.PI / 2;

    const x1 = Math.cos(angle) * (this.outerRadius - 10);
    const x2 = Math.cos(angle) * (this.textRadius - 15);
    const y1 = Math.sin(angle) * this.outerRadius;
    const y2 = Math.sin(angle) * this.textRadius;
    const x3 =
      orignalAngle > Math.PI ? x2 - this.textOffset : x2 + this.textOffset;
    const x3p =
      orignalAngle > Math.PI
        ? x2 - this.textOffset - 4
        : x2 + this.textOffset + 4;
    const textAnchor = orignalAngle > Math.PI ? 'end' : 'start';

    return {
      d: `M${x1} ${y1} L${x2} ${y2} L${x3} ${y2}`,
      angle,
      end: {
        x: x3p,
        y: y2,
      },
      textAnchor,
    };
  }
}
