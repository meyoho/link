import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { OverEventListComponent } from './event-list/component';
import i18n from './i18n';
import { OverviewPageComponent } from './overview-page.component';
import { OverviewRoutingModule } from './routing.module';
import { StatusGaugePieComponent } from './status-gauge-pie/component';

const MODULE_PREFIX = 'overview';

@NgModule({
  imports: [SharedModule, FeatureSharedCommonModule, OverviewRoutingModule],
  declarations: [
    OverviewPageComponent,
    StatusGaugePieComponent,
    OverEventListComponent,
  ],
  providers: [],
  entryComponents: [],
})
export class OverviewModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
