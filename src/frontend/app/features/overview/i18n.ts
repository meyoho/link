export const en = {
  latest_events: 'Latest event fractions',
  event_type_normal: 'Normal',
  event_type_warning: 'Warning',
};

export const zh = {
  latest_events: '最近发生事件比率',
  event_type_normal: '正常',
  event_type_warning: '警告',
};

export default {
  en,
  zh,
};
