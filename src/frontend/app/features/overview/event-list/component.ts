import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { OverviewEvent, PageParams, Resource } from '~api/backendapi';

import { TimeService } from '../../../services';

@Component({
  selector: 'alk-overview-event-list',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  animations: [
    trigger('zoomInOut', [
      transition('* => *', [
        style({ transform: 'scale(0.99) translateY(5px)', opacity: '0.1' }),
        animate(
          '0.3s ease',
          style({ transform: 'scale(1) translateY(0)', opacity: '1' }),
        ),
      ]),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverEventListComponent implements OnInit, OnChanges {
  @Input()
  events: OverviewEvent[];

  timeFormat = 'HH:mm:ss';

  eventItems: OverviewEvent[] = [];
  pageParams: PageParams = {
    pageIndex: 0,
    pageSize: 10,
  };
  constructor(private time: TimeService) {}

  ngOnInit(): void {}

  trackByFn(index: number) {
    return index;
  }

  shouldShowTimestamp(event: OverviewEvent, index: number): string {
    const timestamp = this.time.transform(event.lastTimestamp, this.timeFormat);
    const lastTimestamp =
      index > 0 ? this.shouldShowTimestamp(this.events[index - 1], 0) : '';
    return index === 0 || lastTimestamp !== timestamp ? timestamp : '';
  }

  getReferencedResource(event: OverviewEvent): Resource {
    return {
      objectMeta: {
        name: event.involvedObject.name,
        namespace: event.involvedObject.namespace,
      },
      typeMeta: {
        kind: event.involvedObject.kind,
        apiVersion: event.involvedObject.apiVersion,
      },
    };
  }

  refreshEventItems(params: PageParams): void {
    const events = this.events || [];
    this.pageParams = params;
    this.eventItems = events.slice(
      params.pageIndex * params.pageSize,
      (params.pageIndex + 1) * params.pageSize,
    );
  }

  onPageChange(curIndex: number) {
    this.refreshEventItems({
      pageIndex: curIndex - 1,
      pageSize: this.pageParams.pageSize,
    });
  }

  onPageSizeChange(pageSize: number) {
    this.refreshEventItems({ pageIndex: 0, pageSize });
  }

  ngOnChanges({ events }: SimpleChanges) {
    if (events) {
      this.refreshEventItems(this.pageParams);
    }
  }
}
