import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OverviewPageComponent } from './overview-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: OverviewPageComponent,
      },
    ]),
  ],
  exports: [RouterModule],
})
export class OverviewRoutingModule {}
