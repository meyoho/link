import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { debounce } from 'lodash';
import { BehaviorSubject, Observable, combineLatest, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';
import { OverviewDetail } from '~api/backendapi';

import { UnitFormatterService } from '../../services';

export class ErrorWrapper {
  constructor(public error: any) {}
}

const EMPTY_OVERVIEW: OverviewDetail = {
  nodeStatus: {
    total: 0,
    running: 0,
    warning: 0,
    failed: 0,
  },
  deploymentStatus: {
    total: 0,
    running: 0,
    warning: 0,
    failed: 0,
  },
  daemonSetStatus: {
    total: 0,
    running: 0,
    warning: 0,
    failed: 0,
  },
  statefulSetStatus: {
    total: 0,
    running: 0,
    warning: 0,
    failed: 0,
  },
  namespaceCount: 0,
  podStatus: {
    total: 0,
    running: 0,
    pending: 0,
    completed: 0,
    failed: 0,
  },
  resourceStatus: {
    totalCpu: 0,
    totalMem: 0,
    requestCpu: 0,
    requestMem: 0,
  },
  eventList: [],
};

@Component({
  selector: 'alk-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverviewPageComponent implements OnInit {
  /**
   * TODO: PLACEHOLDER, we are not using namespace for overview at the moment
   *
   * Namespace of the resource
   */
  namespace$: Observable<string>;
  overviewResponse$: Observable<OverviewDetail | ErrorWrapper>;
  overview$: Observable<OverviewDetail>;
  metricsUsage$: Observable<{ cpu: number; memory: number }>;
  fetching = false;
  // Poll pulses
  poll$ = new BehaviorSubject(null);

  private debouncedUpdate: () => void;

  constructor(
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private unitFormatter: UnitFormatterService,
  ) {}

  fetch(_namespace: string = 'default') {
    return this.http.get<OverviewDetail>(`api/v1/overview/`);
  }

  onStatusBarClicked(type: string, status: string) {
    this.router.navigate([`/${type}/list`, { namespace: '', status }]);
  }

  ngOnInit() {
    this.debouncedUpdate = debounce(() => {
      this.poll$.next(null);
    }, 30 * 1000);

    this.namespace$ = this.activatedRoute.params.pipe(
      map(params => params.namespace || ''),
    );

    this.overviewResponse$ = combineLatest(this.namespace$, this.poll$).pipe(
      debounceTime(50),
      tap(() => {
        this.fetching = true;
        this.cdr.markForCheck();
      }),
      switchMap(([namespace]) =>
        this.fetch(namespace).pipe(
          catchError(error => of(new ErrorWrapper(error))),
        ),
      ),
      tap(() => {
        this.fetching = false;
        this.cdr.markForCheck();
        this.debouncedUpdate();
      }),
    );

    this.overview$ = this.overviewResponse$.pipe(
      filter<OverviewDetail>(detail => !(detail instanceof ErrorWrapper)),
      publishReplay(1),
      refCount(),
      startWith(EMPTY_OVERVIEW),
    );

    this.metricsUsage$ = this.overview$.pipe(
      map(overview => {
        const resourceStatus = overview.resourceStatus;
        if (resourceStatus.metrics) {
          return resourceStatus.metrics;
        } else {
          return {
            cpu: resourceStatus.requestCpu,
            memory: resourceStatus.requestMem,
          };
        }
      }),
    );
  }

  namespaceChanged(namespace: string) {
    this.doNavigate({ namespace });
  }

  cpuValueRender = (value: number) =>
    this.unitFormatter.transform(value, 'cpu');

  memValueRender = (value: number) =>
    this.unitFormatter.transform(value, 'memory');

  getEventStatusInfo(detail: OverviewDetail) {
    return detail.eventList.reduce(
      (accum, event) => {
        const type = event.type;
        return { ...accum, [type]: accum[type] + event.count };
      },
      { Normal: 0, Warning: 0 },
    );
  }

  private doNavigate(newParams: any) {
    const currentParams = this.activatedRoute.snapshot.params;
    this.router.navigate(
      [
        '.',
        {
          ...currentParams,
          ...newParams,
        },
      ],
      {
        relativeTo: this.activatedRoute,
      },
    );
  }
}
