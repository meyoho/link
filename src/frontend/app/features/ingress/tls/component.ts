import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IngressTLS } from '~api/raw-k8s';

@Component({
  selector: 'alk-ingress-tls-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressTLSListComponent {
  // Data context for rows.
  @Input()
  tls: IngressTLS[];

  @Input()
  namespace: string;

  columnDefs = ['secret', 'hosts'];

  get isEmpty() {
    return !this.tls || this.tls.length === 0;
  }

  getSecretResource(name: string) {
    return {
      name: name,
      kind: 'Secret',
      namespace: this.namespace,
    };
  }
}
