import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Secret } from '~api/backendapi';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { IngressTLS } from '~api/raw-k8s';

@Component({
  selector: 'alk-ingress-tls-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressTLSFormComponent extends BaseResourceFormArrayComponent<
  IngressTLS
> {
  @Input()
  secrets: Secret[];
  @Input()
  hosts: string[];

  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): IngressTLS[] {
    return [
      {
        secretName: '',
        hosts: [],
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      secretName: this.fb.control(''),
      hosts: this.fb.control([]),
    });
  }
}
