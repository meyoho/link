import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { HTTPIngressPath } from '~api/raw-k8s';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Port, Service } from '~api/backendapi';

@Component({
  selector: 'alk-ingress-paths-item-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressPathsFormComponent extends BaseResourceFormArrayComponent<
  HTTPIngressPath
> {
  @Input()
  services: Service[];

  constructor(injector: Injector) {
    super(injector);
  }

  getPorts(serviceName: string): Port[] {
    const item = this.services.find(
      service => service.objectMeta.name === serviceName,
    );
    return item ? item.internalEndpoint.ports : [];
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): HTTPIngressPath[] {
    return [];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      path: this.fb.control(''),
      backend: this.fb.group({
        serviceName: this.fb.control(''),
        servicePort: this.fb.control(''),
      }),
    });
  }
}
