import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { cloneDeep, uniq } from 'lodash';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import {
  Port,
  Secret,
  SecretList,
  Service,
  ServiceList,
} from '~api/backendapi';
import {
  Ingress,
  IngressRule,
  IngressTLS,
  IngressTypeMeta,
} from '~api/raw-k8s';
import { BaseKubernetesResourceFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-ingress-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressFormComponent
  extends BaseKubernetesResourceFormComponent<Ingress>
  implements OnInit, OnDestroy {
  kind = 'Ingress';
  namespaceSub: Subscription;
  namespace: string;
  secrets: Secret[] = [];
  services: Service[] = [];

  getDefaultFormModel() {
    return IngressTypeMeta;
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(/^[a-z0-9^-]*[a-z0-9-]*[a-z0-9]$/),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    const specForm = this.fb.group({
      backend: this.fb.group({
        serviceName: this.fb.control(''),
        servicePort: this.fb.control(''),
      }),
      tls: this.fb.control([]),
      rules: this.fb.control([]),
    });
    return this.fb.group({
      metadata: metadataForm,
      spec: specForm,
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.namespaceSub = this.form
      .get('metadata.namespace')
      .valueChanges.pipe(filter(value => !!value))
      .subscribe(async namespace => {
        this.namespace = namespace;
        this.services = await this.fetchServices(namespace);
        this.secrets = await this.fetchSecrets(namespace);
        this.cdr.markForCheck();
      });
    this.namespaceSub = this.form
      .get('spec.backend.serviceName')
      .valueChanges.subscribe(() => {
        this.form.get('spec.backend.servicePort').setValue('');
      });
  }

  fetchServices(namespace: string) {
    this.form.get('spec.backend.serviceName').setValue('');
    return this.http
      .get<ServiceList>(`api/v1/services/${namespace || ''}`)
      .toPromise()
      .then(({ services }) => services)
      .catch(() => []);
  }

  fetchSecrets(namespace: string) {
    return this.http
      .get<SecretList>(`api/v1/secrets/${namespace || ''}`)
      .toPromise()
      .then(({ secrets }) =>
        secrets.filter(secret => secret.type === 'kubernetes.io/tls'),
      )
      .catch(() => []);
  }

  get ports(): Port[] {
    const item = this.services.find(
      service =>
        service.objectMeta.name ===
        this.form.get('spec.backend.serviceName').value,
    );
    return item ? item.internalEndpoint.ports : [];
  }

  get ruleHosts(): string[] {
    const hosts = this.form.get('spec.rules').value || [];
    return uniq(
      hosts
        .map((item: IngressRule) => item.host)
        .filter((item: string) => !!item),
    );
  }

  adaptFormModel(form: { [key: string]: any }) {
    const spec = cloneDeep(form.spec);
    if (spec.backend && !spec.backend.serviceName) {
      delete spec.backend;
    }
    if (spec.tls && spec.tls.length) {
      spec.tls.forEach((tls: IngressTLS) => {
        if (!tls.secretName) {
          delete tls.secretName;
        }
      });
      spec.tls = spec.tls.filter(
        (tls: IngressTLS) => tls.secretName || (tls.hosts && tls.hosts.length),
      );
    }
    spec.rules.forEach((rule: any) => {
      delete rule.secretName;
      if (rule.http && (!rule.http.paths || !rule.http.paths.length)) {
        delete rule.http;
      }
    });
    return { ...form, spec };
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.namespaceSub.unsubscribe();
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
