import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Service } from '~api/backendapi';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { IngressRule } from '~api/raw-k8s';

@Component({
  selector: 'alk-ingress-rules-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressRulesFormComponent extends BaseResourceFormArrayComponent<
  IngressRule
> {
  @Input()
  services: Service[];

  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): IngressRule[] {
    return [
      {
        host: '',
        http: {
          paths: [],
        },
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      host: this.fb.control(''),
      http: this.fb.group({
        paths: this.fb.control([]),
      }),
    });
  }
}
