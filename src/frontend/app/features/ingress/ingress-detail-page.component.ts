import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { IngressDetail } from '~api/backendapi';
import { BaseDetailPageComponent } from '~app/abstract';

import { ResourceDataService } from '../../services';

@Component({
  selector: 'alk-ingress-detail-page',
  templateUrl: './ingress-detail-page.component.html',
  styleUrls: ['./ingress-detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressDetailPageComponent
  extends BaseDetailPageComponent<IngressDetail>
  implements OnInit {
  constructor(private resourceData: ResourceDataService, injector: Injector) {
    super(injector);
  }

  tabs = ['basic_info', 'yaml', 'events'];

  fetchData(params: {
    namespace: string;
    name: string;
  }): Observable<IngressDetail> {
    return this.resourceData.getDetail({ ...params, kind: 'ingress' });
  }
}
