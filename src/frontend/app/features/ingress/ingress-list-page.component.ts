import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { Ingress, IngressList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'host',
    label: 'domain_name',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-ingress-list-page',
  templateUrl: './ingress-list-page.component.html',
  styleUrls: ['./ingress-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressListPageComponent
  extends BaseResourceListPageComponent<IngressList, Ingress>
  implements OnInit {
  map(value: IngressList): Ingress[] {
    return value.items;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<IngressList> {
    return this.http.get<IngressList>(
      `api/v1/ingresses/${params.namespace || ''}`,
      {
        params: getQuery(
          // Currently filter by name means "search"
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  getCellValue(item: Ingress, colName: string) {
    if (colName === 'host') {
      return item.rules.map(rule => rule.host || '*').join('; ');
    } else {
      return super.getCellValue(item, colName);
    }
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
