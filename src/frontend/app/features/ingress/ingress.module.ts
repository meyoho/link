import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import { IngressFormComponent } from './form/component';
import { IngressPathsFormComponent } from './form/paths-form/component';
import { IngressRulesFormComponent } from './form/rules-form/component';
import { IngressTLSFormComponent } from './form/tls-form/component';
import i18n from './i18n';
import { IngressActionMenuComponent } from './ingress-action-menu.component';
import { IngressDetailBasicInfoComponent } from './ingress-detail-basic-info.component';
import { IngressDetailPageComponent } from './ingress-detail-page.component';
import { IngressListPageComponent } from './ingress-list-page.component';
import { IngressRoutingModule } from './ingress-routing.module';
import { IngressMutatePageComponent } from './mutate-page/mutate-page.component';
import { IngressRuleListComponent } from './rule/list/component';
import { IngressRuleValueTableComponent } from './rule/value-table/component';
import { IngressTLSListComponent } from './tls/component';
const MODULE_PREFIX = 'ingress';

@NgModule({
  imports: [SharedModule, FeatureSharedCommonModule, IngressRoutingModule],
  declarations: [
    IngressListPageComponent,
    IngressActionMenuComponent,
    IngressDetailPageComponent,
    IngressDetailBasicInfoComponent,
    IngressMutatePageComponent,
    IngressFormComponent,
    IngressRulesFormComponent,
    IngressPathsFormComponent,
    IngressTLSFormComponent,

    IngressRuleListComponent,
    IngressRuleValueTableComponent,
    IngressTLSListComponent,
  ],
  providers: [],
})
export class IngressModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
