import { Component, Injector, Input } from '@angular/core';
import { Ingress } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-ingress-action-menu',
  templateUrl: './ingress-action-menu.component.html',
  styleUrls: [],
})
export class IngressActionMenuComponent extends BaseActionMenuComponent<
  Ingress
> {
  @Input()
  actions = ['update', 'update_labels', 'update_annotations', 'delete'];
  constructor(injector: Injector) {
    super(injector);
  }
}
