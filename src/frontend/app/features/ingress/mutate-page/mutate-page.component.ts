import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';

import { Ingress } from '~api/raw-k8s';
import { BaseResourceMutatePageComponent } from '~app/abstract';

@Component({
  selector: 'alk-ingress-mutate-page',
  templateUrl: './mutate-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressMutatePageComponent extends BaseResourceMutatePageComponent<
  Ingress
> {
  get kind() {
    return 'Ingress';
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
