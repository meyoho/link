import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IngressRule } from '~api/raw-k8s';

@Component({
  selector: 'alk-ingress-rule-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressRuleListComponent {
  // Data context for rows.
  @Input()
  rules: IngressRule[];

  @Input()
  namespace: string;

  columnDefs = ['host', 'rule'];

  get isEmpty() {
    return !this.rules || this.rules.length === 0;
  }
}
