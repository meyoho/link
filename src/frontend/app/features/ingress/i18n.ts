export const en = {
  create_ingress: 'Create Ingress',
  default_backend: 'Default Backend',
  ingress_rules: 'Ingress Rules',
  ingress_tls: 'TLS Certificates',
  serviceName: 'Service name',
  servicePort: 'Service port',
  rules: 'Rules',
  add_rule: 'Add rule',
  add_tls: 'Add TLS',
  path: 'Path',
  service_name: 'Service name',
  tls_domain_placeholder: 'Input or select domains',
};

export const zh = {
  create_ingress: '创建访问权',
  default_backend: '默认服务',
  ingress_rules: '访问权规则',
  ingress_tls: 'TLS 证书',
  serviceName: '默认服务',
  servicePort: '服务端口',
  rules: '访问权限规则',
  add_rule: '添加访问权限',
  add_tls: '添加 TLS',
  path: '路径',
  service_name: '服务名称',
  tls_domain_placeholder: '请输入或选择域名',
};

export default {
  en,
  zh,
};
