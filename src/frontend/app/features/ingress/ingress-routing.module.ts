import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ResourceMutatePageDeactivateGuard } from '~app/services';

import { IngressDetailPageComponent } from './ingress-detail-page.component';
import { IngressListPageComponent } from './ingress-list-page.component';
import { IngressMutatePageComponent } from './mutate-page/mutate-page.component';

// TODO: move somewhere else, like a universal sample provider or laoder?
const sample = `apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: test-ingress
  namespace: default
spec:
  rules:
  - host: example.com`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: IngressListPageComponent,
      },
      {
        path: 'detail',
        component: IngressDetailPageComponent,
      },
      {
        path: 'update',
        component: IngressMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
      },
      {
        path: 'create',
        component: IngressMutatePageComponent,
        canDeactivate: [ResourceMutatePageDeactivateGuard],
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class IngressRoutingModule {}
