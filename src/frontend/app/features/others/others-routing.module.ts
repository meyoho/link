import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SimpleResourceUpdatePageComponent } from '~app/features-shared/common';

import { OthersDetailPageComponent } from './others-detail-page.component';
import { OthersListPageComponent } from './others-list-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        redirectTo: 'namespaced/list',
        pathMatch: 'full',
      },
      {
        path: 'namespaced',
        redirectTo: 'namespaced/list',
        pathMatch: 'full',
      },
      {
        path: 'clustered',
        redirectTo: 'clustered/list',
        pathMatch: 'full',
      },
      {
        path: 'namespaced',
        children: [
          {
            path: 'list',
            component: OthersListPageComponent,
          },
          {
            path: 'detail',
            component: OthersDetailPageComponent,
          },
          {
            path: 'update',
            component: SimpleResourceUpdatePageComponent,
          },
        ],
      },
      {
        path: 'clustered',
        children: [
          {
            path: 'list',
            component: OthersListPageComponent,
          },
          {
            path: 'detail',
            component: OthersDetailPageComponent,
          },
          {
            path: 'update',
            component: SimpleResourceUpdatePageComponent,
          },
        ],
      },
    ]),
  ],
  exports: [RouterModule],
})
export class OthersRoutingModule {}
