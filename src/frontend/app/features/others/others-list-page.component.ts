import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import {
  OtherResource,
  OtherResourceList,
  OtherResourcesScope,
} from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils/query-builder';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'kind',
    label: 'kind',
    sortable: true,
  },
  {
    name: 'apiVersion',
    label: 'version',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  templateUrl: './others-list-page.component.html',
  styleUrls: ['./others-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OthersListPageComponent
  extends BaseResourceListPageComponent<OtherResourceList, OtherResource>
  implements OnInit {
  scope$: Observable<OtherResourcesScope>;
  map(value: OtherResourceList): OtherResource[] {
    return value.resources;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  getCellValue(resource: OtherResource, colName: string) {
    if (colName === 'apiVersion') {
      return resource.typeMeta.apiVersion;
    }
    return super.getCellValue(resource, colName);
  }

  fetchResources(
    params: { scope: string } & FetchParams,
  ): Observable<OtherResourceList | Error> {
    // FIXME: others does not support namespace in path
    return this.http.get<OtherResourceList>('api/v1/others', {
      params: getQuery(
        filterBy('namespace', params.namespace),
        filterBy('scope', params.scope),
        filterBy('name', params.search),
        sortBy(params.sort.active, params.sort.direction === 'desc'),
        pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
      ),
    });
  }

  initFetchParams$() {
    this.scope$ = this.activatedRoute.parent.url.pipe(
      map(urls => urls[0].path as OtherResourcesScope),
    );
    this.fetchParams$ = combineLatest(
      this.scope$,
      super.initFetchParams$(),
    ).pipe(map(([scope, params]) => ({ ...params, scope })));
    return this.fetchParams$;
  }

  initColumns$() {
    super.initColumns$();
    this.columns$ = this.columns$.pipe(
      withLatestFrom(this.scope$, (columns, scope) =>
        scope === 'clustered'
          ? columns.filter(column => column !== 'namespace')
          : columns,
      ),
    );
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
