import { NgModule } from '@angular/core';
import { FeatureSharedCommonModule } from '~app/features-shared/common/module';

import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import i18n from './i18n';
import { OthersDetailPageComponent } from './others-detail-page.component';
import { OthersListPageComponent } from './others-list-page.component';
import { OthersRoutingModule } from './others-routing.module';

@NgModule({
  imports: [SharedModule, FeatureSharedCommonModule, OthersRoutingModule],
  declarations: [OthersListPageComponent, OthersDetailPageComponent],
  providers: [],
})
export class OthersModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations('others', i18n);
  }
}
