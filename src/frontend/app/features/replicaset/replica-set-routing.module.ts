import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  SimpleResourceCreatePageComponent,
  SimpleResourceUpdatePageComponent,
} from '~app/features-shared/common';

import { ReplicaSetDetailPageComponent } from './replica-set-detail-page.component';
import { ReplicaSetListPageComponent } from './replica-set-list-page.component';

// TODO: move somewhere else, like a universal sample provider or laoder?
const sample = `apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: frontend
  namespace: default
  labels:
    app: guestbook
    tier: frontend
spec:
  # this replicas value is default
  # modify it according to your case
  replicas: 3
  selector:
    matchLabels:
      tier: frontend
    matchExpressions:
      - {key: tier, operator: In, values: [frontend]}
  template:
    metadata:
      labels:
        app: guestbook
        tier: frontend
    spec:
      containers:
      - name: php-redis
        image: gcr.io/google_samples/gb-frontend:v3
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        env:
        - name: GET_HOSTS_FROM
          value: dns
          # If your cluster config does not include a dns service, then to
          # instead access environment variables to find service host
          # info, comment out the 'value: dns' line above, and uncomment the
          # line below.
          # value: env
        ports:
        - containerPort: 80
`;

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: ReplicaSetListPageComponent,
      },
      {
        path: 'detail',
        component: ReplicaSetDetailPageComponent,
      },
      {
        path: 'update',
        component: SimpleResourceUpdatePageComponent,
      },
      {
        path: 'create',
        component: SimpleResourceCreatePageComponent,
        data: {
          sample: sample,
        },
      },
    ]),
  ],
  exports: [RouterModule],
})
export class ReplicaSetRoutingModule {}
