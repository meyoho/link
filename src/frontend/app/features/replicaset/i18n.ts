export const en = {
  create_replicaset: 'Create Replica Set',
};

export const zh = {
  create_replicaset: '创建副本集',
};

export default {
  en,
  zh,
};
