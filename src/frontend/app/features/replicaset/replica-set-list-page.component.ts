import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { ReplicaSet, ReplicaSetList } from '~api/backendapi';
import {
  BaseResourceListPageComponent,
  ColumnDef,
  FetchParams,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'pods',
    label: 'status',
  },
  {
    name: 'containerImages',
    label: 'images',
  },
  {
    name: 'metrics',
    label: 'used_resources',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-replicat-set-list-page',
  templateUrl: './replica-set-list-page.component.html',
  styleUrls: ['./replica-set-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReplicaSetListPageComponent
  extends BaseResourceListPageComponent<ReplicaSetList, ReplicaSet>
  implements OnInit {
  @Input()
  id = 'replicaSetList';

  map(value: ReplicaSetList): ReplicaSet[] {
    return value.replicaSets;
  }

  getCellValue(item: ReplicaSet, colName: string) {
    switch (colName) {
      default:
        return super.getCellValue(item, colName);
    }
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  fetchResources(params: FetchParams): Observable<ReplicaSetList> {
    return this.http.get<ReplicaSetList>(
      `api/v1/replicasets/${params.namespace || ''}`,
      {
        params: getQuery(
          filterBy('name', params.search),
          sortBy(params.sort.active, params.sort.direction === 'desc'),
          pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
        ),
      },
    );
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
