import { NgModule } from '@angular/core';

import { FeaturesSharedWorkloadModule } from '../../features-shared/workload-module';
import { SharedModule } from '../../shared/shared.module';
import { TranslateService } from '../../translate';

import i18n from './i18n';
import { ReplicaSetActionMenuComponent } from './replica-set-action-menu.component';
import { ReplicaSetDetailBasicInfoComponent } from './replica-set-detail-basic-info.component';
import { ReplicaSetDetailPageComponent } from './replica-set-detail-page.component';
import { ReplicaSetListPageComponent } from './replica-set-list-page.component';
import { ReplicaSetRoutingModule } from './replica-set-routing.module';

@NgModule({
  imports: [
    SharedModule,
    ReplicaSetRoutingModule,
    FeaturesSharedWorkloadModule,
  ],
  declarations: [
    ReplicaSetListPageComponent,
    ReplicaSetDetailPageComponent,
    ReplicaSetDetailBasicInfoComponent,
    ReplicaSetActionMenuComponent,
  ],
  providers: [],
})
export class ReplicaSetModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations('replicaset', i18n);
  }
}
