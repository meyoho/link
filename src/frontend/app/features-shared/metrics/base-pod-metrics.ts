import { Input } from '@angular/core';
import { PodMetrics } from '~api/backendapi';

export class BasePodMetricsComponent {
  @Input()
  metrics: PodMetrics;

  get safeMetrics() {
    return this.metrics || {};
  }

  types = ['cpu', 'memory'];
  typesToIcon = {
    cpu: 'basic:cpu',
    memory: 'basic:memory',
  };
  typesToColor = {
    cpu: '#7C70E2',
    memory: '#33AFE8',
  };
}
