import { Component } from '@angular/core';

import { BasePodMetricsComponent } from '../base-pod-metrics';

/**
 * Simple metrics cell which only display a simple number for each index.
 */
@Component({
  selector: 'alk-pod-metrics-cell',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class PodMetricsCellComponent extends BasePodMetricsComponent {}
