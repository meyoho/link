export const en = {
  resource_usage: '{{ resource }} usage',
};

export const zh = {
  resource_usage: '已使用{{ resource }}',
};

export default {
  en,
  zh,
};
