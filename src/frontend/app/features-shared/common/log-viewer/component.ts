import { DIALOG_DATA, DialogRef, DialogService, DialogSize } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  SimpleChanges,
} from '@angular/core';
import { saveAs } from 'file-saver';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, mergeScan, publishReplay, refCount, tap } from 'rxjs/operators';
import {
  LogDetails,
  LogInfo,
  LogLine,
  LogSelection,
  LogSources,
} from '~api/backendapi';

import { CodeEditorIntl, MonacoProviderService } from '@alauda/code-editor';
import { LogViewerService } from '~app/services/log-viewer.service';
import { TranslateService } from '~app/translate';

const logsPerView = 100;
const maxLogSize = 2e9;
// Load logs from the beginning of the log file. This matters only if the log file is too large to
// be loaded completely.
const beginningOfLogFile = 'beginning';
// Load logs from the end of the log file. This matters only if the log file is too large to be
// loaded completely.
const endOfLogFile = 'end';
const oldestTimestamp = 'oldest';
const newestTimestamp = 'newest';

export interface LogSelectionParams {
  logFilePosition: string;
  referenceTimestamp: string;
  referenceLineNum: number;
  offsetFrom: number;
  offsetTo: number;
}

type LogSelectionParamScaner = (prev: LogSelection) => LogSelectionParams;

@Component({
  selector: 'alk-logs-viewer',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class LogViewerComponent implements OnInit, OnChanges, OnDestroy {
  // The requested logs' resource type
  @Input()
  resourceType: string;

  // The requested logs' resource name
  @Input()
  resourceName: string;

  // Current namespace
  @Input()
  namespace: string;

  // Log sources based on resource type/name
  logSources: LogSources;

  // Selected pod
  pod: string;

  // Selected container
  container: string;

  logSelectionParamScanner$ = new BehaviorSubject<LogSelectionParamScaner>(
    null,
  );
  logDetails$: Observable<LogDetails>;
  logs$: Observable<string>;
  logInfo$: Observable<LogInfo>;
  logsReady$: Observable<boolean>;

  monacoOptions: monaco.editor.IEditorConstructionOptions = {
    wordWrap: 'on',
    readOnly: true,
    renderLineHighlight: 'none',
  };

  pollNewest = true;

  /**
   * The fullscreen dialog.
   */
  fullscreenDialog: DialogRef<LogViewerComponent>;

  private editor: monaco.editor.IStandaloneCodeEditor;
  private logDetails: LogDetails; // Latest fetched log detail
  private intervalTimer: any;

  constructor(
    private logViewer: LogViewerService,
    private http: HttpClient,
    private zone: NgZone,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    public dialog: DialogService,
    @Optional()
    @Inject(DIALOG_DATA)
    public source: LogViewerComponent,
    public codeEditorIntl: CodeEditorIntl,
    public monacoProvider: MonacoProviderService,
  ) {}

  ngOnInit() {
    this.intervalTimer = setInterval(() => {
      if (this.pollNewest && !this.fullscreenDialog) {
        this.loadNewest();
      }
    }, 10000);

    // If opened in a new window:
    if (this.source) {
      this.resourceName = this.source.resourceName;
      this.resourceType = this.source.resourceType;
      this.namespace = this.source.namespace;
      this.logSources = this.source.logSources;
      this.pod = this.source.pod;
      this.container = this.source.container;
      this.setupLogDetails$();
    }
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalTimer);
  }

  ngOnChanges({ resourceName, resourceType, namespace }: SimpleChanges): void {
    if (resourceName || resourceType || namespace) {
      this.getLogSources();
    }
  }

  async getLogSources() {
    if (this.namespace && this.resourceType && this.resourceName) {
      this.logSources = await this.http
        .get<LogSources>(
          `api/v1/log/source/${this.namespace}/${this.resourceType}/${
            this.resourceName
          }`,
        )
        .toPromise();
    }
    if (this.logSources) {
      this.onPodSelected(this.pod);
      this.onContainerSelected(this.container);
      this.cdr.markForCheck();
    }
  }

  onPodSelected(pod?: string) {
    if (!pod || pod !== this.pod) {
      this.pod = pod || this.logSources.podNames[0];
      this.setupLogDetails$();
    }
  }

  onContainerSelected(container?: string) {
    if (!container || container !== this.container) {
      this.container = container || this.logSources.containerNames[0];
      this.setupLogDetails$();
    }
  }

  loadNewest() {
    this.logSelectionParamScanner$.next(_prev => ({
      logFilePosition: endOfLogFile,
      referenceTimestamp: newestTimestamp,
      referenceLineNum: 0,
      offsetFrom: maxLogSize,
      offsetTo: maxLogSize + logsPerView,
    }));
  }

  loadOldest() {
    this.logSelectionParamScanner$.next(_prev => ({
      logFilePosition: beginningOfLogFile,
      referenceTimestamp: oldestTimestamp,
      referenceLineNum: 0,
      offsetFrom: -maxLogSize - logsPerView,
      offsetTo: -maxLogSize,
    }));
    this.pollNewest = false;
  }

  /**
   * Shifts view by maxLogSize lines to the past.
   * @export
   */
  loadOlder() {
    this.logSelectionParamScanner$.next(prev => ({
      logFilePosition: prev.logFilePosition,
      referenceTimestamp: prev.referencePoint.timestamp,
      referenceLineNum: prev.referencePoint.lineNum,
      offsetFrom: prev.offsetFrom - logsPerView,
      offsetTo: prev.offsetFrom,
    }));
    this.pollNewest = false;
  }

  /**
   * Shifts view by maxLogSize lines to the future.
   * @export
   */
  loadNewer() {
    this.logSelectionParamScanner$.next(prev => ({
      logFilePosition: prev.logFilePosition,
      referenceTimestamp: prev.referencePoint.timestamp,
      referenceLineNum: prev.referencePoint.lineNum,
      offsetFrom: prev.offsetTo,
      offsetTo: prev.offsetTo + logsPerView,
    }));
    this.pollNewest = false;
  }

  onFindClicked() {
    return this.checkActionAndRun('actions.find');
  }

  onMonacoEditorChanged(editor: monaco.editor.IStandaloneCodeEditor) {
    this.editor = editor;
    this.scrollToBottom();
  }

  toggleFullscreen() {
    if (this.fullscreenDialog) {
      this.fullscreenDialog.close();
      this.fullscreenDialog = undefined;
      this.cdr.detectChanges();
    } else if (this.source) {
      this.source.toggleFullscreen();
    } else {
      this.fullscreenDialog = this.dialog.open(LogViewerComponent, {
        size: DialogSize.Fullscreen,
        data: this,
      });
      this.fullscreenDialog.afterClosed().subscribe(() => {
        this.fullscreenDialog = undefined;
      });
      this.cdr.detectChanges();
    }
  }

  async onDownloadClicked() {
    const endpoint = `api/v1/log/file/${this.namespace}/${this.pod}/${
      this.container
    }`;
    const content = await this.http
      .get(endpoint, { responseType: 'blob' })
      .toPromise();
    saveAs(content, `logs-from-${this.container}-in-${this.pod}.txt`);
  }

  private checkActionAndRun(actionName: string) {
    const action = this.getEditorAction(actionName);
    return action && action.run();
  }

  private getEditorAction(actionName: string) {
    return this.editor && this.editor.getAction(actionName);
  }

  private formatAllLogs_(logs: LogLine[]) {
    if (logs.length === 0) {
      logs = [
        {
          timestamp: '0',
          content: this.translate.get('zero_state_hint', {
            resourceName: this.translate.get('logs'),
          }),
        },
      ];
    }
    return logs.map(line => this.formatLine(line));
  }

  /**
   * Formats the given log line as raw HTML to display to the user.
   */
  private formatLine(line: LogLine) {
    // TODO: add ansi colors.
    // monaco+ANSI+terminal+colors

    // add timestamp if needed
    const showTimestamp = this.logViewer.getShowTimestamp();
    return showTimestamp ? `${line.timestamp} ${line.content}` : line.content;
  }

  private scrollToBottom() {
    this.zone.runOutsideAngular(() => {
      setTimeout(() => {
        if (this.editor && this.logDetails) {
          this.editor.revealLine(this.logDetails.logs.length);
        }
      });
    });
  }

  private setupLogDetails$() {
    if (!this.pod || !this.container) {
      // Only setup when pod/container is set
      return;
    }
    this.logDetails$ = this.logSelectionParamScanner$.pipe(
      // Hmm ... switchScan may have a better performance
      mergeScan<LogSelectionParamScaner, LogDetails>(
        (prev, scannerFn) => {
          const params: any = scannerFn(prev && prev.selection);
          function sanitizeParam(param: string) {
            if (params[param] !== undefined && params[param] !== null) {
              params[param] = '' + params[param];
            } else {
              params[param] = '';
            }
          }

          Object.keys(params).forEach(param => sanitizeParam(param));

          const endpoint = `api/v1/log/${this.namespace}/${this.pod}/${
            this.container
          }`;
          return this.http.get<LogDetails>(endpoint, { params });
        },
        null,
        1,
      ),
      tap(logDetails => {
        this.logDetails = logDetails;
      }),
      publishReplay(1),
      refCount(),
    );

    this.logs$ = this.logDetails$.pipe(
      map(logDetails => this.formatAllLogs_(logDetails.logs).join('\n')),
    );

    this.logInfo$ = this.logDetails$.pipe(map(logDetails => logDetails.info));

    this.logsReady$ = this.logInfo$.pipe(
      map(logInfo => logInfo.fromDate && logInfo.fromDate !== '0'),
    );

    this.loadNewest();
  }
}
