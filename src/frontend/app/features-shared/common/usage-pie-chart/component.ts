import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { PieArcDatum, arc, pie } from 'd3-shape';

@Component({
  selector: 'alk-usage-pie-chart',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UsagePieChartComponent {
  private pieSize = 10;
  private margin = 25;
  private textOffset = 10;

  @Input()
  width = 250;

  @Input()
  height = 200;

  @Input()
  total = 0;

  @Input()
  used = 0;

  @Input()
  color = '#1bb392';

  @Input()
  valueRender: (value: number) => string = value => value.toString();

  private valueAccessor: (item: any) => number = (item: any) => item;

  get fraction() {
    return this.total ? (this.used / this.total) * 100 : 0;
  }

  get transform() {
    return `translate(${this.width / 2}, ${this.height / 2})`;
  }

  get outerRadius() {
    return Math.min(this.width, this.height) / 2 - this.margin;
  }

  get textRadius() {
    return this.outerRadius + this.margin / 2;
  }

  get innerRadius() {
    return this.outerRadius - this.pieSize;
  }

  get arcs() {
    return pie()
      .value(this.valueAccessor)
      .sort(undefined)([this.used, this.total - this.used]);
  }

  get fullArcD() {
    return arc()({
      startAngle: 0,
      endAngle: Math.PI * 2,
      innerRadius: this.innerRadius,
      outerRadius: this.outerRadius,
    });
  }

  get fractionArcD() {
    const { startAngle, endAngle } = this.arcs[0];
    return arc().cornerRadius(20)({
      startAngle,
      endAngle,
      innerRadius: this.innerRadius - 3,
      outerRadius: this.outerRadius + 3,
    });
  }

  get unused() {
    return Math.max(this.total - this.used, 0);
  }

  trackByFn(index: number) {
    return index;
  }

  textLink(item: PieArcDatum<any>) {
    const orignalAngle = (item.startAngle + item.endAngle) / 2;
    const angle = orignalAngle - Math.PI / 2;

    let x2 = Math.cos(angle) * (this.textRadius - 15);
    let y2 = Math.sin(angle) * this.textRadius;

    // Makes sure the texts will not present in the center.
    if (Math.abs(y2) < this.height * 0.4) {
      y2 = (y2 / Math.abs(y2)) * (this.height * 0.4);
    }

    if (Math.abs(x2) + 100 > this.width / 2) {
      x2 = (x2 / Math.abs(x2)) * (this.width / 2 - 100);
    }

    const innerAngle = Math.atan2(y2, x2);

    const x1 = Math.cos(innerAngle) * this.outerRadius;
    const y1 = Math.sin(innerAngle) * this.outerRadius;

    const x3 =
      orignalAngle > Math.PI ? x2 - this.textOffset : x2 + this.textOffset;
    const x3p =
      orignalAngle > Math.PI
        ? x2 - this.textOffset - 4
        : x2 + this.textOffset + 4;
    const textAnchor = orignalAngle > Math.PI ? 'end' : 'start';

    return {
      d: `M${x1} ${y1} L${x2} ${y2} L${x3} ${y2}`,
      angle,
      end: {
        x: x3p,
        y: y2,
      },
      textAnchor,
    };
  }
}
