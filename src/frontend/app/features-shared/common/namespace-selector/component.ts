import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  NgZone,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { Observable } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';
import { NamespaceList } from '~api/backendapi';

import { FeatureStateService } from '~app/services';
import { scrollIntoView } from '~app/utils';

@Component({
  selector: 'alk-namespace-selector',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceSelectorComponent implements OnInit, OnChanges {
  @Input()
  namespace: string;

  @Input()
  hideAllNamespaces: boolean;

  @Output()
  namespaceChanged = new EventEmitter<string>();

  labelClicked = new EventEmitter<void>();

  namespaces$: Observable<string[]>;
  constructor(
    private http: HttpClient,
    private featureState: FeatureStateService,
    private ngZone: NgZone,
  ) {}

  ngOnInit() {
    this.namespaces$ = this.labelClicked.pipe(
      startWith(),
      switchMap(() =>
        this.http.get<NamespaceList>('api/v1/permission_namespaces'),
      ),
      map(({ namespaces }) =>
        namespaces.map(namespaceMeta => namespaceMeta.objectMeta.name),
      ),
      publishReplay(1),
      refCount(),
      tap(() => this.scrollToSelected()),
    );
  }

  ngOnChanges() {
    if (this.namespace === undefined || this.namespace === null) {
      this.onSelectChange(this.featureState.get('namespace'));
    }
  }

  onSelectChange(namespace: string) {
    namespace = namespace || '';
    if (namespace !== this.namespace) {
      this.namespace = namespace;
      this.namespaceChanged.emit(namespace);
      this.featureState.set('namespace', namespace);
    }

    this.scrollToSelected();
  }

  onMenuShow() {
    this.scrollToSelected();
  }

  private scrollToSelected() {
    // Make sure the selected namespace is visible
    this.ngZone.runOutsideAngular(() => {
      setTimeout(() => {
        try {
          const container = document.querySelector(
            '.namespace-selector-dropdown .aui-menu',
          );
          const element = container.querySelector(
            '.namespace-dropdown__item--selected .aui-menu-item',
          );
          scrollIntoView(container as HTMLElement, element as HTMLElement);
        } catch (err) {
          // no-ops
        }
      });
    });
  }
}
