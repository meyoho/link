import { Component, Injector } from '@angular/core';
import { Resource } from '~api/backendapi';
import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-update-labels-button',
  template: `
    <aui-icon
      *ngIf="resource"
      (click)="showUpdateLabels()"
      icon="basic:pencil_s"
    ></aui-icon>
  `,
  styles: [
    `
      :host {
        color: #33afe8;
        cursor: pointer;
      }
    `,
  ],
})
export class UpdateLabelsButtonComponent extends BaseActionMenuComponent<
  Resource
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
