import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

import { Service } from '~api/backendapi';

interface ColumnDef {
  name: string;
  label?: string;
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
  },
  {
    name: 'type',
    label: 'type',
  },
  {
    name: 'cluster_ip',
    label: 'cluster_ip',
  },
  {
    name: 'internal_endpoints',
    label: 'internal_endpoints',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-ref-service-list',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RefServiceListComponent implements OnInit {
  @Input()
  list: Service[];

  @Input()
  kind: string; // Use to give zero state an hint

  columnDefs = ALL_COLUMN_DEFS;
  columns = this.columnDefs.map(columnDef => columnDef.name);

  constructor() {}

  ngOnInit() {}

  getCellValue(item: Service, colName: string) {
    switch (colName) {
      case 'cluster_ip':
        return item.clusterIP;
      case 'internal_endpoints':
        return item.internalEndpoint;
      case 'selectors':
        return item.selector;
      case 'name':
      case 'namespace':
      case 'creationTimestamp':
      case 'labels':
        return item.objectMeta[colName];
      case 'kind':
        return item.typeMeta[colName];
      default:
        return (item as any)[colName];
    }
  }
}
