import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';

import { BaseStringMapFormComponent } from '~app/abstract';

@Component({
  selector: 'alk-key-value-form-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyValueFormTableComponent extends BaseStringMapFormComponent
  implements OnInit {
  form: FormArray;
  constructor(injector: Injector) {
    super(injector);
  }

  rowBackgroundColorFn(row: FormControl) {
    if (row.invalid) {
      return '#fdefef';
    } else {
      return '';
    }
  }
}
