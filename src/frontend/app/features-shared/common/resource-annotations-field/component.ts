import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Resource } from '~api/backendapi';

@Component({
  selector: 'alk-resource-annotations-field',
  templateUrl: './template.html',
  styles: [
    `
      alk-update-annotations-button {
        margin-left: 4px;
        flex-shrink: 0;
      }
    `,
  ],
})
export class ResourceAnnotationsFieldComponent {
  @Input()
  resource: Resource;
  @Output()
  updated = new EventEmitter();
}
