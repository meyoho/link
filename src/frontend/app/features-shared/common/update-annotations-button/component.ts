import { Component, Injector } from '@angular/core';
import { Resource } from '~api/backendapi';
import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-update-annotations-button',
  template: `
    <aui-icon
      *ngIf="resource"
      (click)="showUpdateAnnotations()"
      icon="basic:pencil_s"
    ></aui-icon>
  `,
  styles: [
    `
      :host {
        color: #33afe8;
        cursor: pointer;
      }
    `,
  ],
})
export class UpdateAnnotationtsButtonComponent extends BaseActionMenuComponent<
  Resource
> {
  constructor(injector: Injector) {
    super(injector);
  }
}
