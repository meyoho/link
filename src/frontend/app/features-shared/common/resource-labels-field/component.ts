import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Resource } from '~api/backendapi';

@Component({
  selector: 'alk-resource-labels-field',
  templateUrl: './template.html',
  styles: [
    `
      alk-update-labels-button {
        margin-left: 4px;
        flex-shrink: 0;
      }
    `,
  ],
})
export class ResourceLabelsFieldComponent {
  @Input()
  resource: Resource;
  @Output()
  updated = new EventEmitter();
}
