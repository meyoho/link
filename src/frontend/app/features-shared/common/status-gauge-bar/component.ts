import { Status } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { reduce } from 'lodash';
import { GenericStatus, GenericStatusList } from '~api/status';

import { TranslateService } from '../../../translate';

@Component({
  selector: 'alk-status-gauge-bar',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatusGaugeBarComponent implements OnInit, OnChanges {
  @Input()
  statusInfo: { [key: string]: number; total?: number } = {};

  @Input()
  labelRender: (key: string) => string;

  @Output()
  statusBarClick = new EventEmitter();

  statusItems: Status[] = [];

  StatusToGenericStatus: { [key: string]: GenericStatus } = {
    stopped: GenericStatus.Stopped,
    running: GenericStatus.Running,
    pending: GenericStatus.Pending,
    failed: GenericStatus.Error,
    warning: GenericStatus.Warning,
    bound: GenericStatus.Bound,
    lost: GenericStatus.Warning,
    completed: GenericStatus.Success,
    unknown: GenericStatus.Unknown,
    null: GenericStatus.Null,
  };

  onStatusBarClick(status: string) {
    this.statusBarClick.emit(status);
  }

  constructor(private translate: TranslateService) {}

  ngOnInit() {
    const defaultLabelRender = (key: string) => this.translate.get(key);
    this.labelRender = this.labelRender || defaultLabelRender;
  }

  ngOnChanges({ statusInfo }: SimpleChanges) {
    if (statusInfo) {
      this.statusItems = this.updateStatuItems();
    }
  }

  trackByFn(index: number) {
    return index;
  }

  private updateStatuItems() {
    const item = 'status-gauge-container__item';
    const total =
      this.statusInfo.total ||
      reduce(this.statusInfo, (acc: number, cur: number) => acc + cur, 0);
    // 当为空时，不能不显示任何状态，因此显示一条null
    if (!total) {
      return Object.keys(this.statusInfo)
        .filter(key => key !== 'total')
        .map((key, i) => ({
          key,
          status: this.StatusToGenericStatus.null,
          scale: i === 0 ? 1 : 0,
          class: item + ` ${item}--${this.StatusToGenericStatus.null}`,
        }));
    }
    return Object.keys(this.statusInfo)
      .filter(key => key !== 'total')
      .map(key => ({
        key,
        status: this.StatusToGenericStatus[key],
        scale: this.statusInfo[key],
        class: item + ` ${item}--${this.StatusToGenericStatus[key]}`,
      }))
      .sort(
        (a, b) =>
          GenericStatusList.indexOf(a.status) -
          GenericStatusList.indexOf(b.status),
      );
  }
}
