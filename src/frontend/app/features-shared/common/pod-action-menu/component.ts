import { Component, Injector, Input } from '@angular/core';
import { Pod } from '~api/backendapi';

import { BaseActionMenuComponent } from '~app/abstract';

@Component({
  selector: 'alk-pod-action-menu',
  templateUrl: './template.html',
  styleUrls: [],
})
export class PodActionMenuComponent extends BaseActionMenuComponent<Pod> {
  @Input()
  actions = ['view_logs', 'update_labels', 'update_annotations', 'delete'];
  constructor(injector: Injector) {
    super(injector);
  }
}
