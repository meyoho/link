import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import {
  catchError,
  first,
  map,
  publishReplay,
  refCount,
  switchMap,
} from 'rxjs/operators';
import { OtherResourceDetail } from '~api/backendapi';

import { HistoryService, OthersDataService } from '../../../services';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class SimpleResourceUpdatePageComponent implements OnInit {
  kind$: Observable<string>;
  name$: Observable<string>;
  detailResponse$: Observable<OtherResourceDetail | HttpErrorResponse>;

  constructor(
    private othersData: OthersDataService,
    private activatedRoute: ActivatedRoute,
    private history: HistoryService,
  ) {}

  ngOnInit() {
    this.kind$ = this.activatedRoute.params.pipe(map(params => params.kind));

    this.name$ = this.activatedRoute.params.pipe(map(params => params.name));

    this.detailResponse$ = this.activatedRoute.params.pipe(
      first(),
      switchMap(params =>
        this.othersData.getDetail(params as any).pipe(
          catchError(error => {
            return of(error);
          }),
        ),
      ),
      publishReplay(1),
      refCount(),
    );
  }

  onModeChange(mode: string) {
    if (mode === 'view') {
      this.history.back(['..'], { relativeTo: this.activatedRoute });
    }
  }
}
