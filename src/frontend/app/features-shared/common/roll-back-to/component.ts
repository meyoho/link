import { DialogRef, MessageService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Resource, ResourceDetail } from '~api/backendapi';
import {
  ConfirmBoxService,
  DetailParams,
  OthersDataService,
  TimeService,
} from '~app/services';
import { TranslateService } from '~app/translate';

@Component({
  selector: 'alk-roll-back-dialog',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class RollBackComponent implements OnInit {
  @Input() resource: Resource;
  @Input() curResourceParams: DetailParams;
  @Input() rollBackResourceParams: DetailParams;

  updated$ = new BehaviorSubject(null);

  curData: ResourceDetail;
  newData: ResourceDetail;

  async ngOnInit() {
    const newDetailParams = {
      apiVersion: this.resource.typeMeta.apiVersion,
      kind: this.resource.typeMeta.kind,
      name: this.resource.objectMeta.name,
      namespace: this.resource.objectMeta.namespace,
    };
    [this.curData, this.newData] = await Promise.all([
      this.othersData.getDetail(this.curResourceParams).toPromise(),
      this.othersData.getDetail(newDetailParams).toPromise(),
    ]);
  }

  onConfirm() {
    const version = this.time.transform(
      this.resource.objectMeta.creationTimestamp,
    );
    this.confirmBox
      .confirm({
        title: this.translate.get('roll_back_to_title', {
          version,
        }),
        content: this.translate.get('roll_back_to_hint', {
          version,
        }),
      })
      .then(() => {
        const params = this.rollBackResourceParams;
        this.http
          .put(
            `api/v1/${params.kind.toLowerCase()}s/${params.namespace}/${
              params.name
            }/rollback/${
              this.newData.objectMeta.annotations[
                params.kind.toLowerCase() + '.kubernetes.io/revision'
              ]
            }`,
            {},
          )
          .toPromise()
          .then(() => {
            this.auiMessageService.success({
              content: `${this.translate.get(
                params.kind.toLowerCase(),
              )} ${this.translate.get('roll_back_succeeded')}`,
            });
            this.updated$.next(this.curData);
            this.dialogRef.close();
          })
          .catch(error => {
            this.auiMessageService.error(error);
          });
      })
      .catch(() => {});
  }

  constructor(
    private http: HttpClient,
    private othersData: OthersDataService,
    private confirmBox: ConfirmBoxService,
    private translate: TranslateService,
    private time: TimeService,
    private auiMessageService: MessageService,
    private dialogRef: DialogRef,
  ) {}
}
