import { Component, Input, OnInit } from '@angular/core';
import { Condition } from '~api/backendapi';

@Component({
  selector: 'alk-condition-list',
  templateUrl: './template.html',
})
export class ConditionListComponent implements OnInit {
  @Input()
  conditions: Condition[] = [];
  @Input()
  kind = 'Pod';

  columns: string[];

  ngOnInit() {
    if (this.kind === 'Deployment') {
      this.columns = [
        'condition_type',
        'status',
        'lastTransitionTime',
        'reason',
        'message',
      ];
    } else {
      this.columns = [
        'condition_type',
        'status',
        'lastProbeTime',
        'lastTransitionTime',
        'reason',
        'message',
      ];
    }
  }

  get showZeroState() {
    return !this.conditions || this.conditions.length === 0;
  }
}
