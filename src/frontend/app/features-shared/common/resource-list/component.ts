import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Resource, ResourceList } from '~api/backendapi';
import {
  BaseResourceListComponent,
  ColumnDef,
  FetchParams,
  TableState,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
  },
  {
    name: 'namespace',
    label: 'namespace',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
  {
    name: 'action',
    label: '',
  },
];

@Component({
  selector: 'alk-resource-list',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceListComponent
  extends BaseResourceListComponent<ResourceList, Resource>
  implements OnInit {
  @Input()
  kind: string;
  @Input()
  endpoint: string;
  @Input()
  resourceKeyInList: string;
  @Input()
  initResourceList: ResourceList;
  @Input()
  initResourceListPageSize: number;

  tableState$ = new BehaviorSubject<TableState>({});
  initialized = false;

  map(value: ResourceList): Resource[] {
    return (value as any)[this.resourceKeyInList];
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  getTableState$(): Observable<TableState> {
    return this.tableState$;
  }

  fetchResources(params: FetchParams): Observable<ResourceList | Error> {
    if (!this.initialized && this.initResourceList) {
      this.initialized = true;
      return of(this.initResourceList);
    } else {
      return this.http.get<ResourceList>(
        `${this.endpoint}/${params.namespace || ''}`,
        {
          params: getQuery(
            filterBy('name', params.search),
            sortBy(params.sort.active, params.sort.direction === 'desc'),
            pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
          ),
        },
      );
    }
  }

  setTableState(newState: TableState): Promise<boolean> {
    this.tableState$.next(newState);
    return Promise.resolve(true);
  }

  ngOnInit() {
    if (this.initResourceList) {
      this.tableState$.next({
        pageIndex: 0,
        pageSize: this.initResourceListPageSize,
      });
    }
    super.ngOnInit();
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
