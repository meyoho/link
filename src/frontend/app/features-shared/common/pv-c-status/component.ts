import { Component, Input, OnInit } from '@angular/core';
import { Event } from '~api/backendapi';
import { GenericStatus } from '~api/status';

const ICONS = {
  Available: GenericStatus.Success,
  Bound: GenericStatus.Bound,
  Released: GenericStatus.Warning,
  Failed: GenericStatus.Error,
  Pending: GenericStatus.Pending,
  Lost: GenericStatus.Warning,
};

@Component({
  selector: 'alk-pv-c-status',
  templateUrl: './template.html',
  styles: [``],
})
export class PvCStatusComponent implements OnInit {
  @Input()
  currentStatus:
    | 'Available'
    | 'Bound'
    | 'Released'
    | 'Failed'
    | 'Pending'
    | 'Lost';

  @Input()
  events: Event[] = [];

  warnings: string[];
  tooltipType: string;

  ngOnInit(): void {
    this.warnings = this.events.map(event => event.message);
    this.tooltipType = 'default';
  }

  get iconStatus() {
    if (this.events.length > 0) {
      return GenericStatus.Error;
    }
    return ICONS[this.currentStatus];
  }

  get statusLabel() {
    return `pv_c_${this.currentStatus.toLowerCase()}`;
  }
}
