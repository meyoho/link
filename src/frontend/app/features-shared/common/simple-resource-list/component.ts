import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Resource, ResourceList } from '~api/backendapi';

import {
  BaseResourceListComponent,
  FetchParams,
  TableState,
} from '~app/abstract';
import { DetailParams } from '~app/services';

interface ColumnDef {
  name: string;
  label: string;
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
  },
  {
    name: 'namespace',
    label: 'namespace',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
  {
    name: 'action',
    label: '',
  },
];

interface SimpleResourceList extends ResourceList {
  items: Resource[];
}

@Component({
  selector: 'alk-simple-resource-list',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleResourceListComponent
  extends BaseResourceListComponent<SimpleResourceList, Resource>
  implements OnInit, OnChanges {
  @Input() curResourceParams: DetailParams; // Roll back required current resourceDetail
  @Input() rollBackResourceParams: DetailParams; // Roll back required target resource info

  @Input()
  list: Resource[]; // The user will need to provide all list data

  @Input() actions = [
    'update',
    'update_labels',
    'update_annotations',
    'delete',
  ];

  @Input()
  kind: string; // Use to give zero state an hint

  columnDefs = ALL_COLUMN_DEFS;
  columns = this.columnDefs.map(columnDef => columnDef.name);
  tableState$ = new BehaviorSubject<TableState>({ pageSize: 10 });

  getColumnDefs() {
    return this.columnDefs;
  }

  getTableState$() {
    return this.tableState$;
  }

  setTableState(state: TableState) {
    this.tableState$.next(state);
    return Promise.resolve(true);
  }

  /**
   * For a resource and colume name, returns its associated data.
   */
  getCellValue(item: Resource, colName: string) {
    switch (colName) {
      case 'name':
      case 'namespace':
      case 'creationTimestamp':
      case 'labels':
        return item.objectMeta[colName];
      case 'kind':
        return item.typeMeta[colName];
      default:
        return (item as any)[colName];
    }
  }

  map(value: SimpleResourceList): Resource[] {
    return value.items;
  }

  fetchResources(params: FetchParams): Observable<SimpleResourceList> {
    const list = this.list || [];
    const slicedResource = {
      items: list.slice(
        params.pageParams.pageIndex * params.pageParams.pageSize,
        (params.pageParams.pageIndex + 1) * params.pageParams.pageSize,
      ),
      listMeta: { totalItems: list.length },
    };
    return of(slicedResource);
  }

  ngOnChanges({ list }: SimpleChanges) {
    if (list) {
      this.updated$.next(null);
    }
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
