import { CodeEditorModule } from '@alauda/code-editor';
import {
  AutocompleteModule,
  ButtonModule,
  CardModule,
  CheckboxModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InputModule,
  PaginatorModule,
  SelectModule,
  SortModule,
  StatusBarModule,
  TableModule,
  TooltipModule,
} from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared';
import { TranslateModule } from '../../translate';

import {
  ArrayFormTableComponent,
  ArrayFormTableFooterDirective,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableZeroStateDirective,
} from './array-form-table/component';
import { ConditionListComponent } from './condition-list/component';
import { EventListPageComponent } from './event-list/event-list.component';
import { ExternalEndpointComponent } from './external-endpoint/component';
import { InternalEndpointComponent } from './internal-endpoint/component';
import { KeyValueFormListComponent } from './key-value-form-list/component';
import { KeyValueFormTableComponent } from './key-value-form-table/component';
import { LogViewerComponent } from './log-viewer/component';
import { ModeSelectorComponent } from './mode-selector/component';
import { NamespaceSelectorComponent } from './namespace-selector/component';
import { PodActionMenuComponent } from './pod-action-menu/component';
import { PvCStatusComponent } from './pv-c-status/component';
import { RefPersistentVolumeListComponent } from './ref-pv-list/component';
import { RefPersistentVolumeClaimListComponent } from './ref-pvc-list/component';
import { RefServiceListComponent } from './ref-service-list/component';
import { ResourceAnnotationsFieldComponent } from './resource-annotations-field/component';
import { ResourceLabelsFieldComponent } from './resource-labels-field/component';
import { ResourceListComponent } from './resource-list/component';
import { ResourceYamlEditorComponent } from './resource-yaml-editor/component';
import { RollBackComponent } from './roll-back-to/component';
import { SimpleResourceActionMenuComponent } from './simple-resource-action-menu/component';
import { SimpleResourceCreatePageComponent } from './simple-resource-create-page/component';
import { SimpleResourceListComponent } from './simple-resource-list/component';
import { SimpleResourceUpdatePageComponent } from './simple-resource-update-page/component';
import { StatusGaugeBarComponent } from './status-gauge-bar/component';
import { UpdateAnnotationtsButtonComponent } from './update-annotations-button/component';
import { UpdateLabelsButtonComponent } from './update-labels-button/component';
import { UpdateLabelsComponent } from './update-labels/component';
import { UpdateReplicasComponent } from './update-replicas/component';
import { UsageBarComponent } from './usage-bar/component';
import { UsagePieChartComponent } from './usage-pie-chart/component';

const EXPORTABLES = [
  NamespaceSelectorComponent,
  EventListPageComponent,
  UpdateLabelsComponent,
  UpdateReplicasComponent,
  ResourceYamlEditorComponent,
  SimpleResourceListComponent,
  SimpleResourceActionMenuComponent,
  SimpleResourceUpdatePageComponent,
  SimpleResourceCreatePageComponent,
  ResourceListComponent,
  RefPersistentVolumeClaimListComponent,
  RefPersistentVolumeListComponent,
  RefServiceListComponent,
  ConditionListComponent,
  PodActionMenuComponent,
  InternalEndpointComponent,
  ExternalEndpointComponent,
  PvCStatusComponent,
  LogViewerComponent,
  UsageBarComponent,
  UsagePieChartComponent,
  UpdateAnnotationtsButtonComponent,
  UpdateLabelsButtonComponent,
  ResourceLabelsFieldComponent,
  ResourceAnnotationsFieldComponent,
  StatusGaugeBarComponent,
  ModeSelectorComponent,
  ArrayFormTableComponent,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableFooterDirective,
  ArrayFormTableZeroStateDirective,
  KeyValueFormTableComponent,
  KeyValueFormListComponent,
  RollBackComponent,
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    FormModule,
    PortalModule,
    TranslateModule,
    TableModule,
    StatusBarModule,
    SortModule,
    TooltipModule,
    IconModule,
    InputModule,
    AutocompleteModule,
    PaginatorModule,
    DialogModule,
    ButtonModule,
    CheckboxModule,
    DropdownModule,
    SelectModule,
    MatProgressBarModule,
    FlexLayoutModule,
    CodeEditorModule,
    CardModule,
    ReactiveFormsModule,

    SharedModule,
  ],
  declarations: [...EXPORTABLES],
  exports: [...EXPORTABLES],
  entryComponents: [
    UpdateLabelsComponent,
    UpdateReplicasComponent,
    LogViewerComponent,
    RollBackComponent,
  ],
})
export class FeatureSharedCommonModule {}
