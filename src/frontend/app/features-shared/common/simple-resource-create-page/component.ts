import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { safeLoad } from 'js-yaml';
import { Resource } from '~api/backendapi';

import {
  FeatureStateService,
  HistoryService,
  ResourceDataService,
  YamlSampleService,
} from '../../../services';
import { getDetailRoutes } from '../../../utils';
import { createActions } from '../../../utils/code-editor-config';

@Component({
  selector: 'alk-simple-resource-create-page',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class SimpleResourceCreatePageComponent implements OnInit {
  sample: string;
  namespace: string;
  yaml = '';
  creating = false;
  editorOptions = {
    language: 'yaml',
  };
  actionsConfig = createActions;
  markers: monaco.editor.IMarker[];

  constructor(
    private resourceData: ResourceDataService,
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private history: HistoryService,
    private dialog: DialogService,
    private featureState: FeatureStateService,
    private yamlSample: YamlSampleService,
  ) {}

  ngOnInit() {
    this.sample = this.yamlSample.feedDefaultNamespace(
      this.activatedRoute.snapshot.data.sample,
    );

    this.namespace =
      this.activatedRoute.snapshot.params['namespace'] ||
      this.featureState.get('namespace') ||
      'default';
  }

  async onCreate() {
    this.creating = true;
    try {
      const yaml = this.yaml;
      await this.resourceData.create(yaml);
      // TODO:
      // this.router.navigate(this.getRoutesFromYaml(yaml));
      this.back();
    } catch (err) {
      // ...?
    }
    this.creating = false;
    this.cdr.markForCheck();
  }

  back() {
    this.history.back(['..'], this.activatedRoute);
  }

  showSampleDialog(template: TemplateRef<any>) {
    this.dialog.open(template, {
      size: DialogSize.Big,
    });
  }

  onModelChange(value: string) {
    this.yaml = value;

    // Following is a test of getting error markers
    setTimeout(() => {
      // TODO: add resource URI
      this.markers = monaco.editor.getModelMarkers({});
      this.cdr.markForCheck();
    }, 1000);
  }

  getRoutesFromYaml(yaml: string) {
    const obj: any = safeLoad(yaml);
    const resource: Resource = {
      typeMeta: {
        kind: obj.kind,
        apiVersion: obj.apiVersion,
      },
      objectMeta: {
        name: obj.metadata.name,
        namespace: obj.metadata.namespace,
      },
    };
    return getDetailRoutes(resource);
  }
}
