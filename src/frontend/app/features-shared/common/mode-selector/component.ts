import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'alk-mode-selector',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class ModeSelectorComponent {
  @Input()
  selected: string;
  @Input()
  modes: string[];
  @Output()
  modeClick = new EventEmitter();

  modeClicked(mode: string) {
    this.modeClick.emit(mode);
  }

  get selectedIndex() {
    return this.modes.indexOf(this.selected);
  }
}
