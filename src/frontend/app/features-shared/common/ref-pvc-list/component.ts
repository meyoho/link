import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

import { PersistentVolumeClaim, Resource } from '~api/backendapi';

interface ColumnDef {
  name: string;
  label?: string;
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
  },
  {
    name: 'namespace',
    label: 'namespace',
  },
  {
    name: 'status',
    label: 'status',
  },
  {
    name: 'volume',
    label: 'related_pv',
  },
  {
    name: 'capacity',
    label: 'capacity',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-ref-pvc-list',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RefPersistentVolumeClaimListComponent implements OnInit {
  @Input()
  list: PersistentVolumeClaim[];

  @Input()
  kind: string; // Use to give zero state an hint

  @Input()
  showNamespace: boolean;

  columnDefs: ColumnDef[];
  columns: string[];

  constructor() {}

  ngOnInit() {
    this.columnDefs = ALL_COLUMN_DEFS.filter(col => {
      const shouldHide = !this.showNamespace && col.name === 'namespace';
      return !shouldHide;
    });

    this.columns = this.columnDefs.map(columnDef => columnDef.name);
  }

  getPersistentVolumeResource(volumeName: string) {
    if (volumeName) {
      const volume: Resource = {
        typeMeta: {
          apiVersion: 'v1',
          kind: 'PersistentVolume',
        },
        objectMeta: {
          name: volumeName,
        },
      };
      return volume;
    } else {
      return undefined;
    }
  }

  getCellValue(item: PersistentVolumeClaim, colName: string) {
    switch (colName) {
      case 'status':
        return item.status;
      case 'volume':
        return item.volume;
      case 'capacity':
        return item.capacity.storage;
      case 'name':
      case 'namespace':
      case 'creationTimestamp':
      case 'labels':
        return item.objectMeta[colName];
      case 'kind':
        return item.typeMeta[colName];
      default:
        return (item as any)[colName];
    }
  }
}
