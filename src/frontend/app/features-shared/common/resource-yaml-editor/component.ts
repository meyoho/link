import { MessageService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { safeDump, safeLoad } from 'js-yaml';
import { first } from 'rxjs/operators';
import { OtherResourceDetail } from '~api/backendapi';

import { OthersDataService } from '../../../services';
import { TranslateService } from '../../../translate';
import {
  compareActions,
  updateActions,
  viewActions,
} from '../../../utils/code-editor-config';

export enum EditorModes {
  View = 'view',
  Update = 'update',
  Compare = 'compare',
}

@Component({
  selector: 'alk-resource-yaml-editor',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceYamlEditorComponent implements OnInit, OnChanges {
  EditorModes = EditorModes;

  editorOptions: monaco.editor.IEditorConstructionOptions = {
    language: 'yaml',
    readOnly: true,
    renderLineHighlight: 'none',
  };

  editorOptionsUpdate: monaco.editor.IEditorConstructionOptions = {
    ...this.editorOptions,
    readOnly: false,
    renderLineHighlight: 'all',
  };

  actionsConfigView = viewActions;
  actionsConfigUpdate = updateActions;
  actionsConfigCompare = compareActions;

  // The original model
  yaml = '';

  // The model for user edits.
  yamlModel = '';

  // Resource is updating
  updating = false;

  /**
   * Modes for the editor.
   */
  @Input()
  mode: EditorModes = EditorModes.View;

  @Output()
  modeChange = new EventEmitter<EditorModes>();

  /**
   * User should provide a valid resource detail object.
   */
  @Input()
  resourceDetail: OtherResourceDetail;

  @Input()
  newData: OtherResourceDetail;

  /**
   * Updated event
   */
  @Output()
  updated = new EventEmitter();

  constructor(
    private othersData: OthersDataService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {}

  ngOnChanges({ resourceDetail }: SimpleChanges): void {
    if (
      !this.newData &&
      resourceDetail &&
      this.resourceDetail &&
      this.resourceDetail.data
    ) {
      this.yaml = safeDump(this.resourceDetail.data, { lineWidth: 9999 });
      this.yamlModel = this.yamlModel || this.yaml;
    } else if (this.newData && resourceDetail && this.resourceDetail) {
      this.yaml = safeDump(this.resourceDetail.data.spec, { lineWidth: 9999 });
      this.yamlModel = safeDump(this.newData.data.spec, { lineWidth: 9999 });
    }
  }

  async updateResource() {
    try {
      const updatedResource = safeLoad(this.yamlModel);
      this.updating = true;
      const params = this.othersData.getResourceDetailParams(
        this.resourceDetail,
      );
      const res = await this.othersData
        .updateDetail(params, updatedResource)
        .pipe(first())
        .toPromise();
      this.auiMessageService.success({
        content: this.translate.get('update_succeeded'),
      });
      this.updated.emit(res);
      this.changeMode(EditorModes.View);
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
    this.updating = false;
    this.cdr.markForCheck();
  }

  changeMode(mode: EditorModes) {
    this.mode = mode;
    this.modeChange.next(mode);
  }
}
