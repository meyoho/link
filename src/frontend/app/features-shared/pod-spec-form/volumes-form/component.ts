import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { safeDump } from 'js-yaml';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Volume } from '~api/raw-k8s';

import { TranslateService } from '../../../translate';
import { VolumeFormDialogComponent } from '../volume-form-dialog/component';
import { AVAILABLE_VOLUME_TYPES } from '../volumes';

@Component({
  selector: 'alk-volumes-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumesFormComponent extends BaseResourceFormArrayComponent<
  Volume
> {
  @Input()
  namespace: string;

  constructor(
    private dialog: DialogService,
    private translate: TranslateService,
    injector: Injector,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): Volume[] {
    return [];
  }

  getVolumeType(volume: any) {
    return Object.keys(volume).find(key => key !== 'name');
  }

  getVolumeAttributes(volume: any): any {
    return volume[this.getVolumeType(volume)];
  }

  getYaml(json: any) {
    return safeDump(json).trim();
  }

  async addVolume() {
    const dialogRef = this.dialog.open(VolumeFormDialogComponent, {
      size: DialogSize.Big,
    });
    dialogRef.componentInstance.title = this.translate.get(
      'pod_spec_form.add_volume',
    );
    dialogRef.componentInstance.namespace = this.namespace;

    try {
      const newVolume = await dialogRef.afterClosed().toPromise();
      if (newVolume) {
        const control = this.fb.control(newVolume);
        this.form.push(control);
      }
    } catch (err) {}
    this.cdr.markForCheck();
  }

  async editVolume(index: number) {
    const dialogRef = this.dialog.open(VolumeFormDialogComponent, {
      size: DialogSize.Big,
    });
    dialogRef.componentInstance.title = this.translate.get(
      'pod_spec_form.update_volume',
    );
    dialogRef.componentInstance.volume = this.form.controls[index].value;
    dialogRef.componentInstance.namespace = this.namespace;

    try {
      const newVolume = await dialogRef.afterClosed().toPromise();
      if (newVolume) {
        this.form.get([index]).setValue(newVolume);
      }
    } catch (err) {}

    this.cdr.markForCheck();
  }

  allowEdit(volume: Volume) {
    return AVAILABLE_VOLUME_TYPES.includes(this.getVolumeType(volume));
  }
}
