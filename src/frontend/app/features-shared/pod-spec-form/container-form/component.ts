import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Container } from '~api/raw-k8s';

import { VolumesFormComponent } from '../volumes-form/component';

const DEFAULT_CONTAINER: Container = {
  name: '',
  image: '',
};

@Component({
  selector: 'alk-container-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerFormComponent extends BaseResourceFormGroupComponent<
  Container
> {
  @Input()
  namespace: string;

  @Input()
  updateMode: boolean;

  @Input()
  volumesForm: VolumesFormComponent;

  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(/^[a-z0-9^-]*[a-z0-9-]*[a-z0-9]$/),
      ]),
      image: [],
      volumeMounts: [],
      env: [],
      envFrom: [],
      args: [],
      command: [],
      resources: {},
    });
  }

  getDefaultFormModel() {
    return DEFAULT_CONTAINER;
  }
}
