import { OptionComponent } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidatorFn,
} from '@angular/forms';
import { safeDump } from 'js-yaml';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { BehaviorSubject, Observable, of } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';
import { ConfigMap, ResourceList, Secret } from '~api/backendapi';
import { EnvVar } from '~api/raw-k8s';

import { TranslateService } from '../../../translate';
import {
  ENV_VAR_SOURCE_TYPE_TO_KIND,
  KIND_TO_ENV_VAR_SOURCE_TYPE,
  SupportedEnvVarSourceKind,
  SupportedEnvVarSourceType,
  getEnvVarSource,
  getEnvVarSourceType,
  isEnvVarSourceMode,
  isEnvVarSourceSupported,
} from '../../utils/env-var';

interface EnvRefObj {
  kind: SupportedEnvVarSourceKind; // One of Secret, configMap
  name: string;
  apiVersion?: string;
  fieldPath?: string;
  containerName?: string;
  resource?: string;
}

interface EnvVarFormModel extends EnvVar {
  refObj?: EnvRefObj;
  refObjKey?: string;
}

@Component({
  selector: 'alk-env-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvFormComponent
  extends BaseResourceFormArrayComponent<EnvVar, EnvVarFormModel>
  implements OnInit {
  @Input()
  set namespace(namespace: string) {
    this.namespaceChanged.next(namespace);
  }
  @Input() containerName = '';

  configMaps$: Observable<ConfigMap[]>;
  secrets$: Observable<Secret[]>;

  private namespaceChanged = new BehaviorSubject<string>(this.namespace);

  constructor(
    private httpClient: HttpClient,
    private translate: TranslateService,
    injector: Injector,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    this.configMaps$ = this.namespaceChanged.pipe(
      switchMap(namespace =>
        this.getNamespaceResources$(namespace, 'configMap'),
      ),
      map((list: any) => list.items),
      publishReplay(1),
      refCount(),
    );

    this.secrets$ = this.namespaceChanged.pipe(
      switchMap(namespace => this.getNamespaceResources$(namespace, 'secret')),
      map((list: any) => list.secrets),
      publishReplay(1),
      refCount(),
    );
  }

  valueFromModeChange(refObjKey: AbstractControl) {
    refObjKey.setValue('');
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [{ name: '', value: '' }];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  adaptResourceModel(envVars: EnvVar[]) {
    if (!envVars || envVars.length === 0) {
      envVars = this.getDefaultFormModel();
    }

    // Fill in keyRefObj when applied:
    return envVars.map((envVar: EnvVarFormModel) => {
      if (isEnvVarSourceSupported(envVar)) {
        const kind =
          ENV_VAR_SOURCE_TYPE_TO_KIND[getEnvVarSourceType(envVar.valueFrom)];
        const envVarSource: any = getEnvVarSource(envVar);
        let refObj;
        let refObjKey;
        switch (kind) {
          case 'FieldRef':
            refObj = {
              kind,
              name: kind,
              apiVersion: envVarSource.apiVersion || 'v1',
              fieldPath: envVarSource.fieldPath,
            };
            refObjKey = envVarSource.fieldPath;
            break;
          case 'ResourceFieldRef':
            refObj = {
              kind,
              name: kind,
              containerName: envVarSource.containerName || this.containerName,
              resource: envVarSource.resource,
            };
            refObjKey = envVarSource.resource;
            break;

          default:
            refObj = {
              kind,
              name: envVarSource.name,
            };
            refObjKey = envVarSource.key;
            break;
        }
        envVar = {
          ...envVar,
          refObj,
          refObjKey,
        };
      }

      return envVar;
    });
  }

  adaptFormModel(envVars: EnvVarFormModel[]): EnvVar[] {
    if (envVars) {
      envVars = envVars.filter(
        ({ name, value, valueFrom }) => name || value || valueFrom,
      );
    }

    return envVars.map(envVar => {
      if (envVar.refObj && envVar.refObj.kind) {
        const refKind: SupportedEnvVarSourceType =
          KIND_TO_ENV_VAR_SOURCE_TYPE[envVar.refObj.kind];
        let envVarSource: any;
        switch (refKind) {
          case 'fieldRef':
            envVarSource = {
              [refKind]: {
                apiVersion: 'v1',
                fieldPath: envVar.refObjKey,
              },
            };
            break;
          case 'resourceFieldRef':
            envVarSource = {
              [refKind]: {
                containerName: this.containerName,
                resource: envVar.refObjKey,
              },
            };
            break;

          default:
            envVarSource = {
              [refKind]: {
                name: envVar.refObj.name,
                key: envVar.refObjKey,
              },
            };
            break;
        }
        return {
          name: envVar.name,
          valueFrom: envVarSource,
        };
      } else {
        return {
          name: envVar.name,
          value: envVar.value,
        };
      }
    });
  }

  getYaml(json: any) {
    return safeDump(json).trim();
  }

  getRefObj(obj: ConfigMap | Secret): EnvRefObj {
    return {
      name: obj.objectMeta.name,
      kind: obj.typeMeta.kind as SupportedEnvVarSourceKind,
    };
  }

  trackByFn = (index: number, control: FormControl) => {
    return `${index}.${this.envVarViewMode(control.value)}`;
  };

  envVarViewMode(envVar: EnvVar): 'value' | 'valueFrom' | 'yaml' {
    if (!isEnvVarSourceMode(envVar)) {
      return 'value';
    } else if (isEnvVarSourceSupported(envVar)) {
      return 'valueFrom';
    } else {
      return 'yaml';
    }
  }

  // Overwrite add so that we could have different types of controls
  add(index = this.length, withRef = false) {
    const control = this.createNewControl();
    if (withRef) {
      control.get('valueFrom').reset({});
    }
    this.form.insert(index, control);
    this.cdr.markForCheck();
  }

  refObjTrackByFn = (refObj: EnvRefObj) => {
    let str;
    if (refObj && refObj.kind) {
      if (refObj.kind === 'FieldRef' || refObj.kind === 'ResourceFieldRef') {
        str = 'Downward API: ' + refObj.kind;
      } else {
        str =
          this.translate.get(refObj.kind.toLowerCase()) + ': ' + refObj.name;
      }
    } else {
      str = '';
    }
    return str;
  };

  refObjFilterFn = (filterString: string, option: OptionComponent) => {
    return option.value.name.includes(filterString);
  };

  // FIXME: use member variable instead since this function returns new observable
  // per call.
  // FIXME: should reset selected key to null after reselect a new refobj.
  getRefObjKeys(control: FormGroup): Observable<string[]> {
    const refObjControl = control.get('refObj');
    return refObjControl.valueChanges.pipe(
      startWith(refObjControl.value),
      switchMap(refObj => {
        let objs$;
        let objsArr;
        switch (refObj.kind) {
          case 'Secret':
            objs$ = this.secrets$;
            break;
          case 'ConfigMap':
            objs$ = this.configMaps$;
            break;
          case 'FieldRef':
            objsArr = [
              'metadata.name',
              'metadata.namespace',
              'spec.nodeName',
              'spec.serviceAccountName',
              'status.hostIP',
              'status.podIP',
            ];
            break;
          case 'ResourceFieldRef':
            objsArr = [
              'limits.cpu',
              'limits.memory',
              'limits.ephemeral-storage',
              'requests.cpu',
              'requests.memory',
              'requests.ephemeral-storage',
            ];
            break;
        }
        return refObj.kind === 'Secret' || refObj.kind === 'ConfigMap'
          ? objs$.pipe(
              map(objs => {
                const selectObj = objs.find(
                  obj => obj.objectMeta.name === refObj.name,
                );
                return selectObj ? selectObj.keys : [];
              }),
            )
          : of(objsArr);
      }),
      tap(keys => {
        const keyControl = control.get('refObjKey');
        const enableKeyControl = keys && keys.length > 0;
        if (enableKeyControl) {
          keyControl.enable({ emitEvent: false });
        } else {
          keyControl.disable({ emitEvent: false });
        }
      }),
    );
  }

  rowBackgroundColorFn(row: FormControl) {
    if (row.invalid) {
      return '#fdefef';
    } else {
      return '';
    }
  }

  private getPreviousKeys(index: number) {
    return this.formModel
      .slice(0, index)
      .map(({ name }) => name)
      .filter(name => !!name);
  }

  private getNamespaceResources$(namespace: string, resourceName: string) {
    return this.httpClient
      .get<ResourceList>(
        `api/v1/${resourceName.toLocaleLowerCase()}s/${namespace || ''}`,
      )
      .pipe(
        publishReplay(1),
        refCount(),
      );
  }

  private createNewControl() {
    const missingKeyValidator: ValidatorFn = control => {
      const { name, value, refObj } = control.value;

      if (!name && (value || (refObj && refObj.name))) {
        return {
          keyIsMissing: true,
        };
      } else {
        return null;
      }
    };

    const duplicateKeyValidator: ValidatorFn = control => {
      const index = this.form.controls.indexOf(control);
      const previousKeys = this.getPreviousKeys(index);

      const { name } = control.value;

      if (previousKeys.includes(name)) {
        return {
          duplicateKey: name,
        };
      } else {
        return null;
      }
    };

    const keyPatternValidator: ValidatorFn = control => {
      const pattern = /^[a-zA-Z_-][a-zA-Z0-9._-]*$/;
      const { name } = control.value;

      if (name && !name.match(pattern)) {
        return {
          patternError: true,
        };
      } else {
        return null;
      }
    };

    return this.fb.group(
      {
        name: [],
        value: [],
        valueFrom: [],

        // The followings are view only controls and will be filtered out later
        refObj: [{}],
        refObjKey: [],
      },
      {
        validator: [
          missingKeyValidator,
          duplicateKeyValidator,
          keyPatternValidator,
        ],
      },
    );
  }
}
