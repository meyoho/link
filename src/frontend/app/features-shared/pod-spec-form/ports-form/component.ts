import { Component, Injector } from '@angular/core';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { OnFormArrayResizeFn } from '../../../utils';

@Component({
  selector: 'alk-ports-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class PortsFormComponent extends BaseResourceFormArrayComponent {
  protocols = ['TCP', 'UDP'];

  constructor(injector: Injector) {
    super(injector);
  }

  getOnFormArrayResizeFn(): OnFormArrayResizeFn {
    return () => this.createNewPortControl();
  }

  createNewPortControl() {
    return this.fb.group({
      name: [''],
      containerPort: [''],
      protocol: [this.protocols[0]],
    });
  }
}
