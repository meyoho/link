import { FormModule, SwitchModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../shared';
import { TranslateService } from '../../translate';
import { FeatureSharedCommonModule } from '../common/module';

import { PodSpecFormComponent } from './component';
import { ContainerFormComponent } from './container-form/component';
import { EnvFromFormComponent } from './env-from-form/component';
import { EnvFormComponent } from './envs-form/component';
import i18n from './i18n';
import { KeyToPathItemsFormComponent } from './key-to-path-items-form/component';
import { PortsFormComponent } from './ports-form/component';
import { ResourcesFormComponent } from './resources-form/component';
import { VolumeFormDialogComponent } from './volume-form-dialog/component';
import { VolumeMountsComponent } from './volume-mounts/component';
import { VolumesFormComponent } from './volumes-form/component';

const COMPONENTS = [
  ContainerFormComponent,
  VolumesFormComponent,
  VolumeMountsComponent,
  EnvFormComponent,
  EnvFromFormComponent,
  ResourcesFormComponent,
  PortsFormComponent,
  PodSpecFormComponent,
  VolumeFormDialogComponent,
  KeyToPathItemsFormComponent,
];

const MODULE_PREFIX = 'pod_spec_form';

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormModule,
    SwitchModule,
    SharedModule,
    FeatureSharedCommonModule,
  ],
  exports: [...COMPONENTS],
  providers: [],
  entryComponents: [VolumeFormDialogComponent],
})
export class PodSpecFormModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations(MODULE_PREFIX, i18n);
  }
}
