export const en = {
  volume_mounts: 'Volume Mounts',
  related_attrs: 'Attributes',
  add_volume: 'Add Volume',
  add_container: 'Add Container',
  update_volume: 'Update Volume',
  mount_path: 'Mount Path',
  sub_path: 'Sub Path',
  add_mount_path: 'Add Mount Path',
  no_container_name_hint: 'Unnamed',
  mount_path_no_volumes_hint:
    'Please add a volume first before adding volume mount',
  service_account_name: 'Service Account Name',
  service_account_name_help_info: 'Service Account to access pods',
  image_pull_secrets_help_info:
    'Credential information used to access the docker repository.',
  container_resources_help_info: `The "Requests" is used to allocate the cluster resources
    , when the resources are not adequately allocated (the nodes in the cluster do not have the required resources by the "Requests")
    , the container fails to be created
    ; The "Limits" is used to set the maximum upper limit of
     the resources used by the container to avoid exceptions in case of excessive resource consumption.`,
  env_name_help_info: 'Start with letters, accept a-z, A-Z, 0-9, ., _, -',
  commands_help_info:
    'Input command to control the operation of the container.',
  args_help_info: 'Input parameters for passing commands to the container.',
  volume_mounts_help_info:
    'The path where the volume is mounted to the container',

  hostPath: 'Host Path',
  configMap: 'Config Map',
  emptyDir: 'Empty Directory',
  persistentVolumeClaim: 'PVC',
  secret: 'Secret',
  key_to_path: 'Key to Path Mappings',
  key_to_path_enabled: 'Key to Path Mappings Enabled',
  volume_name: 'Volume Name',

  container_resources: 'Container Resources',

  requests: 'Requests',
  limits: 'Limits',
};

export const zh = {
  volume_mounts: '存储卷挂载',
  related_attrs: '相关配置',
  add_volume: '添加存储卷',
  update_volume: '更新存储卷',
  mount_path: '挂载路径',
  sub_path: '子路径',
  add_mount_path: '添加挂载路径',
  service_account_name: '账号凭证',
  service_account_name_help_info: '用于访问容器组下进程的账号凭证信息',
  image_pull_secrets_help_info: '用于访问镜像仓库的凭证信息',
  container_resources_help_info:
    '“请求值”用于集群分配资源，当集群中的节点没有“请求值”所要求的资源数量时，容器会创建失败；“限制值”用于设置容器使用资源的最大上限值，避免异常情况下资源消耗过多',
  env_name_help_info: '不以 0-9 开头，支持使用 a-z, A-Z, 0-9, .,  _, - ',
  commands_help_info: '用于控制容器运行的输入命令',
  args_help_info: '用于传递给容器执行命令的输入参数',
  volume_mounts_help_info: '存储卷挂载到容器中的路径',

  no_container_name_hint: '未命名',
  mount_path_no_volumes_hint: '添加挂载路径需要有效的存储卷',
  key_to_path: '引用配置项',
  key_to_path_enabled: '配置项',
  add_container: '添加容器',
  volume_name: '存储卷名称',

  hostPath: '主机路径',
  configMap: '配置字典',
  secret: '保密字典',
  emptyDir: '空目录',
  persistentVolumeClaim: '持久卷声明',

  container_resources: '容器大小',

  requests: '请求值',
  limits: '限制值',
};

export default {
  en,
  zh,
};
