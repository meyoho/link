import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { KeyToPath } from '~api/raw-k8s';

@Component({
  selector: 'alk-key-to-path-items-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyToPathItemsFormComponent extends BaseResourceFormArrayComponent<
  KeyToPath
> {
  @Input()
  availableKeys: string[];

  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): KeyToPath[] {
    return [
      {
        key: '',
        path: '',
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group(
      {
        key: [''],
        path: [''],
      },
      {
        validator: [
          (control: AbstractControl) => {
            const { key, path } = control.value;
            return (!key && !path) || (key && path) ? null : { error: true };
          },
        ],
      },
    );
  }
}
