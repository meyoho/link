import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Subscription } from 'rxjs';
import { VolumeMount } from '~api/raw-k8s';

import { VolumesFormComponent } from '../volumes-form/component';

@Component({
  selector: 'alk-volume-mounts-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VolumeMountsComponent
  extends BaseResourceFormArrayComponent<VolumeMount>
  implements OnInit, OnDestroy {
  @Input()
  volumesForm: VolumesFormComponent;

  private volumesFormSub = Subscription.EMPTY;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    if (this.volumesForm) {
      this.volumesFormSub = this.volumesForm.form.valueChanges.subscribe(() => {
        this.cdr.markForCheck();
      });
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.volumesFormSub.unsubscribe();
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): VolumeMount[] {
    return [];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewMountControl();
  }

  add(index = this.length) {
    if (!this.noVolumes) {
      super.add(index);
    }
  }

  get volumes() {
    return this.volumesForm && this.volumesForm.formModel;
  }

  get noVolumes() {
    return !this.volumes || this.volumes.length === 0;
  }

  private createNewMountControl() {
    const defaultName =
      this.volumes && this.volumes.length && this.volumes[0].name;
    return this.fb.group({
      name: [defaultName],
      mountPath: [''],
      subPath: [''],
      readOnly: [false],
    });
  }
}
