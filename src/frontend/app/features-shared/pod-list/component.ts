import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Pod, PodList } from '~api/backendapi';
import {
  BaseResourceListComponent,
  ColumnDef,
  FetchParams,
  TableState,
} from '~app/abstract';

import { filterBy, getQuery, pageBy, sortBy } from '../../utils';

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
  },
  {
    name: 'namespace',
    label: 'namespace',
    sortable: true,
  },
  {
    name: 'status',
    label: 'status',
    sortable: true,
  },
  {
    name: 'metrics',
    label: 'used_resources',
  },
  {
    name: 'restarts',
    label: 'restarts',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
    sortable: true,
    sortStart: 'desc',
  },
  {
    name: 'action',
  },
];

@Component({
  selector: 'alk-pod-list',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodListComponent extends BaseResourceListComponent<PodList, Pod>
  implements OnInit {
  @Input()
  endpoint: string;

  @Input()
  showNamespace: boolean;

  tableState$ = new BehaviorSubject<TableState>({});
  initialized = false;

  map(value: PodList): Pod[] {
    return value.pods;
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS.filter(col => {
      const shouldHide = !this.showNamespace && col.name === 'namespace';
      return !shouldHide;
    });
  }

  getTableState$(): Observable<TableState> {
    return this.tableState$;
  }

  fetchResources(params: FetchParams): Observable<PodList | Error> {
    return this.http.get<PodList>(this.endpoint, {
      params: getQuery(
        filterBy('name', params.search),
        sortBy(params.sort.active, params.sort.direction === 'desc'),
        pageBy(params.pageParams.pageIndex, params.pageParams.pageSize),
      ),
    });
  }

  setTableState(newState: TableState): Promise<boolean> {
    this.tableState$.next(newState);
    return Promise.resolve(true);
  }

  getCellValue(item: Pod, colName: string) {
    switch (colName) {
      case 'restarts':
        return item.restartCount;
      default:
        return super.getCellValue(item, colName);
    }
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
