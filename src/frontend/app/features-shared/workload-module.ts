import { NgModule } from '@angular/core';

import { FeatureSharedCommonModule } from './common/module';
import { MetricsModule } from './metrics/module';
import { PodEnvModule } from './pod-env/module';
import { PodListModule } from './pod-list/module';
import { PodSpecFormModule } from './pod-spec-form/module';
import { PodStatusModule } from './pod-status/module';

const EXPORTABLES = [
  MetricsModule,
  PodListModule,
  PodSpecFormModule,
  PodEnvModule,
  PodStatusModule,
  FeatureSharedCommonModule,
];

// Workload 相关的Shared Module
@NgModule({
  declarations: [],
  imports: [...EXPORTABLES],
  exports: [...EXPORTABLES],
  providers: [],
})
export class FeaturesSharedWorkloadModule {}
