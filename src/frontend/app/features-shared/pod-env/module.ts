import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared';
import { PodSpecFormModule } from '../pod-spec-form/module';

import { EnvFromDialogComponent } from './env-from-dialog/component';
import { EnvDialogComponent } from './env-var-dialog/component';
import { PodEnvListComponent } from './list/component';

@NgModule({
  declarations: [
    PodEnvListComponent,
    EnvFromDialogComponent,
    EnvDialogComponent,
  ],
  imports: [CommonModule, RouterModule, SharedModule, PodSpecFormModule],
  exports: [PodEnvListComponent],
  providers: [],
  entryComponents: [EnvFromDialogComponent, EnvDialogComponent],
})
export class PodEnvModule {}
