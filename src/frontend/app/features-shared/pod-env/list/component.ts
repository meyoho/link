import { DialogService, DialogSize } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { safeDump } from 'js-yaml';
import { cloneDeep } from 'lodash';
import { Container, EnvFromSource, EnvVar, PodSpec } from '~api/raw-k8s';

import { getEnvFromSource, getEnvFromSourceKind } from '../../utils/env-from';
import {
  getEnvVarSource,
  getEnvVarSourceKind,
  isEnvVarSourceMode,
  isEnvVarSourceSupported,
} from '../../utils/env-var';
import { EnvFromDialogComponent } from '../env-from-dialog/component';
import { EnvDialogComponent } from '../env-var-dialog/component';

@Component({
  selector: 'alk-pod-env-list',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class PodEnvListComponent implements OnInit, OnChanges {
  @Input()
  spec: PodSpec;

  @Input()
  namespace: string;

  @Input()
  noUpdate: boolean;

  @Output()
  update = new EventEmitter<PodSpec>();

  readonly envListColumnDefs = ['name', 'config_value'];

  containers: Container[] = [];

  constructor(private dialog: DialogService) {}

  ngOnInit(): void {}

  ngOnChanges({ spec }: SimpleChanges) {
    if (spec && this.spec) {
      this.containers = this.spec.containers || [];
    }
  }

  envVarViewMode(envVar: EnvVar): 'value' | 'valueFrom' | 'yaml' {
    if (!isEnvVarSourceMode(envVar)) {
      return 'value';
    } else if (isEnvVarSourceSupported(envVar)) {
      return 'valueFrom';
    } else {
      return 'yaml';
    }
  }

  envVarResource(envVar: EnvVar) {
    const source = getEnvVarSource(envVar);
    const kind = getEnvVarSourceKind(envVar.valueFrom);
    return kind === 'ResourceFieldRef' || kind === 'FieldRef'
      ? { ...source, kind }
      : {
          kind,
          namespace: this.namespace,
          name: source.name,
          key: source.key,
        };
  }

  envFromResource(
    envFrom: EnvFromSource,
  ): { kind: string; name: string; namespace: string } {
    const source = getEnvFromSource(envFrom);

    return {
      kind: getEnvFromSourceKind(envFrom),
      name: source.name,
      namespace: this.namespace,
    };
  }

  async editEnvFrom(container: Container, index: number) {
    const newEnvFrom = await this.dialog
      .open(EnvFromDialogComponent, {
        data: {
          envFrom: cloneDeep(container.envFrom || []),
          namespace: this.namespace,
        },
      })
      .afterClosed()
      .toPromise();

    if (newEnvFrom) {
      const newContainer = cloneDeep(container);
      newContainer.envFrom = newEnvFrom;

      this.onContainerUpdate(newContainer, index);
    }
  }

  async editEnv(container: Container, index: number) {
    const newEnv = await this.dialog
      .open(EnvDialogComponent, {
        data: {
          env: cloneDeep(container.env || []),
          namespace: this.namespace,
          containerName: container.name,
        },
        size: DialogSize.Large,
      })
      .afterClosed()
      .toPromise();

    if (newEnv) {
      const newContainer = cloneDeep(container);
      newContainer.env = newEnv;

      this.onContainerUpdate(newContainer, index);
    }
  }

  getYaml(json: any) {
    return safeDump(json).trim();
  }

  private onContainerUpdate(container: Container, index: number) {
    const newSpec = cloneDeep(this.spec);
    newSpec.containers[index] = container;
    this.update.emit(newSpec);
  }
}
