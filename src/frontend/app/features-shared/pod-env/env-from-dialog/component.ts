import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { EnvFromSource } from '~api/raw-k8s';

@Component({
  templateUrl: './template.html',
})
export class EnvFromDialogComponent implements OnInit {
  envFrom: EnvFromSource[] = [];
  namespace = '';

  constructor(
    @Inject(DIALOG_DATA)
    public data: { envFrom: EnvFromSource[]; namespace: string },
    private dialogRef: DialogRef,
  ) {}

  ngOnInit(): void {
    if (this.data) {
      const { envFrom, namespace } = this.data;
      this.envFrom = envFrom;
      this.namespace = namespace;
    }
  }

  onConfirm() {
    this.dialogRef.close(this.envFrom);
  }
}
