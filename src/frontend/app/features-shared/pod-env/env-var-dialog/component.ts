import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { EnvVar } from '~api/raw-k8s';

@Component({
  templateUrl: './template.html',
})
export class EnvDialogComponent implements OnInit {
  env: EnvVar[] = [];
  namespace = '';
  containerName = '';

  constructor(
    @Inject(DIALOG_DATA)
    public data: { env: EnvVar[]; namespace: string; containerName: string },
    private dialogRef: DialogRef,
  ) {}

  ngOnInit(): void {
    if (this.data) {
      const { env, namespace, containerName } = this.data;
      this.env = env;
      this.namespace = namespace;
      this.containerName = containerName;
    }
  }

  onConfirm() {
    this.dialogRef.close(this.env);
  }
}
