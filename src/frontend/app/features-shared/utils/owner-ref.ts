import { Resource } from '~api/backendapi';
import { KubernetesResource, OwnerReference } from '~api/raw-k8s';

export function getOwnerResource(kobj: KubernetesResource): Resource {
  if (kobj && kobj.metadata.ownerReferences) {
    const controller = kobj.metadata.ownerReferences.find(
      (ownerRef: OwnerReference) => ownerRef.controller,
    );
    if (controller) {
      const resource = {
        typeMeta: {
          kind: controller.kind,
          apiVersion: controller.apiVersion,
        },
        objectMeta: {
          name: controller.name,
          namespace: kobj.metadata.namespace,
          uid: controller.uid,
        },
      };

      return resource;
    }
  }
}
