import { PodControllerInfo, PodInfoItem } from '~api/backendapi';
import { PodStatusEnum } from '~api/raw-k8s';
import { GenericStatus } from '~api/status';

export const POD_STATUSES = [
  PodStatusEnum.Completed,
  PodStatusEnum.Running,
  PodStatusEnum.Pending,
  PodStatusEnum.Failed,
];

export function getPodStatusIcon(status: PodStatusEnum) {
  if (status.split(':')[0] === 'Initing') {
    status = PodStatusEnum.Initing;
  }
  switch (status) {
    case PodStatusEnum.Completed:
      return GenericStatus.Success;
    case PodStatusEnum.Succeeded:
      return GenericStatus.Success;
    case PodStatusEnum.Running:
      return GenericStatus.Running;
    case PodStatusEnum.Pending:
      return GenericStatus.Pending;
    case PodStatusEnum.Initing:
      return GenericStatus.Pending;
    case PodStatusEnum.ContainerCreating:
      return GenericStatus.Pending;
    case PodStatusEnum.PodInitializing:
      return GenericStatus.Pending;
    case PodStatusEnum.Terminating:
      return GenericStatus.Pending;
    default:
      return GenericStatus.Error;
  }
}

export function getPodStatusToPod(
  podControllerInfo: PodControllerInfo,
): { [key: string]: PodInfoItem[] } {
  // Init status map first
  const podStatusToPod: {
    [key: string]: PodInfoItem[];
  } = POD_STATUSES.reduce((accum, status) => ({ ...accum, [status]: [] }), {});
  if (podControllerInfo && podControllerInfo.pods) {
    podControllerInfo.pods.forEach(podInfoItem =>
      podStatusToPod[podInfoItem.status].push(podInfoItem),
    );
  }
  return podStatusToPod;
}
