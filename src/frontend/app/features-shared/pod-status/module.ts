import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared';

import { PodControllerInfoIconsComponent } from './pod-controller-info-icons/component';
import { PodControllerStatusComponent } from './pod-controller-status/component';
import { PodStatusComponent } from './pod-status/component';

const EXPORTABLES = [
  PodStatusComponent,
  PodControllerStatusComponent,
  PodControllerInfoIconsComponent,
];

@NgModule({
  declarations: [...EXPORTABLES],
  imports: [CommonModule, SharedModule],
  exports: [...EXPORTABLES],
  providers: [],
})
export class PodStatusModule {}
