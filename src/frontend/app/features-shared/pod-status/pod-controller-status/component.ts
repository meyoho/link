import { Status } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { reduce } from 'lodash';
import { PodControllerInfo, PodInfoItem } from '~api/backendapi';
import { PodStatusEnum } from '~api/raw-k8s';
import { GenericStatus } from '~api/status';

import {
  POD_STATUSES,
  getPodStatusIcon,
  getPodStatusToPod,
} from '../../utils/pod-status';

@Component({
  selector: 'alk-pod-controller-status',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerStatusComponent implements OnChanges {
  @Input()
  podControllerInfo: PodControllerInfo;

  @Input()
  tooltipPosition = 'start';

  podStatusToPod: { [key: string]: PodInfoItem[] } = {};
  iconStatus: GenericStatus;
  running: number;
  desired: number;
  GenericStatus = GenericStatus;
  POD_STATUSES = [...POD_STATUSES, 'notYetCreated'];
  getPodStatusIcon = getPodStatusIcon;
  podStatusRef: Status[] = [
    {
      scale: 1,
      class:
        'pod-controller-status-gauge__item' +
        ' pod-controller-status-gauge__item--Null',
    },
  ];

  ngOnChanges({ podControllerInfo }: SimpleChanges) {
    if (podControllerInfo && podControllerInfo.currentValue) {
      this.podStatusToPod = getPodStatusToPod(this.podControllerInfo);
      this.running =
        this.podStatusToPod[PodStatusEnum.Running].length +
        this.podStatusToPod[PodStatusEnum.Completed].length;
      this.desired = this.podControllerInfo.desired;
      this.updateStatuItems();
    }
  }

  updateStatuItems() {
    let podStatusRef: Status[] = [];
    const block = 'pod-controller-status-gauge__item';

    podStatusRef = Object.keys(this.podStatusToPod).map(podStatus => ({
      scale: this.podStatusToPod[podStatus].length,
      class: block + ` ${block}--${podStatus}`,
    }));
    if (this.notYetCreatedPods > 0) {
      podStatusRef.push({
        scale: this.notYetCreatedPods,
        class: block + ` ${block}--Null`,
      });
    } else if (!this.total) {
      // 当没有任何状态的情况下，显示一条null
      podStatusRef = [
        {
          scale: 1,
          class: block + ` ${block}--Null`,
        },
      ];
    }
    this.podStatusRef = podStatusRef;
  }

  tooltipStatusShow(status: string) {
    return (
      (this.podStatusToPod[status] && this.podStatusToPod[status].length > 0) ||
      (status === 'notYetCreated' && this.notYetCreatedPods > 0)
    );
  }

  get notYetCreatedPods() {
    return (
      this.podControllerInfo &&
      this.desired - this.podControllerInfo.pods.length
    );
  }

  get total() {
    return (
      reduce(
        Object.keys(this.podStatusToPod).map(
          (podStatus: string) => this.podStatusToPod[podStatus].length,
        ),
        (acc, cur) => acc + cur,
        0,
      ) + this.notYetCreatedPods
    );
  }
}
