import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { PodControllerInfo, PodInfoItem } from '~api/backendapi';
import { GenericStatus } from '~api/status';

import {
  POD_STATUSES,
  getPodStatusIcon,
  getPodStatusToPod,
} from '../../utils/pod-status';

@Component({
  selector: 'alk-pod-controller-info-icons',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerInfoIconsComponent implements OnChanges {
  @Input()
  podControllerInfo: PodControllerInfo;

  podStatusToPod: { [key: string]: PodInfoItem[] };
  iconStatus: GenericStatus;
  POD_STATUSES = POD_STATUSES;
  getPodStatusIcon = getPodStatusIcon;

  ngOnChanges({ podControllerInfo }: SimpleChanges) {
    if (podControllerInfo && podControllerInfo.currentValue) {
      this.podStatusToPod = getPodStatusToPod(this.podControllerInfo);
    }
  }
}
