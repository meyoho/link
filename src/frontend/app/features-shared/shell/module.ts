import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared';

import { ShellComponent } from './component';

@NgModule({
  declarations: [ShellComponent],
  imports: [CommonModule, SharedModule],
  exports: [ShellComponent],
  providers: [],
})
export class ShellModule {}
