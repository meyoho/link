import { CodeEditorComponent } from '@alauda/code-editor';
import {
  Directive,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';
import { debounce } from 'lodash';
import { Subscription } from 'rxjs';

@Directive({
  selector:
    // tslint:disable-next-line:directive-selector
    'ng-monaco-editor, ng-monaco-diff-editor, aui-code-editor',
})
export class CodeEditorEnhancementDirective implements OnInit, OnDestroy {
  private monacoEditorSubscription: Subscription;
  private disposable: monaco.IDisposable;
  @Input()
  options: any;

  @HostBinding('class.readonly')
  get isReadOnly() {
    return this.options && this.options.readOnly;
  }

  constructor(@Optional() private auiCodeEditor: CodeEditorComponent) {}

  ngOnInit() {
    if (this.auiCodeEditor) {
      this.monacoEditorSubscription = this.auiCodeEditor.monacoEditorChanged.subscribe(
        (editor: monaco.editor.IStandaloneCodeEditor) => {
          editor.focus();

          // Makes sure we apply the indentation correctly
          const detectIndentation = () => {
            if (editor && editor.getModel()) {
              const model = editor.getModel();
              model.detectIndentation(true, 2);

              // FIXME:
              // this.elementRef.nativeElement.style.height = model.getLineCount() * 19 + 'px';
            }
          };

          if (this.disposable) {
            this.disposable.dispose();
          }

          this.disposable = editor.onDidChangeModelContent(
            debounce(detectIndentation, 500),
          );
        },
      );
    }
  }

  ngOnDestroy(): void {
    if (this.monacoEditorSubscription) {
      this.monacoEditorSubscription.unsubscribe();
    }

    if (this.disposable) {
      this.disposable.dispose();
    }
  }
}
