import {
  ChangeDetectorRef,
  Directive,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { TocContentsContainerDirective } from './contents-container.directive';

@Directive({
  selector: '[alkTocLink]',
})
export class TocLinkDirective implements OnInit, OnDestroy {
  @HostBinding('class.active')
  active: boolean;

  @Input()
  for: TocContentsContainerDirective;
  @Input()
  alkTocLink: string;

  private _subs: Subscription[] = [];

  @HostListener('click')
  onClick() {
    this.for.scrollTo(this.alkTocLink);
  }

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnInit() {
    if (!this.for || !this.alkTocLink) {
      return;
    }
    this._subs.push(
      this.for.activedChange.subscribe((actived: string) => {
        this.active = actived === this.alkTocLink;
        this.cdr.markForCheck();
      }),
    );
  }

  ngOnDestroy() {
    this._subs.forEach(sub => sub.unsubscribe());
  }
}
