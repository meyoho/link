import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Optional,
} from '@angular/core';

import { TocContentsContainerDirective } from './contents-container.directive';

@Directive({
  selector: '[alkTocContent]',
})
export class TocContentDirective implements OnInit, OnDestroy {
  @HostBinding('class.active')
  active: boolean;

  @Input()
  alkTocContent: string;

  nativeElement: any;

  constructor(
    elemRef: ElementRef,
    @Optional() private containerDirective: TocContentsContainerDirective,
  ) {
    this.nativeElement = elemRef.nativeElement;
  }

  ngOnInit(): void {
    if (this.containerDirective) {
      this.containerDirective.registerContent(this);
    }
  }

  ngOnDestroy(): void {
    if (this.containerDirective) {
      this.containerDirective.deregisterContent(this);
    }
  }
}
