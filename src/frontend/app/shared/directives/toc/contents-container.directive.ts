import {
  AfterContentInit,
  ChangeDetectorRef,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  OnDestroy,
  Output,
} from '@angular/core';
import { easeCubicIn } from 'd3-ease';
import { maxBy, minBy } from 'lodash';
import { EMPTY as empty, Subject, Subscription, of } from 'rxjs';
import { animationFrameScheduler } from 'rxjs';
import {
  concat,
  map,
  repeat,
  startWith,
  switchMap,
  takeUntil,
  takeWhile,
  tap,
} from 'rxjs/operators';

import { TocContentDirective } from './content.directive';

@Directive({
  selector: '[alkTocContentsContainer]',
  exportAs: 'toc-contents-container',
  providers: [TocContentsContainerDirective],
})
export class TocContentsContainerDirective
  implements AfterContentInit, OnDestroy {
  @Output()
  activedChange = new EventEmitter<string>();
  private _contents: TocContentDirective[] = [];
  private _scrollTop$ = new Subject<number>();
  private _scrollTo$ = new Subject<string>();
  private _onDestroy$ = new Subject<void>();
  private _subs: Subscription[] = [];
  private _nativeElement: any;

  get scrollTop(): number {
    return this._nativeElement.scrollTop || 0;
  }

  set scrollTop(value: number) {
    this._nativeElement.scrollTop = value;
  }

  get isScrollEnd() {
    return (
      this._nativeElement.scrollHeight - this._nativeElement.scrollTop ===
      this._nativeElement.clientHeight
    );
  }

  constructor(elementRef: ElementRef, private cdr: ChangeDetectorRef) {
    this._nativeElement = elementRef.nativeElement;
  }

  getOffsetTop(element: any): number {
    if (element.parentElement === this._nativeElement) {
      return element.offsetTop;
    }
    return element.offsetTop + this.getOffsetTop(element.parentElement);
  }

  ngAfterContentInit() {
    const actived$ = this._scrollTop$.pipe(
      startWith(this.scrollTop),
      map(scrollTop => {
        return this.isScrollEnd
          ? maxBy(this._contents, content =>
              this.getOffsetTop(content.nativeElement),
            )
          : minBy(this._contents, content =>
              Math.abs(
                (scrollTop || 0) - this.getOffsetTop(content.nativeElement),
              ),
            );
      }),
      map(actived => actived.alkTocContent),
      tap(actived => {
        this._contents.forEach(content => {
          content.active = actived === content.alkTocContent;
        });
        this.cdr.markForCheck();
      }),
    );

    const scrollTween$ = this._scrollTo$.pipe(
      switchMap(name => {
        const target = this._contents.find(
          content => content.alkTocContent === name,
        );

        if (!target) {
          return empty;
        }

        const destination = this.getOffsetTop(target.nativeElement);

        const start = performance.now();
        const source = this.scrollTop;
        const duration = 500;

        return of(0, animationFrameScheduler).pipe(
          repeat(),
          map(() => (performance.now() - start) / duration),
          takeWhile(t => t < 1),
          concat(of(1)),
          map(easeCubicIn),
          map(t => source * (1 - t) + destination * t),
        );
      }),
      takeUntil(this._onDestroy$),
    );

    this._subs.push(actived$.subscribe(this.activedChange));
    this._subs.push(
      scrollTween$.subscribe(tweenValue => {
        this.scrollTop = tweenValue;
      }),
    );
  }

  @HostListener('scroll')
  onScroll() {
    this._scrollTop$.next(this.scrollTop);
  }

  ngOnDestroy() {
    this._subs.forEach(sub => sub.unsubscribe());
    this._onDestroy$.next();
  }

  scrollTo(content: string) {
    this._scrollTo$.next(content);
  }

  registerContent(tocContent: TocContentDirective) {
    this._contents = [...this._contents, tocContent];
  }

  deregisterContent(tocContent: TocContentDirective) {
    this._contents = this._contents.filter(
      content => content.alkTocContent !== tocContent.alkTocContent,
    );
  }
}
