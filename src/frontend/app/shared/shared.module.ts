import { CodeEditorModule } from '@alauda/code-editor';
import {
  AutocompleteModule,
  ButtonModule,
  CardModule,
  CheckboxModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InputModule,
  MessageModule,
  NotificationModule,
  PaginatorModule,
  PlatformNavModule,
  RadioModule,
  SelectModule,
  SortModule,
  StatusBarModule,
  TableModule,
  TabsModule,
  TagModule,
  TooltipModule,
} from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material';

import { TranslateModule } from '../translate';

import { ComponentsModule } from './components';
import { DirectivesModule } from './directives';
import { PipesModule } from './pipes';

const EXPORTABLE_IMPORTS = [
  // Vendor modules:
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  TranslateModule,

  // Material imports
  FlexLayoutModule,
  MatProgressBarModule,

  // AUI imports
  FormModule,
  IconModule,
  StatusBarModule,
  ButtonModule,
  DropdownModule,
  SortModule,
  TableModule,
  CodeEditorModule,
  CardModule,
  TooltipModule,
  DialogModule,
  NotificationModule,
  MessageModule,
  InputModule,
  AutocompleteModule,
  CheckboxModule,
  RadioModule,
  SelectModule,
  PaginatorModule,
  TabsModule,
  TagModule,
  PlatformNavModule,

  // App shared modules:
  ComponentsModule,
  PipesModule,
  DirectivesModule,
];

@NgModule({
  imports: [...EXPORTABLE_IMPORTS],
  exports: [...EXPORTABLE_IMPORTS],
  declarations: [],
})
export class SharedModule {}
