import { Component, Input } from '@angular/core';

import { TimeService } from '../../../services';

@Component({
  selector: 'alk-time',
  template: `
    <span [auiTooltip]="tooltip">{{ main }}</span>
  `,
  styles: [':host { cursor: default }'],
})
export class TimeComponent {
  @Input()
  timestamp: string;

  /**
   * 优先使用相对时间
   */
  @Input()
  mode: 'absolute' | 'relative' = 'relative';

  @Input()
  format: string;

  get main() {
    return this.showAsRelative ? this.relativeTime : this.time;
  }

  // Tooltip has opposite display logic with main label
  get tooltip() {
    return this.showAsRelative ? this.time : this.relativeTime;
  }

  get time() {
    const format =
      this.format || (this.relative ? 'YYYY-MM-DD' : 'YYYY-MM-DD HH:mm:ss');
    return this.timeService.transform(this.timestamp, format);
  }

  get relativeTime() {
    return this.timeService.transformRelative(this.timestamp);
  }

  get showAsRelative() {
    return this.relative && !this.timeService.tooOld(this.timestamp);
  }

  get relative() {
    return this.mode !== 'absolute';
  }

  constructor(private timeService: TimeService) {}
}
