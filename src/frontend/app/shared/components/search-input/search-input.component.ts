import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'alk-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchInputComponent {
  private _value = '';

  @Input()
  set value(value: string) {
    this._value = value || '';
  }

  @Input()
  searching = false;

  get value() {
    return this._value;
  }

  @Output()
  valueChange = new EventEmitter();
  @Output()
  search = new EventEmitter();

  onSearch(search: string) {
    this.search.emit(search || '');
    this.value = search;
  }

  onValueChange(value: string) {
    this.valueChange.emit(value || '');
    this.value = value;
  }
}
