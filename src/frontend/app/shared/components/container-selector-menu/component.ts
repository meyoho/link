import { MenuComponent } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { get } from 'lodash';
import { DeploymentDetail, LogSources } from '~api/backendapi';

import { ResourceDataService } from '../../../services';

@Component({
  selector: 'alk-container-selector-menu',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class ContainerSelectorMenuComponent implements OnInit, OnChanges {
  // We need the following params to get containers for a random workload resource
  @Input()
  resourceKind: string;

  @Input()
  resourceName: string;

  @Input()
  namespace: string;

  @Output()
  containerSelect = new EventEmitter();

  // Optional:
  @Input()
  selected: { pod: string; container: string };

  @ViewChild(MenuComponent)
  menu: MenuComponent;

  // Log sources (also refers to container) based on resource type/name
  logSources: LogSources;

  constructor(
    private resourceData: ResourceDataService,
    private http: HttpClient,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit(): void {}

  ngOnChanges({ resourceName, resourceKind, namespace }: SimpleChanges): void {
    if (resourceName || resourceKind || namespace) {
      this.getLogSources();
    }
  }

  onContainerSelected(pod: string, container: string) {
    this.containerSelect.emit({ pod, container });
  }

  private async getLogSources() {
    if (
      this.namespace &&
      this.resourceKind &&
      this.resourceName &&
      // Currently container list only init once.
      !this.logSources
    ) {
      let resourceKind = this.resourceKind;
      let resourceName = this.resourceName;

      if (this.resourceKind === 'Deployment') {
        const detail = await this.resourceData
          .getDetail<DeploymentDetail>({
            kind: this.resourceKind,
            name: this.resourceName,
            namespace: this.namespace,
          })
          .toPromise();

        resourceKind = 'ReplicaSet';
        resourceName = get(detail, 'newReplicaSet.objectMeta.name');
      }

      this.logSources = await this.http
        .get<LogSources>(
          `api/v1/log/source/${this.namespace}/${resourceKind}/${resourceName}`,
        )
        .toPromise();
    }
    this.cdr.markForCheck();
  }
}
