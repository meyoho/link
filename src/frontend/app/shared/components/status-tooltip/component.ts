import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { GenericStatus, GenericStatusList } from '~api/status';

import { TranslateService } from '../../../translate';

const StatusToGenericStatus: { [key: string]: GenericStatus } = {
  running: GenericStatus.Success,
  pending: GenericStatus.Pending,
  failed: GenericStatus.Error,
  warning: GenericStatus.Warning,
  succeeded: GenericStatus.Success,
};

@Component({
  selector: 'alk-status-tooltip',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class StatusTooltipComponent implements OnChanges {
  @Input()
  type: string;
  @Input()
  statusInfo: { [key: string]: number; total?: number } = {};

  statusItems: {
    value: number;
    label: string;
    status: GenericStatus;
    key: string;
  }[] = [];

  constructor(private translate: TranslateService) {}

  ngOnChanges({ statusInfo }: SimpleChanges) {
    if (statusInfo) {
      this.statusItems = this.updateStatuItems();
    }
  }

  private updateStatuItems() {
    return Object.keys(this.statusInfo)
      .filter(key => key !== 'total')
      .map(key => ({
        label: this.translate.get(key),
        value: this.statusInfo[key],
        status: StatusToGenericStatus[key],
        key,
      }))
      .filter(item => !!item.value)
      .sort(
        (a, b) =>
          GenericStatusList.indexOf(a.status) -
          GenericStatusList.indexOf(b.status),
      );
  }
}
