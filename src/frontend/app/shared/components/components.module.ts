import {
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InputModule,
  SortModule,
  TableModule,
  TooltipModule,
} from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '../../translate';

import {
  BreadcrumbComponent,
  BreadcrumbItemDirective,
  BreadcrumbSeparatorComponent,
} from './breadcrumb/breadcrumb.component';
import { DefaultBreadcrumbComponent } from './breadcrumb/default-breadcrumb.component';
import { ContainerSelectorMenuComponent } from './container-selector-menu/component';
import { FoldableBlockComponent } from './foldable-block/component';
import { SearchInputComponent } from './search-input/search-input.component';
import { ShellButtonComponent } from './shell-button/component';
import { ShrinkListComponent } from './shrink-list/component';
import { StatusIconComponent } from './status-icon/status-icon.component';
import { StatusTooltipComponent } from './status-tooltip/component';
import { StringsLabelComponent } from './strings-label/component';
import { TagsLabelComponent } from './tags-label/tags-label.component';
import { TimeComponent } from './time/component';
import { ZeroStateComponent } from './zero-state/zero-state.component';

const EXPORTABLES = [
  TagsLabelComponent,
  StringsLabelComponent,
  ZeroStateComponent,
  StatusIconComponent,
  BreadcrumbComponent,
  BreadcrumbItemDirective,
  DefaultBreadcrumbComponent,
  BreadcrumbSeparatorComponent,
  TimeComponent,
  ShrinkListComponent,
  StatusTooltipComponent,
  FoldableBlockComponent,
  SearchInputComponent,
  ShellButtonComponent,
  ContainerSelectorMenuComponent,
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    FormModule,
    PortalModule,
    TranslateModule,
    TableModule,
    SortModule,
    TooltipModule,
    IconModule,
    InputModule,
    DialogModule,
    DropdownModule,
  ],
  declarations: [...EXPORTABLES],
  exports: [...EXPORTABLES],
})
export class ComponentsModule {}
