import { Component, Input } from '@angular/core';

@Component({
  selector: 'alk-foldable-block',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class FoldableBlockComponent {
  @Input() text = 'advanced_attrs';
  folded = true;
}
