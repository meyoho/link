import {
  Component,
  ContentChildren,
  Directive,
  QueryList,
  TemplateRef,
} from '@angular/core';

@Directive({
  selector: '[alkBreadcrumbItem]',
})
export class BreadcrumbItemDirective {}

@Component({
  selector: 'alk-breadcrumb-separator',
  template: '<span>/</span>',
  styles: [
    `
      :host {
        margin: 0 4px;
      }
    `,
  ],
})
export class BreadcrumbSeparatorComponent {}

@Component({
  selector: 'alk-breadcrumb',
  templateUrl: 'breadcrumb.component.html',
  styleUrls: ['breadcrumb.component.scss'],
})
export class BreadcrumbComponent {
  @ContentChildren(BreadcrumbItemDirective, { read: TemplateRef })
  itemTemplateRefs: QueryList<TemplateRef<any>>;
}
