import {
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  OnInit,
  QueryList,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UiStateService } from '../../../services';

import { BreadcrumbItemDirective } from './breadcrumb.component';

const INGORED_PARTIALS = ['list', 'detail', 'update'];

@Component({
  selector: 'alk-default-breadcrumb',
  template: `
    <alk-breadcrumb>
      <ng-container *ngFor="let partial of (partials$ | async); last as isLast">
        <ng-container *alkBreadcrumbItem>
          <a
            *ngIf="
              partial.routerLink &&
                !(contentItemTemplates?.length === 0 && isLast);
              else: notHrefItem
            "
            [routerLink]="partial.routerLink"
          >
            {{ partial.label | translate }}
          </a>
          <ng-template #notHrefItem>
            <span>{{ partial.label | translate }}</span>
          </ng-template>
        </ng-container>
      </ng-container>
      <!-- We want to project only the breaditem contents here -->
      <ng-container *ngFor="let templateRef of contentItemTemplates">
        <!--
          Note: we need alkBreadcrumbItem here since ContentChild
          queries are only meaningful in the current template.
        -->
        <ng-container *alkBreadcrumbItem>
          <ng-container *ngTemplateOutlet="templateRef"></ng-container>
        </ng-container>
      </ng-container>
    </alk-breadcrumb>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DefaultBreadcrumbComponent implements OnInit {
  partials$: Observable<{ routerLink?: string; label: string }[]>;
  @ContentChildren(BreadcrumbItemDirective, { read: TemplateRef })
  contentItemTemplates: QueryList<TemplateRef<any>>;

  constructor(
    private uiState: UiStateService,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.partials$ = this.uiState.activeItemInfo$.pipe(
      map(navItemPartials => [
        ...navItemPartials,
        ...this.getLeafUrls()
          .filter(partial => !INGORED_PARTIALS.includes(partial))
          .map(partial => ({ label: partial })),
      ]),
    );
  }

  findLeaftRoute(activatedRoute: ActivatedRoute): ActivatedRoute {
    return activatedRoute.firstChild
      ? this.findLeaftRoute(activatedRoute.firstChild)
      : activatedRoute;
  }

  getLeafUrls() {
    return this.findLeaftRoute(this.activatedRoute).snapshot.url.map(
      fragment => fragment.path,
    );
  }
}
