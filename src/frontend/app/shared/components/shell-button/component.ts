import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { TerminalPageParams } from '~api/shell';

import { TerminalService } from '../../../services';

@Component({
  selector: 'alk-shell-button',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ShellButtonComponent implements OnInit {
  @Input()
  params: TerminalPageParams;
  constructor(private terminal: TerminalService) {}

  ngOnInit(): void {}

  openTerminal({ pod, container }: { pod: string; container: string }) {
    this.terminal.openTerminal({
      pod,
      container,
      namespace: this.params.namespace,

      // Optional
      resourceName: this.params.resourceName,
      resourceKind: this.params.resourceKind,
    });
  }
}
