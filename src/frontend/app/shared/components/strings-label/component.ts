import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'alk-strings-label',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StringsLabelComponent {
  @Input()
  strings: string[];
}
