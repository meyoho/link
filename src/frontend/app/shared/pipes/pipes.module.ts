import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DetailRoutesPipe } from './detail-routes.pipe';
import { SmoothNumberPipe } from './smooth-number.pipe';
import { TimePipe } from './time.pipe';
import { UnitFormatterPipe } from './unit-formatter.pipe';

const EXPORTABLES = [
  TimePipe,
  DetailRoutesPipe,
  UnitFormatterPipe,
  SmoothNumberPipe,
];

@NgModule({
  imports: [CommonModule],
  declarations: [...EXPORTABLES],
  exports: [...EXPORTABLES],
})
export class PipesModule {}
