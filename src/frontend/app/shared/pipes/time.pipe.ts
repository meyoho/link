import { Pipe, PipeTransform } from '@angular/core';

import { TimeService } from '../../services';

/**
 * Transform a time string to human readable string.
 */
@Pipe({ name: 'alkTime' })
export class TimePipe implements PipeTransform {
  constructor(private time: TimeService) {}

  transform(value: string, dateFormat = 'YYYY-MM-DD HH:mm:ss'): any {
    if (!value) {
      return '-';
    }
    return this.time.transform(value, dateFormat);
  }
}
