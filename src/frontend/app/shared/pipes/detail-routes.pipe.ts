import { Pipe, PipeTransform } from '@angular/core';
import { Resource } from '~api/backendapi';

import { DetailRouteParams, ExtraParams, getDetailRoutes } from '../../utils';

@Pipe({ name: 'alkDetailRoutes' })
export class DetailRoutesPipe implements PipeTransform {
  transform(
    resource: Resource | DetailRouteParams,
    extraOptions?: ExtraParams,
  ): any[] {
    return getDetailRoutes(resource, extraOptions);
  }
}
