import { Pipe, PipeTransform } from '@angular/core';
import { Observable, Subject, interval } from 'rxjs';
import { animationFrameScheduler } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';

const STEPS = 30;
const simpleCurve = (step: number, steps: number) => Math.pow(step / steps, 2);

@Pipe({ name: 'alkSmoothNumber', pure: false })
export class SmoothNumberPipe implements PipeTransform {
  private latestVal = 0;
  private value$ = new Subject<number>();
  private valueChange$ = this.value$.asObservable().pipe(
    distinctUntilChanged(),
    switchMap(target => {
      const begin = this.latestVal || 0;
      return interval(25, animationFrameScheduler).pipe(
        map(
          number => begin + simpleCurve(number + 1, STEPS) * (target - begin),
        ),
        take(STEPS),
      );
    }),
    publishReplay(1),
    refCount(),
    tap(val => (this.latestVal = val)),
  );

  transform(value: number): Observable<number> {
    this.value$.next(value);
    return this.valueChange$;
  }
}
