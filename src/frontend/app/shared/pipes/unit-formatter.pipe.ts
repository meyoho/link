import { Pipe, PipeTransform } from '@angular/core';

import { UnitFormatterService } from '../../services/unit-formatter.service';

/**
 * Formats memory in bytes to a binary prefix format, e.g., 789,21 MB.
 */
@Pipe({ name: 'alkUnitFormatter' })
export class UnitFormatterPipe implements PipeTransform {
  constructor(private unitFormatter: UnitFormatterService) {}
  transform(value: number, type: 'memory' | 'cpu'): string {
    return this.unitFormatter.transform(value, type);
  }
}
