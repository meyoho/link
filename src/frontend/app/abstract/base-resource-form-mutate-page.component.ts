import { MessageService, NotificationService } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { safeDump, safeLoadAll } from 'js-yaml';
import { get } from 'lodash';
import { BehaviorSubject, Observable, combineLatest, of } from 'rxjs';
import {
  first,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { KubernetesResource } from '~api/raw-k8s';
import {
  HistoryService,
  ResourceDataService,
  YamlSampleService,
} from '~app/services';
import { TranslateService } from '~app/translate';
import { getDetailRoutes } from '~app/utils';
import { createActions, updateActions } from '~app/utils/code-editor-config';

export abstract class BaseResourceMutatePageComponent<
  R extends KubernetesResource = KubernetesResource
> implements OnInit, AfterViewInit {
  name$: Observable<string>;

  resource$: Observable<R>;
  detailRouteParams: any;

  // Model for form
  formModel: R = {} as R;

  // YAML content
  yaml = '';

  originalYaml$: Observable<string>;

  // create/update
  method$: Observable<string>;
  isUpdate$: Observable<boolean>;

  mode: 'form' | 'yaml' = 'form';

  mode$ = new BehaviorSubject<string>(this.mode);

  submitting = false;

  editorOptions = {
    language: 'yaml',
  };

  initialized = false;

  // TODO: there are some slight difference between to modes
  actionsConfig$: Observable<any>;

  @ViewChild(NgForm)
  form: NgForm;

  // User need to provide this:
  abstract readonly kind: string;

  readonly activatedRoute: ActivatedRoute;
  readonly resourceData: ResourceDataService;
  readonly history: HistoryService;
  readonly cdr: ChangeDetectorRef;
  readonly yamlSample: YamlSampleService;
  readonly auiMessageService: MessageService;
  readonly auiNotificationService: NotificationService;
  readonly translate: TranslateService;
  readonly router: Router;

  constructor(injector: Injector) {
    this.activatedRoute = injector.get(ActivatedRoute);
    this.resourceData = injector.get(ResourceDataService);
    this.history = injector.get(HistoryService);
    this.cdr = injector.get(ChangeDetectorRef);
    this.yamlSample = injector.get(YamlSampleService);
    this.auiMessageService = injector.get(MessageService);
    this.auiNotificationService = injector.get(NotificationService);
    this.translate = injector.get(TranslateService);
    this.router = injector.get(Router);
  }

  ngOnInit() {
    this.method$ = this.activatedRoute.url.pipe(
      startWith(this.activatedRoute.snapshot.url),
      map(url => url[0].path),
      publishReplay(1),
      refCount(),
    );

    this.isUpdate$ = this.method$.pipe(map(method => method === 'update'));

    this.resource$ = this.isUpdate$.pipe(
      switchMap(isUpdate => {
        if (!isUpdate) {
          const sample = this.yamlSample.feedDefaultNamespace(
            this.activatedRoute.snapshot.data['sample'],
          );
          return of(this.yamlToForm(sample));
        } else {
          return this.fetchDataForUpdate();
        }
      }),
      publishReplay(1),
      refCount(),
    );

    this.resource$.pipe(first()).subscribe(resource => {
      if (!this.initialized && resource) {
        this.formModel = resource;
        this.yaml = this.formToYaml(resource);
        this.detailRouteParams = {
          name: resource.metadata.name,
          namespace: resource.metadata.namespace || '',
          kind: resource.kind,
          apiVersion: resource.apiVersion,
        };
        this.initialized = true;
        this.cdr.markForCheck();
      }
    });

    this.originalYaml$ = this.resource$.pipe(
      map(resource => this.formToYaml(resource)),
    );

    this.actionsConfig$ = this.isUpdate$.pipe(
      map(isUpdate => (isUpdate ? updateActions : createActions)),
    );

    this.name$ = this.resource$.pipe(
      map(resource => get(resource, 'metadata.name')),
    );
  }

  ngAfterViewInit() {
    this.form.statusChanges.subscribe(() => {
      this.cdr.markForCheck();
    });
  }

  modeChange(mode: 'form' | 'yaml') {
    if (mode === this.mode) {
      return;
    }

    if (mode === 'form') {
      try {
        this.formModel = this.yamlToForm(this.yaml);
      } catch (err) {
        this.auiNotificationService.error({
          title: this.translate.get('yaml_format_error_message'),
          content: err.message,
        });
        return;
      }
    } else {
      this.yaml = this.formToYaml(this.formModel);
    }

    this.mode = mode;

    this.mode$.next(mode);

    // Reset submitted status
    (this.form as any).submitted = false;
    this.cdr.markForCheck();
  }

  formModelChange(form: R) {
    this.formModel = form;
  }

  get submitDisabled() {
    return this.submitting;
  }

  async onSubmit(event: Event) {
    this.form.onSubmit(event);
    if (this.form.invalid) {
      return;
    }
    if (this.mode === 'yaml') {
      this.formModel = this.yamlToForm(this.yaml);
    }

    this.submitting = true;
    this.cdr.markForCheck();

    try {
      const isUpdate = await this.isUpdate$.pipe(first()).toPromise();
      if (isUpdate) {
        await this.resourceData.updatePromise(
          this.activatedRoute.snapshot.params as any,
          this.formModel,
        );
      } else {
        await this.resourceData.create(this.formModel);
      }
      this.afterMutation(this.formModel);
    } catch (err) {
      console.log(err);
    }
    this.submitting = false;
    this.cdr.markForCheck();
  }

  afterMutation(resource: R) {
    this.router.navigate(
      getDetailRoutes({
        apiVersion: resource.apiVersion,
        kind: resource.kind,
        name: resource.metadata.name,
        namespace: resource.metadata.namespace,
      }),
    );
  }

  back() {
    this.history.back(['..'], this.activatedRoute);
  }

  fetchDataForUpdate() {
    return combineLatest(this.activatedRoute.params, this.mode$).pipe(
      switchMap(([params]) =>
        this.resourceData
          .getRawDetail({
            ...params,
            kind: this.kind,
          } as any)
          .pipe(map(detail => detail.data as R)),
      ),
    );
  }

  private yamlToForm(yaml: string) {
    const formModels = safeLoadAll(yaml).map(item =>
      item === 'undefined' ? undefined : item,
    );
    let formModel = formModels[0];

    // For now we can only process a single deployment resource in the yaml.
    if (formModels.length > 1) {
      this.auiMessageService.warning(
        this.translate.get('multi_yaml_resource_warning'),
      );
    }

    if (!formModel || formModel instanceof String) {
      formModel = {};
    }

    return formModel;
  }

  private formToYaml(json: any) {
    try {
      // Following line is to remove undefined values
      json = JSON.parse(JSON.stringify(json));
      return safeDump(json);
    } catch (err) {
      console.log(err);
    }
  }
}
