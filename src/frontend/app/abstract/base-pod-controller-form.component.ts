import { Injector, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { isEqual } from 'lodash';
import { KubernetesResource } from '~api/raw-k8s';

import { BaseKubernetesResourceFormComponent } from './base-kubernetes-resource-form.component';

/**
 * UI for mutating a raw k8s deployment object.
 * Note that we will not be able to alter all fields with the UI due to implementation
 * limitations, thus we have to make sure:
 * - non-ui-related fields will be preserved upon transformation.
 * - delete fields will be correctly deleted.
 * - some fields are reserved and should not be updated
 * - only contains a single resource
 */
export abstract class BasePodControllerFormComponent<
  R extends KubernetesResource
> extends BaseKubernetesResourceFormComponent<R> implements OnInit {
  constructor(injector: Injector) {
    super(injector);
  }

  // User need to provide this:
  abstract readonly kind: string;

  createSpecForm() {
    return this.fb.group({
      selector: this.fb.group({
        matchLabels: this.fb.control({}),
      }),
      template: this.fb.group({
        spec: this.fb.control({}),
        metadata: this.fb.group({
          labels: this.fb.control({}),
        }),
      }),
    });
  }

  getDefaultSelectorKey() {
    return 'apps.' + this.kind.toLowerCase();
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(/^[a-z0-9^-]*[a-z0-9-]*[a-z0-9]$/),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    const specForm = this.createSpecForm();

    return this.fb.group({
      metadata: metadataForm,
      spec: specForm,
    });
  }

  onBlur() {
    super.onBlur();

    if (this.ensureMatchLabels()) {
      this.form.updateValueAndValidity({ onlySelf: true });
    }
  }

  /**
   * We need to make sure the matchLabels align with spec.template.metadata.labels
   */
  private ensureMatchLabels() {
    if (!this.form || this.updateMode) {
      return;
    }
    const matchLabels = this.form.get(['spec', 'selector', 'matchLabels']);
    const templateLabels = this.form.get([
      'spec',
      'template',
      'metadata',
      'labels',
    ]);

    const name = this.form.get(['metadata', 'name']).value;

    const defaultMatchLabels = {
      [this.getDefaultSelectorKey()]: name,
    };

    const shouldRecoverFromName = () => {
      const entries = Object.entries(matchLabels.value || {});
      if (entries.length === 0) {
        return true;
      }

      if (
        entries.length === 1 &&
        entries[0][0] === this.getDefaultSelectorKey() &&
        entries[0][1] !== name
      ) {
        return true;
      }

      return false;
    };

    if (shouldRecoverFromName()) {
      matchLabels.setValue(defaultMatchLabels, { emit: false });
      templateLabels.setValue(defaultMatchLabels, { emit: false });
      return true;
    } else if (!isEqual(matchLabels.value, templateLabels.value)) {
      templateLabels.setValue(matchLabels.value, { emit: false });
      return true;
    }

    return;
  }
}
