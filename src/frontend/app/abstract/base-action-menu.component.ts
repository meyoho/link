import {
  EventEmitter,
  Injector,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Resource, StringMap } from '~api/backendapi';

import {
  DialogService,
  DialogSize,
  MenuComponent,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { RollBackComponent } from '~app/features-shared/common/roll-back-to/component';
import { UpdateLabelsComponent } from '~app/features-shared/common/update-labels/component';
import {
  ConfirmBoxService,
  DetailParams,
  ResourceDataService,
} from '~app/services';
import { TranslateService } from '~app/translate';
import { getDetailRoutes } from '~app/utils';

export abstract class BaseActionMenuComponent<R extends Resource> {
  @Input()
  resource: R;

  @Input() curResourceParams: DetailParams;
  @Input() rollBackResourceParams: DetailParams;

  @Output()
  updated = new EventEmitter();

  @Output()
  deleted = new EventEmitter();

  @ViewChild(MenuComponent)
  menu: MenuComponent;

  protected router: Router;
  protected resourceData: ResourceDataService;
  protected dialog: DialogService;
  protected auiMessageService: MessageService;
  protected auiNotificationService: NotificationService;
  protected translate: TranslateService;
  protected confirmBox: ConfirmBoxService;

  constructor(public injector: Injector) {
    this.router = this.injector.get(Router);
    this.resourceData = this.injector.get(ResourceDataService);
    this.dialog = this.injector.get(DialogService);
    this.auiMessageService = this.injector.get(MessageService);
    this.auiNotificationService = this.injector.get(NotificationService);
    this.translate = this.injector.get(TranslateService);
    this.confirmBox = this.injector.get(ConfirmBoxService);
  }

  async doDelete() {
    try {
      const confirmed = await this.confirmBox.delete({
        kind: this.resource.typeMeta.kind,
        name: this.resource.objectMeta.name,
      });
      if (confirmed) {
        await this.resourceData.deleteDetail(this.getUrlParams()).toPromise();
        this.auiMessageService.success({
          content: this.translate.get('delete_succeeded'),
        });
        this.deleted.next(this.resource);
      }
    } catch (error) {
      this.auiNotificationService.error({ content: error.error || error });
    }
  }

  showUpdateAnnotations() {
    return this.patchFieldDialog('annotations');
  }

  showUpdateLabels() {
    return this.patchFieldDialog('labels');
  }

  private patchFieldDialog(field: 'annotations' | 'labels') {
    const dialogRef = this.dialog.open(UpdateLabelsComponent);
    dialogRef.componentInstance.labels = this.resource.objectMeta[field];
    dialogRef.componentInstance.title = this.translate.get(`update_` + field);
    dialogRef.componentInstance.onUpdate = async (labels: StringMap) => {
      try {
        await this.resourceData
          .patchField(this.getUrlParams(), field, labels)
          .toPromise();
        this.updated.next(this.resource);
        dialogRef.close();
        this.auiMessageService.success({
          content: `${this.translate.get(field)} ${this.translate.get(
            'update_succeeded',
          )}`,
        });
      } catch (error) {
        this.auiNotificationService.error({ content: error.error || error });
      }
    };
  }

  goToUpdatePage() {
    this.router.navigate(getDetailRoutes(this.resource, { mode: 'update' }));
  }

  goToRollbackPage() {
    const dialogRef = this.dialog.open(RollBackComponent, {
      size: DialogSize.Large,
    });
    dialogRef.componentInstance.resource = this.resource;
    dialogRef.componentInstance.curResourceParams = this.curResourceParams;
    dialogRef.componentInstance.rollBackResourceParams = this.rollBackResourceParams;
    dialogRef.componentInstance.updated$.subscribe(resource => {
      this.updated.next(resource);
    });
  }

  goToLogsPage() {
    this.router.navigate(getDetailRoutes(this.resource, { tab: 'logs' }));
  }

  getUrlParams() {
    return this.resourceData.getResourceDetailParams(this.resource);
  }
}
