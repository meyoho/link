import { MessageService } from '@alauda/ui';
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Injector, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { debounce } from 'lodash';
import {
  BehaviorSubject,
  Observable,
  Subscription,
  combineLatest,
  of,
} from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';
import { Resource, ResourceDetail } from '~api/backendapi';
import { TerminalPageParams } from '~api/shell';
import { HistoryService } from '~app/services';
import { TranslateService } from '~app/translate';
import { getDetailRoutes } from '~app/utils';

export const POLLING_TIMER = 10000;
export class ErrorWrapper {
  constructor(public error: any) {}
}

export abstract class BaseDetailPageComponent<R extends ResourceDetail>
  implements OnInit, OnDestroy {
  /**
   * Tab name calculated based on current route params
   */
  tab$: Observable<string>;
  /**
   * Tab index calculated based on current route params (used by aui-tab)
   */
  tabIndex$: Observable<number>;
  /**
   * Kind of the resource
   */
  kind$: Observable<string>;
  /**
   * Namespace of the resource
   */
  namespace$: Observable<string>;
  /**
   * Name of the resource
   */
  name$: Observable<string>;

  detailResponse$: Observable<R | ErrorWrapper>;
  // Similar to detailResponse, but filtered out errors.
  detail$: Observable<R>;
  // Similar to detailResponse, but filter only errors.
  error$: Observable<ErrorWrapper>;

  shellParams$: Observable<TerminalPageParams>;

  // Poll pulses
  poll$ = new BehaviorSubject(null);
  // Update pulses
  updated$ = new BehaviorSubject(null);

  // Fetching state
  fetching = false;

  /**
   * Change this if you want to change poll timing.
   */
  pollInterval = 30000;

  /**
   * Whether or not the detail page is loaded once.
   */
  firstLoad = false;

  router: Router;
  activatedRoute: ActivatedRoute;
  cdr: ChangeDetectorRef;
  history: HistoryService;
  translate: TranslateService;
  messageService: MessageService;

  protected debouncedUpdate: () => void;

  private errorSubscription: Subscription;

  constructor(public injector: Injector) {
    this.router = this.injector.get(Router);
    this.activatedRoute = this.injector.get(ActivatedRoute);
    this.cdr = this.injector.get(ChangeDetectorRef);
    this.debouncedUpdate = debounce(() => {
      this.poll$.next(null);
    }, this.pollInterval);
    this.history = this.injector.get(HistoryService);
    this.translate = this.injector.get(TranslateService);
    this.messageService = this.injector.get(MessageService);
  }

  abstract tabs: string[];
  abstract fetchData(params: {
    kind: string;
    namespace: string;
    name: string;
    apiVersion?: string;
  }): Observable<R>;

  ngOnInit() {
    this.tabIndex$ = this.activatedRoute.params.pipe(
      map(params => Math.max(this.tabs.indexOf(params.tab), 0)),
    );

    this.tab$ = this.activatedRoute.params.pipe(
      map(params => params.tab || this.tabs[0]),
    );

    this.kind$ = this.activatedRoute.params.pipe(map(params => params.kind));

    this.name$ = this.activatedRoute.params.pipe(map(params => params.name));

    this.namespace$ = this.activatedRoute.params.pipe(
      map(params => params.namespace),
    );

    // Optinal field. Only viable if the resource supports shell.
    this.shellParams$ = combineLatest(
      this.kind$,
      this.name$,
      this.namespace$,
    ).pipe(
      map(([resourceKind, resourceName, namespace]) => ({
        resourceKind,
        resourceName,
        namespace,
      })),
    );

    this.detailResponse$ = combineLatest(
      this.activatedRoute.params,
      this.poll$,
      this.updated$,
    ).pipe(
      debounceTime(50),
      tap(() => {
        this.fetching = true;
        this.cdr.markForCheck();
      }),
      switchMap(([params]: any[]) =>
        this.fetchData(params).pipe(
          catchError(error => {
            return of(new ErrorWrapper(error));
          }),
        ),
      ),
      tap(res => {
        this.fetching = false;
        this.cdr.markForCheck();
        this.debouncedUpdate();

        if (!(res instanceof ErrorWrapper)) {
          this.firstLoad = true;
        }
      }),
      publishReplay(1),
      refCount(),
    );

    this.detail$ = this.detailResponse$.pipe(
      filter<R>(detail => !(detail instanceof ErrorWrapper)),
      publishReplay(1),
      refCount(),
    );

    this.error$ = this.detailResponse$.pipe(
      filter<ErrorWrapper>(detail => detail instanceof ErrorWrapper),
      publishReplay(1),
      refCount(),
    );

    this.errorSubscription = this.error$.subscribe(error => {
      if (
        error.error instanceof HttpErrorResponse &&
        error.error.status === 404
      ) {
        if (!this.firstLoad && error.error.status === 404) {
          this.messageService.error(this.translate.get('404_error_toast'));
        }
        // Goback to the previous page if removed
        this.router.navigate(['..'], { relativeTo: this.activatedRoute });
        // FIXME: use history here instead:
        // this.history.back(['..'], { relativeTo: this.activatedRoute });
        this.errorSubscription.unsubscribe();
        this.errorSubscription = undefined;
      }
    });
  }

  onUpdate(_item: any) {
    this.updated$.next(null);
  }

  doNavigate(newParams: any) {
    const currentParams = this.activatedRoute.snapshot.params;
    this.router.navigate(
      [
        '.',
        {
          ...currentParams,
          ...newParams,
        },
      ],
      {
        relativeTo: this.activatedRoute,
        replaceUrl: true,
      },
    );
  }

  onTabIndexChange(tabIndex: number) {
    this.doNavigate({ tab: this.tabs[tabIndex] });
  }

  getDetailRoutes(item: Resource) {
    return getDetailRoutes(item);
  }

  ngOnDestroy() {
    this.poll$.complete();
    this.updated$.complete();

    if (this.errorSubscription) {
      this.errorSubscription.unsubscribe();
      this.errorSubscription = undefined;
    }
  }
}
