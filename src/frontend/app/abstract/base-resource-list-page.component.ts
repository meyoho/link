import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { startWith } from 'rxjs/operators';
import { Resource, ResourceList } from '~api/backendapi';

import {
  BaseResourceListComponent,
  TableState,
} from './base-resource-list.component';

/**
 * Base for resource list pages. Note, we name this as "page" since most of
 * the parameters are set via router and hence there should be only one item per page.
 */
export abstract class BaseResourceListPageComponent<
  RList extends ResourceList,
  R extends Resource
> extends BaseResourceListComponent<RList, R> {
  router: Router;
  activatedRoute: ActivatedRoute;

  getTableState$() {
    return this.activatedRoute.params.pipe(
      startWith(this.activatedRoute.snapshot.params),
    );
  }

  setTableState(newState: TableState) {
    const mergedParams = {
      ...this.activatedRoute.snapshot.params,
      pageIndex: 0,
      ...newState,
    };
    return this.router.navigate(['.', mergedParams], {
      relativeTo: this.activatedRoute,
      replaceUrl: true,
    });
  }

  constructor(injector: Injector) {
    super(injector);

    this.router = injector.get(Router);
    this.activatedRoute = injector.get(ActivatedRoute);
  }
}
