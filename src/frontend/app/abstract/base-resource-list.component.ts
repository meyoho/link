import { Sort, SortDirection } from '@alauda/ui';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Injector, OnInit } from '@angular/core';
import { debounce } from 'lodash';
import { BehaviorSubject, Observable, combineLatest, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';
import {
  ListMeta,
  PageParams,
  Resource,
  ResourceList,
  isHttpErrorResponse,
  isNotHttpErrorResponse,
} from '~api/backendapi';
import { AppConfigService } from '~app/services';

export interface ColumnDef {
  name: string;
  label?: string;
  sortable?: boolean;
  sortStart?: 'asc' | 'desc';
}

export interface FetchParams {
  namespace: string;
  search: string;
  sort: Sort;
  pageParams: PageParams;
}

export interface TableState {
  namespace?: string;
  search?: string;

  // Sort params:
  active?: string;
  direction?: SortDirection;

  // Page params
  pageSize?: number;
  pageIndex?: number;
}

/**
 * Base for resource list. Note, unlike simple resource list, this normally requires
 * the user to feed standalone fetch methods.
 *
 * Standard unidirection data flow:
 * [Action](setTableState)
 * ↓↓↓
 * [Table State](construct fetch params)
 * ↓↓↓
 * [Fetch Params](fetchResources)
 * ↓↓↓
 * [Fetch Observable](map)
 * ↓↓↓
 * [Resource List]
 *
 * Typically, every fetch is made through setTableState. A fetch will land if
 * tableState$ emits a new state.
 */
export abstract class BaseResourceListComponent<
  RList extends ResourceList,
  R extends Resource
> implements OnInit {
  protected poll$ = new BehaviorSubject(null);
  protected debouncedUpdate: () => void;

  updated$ = new BehaviorSubject(null);
  fetchParams$: Observable<FetchParams>;

  rawResponse$: Observable<RList | HttpErrorResponse>;
  list$: Observable<R[]>;
  listMeta$: Observable<ListMeta>;

  // List of fetch related params. They should be read only most of the time.
  pageParams$: Observable<PageParams>;
  sort$: Observable<Sort>;
  search$: Observable<string>;
  namespace$: Observable<string>;

  columnDefs: ColumnDef[];
  columns$: Observable<string[]>;

  fetching = false;
  searching = false;
  showZeroState = false;

  /**
   * Change this if you want to change poll timing.
   */
  pollInterval = 30000;

  http: HttpClient;
  cdr: ChangeDetectorRef;
  appConfig: AppConfigService;

  /**
   * Map API result into Resource list
   */
  abstract map(value: RList): R[];

  /**
   * The column definitions
   */
  abstract getColumnDefs(): ColumnDef[];

  /**
   * The source of the table's state.
   * One particular usage is the activatedRoute's params.
   */
  abstract getTableState$(): Observable<TableState>;

  /**
   * The fetch method to be called upon various parameters.
   */
  abstract fetchResources(params: FetchParams): Observable<RList | Error>;

  /**
   * Set new table state. Returns true if state is successfully updated.
   */
  abstract setTableState(newState: TableState): Promise<boolean>;

  /**
   * For a resource and colume name, returns its associated data.
   */
  getCellValue(item: R, colName: string) {
    switch (colName) {
      case 'name':
      case 'namespace':
      case 'creationTimestamp':
      case 'labels':
        return item.objectMeta[colName];
      case 'kind':
        return item.typeMeta[colName];
      default:
        return (item as any)[colName];
    }
  }

  /**
   * To be used within a
   */
  trackByFn(index: number, rowItem: R) {
    return rowItem.typeMeta.kind + '.' + rowItem.objectMeta.uid + '.' + index;
  }

  /**
   * Called when data is updated edgerly.
   */
  onUpdate(item: any) {
    this.updated$.next(item);
  }

  ngOnInit() {
    // The ordering of the following is significant.
    this.initStateParams$();
    this.initFetchParams$();
    this.initRawResponse$();
    this.initColumns$();
    this.initList$();
  }

  sortData(sort: Sort) {
    this.setTableState(sort);
  }

  async searchByName(name: string) {
    // Only mark as searching state when set state success.
    this.searching = await this.setTableState({
      search: name,
      // 搜索的时候应设空的active和directiion
      active: '',
      direction: '',
    });
    this.cdr.markForCheck();
  }

  namespaceChanged(namespace: string) {
    this.setTableState({ namespace });
  }

  onPageChange(curIndex: number) {
    this.setTableState({ pageIndex: curIndex - 1 });
  }

  onPageSizeChange(pageSize: number) {
    this.setTableState({ pageIndex: 0, pageSize });
  }

  initStateParams$() {
    const tableState$ = this.getTableState$();

    this.namespace$ = tableState$.pipe(map(params => params.namespace));

    this.pageParams$ = tableState$.pipe(
      map(params => ({
        pageSize: params.pageSize || 20,
        pageIndex: params.pageIndex || 0,
      })),
    );

    this.sort$ = tableState$.pipe(
      map(params => ({
        // 搜索的时候应设空active和directiion
        active: params.active || (!params.search && 'creationTimestamp'),
        direction: params.direction || (!params.search && 'desc'),
      })),
    );

    this.search$ = tableState$.pipe(map(params => params.search || ''));
  }

  /**
   * Init fetching params to be used when fetching list result.
   */
  initFetchParams$() {
    this.fetchParams$ = combineLatest(
      this.namespace$,
      this.search$,
      this.sort$,
      this.pageParams$,

      // Polling pulses
      this.poll$,
      this.updated$,
    ).pipe(
      map(([namespace, search, sort, pageParams]) => ({
        namespace,
        search,
        sort,
        pageParams,
      })),
    );
    return this.fetchParams$;
  }

  /**
   * Initialize raw response of the given resource. The result will be used for
   * displaying rows data
   */
  initRawResponse$(): Observable<RList | HttpErrorResponse> {
    this.rawResponse$ = this.fetchParams$.pipe(
      debounceTime(50),
      tap(() => {
        this.fetching = true;
        this.showZeroState = false;
        this.cdr.markForCheck();
      }),
      switchMap(params =>
        this.fetchResources(params).pipe(catchError(error => of(error))),
      ),
      tap(res => {
        this.fetching = false;
        this.searching = false;
        this.showZeroState =
          isHttpErrorResponse(res) || (res as RList).listMeta.totalItems === 0;
        this.cdr.markForCheck();
        this.debouncedUpdate();
      }),
      publishReplay(1),
      refCount(),
    );

    return this.rawResponse$;
  }

  /**
   * Init columns$ related observables.
   */
  initColumns$() {
    this.columnDefs = this.getColumnDefs();

    // Should hide Metrics column when metcis server is not enabled
    if (!this.appConfig.metricsServerEnabled) {
      this.columnDefs = this.columnDefs.filter(
        colDef => colDef.name !== 'metrics',
      );
    }
    const allColumns = this.columnDefs.map(colDef => colDef.name);
    const columnsWithoutNamespace = allColumns.filter(
      column => column !== 'namespace',
    );

    this.columns$ = this.namespace$.pipe(
      map(namespace => (namespace ? columnsWithoutNamespace : allColumns)),
    );
  }

  /**
   * Init list$ related observables.
   */
  initList$() {
    this.listMeta$ = this.rawResponse$.pipe(
      filter(res => isNotHttpErrorResponse(res)),
      map((res: RList) => res.listMeta),
    );

    this.list$ = this.rawResponse$.pipe(
      map(res => (isNotHttpErrorResponse(res) ? this.map(res) : [])),
    );
  }

  constructor(injector: Injector) {
    this.http = injector.get(HttpClient);
    this.cdr = injector.get(ChangeDetectorRef);
    this.appConfig = injector.get(AppConfigService);

    this.debouncedUpdate = debounce(() => {
      this.poll$.next(null);
    }, this.pollInterval);
  }
}
