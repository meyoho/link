import { HttpClient } from '@angular/common/http';
import { Injector, OnInit } from '@angular/core';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
import { NamespaceList } from '~api/backendapi';
import { KubernetesResource } from '~api/raw-k8s';

export abstract class BaseKubernetesResourceFormComponent<
  R extends KubernetesResource
> extends BaseResourceFormGroupComponent<R> implements OnInit {
  namespaces$: Observable<string[]>;

  http: HttpClient;

  constructor(injector: Injector) {
    super(injector);
    this.http = injector.get(HttpClient);
  }

  // User need to provide this:
  abstract readonly kind: string;

  ngOnInit(): void {
    super.ngOnInit();

    this.namespaces$ = this.http
      .get<NamespaceList>('api/v1/permission_namespaces')
      .pipe(
        map(({ namespaces }) =>
          namespaces.map(namespaceMeta => namespaceMeta.objectMeta.name),
        ),
        publishReplay(1),
        refCount(),
      );
  }

  adaptFormModel(resource: R) {
    resource = Object.assign({}, resource, this.getDefaultFormModel());
    // Remove this for ease of mutating.
    if (resource.metadata) {
      delete resource.metadata.resourceVersion;
    }
    return resource;
  }

  get currentNamespace() {
    return this.form.get('metadata.namespace').value;
  }
}
