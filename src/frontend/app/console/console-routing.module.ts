import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ConsoleComponent } from './console.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ConsoleComponent,
        children: [
          {
            path: 'overview',
            loadChildren: 'app/features/overview/module#OverviewModule',
          },
          {
            path: 'pod',
            loadChildren: 'app/features/pod/pod.module#PodModule',
          },
          {
            path: 'replicaset',
            loadChildren:
              'app/features/replicaset/replica-set.module#ReplicaSetModule',
          },
          {
            path: 'deployment',
            loadChildren: 'app/features/deployment/module#DeploymentModule',
          },
          {
            path: 'cronjob',
            loadChildren: 'app/features/cronjob/module#CronJobModule',
          },
          {
            path: 'job',
            loadChildren: 'app/features/job/module#JobModule',
          },
          {
            path: 'statefulset',
            loadChildren: 'app/features/statefulset/module#StatefulSetModule',
          },
          {
            path: 'persistentvolume',
            loadChildren:
              'app/features/persistentvolume/persistent-volume.module#PersistentVolumeModule',
          },
          {
            path: 'persistentvolumeclaim',
            loadChildren:
              'app/features/persistentvolumeclaim/persistent-volume-claim.module#PersistentVolumeClaimModule',
          },
          {
            path: 'storageclass',
            loadChildren:
              'app/features/storageclass/storage-class.module#StorageClassModule',
          },
          {
            path: 'daemonset',
            loadChildren: 'app/features/daemonset/module#DaemonSetModule',
          },
          {
            path: 'node',
            loadChildren: 'app/features/node/node.module#NodeModule',
          },
          {
            path: 'namespace',
            loadChildren: 'app/features/namespace/module#NamespaceModule',
          },
          {
            path: 'service',
            loadChildren: 'app/features/service/service.module#ServiceModule',
          },
          {
            path: 'ingress',
            loadChildren: 'app/features/ingress/ingress.module#IngressModule',
          },
          {
            path: 'networkpolicy',
            loadChildren:
              'app/features/networkpolicy/network-policy.module#NetworkPolicyModule',
          },
          {
            path: 'configmap',
            loadChildren: 'app/features/configmap/module#ConfigMapModule',
          },
          {
            path: 'secret',
            loadChildren: 'app/features/secret/secret.module#SecretModule',
          },
          {
            path: 'others',
            loadChildren: 'app/features/others/others.module#OthersModule',
          },
          {
            path: 'administrator',
            loadChildren:
              'app/features/administrator/administrator.module#AdministratorModule',
          },
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'overview',
          },
        ],
      },
    ]),
  ],
  exports: [RouterModule],
})
export class ConsoleRoutingModule {}
