import { Component, OnInit } from '@angular/core';
import { get } from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppConfigService, AuthService } from '../../services';
import { TranslateService } from '../../translate';

const EMAIL_REG = /(\w+([-+.]\w+)*)@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

@Component({
  selector: 'alk-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrls: ['./account-menu.component.scss'],
})
export class AccountMenuComponent implements OnInit {
  isActive = false;
  isDevops = true;
  displayName$: Observable<string>;

  constructor(
    public auth: AuthService,
    private translate: TranslateService,
    public appConfig: AppConfigService,
  ) {}

  ngOnInit() {
    this.displayName$ = this.auth.state$.pipe(
      map(state => {
        if (get(state, 'account.name')) {
          return get(state, 'account.name');
        }
        if (EMAIL_REG.test(get(state, 'account.email'))) {
          return RegExp.$1;
        }
        return get(state, 'account.email');
      }),
    );
  }

  logOut() {
    this.auth.logout();
  }

  toggleLang() {
    this.translate.changeLanguage(this.translate.otherLang);
  }

  get currentLang() {
    return this.translate.currentLang;
  }

  toPortal() {
    this.auth.redirectToPortal();
  }
}
