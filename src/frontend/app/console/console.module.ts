import {
  LayoutModule as AuiLayoutModule,
  PlatformNavModule as AuiNavModule,
} from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared';

import { AccountMenuComponent } from './account-menu/account-menu.component';
import { ConsoleRoutingModule } from './console-routing.module';
import { ConsoleComponent } from './console.component';

@NgModule({
  imports: [
    AuiLayoutModule,
    AuiNavModule,
    SharedModule,
    RouterModule,
    PortalModule,
    ConsoleRoutingModule,
  ],
  declarations: [ConsoleComponent, AccountMenuComponent],
})
export class ConsoleModule {}
