import { ThemeService } from '@alauda/theme';
import {
  DialogRef,
  DialogService,
  DialogSize,
  NavGroupConfig,
  NavItemConfig,
  NotificationService,
} from '@alauda/ui';
import { animate, style, transition, trigger } from '@angular/animations';
import { TemplatePortal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  TemplateRef,
} from '@angular/core';
import {
  ActivatedRoute,
  NavigationEnd,
  NavigationStart,
  Router,
} from '@angular/router';
import { remove } from 'lodash';
import { Observable, Subscription, combineLatest } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';
import { TranslateService } from '~app/translate';

import {
  AuthService,
  ResourceDataService,
  TemplateHolderType,
  TerminalService,
  UiStateService,
} from '../services';
import { createActions } from '../utils/code-editor-config';

const DEFAULT_NAV_CONFIG: NavGroupConfig[] = [
  {
    items: [
      {
        label: 'nav_overview',
        key: 'nav_overview',
        icon: 'basic:bar_chart_s',
        name: 'nav_overview',
        routerLink: ['/overview'],
      },
      {
        icon: 'basic:sitemap_s',
        name: 'nav_compute',
        label: 'nav_compute',
        key: 'nav_compute',
        children: [
          {
            label: 'nav_deployment',
            key: 'nav_deployment',
            name: 'nav_deployment',
            routerLink: ['/deployment'],
          },
          {
            name: 'nav_daemonset',
            label: 'nav_daemonset',
            key: 'nav_daemonset',
            routerLink: ['/daemonset'],
          },
          {
            label: 'nav_statefulset',
            key: 'nav_statefulset',
            name: 'nav_statefulset',
            routerLink: ['/statefulset'],
          },
          {
            label: 'nav_job',
            key: 'nav_job',
            name: 'nav_job',
            routerLink: ['/job'],
          },
          {
            label: 'nav_cronjob',
            key: 'nav_cronjob',
            name: 'nav_cronjob',
            routerLink: ['/cronjob'],
          },
          {
            label: 'nav_replica_set',
            key: 'nav_replica_set',
            name: 'nav_replica_set',
            routerLink: ['/replicaset'],
          },
          {
            label: 'nav_pod',
            key: 'nav_pod',
            name: 'nav_pod',
            routerLink: ['/pod'],
          },
        ],
      },
      {
        icon: 'basic:gears_s',
        label: 'nav_config',
        key: 'nav_config',
        name: 'nav_config',
        children: [
          {
            label: 'nav_configmap',
            key: 'nav_configmap',
            name: 'nav_configmap',
            routerLink: ['/configmap'],
          },
          {
            label: 'nav_secret',
            key: 'nav_secret',
            name: 'nav_secret',
            routerLink: ['/secret'],
          },
        ],
      },
      {
        icon: 'basic:internet',
        label: 'nav_network',
        key: 'nav_network',
        name: 'nav_network',
        children: [
          {
            label: 'nav_service',
            key: 'nav_service',
            name: 'nav_service',
            routerLink: ['/service'],
          },
          {
            label: 'nav_ingress',
            key: 'nav_ingress',
            name: 'nav_ingress',
            routerLink: ['/ingress'],
          },
          {
            label: 'nav_networkpolicy',
            key: 'nav_networkpolicy',
            name: 'nav_networkpolicy',
            routerLink: ['/networkpolicy'],
          },
        ],
      },
      {
        icon: 'basic:storage_s',
        label: 'nav_storage',
        key: 'nav_storage',
        name: 'nav_storage',
        children: [
          {
            label: 'nav_persistentvolumeclaim',
            key: 'nav_persistentvolumeclaim',
            name: 'nav_persistentvolumeclaim',
            routerLink: ['/persistentvolumeclaim'],
          },
          {
            label: 'nav_persistentvolume',
            key: 'nav_persistentvolume',
            name: 'nav_persistentvolume',
            routerLink: ['/persistentvolume'],
          },
          {
            label: 'nav_storageclass',
            key: 'nav_storageclass',
            name: 'nav_storageclass',
            routerLink: ['/storageclass'],
          },
        ],
      },
      {
        icon: 'basic:server_s',
        label: 'nav_cluster',
        key: 'nav_cluster',
        name: 'nav_cluster',
        children: [
          {
            label: 'nav_node',
            key: 'nav_node',
            name: 'nav_node',
            routerLink: ['/node'],
          },
          {
            label: 'nav_namespace',
            key: 'nav_namespace',
            name: 'nav_namespace',
            routerLink: ['/namespace'],
          },
        ],
      },
      {
        icon: 'basic:other_s',
        label: 'nav_other_resources',
        key: 'nav_other_resources',
        name: 'nav_other_resources',
        children: [
          {
            label: 'nav_other_resources_namespaced',
            key: 'nav_other_resources_namespaced',
            name: 'nav_other_resources_namespaced',
            routerLink: ['/others', 'namespaced'],
          },
          {
            label: 'nav_other_resources_clustered',
            key: 'nav_other_resources_clustered',
            name: 'nav_other_resources_clustered',
            routerLink: ['/others', 'clustered'],
          },
        ],
      },
      {
        icon: 'basic:admin_set_s',
        label: 'nav_administrator_config',
        key: 'nav_administrator_config',
        name: 'nav_administrator_config',
        children: [
          {
            label: 'nav_roles',
            key: 'nav_roles',
            name: 'nav_roles',
            routerLink: ['/administrator', 'roles'],
          },
          {
            label: 'nav_role-bindings',
            key: 'nav_role-bindings',
            name: 'nav_role-bindings',
            routerLink: ['/administrator', 'role-bindings'],
          },
          {
            label: 'nav_users',
            key: 'nav_users',
            name: 'nav_users',
            routerLink: ['/administrator', 'users'],
            adminOn: true,
          },
        ],
      },
    ],
  },
];

@Component({
  selector: 'alk-console',
  templateUrl: 'console.component.html',
  styleUrls: ['console.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition('* => *', [
        style({ transform: 'scale(0.99) translateY(5px)', opacity: '0.1' }),
        animate(
          '0.3s ease',
          style({ transform: 'scale(1) translateY(0)', opacity: '1' }),
        ),
      ]),
    ]),
  ],
})
export class ConsoleComponent implements OnInit, AfterViewInit, OnDestroy {
  navTheme$ = this.themeService.navTheme$;
  initialized = false;

  pageHeaderContentTemplate$: Observable<TemplatePortal<any>>;

  // Default nav config
  navConfig$: Observable<NavGroupConfig[]>;
  activatedKey$: Observable<string>;
  activatedRoute: ActivatedRoute;

  // Path pattern: '{category_name}.{primary_name}.{name}'
  expandedPaths: string[] = [];

  editorOptions = {
    language: 'yaml',
  };

  edtiorActions = createActions;

  yamlCreating = false;
  yamlInputValue = '';
  yamlOriginalValue = '';
  openedYamlDialog: DialogRef<any>;

  showLoadingBar$: Observable<boolean>;

  private routerEndSub = Subscription.EMPTY;

  constructor(
    public uiState: UiStateService,
    private dialog: DialogService,
    private cdr: ChangeDetectorRef,
    private resourceData: ResourceDataService,
    private router: Router,
    private auth: AuthService,
    private terminal: TerminalService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private themeService: ThemeService,
    injector: Injector,
  ) {
    this.activatedRoute = injector.get(ActivatedRoute);
  }

  ngOnInit(): void {
    this.navConfig$ = combineLatest(
      this.auth.isAdmin$,
      this.translate.currentLang$,
    ).pipe(
      map(([isAdmin, _]) => {
        const configs = DEFAULT_NAV_CONFIG.map(group => {
          const navGroupConfig: NavGroupConfig = {
            items: [],
          };
          if (group.title) {
            navGroupConfig.title = group.title;
          }
          navGroupConfig.items = this.processNavItemsWithAdminPremission(
            group.items,
            isAdmin,
          );
          return navGroupConfig;
        });
        remove(configs, config => !config.items.length);
        return configs;
      }),
      switchMap(raw => this.mapToNavConfig(raw)),
      publishReplay(1),
      refCount(),
    );

    this.pageHeaderContentTemplate$ = this.uiState
      .getTemplateHolder(TemplateHolderType.PageHeaderContent)
      // Since template may be triggered in a child component,
      // we must do another change detection here.
      .templatePortal$.pipe(tap(() => this.cdr.markForCheck()));

    this.routerEndSub = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        startWith({}),
        debounceTime(50),
      )
      .subscribe(() => {
        this.initialized = true;
        this.cdr.markForCheck();
      });

    const hasPendingRequest$ = this.uiState.requests$.pipe(
      map(requests => requests.size > 0),
      debounceTime(0),
      tap(() => this.cdr.markForCheck()),
    );

    const navigating$ = this.router.events.pipe(
      filter(
        event =>
          event instanceof NavigationEnd || event instanceof NavigationStart,
      ),
      map(event => event instanceof NavigationStart),
    );

    this.showLoadingBar$ = combineLatest(hasPendingRequest$, navigating$).pipe(
      map(([hasPendingRequest, navigating]) => hasPendingRequest || navigating),
    );

    this.activatedKey$ = combineLatest(
      this.navConfig$.pipe(map(config => this.flatNavConfig(config))),
      this.router.events.pipe(
        filter(event => event instanceof NavigationEnd),
        map((event: NavigationEnd) => event.url),
        startWith(this.router.url),
        distinctUntilChanged(),
        switchMap(() => this.activatedRoute.paramMap),
      ),
    ).pipe(
      map(([items, _]) => {
        return items.find(item => {
          return this.router.isActive(item.left.routerLink.join('/'), false);
        });
      }),
      filter(config => !!config),
      tap(config => {
        this.uiState.setActiveItemInfo(config.path);
      }),
      map(config => config.left.key),
      distinctUntilChanged(),
      publishReplay(1),
      refCount(),
    );
  }

  protected flatNavConfig(
    config: NavGroupConfig[],
  ): { path: NavItemConfig[]; left: NavItemConfig }[] {
    const pickChildren = (
      item: NavItemConfig,
      path: NavItemConfig[] = [],
    ): { path: NavItemConfig[]; left: NavItemConfig }[] => {
      return item.children
        ? item.children.reduce(
            (prev, curr) => prev.concat(pickChildren(curr, path.concat(item))),
            [],
          )
        : [{ path: path.concat(item), left: item }];
    };
    return config.reduce(
      (prev, curr) =>
        prev.concat(curr.items.reduce((p, c) => p.concat(pickChildren(c)), [])),
      [],
    );
  }

  ngOnDestroy(): void {
    this.routerEndSub.unsubscribe();
  }

  ngAfterViewInit(): void {}

  private mapToNavConfig(
    navGroupConfigs: NavGroupConfig[],
  ): Observable<NavGroupConfig[]> {
    return this.translate.currentLang$.pipe(
      map(() => {
        return navGroupConfigs.map(group => {
          const groupConfig: NavGroupConfig = {
            items: [],
          };
          if (group.title) {
            groupConfig.title = this.translate.get(group.title);
          }
          groupConfig.items = group.items.map(config => {
            let newConfig: NavItemConfig = {
              label: config.label,
              key: config.key,
            };
            if (config.children) {
              newConfig.children = config.children.map(child => ({
                ...child,
                label: this.translate.get(child.label),
              }));
            }
            newConfig = {
              ...config,
              ...newConfig,
              label: this.translate.get(config.label),
            };
            return newConfig;
          });
          return groupConfig;
        });
      }),
    );
  }

  handleActivatedItemChange(config: NavItemConfig) {
    this.router.navigate(config.routerLink);
  }

  getPath(categoryIndex: number, ...items: NavItemConfig[]) {
    return [categoryIndex, ...items.map(item => item.name)].join('.');
  }

  showYamlCreateDialog(templateRef: TemplateRef<any>) {
    this.yamlOriginalValue = this.yamlInputValue;
    this.openedYamlDialog = this.dialog.open(templateRef, {
      size: DialogSize.Large,
    });
  }

  async createResource(yaml: string) {
    this.yamlCreating = true;
    try {
      const res = await this.resourceData.createApply(yaml);
      if (res === 'cancel') {
        this.yamlCreating = false;
        return;
      }
      this.openedYamlDialog.close();
      this.openedYamlDialog = null;
    } catch ({ error }) {
      this.auiNotificationService.error({
        content: error.message,
      });
    }
    this.yamlCreating = false;
    this.cdr.markForCheck();
  }

  onPageAnimationStart() {
    // A dirty fix to make sure when router component changes, the page keeps at
    // the top.
    document.querySelector('.aui-layout__page').scrollTo(0, 0);
  }

  openKubectl() {
    this.terminal.openTerminal({
      kubectl: true,
    });
  }

  private processNavItemsWithAdminPremission(
    navItemConfigs: NavItemConfig[],
    isAdmin: boolean,
  ) {
    remove(navItemConfigs, item => {
      if (item.adminOn && !isAdmin) {
        return true;
      }
      if (item.children) {
        this.processNavItemsWithAdminPremission(item.children, isAdmin);
      }
      return false;
    });
    return navItemConfigs;
  }
}
