import { ThemeServiceConfig } from '@alauda/theme';

export const themeServiceConfig: ThemeServiceConfig = {
  api: 'api/v1/configuration',
  productName: 'alauda-kubernetes',
  navLogoSelector: '.nav-header__logo',
  storageKey: 'platform_theme_aks',
};
