/**
 * Given a fraction number from 0 ~ 100, returns a color as an indicator of usage.
 */
export function getUsageFractionColor(fraction: number) {
  if (fraction > 90) {
    return '#ef8181';
  } else if (fraction > 70) {
    return '#f8ac58';
  } else {
    return '#48c2a8';
  }
}
