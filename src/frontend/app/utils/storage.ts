export const getParams = (search: string) => {
  if (!search) {
    return {};
  }
  const queryString = search.substr(1);
  if (!queryString) {
    return {};
  }

  return queryString.split('&').reduce((accum: any, segment: string) => {
    if (!segment) {
      return accum;
    }
    const [key, value] = segment.split('=');
    if (!key) {
      return accum;
    }
    return {
      ...accum,
      [key]: value,
    };
  }, {});
};
