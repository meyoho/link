// This function returns the detail page of a resource
import { Resource } from '~api/backendapi';

export interface ExtraParams {
  // Mode name. default is view.
  mode?: 'update' | 'view';

  // Tab name
  tab?: string;
}

export function isInOthersCategory(_type: string) {
  return ![
    'pod',
    'replicaset',
    'deployment',
    'statefulset',
    'persistentvolume',
    'persistentvolumeclaim',
    'storageclass',
    'daemonset',
    'service',
    'ingress',
    'networkpolicy',
    'configmap',
    'secret',
    'node',
    'namespace',
    'job',
    'cronjob',
    'role',
    'rolebinding',
    'clusterrole',
    'clusterrolebinding',
  ].includes(_type.toLowerCase());
}

export interface DetailRouteParams {
  kind?: string;
  name?: string;
  namespace?: string;
  apiVersion?: string;
}

function isResource(
  resourceOrParams: Resource | DetailRouteParams,
): resourceOrParams is Resource {
  return (
    !!resourceOrParams &&
    !!(resourceOrParams as Resource).typeMeta &&
    !!(resourceOrParams as Resource).objectMeta
  );
}

/**
 * Usage: router.navigate(getNavigateRoutes(resource))
 */
export function getDetailRoutes(
  resourceOrParams: Resource | DetailRouteParams,
  extraOptions: ExtraParams = {},
) {
  if (!resourceOrParams) {
    return [];
  }

  let routeParams: DetailRouteParams = {};

  if (isResource(resourceOrParams)) {
    routeParams = {
      kind: resourceOrParams.typeMeta.kind,
      apiVersion: resourceOrParams.typeMeta.apiVersion,
      name: resourceOrParams.objectMeta.name,
      namespace: resourceOrParams.objectMeta.namespace,
    };
  } else {
    routeParams = resourceOrParams;
  }

  if (routeParams.kind) {
    let paths = ['/'];

    const apiVersion = routeParams.apiVersion || 'v1';

    const params: { [key: string]: string } = {
      name: routeParams.name,
      namespace: routeParams.namespace || '',
      kind: routeParams.kind,
      apiVersion,

      ...extraOptions,
    };

    // Since we are treating limitrange/resourcequota in namespace routing, we need
    // to have some hack here:
    if (
      ['resourcequota', 'limitrange'].includes(routeParams.kind.toLowerCase())
    ) {
      paths = [...paths, 'namespace', routeParams.kind.toLowerCase()];
    } else if (
      // for administrator resources role/rolebinding:
      ['role', 'clusterrole'].includes(routeParams.kind.toLowerCase())
    ) {
      return [
        '/administrator/roles/detail',
        {
          name: routeParams.name,
          namespace: routeParams.namespace || '',
        },
      ];
    } else if (
      ['rolebinding', 'clusterrolebinding'].includes(
        routeParams.kind.toLowerCase(),
      )
    ) {
      return [
        '/administrator/role-bindings/detail',
        {
          name: routeParams.name,
          namespace: routeParams.namespace || '',
        },
      ];
    } else if (isInOthersCategory(routeParams.kind)) {
      paths = [
        ...paths,
        'others',
        routeParams.namespace ? 'namespaced' : 'clustered',
      ];
    } else {
      paths = [...paths, routeParams.kind.toLowerCase()];
    }

    paths = [...paths, extraOptions.mode === 'update' ? 'update' : 'detail'];

    return [...paths, params];
  } else {
    return [];
  }
}
