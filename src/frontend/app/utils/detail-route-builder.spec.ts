import {
  LimitRange,
  Resource,
  ResourceQuota,
  RoleBindingListItem,
  RoleRef,
} from '@api/backendapi';

import { getDetailRoutes } from './detail-route-builder';

describe('detail route builder', () => {
  let route: any[];
  it('get role detail route from role', () => {
    const role: Resource = {
      objectMeta: { name: 'resource1', namespace: 'namespace1', uid: 'uid1' },
      typeMeta: { kind: 'role', apiVersion: 'api' },
    };

    route = getDetailRoutes(role);

    expect(route).toEqual([
      '/administrator/roles/detail',
      {
        name: role.objectMeta.name,
        namespace: role.objectMeta.namespace,
      },
    ]);
  });

  it('get role detail route from roleRef', () => {
    const roleRef: RoleRef = {
      kind: 'Role',
      name: 'Role1',
      namespace: 'namespace1',
    };

    route = getDetailRoutes(roleRef);

    expect(route).toEqual([
      '/administrator/roles/detail',
      {
        name: roleRef.name,
        namespace: roleRef.namespace,
      },
    ]);
  });

  it('get rolebinding detail route', () => {
    const roleBinding: RoleBindingListItem = {
      objectMeta: { name: 'resource1', namespace: 'namespace1', uid: 'uid1' },
      typeMeta: { kind: 'Rolebinding', apiVersion: 'api' },
      roleRef: {
        kind: 'Role',
        name: 'Role1',
        namespace: 'namespace1',
      },
    };

    route = getDetailRoutes(roleBinding);

    expect(route).toEqual([
      '/administrator/role-bindings/detail',
      {
        name: roleBinding.objectMeta.name,
        namespace: roleBinding.objectMeta.namespace,
      },
    ]);
  });

  it('get resourcequota detail route', () => {
    const resourceQuota: ResourceQuota = {
      objectMeta: { name: 'resource1', namespace: 'namespace1', uid: 'uid1' },
      typeMeta: { kind: 'resourcequota', apiVersion: 'api' },
      scopes: ['a', 'b'],
      statusList: { resource1: { used: 'aaa', hard: 'bbb' } },
    };

    route = getDetailRoutes(resourceQuota);

    expect(route).toEqual([
      '/',
      'namespace',
      'resourcequota',
      'detail',
      {
        apiVersion: resourceQuota.typeMeta.apiVersion,
        kind: resourceQuota.typeMeta.kind,
        name: resourceQuota.objectMeta.name,
        namespace: resourceQuota.objectMeta.namespace,
      },
    ]);
  });

  it('get limitrange detail route', () => {
    const limitrange: LimitRange = {
      objectMeta: { name: 'resource1', namespace: 'namespace1', uid: 'uid1' },
      typeMeta: { kind: 'limitrange', apiVersion: 'api' },
      limits: [
        {
          type: 'Container',
          min: { a: 'b' },
          max: { a: 'b' },
          default: { a: 'b' },
          defaultRequest: { a: 'b' },
          maxLimitRequestRatio: { a: 'b' },
        },
      ],
    };

    route = getDetailRoutes(limitrange);

    expect(route).toEqual([
      '/',
      'namespace',
      'limitrange',
      'detail',
      {
        apiVersion: limitrange.typeMeta.apiVersion,
        kind: limitrange.typeMeta.kind,
        name: limitrange.objectMeta.name,
        namespace: limitrange.objectMeta.namespace,
      },
    ]);
  });

  it('get others detail route', () => {
    const otherResource: Resource = {
      objectMeta: { name: 'resource1', namespace: 'namespace1', uid: 'uid1' },
      typeMeta: { kind: 'unknown', apiVersion: 'api' },
    };

    route = getDetailRoutes(otherResource);

    expect(route).toEqual([
      '/',
      'others',
      'namespaced',
      'detail',
      {
        apiVersion: otherResource.typeMeta.apiVersion,
        kind: otherResource.typeMeta.kind,
        name: otherResource.objectMeta.name,
        namespace: otherResource.objectMeta.namespace,
      },
    ]);
  });
});
