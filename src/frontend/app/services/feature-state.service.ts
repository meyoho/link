import { Injectable } from '@angular/core';

const LOCAL_STORAGE_NAMESPACE = 'alk';

/**
 * Acts as a general feature state store
 */
@Injectable({
  providedIn: 'root',
})
export class FeatureStateService {
  constructor() {}

  set(id: string, value: any) {
    window.localStorage.setItem(
      `${LOCAL_STORAGE_NAMESPACE}.${id}`,
      JSON.stringify(value),
    );
  }

  get(id: string) {
    return JSON.parse(
      window.localStorage.getItem(`${LOCAL_STORAGE_NAMESPACE}.${id}`),
    );
  }
}
