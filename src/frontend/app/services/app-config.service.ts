// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface AppConfig {
  serverTime: number;
  enableAnounymous: boolean;
  enableMetricsServer: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class AppConfigService {
  private readonly configPath_ = 'api/appConfig.json';
  private config_: AppConfig;
  private initTime_: number;
  private initPromise: Promise<AppConfig>;

  constructor(private readonly http: HttpClient) {}

  init(): Promise<AppConfig> {
    if (!this.initPromise) {
      this.initPromise = this.getAppConfig$().toPromise();
      this.initPromise.then(config => {
        this.config_ = config;
        this.initTime_ = new Date().getTime();
      });
    }
    return this.initPromise;
  }

  getAppConfig() {
    return this.initPromise;
  }

  getAppConfig$(): Observable<AppConfig> {
    return this.http.get<AppConfig>(this.configPath_);
  }

  getServerTime(): Date {
    if (this.config_ && this.config_.serverTime) {
      const elapsed = new Date().getTime() - this.initTime_;
      return new Date(this.config_.serverTime + elapsed);
    } else {
      return new Date();
    }
  }

  get anounymousEnabled() {
    return this.config_ && this.config_.enableAnounymous;
  }

  get metricsServerEnabled() {
    return this.config_ && this.config_.enableMetricsServer;
  }
}
