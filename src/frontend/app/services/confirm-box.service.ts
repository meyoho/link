import { ConfirmDialogConfig, DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import { TranslateService } from '../translate';

@Injectable({
  providedIn: 'root',
})
export class ConfirmBoxService {
  constructor(
    private dialog: DialogService,
    private translate: TranslateService,
  ) {}

  confirm(options: ConfirmDialogConfig): Promise<boolean> {
    return this.dialog
      .confirm({
        ...options,
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => true)
      .catch(() => false);
  }

  delete(options: { kind: string; name: string }) {
    return this.confirm({
      title: this.translate.get('delete_resource_confirm', {
        kind: this.translate.get(options.kind.toLowerCase()),
        name: this.translate.get(options.name),
      }),
    });
  }
}
