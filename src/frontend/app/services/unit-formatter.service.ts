import { Injectable } from '@angular/core';

import { TranslateService } from '../translate';

const MEMORY_DEF = {
  base: 1024,
  powerSuffixes: ['', 'KB', 'MB', 'GB', 'TB', 'PB'],
};

const CPU_DEF = {
  base: 1000,
  powerSuffixes: ['', 'k', 'M', 'G', 'T'],
};

const UNIT_DEFS = {
  memory: MEMORY_DEF,
  cpu: CPU_DEF,
};

@Injectable({
  providedIn: 'root',
})
export class UnitFormatterService {
  constructor(private translate: TranslateService) {}

  transform(value: number, type: 'memory' | 'cpu'): string {
    if (value === undefined || value === null) {
      return '-';
    }

    const base = UNIT_DEFS[type].base;
    let powerSuffixes = UNIT_DEFS[type].powerSuffixes;
    let divider = 1;
    let power = 0;

    if (type === 'cpu') {
      value = value / base;
      const CPU_UNIT = this.translate.get('cores');
      powerSuffixes = powerSuffixes.map(item => `${item} ${CPU_UNIT}`);
    }

    while (value / divider > base && power < powerSuffixes.length - 1) {
      divider *= base;
      power += 1;
    }

    let formatted = (+(value / divider).toFixed(2)).toString();
    const suffix = powerSuffixes[power];

    if (formatted === '0') {
      formatted = '< 0.01';
    }
    return suffix ? `${formatted} ${suffix}` : formatted;
  }
}
