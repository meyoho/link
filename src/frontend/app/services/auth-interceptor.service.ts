import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (req.url.includes('api/v1')) {
      req = req.clone({
        setHeaders: {
          // Currently all tokens are seen as Bearer token.
          Authorization: `Bearer ${this.auth.getToken()}`,
        },
      });
    }
    return next.handle(req).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            // TODO
          }
          if (req.url.includes('api/v1') && !req.url.includes('api/v1/login')) {
            this.auth.firstApiInvoked$.next(true);
          }
        },
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              // TODO
              this.auth.logout();
            }
            if (err.status === 402) {
              this.auth.redirectToPortal();
            }
          }
        },
      ),
    );
  }
}
