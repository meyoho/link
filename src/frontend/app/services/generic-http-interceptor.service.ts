import { MessageService } from '@alauda/ui';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { TranslateService } from '../translate';

import { UiStateService } from './ui-state.service';

/**
 * Generic http interceptors
 */
@Injectable()
export class GenericHttpInterceptor implements HttpInterceptor {
  constructor(
    private auiMessageService: MessageService,
    private translate: TranslateService,
    private uiState: UiStateService,
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const onRequestComplete = () => {
      this.uiState.requests.delete(req);
      this.uiState.requests$.next(this.uiState.requests);
    };

    return new Observable<HttpEvent<any>>(observer => {
      this.uiState.requests.add(req);
      this.uiState.requests$.next(this.uiState.requests);

      const subscription = next.handle(req).subscribe(
        res => {
          observer.next(res);
        },
        response => {
          onRequestComplete();
          observer.error(response);
          if (!(response instanceof HttpErrorResponse)) {
            return;
          }
          if ([0, 500].includes(response.status)) {
            this.auiMessageService.warning({
              content: this.translate.get('500_error_toast'),
            });
          }
        },
        () => {
          onRequestComplete();
          observer.complete();
        },
      );

      return () => {
        onRequestComplete();
        subscription.unsubscribe();
      };
    });
  }
}
