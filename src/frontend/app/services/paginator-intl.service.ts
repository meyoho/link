import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { TranslateService } from '../translate';

@Injectable({
  providedIn: 'root',
})
export class CustomPaginatorIntlService {
  constructor(private translate: TranslateService) {}

  readonly changes: Subject<void> = new Subject<void>();

  get itemsPerPageLabel() {
    return this.translate.get('paginator_items_per_page_label');
  }

  get nextPageLabel() {
    return this.translate.get('paginator_next_page_label');
  }

  get previousPageLabel() {
    return this.translate.get('paginator_previous_page_label');
  }

  get firstPageLabel() {
    return this.translate.get('paginator_first_page_label');
  }

  get lastPageLabel() {
    return this.translate.get('paginator_last_page_label');
  }

  getTotalLabel = (length: number) => {
    return this.translate.get('paginator_get_total_label', {
      length: length,
    });
  };
}
