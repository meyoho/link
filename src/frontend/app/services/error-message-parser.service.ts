import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { TranslateService } from '../translate';

/**
 * Utility service for parsing
 */
@Injectable({
  providedIn: 'root',
})
export class ErrorMessageParserService {
  constructor(private translate: TranslateService) {}

  /**
   * Parse {@link AbstractControl} errors and returns the translated error messages.
   */
  getControlErrorMessage(
    control: AbstractControl,
    fieldName: string,
  ): string[] {
    const errors = control.errors || [];
    return Object.entries(errors).map(pair =>
      this.parseErrorObject(pair, fieldName),
    );
  }

  parseErrorObject([key, error]: [string, any], fieldName: string): string {
    switch (key) {
      case 'required':
        return this.translate.get('required');
      case 'minlength':
        return this.translate.get('minlength_message', {
          requiredLength: error.requiredLength,
        });
      case 'maxlength':
        return this.translate.get('maxlength_message', {
          requiredLength: error.requiredLength,
        });
      case 'length':
        return this.translate.get('length_message', {
          requiredLength: error.requiredLength,
        });
      case 'confirmPassword':
        return this.translate.get('confirm_password_incorrect');
      case 'pattern':
        return this.translate.get('invalid_pattern', {
          type: this.translate.get(fieldName),
        });
      case 'mobile_exist':
        return this.translate.get('mobile_exist');
      case 'passwordStrength':
        return this.translate.get('password_strength_too_low');
      case 'register_username':
        return this.translate.get('invalid_register_username');
      case 'register_email':
        return this.translate.get('invalid_register_email');
      case 'register_mobile':
        return this.translate.get('invalid_register_mobile');
      case 'register_mobile_vcode_invalid':
        return this.translate.get('invalid_register_vcode');
      default:
        return '';
    }
  }
}
