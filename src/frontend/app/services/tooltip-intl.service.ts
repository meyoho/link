import { Injectable } from '@angular/core';

import { TranslateService } from '../translate';

@Injectable({
  providedIn: 'root',
})
export class CustomTooltipIntlService {
  constructor(private translate: TranslateService) {}
  get copyTip() {
    return this.translate.get('copy_tooltip_click_to_copy');
  }

  get copySuccessTip() {
    return this.translate.get('copy_tooltip_copy_succeeded');
  }

  get copyFailTip() {
    return this.translate.get('copy_tooltip_copy_faield');
  }
}
