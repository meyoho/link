import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import * as jwtDecode from 'jwt-decode';
import { get } from 'lodash';
import { BehaviorSubject, Observable, Subject, Subscription, of } from 'rxjs';
import {
  catchError,
  concatMap,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { TranslateService } from '../translate';

import { AppConfigService } from './app-config.service';

const TOKEN = 'token';
const ACCESS_TOKEN = 'access_token';

export interface AccountInfo {
  iss?: string;
  sub?: string;
  aud?: string;
  exp?: number;
  iat?: number;
  azp?: string;
  at_hash?: string;
  email?: string;
  email_verified?: true;
  name?: string;
  token?: string;
  ext?: {
    is_admin?: boolean;
    conn_id?: boolean;
  };
}

interface AuthState {
  error: any;
  account: AccountInfo;
  accessToken: string;
  enabled: boolean;
  devopsHost: string;
  portalHost: string;
  authUrl: string;
}

const initialState: AuthState = {
  error: null,
  account: null,
  accessToken: '',
  enabled: true,
  devopsHost: '',
  portalHost: '',
  authUrl: '',
};

interface LoginMeta {
  settings: {
    enabled: boolean;
    client_id: string;
    client_secret: string;
    redirect_uri: string;
    auth_issuer: string;
    auth_namespace: string;
    root_ca_filepath: string;
    scopes: string[];
    response_type: string;
    custom_redirect_uri: {
      devops: string;
      dashboard: string;
    };
  };
  auth_url: string;
  errors: any[];
}

export const LOGIN_URL = 'api/v1/login';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements OnDestroy {
  account: AccountInfo;
  snapshot: AuthState = initialState;
  firstApiInvoked$ = new BehaviorSubject(false);
  private subscriptions: Subscription[] = [];
  private login$ = new Subject<any>();

  state$: Observable<AuthState> = this.login$.pipe(
    concatMap(params => {
      return this.http.get<LoginMeta>('api/v1/login').pipe(
        tap(result => {
          if (result.settings && result.settings.enabled && !result.auth_url) {
            throw Error(`auth_url must provided`);
          }
        }),
        map(result => ({
          ...initialState,
          devopsHost: get(result, 'settings.custom_redirect_uri.devops') || '',
          portalHost: get(result, 'settings.custom_redirect_uri.portal') || '',
          enabled: get(result, 'settings.enabled') || false,
          authUrl: result.auth_url,
        })),
        catchError((error: any) => of({ ...initialState, error })),
        concatMap(state => {
          if (state.error || !state.enabled) {
            return of(state);
          }

          if (params.id_token) {
            const account = this.decodeToken(params.id_token);
            const accessToken = params.access_token;
            return of({ ...state, account, accessToken });
          }

          if (params.code) {
            return this.http
              .get<LoginMeta>(`api/v1/login/callback?code=${params.code}`)
              .pipe(
                map((res: any) => {
                  const token = get(res, 'id_token');
                  const accessToken = get(res, ACCESS_TOKEN);
                  const account = this.decodeToken(token);
                  return {
                    ...state,
                    account,
                    accessToken,
                  };
                }),
              );
          }

          return of({
            ...state,
            account: this.decodeToken(localStorage.getItem(TOKEN)),
            accessToken: localStorage.getItem(ACCESS_TOKEN),
          });
        }),
        tap(state => {
          if (state.error) {
            return;
          }
          this.snapshot = state;
          if (!state.enabled) {
            return;
          }

          if (state.account && state.account.token) {
            localStorage.setItem(TOKEN, state.account.token);
            localStorage.setItem(ACCESS_TOKEN, state.accessToken);
          }
        }),
      );
    }),
    publishReplay(1),
    refCount(),
  );

  isAdmin$ = this.state$.pipe(
    map(state => !!get(state.account, 'ext.is_admin')),
  );

  authedOrNoNeed$: Observable<boolean> = this.state$.pipe(
    switchMap(state => {
      // Preflight an API call to make sure auth works properly.
      if (state.account && state.enabled && !this.appConfig.anounymousEnabled) {
        return this.http.get('api/v1/nodes').pipe(map(() => true));
      } else {
        return of(true);
      }
    }),
    catchError(() => of(false)),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private http: HttpClient,
    private appConfig: AppConfigService,
    private translate: TranslateService,
  ) {
    this.subscriptions.push(this.state$.subscribe());
  }

  getAccount(): AccountInfo {
    return this.snapshot.account || {};
  }

  isLogdedin(): boolean {
    return !!this.getToken();
  }

  logout() {
    localStorage.removeItem(TOKEN);
    localStorage.removeItem(ACCESS_TOKEN);
    if (this.snapshot.authUrl && window && !this.appConfig.anounymousEnabled) {
      window.location.href = this.snapshot.authUrl;
    }
  }

  getToken() {
    return this.getAccount().token;
  }

  getEmail() {
    return this.getAccount().email;
  }

  redirectToPortal(state = this.snapshot) {
    if (window && state.portalHost) {
      const queryParams = get(state.account, 'token')
        ? `?locale=${this.translate.currentLang}&id_token=${get(
            state.account,
            'token',
          )}&from=alauda-kubernetes`
        : '?from=alauda-kubernetes';
      window.location.href = state.portalHost + queryParams;
    }
  }

  private decodeToken(token: string) {
    try {
      const decoded = jwtDecode(token);
      return {
        token: token,
        name: get(decoded, 'name') || '',
        email: get(decoded, 'email') || '',
        ext: get(decoded, 'ext') || {},
      };
    } catch (err) {
      return null;
    }
  }

  login(params: any) {
    this.login$.next(params);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
