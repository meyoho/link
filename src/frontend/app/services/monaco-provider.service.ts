import { AlaudaMonacoProviderService } from '@alauda/code-editor';
import { Injectable } from '@angular/core';

const SIMPLE_K8S_SCHEMA = {
  type: 'object',
  required: ['kind', 'apiVersion'],
  properties: {
    kind: {
      type: ['string', 'null'],
    },
    apiVersion: {
      type: ['string', 'null'],
    },
    metadata: {
      type: 'object',
    },
  },
};

/**
 * Custom monaco provider to do some customizations.
 */
@Injectable({
  providedIn: 'root',
})
export class CustomMonacoProviderService extends AlaudaMonacoProviderService {
  async initMonaco() {
    await super.initMonaco();

    // Load custom yaml language service:
    await super.loadModule([
      // YAML language services are currently managed manually in thirdparty_lib
      'vs/basic-languages/monaco.contribution',
      'vs/language/yaml/monaco.contribution',
    ]);
    this.configYaml();
  }

  private configYaml() {
    monaco.languages.yaml.yamlDefaults.setDiagnosticsOptions({
      enableSchemaRequest: true,
      validate: true,
      schemas: [
        {
          uri: undefined,
          fileMatch: ['*'],
          schema: SIMPLE_K8S_SCHEMA,
        },
      ],
    });
  }
}
