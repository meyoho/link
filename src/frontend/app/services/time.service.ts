import { Injectable } from '@angular/core';
import * as distanceInWordsStrict from 'date-fns/distance_in_words_strict';
import * as format from 'date-fns/format';
import * as zh_cn from 'date-fns/locale/zh_cn/index.js';

import { TranslateService } from '../translate';

import { AppConfigService } from './app-config.service';

const milisecondsInAWeek = 1000 * 60 * 60 * 24 * 7;

@Injectable({
  providedIn: 'root',
})
export class TimeService {
  translatedUnits: { [key: string]: string[] };

  constructor(
    private readonly config: AppConfigService,
    private readonly translate: TranslateService,
  ) {}

  // Given time is too old and should display as normal timestamp.
  tooOld(value: string) {
    if (value == null) {
      return null;
    }

    // Current server time.
    const serverTime = this.config.getServerTime();

    // Current and given times in miliseconds.
    const currentTime = this.getCurrentTime_(serverTime);
    const givenTime = new Date(value).getTime();

    return currentTime - givenTime > milisecondsInAWeek;
  }

  transformRelative(value: string, addSuffix = true): string {
    if (value == null) {
      return '-';
    }

    // Current server time.
    const serverTime = this.config.getServerTime();

    // Current and given times in miliseconds.
    const currentTime = this.getCurrentTime_(serverTime);
    const givenTime = new Date(value).getTime();

    return distanceInWordsStrict(currentTime, givenTime, {
      addSuffix,
      locale: this.translate.currentLang === 'zh' ? zh_cn : 'en',
    });
  }

  transform(
    value: Date | string | number,
    dateFormat = 'YYYY-MM-DD HH:mm:ss',
  ): string {
    if (value === null) {
      return '-';
    }

    return format(value, dateFormat);
  }

  /**
   * Returns current time. If appConfig.serverTime is provided then it will be returned, otherwise
   * current client time will be used.
   */
  private getCurrentTime_(serverTime: Date): number {
    return serverTime ? serverTime.getTime() : new Date().getTime();
  }
}
