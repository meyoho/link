import { ConfirmType } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CanDeactivate } from '@angular/router';
import { first } from 'rxjs/operators';
import { BaseResourceMutatePageComponent } from '~app/abstract';

import { TranslateService } from '../translate';

import { ConfirmBoxService } from './confirm-box.service';

@Injectable({
  providedIn: 'root',
})
export class ResourceMutatePageDeactivateGuard
  implements CanDeactivate<BaseResourceMutatePageComponent> {
  constructor(
    private confirmBox: ConfirmBoxService,
    private translate: TranslateService,
  ) {}

  async canDeactivate(
    component: BaseResourceMutatePageComponent,
  ): Promise<boolean> {
    const form: NgForm = component.form;

    if (form && form.dirty && !form.submitted) {
      const method = await component.method$.pipe(first()).toPromise();
      const name = await component.name$.pipe(first()).toPromise();

      const title = this.translate.get(
        method === 'update'
          ? 'resource_mutate_page_dirty_title_update'
          : 'resource_mutate_page_dirty_title_create',
        {
          kind: this.translate.get(component.kind.toLowerCase()),
          name,
        },
      );

      const content = this.translate.get(
        method === 'update'
          ? 'resource_mutate_page_dirty_content_update'
          : 'resource_mutate_page_dirty_content_create',
        {
          kind: this.translate.get(component.kind.toLowerCase()),
          name,
        },
      );

      return await this.confirmBox.confirm({
        title,
        content,
        confirmType: ConfirmType.Warning,
      });
    } else {
      return Promise.resolve(true);
    }
  }
}
