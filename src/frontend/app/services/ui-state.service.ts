import { TemplatePortal } from '@angular/cdk/portal';
import { HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

/**
 * Provides a holder for UI templates to be used globally.
 */
export class TemplateHolder {
  private templatePortalSubject = new BehaviorSubject<TemplatePortal<any>>(
    undefined,
  );

  get templatePortal$() {
    return this.templatePortalSubject;
  }

  setTemplatePortal(templatePortal: TemplatePortal<any>) {
    this.templatePortalSubject.next(templatePortal);
  }
}

export enum TemplateHolderType {
  PageHeaderContent = 'PageHeaderContent',
}

export interface ActiveNavItem {
  label: string;
  routerLink?: string | any[];
}

export type ActiveNavItemInfo = ActiveNavItem[];

/**
 * Acts as a general ui state store
 */
@Injectable({
  providedIn: 'root',
})
export class UiStateService {
  private templateHolders = new Map<TemplateHolderType, TemplateHolder>();
  activeItemInfo$ = new BehaviorSubject<ActiveNavItemInfo>([]);

  // Pending requests
  requests = new Set<HttpRequest<any>>();

  // Pending requests in observable
  requests$ = new BehaviorSubject<Set<HttpRequest<any>>>(this.requests);

  private regiserTemplateHolder(id: TemplateHolderType) {
    if (this.templateHolders.has(id)) {
      throw new Error(`Template holder for ${id} has already registered!`);
    }
    this.templateHolders.set(id, new TemplateHolder());
  }

  // Will init template holder if not initialzed yet
  getTemplateHolder(id: TemplateHolderType) {
    if (!this.templateHolders.has(id)) {
      this.regiserTemplateHolder(id);
    }

    return this.templateHolders.get(id);
  }

  /**
   * NOTE: only set active item info in ConsoleComponent
   */
  setActiveItemInfo(activeNavItemInfo: ActiveNavItemInfo) {
    this.activeItemInfo$.next(activeNavItemInfo);
  }
}
