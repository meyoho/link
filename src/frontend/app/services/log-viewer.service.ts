import { Injectable } from '@angular/core';

/**
 * Log Viewer states.
 */
@Injectable({
  providedIn: 'root',
})
export class LogViewerService {
  private following_: boolean;
  private inverted_: boolean;
  private compact_: boolean;
  private showTimestamp_: boolean;
  private previous_: boolean;

  setFollowing() {
    this.following_ = !this.following_;
  }

  /**
   * @return {boolean}
   * @export
   */
  getFollowing() {
    return this.following_;
  }

  setInverted() {
    this.inverted_ = !this.inverted_;
  }

  /**
   * @return {boolean}
   * @export
   */
  getInverted() {
    return this.inverted_;
  }

  setCompact() {
    this.compact_ = !this.compact_;
  }

  /**
   * @return {boolean}
   * @export
   */
  getCompact() {
    return this.compact_;
  }

  setShowTimestamp() {
    this.showTimestamp_ = !this.showTimestamp_;
  }

  /**
   * @return {boolean}
   * @export
   */
  getShowTimestamp() {
    return this.showTimestamp_;
  }

  setPrevious() {
    this.previous_ = !this.previous_;
  }

  /**
   * @return {boolean}
   * @export
   */
  getPrevious() {
    return this.previous_;
  }

  /**
   * @param {string} pod
   * @param {string} container
   * @return {string}
   */
  getLogFileName(pod: string, container: string) {
    return `logs-from-${container}-in-${pod}.txt`;
  }
}
