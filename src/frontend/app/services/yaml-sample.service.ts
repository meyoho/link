import { Injectable } from '@angular/core';
import { safeDump, safeLoadAll } from 'js-yaml';
import { get, set } from 'lodash';

import { FeatureStateService } from './feature-state.service';

@Injectable({
  providedIn: 'root',
})
export class YamlSampleService {
  constructor(private featureState: FeatureStateService) {}

  feedDefaultNamespace(sampleYaml: string): string {
    if (!sampleYaml) {
      return;
    }
    try {
      let jsons = safeLoadAll(sampleYaml);
      const currentNamespace = this.featureState.get('namespace') || 'default';
      jsons = jsons.map(json => {
        if (get(json, 'metadata.namespace')) {
          set(json, 'metadata.namespace', currentNamespace);
        }
        return json;
      });

      sampleYaml = jsons.map(json => safeDump(json)).join('---\n');
    } catch (err) {
      console.error('Failed to convert sample, please check the format');
    }

    return sampleYaml;
  }
}
