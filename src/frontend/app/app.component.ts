import { ThemeService } from '@alauda/theme';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './services';
import { getParams } from './utils/storage';

@Component({
  selector: 'alk-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    public auth: AuthService,
    public router: Router,
    private themeService: ThemeService,
  ) {
    this.themeService.subscribePlatformTheme();
  }

  ngOnInit() {
    const hashParams = getParams(window.location.hash);
    const queryParams = getParams(window.location.search);
    if (hashParams['id_token']) {
      this.auth.login(hashParams);
      window.location.hash = '';
    } else {
      this.auth.login(queryParams);
    }
  }
}
