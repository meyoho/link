/**
 * 主要封装一些kubectl 的操作
 * Created by liuwei on 2018/3/2.
 */

import { CommonHttp } from './common.http';

import { CommonMethod } from './common.method';

export class CommonKubectl {
  private static _yamlFileName;

  static get yamlFileName() {
    return CommonKubectl._yamlFileName;
  }

  static set yamlFileName(value) {
    CommonKubectl._yamlFileName = value;
  }

  /**
   * 根据YAML 模版创建一个资源
   *
   * @parameter {filename} string 类型, 创建 test_data 文件下的YAML模版文件名
   * @parameter {parameter} string 类型, YAML模版里要替换的的参数
   * @example createResource('configmap.yaml', {'$NAME':'qqqqqq'})
   * @returns  string 类型，命令执行后在屏幕上输出 例如：configmap "qqqqqq" created
   */
  static createResource(filename, parameter, testscriptfilename) {
    // 根据YAML 文件的模版生成一个字符串格式的 YAML
    const yaml = CommonMethod.readyamlfile(filename, parameter);

    // 将生成的生成一个字符串格式的 YAML 写入到临时文件
    const tempfilename = CommonMethod.writeyamlfile(
      `${testscriptfilename}.yaml`,
      yaml,
    );

    // 执行kubectl create configmap 命令创建configmap ,以字符串的格式返回命令的输出
    // console.log('kubectl create -f ' + CommonKubectl.yamlFileName)
    CommonMethod.execCommand(`kubectl create -f ${tempfilename}`);
    return tempfilename;
  }

  /**
   * 只能删除由 createResource 方法创建的资源，调用前必须先执行 createResource
   *
   *
   * @example  deleteResourceByYmal()
   * @returns  string 类型，命令执行后在屏幕上输出 例如：configmap "qqqqqq" deleted
   */
  static deleteResourceByYmal(yamlFileName = CommonKubectl.yamlFileName) {
    console.log(`kubectl delete -f ${yamlFileName}`);
    return CommonMethod.execCommand(`kubectl delete -f ${yamlFileName}`);
  }

  /**
   * 根据资源类型，名称，删除一个资源
   *
   * @parameter {kind} string 类型, 资源类型 例如：configmap
   * @parameter {name} string 类型, 资源名称
   *
   * @example  deleteResource('configmap'， 'qqqqqq')
   * @returns  string 类型，命令执行后在屏幕上输出 例如：configmap "qqqqqq" deleted
   */
  static deleteResource(kind, name) {
    return CommonMethod.execCommand('kubectl delete ' + kind + ' ' + name);
  }

  /**
   * 根据资源类型，namespace, 获取资源
   *
   * @parameter {kind} string 类型, 资源类型 例如：configmap
   * @parameter {namespace} string 类型, 资源类型 例如：configmap
   *
   * @example  getResource('configmap'， 'default')
   * @returns  string 类型，命令执行后在屏幕上输出:
   * NAME               DATA      AGE
   liuwe8dd888        0         54m
   liuwe8ddf888       0         46m
   liuwei11d87dd232   2         12m
   liuwei123456       0         13h
   liuwei1234567      2         32m
   liuwei88888        0         1h
   liuwei88dd888      0         58m
   liuweitest         1         15h
   liuweitest123      1         15h
   */
  static getResource(kind, namespace = 'default') {
    return CommonMethod.execCommand('kubectl get ' + kind + ' -n ' + namespace);
  }

  /**
   * 删除创建资源时，临时生成的YAML文件
   *
   */
  static deleteYamlFile() {
    if (typeof CommonKubectl.yamlFileName !== 'undefined') {
      // 删除临时生成的YAML文件
      CommonMethod.execCommand('rm -f ' + CommonKubectl.yamlFileName);
    }
  }

  /**
   * 执行一个kubectl 命令
   *
   * @parameter {cmd} string 类型, kubectl 命令,
   *
   * @example  execKubectlCommand('kubectl get pods')
   * @returns  string 类型，命令执行后在屏幕上输出结果
   */
  static execKubectlCommand(cmd) {
    return CommonMethod.execCommand(cmd);
  }

  /**
   * 判断命令的执行结果是否包含关键字 {item}
   *
   * @parameter {cmdResult} string 类型, kubectl 命令执行后返回的字符串
   * @parameter {name} string 类型, 例如：namespace 的名称
   *
   * @example  resultContains(CommonKubectl.execKubectlCommand('kubectl get namespace -o json'), 'default')
   * @returns  boolean 类型, true 表示包含， false  表示不包含
   */
  static resultContains(cmdResult, name) {
    const result = JSON.parse(cmdResult);

    for (let i = 0; i < result.items.length; i++) {
      if (result.items[i].metadata.name === name) {
        return true;
      }
    }
    return false;
  }

  static async sendMessage(message: any, done: any) {
    if ('true' in message) {
      // await CommonHttp.sendMessageToDingDing(message.true + '[你强]');
      done();
    } else {
      await CommonHttp.sendMessageToDingDing(message.false + '[可怜]');
      done();
    }
  }

  static isRunningState(
    name: string,
    kind: string = 'pods',
    namespace: string = 'alauda-system',
  ) {
    let hasPod = false;
    try {
      const podlist = CommonMethod.parseYaml(
        CommonMethod.execCommand(`kubectl get ${kind} -n ${namespace} -o yaml`),
      );
      for (const iterator of podlist.items) {
        const podName = iterator.metadata.name;
        if (podName.includes(name)) {
          hasPod = true;
          JSON.stringify(iterator.status.containerStatuses[0].state);
          const isRunning =
            'running' in iterator.status.containerStatuses[0].state;
          if (isRunning) {
            return { true: `${name}'s state is Running` };
          } else {
            return {
              false: `${name}'s state is ${
                iterator.status.containerStatuses[0].state
              }`,
            };
          }
        }
      }
      if (!hasPod) {
        return { false: `${kind} ${name} 不存在` };
      }
    } catch (e) {
      return {
        false: `kubectl 查询 ${kind} ${name} 状态时发生异常  ${CommonMethod.execCommand(
          `kubectl get ${kind} -n ${namespace} -o yaml`,
        )}`,
      };
    }
  }
}
