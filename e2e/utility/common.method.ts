/**
 * Created by liuwei on 2018/2/28.
 */
import { atob } from 'abab';
import { exec, execSync } from 'child_process';
import { readFileSync, writeFileSync } from 'fs';

import { ServerConf } from '../config/serverConf';

export class CommonMethod {
  static isClearTestData = true;
  /**
   * 比较两个字符串变量的大小
   * stringObject < target 返回 1
   * stringObject > target 返回 -1
   *
   * @parameter {stringObject} string 变量
   * @parameter {target} string 量
   */
  static stringDown(stringObject, target) {
    return stringObject.localeCompare(target) < 0 ? 1 : -1;
  }

  /**
   * 读取YAML（{fileName}）文件，返回文件内容
   *
   * @parameter {fileName} string 类型, 文件名称
   * @parameter {parameter} 键值对格式，键写在YAML文件中 例如： { '$NAME': 20, '$PASSWORD': 30 }
   *
   * @example  readyamlfile('../test_data/configmap.yaml', {'$NAME':'liuweiconfigmap'});
   * @returns  string 类型，返回YAML 文件内容，该内容包含的所有键值被对应的值替换
   *
   */
  static readyamlfile(fileName, parameter: { [key: string]: string } = {}) {
    let data = readFileSync(ServerConf.TESTDATAPATH + fileName, 'utf8');
    for (const key of Object.keys(parameter)) {
      while (data.indexOf(key) >= 0) {
        data = data.replace(key, parameter[key]);
      }
      data = data.replace(key, parameter[key]);
    }
    const image =
      process.env.IMAGE || 'index.alauda.cn/alaudaorg/qaimages:helloworld';
    return data.replace('${IMAGE}', image);
  }

  /**
   * 将 {value} 值写入{fileName} 文件，返回文件的完整名
   *
   * @parameter {fileName} string 类型, 文件名称
   * @parameter {value} string 类型, 要写入 {fileName 的文件内容
   *
   * @returns  string 类型，返回文件的完整路径
   * @example writeyamlfile('temp.yaml', 'yaml 格式的文件字符串内容')
   */
  static writeyamlfile(fileName, value) {
    writeFileSync(ServerConf.TESTDATAPATH + 'temp/' + fileName, value);
    return ServerConf.TESTDATAPATH + 'temp/' + fileName;
  }

  /**
   * 根据yaml 模版生成yaml, 以字符串的格式返回
   * @param filename yaml 模版文件
   * @param parameter 模版文件中要替换的内容
   * @param testfilename 测试脚本的文件名
   */
  static generateStringYaml(filename, parameter, testfilename) {
    // 根据YAML 文件的模版生成一个字符串格式的 YAML
    const yaml = CommonMethod.readyamlfile(filename, parameter);

    // 将生成的生成一个字符串格式的 YAML 写入到临时文件
    const name = CommonMethod.writeyamlfile(`${testfilename}.yaml`, yaml);
    // 返回字符串格式的yaml
    const data = CommonMethod.readyamlfile(
      `temp/${testfilename}.yaml`,
      parameter,
    );

    const m = new Map();
    m.set('name', name);
    m.set('data', data);
    return m;
  }

  /**
   * 命令行执行任意一个命令
   *
   * @parameter {cmd} string 类型, 命令， 例如： kubectl get service -n liuweinamespace
   *
   * @returns  string 类型，命令执行后在屏幕上输出的内容
   * @example execCommand('kubectl get configmap');
   */
  static execCommand(cmd) {
    try {
      return String(execSync(cmd));
    } catch (ex) {
      return ex.toString();
    }
  }

  /**
   * 程序等待 milliSeconds
   *
   * @parameter {milliSeconds} int 类型, 命令.
   *
   * @returns  string 类型，命令执行后在屏幕上输出的内容
   * @example sleep(1000);
   */
  static sleep(milliSeconds) {
    const startTime = new Date().getTime();
    while (new Date().getTime() < startTime + milliSeconds) {}
  }

  /**
   * 解析yaml
   * @param yamlValue, string 类型，从yaml读出的值
   * @return {any}
   */
  static parseYaml(yamlValue) {
    const yaml = require('js-yaml');
    try {
      return yaml.safeLoad(yamlValue);
    } catch (error) {
      return error;
    }
  }

  /**
   * 将通过getText() 方法获得的页面元素的文本转化成数组
   * @param testlist 过getText() 方法获得的页面元素的文本
   */
  static converNodeListTextToArray(testlist) {
    const testArray = [];
    for (let i = 0, len = testlist.length; i < len; i++) {
      testArray.push(testlist[i]);
    }
    return testArray;
  }

  /**
   * 生成测试数据
   */
  static random_generate_testData(prefix = 'link-auto-') {
    // CommonMethod.clearTestData();
    CommonMethod.sleep(1);
    return prefix + String(new Date().getTime());
  }

  /**
   * 生成从minNum到maxNum的随机数
   */
  static randomNum(minNum, maxNum) {
    if (arguments.length === 1) {
      return parseInt((Math.random() * minNum + 1).toString(), 10).toString();
    } else if (arguments.length === 2) {
      return parseInt(
        Math.random() * (maxNum - minNum + 1) + minNum,
        10,
      ).toString();
    } else {
      return '0';
    }
  }

  /**
   * 清理由于异常中断引起的脏数据
   */
  static clearTestData() {
    if (CommonMethod.isClearTestData) {
      const namespacelist = CommonMethod.parseYaml(
        CommonMethod.execCommand('kubectl get namespaces -o yaml'),
      );
      for (const iterator of namespacelist.items) {
        const name = iterator.metadata.name;
        if (name.includes('link-auto-')) {
          const patt1 = new RegExp('^[0-9]*$');
          if (patt1.test(name.replace('link-auto-', ''))) {
            console.log('==========> clear test data');
            // CommonMethod.execCommand(`kubectl delete namespace ${name}`);
            exec(`kubectl delete namespace ${name}`, (err, sto) => {
              CommonMethod.isClearTestData = true;
            });
          }
        }
      }
    }
    CommonMethod.isClearTestData = false;
  }

  /**
   * base64 解码
   * @param base64Str base64 字符串
   */
  static decode(base64Str: string): string {
    return atob(base64Str);
  }
}
