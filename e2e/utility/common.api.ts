/**
 * Created by liuwei on 2018/3/8.
 */

import { ServerConf } from '../config/serverConf';

import { CommonKubectl } from './common.kubectl';
import { CommonMethod } from './common.method';

export class CommonApi {
  /**
   * 检查资源是否存在
   *
   * @parameter {items} [kubectl get configmap -o json] 命令获得的返回体里面的items
   * "apiVersion": "v1",
    "items": [
        {
            "apiVersion": "v1",
            "data": {
                "cm1": "cm1",
                "cm2": "cm2"
            },
   * @parameter {name} string 资源名称
   *
   */
  private static _exist(items, name) {
    for (const item of items) {
      if (item.metadata.name === name) {
        return true;
      }
    }
    return false;
  }

  /**
   * 检查资源是否存在，如果不存在则创建
   *
   * @parameter {filename} string 类型
   * @parameter {parameter} 字典
   * @parameter {kind} string 类型
   *
   * @example createResource('namespace.yaml', {'$NAME': namespace1, '$LABEL': namespace1}, 'namespace');
   */
  static createResource(filename, parameter, kind) {
    if (!CommonApi.isExistResource(parameter['${NAME}'], kind)) {
      // 资源不存在创建资源
      CommonKubectl.createResource(filename, parameter, 'temp');

      // 资源创建后，检查资源是否存在，30秒 超时
      let timeout = 1;
      while (!CommonApi.isExistResource(parameter['${NAME}'], kind)) {
        CommonMethod.sleep(1000);
        timeout++;
        if (timeout > 30) {
          break;
        }
      }
    }
  }

  /**
   * 检查资源是否存在，如果存在则删除
   *
   * @parameter {name} string 类型
   * @parameter {kind} string 类型
   *
   * @example createResource('namespace.yaml', {'$NAME': namespace1, '$LABEL': namespace1}, 'namespace');
   */
  static deleteResource(name, kind, namespace) {
    if (CommonApi.isExistResource(name, kind)) {
      // 如果资源存在，删除资源
      // console.log('删除命令是 ： kubectl delete ' + kind + ' ' + name + ' -n ' + namespace);
      CommonKubectl.execKubectlCommand(
        'kubectl delete ' + kind + ' ' + name + ' -n' + namespace,
      );
      CommonMethod.sleep(1000);

      let timeout = 0;
      // 每隔一秒查询一次如果存在继续查询，查询60秒后超时退出
      while (CommonApi.isExistResource(name, kind)) {
        CommonMethod.sleep(1000);
        timeout++;
        if (timeout > 60) {
          break;
        }
      }
    }
  }

  /**
   * 执行kubectl delete -f 删除资源
   * @param filename yaml 文件名
   */
  static deleteResourceFromYaml(filename = 'temp.yaml') {
    const filefullPath = ServerConf.TESTDATAPATH + filename;
    CommonKubectl.execKubectlCommand(`kubectl delete -f ${filefullPath}`);
    CommonKubectl.execKubectlCommand(`rm -rf ${filefullPath}`);
    CommonMethod.sleep(1000);
  }

  /**
   * 检查资源是否存在
   *
   * @parameter {name} string 资源名称
   * @parameter {kind} string 资源类型
   *
   * @example isExistResource('alauda-deployment-test2250','deploy')
   *
   * @return 布尔类型，true，存在，false 不存在
   */
  static isExistResource(name, kind) {
    const cmd = `kubectl get ${kind} --all-namespaces -o json`;
    const result = JSON.parse(CommonKubectl.execKubectlCommand(cmd));
    return CommonApi._exist(result.items, name);
  }

  /**
   * 获得资源，返回json 格式
   */
  static getResourceJSON(name, kind, namespace) {
    if (CommonApi.isExistResource(name, kind)) {
      return JSON.parse(
        CommonKubectl.execKubectlCommand(
          `kubectl get ${kind} ${name} -n ${namespace} -o json`,
        ),
      );
    } else {
      return false;
    }
  }
}
