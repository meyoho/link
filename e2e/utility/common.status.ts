/**
 * Created by liuwei on 2018/3/23.
 */

import { CommonKubectl } from './common.kubectl';
import { CommonMethod } from './common.method';

export class CommonStatus {
  static getPodStatus(key) {
    const pod_status = { Running: '运行中', Pending: '挂起' };
    return pod_status[key];
  }
}
