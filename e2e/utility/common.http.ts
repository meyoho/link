/**
 * Created by liuwei on 2018/2/28.
 */
import axios from 'axios';

export class CommonHttp {
  static sendMessageToDingDing(message) {
    const data = {
      msgtype: 'text',
      text: { content: message },
      at: { isAtAll: false },
    };
    const dingdingUrl = process.env.DING_DING_URL;

    const params = {
      access_token: process.env.DING_DING_ACCESS_TOKEN,
    };
    // axios.post(dingdingUrl, payload, { params }).then(console.log);
    return axios.post(dingdingUrl, data, { params });
  }
}
