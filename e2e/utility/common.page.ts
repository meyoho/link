/**
 * Created by liuwei on 2018/2/14.
 */
import { ElementFinder, browser, by, element, protractor } from 'protractor';

export class CommonPage {
  /**
   * wait an element to present on the page
   *
   * @parameter {elem} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementPresent(elem, timeout = 60000) {
    return browser.driver
      .wait(() => {
        return elem.isPresent().then(isPresent => isPresent);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error [' + err + ']');
          return false;
        },
      );
  }

  /**
   * wait an element to disappear on the page
   *
   * @parameter {elem} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementNotPresent(elem: ElementFinder, timeout = 60000) {
    return browser.driver
      .wait(() => {
        return elem.isPresent().then(isPresent => !isPresent);
      }, timeout)
      .then(
        () => true,
        async err => {
          console.warn(
            `wait error [${err}] for element: ${await browser.getCurrentUrl()}`,
          );
          return false;
        },
      );
  }

  /**
   * wait an element to display on the page
   *
   * @parameter {elem} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementDisplay(elem, timeout = 60000) {
    return browser.driver
      .wait(() => {
        return elem.isDisplayed().then(isDisplayed => isDisplayed);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error: [' + err + ']');
          return false;
        },
      );
  }

  /**
   * wait an element to hidden on the page
   *
   * @parameter {elem} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementNotDisplay(elem, timeout = 60000) {
    return browser.driver
      .wait(() => {
        return elem.isDisplayed().then(isDisplayed => !isDisplayed);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error: [' + err + ']');
          return false;
        },
      );
  }

  /**
   * 等待元素的文本变成期望的文本
   * @param elem 页面元素
   * @param expectText 期望文本
   * @param timeout 超时时间
   */
  static waitElementTextChangeTo(elem, expectText, timeout = 120000) {
    return browser.driver
      .wait(() => {
        return elem.getText().then(text => {
          return text.trim() === String(expectText).trim();
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error: [' + err + ']');
          return false;
        },
      );
  }

  /**
   * 等待元素能单击
   * @param elem 页面元素
   * @param timeout 等待多久超时
   */
  static waitElementClickable(elem, timeout = 60000) {
    const EC = protractor.ExpectedConditions;
    return browser.driver.wait(EC.elementToBeClickable(elem), timeout);
  }

  static get _leftNavItem() {
    return {
      容器组: '/pod',
      副本集: '/replicaset',
      部署: '/deployment',
      服务: '/service',
      配置字典: '/configmap',
      保密字典: '/secret',
      节点: '/node',
      命名空间相关资源: '/others,namespaced',
      集群相关资源: '/others,clustered',
      角色: '/administrator,roles',
    };
  }

  /**
   * 等待页面上progressbar 不显示mat-progress-bar[class*=alk-invisible]
   */
  static waitProgressbarNotDisplay() {
    const progressbar = element(
      by.css('mat-progress-bar[class*=alk-invisible]'),
    );
    CommonPage.waitElementPresent(progressbar);
    CommonPage.waitElementNotDisplay(progressbar);
    return browser.sleep(500);
  }

  /**
   * 等待 操作动作提示 消失
   */
  static waitToastNotPresent() {
    browser.sleep(500);
    CommonPage.waitElementNotPresent(
      element(by.css('aui-message .aui-message__content')),
    );
  }

  /**
   * 单击页面的左导航
   *
   * @parameter {text} 左导航显示的文字，
   * @example clickLeftNavByText('命名空间相关资源');
   */
  static clickLeftNavByText(text) {
    const child_xpath = `//div[@class="aui-platform-nav__label"  and normalize-space(text()) ='${text}']`;
    const parent_xpath =
      child_xpath + `/ancestor::div[contains( @class,'aui-nav-item-li')]`;

    const parent_elem = element(by.xpath(parent_xpath));
    const child_elem = element(by.xpath(child_xpath));
    // 等待左导航加载
    CommonPage.waitElementPresent(child_elem, 20000);
    child_elem.isDisplayed().then(function(isDisplayed) {
      if (isDisplayed) {
        // 如果左导航显示了，单击
        CommonPage.waitElementClickable(child_elem);
        child_elem.click();
      } else {
        // 如果没显示，找到父菜单，单击
        CommonPage.waitElementClickable(parent_elem);
        parent_elem.click();
        CommonPage.waitElementClickable(child_elem);
        child_elem.click();
      }
    });
    // 等待page Progressbar 元素不显示
    return CommonPage.waitProgressbarNotDisplay();
  }
}
