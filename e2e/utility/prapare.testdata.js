
const { execSync, exec } = require('child_process');
let fs = require('fs');
let join = require('path').join;
const { readFileSync, writeFileSync } =  require('fs')


class TestData {
    get testdataPath() {
        return process.cwd() + '/e2e/test_data/';
    }
    constructor() {}
    /**
     * 根据YAML 模版创建一个资源
     *
     * @parameter {filename} string 类型, 创建 test_data 文件下的YAML模版文件名
     * @parameter {parameter} string 类型, YAML模版里要替换的的参数
     * @example createResource('configmap.yaml', {'$NAME':'qqqqqq'})
     * @returns  string 类型，命令执行后在屏幕上输出 例如：configmap "qqqqqq" created
     */
    createResource(filename, parameter, testscriptfilename) {
        // 根据YAML 文件的模版生成一个字符串格式的 YAML
        const yaml = this.readyamlfile(filename, parameter);

        // 将生成的生成一个字符串格式的 YAML 写入到临时文件
        const tempfilename = this.writeyamlfile(
        `${testscriptfilename}.yaml`,
        yaml,
        );
        // 执行kubectl create configmap 命令创建configmap ,以字符串的格式返回命令的输出
        this.execCommand(`kubectl create -f ${tempfilename}`);
        return tempfilename;
    }

    writeyamlfile(fileName, value) {
        writeFileSync(this.testdataPath + 'temp/' + fileName, value);
        return this.testdataPath + 'temp/' + fileName;
    }
    /**
     * 读取YAML（{fileName}）文件，返回文件内容
     *
     * @parameter {fileName} string 类型, 文件名称
     * @parameter {parameter} 键值对格式，键写在YAML文件中 例如： { '$NAME': 20, '$PASSWORD': 30 }
     *
     * @returns  string 类型，返回YAML 文件内容，该内容包含的所有键值被对应的值替换
     * @param {*} fileName 
     * @param {*} parameter 
     */
    readyamlfile(fileName, parameter = {}) {
        let data = readFileSync(this.testdataPath + fileName, 'utf8');
        for (const key of Object.keys(parameter)) {
        while (data.indexOf(key) >= 0) {
            data = data.replace(key, parameter[key]);
        }
        data = data.replace(key, parameter[key]);
        }
        const image =
        process.env.IMAGE || 'index.alauda.cn/alaudaorg/qaimages:helloworld';
        return data.replace('${IMAGE}', image);
    }
    /**
     * 执行传入的命令
     * @param {*} cmd 
     */
    execCommand(cmd) {
        try {
        return String(execSync(cmd));
        } catch (ex) {
        return ex.toString();
        }
    }
    /**
        * 生成一个名称为link-auto-prapare*****，*****为10000到99999
        */
    createName() {
        return `link-auto-prapare-${this.randomNum( 10000 , 99999 )}`;
    }
    /**
     * 通过测试数据文件清理测试数据
    */
    clearPrapareData(name) {
      console.log(`正在删除 NAMESPACE:${name}`);
      this.execCommand(`kubectl delete namespace ${name}`);
    }
    /**
     * 生成随机数
     * @param {*} minNum 最小
     * @param {*} maxNum 最大
     * 返回值为string类型
     */
    randomNum(minNum, maxNum) {
        if (arguments.length === 1) {
        return parseInt((Math.random() * minNum + 1).toString(), 10).toString();
        } else if (arguments.length === 2) {
        return parseInt(
            Math.random() * (maxNum - minNum + 1) + minNum,
            10,
        ).toString();
        } else {
        return '0';
        }
    }
}
module.exports = {
  // 通用
  TestData: new TestData(),
};
