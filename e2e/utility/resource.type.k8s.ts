/**
 * kubectl get 命令后面跟的 资源类型参数
 * Created by liuwei on 2018/3/8.
 */

const k8s_type_all = 'all';
const k8s_type_certificatesigningrequests = 'csr';
const k8s_type_clusterrolebindings = 'clusterrolebindings';
const k8s_type_clusterroles = 'clusterroles';
const k8s_type_componentstatuses = 'cs';
const k8s_type_configmaps = 'cm';
const k8s_type_controllerrevisions = 'controllerrevisions';
const k8s_type_cronjobs = 'cronjobs';
const k8s_type_customresourcedefinition = 'crd';
const k8s_type_daemonsets = 'ds';
const k8s_type_deployments = 'deploy';
const k8s_type_endpoints = 'ep';
const k8s_type_events = 'ev';
const k8s_type_horizontalpodautoscalers = 'hpa';
const k8s_type_ingresses = 'ing';
const k8s_type_jobs = 'jobs';
const k8s_type_limitranges = 'limits';
const k8s_type_namespaces = 'ns';
const k8s_type_networkpolicies = 'netpol';
const k8s_type_nodes = 'no';
const k8s_type_persistentvolumeclaims = 'pvc';
const k8s_type_persistentvolumes = 'pv';
const k8s_type_poddisruptionbudgets = 'pdb';
const k8s_type_podpreset = 'podpreset';
const k8s_type_pods = 'po';
const k8s_type_podsecuritypolicies = 'psp';
const k8s_type_podtemplates = 'podtemplates';
const k8s_type_replicasets = 'rs';
const k8s_type_replicationcontrollers = 'rc';
const k8s_type_resourcequotas = 'quota';
const k8s_type_rolebindings = 'rolebindings';
const k8s_type_roles = 'roles';
const k8s_type_secrets = 'secrets';
const k8s_type_serviceaccounts = 'sa';
const k8s_type_services = 'svc';
const k8s_type_statefulsets = 'sts';
const k8s_type_storageclasses = 'sc';

export {
  k8s_type_all,
  k8s_type_certificatesigningrequests,
  k8s_type_clusterrolebindings,
  k8s_type_clusterroles,
  k8s_type_componentstatuses,
  k8s_type_configmaps,
  k8s_type_controllerrevisions,
  k8s_type_cronjobs,
  k8s_type_customresourcedefinition,
  k8s_type_daemonsets,
  k8s_type_deployments,
  k8s_type_endpoints,
  k8s_type_events,
  k8s_type_horizontalpodautoscalers,
  k8s_type_ingresses,
  k8s_type_jobs,
  k8s_type_limitranges,
  k8s_type_namespaces,
  k8s_type_networkpolicies,
  k8s_type_nodes,
  k8s_type_persistentvolumeclaims,
  k8s_type_persistentvolumes,
  k8s_type_poddisruptionbudgets,
  k8s_type_podpreset,
  k8s_type_pods,
  k8s_type_podsecuritypolicies,
  k8s_type_podtemplates,
  k8s_type_replicasets,
  k8s_type_replicationcontrollers,
  k8s_type_resourcequotas,
  k8s_type_rolebindings,
  k8s_type_roles,
  k8s_type_secrets,
  k8s_type_serviceaccounts,
  k8s_type_services,
  k8s_type_statefulsets,
  k8s_type_storageclasses,
};
