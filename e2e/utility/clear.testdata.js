
const { execSync, exec } = require('child_process');
let fs = require('fs');
let join = require('path').join;

class TestData {
  get testdataPath() {
    return process.cwd() + '/e2e/test_data/temp';
  }

  constructor() {}

  parseYaml(yamlValue) {
    const yaml = require('js-yaml');
    try {
      return yaml.safeLoad(yamlValue);
    } catch (error) {
      return error;
    }
  }

  clear() {
    // this.clearFromTestDataFile();
    this.clearNamespace();
    this.clearClusterRole();
    this.clearClusterRolebinding();
    this.clearPV();
    this.clearStorageClass();
  }

  /**
   * 通过测试数据文件清理测试数据
   */
  clearFromTestDataFile() {
    var filenames = this.findSync(this.testdataPath);
    filenames.forEach(testdata => {
      if (!testdata.includes('.gitignore')) {
        try {
          // execSync(`kubectl delete -f ${testdata}`).then((error,std) =>{});
          exec(`kubectl delete -f ${testdata}`, (err, sto) => {});
        } catch (error) {}
      }
    });
  }

  /**
   * 清理namespaces
   */
  clearNamespace() {
    // 获取所有命名空间
    const namespacelist = this.parseYaml(
      execSync('kubectl get namespaces -o yaml'),
    );
    for (const iterator of namespacelist.items) {
      const name = iterator.metadata.name;
      const creationTimestamp = iterator.metadata.creationTimestamp.getTime();
      let timeDiff = parseInt(new Date().getTime() - creationTimestamp);
      // 获得命名空间存在了多长时间
      timeDiff = parseInt(timeDiff / (1000 * 60));
      if (name.includes('link-auto-')) {
        const patt1 = new RegExp('^[0-9]*$');
        if (patt1.test(name.replace('link-auto-', ''))) {
          console.log('==========> clear test data');
          try {
            if (timeDiff > 60) {
              // 如果命名空间超过了3小时， 删除
              console.log(
                `脏数据命名空间【${
                  iterator.metadata.name
                }】 存在超过了1个小时，自动删除了`,
              );
              // execSync(`kubectl delete namespace ${name}`);
              exec(`kubectl delete namespace ${name}`, (err, sto) => {});
            }
          } catch (error) {}
        }
      }
      if (name.includes('link-auto-prapare-')) {
        const patt1 = new RegExp('^[0-9]*$');
        if (patt1.test(name.replace('link-auto-prapare-', ''))) {
          console.log('==========> clear test data');
          try {
            if (timeDiff > 90) {
              // 如果命名空间超过了1.5小时， 删除
              console.log(
                `脏数据命名空间【${
                  iterator.metadata.name
                }】 存在超过了1.5个小时，自动删除了`,
              );
              // execSync(`kubectl delete namespace ${name}`);
              exec(`kubectl delete namespace ${name}`, (err, sto) => {});
            }
          } catch (error) {}
        }
      }
    }
  }
/**
   * PVC
   */
  clearPV() {
    // 获取所有命名空间
    const namespacelist = this.parseYaml(
      execSync('kubectl get pv -o yaml'),
    );

    for (const iterator of namespacelist.items) {
      const name = iterator.metadata.name;
      const creationTimestamp = iterator.metadata.creationTimestamp.getTime();
      let timeDiff = parseInt(new Date().getTime() - creationTimestamp);
      // 获得命名空间存在了多长时间
      timeDiff = parseInt(timeDiff / (1000 * 60));

      if (name.includes('link-auto-')) {
        const patt1 = new RegExp('^[0-9]*$');
        if (patt1.test(name.replace('link-auto-', ''))) {
          console.log('==========> clear test data');
          try {
            if (timeDiff > 60) {
              // 如果命名空间超过了3小时， 删除
              console.log(
                `脏数据PVC【${
                  iterator.metadata.name
                }】 存在超过了1个小时，自动删除了`,
              );
              // execSync(`kubectl delete namespace ${name}`);
              exec(`kubectl delete pv ${name}`, (err, sto) => {});
            }
          } catch (error) {}
        }
      }
    }
  }
  /**
   * StorageClass
   */
  clearStorageClass() {
    // 获取所有命名空间
    const namespacelist = this.parseYaml(
      execSync('kubectl get StorageClass -o yaml'),
    );

    for (const iterator of namespacelist.items) {
      const name = iterator.metadata.name;
      const creationTimestamp = iterator.metadata.creationTimestamp.getTime();
      let timeDiff = parseInt(new Date().getTime() - creationTimestamp);
      // 获得命名空间存在了多长时间
      timeDiff = parseInt(timeDiff / (1000 * 60));

      if (name.includes('link-auto-')) {
        const patt1 = new RegExp('^[0-9]*$');
        if (patt1.test(name.replace('link-auto-', ''))) {
          console.log('==========> clear test data');
          try {
            if (timeDiff > 60) {
              // 如果命名空间超过了3小时， 删除
              console.log(
                `脏数据StorageClass【${
                  iterator.metadata.name
                }】 存在超过了1个小时，自动删除了`,
              );
              // execSync(`kubectl delete namespace ${name}`);
              exec(`kubectl delete StorageClass ${name}`, (err, sto) => {});
            }
          } catch (error) {}
        }
      }
    }
  }
  /**
   * ClusterRole
   */
  clearClusterRole() {
    // 获取所有命名空间
    const namespacelist = this.parseYaml(
      execSync('kubectl get ClusterRole -o yaml'),
    );

    for (const iterator of namespacelist.items) {
      const name = iterator.metadata.name;
      const creationTimestamp = iterator.metadata.creationTimestamp.getTime();
      let timeDiff = parseInt(new Date().getTime() - creationTimestamp);
      // 获得命名空间存在了多长时间
      timeDiff = parseInt(timeDiff / (1000 * 60));

      if (name.includes('link-auto-')) {
        const patt1 = new RegExp('^[0-9]*$');
        if (patt1.test(name.replace('link-auto-', ''))) {
          console.log('==========> clear test data');
          try {
            if (timeDiff > 60) {
              // 如果命名空间超过了1小时， 删除
              console.log(
                `脏数据ClusterRole【${
                  iterator.metadata.name
                }】 存在超过了1个小时，自动删除了`,
              );
              // execSync(`kubectl delete namespace ${name}`);
              exec(`kubectl delete ClusterRole ${name}`, (err, sto) => {});
            }
          } catch (error) {}
        }
      }
    }
  }

   /**
   * clearClusterRolebinding()
   */
  clearClusterRolebinding() {
    // 获取所有命名空间
    const namespacelist = this.parseYaml(
      execSync('kubectl get ClusterRolebinding -o yaml'),
    );

    for (const iterator of namespacelist.items) {
      const name = iterator.metadata.name;
      const creationTimestamp = iterator.metadata.creationTimestamp.getTime();
      let timeDiff = parseInt(new Date().getTime() - creationTimestamp);
      // 获得命名空间存在了多长时间
      timeDiff = parseInt(timeDiff / (1000 * 60));

      if (name.includes('link-auto-')) {
        const patt1 = new RegExp('^[0-9]*$');
        if (patt1.test(name.replace('link-auto-', ''))) {
          console.log('==========> clear test data');
          try {
            if (timeDiff > 60) {
              // 如果命名空间超过了1小时， 删除
              console.log(
                `脏数据ClusterRolebinding【${
                  iterator.metadata.name
                }】 存在超过了1个小时，自动删除了`,
              );
              // execSync(`kubectl delete namespace ${name}`);
              exec(`kubectl delete ClusterRolebinding ${name}`, (err, sto) => {});
            }
          } catch (error) {}
        }
      }
    }
  }


  /**
   * 获得startPath 路径下所有的文件名
   * @param {string} startPath
   */
  findSync(startPath) {
    let result = [];
    function finder(path) {
      let files = fs.readdirSync(path);
      files.forEach((val, index) => {
        let fPath = join(path, val);
        let stats = fs.statSync(fPath);
        if (stats.isDirectory()) finder(fPath);
        if (stats.isFile()) result.push(fPath);
      });
    }
    finder(startPath);
    return result;
  }

  /**
   * 判断OIDC 是否开启
   */
  isOpenOIDC() {
    try {
      const authConfigmap = this.parseYaml(
        execSync('kubectl get configmap auth-config -n alauda-system -o yaml'),
      );
      if (String(authConfigmap.data.enabled).toLocaleUpperCase() === 'TRUE') {
        return true;
      }
    } catch (error) {
      return false;
    }
  }
  waitElementPresent(elem, timeout = 60000) {
    return browser.driver
      .wait(() => {
        return elem.isPresent().then(isPresent => isPresent);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error [' + err + ']');
          return false;
        },
      );
  }
  /**
   * 登录页面的登录
   */
  login(ifprivate) {
    if (ifprivate === 'true') {
      console.log('进行登录操作')
      browser.waitForAngularEnabled(false);

      browser.get(`${browser.baseUrl}/?locale=zh`);

      this.waitElementPresent(element(by.css('.theme-panel')));
      //判断是否有管理员登录按钮并点击
      element(by.css('.dex-subtle-text')).isPresent().then(isPresent => {
        if (isPresent) {
          element(by.css('.dex-subtle-text')).click();
        }
      });
      // 输入用户名
      element(by.css('input#login')).click();
      element(by.css('input#login')).clear();
      element(by.css('input#login')).sendKeys('admin@alauda.io');

      // 输入密码
      element(by.css('input#password')).click();
      element(by.css('input#password')).clear();
      element(by.css('input#password')).sendKeys('password');
      // 点击登录
      element(by.css('button#submit-login')).click(); 
      // 等待登录成功
      this.waitElementPresent(element(by.css('.account-menu__display')));
    } else {
      console.log('通过token进入')
      console.log(
        `${browser.baseUrl}/?locale=zh&id_token=${process.env.USER_TOKEN}`,
      );
      browser.get(
        `${browser.baseUrl}/?locale=zh&id_token=${process.env.USER_TOKEN}`,
      ); 
    }
  }
}

module.exports = {
  // 通用
  TestData: new TestData(),
};
