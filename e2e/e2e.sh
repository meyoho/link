#!/bin/bash

# set -o errexit
set -o pipefail
set -o nounset
set +u

PLUGIN_NAME="${PLUGIN_NAME:-link}"
RESULTS_DIR="${RESULTS_DIR:-/tmp/results}"
# run tests
if [ ! $CASE_TYPE ]
then
  yarn e2e:retry
  CODE="$?"
elif [ $CASE_TYPE == "fulltest" ]
then
    yarn e2e:fulltest
    CODE="$?"

elif [ $CASE_TYPE == "stability" ]
then
    yarn e2e:stability
    CODE="$?"

elif [ $CASE_TYPE == "monitor" ]
then
    yarn e2e:monitor
    CODE="$?"
else
    yarn e2e:retry
    CODE="$?"
fi


cp -r ./e2e-reports/* ${RESULTS_DIR}
# cp test.log ${RESULTS_DIR}
# cat test.log

# Gather results into one file
cd ${RESULTS_DIR}
tar -czf ${PLUGIN_NAME}.tar.gz *

# Let the sonobuoy worker know the job is done
echo -n "${RESULTS_DIR}/${PLUGIN_NAME}.tar.gz" >"${RESULTS_DIR}/done"
exit $CODE 