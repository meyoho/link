/**
 * 其他资源列表的页面类
 * Created by liuwei on 2018/3/1.
 *
 */
import { by } from 'protractor';

import { AlaudaConfirmDialog } from '../../element_objects/alauda.confirmdialog';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { YamlComponent } from '../component/yaml.component';

export class OtherResourcePage extends ResourcePage {
  navigateToAboutNamespace() {
    CommonPage.clickLeftNavByText('命名空间相关资源');
    CommonPage.waitProgressbarNotDisplay();
  }
  navigateToAboutCluster() {
    CommonPage.clickLeftNavByText('集群相关资源');
    CommonPage.waitProgressbarNotDisplay();
  }
  /**
   * 确认对话框
   */
  get confirmDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(by.css('div.aui-confirm-dialog'));
  }

  /**
   * 通过yaml 创建资源的dialog 页面
   */
  get yamlCreator() {
    return new YamlComponent(by.css('aui-dialog .aui-dialog'));
  }
}
