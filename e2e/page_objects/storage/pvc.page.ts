/**
 * PVC列表的页面类
 * Created by liuwei on 2018/5/23.
 *
 */

import { ServerConf } from '../../config/serverConf';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { AdvancedAttributes } from '../component/advancedAttributes';
import { DeploymentPage } from '../computer/deployment.page';

export class PvcPage extends ResourcePage {
  navigateTo() {
    CommonPage.clickLeftNavByText('持久卷声明');
    CommonPage.waitProgressbarNotDisplay();
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tabname: string = 'input') {
    if (text === '命名空间' || text === '存储卷模式' || text === '存储类') {
      return this.auiSelect;
    } else if (text === '标签' || text === '注解' || text === '选择器') {
      return this.getAlaudaInputTable();
    } else {
      return super.getElementByText(text);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    if (name === '命名空间' || name === '存储卷模式' || name === '存储类') {
      this.getElementByText(name).selectinfrom(name, value);
    } else if (name === '标签' || name === '注解' || name === '选择器') {
      this.getElementByText(name).fillinTable(name, value);
    } else {
      this.getElementByText(name).clear();
      this.getElementByText(name).sendKeys(value);
    }
  }

  /**
   * 返回高级按钮
   */
  get advancedAttributes() {
    return new AdvancedAttributes(
      '.aui-card',
      '.aui-card__header',
      '.foldable-bar',
    );
  }

  createPVC(
    name,
    accessmode,
    volumemode,
    storageclasses: string = `${name}`,
    selectors = ['release', `${name}`, ''],
    size: string = '100m',
    labels = ['alauda.test', 'true', ''],
    annotations = ['alauda.test', 'true', ''],
    namespace = ServerConf.NAMESPACE,
  ) {
    this.enterValue('命名空间', namespace);
    this.enterValue('名称', name);
    this.radioGroupButton.clickRadioByText(accessmode);
    this.enterValue('存储卷模式', volumemode);
    this.enterValue('存储类', storageclasses);
    this.enterValue('大小', size);
    this.advancedAttributes.clickAdvanced('持久卷声明-基本');
    this.enterValue('标签', labels);
    this.enterValue('注解', annotations);
    this.enterValue('选择器', selectors);
  }
}
