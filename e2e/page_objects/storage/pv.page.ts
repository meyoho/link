/**
 * PV列表的页面类
 * Created by liuwei on 2018/5/23.
 *
 */

import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class PvPage extends ResourcePage {
  navigateTo() {
    CommonPage.clickLeftNavByText('持久卷');
    CommonPage.waitProgressbarNotDisplay();
  }
}
