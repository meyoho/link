/**
 * Created by liuwei on 2018/3/26.
 */

import {
  $$,
  ElementArrayFinder,
  ElementFinder,
  browser,
  by,
  element,
} from 'protractor';

import { AlaudaBreadCrumb } from '../element_objects/alauda.breadcrumb';
import { AlaudaButton } from '../element_objects/alauda.button';
import { AlaudaConfirmDialog } from '../element_objects/alauda.confirmdialog';
import { AlaudaDropdown } from '../element_objects/alauda.dropdown';
import { AlaudaElement } from '../element_objects/alauda.element';
import { AlaudaRadio } from '../element_objects/alauda.radio';
import { AlaudaToast } from '../element_objects/alauda.toastdialog';
import { AlaudaPaginator } from '../element_object_new/alauda.aui_paginator';
import { CommonPage } from '../utility/common.page';

import { Namespace } from './component/namespace.component';

export class PageBase {
  /**
   * 点击左导航到测试页面
   */
  navigateTo() {}

  waitElementChangeto(elem, expectText, timeout = 120000) {
    return browser.driver
      .wait(() => {
        return elem.getText().then(text => {
          return text.includes(String(expectText).trim());
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error: [' + err + ']');
          return false;
        },
      );
  }
  /**
   * 获得新窗口的
   * @param index 窗口的index, 从 0 开始
   */
  switchWindow(index: number) {
    browser.getAllWindowHandles().then(function(handles) {
      const newWindowHandle = handles[index];
      browser
        .switchTo()
        .window(newWindowHandle)
        .then(() => {
          browser.getWindowHandle().then(head => {
            console.log('switch to new Window: ' + head);
          });
        });
    });
  }
  /**
   * 单击左导航
   */
  clickLeftNavByText(text) {
    CommonPage.waitElementNotPresent(this.toastsuccess.message);
    CommonPage.clickLeftNavByText(text);
    this.waitElementChangeto(element(by.css('alk-breadcrumb')), text);
    this.breadcrumb.getText().then(breadcrumb => {
      console.log(
        '---------------------' + breadcrumb + '----------------------',
      );
    });
    expect(this.breadcrumb.getText()).toContain(text);
    CommonPage.waitProgressbarNotDisplay();
  }
  /**
   * 获取kubectl按钮
   */
  get kubectlButton() {
    return element(by.css('div.layout-toolbar__kubectl'));
  }

  /**
   * 页面右上角创建资源的按钮
   */
  get iconCreateResource(): AlaudaButton {
    return new AlaudaButton(
      by.css('.aui-layout__toolbar .layout-toolbar__create-button .aui-icon'),
    );
  }

  /**
   * 页面右上角用户图标的按钮
   */
  get accountMenu(): AlaudaDropdown {
    return new AlaudaDropdown(
      by.css('.aui-layout__toolbar .account-menu .aui-icon'),
      by.css('.aui-layout__toolbar .menu-dropdown__content'),
    );
  }

  /**
   * 页面右上角用户文字显示
   */
  get account() {
    return element(by.css('.account-menu .account-menu__display'));
  }

  /**
   * 页面上的选择命名空间的空间
   */
  get namespace(): Namespace {
    return new Namespace(
      by.css('.namespace-label .namespace-label__icon'),
      by.css('aui-menu-item button[class*=aui-menu-item]'),
    );
  }

  /**
   * 面包屑
   */
  get breadcrumb(): AlaudaBreadCrumb {
    return new AlaudaBreadCrumb();
  }

  /**
   * 详情页面，面包屑右侧的操作下拉框
   */
  get operation(): AlaudaDropdown {
    return new AlaudaDropdown(
      by.css('.layout-page-header > .aui-button'),
      by.css('aui-menu-item button[class*=aui-menu-item]'),
    );
  }

  /**
   * 确认对话框
   */
  get confirmDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog();
  }

  /**
   * 取消创建提示对话框
   */
  get warningDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('aui-confirm-dialog'),
      '.aui-confirm-dialog__title',
      '.aui-confirm-dialog__confirm-button',
      '.aui-confirm-dialog__cancel-button',
    );
  }
  /**
   * 在创建时点击左导航
   */
  clickLeftText(text) {
    const child_xpath = `//div[@class="aui-platform-nav__label"  and normalize-space(text()) ='${text}']`;
    const parent_xpath =
      child_xpath + `/ancestor::div[contains( @class,'aui-nav-item-li')]`;
    const parent_elem = element(by.xpath(parent_xpath));
    const child_elem = element(by.xpath(child_xpath));
    // 如果左导航显示了，单击
    child_elem.click();
  }
  /**
   * toast message
   */
  get toastsuccess(): AlaudaToast {
    return new AlaudaToast(by.css('aui-message .aui-message__content'));
  }

  /**
   * 表格数据加载时的等待提示
   */
  get progressbar(): ElementFinder {
    return element(by.css('mat-progress-bar[role=progressbar]'));
  }

  /**
   * 退出登录
   * @param item ’退出登录‘
   */
  logout(item = '退出登录') {
    this.accountMenu.select(item);
  }

  /**
   * 根据按钮上的文字获得按钮
   * @param text 按钮上的文字
   */
  getButtonByText(text): AlaudaButton {
    return new AlaudaButton(
      by.xpath(
        `//button/descendant-or-self::*[normalize-space(text()) ='${text}']`,
      ),
    );
  }

  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement();
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname: string = 'input'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    this.getElementByText(name).clear();
    this.getElementByText(name).sendKeys(value);
    browser.sleep(100);
  }
  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        this.enterValue(key, data[key]);
      }
    }
  }

  get radioButton() {
    return new AlaudaRadio('aui-radio-group .aui-radio-button__label');
  }

  clickRadioBbyText(name) {
    this.radioButton.getRadioByName(name).click();
  }

  /**
   * 配置管理页面点击蓝色笔进行更新
   * @param leftText 左侧文字
   * @param rowfinder 行选择
   */
  clickBasicPencil(
    leftText: string,
    rowfinder: ElementArrayFinder = $$('div.alk-basic-info-field'),
  ) {
    rowfinder
      .filter((elem, index) => {
        return elem.getText().then(text => {
          return text
            .replace('*\n', '')
            .trim()
            .includes(leftText);
        });
      })
      .first()
      .$('svg')
      .click();
    CommonPage.waitElementDisplay(element(by.css('aui-dialog-header')), 60000);
  }

  get alaudaPaginator() {
    return new AlaudaPaginator();
  }
}
