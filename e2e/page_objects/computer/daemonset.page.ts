/**
 * 守护进程集列表的页面类
 * Created by liuwei on 2018/5/14.
 *
 */
import { ServerConf } from '../../config/serverConf';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

import { DeploymentPage } from './deployment.page';

export class DaemonsetPage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('守护进程集');
    CommonPage.waitProgressbarNotDisplay();
    this.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }
  get elem() {
    return new DeploymentPage();
  }
  /**
   * * 填写创建deployment页面的参数
   * @param namespace 命名空间
   * @param name Deployment名称
   * @param strategy 更新策略
   * @param label 默认为0 传入1时 执行添加标签，注解，选择器的操作。
   */
  fillinBasical(namespace, name, strategy, label = '0') {
    this.elem.enterValue('命名空间', namespace);
    this.elem.enterValue('名称', name);
    // 点击部署基本card的高级参数
    this.advancedAttributes.clickAdvanced('守护进程集 - 基本');
    if (label === '1') {
      this.elem.enterValue('标签', ['test', 'linkautotest', '0']);
      this.elem.enterValue('注解', ['test', 'linkautotest', '0']);
      this.elem.enterValue('选择器', [
        'name',
        name,
        '0',
        'linkautotest',
        'linkautotest',
        '0',
      ]);
    }
    this.elem.enterValue('更新策略', strategy);
    this.advancedAttributes.clickAdvanced('守护进程集 - 基本');
  }
  /**
   * 填写容器基本信息
   * @param name 名称
   * @param resources 容器大小  若传入为’‘ 不填写
   * @param config 默认为‘0’ 为‘1’时添加。
   * @param image 镜像
   */
  fillincontainer(
    name,
    resources,
    env,
    config = '0',
    image = ServerConf.IMAGE,
  ) {
    this.elem.enterValue('容器名称', name);
    this.elem.enterValue('镜像', image);
    this.elem.enterValue('容器大小', resources);
    this.elem.enterValue('环境变量', ['keys', 'value', '0']);
    if (config === '1') {
      this.elem.enterValue('配置引用', env);
    }
  }
  /**
   * 填写容器高级
   * @param argument 参数 若传入为’‘ 则不填写
   * @param command 命令 若传入为’‘ 不填写命令
   */
  fillinContainerAdvanced(argument, command) {
    this.advancedAttributes.clickAdvanced('容器组');
    this.elem.enterValue('执行参数', argument);
    this.elem.enterValue('执行命令', command);
    this.advancedAttributes.clickAdvanced('容器组');
  }
  /**
   * 填加一个存储
   * @param name 名称
   * @param subpath 子路径
   * @param mountpath 本地路径
   * @param redonly 是否只读 若传'1'则点击传’0‘不点击
   * @param del 删除按钮 若传'1'则点击传’0‘不点击
   */
  fillinVolumeMounts(name, subpath, mountpath, redonly = '0', del = '0') {
    this.advancedAttributes.clickAdvanced('容器组');
    this.elem.enterValue('存储卷挂载', [
      name,
      subpath,
      mountpath,
      redonly,
      del,
    ]);
    this.advancedAttributes.clickAdvanced('容器组');
  }
}
