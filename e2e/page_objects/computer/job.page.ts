/**
 * 其他资源列表的页面类
 * Created by liuwei on 2018/3/1.
 *
 */
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class JobPage extends ResourcePage {
  navigateTo() {
    CommonPage.clickLeftNavByText('任务');
    CommonPage.waitProgressbarNotDisplay();
  }
}
