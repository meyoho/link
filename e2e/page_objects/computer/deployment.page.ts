import { $$, browser, by, element } from 'protractor';
import { protractor } from 'protractor/built/ptor';

import { ServerConf } from '../../config/serverConf';
import { AlaudaConfirmDialog } from '../../element_objects/alauda.confirmdialog';
import { AlaudaElement } from '../../element_objects/alauda.element';
import { AlaudaInputGroup } from '../../element_objects/alauda.inputgroup';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { Namespace } from '../component/namespace.component';

export class DeploymentPage extends ResourcePage {
  get containerElement() {
    return new AlaudaElement(
      'alk-container-form .aui-form-item',
      '.aui-form-item__label',
      '.aui-form-item__control',
    );
  }
  /**
   * 点击左导航的部署并选择所有命名空间
   */
  navigateTo() {
    CommonPage.clickLeftNavByText('部署');
    CommonPage.waitProgressbarNotDisplay();
  }
  // get auitagsinput() {
  //   return element ()
  // }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tabname: string = 'input') {
    if (
      text === '命名空间' ||
      text === '更新策略' ||
      text === '服务' ||
      text === '资源类型' ||
      text === '资源名称' ||
      text === '类型'
    ) {
      return this.auiSelect;
    } else if (
      text === '环境变量' ||
      text === '存储卷挂载' ||
      text === '标签' ||
      text === '注解' ||
      text === '选择器' ||
      text === '数据'
    ) {
      return this.getAlaudaInputTable();
    } else if (text === '容器名称') {
      return this.containerElement.getElementByText('名称');
    } else if (text === '容器大小') {
      return new AlaudaInputGroup();
    } else if (text === 'CPU' || text === '内存') {
      return this.resourceLimit;
    } else if (text === '配置引用' || text === '镜像仓库凭证') {
      return this.getmuitiSelect();
    } else if (text === '值' || text === '证书' || text === '私钥') {
      return super.getElementByText(text, 'textarea');
    } else if (text === '执行命令' || text === '执行参数') {
      return super.getElementByText(text, 'aui-tags-input div');
    } else {
      return super.getElementByText(text);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    if (
      name === '命名空间' ||
      name === '更新策略' ||
      name === '服务' ||
      name === '资源类型' ||
      name === '资源名称' ||
      name === '类型'
    ) {
      this.getElementByText(name).selectinfrom(name, value);
    } else if (
      name === '标签' ||
      name === '注解' ||
      name === '环境变量' ||
      name === '数据'
    ) {
      this.getElementByText(name).fillinTable(name, value);
    } else if (name === '存储卷挂载' || name === '选择器') {
      this.getElementByText(name).addandfillinTable(name, value);
    } else if (name === '执行参数' || name === '执行命令') {
      if (value !== '') {
        this.getElementByText(name).click();
        this.getElementByText(name)
          .$('input')
          .sendKeys(value);
        this.getElementByText(name)
          .$('input')
          .sendKeys(protractor.Key.ENTER);
      }
    } else if (name === '容器名称') {
      this.getElementByText(name).clear();
      this.getElementByText(name).sendKeys(value);
    } else if (name === 'CPU' || name === '内存') {
      this.getElementByText(name).sendValue(name, value);
    } else if (name === '容器大小') {
      if (value !== '') {
        this.getElementByText(name).sendValue('CPU', ['50m', '50m']);
        this.getElementByText(name).sendValue('内存', ['50Mi', '50Mi']);
      }
    } else if (name === '配置引用' || name === '镜像仓库凭证') {
      if (value !== '') {
        this.getElementByText(name).selectinfrom(name, value);
      }
    } else {
      this.getElementByText(name).clear();
      this.getElementByText(name).sendKeys(value);
    }
  }

  /**
   * * 填写创建deployment页面的参数
   * @param namespace 命名空间
   * @param name Deployment名称
   * @param strategy 更新策略
   * @param argument c参数
   * @param command 命令
   */
  fillinBasical(name, num, strategy, namespace = ServerConf.NAMESPACE) {
    this.enterValue('命名空间', namespace);
    this.enterValue('名称', name);
    this.enterValue('实例数量', num);
    // 点击部署基本card的高级参数
    this.advancedAttributes.clickAdvanced('部署 - 基本');
    this.enterValue('更新策略', strategy);
    this.advancedAttributes.clickAdvanced('部署 - 基本');
  }
  /**
   * 填写容器基本信息
   * @param name 名称
   * @param resources 容器大小  若传入为’‘ 不填写
   * @param config 默认为‘0’ 为‘1’时添加。
   * @param image 镜像
   */
  fillincontainer(name, resources, config = '0', image = ServerConf.IMAGE) {
    this.enterValue('容器名称', name);
    this.enterValue('镜像', image);
    this.enterValue('容器大小', resources);
    this.enterValue('环境变量', ['keys', 'value', '0']);
    if (config === '1') {
      this.enterValue('配置引用', name);
    }
  }
  /**
   * 填写容器高级
   * @param argument 参数 若传入为’‘ 则不填写
   * @param command 命令 若传入为’‘ 不填写命令
   */
  fillinContainerAdvanced(argument, command) {
    this.advancedAttributes.clickAdvanced('容器组');
    this.enterValue('执行参数', argument);
    this.enterValue('执行命令', command);
    this.advancedAttributes.clickAdvanced('容器组');
  }
  /**
   * 填加一个存储
   * @param name 名称
   * @param subpath 子路径
   * @param mountpath 本地路径
   * @param redonly 是否只读 若传'1'则点击传’0‘不点击
   * @param del 删除按钮 若传'1'则点击传’0‘不点击
   */
  fillinVolumeMounts(name, subpath, mountpath, redonly = '0', del = '0') {
    this.advancedAttributes.clickAdvanced('容器组');
    this.enterValue('存储卷挂载', [name, subpath, mountpath, redonly, del]);
    this.advancedAttributes.clickAdvanced('容器组');
  }
  /**
   * rollback对话框
   */
  get rollbackDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('alk-roll-back-dialog'),
      '.aui-dialog__header-title',
    );
  }
  /**
   * rollback确认对话框
   */
  get rollbackConfirmDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('aui-confirm-dialog'),
      '.aui-confirm-dialog__title span',
    );
  }
}
