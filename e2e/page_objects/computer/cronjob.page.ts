import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { TableComponent } from '../component/table.component';

export class CronJobPage extends ResourcePage {
  navigateTo() {
    CommonPage.clickLeftNavByText('定时任务');
    CommonPage.waitProgressbarNotDisplay();
  }

  get alkjoblist() {
    return new TableComponent('alk-job-list');
  }
}
