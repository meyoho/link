/**
 * 部署列表的页面类
 * Created by liuwei on 2018/3/19.
 *
 */
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class ReplicasetPage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('副本集');
    CommonPage.waitProgressbarNotDisplay();
    this.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }
}
