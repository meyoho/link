/**
 * 其他资源列表的页面类
 * Created by liuwei on 2018/3/1.
 *
 */
import { browser, by, element } from 'protractor';

import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { ExecPage } from '../component/exec.page';

export class PodPage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('容器组');
    CommonPage.waitProgressbarNotDisplay();
    this.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }
  get exec() {
    return new ExecPage();
  }
  get execButton() {
    return element(by.css('alk-shell-button .exec-button-trigger'));
  }
  // aui-menu-item .aui-menu-item--default
  get podsmenu() {
    return element.all(by.css('aui-menu-item .aui-menu-item--default'));
  }
  clickexecButton(podname) {
    CommonPage.waitElementPresent(this.execButton);
    this.execButton.click();
    console.log(
      '---------------------------------exec--------------------------------',
    );
    CommonPage.waitElementPresent(this.podsmenu);
    this.podsmenu
      .filter(elem => {
        return elem.getText().then(text => {
          return text === podname;
        });
      })
      .first()
      .click();
    browser.sleep(100);
  }
}
