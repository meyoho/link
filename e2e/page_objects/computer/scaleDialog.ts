/**
 * 确认对话框，包含确认提示内容，取消按钮，确认按钮
 * Created by liuwei on 2018/2/22.
 */

import { by, element } from 'protractor';

import { AlaudaConfirmDialog } from '../../element_objects/alauda.confirmdialog';
import { CommonPage } from '../../utility/common.page';

export class ScaleDialog extends AlaudaConfirmDialog {
  private _root_selector1;
  private _label_selector;
  private _input_selector;

  constructor(
    root_selector = by.css('aui-dialog'),
    confirm_title = 'aui-dialog__header-title',
    confirm_ok = '.aui-button--primary',
    confirm_cancel = '.aui-button--default',
    label_selector = '.aui-form-item__label',
    input_selector = '.aui-form-item__control input',
  ) {
    super(root_selector, confirm_title, confirm_ok, confirm_cancel);
    this._root_selector1 = root_selector;
    this._label_selector = label_selector;
    this._input_selector = input_selector;
  }

  get inputbox() {
    return element(this._root_selector1).$(this._input_selector);
  }

  get label() {
    return element(this._root_selector1).$(this._label_selector);
  }

  inputScale(scale) {
    CommonPage.waitElementPresent(this.inputbox);
    this.inputbox.clear();
    this.inputbox.sendKeys(scale);
  }
}
