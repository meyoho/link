import { By, browser, by, element } from 'protractor';

import { AuiSelect } from '../../element_objects/alauda.auiSelect';
import { AlaudaElement } from '../../element_objects/alauda.element';
import { AlaudaInputTable } from '../../element_objects/alauda.inputtable';
import { AlaudaRadio } from '../../element_objects/alauda.radio';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class AddVolumePage extends ResourcePage {
  /**
   * 重写父类属性，用于getElementByText 方法定位元素
   */
  get alaudaElement() {
    return new AlaudaElement(
      'alk-volume-form-dialog .aui-form-item',
      '.aui-form-item__label-wrapper',
      '.aui-form-item__content',
    );
  }
  get alaudaRadio() {
    return new AlaudaRadio('alk-volume-form-dialog .aui-radio-button');
  }
  get alaudaSelect() {
    return new AuiSelect(
      element(
        by.css(
          'alk-volume-form-dialog .volume-attributes-wrapper:not([hidden]) aui-select .aui-select__input',
        ),
      ),
      element(
        by.css(
          'alk-volume-form-dialog .volume-attributes-wrapper:not([hidden]) aui-select input',
        ),
      ),
    );
  }
  clickCommitButton() {
    this.getButtonByText('确定').click();
  }
  get cancelButton() {
    return this.getButtonByText('取消');
  }
  clickswitchbar() {
    element(by.css('alk-volume-form-dialog .aui-switch__bar')).click();
  }
  get inputtable() {
    return new AlaudaInputTable(
      'table button',
      'table td',
      '//div[@class="bottom-control-buttons"]/../../preceding-sibling::tr[2]',
      'alk-volume-form-dialog .aui-form-item',
      '.aui-form-item__label',
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tabname: string = 'input'): any {
    if (text === '类型') {
      return this.alaudaRadio;
    } else if (
      text === '持久卷声明' ||
      text === '配置字典' ||
      text === '保密字典'
    ) {
      return this.alaudaSelect;
    } else if (text === '名称' || text === '主机路径') {
      return this.alaudaElement.getElementByText(text);
    } else if (text === '引用配置项') {
      return this.inputtable;
    } else {
      return this.alaudaElement.getElementByText(text, '.aui-switch');
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(text, value) {
    if (text === '类型') {
      this.getElementByText(text)
        .getRadioByName(value)
        .click();
    } else if (
      text === '持久卷声明' ||
      text === '配置字典' ||
      text === '保密字典'
    ) {
      this.getElementByText(text).select(value);
    } else if (text === '名称' || text === '主机路径') {
      this.getElementByText(text).clear();
      this.getElementByText(text).sendKeys(value);
    } else if (text === '引用配置项') {
      this.getElementByText(text).fillinTable(text, [value, value]);
    } else {
      this.getElementByText(text).click();
    }
  }

  /**
   * 填写创建deployment页面的参数
   * @param name 名称可以自定义
   * @param type 可以是持久卷声明，配置字典，保密字典，主机路径，和空目录
   * @param volume 根据持久卷声明 名称，配置字典 名称，保密字典 为名称，主机路径 添加的路径 空目录 不填写
   */
  addVolume(name, type, volume) {
    this.enterValue('名称', name);
    this.enterValue('类型', type);
    CommonPage.waitProgressbarNotDisplay();
    // this.enterValue('标签', 'app');
    // 点击部署基本card的高级参数
    browser.sleep(500);
    if (type === '持久卷声明') {
      this.enterValue('持久卷声明', volume);
    } else if (type === '配置字典') {
      this.enterValue('配置字典', volume);
      this.clickswitchbar();
      this.enterValue('引用配置项', 'cm1');
    } else if (type === '保密字典') {
      this.enterValue('保密字典', volume);
    } else if (type === '主机路径') {
      this.enterValue('主机路径', volume);
    }
    this.clickCommitButton();
  }
}
