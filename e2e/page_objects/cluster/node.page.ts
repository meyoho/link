import { $$ } from 'protractor';

import { AlaudaBasicInfo } from '../../element_objects/alauda.basicinfo';
import { AlaudaTable } from '../../element_objects/alauda.table';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { TableComponent } from '../component/table.component';

export class NodePage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('节点');
    CommonPage.waitProgressbarNotDisplay();
  }
  /**
   * 获取列表中的第一个主机名称
   */
  get nodeNameElem() {
    return $$('aui-table-cell a').first();
  }
  /**
   * 系统信息card
   */
  get systemMassage() {
    return new AlaudaBasicInfo('系统信息');
  }
  /**
   * 返回现状表格
   */
  get conditionTable() {
    return new AlaudaTable(
      'alk-condition-list .aui-table__header-cell',
      'alk-condition-list .aui-table__cell',
      'alk-condition-list .aui-table__row',
    );
  }
  /**
   * 返回容器组列表
   */
  get podTable() {
    return new AlaudaTable(
      'alk-pod-list .aui-table__header-cell',
      'alk-pod-list .aui-table__cell',
      'alk-pod-list .aui-table__row',
    );
  }
}
