import { by, element } from 'protractor';

import { AlaudaConfirmDialog } from '../../element_objects/alauda.confirmdialog';
import { AlaudaDropdown } from '../../element_objects/alauda.dropdown';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { TableComponent } from '../component/table.component';
import { DeploymentPage } from '../computer/deployment.page';
export class NamespacePage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }
  /**
   * 资源详情页的基本信息
   */
  get resourceQuotas() {
    return new TableComponent('alk-resource-quota-status-list');
  }
  /**
   * 资源详情页的resourecQuota列表
   */
  get resourceQuotasAll() {
    return new TableComponent('alk-effective-quota-list');
  }
  /**
   * 资源详情页的limitrange列表
   */
  get limitRanges() {
    return new TableComponent('alk-limit-range-limit-list');
  }
  /**
   * 资源详情页的resourecQuota的有效资源详情列表
   */
  get headerselect() {
    return new AlaudaDropdown(
      '.aui-select',
      'aui-option',
      'aui-card .card-header',
      'aui-card .card-header__label',
    );
  }
  /**
   * 可以通过此方法引用deployment中的entervalue
   */
  get elem() {
    return new DeploymentPage();
  }
  clickokelement() {
    element(by.css('.aui-dialog__footer  .aui-button--primary')).click();
    CommonPage.waitElementNotPresent(this.cancelelement);
    CommonPage.waitProgressbarNotDisplay();
  }
  get cancelelement() {
    return element(by.css('.aui-dialog__footer  .aui-button--default'));
  }
  /**
   * 填写命名空间创建对话框
   * @param name 命名空间名称
   * @param label 命名空间的标签与注解。不想填写请传入非‘1’字符
   */
  addnamespace(name: string, label: string = '1') {
    this.elem.enterValue('名称', name);
    if (label === '1') {
      this.elem.enterValue('标签', ['alauda.test', 'true', '']);
      this.elem.enterValue('注解', ['alauda.test', 'true', '']);
    }
  }
  /**
   * 点击添加按钮
   */
  clickaddButton() {
    element(
      by.css('.alk-mutate-page-bottom-buttons .aui-button--primary'),
    ).click();
    CommonPage.waitProgressbarNotDisplay();
  }

  limitrangedialog() {
    return new AlaudaConfirmDialog(by.css('aui-dialog'));
  }
  /**
   * 填写命名空间添加资源限额
   * @param name 命名空间名称
   * @param label 命名空间的标签与注解。不想填写请传入非‘1’字符
   */
  addlimitrange(name: string, label: string = '1') {
    this.elem.enterValue('名称', name);
    if (label === '1') {
      this.elem.enterValue('标签', ['alauda.test', 'true', '']);
      this.elem.enterValue('注解', ['alauda.test', 'true', '']);
    }
  }
  /**
   * 添加资源限制的限制
   * @param type 资源类型
   * @param name 资源名称
   * @param min 最小值
   * @param max 最大值
   * @param MaxLimitRequestRatio 最大限制请求比
   * @param request 默认请求值
   * @param limit 默认限制值
   */
  addlimitrangelimit(
    type: string,
    name: string,
    min,
    max,
    MaxLimitRequestRatio,
    request = '',
    limit = '',
  ) {
    this.getAlaudaInputTable().clickAddButton('限制值');
    CommonPage.waitElementPresent(this.limitrangedialog().buttonCancel);
    this.elem.enterValue('资源类型', type);
    this.elem.enterValue('资源名称', name);
    this.elem.enterValue('最小值', min);
    this.elem.enterValue('最大值', max);
    if (type !== '容器组') {
      this.elem.enterValue('默认请求值', request);
      this.elem.enterValue('默认限制值', limit);
    }
    this.elem.enterValue('最大限制请求比', MaxLimitRequestRatio);
    CommonPage.waitElementClickable(this.limitrangedialog().buttonConfirm);
    this.limitrangedialog().clickConfirm();
  }
  /**
   *  * 填写命名空间添加资源配额
   * @param name 名称
   * @param pods 容器组数
   * @param pvc 持久卷声明 数
   * @param storage 存储数
   * @param cpu CPU的请求与限制值 默认填写['10', '20'],
   * @param memory 内存请求与限制值['20', '50'],
   * @param label 标签与注解 默认为['alauda.test', 'true', '']);
   */
  addresourcequota(
    name: string,
    pods: string,
    pvc: string,
    storage: string,
    cpu = ['10', '20'],
    memory = ['20', '50'],
    label: string = '1',
  ) {
    this.elem.enterValue('名称', name);
    if (label === '1') {
      this.elem.enterValue('标签', ['alauda.test', 'true', '']);
      this.elem.enterValue('注解', ['alauda.test', 'true', '']);
    }
    this.elem.enterValue('CPU', cpu);
    this.elem.enterValue('内存', memory);
    this.elem.enterValue('容器组', pods);
    this.elem.enterValue('存储', storage);
    this.elem.enterValue('持久卷声明', pvc);
  }
}
