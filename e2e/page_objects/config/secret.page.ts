/**
 * 部署列表的页面类
 * Created by liuwei on 2018/3/19.
 *
 */
import { by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { DeploymentPage } from '../computer/deployment.page';

export class SecretPage extends ResourcePage {
  navigateTo() {
    this.clickLeftNavByText('保密字典');
    CommonPage.waitProgressbarNotDisplay();
    this.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 单击创建配置字典按钮
   */
  clickCreateButton() {
    this.getButtonByText('创建保密字典').click();
    CommonPage.waitElementPresent(this.yamlCreator.buttonCreate);
  }
  get elem() {
    return new DeploymentPage();
  }
  get addtupeinput() {
    return element(by.css('alk-custom-type .aui-form-item input'));
  }
  get addtypebutton() {
    return element(by.css('alk-custom-type .aui-button--primary'));
  }
  /**
   * 添加基础名称
   * @param namespace 命名空间
   * @param name 名称
   */
  addSecret(name, namespace = ServerConf.NAMESPACE) {
    this.elem.enterValue('命名空间', namespace);
    this.elem.enterValue('名称', name);
    this.more.clickAdvanced('创建 保密字典');
    this.elem.enterValue('标签', ['alauda.test', 'true', '']);
    // 清理原有的默认值
  }
  /**
   * 添加保密字典类型的数据内容
   * @param type 类型 TLS dockerconfig dockerconfigjson Opaque 和自定义类型
   * @param key TLS为证书  dockerconfigdockerconfigjson为dockerServiceAddress
   * @param value TLS为私钥
   */
  addSecretdata(type, key, value) {
    if (type !== '自定义') {
      this.elem.enterValue('类型', type);
    }
    if (type === 'TLS') {
      this.elem.enterValue('数据', ['', '', '', '1']);
      this.elem.enterValue('键', 'tls.crt');
      this.elem.enterValue('值', value);
    } else if (type === 'dockercfg' || type === 'dockerconfigjson') {
      this.elem.enterValue('镜像服务地址', key);
      this.elem.enterValue('用户名', value);
      this.elem.enterValue('密码', '111111');
      this.elem.enterValue('邮箱', 'test@alauda.io');
    } else if (type === '自定义') {
      this.getButtonByText('自定义').click();
      this.addtupeinput.clear();
      this.addtupeinput.sendKeys(key);
      this.addtypebutton.click();
      this.elem.enterValue('类型', key);
      this.elem.enterValue('键', key);
      this.elem.enterValue('值', value);
    } else {
      this.elem.enterValue('数据', ['', '', '', '1']);
      this.elem.enterValue('键', key);
      this.elem.enterValue('值', value);
    }

    // 清理原有的默认值
  }
  updateSecret(name, key, value) {
    // 清理原有的默认值
    this.elem.enterValue('键', key);
    this.elem.enterValue('值', value);
  }

  get secretKey() {
    return element.all(by.css('div .secret-data-keys__label-key'));
  }

  clickSecretkey(key) {
    this.secretKey
      .filter(elem => {
        return elem.getText().then(text => {
          return text === key;
        });
      })
      .click();
  }
  get secretValue() {
    return element(by.css('pre.secret-data-section__content'));
  }
}
