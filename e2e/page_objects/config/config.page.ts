/**
 * 部署列表的页面类
 * Created by liuwei on 2018/3/19.
 *
 */
import { by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { DeploymentPage } from '../computer/deployment.page';

export class ConfigPage extends ResourcePage {
  navigateTo() {
    this.clickLeftNavByText('配置字典');
    CommonPage.waitProgressbarNotDisplay();
    this.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 单击创建配置字典按钮
   */
  clickCreateButton() {
    this.getButtonByText('创建').click();
    CommonPage.waitProgressbarNotDisplay();
  }
  /**
   * 单击创建配置字典按钮
   */
  clickUpdateButton() {
    this.getButtonByText('更新').click();
    CommonPage.waitProgressbarNotDisplay();
  }
  get elem() {
    return new DeploymentPage();
  }

  addconfig(name, key, value) {
    this.elem.enterValue('命名空间', ServerConf.NAMESPACE);
    this.elem.enterValue('名称', name);
    // 清理原有的默认值
    this.elem.enterValue('数据', ['', '', '', '1']);
    this.elem.enterValue('键', key);
    this.elem.enterValue('值', value);
    this.more.clickAdvanced('创建 配置字典');
    this.elem.enterValue('标签', ['alauda.test', 'true', '']);
  }

  updateconfig(name, key, value) {
    // 清理原有的默认值
    this.elem.enterValue('键', key);
    this.elem.enterValue('值', value);
  }

  get configkey() {
    return element.all(by.css('div .config-data-section__header')).getText();
  }

  get configvalue() {
    return element.all(by.css('pre.config-data-section__content')).getText();
  }
}
