/**
 * Created by changdongmei on 2018/3/26.
 */

import { by } from 'protractor';

import { AlaudaBasicInfo } from '../../element_objects/alauda.basicinfo';
import { AlaudaCheckBox } from '../../element_objects/alauda.checkbox';
import { AlaudaDropdown } from '../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../element_objects/alauda.element';
import { AlaudaInputTable } from '../../element_objects/alauda.inputtable';
import { AlaudaRadio } from '../../element_objects/alauda.radio';
import { ResourceInfo } from '../component/resourceInfo';
import { SearchBox } from '../component/search.component';
import { TableComponent } from '../component/table.component';
import { PageBase } from '../page.base';

export class AuthPageBase extends PageBase {
  /**
   * 获取成员列表页面的成员表
   * 点击左导航到角色页面
   */
  navigateTo() {
    super.clickLeftNavByText('角色');
  }
  /**
   * 获列表页面的成员表
   */
  get authTable() {
    return new TableComponent();
  }
  /**
   * 获取搜索框
   */
  get searchBox() {
    return new SearchBox();
  }

  /**
   * 资源详情页的基本信息部分
   */
  get basicInfo(): AlaudaBasicInfo {
    return new AlaudaBasicInfo('基本信息', 'aui-card', '.aui-card__header h2');
  }

  /**
   * 重写父类属性，用于getElementByText 方法定位元素
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      '.aui-form-item__label',
      '.aui-form-item__content',
    );
  }
  /**
   * 页面上Pod 资源列表的table
   *
   */
  get resourceTable() {
    return new TableComponent('div .aui-card__content');
  }
  /**
   * 资源详情页的基本信息
   */
  get resourceBasicInfo() {
    return new ResourceInfo();
  }
  /**
   * 下拉框，多个下拉框
   */
  get dropdown() {
    return new AlaudaDropdown(
      'aui-select .aui-select__input',
      'aui-tooltip .aui-option',
    );
  }
  /**
   * 可编辑表格，中有多个类型的输入框或按钮
   */
  get inputtable() {
    return new AlaudaInputTable(
      'alk-subjects-editor a',
      'alk-subjects-editor aui-table-cell',
      '',
      'aui-form-item .aui-form-item',
      '.aui-form-item__label',
      'alk-subjects-editor aui-table-row',
    );
  }
  get radio() {
    return new AlaudaRadio();
  }
  get checkbox() {
    return new AlaudaCheckBox();
  }
}
