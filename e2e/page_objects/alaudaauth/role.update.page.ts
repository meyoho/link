/**
 * Created by changdongmei on 2018/3/26.
 */

import { browser, by, element } from 'protractor';

import { AlaudaElement } from '../../element_objects/alauda.element';
import { CommonPage } from '../../utility/common.page';

import { AuthPageBase } from './auth.base.page';

export class RoleUpdatePage extends AuthPageBase {
  public buttonTextConfirm;
  public buttonTextCancel;
  public labelTextName;
  public labelTextDisplayName;
  public labelTextDescription;
  public labelTextRoleType;
  constructor() {
    super();
    this.labelTextName = '名称';
    this.labelTextDisplayName = '显示名称';
    this.labelTextDescription = '描述';
    this.labelTextRoleType = '角色类型';
    this.buttonTextConfirm = '确定';
    this.buttonTextCancel = '取消';
  }

  /**
   * 确认更新信息
   */
  get title() {
    return element(by.css('alk-role-update-dialog .aui-dialog__header-title'));
  }

  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-dialog-content .alk-basic-info-field',
      '.alk-basic-info-field__label',
      '.alk-basic-info-field__content',
    );
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '描述':
        this.getElementByText(name, 'textarea').clear();
        this.getElementByText(name, 'textarea').sendKeys(value);
        break;
      default:
        this.getElementByText(name).clear();
        this.getElementByText(name).sendKeys(value);
        break;
    }
    browser.sleep(100);
  }

  /**
   * 点击列表中的更新，并等待页面加载出来
   * @param name
   */
  updateRole(displayname, description) {
    // 更改显示名称
    this.enterValue('显示名称', displayname);
    // 更改描述
    this.enterValue('描述', description);
    // 点击更新
    this.getButtonByText(this.buttonTextConfirm).click();
    CommonPage.waitElementNotPresent(
      this.getButtonByText(this.buttonTextConfirm).button,
    );
  }
}
