/**
 * Created by changdongmei on 2018/3/26.
 */

import { browser, by, element } from 'protractor';

import { AlaudaRadio } from '../../element_objects/alauda.radio';
import { CommonPage } from '../../utility/common.page';
import { Namespace } from '../component/namespace.component';

import { AuthPageBase } from './auth.base.page';

export class RoleCreatePage extends AuthPageBase {
  public buttonTextCreate = '创建';

  /**
   * 标题
   */
  get title() {
    return element(by.css('alk-role-create h2'));
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tabname: string = 'input') {
    switch (text) {
      case '名称':
        return super.getElementByText(text);
      case '显示名称':
        return super.getElementByText(text);
      case '描述':
        return super.getElementByText(text, 'textarea');
      case '角色类型':
        return new AlaudaRadio();
      default:
        return this.dropdown;
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '角色类型':
        this.getElementByText(name)
          .getRadioByName(value)
          .click();
        break;
      case '命名空间':
        this.getElementByText(name).selectinfrom(name, value);
        break;
      default:
        this.getElementByText(name).clear();
        this.getElementByText(name).sendKeys(value);
        break;
    }
    browser.sleep(100);
  }

  /**
   * 创建一个角色
   * @param rolename 角色名称
   * @param displayname 显示名称
   * @param description 描述
   * @param namespace 命名空间
   */
  createRole(rolename, displayname, description, roletype, namespace) {
    CommonPage.waitElementPresent(this.title);
    this.enterValue('名称', rolename);
    this.enterValue('显示名称', displayname);
    this.enterValue('描述', description);
    this.enterValue('角色类型', roletype);
    if (roletype === '命名空间相关') {
      this.enterValue('命名空间', namespace);
    }
    browser.sleep(1000);
    this.getButtonByText('创建').click();
    expect(this.toastsuccess.getText()).toContain('创建成功');
  }
}
