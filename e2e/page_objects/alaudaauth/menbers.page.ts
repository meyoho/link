/**
 * Created by changdongmei on 2018/3/21.
 */
import { CommonPage } from '../../utility/common.page';

import { AuthPageBase } from './auth.base.page';

export class MenberPage extends AuthPageBase {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('成员');
    CommonPage.waitElementNotPresent(this.authTable.progressbar);
  }
}
