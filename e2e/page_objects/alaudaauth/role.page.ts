/**
 * Created by changdongmei on 2018/3/26.
 */

import { browser } from 'protractor';

import { CommonPage } from '../../utility/common.page';
import { TableComponent } from '../component/table.component';

import { AuthPageBase } from './auth.base.page';
import { RoleCreatePage } from './role.create.page';
import { RoleUpdatePage } from './role.update.page';

export class RolePage extends AuthPageBase {
  public buttonTextCreate;
  public buttonTextUpdate;
  public buttonTextDelete;
  public roleUpdatePage;
  public roleCreatePage;

  constructor() {
    super();
    this.buttonTextCreate = '创建角色';
    this.buttonTextUpdate = '更新';
    this.buttonTextDelete = '删除';

    this.roleCreatePage = new RoleCreatePage();
    this.roleUpdatePage = new RoleUpdatePage();
  }
  /**
   * 点击左导航到角色页面
   */
  navigateTo() {
    super.clickLeftNavByText('角色');
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 获取详情页面的权限列表
   */
  get roleRuleTable() {
    return new TableComponent();
  }
  /**
   * 更新角色
   * @param name 要更新的角色名称
   * @param displayname 更新的参数
   * @param descripion 更新的参数
   */
  updateRole(name, displayname, descripion) {
    const that = this;
    // 判断是详情页还是列表页面
    this.getButtonByText(this.buttonTextUpdate)
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          // 是列表页面
          that.authTable.clickOperationButtonByRow(
            [name],
            this.buttonTextUpdate,
          );
        } else {
          // 是详情页面
          this.getButtonByText(this.buttonTextUpdate).click();
        }
      });
    // 等待更新页面的元素加载出来
    CommonPage.waitElementPresent(this.roleUpdatePage.title);
    // 更新显示名称与描述
    this.roleUpdatePage.updateRole(displayname, descripion);
    CommonPage.waitElementNotPresent(this.roleUpdatePage.title);
    CommonPage.waitProgressbarNotDisplay();
  }
  /**
   * 删除一个角色
   * @param name 要删除的角色名
   */
  deleteRole(name) {
    const that = this;
    // 判断是详情页还是列表页面
    this.getButtonByText(this.buttonTextDelete)
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          // 是列表页面
          that.authTable.clickOperationButtonByRow(
            [name],
            this.buttonTextDelete,
          );
        } else {
          // 是详情页面
          this.getButtonByText(this.buttonTextDelete).click();
        }
      });
    CommonPage.waitElementDisplay(this.confirmDialog.confirmTitle);
    // 点击{确认
    this.confirmDialog.clickConfirm();
  }

  /**
   * 创建角色
   * @param name 要创建的角色名称
   * @param displayname 显示名称
   * @param descripion 描述
   * @param namespace 命名空间
   */
  createRole(
    name,
    displayname,
    descripion,
    roletype = '集群相关',
    namespace = '',
  ) {
    // 点击创建角色
    this.getButtonByText(this.buttonTextCreate).click();
    // 等待创建角色页面加载出来
    CommonPage.waitElementPresent(this.roleCreatePage.title);
    // 填写创建参数,创建角色
    this.roleCreatePage.createRole(
      name,
      displayname,
      descripion,
      roletype,
      namespace,
    );
    // 等待页面消失
    CommonPage.waitElementNotPresent(this.roleCreatePage.title);
    browser.sleep(100);
    CommonPage.waitProgressbarNotDisplay();
  }
}
