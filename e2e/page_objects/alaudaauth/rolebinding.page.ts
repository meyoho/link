/**
 * Created by changdongmei on 2018/3/26.
 */

import { browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AlaudaBasicInfo } from '../../element_objects/alauda.basicinfo';
import { AlaudaRadio } from '../../element_objects/alauda.radio';
import { AlaudaTable } from '../../element_objects/alauda.table';
import { CommonPage } from '../../utility/common.page';
import { TableComponent } from '../component/table.component';

import { AuthPageBase } from './auth.base.page';
import { RoleCreatePage } from './role.create.page';
import { RoleUpdatePage } from './role.update.page';

export class RoleBindingPage extends AuthPageBase {
  /**
   * 点击左导航到角色页面
   */
  navigateTo() {
    super.clickLeftNavByText('角色绑定');
  }

  /**
   * 资源详情页的基本信息部分
   */
  get basicInfo() {
    return new AlaudaBasicInfo(
      '基本信息',
      'aui-card',
      '.basic-info-header__label',
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tabname: string = 'input') {
    if (text === '角色绑定类型') {
      return new AlaudaRadio();
    } else if (text === '命名空间' || text === '角色名称') {
      return this.dropdown;
    } else if (text === '对象') {
      return this.inputtable;
    } else if (text === '描述') {
      return super.getElementByText(text, 'textarea');
    } else {
      return super.getElementByText(text, 'input');
    }
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    if (name === '角色绑定类型') {
      this.getElementByText(name)
        .getRadioByName(value)
        .click();
    } else if (name === '命名空间') {
      this.getElementByText(name).selectinfrom(name, value);
    } else if (name === '角色名称') {
      this.getElementByText(name).selectsinfrom(name, value);
    } else if (name === '对象') {
      this.getElementByText(name).fillinTable(name, value);
    } else {
      this.getElementByText(name).clear();
      this.getElementByText(name).sendKeys(value);
    }
    browser.sleep(100);
  }

  /**
   * 创建一个角色绑定
   * @param name 名称
   * @param type 角色绑定类型
   * @param user 用户名与类型
   * @param desc 描述
   * @param displayname 显示名称
   */
  createRoleBinding(
    name,
    type,
    user,
    desc = '自动化创建角色绑定',
    displayname = '角色绑定测试',
  ) {
    this.enterValue('名称', name);
    this.enterValue('显示名称', displayname);
    this.enterValue('描述', desc);
    this.enterValue('角色绑定类型', type);
    browser.sleep(100);
    if (type === '命名空间相关') {
      this.enterValue('命名空间', ServerConf.NAMESPACE);
      this.enterValue('角色名称', ['命名空间相关', name]);
    } else {
      this.enterValue('角色名称', [name]);
    }
    this.enterValue('对象', user);
  }

  updateRoleBinding(displayname, desc) {
    this.enterValue('显示名称', displayname);
    this.enterValue('描述', desc);
  }
  clickupdatebutton() {
    element(
      by.css('alk-role-bindings-update-dialog .aui-button--primary'),
    ).click();
  }
}
