/**
 * Created by changdongmei on 2018/3/26.
 */

import { browser, by, element } from 'protractor';

import { AlaudaBasicInfo } from '../../element_objects/alauda.basicinfo';
import { AlaudaRadio } from '../../element_objects/alauda.radio';
import { CommonPage } from '../../utility/common.page';

import { AuthPageBase } from './auth.base.page';

export class RolePowerPage extends AuthPageBase {
  /**
   * 标题
   */
  get title() {
    return element(by.css('aui-card__header'));
  }
  /**
   * 资源详情页的基本信息部分
   */
  get basicInfo() {
    return new AlaudaBasicInfo('更新权限规则');
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tabname: string = 'input') {
    if (text === '操作类型' || text === '资源类型') {
      return this.radio;
    } else if (text === 'API 组') {
      return super.getElementByText(text);
    } else {
      return this.checkbox;
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    if (name === '操作类型' || name === '资源类型') {
      this.getElementByText(name).getRadioInform(name, value);
    } else if (name === 'API 组') {
      if (value !== '') {
        this.getElementByText(name).clear();
        this.getElementByText(name).sendKeys(value);
      }
    } else {
      this.getElementByText(name).checkInform(name, value);
    }
    browser.sleep(100);
  }

  /**
   * 添加权限
   * @param actiontype 操作类型单选 【只读】 【全部】 【自定义】
   * @param action 操作复选框 传值类型['get', 'list', 'proxy', 'watch', 'create', 'update', 'delete', 'deletecollection', 'patch']
   * @param resourcetype 资源类型单选 【推荐】 【全部】 【自定义】
   * @param resource 资源复选框 ['', '']
   * @param adminresource 管理员资源 ['角色绑定', '角色', '保密字典']
   * @param api 输入框
   */
  addpower(
    actiontype,
    resourcetype,
    action = [''],
    resource = [''],
    adminresource = [''],
    api = '',
  ) {
    this.enterValue('操作类型', actiontype);
    this.enterValue('操作', action);
    this.enterValue('资源类型', resourcetype);
    this.enterValue('资源', resource);
    this.enterValue('管理员资源', adminresource);
    this.enterValue('API 组', api);
  }
}
