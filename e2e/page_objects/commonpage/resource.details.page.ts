/**
 * 资源详情的页面类
 * Created by liuwei on 2018/4/28.
 *
 */
import { $$, by } from 'protractor';

import { AlaudaBasicInfo } from '../../element_objects/alauda.basicinfo';
import { AlaudaDataViewer } from '../../element_objects/alauda.dataViewer';
import { AlaudaTab } from '../../element_objects/alauda.tab';
import { AlaudaTag } from '../../element_object_new/alauda.tag';
import { Logviewer } from '../component/logs-viewer.component';
import { TableComponent } from '../component/table.component';
import { YamlComponent } from '../component/yaml.component';
import { PageBase } from '../page.base';

export class ResourceDetailPage extends PageBase {
  /**
   * 资源详情页的，tab 信息
   */
  get resourceInfoTab(): AlaudaTab {
    return new AlaudaTab(by.css('.aui-tab-label__content'));
  }

  /**
   * 资源详情页的 yaml 编辑器
   */
  get yamlEditor(): YamlComponent {
    return new YamlComponent(by.tagName('alk-resource-yaml-editor'));
  }

  /**
   * 资源详情页的 配置字典表格部分
   */
  get configmapList(): TableComponent {
    return new TableComponent('alk-pod-env-list');
  }

  /**
   * 资源详情页的日志部分
   */
  get logviewer() {
    return new Logviewer();
  }

  /**
   * 资源详情页的 事件表格部分
   */
  get eventList(): TableComponent {
    return new TableComponent('alk-event-list');
  }

  /**
   * 资源详情页的基本信息部分
   */
  get basicInfo(): AlaudaBasicInfo {
    return new AlaudaBasicInfo('基本信息');
  }

  /**
   * 资源详情页的系统信息部分
   */
  get systemInfo(): AlaudaBasicInfo {
    return new AlaudaBasicInfo('系统信息');
  }

  /**
   * 资源详情页的容器组表格部分
   */
  get container_list(): TableComponent {
    return new AlaudaBasicInfo('容器').getTable('alk-container-list');
  }

  /**
   * 资源详情页的容器组表格部分
   */
  get podList(): TableComponent {
    return new AlaudaBasicInfo('容器组').getTable('alk-pod-list');
  }

  /**
   * 资源详情页的旧副本集的部分
   */
  get old_replicasetList(): TableComponent {
    return new AlaudaBasicInfo('旧副本集').getTable('alk-simple-resource-list');
  }

  /**
   * 资源详情页的旧副本集的部分
   */
  get endpointList(): TableComponent {
    return new AlaudaBasicInfo('端点').getTable('alk-endpoint-list');
  }

  /**
   * 资源详情页的PVC表格信息部分
   */
  get pvcList() {
    return new TableComponent('alk-ref-pvc-list');
  }

  /**
   * 详情页的服务表格部分
   */
  get serviceList(): TableComponent {
    return new AlaudaBasicInfo('服务').getTable('alk-ref-service-list');
  }

  /**
   * 详情页的现状表格部分
   */
  get conditionList(): TableComponent {
    return new AlaudaBasicInfo('现状').getTable('alk-condition-list');
  }

  /**
   * 资源详情页的配置字典配置引用的tag
   */
  get envtag(): AlaudaTag {
    return new AlaudaTag($$('.env-from-tags aui-tag'));
  }

  get dataViewer(): AlaudaDataViewer {
    return new AlaudaDataViewer();
  }
}
