/**
 * 其他资源列表的页面类
 * Created by liuwei on 2018/3/1.
 *
 */
import { $$, ElementFinder, browser, by, element } from 'protractor';

import { AlaudaButton } from '../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../element_objects/alauda.element';
import { AlaudaInputGroup } from '../../element_objects/alauda.inputgroup';
import { AlaudaInputTable } from '../../element_objects/alauda.inputtable';
import { MuitiSelect } from '../../element_objects/alauda.muitiselect';
import { AlaudaRadio } from '../../element_objects/alauda.radio';
import { AlaudaTab } from '../../element_objects/alauda.tab';
import { AlaudaRadioGroup } from '../../element_object_new/alauda.aui_radio_group';
import { CommonPage } from '../../utility/common.page';
import { AdvancedAttributes } from '../component/advancedAttributes';
import { KeyValueControl } from '../component/key_value.component';
import { ResourceInfo } from '../component/resourceInfo';
import { SearchBox } from '../component/search.component';
import { TableComponent } from '../component/table.component';
import { YamlComponent } from '../component/yaml.component';
import { ScaleDialog } from '../computer/scaleDialog';
import { PageBase } from '../page.base';

import { ResourceDetailPage } from './resource.details.page';

export class ResourcePage extends PageBase {
  navigateTo() {
    // browser.get('/others');
    CommonPage.clickLeftNavByText('命名空间相关资源');
  }
  /**
   * 获取yaml更新页面的更新按钮
   */
  get yamlUpdateButton() {
    return element(by.css('button.aui-button--primary'));
  }
  get resourceCreate() {
    return new AlaudaButton(
      by.css('.aui-layout__toolbar .layout-toolbar__create-button'),
    );
  }
  get yamlCreate() {
    return element(by.css('button.aui-menu-item--default'));
  }
  /**
   * 重写父类属性，用于getElementByText 方法定位元素
   */
  get alaudaElement() {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      '.aui-form-item__label',
      '.aui-form-item__control',
    );
  }
  /**
   * 获取muitiSelect下拉框
   */
  getmuitiSelect() {
    return new MuitiSelect();
  }
  /**
   * 获取命名空间添加资源配额页面的CPU与内存控件
   */
  get resourceLimit() {
    return new AlaudaInputGroup(
      'aui-form-item .aui-form-item',
      'aui-form-item .aui-form-item__label',
      'aui-input-group .aui-input',
    );
  }
  /**
   * 获取输入表格
   */
  getAlaudaInputTable() {
    return new AlaudaInputTable();
  }
  /**
   * 页面上Pod 资源列表的table
   *
   */
  get resourceTable() {
    return new TableComponent('div .aui-card__content');
  }

  /**
   * 返回添加存储卷的按钮
   */
  get volumesButton() {
    return element(by.css('alk-volumes-form button'));
  }
  /**
   * 此方法用于返回创建部署页面的两个下拉列表
   * @param icon 定位有下拉按钮的文本框的控件的唯一的css
   */
  get auiSelect() {
    return new AlaudaDropdown(
      'aui-select .aui-select__input',
      'aui-tooltip .aui-option',
    );
  }
  /**
   * 检索框
   */
  get searchBox() {
    return new SearchBox();
  }
  searchResource(resourceName) {
    this.searchBox.search(resourceName);
  }

  /**

   * 更新标签dialog 页
   */
  get resourcelabel() {
    return new KeyValueControl(
      by.xpath('//div[@class="aui-dialog-title"]'),
      by.tagName('aui-dialog-content'),
      by.tagName('aui-dialog-footer'),
    );
  }

  /**
   * 更新注解dialog 页
   */
  get resourceAnnotations() {
    return new KeyValueControl(
      by.xpath('//div[@class="aui-dialog-title"]'),
      by.tagName('aui-dialog-content'),
      by.tagName('aui-dialog-footer'),
    );
  }

  /**
   * 更新选择器dialog 页
   */
  get resourceSelector() {
    return new KeyValueControl(
      by.xpath('//div[@class="aui-dialog-title"]'),
      by.tagName('aui-dialog-content'),
      by.tagName('aui-dialog-footer'),
    );
  }

  /**
   * 扩缩容dialog 页
   */
  get resourceScale() {
    return new ScaleDialog();
  }

  /**
   * 点击确认
   */
  clickconfirmButton() {
    this.getButtonByText('确定').click();
  }

  /**
   * 通过yaml 创建资源的dialog 页面
   */
  get yamlCreator() {
    return new YamlComponent(by.css('alk-console router-outlet + *'));
  }

  /**
   * 资源详情页的基本信息
   */
  get resourceBasicInfo() {
    return new ResourceInfo();
  }

  /**
   * 资源的详情页
   */
  get detailPage(): ResourceDetailPage {
    return new ResourceDetailPage();
  }

  /**
   * 获取表单与yaml创建的按钮
   */
  get radioButton(): AlaudaRadio {
    return new AlaudaRadio('alk-mode-selector .mode-selector__label');
  }

  /**
   * 获取表单与yaml创建的按钮
   */
  get radioGroupButton(): AlaudaRadioGroup {
    return new AlaudaRadioGroup($$('aui-radio-group .aui-radio-button'));
  }

  /**
   * 获取配置管理页面的容器组的名称
   */
  get containerTitle() {
    return $$('div.container-title');
  }

  /**
   * 获取高级参数
   */
  get advancedAttributes() {
    return new AdvancedAttributes();
  }
  /**
   *
   * @param name
   */
  clickYAMLbutton() {
    CommonPage.waitElementPresent(this.getButtonByText('YAML'));
    this.getButtonByText('YAML').click();
    this.yamlCreator.waiteditorspinnerNotdisplay();
    browser.sleep(100);
  }
  /**
   * 资源创建成功后，页面需要等待30秒自动刷新状态才会发生变化，
   * 这个过程太慢了，waitAction 函数主动发请求，让状态发生变化
   * @param elem 需要等待的页面元素
   * @param waitAction 主动请求的函数
   * @param expectText 期望值等待的页面元素值等于 expectText
   * @param timeout 超时时间
   */
  waitCreateSuccess(
    elem: ElementFinder,
    waitAction = () => {
      CommonPage.clickLeftNavByText('副本集');
      CommonPage.clickLeftNavByText('守护进程集');
    },
    expectText = '1 / 1',
    timeout = 120000,
  ) {
    browser.driver
      .wait(() => {
        return elem.getText().then(text => {
          waitAction();
          return text.trim() === String(expectText).trim();
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error: [' + err + ']');
          return false;
        },
      );
  }
  /**
   * 通过UI创建部署等页面的添加存储卷按钮
   */
  clickAddVolume() {
    element(by.css('alk-volumes-form .add-button')).click();
  }
  /**
   * 通过ui创建部署等页面的点击添加容器
   */
  clickAddContaineButton() {
    this.getButtonByText('添加容器').click();
    browser.sleep(1000);
  }
  /**
   * 点击创建部署
   */
  clickCreateButton() {
    this.getButtonByText('创建').click();
  }
  /**
   * 获取更多参数
   */
  get more() {
    return new AdvancedAttributes(
      '.aui-card',
      '.aui-card__header',
      '.foldable-bar',
      '.foldable-bar',
    );
  }
}
