/**
 * overview page
 */

import { AlaudaChartCard } from '../../element_objects/alauda.chartcard';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class OverviewPage extends ResourcePage {
  /**
   * 点击左导航概览
   */
  navigateTo() {
    this.clickLeftNavByText('概览');
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 获取概览页面
   * @param name 如命名空间 主机节点 事件等
   */
  getchartCard(name) {
    return new AlaudaChartCard(name);
  }
}
