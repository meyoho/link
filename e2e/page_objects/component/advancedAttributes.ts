import { $$, browser, by, element } from 'protractor';

import { CommonPage } from '../../utility/common.page';

export class AdvancedAttributes {
  private _auicardselector;
  private _auititleselector;
  private _barselector;
  private _dividerselector;

  constructor(
    auicard_selector = '.aui-card',
    auititle_selector = '.aui-card__header',
    bar_selector = '.foldable-bar',
    divider_selector = '.alk-card-section-divider',
  ) {
    this._auicardselector = auicard_selector;
    this._auititleselector = auititle_selector;
    this._barselector = bar_selector;
    this._dividerselector = divider_selector;
  }
  /**
   * 获取高级参数模块的标题
   */
  get titleofAdvanced() {
    return element(by.css(this._dividerselector));
  }
  /**
   * 根据card的标题名称返回一个
   * @param titleLabelText aui-card 的标题
   */
  clickAdvanced(titleLabelText: string) {
    $$(this._auicardselector)
      .filter((elem, index) => {
        return elem
          .$(this._auititleselector)
          .getText()
          .then(text => {
            return text === titleLabelText;
          });
      })
      .first()
      .$(this._barselector)
      .click();
  }
}
