/**
 * Created by liuwei on 2018/3/24.
 *
 */
import { browser, by, element } from 'protractor';

import { AlaudaDropdown } from '../../element_objects/alauda.dropdown';
import { CommonPage } from '../../utility/common.page';

export class Namespace {
  private icon_selector;
  private item_selector;

  /**
   * 命名空间控件
   * @param icon_selector 下拉框icon 的selector
   * @param item_selector 下拉框的每个item 的selector
   */
  constructor(
    icon_selector = by.css('.namespace-label .namespace-label__icon'),
    item_selector = by.css('aui-menu-item button[class*=aui-menu-item]'),
  ) {
    this.icon_selector = icon_selector;
    this.item_selector = item_selector;
  }

  /**
   * 页面上的选择命名空间的空间
   */
  get namespace() {
    return new AlaudaDropdown(this.icon_selector, this.item_selector);
  }

  /**
   * 表格数据加载时的等待提示
   */
  get progressbar() {
    return element(by.css('mat-progress-bar[class*=mat-progress-bar]'));
  }

  /**
   * 选择命名空间
   * @param namespace 命名空间
   */
  select(namespace) {
    this.namespace.select(namespace);
    browser.sleep(200);
    this.progressbar.isPresent().then(isPresent => {
      if (isPresent) {
        CommonPage.waitProgressbarNotDisplay();
      } else {
        console.log('progressbar is not present');
      }
    });
  }
}
