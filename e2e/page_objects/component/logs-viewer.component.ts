/**
 * Created by liuwei on 2018/3/29.
 *
 */
import { browser, by, element } from 'protractor';

import { AlaudaCheckBox } from '../../element_objects/alauda.checkbox';
import { AlaudaDropdown } from '../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../element_objects/alauda.inputbox';

export class Logviewer {
  /**
   * 容器的下拉框
   */
  get container() {
    return new AlaudaDropdown(
      by.css('aui-select input'),
      by.css('aui-option div[class*=aui-option]'),
    );
  }

  /**
   * 日志文本框
   */
  get logTextarea() {
    return new AlaudaInputbox(
      by.css('.overflow-guard .monaco-scrollable-element'),
    );
  }

  /**
   * 查找工具栏的文本框
   */
  get finder_inputbox() {
    return new AlaudaInputbox(
      by.css('.monaco-findInput .monaco-inputbox input'),
    );
  }

  /**
   * 自动更新checkbox
   */
  get checkBoxAutoRefresh() {
    return new AlaudaCheckBox();
  }

  /**
   * 查找下拉工具栏的文本框右侧的查找结果
   */
  get resultfinder() {
    return element(by.css('.find-widget .matchesCount'));
  }

  /**
   * 判断日志是否是夜间模式
   */
  isNightMode() {
    return element(by.css('alk-logs-viewer .log-container'))
      .getAttribute('class')
      .then(function(css) {
        return css.includes('--dark');
      });
  }

  /**
   * 获取所有行号
   */
  get lineNumber() {
    return element.all('.overflow-guard .line-numbers');
  }

  /**
   * 获取日志查询时间
   */
  get logqueryTime() {
    const css = '.log-container-toolbar__time-span';
    return element(by.css(css)).getText();
  }

  /**
   * 等待日志显示
   * @param expectText 日志的部分内容，
   * @param timeout 超时时间
   */
  waitLogDisplay(expectText: string = 'hehe.txt', timeout = 120000) {
    return browser.driver
      .wait(() => {
        return this.logTextarea.getText().then(text => {
          return text.trim().includes(String(expectText).trim());
        });
      }, timeout)
      .then(
        () => true,
        err => {
          this.logTextarea.getText().then(text => {
            console.log(
              `容器中的日志没有包含${expectText} \n此时日志显示的是： ${text}`,
            );
          });
          return false;
        },
      );
  }

  /**
   * 获得工具栏上的按钮
   * @param innerText 工具栏button 的文本
   */
  getToolbarButton(innerText = '首页') {
    const elembuttonList = element.all(
      by.css('.log-container-toolbar .log-container-toolbar__control-button'),
    );
    switch (innerText) {
      case '首页':
        return elembuttonList.get(0);
      case '上一页':
        return elembuttonList.get(1);
      case '下一页':
        return elembuttonList.get(2);
      case '最后一页':
        return elembuttonList.get(3);
      case '时间':
        return element(
          by.css(
            '.log-container-toolbar>*:not(.log-container-toolbar__control-button)' +
              ':not(.log-container-toolbar__spacer)',
          ),
        );
    }
    return elembuttonList.filter((elem, index) => {
      return elem.getText().then(text => {
        return text.trim() === innerText;
      });
    });
  }
}
