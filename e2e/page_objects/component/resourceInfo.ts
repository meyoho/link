/**
 * 资源列表的页面类, 包含单击资源名进入详情页， 单击右侧选择操作，验证排序等功能
 * Created by liuwei on 2018/3/1.
 *
 */
import { by } from 'protractor';

import { AlaudaBasicInfo } from '../../element_objects/alauda.basicinfo';
import { AlaudaTab } from '../../element_objects/alauda.tab';

import { Logviewer } from './logs-viewer.component';

export class ResourceInfo {
  /**
   * 资源详情页的，tab 信息,如‘基本信息’‘YAML’‘日志’‘事件’等
   */
  get resourceInfoTab() {
    return new AlaudaTab(by.css('.aui-tab-label__content'));
  }

  /**
   * 资源详情页的‘基本信息’部分
   */
  get basicInfo() {
    return new AlaudaBasicInfo('基本信息');
  }

  /**
   * 资源详情页的基本信息的按钮
   */
  get basicButtion() {
    return new AlaudaBasicInfo(
      '基本信息',
      'aui-card',
      '.aui-card__header',
      '.alk-basic-info-field',
      '.alk-basic-info-field__label',
      '.basic-pencil_s',
    );
  }
  /**
   * 资源详情页的日志部分
   */
  get logviewer() {
    return new Logviewer();
  }
}
