/**
 * 资源列表的页面类, 包含单击资源名进入详情页， 单击右侧选择操作，验证排序等功能
 * Created by liuwei on 2018/3/1.
 *
 */
import { $, browser, by, element, promise } from 'protractor';

import { AlaudaTable } from '../../element_objects/alauda.table';
import { CommonPage } from '../../utility/common.page';

export class TableComponent extends AlaudaTable {
  private _parent_selector;
  // private _header_cell_selector;
  private _body_cell_selector;
  // private _body_row_selector;

  constructor(
    parent_selector: string = 'aui-card',
    header_cell_selector: string = '.aui-table__header-cell',
    body_cell_selector: string = '.aui-table__cell',
    body_row_selector: string = '.aui-table__row',
  ) {
    super(
      `${parent_selector} ${header_cell_selector}`,
      `${parent_selector} ${body_cell_selector}`,
      `${parent_selector} ${body_row_selector}`,
    );
    // this._header_cell_selector = `${parent_selector} ${header_cell_selector}`;
    this._body_cell_selector = `${parent_selector} ${body_cell_selector}`;
    // this._body_row_selector = `${parent_selector} ${body_row_selector}`;
    this._parent_selector = parent_selector;
  }

  /**
   * 表格数据加载时的等待提示
   */
  get progressbar() {
    return element(by.css('.mat-progress-bar'));
  }

  /**
   * 重写父类的方法，单击表头后等待提示消失
   * @param headername 表头列名
   */
  clickHeaderByName(headername) {
    super.clickHeaderByName(headername);
    CommonPage.waitProgressbarNotDisplay();
    return browser.sleep(100);
  }

  /**
   * 根据关键字keys, 找到资源列表的一行,单击资源名称进入详情页
   * @param keys [podname2317, namespace2317, 'Pod']
   * @return {any} 按名称单击资源
   */
  clickResourceNameByRow(keys): any {
    CommonPage.waitElementPresent(
      this.getRow(keys).$(`${this._body_cell_selector} a`),
    );
    this.getRow(keys)
      .$('.aui-table__cell a')
      .click();
    CommonPage.waitElementNotPresent($(`alk-search-input input`));
    CommonPage.waitProgressbarNotDisplay();
    return browser.sleep(500);
  }

  /**
   * 根据关键字keys, 找到资源列表的一行， 根据columeName 找到所在列的单元格里面的标签
   * @param keys [podname2317, namespace2317, 'Pod']
   * @return {any}
   */
  getCellLabel(keys, columeName = '名称'): any {
    const body_cell_selector = this._body_cell_selector;
    return this.getCell(columeName, keys).then(function(elem) {
      return elem.$$(`${body_cell_selector} .plain-container__label`).getText();
    });
  }

  /**
   * 根据关键字keys, 找到资源列表的一行，单击操作按钮
   * @param keys [podname2317, namespace2317, 'Pod']
   * @param actionName 操作的名字，例如 '更新'， '更新标签'， '更新注解'， '删除'
   * @return {any}
   */
  clickOperationButtonByRow(keys, actionName): any {
    CommonPage.waitElementClickable(
      this.getRow(keys).$('.cdk-column-action .aui-icon'),
    );
    this.getRow(keys)
      .$('.cdk-column-action .aui-icon')
      .click();
    CommonPage.waitElementPresent(element(by.css('div[class*=aui-menu]')));
    browser.sleep(100);
    return this._selectOperation(actionName);
  }

  /**
   * 操作列表的所有元素
   */
  get _operationList() {
    return element.all(by.css('aui-menu-item button[class*=aui-menu-item]'));
  }

  /**
   * 单击操作按钮，或从table 的某行单击操作列按钮后，选择一个操作
   * @parameter {menu_item} string 类型, 操作的名字，例如 '更新'， '更新标签'， '更新注解'， '删除'
   *
   */
  _selectOperation(menu_item) {
    CommonPage.waitElementPresent(element(by.css('div[class*=aui-menu]')));
    const textList = this._operationList.getText();
    this._operationList
      .filter(elem => {
        return elem.getText().then(text => {
          return text.indexOf(menu_item) !== -1;
        });
      })
      .first()
      .click();
    CommonPage.waitProgressbarNotDisplay();
    return textList;
  }

  /**
   * 重写父类的方法 getColumeTextByName, 验证排序的方法会依赖这个
   * @param name 表格的列的名称
   */
  getColumeTextByName(name) {
    return this._getColumeIndexByColumeName(name).then(tableInfo => {
      const index = (tableInfo.columeIndex + 1).toString();
      switch (name) {
        case '名称':
          return element
            .all(by.xpath('//aui-table-cell[' + index + ']//a'))
            .getText();
        case '创建时间':
          return element
            .all(by.xpath('//aui-table-cell[' + index + ']//span'))
            .getAttribute('title');
        default:
          return super.getColumeTextByName(name);
      }
    });
  }

  /**
   * 表格为空时的显示
   */
  get noResult() {
    return $(this._parent_selector).$('alk-zero-state .zero-state-hint');
  }

  /**
   * 返回列表中的总数，有分页时从分页处取，无分页时返回列表中的条数
   */
  get totalNum(): promise.Promise<string> {
    let totalnum;
    element(by.css('aui-paginator .aui-paginator'))
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          $('aui-paginator .aui-paginator .aui-paginator__total')
            .getText()
            .then(text => {
              totalnum = text.substring(
                text.indexOf('共') + 1,
                text.indexOf('条'),
              );
            });
        } else {
          this.getRowCount().then(num => {
            totalnum = num.toString();
          });
        }
      });
    return browser.sleep(100).then(() => {
      return totalnum;
    });
  }
}
