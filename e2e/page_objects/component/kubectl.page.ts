/**
 * 其他资源列表的页面类
 * Created by liuwei on 2018/3/1.
 *
 */
import { by, element } from 'protractor';

import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';

export class KubectlPage extends ResourcePage {
  get textlayer() {
    return element(by.css('canvas.xterm-text-layer'));
  }
  getterminaltoolbar(text) {
    return element
      .all(by.css('alk-terminal-page .terminal-toolbar__control-button'))
      .filter(elem => {
        return elem.getText().then(innertext => {
          return text === innertext;
        });
      })
      .first();
  }
  get alkbreadcrumb() {
    return element(by.css('alk-terminal-page alk-breadcrumb'));
  }
  get connectionstatus() {
    CommonPage.waitElementPresent(
      element(by.css('div.status-indicator--goodConnection')),
    );
    return element(by.css('div.status-indicator--goodConnection'))
      .isPresent()
      .then(isPresent => {
        return isPresent;
      });
  }
}
