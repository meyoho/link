/**
 * Created by liuwei on 2018/3/24.
 *
 */
import { browser, by, element } from 'protractor';

import { AlaudaSearchBox } from '../../element_objects/alauda.searchbox';
import { CommonPage } from '../../utility/common.page';

export class SearchBox {
  private icon_selector;
  private item_selector;

  /**
   * 命名空间控件
   * @param icon_selector 下拉框icon 的selector
   * @param item_selector 下拉框的每个item 的selector
   */
  constructor(
    icon_selector = by.css(
      'alk-search-input .search-icon-container__search-icon',
    ),
    item_selector = by.css('alk-search-input input'),
  ) {
    this.icon_selector = icon_selector;
    this.item_selector = item_selector;
  }

  /**
   * 检索框
   */
  get searchBox() {
    return new AlaudaSearchBox(this.item_selector, this.icon_selector);
  }

  /**
   * 表格数据加载时的等待提示
   */
  get progressbar() {
    return element(by.css('.mat-progress-bar'));
  }

  /**
   * 搜索
   * @param value 命名空间
   */
  search(value) {
    this.searchBox.search(value);
    browser.sleep(500);
    CommonPage.waitProgressbarNotDisplay();
  }
}
