/**
 * 封装更新资源，更新注释 页面类，包含key, value 的修改和增加功能
 * Created by liuwei on 2018/3/16.
 */

import { browser, by, element } from 'protractor';

import { AlaudaInputbox } from '../../element_objects/alauda.inputbox';
import { CommonPage } from '../../utility/common.page';

export class KeyValueControl {
  private _labelselector;
  private _contentselector;
  private _actionselector;

  constructor(labelselector, contentselector, actionselector) {
    this._labelselector = labelselector;
    this._contentselector = contentselector;
    this._actionselector = actionselector;
  }

  /**
   * 更新资源标签，更新注解，的 dialog 页面title
   * @return {ElementFinder}
   */
  get title() {
    return element(this._labelselector);
  }

  get _label() {
    return element(this._labelselector);
  }
  get _content() {
    return element(this._contentselector);
  }

  get _action() {
    return element(this._actionselector);
  }

  /**
   * 修改一个已经存在的值
   * @param key
   * @param value
   */
  setValue(key, value) {
    // 找到所有文本框
    const elemList = element.all(by.css('alk-key-value-inputs input'));

    // 查找值等于key的文本框
    const elemkey = elemList
      .filter(function(elem, index) {
        return elem.getAttribute('ng-reflect-model').then(function(text) {
          return text.indexOf(key) !== -1;
        });
      })
      .first();

    // 在找到的文本框中，输入value
    elemkey.clear();
    elemkey.sendKeys(value);
  }

  /**
   * 新增一个值
   * @param key
   * @param value
   */
  newValue(key, value) {
    // 找到【+】 按钮，注：页面中只有一个【+】按钮
    const addbutton = this._content.$('.add-button');
    // 单击 【+】 按钮后，页面新增2个空白文本框，要求输入key, value
    addbutton.click();
    browser.sleep(100);

    // 找到页面新增的2个空白文本框（key, value）  svgicon="add"
    const keye_elem = new AlaudaInputbox(
      by.xpath(
        '//div[@class="alk-form-table__bottom-control-buttons"]/../../preceding-sibling::tr[2]/td[1]/input',
      ),
    );
    const value_elem = new AlaudaInputbox(
      by.xpath(
        '//div[@class="alk-form-table__bottom-control-buttons"]/../../preceding-sibling::tr[2]/td[2]/input',
      ),
    );

    // 输入key, value
    keye_elem.input(key);
    value_elem.input(value);
  }

  /**
   * 更新按钮
   * @return {AlaudaButton}
   */
  get buttonUpdate() {
    return this._action.$('.aui-button--primary');
  }

  /**
   * 单击更新按钮
   * @return {any}
   */
  clickUpdate() {
    CommonPage.waitElementPresent(this.buttonUpdate);
    this.buttonUpdate.click();
    // 单击成功后等待元素消失
    CommonPage.waitElementNotPresent(this.buttonUpdate);
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 取消按钮
   * @return {AlaudaButton}
   */
  get buttonCancel() {
    return this._action.$('.aui-button--default');
  }

  /**
   * 单击取消按钮
   * @return {any}
   */
  clickCancel() {
    this.buttonCancel.click();
    return CommonPage.waitElementNotPresent(this.buttonCancel);
  }
}
