/**
 * Created by liuwei on 2018/3/1.
 *
 */
import { browser, by, element } from 'protractor';

import { AlaudaYamlEditor } from '../../element_objects/alauda.ymaleditor';
import { CommonPage } from '../../utility/common.page';

export class YamlComponent extends AlaudaYamlEditor {
  /**
   * 编辑器
   *
   * @parameter {codeEditorSelector} code 编辑器的selector
   * @parameter {toolbarselector} code 编辑器上面的 toolbar
   *
   */
  constructor(
    rootSelector = by.css('.aui-dialog'),
    toolbarselector = '.aui-code-editor-toolbar__control-button',
    codeEditor_selector = '.aui-code-editor',
  ) {
    super(rootSelector, toolbarselector, codeEditor_selector);
  }

  /**
   * 编辑器的左上角编辑语言控件
   *
   */
  get language() {
    return this.rootElement.$('.aui-code-editor-toolbar__language');
  }

  /**
   * 编辑器的下边的YAML 样例
   *
   */
  get labelsample() {
    return this.rootElement.$('.sample-toolbar div:nth-child(1)');
  }

  /**
   * 编辑器的下边的写入button
   *
   */
  get labelwrite() {
    return this.rootElement.$('.sample-toolbar div:nth-child(2)');
  }
  /**
   * yaml读写面板
   */
  get yamlreadwrite() {
    return this.rootElement.$('.ng-monaco-editor-spinner');
  }
  /**
   * 等待ng-monaco-editor-spinner 消失
   */
  waiteditorspinnerNotdisplay() {
    CommonPage.waitElementNotDisplay(this.yamlreadwrite);
  }
  /**
   * 编辑器的下边的查看button
   *
   */
  get labelview() {
    return this.rootElement.$('.sample-toolbar div:nth-child(3)');
  }

  get buttonCreate() {
    return this.rootElement.$('.aui-button--primary');
  }

  // 单击创建按钮
  clickButtonCreate() {
    this.buttonCreate.click();
    element(by.css('aui-confirm-dialog .aui-button--primary'))
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          CommonPage.waitElementNotDisplay(this.buttonCancel);
        }
      });
    CommonPage.waitProgressbarNotDisplay();
    return browser.sleep(100);
  }

  get buttonCancel() {
    return this.rootElement.$('.aui-button--default');
  }
}
