import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { TableComponent } from '../component/table.component';

export class NetworkPoliciesPage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('网络策略');
    CommonPage.waitProgressbarNotDisplay();
  }
}
