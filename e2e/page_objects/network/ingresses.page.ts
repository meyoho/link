import { browser, protractor } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AlaudaElement } from '../../element_objects/alauda.element';
import { AlaudaInputGroup } from '../../element_objects/alauda.inputgroup';
import { AlaudaTable } from '../../element_objects/alauda.table';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { TableComponent } from '../component/table.component';
import { DeploymentPage } from '../computer/deployment.page';

export class IngressesPage extends ResourcePage {
  navigateTo() {
    // super.navigateTo();
    CommonPage.clickLeftNavByText('访问权');
    CommonPage.waitProgressbarNotDisplay();
  }

  get RuleTable() {
    return new AlaudaTable('table th', 'table td', 'table tr');
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tabname: string = 'input'): any {
    if (text === '名称' || text === '域名') {
      return super.getElementByText(text);
    } else if (text === '规则' || text === '标签' || text === '注解') {
      return this.getAlaudaInputTable();
    } else {
      return this.auiSelect;
    }
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    if (name === '名称' || name === '域名') {
      this.getElementByText(name).clear();
      this.getElementByText(name).sendKeys(value);
    } else if (name === '规则' || name === '标签' || name === '注解') {
      this.getElementByText(name).addandfillinTable(name, value);
    } else {
      this.getElementByText(name).selectinfrom(name, value);
      CommonPage.waitProgressbarNotDisplay();
    }
  }
  /**
   * 添加ingress的基本详情
   * @param namespace 命名空间
   * @param name 名称
   * @param servicename 默认服务 默认不选择传入名称后选择
   * @param serviceport 服务端口
   */
  addingress(
    name,
    servicename = '',
    serviceport = '',
    namespace = ServerConf.NAMESPACE,
  ) {
    this.enterValue('命名空间', namespace);
    CommonPage.waitProgressbarNotDisplay();
    this.enterValue('名称', name);
    if (servicename !== '') {
      this.enterValue('默认服务', servicename);
      CommonPage.waitProgressbarNotDisplay();
      this.enterValue('服务端口', serviceport);
    }
  }

  /**
   * 添加域名
   * @param domain 域名
   * @param rule 规则 [路径，服务名称，服务端口]
   * @param tls tls证书 默认不添加 传入参数后添加
   */
  adddomain(domain = '', rule = []) {
    this.enterValue('域名', domain);
    this.enterValue('规则', rule);
  }
  /**
   * 点击添加访问权按钮
   */
  clickAddDomain() {
    this.getButtonByText('添加访问权限').click();
    browser.sleep(500);
  }
  /**
   * 点击更新按钮
   */
  clickupdate() {
    this.getButtonByText('更新').click();
    browser.sleep(500);
  }
}
