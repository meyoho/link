/**
 * 部署列表的页面类
 * Created by liuwei on 2018/3/19.
 *
 */
import { browser, by } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AlaudaRadio } from '../../element_objects/alauda.radio';
import { CommonPage } from '../../utility/common.page';
import { ResourcePage } from '../commonpage/resource.common.page';
import { AdvancedAttributes } from '../component/advancedAttributes';
import { KeyValueControl } from '../component/key_value.component';
import { TableComponent } from '../component/table.component';
import { DeploymentPage } from '../computer/deployment.page';

export class ServicePage extends ResourcePage {
  navigateTo() {
    this.clickLeftNavByText('服务');
    CommonPage.waitProgressbarNotDisplay();
  }

  /**
   * 详情页的容器表格信息
   */
  get detailInfo_endpoint() {
    return new TableComponent('alk-endpoint-list');
  }

  /**
   * 更新选择器dialog 页
   */
  get resourceSelector() {
    return new KeyValueControl(
      by.xpath('//div[@class="aui-dialog-title"]'),
      by.tagName('aui-dialog-content'),
      by.tagName('aui-dialog-footer'),
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tabname: string = 'input') {
    if (text === '命名空间' || text === '类型') {
      return this.auiSelect;
    } else if (
      text === '端口配置' ||
      text === '选择器' ||
      text === '标签' ||
      text === '注解'
    ) {
      return this.getAlaudaInputTable();
    } else {
      return super.getElementByText(text);
    }
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    if (name === '命名空间' || name === '类型') {
      this.getElementByText(name).selectinfrom(name, value);
    } else if (
      name === '端口配置' ||
      name === '选择器' ||
      name === '标签' ||
      name === '注解'
    ) {
      this.getElementByText(name).addandfillinTable(name, value);
    } else {
      this.getElementByText(name).clear();
      this.getElementByText(name).sendKeys(value);
    }
  }

  /**
   * 获取表单与yaml创建的按钮
   */
  get selectButton() {
    return new AlaudaRadio('aui-radio-group aui-radio-button');
  }

  /**
   * 获取高级参数
   */
  get advancedAttributes() {
    return new AdvancedAttributes(
      '.aui-card',
      '.aui-card__header',
      '.foldable-bar',
    );
  }

  /**
   * 填写一个服务表单
   * @param namespace 命名空间
   * @param name 名称
   * @param type 类型
   * @param clusterip 集群ip 自动或手动或None
   * @param ipAddress 手动时IP地址
   * @param sessionaffinity 会话保持 None 或 ClientIP
   */
  addservice(
    name,
    type,
    clusterip = '自动',
    ipAddress = '',
    sessionaffinity = 'None',
    namespace = ServerConf.NAMESPACE,
  ) {
    this.enterValue('命名空间', namespace);
    this.enterValue('名称', name);
    this.enterValue('类型', type);
    if (clusterip !== '自动') {
      this.selectButton.getRadioInform('集群IP', clusterip);
    }
    if (clusterip === '手动') {
      this.enterValue('IP地址', ipAddress);
    }
    if (type === 'ExternalName') {
      this.enterValue('ExternalName', 'alauda.linkautotest.com');
    } else {
      this.selectButton.getRadioInform('会话保持', sessionaffinity);
      if (type === 'LoadBalancer') {
        this.enterValue('负载均衡器IP', '123.23.121.22');
      }
    }
  }
  /**
   * 添加标签alauda.test: true与注解alauda.test: true
   */
  addMore() {
    this.advancedAttributes.clickAdvanced('创建 服务');
    this.enterValue('标签', [
      'alauda.test',
      'true',
      '',
      'alauda.test',
      'true',
      '1',
    ]);
    this.enterValue('注解', [
      'alauda.test',
      'true',
      '',
      'alauda.test',
      'true',
      '1',
    ]);
  }
  /**
   * 添加prot和selector
   * @param prot port 默认不添加传入[] 不添加更改[名称，协议(TCP/UDP),服务端口，pod端口，(nodeport和LoadBalancer有)主机端口,按钮]
   * @param selector 默认不添加传入[] 不添加更改【键，值，按钮】
   */
  addPortAndSelector(prot = [], selector = []) {
    if (prot.length !== 0) {
      this.enterValue('端口配置', prot);
    }
    if (selector.length !== 0) {
      this.enterValue('选择器', selector);
    }
  }
  /**
   * 点击更新
   */
  clickUpdate() {
    this.getButtonByText('更新').click();
    return browser.sleep(500);
  }
}
