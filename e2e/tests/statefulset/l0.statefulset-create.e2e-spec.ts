/**
 * Created by liuwei on 2018/5/10.
 */

import { ServerConf } from '../../config/serverConf';
import { StatefulsetPage } from '../../page_objects/computer/statefulset.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:有状态副本集列表页创建功能', () => {
  const statefulsetPage = new StatefulsetPage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();

  beforeAll(() => {
    statefulsetPage.navigateTo();
    // 创建一个Deployment前先创建一个命名空间和一个配置文件
    this.testdata1 = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.daemonset-createprepare.e2e-spec',
    );
    // 创建1个命名空间，命名空间下面创建一个 replicasets.
    this.testdata2 = CommonMethod.generateStringYaml(
      'statefulsetcreate.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.statefulset-create.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata1);
    CommonKubectl.deleteResourceByYmal(this.testdata2.get('name'));
  });

  it('L0:AldK8S-138: 单击左导航计算／有状态副本集 -- 进入有状态副本集列表页--单击创建有状态副本集按钮--进入创建页--输入合法yaml, 单击创建按钮--验证创建成功', () => {
    statefulsetPage.getButtonByText('创建有状态副本集').click();
    statefulsetPage.clickYAMLbutton();

    // 清空yaml
    statefulsetPage.yamlCreator.clickToolbarByName('清空');

    // 写入yaml
    statefulsetPage.yamlCreator.setYamlValue(this.testdata2.get('data'));
    statefulsetPage.yamlCreator.clickButtonCreate();

    // 验证创建成功后，有成功的toast 提示
    // expect(statefulsetPage.toastsuccess.message.isPresent()).toBeTruthy();
    // expect(statefulsetPage.toastsuccess.getText()).toContain('创建成功');
    statefulsetPage.navigateTo();

    CommonPage.waitElementPresent(
      statefulsetPage.resourceTable.getCell('名称', [name]),
    );
    statefulsetPage.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });
});
