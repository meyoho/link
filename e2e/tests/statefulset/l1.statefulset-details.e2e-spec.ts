/**
 * Created by liuwei on 2018/5/10.
 */
import { browser } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { StatefulsetPage } from '../../page_objects/computer/statefulset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_statefulsets } from '../../utility/resource.type.k8s';

describe('L1:有状态副本集更新功能', () => {
  const statefulsetPage = new StatefulsetPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    // 创建一个 ReplicaSets
    this.testdata = CommonKubectl.createResource(
      'statefulSet.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l1.statefulset-details.e2e-spec',
    );
    statefulsetPage.navigateTo();
    // 选择新创建的namespace
    statefulsetPage.namespace.select(namespace);
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L1:AldK8S-139: 单击左导航计算／有状态副本集 -- 进入有状态副本集列表页--单击一个副本集名称--进入详情页-- 单击YAML tab--验证YAML 显示正确', () => {
    statefulsetPage.resourceTable.clickResourceNameByRow(rowkey);
    statefulsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    statefulsetPage.detailPage.yamlEditor
      .getYamlValue()
      .then(function(textyaml) {
        expect(CommonMethod.parseYaml(textyaml).metadata.name).toBe(name);
      });
  });

  it('L1:AldK8S-140: 单击左导航计算／有状态副本集 -- 进入有状态副本集列表页--单击一个副本集名称--进入详情页-- 单击配置管理 tab--验证配置管理 显示正确', () => {
    statefulsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
      '配置管理',
    );
    // 验证Pod 详情页【配置管理】表格部分表头显示正确
    expect(statefulsetPage.detailPage.configmapList.getHeaderText()).toEqual([
      '名称',
      '值',
    ]);
    statefulsetPage.detailPage.configmapList
      .getCell('值', rowkey)
      .then(function(elem) {
        expect(elem.getText()).toContain(name);
      });
  });

  it('L1:AldK8S-141: 单击左导航计算／有状态副本集 -- 进入有状态副本集列表页--单击一个副本集名称--进入详情页-- 单击事件 tab--验证事件 显示正确', () => {
    statefulsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');

    // 验证Pod 详情页【事件】表格部分表头显示正确
    expect(statefulsetPage.detailPage.eventList.getHeaderText()).toEqual([
      '消息',
      '来源',
      '子对象',
      '总数',
      '首次出现时间',
      '最近出现时间',
    ]);

    // 验证事件的数量大于0
    statefulsetPage.detailPage.eventList.getRowCount().then(rowcount => {
      expect(rowcount).toBeGreaterThan(0);
    });
  });

  it('L1:AldK8S-142: 单击左导航计算／有状态副本集 -- 进入有状态副本集列表页--单击一个副本集名称--进入详情页-- 单击日志 tab--验证日志 显示正确', async () => {
    statefulsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    statefulsetPage.resourceBasicInfo.logviewer
      .getToolbarButton('查找')
      .click();
    statefulsetPage.resourceBasicInfo.logviewer.waitLogDisplay();
    statefulsetPage.resourceBasicInfo.logviewer.finder_inputbox.input('hehe');
    statefulsetPage.resourceBasicInfo.logviewer.resultfinder
      .getText()
      .then(result => {
        expect(result.includes('无结果')).toBeFalsy();
      });
  });

  it('L1:AldK8S-143: 单击左导航计算／有状态副本集 -- 进入有状态副本集列表页--单击一个副本集名称--进入详情页--验证显示信息正确', () => {
    statefulsetPage.clickLeftNavByText('有状态副本集');
    statefulsetPage.resourceTable.getCell('状态', rowkey).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
    });
    statefulsetPage.resourceTable.clickResourceNameByRow(rowkey);
    const resourceJson = CommonApi.getResourceJSON(
      name,
      k8s_type_statefulsets,
      namespace,
    );

    // 验证名称显示正确
    expect(
      statefulsetPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(resourceJson.metadata.name);

    // 验证命名空间显示正确
    expect(
      statefulsetPage.resourceBasicInfo.basicInfo
        .getElementByText('命名空间')
        .getText(),
    ).toBe(resourceJson.metadata.namespace);

    // 验证标签显示正确
    const rslabes = Object.keys(resourceJson.metadata.labels);
    for (const key of rslabes) {
      expect(
        statefulsetPage.resourceBasicInfo.basicInfo
          .getElementByText('标签')
          .getText(),
      ).toContain(resourceJson.metadata.labels[key]);
    }

    // 验证注解显示正确
    const rsannotations = Object.keys(resourceJson.metadata.annotations);
    for (const key of rsannotations) {
      expect(
        statefulsetPage.resourceBasicInfo.basicInfo
          .getElementByText('注解')
          .getText(),
      ).toContain(resourceJson.metadata.annotations[key]);
    }

    // 验证镜像显示正确
    expect(
      statefulsetPage.resourceBasicInfo.basicInfo
        .getElementByText('镜像')
        .getText(),
    ).toBe(resourceJson.spec.template.spec.containers[0].image);

    // 验证选择器显示正确
    const matchLabelsKey = Object.keys(resourceJson.spec.selector.matchLabels);
    for (const key of matchLabelsKey) {
      expect(
        statefulsetPage.resourceBasicInfo.basicInfo
          .getElementByText('选择器')
          .getText(),
      ).toContain(resourceJson.spec.selector.matchLabels[key]);
    }

    // 验证状态显示正确
    expect(
      statefulsetPage.resourceBasicInfo.basicInfo
        .getElementByText('状态')
        .getText(),
    ).toBe('1 / 1');

    // // 验证创建时间正确
    // replicasetPage.resourceBasicInfo.basicInfo.getElementByText('创建时间').getText().then((ctrateTimeStr) => {
    //   expect(new Date(ctrateTimeStr).getTime()).toBe(new Date(resourceJson.metadata.creationTimestamp).getTime());
    // });

    expect(statefulsetPage.detailPage.podList.getRowCount()).toBe(1);

    statefulsetPage.detailPage.podList
      .getColumeTextByName('名称')
      .then(nameList => {
        for (const containersName of nameList) {
          expect(containersName).toContain(name);
        }
      });

    expect(statefulsetPage.detailPage.serviceList.getRowCount()).toBe(1);

    statefulsetPage.detailPage.serviceList
      .getColumeTextByName('名称')
      .then(nameList => {
        for (const containersName of nameList) {
          expect(containersName).toContain(name);
        }
      });
  });
  it('L1:AldK8S-146: 单击左导航计算／有状态副本集 -- 进入有状态副本集列表页--单击一个副本集名称--进入详情页-- 单击操作----选择扩缩容--验证更新正确', () => {
    statefulsetPage.operation.select('扩缩容');
    CommonPage.waitElementPresent(statefulsetPage.resourceScale.buttonConfirm);
    statefulsetPage.resourceScale.inputScale('2');
    statefulsetPage.resourceScale.clickConfirm();

    browser.driver
      .wait(() => {
        statefulsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
          '事件',
        );
        statefulsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
          '基本信息',
        );
        return statefulsetPage.detailPage.podList.getRowCount().then(count => {
          return count === 2;
        });
      }, 120000)
      .then(
        () => true,
        err => {
          console.warn('扩容失败: [' + err + ']');
          return false;
        },
      );

    // 验证扩缩容正确
    expect(statefulsetPage.detailPage.podList.getRowCount()).toBe(2);
  });

  it('L1:AldK8S-147: 单击左导航计算／有状态副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页-- 单击操作----选择更新删除--验证删除正确', () => {
    statefulsetPage.operation.select('删除');
    statefulsetPage.confirmDialog.clickConfirm();
    CommonPage.clickLeftNavByText('有状态副本集');
    statefulsetPage.searchBox.search(name);
    expect(statefulsetPage.resourceTable.noResult.getText()).toBe('无数据');
  });
});
