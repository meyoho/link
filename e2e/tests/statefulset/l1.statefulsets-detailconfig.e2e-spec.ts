/**
 * Created by liuwei on 2018/3/15.
 */
import { $$, browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { StatefulsetPage } from '../../page_objects/computer/statefulset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_deployments } from '../../utility/resource.type.k8s';

describe('L1:部署配置管理功能的验证', () => {
  const statefulsetPage = new StatefulsetPage();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const configmap = name;
  const namespace = ServerConf.NAMESPACE;
  beforeAll(() => {
    statefulsetPage.navigateTo();
    // 创建一个 ReplicaSets
    this.testdata = CommonKubectl.createResource(
      'statefulSet.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': name,
      },
      'l1.statefulsets-detailconfig.e2e-spec',
    );

    statefulsetPage.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-261:L1:单击左导航计算/有状态副本集 -- 进入有状态副本集列表页--单击一个副本集名称--进入详情页面--点击配置管理--点击环境变量后的蓝色笔--添加与引用环境变量--验证环境变量与引用的', () => {
    statefulsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
      '配置管理',
    );
    statefulsetPage.clickBasicPencil('环境变量', $$('div.aui-card__header'));
    statefulsetPage
      .getAlaudaInputTable()
      .fillinTablenolabel([label_newkey, label_newvalue, '0']);
    statefulsetPage.clickconfirmButton();
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    statefulsetPage.detailPage.configmapList
      .getCell('名称', [label_newkey])
      .then(elem => {
        elem.getText().then(text => {
          expect(text).toBe(label_newkey);
        });
      });
    statefulsetPage.detailPage.configmapList
      .getCell('值', [label_newvalue])
      .then(elem => {
        elem.getText().then(text => {
          expect(text).toBe(label_newvalue);
        });
      });
  });
  it('AldK8S-262:L1:单击左导航计算/有状态副本集 -- 进入有状态副本集列表页--单击一个副本集名称--进入详情页面--点击配置管理--点击配置引用后的蓝色笔--添加配引用--验证引用成功', () => {
    statefulsetPage.navigateTo();
    statefulsetPage.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
    statefulsetPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
      '配置管理',
    );

    statefulsetPage.clickBasicPencil('配置引用', $$('div.aui-card__header'));
    statefulsetPage.getmuitiSelect().select(name);
    statefulsetPage.clickconfirmButton();
    browser.sleep(1000);
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    statefulsetPage.detailPage.envtag.getTagText().then(text => {
      expect(text).toEqual([`配置字典 ${name}`]);
    });
  });
});
