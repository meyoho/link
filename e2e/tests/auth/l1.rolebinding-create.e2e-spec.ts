/**
 * Created by changdongmei on 2018/3/26.
 */
import { ServerConf } from '../../config/serverConf';
import { RoleBindingPage } from '../../page_objects/alaudaauth/rolebinding.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import {
  k8s_type_clusterrolebindings,
  k8s_type_clusterroles,
  k8s_type_namespaces,
} from '../../utility/resource.type.k8s';
describe('L1:角色绑定的增删改', () => {
  const page = new RoleBindingPage();
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();
  const displayname = CommonMethod.random_generate_testData();
  const desc = CommonMethod.random_generate_testData();
  const rolename = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'roleprepare.yaml',
      {
        '${NAME1}': name1,
        '${NAME2}': name2,
        '${NAMESPACE}': namespace,
      },
      'l1.rolebinding-create.e2e-spec',
    );
  });

  afterAll(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete ${k8s_type_clusterrolebindings} ${name2}`,
    );
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  beforeEach(() => {
    // 点击左导航的角色按钮
    page.navigateTo();
  });
  afterEach(() => {});

  it('AldK8S-86:L1:点击创建角色绑定-名称-显示名称-命名空间相关-选择命名空间-选择角色名称-对象类型用户-选择对象名称-点击创建-验证角色创建成功', () => {
    page.getButtonByText('创建角色绑定').click();
    // 创建角色
    page.createRoleBinding(name1, '命名空间相关', [
      'User',
      'test@alauda.io',
      '',
      '',
    ]);
    page.getButtonByText('创建').click();
    CommonPage.waitElementPresent(page.toastsuccess.message);
    page.toastsuccess.getText().then(text => {
      expect(text).toContain('创建成功');
    });
  });

  it('AldK8S-87:L1:点击创建角色绑定-名称-显示名称-集群相关-选择角色名称-对象类型用户-选择对象名称-点击创建-验证角色创建成功', () => {
    page.getButtonByText('创建角色绑定').click();
    // 创建角色
    page.createRoleBinding(name2, '集群相关', [
      'User',
      'test@alauda.io',
      '',
      '',
    ]);
    page.getButtonByText('创建').click();
    // 验证创建成功后的角色
    CommonPage.waitElementPresent(page.toastsuccess.message);
    page.toastsuccess.getText().then(text => {
      expect(text).toContain('创建成功');
    });
  });

  it('AldK8S-94:L1:点击列表上的操作-点击更新-点击更新按钮-验证更新成功', () => {
    page.resourceTable.clickOperationButtonByRow([name1], '更新');
    // 更新角色
    page.updateRoleBinding('test update in list', 'test update in list');
    page.getButtonByText('更新').click();
    CommonPage.waitElementPresent(page.toastsuccess.message);
    page.toastsuccess.getText().then(text => {
      expect(text).toContain('更新成功');
    });
  });
  it('AldK8S-95:L1:点击列表上的操作-点击删除-点击确定-验证删除成功', () => {
    page.resourceTable.clickOperationButtonByRow([name1], '删除');
    // 更新角色
    page.confirmDialog.clickConfirm();
    page.authTable
      .getRow([name1])
      .isPresent()
      .then(bool => {
        expect(bool).toEqual(false);
      });
  });
  it('AldK8S-92:L1:打开角色绑定详情页-点击更新-更新绑定信息-点击更新-验证更新成功', () => {
    page.resourceTable.clickResourceNameByRow([name2]);
    // 更新角色
    page.getButtonByText('更新').click();
    page.updateRoleBinding(
      'test update in basic-info',
      'test update in basic-info',
    );
    page.clickupdatebutton();
    // 验证更新后的角色内容
    CommonPage.waitElementPresent(page.toastsuccess.message);
    page.toastsuccess.getText().then(text => {
      expect(text).toContain('更新成功');
    });
  });
  it('AldK8S-93:L1:打开角色绑定详情页-点击删除-点击确定-验证角色绑定删除成功', () => {
    page.authTable.clickResourceNameByRow([name2]);
    page.getButtonByText('删除').click();
    page.confirmDialog.clickConfirm();
    page.authTable
      .getRow([name2])
      .isPresent()
      .then(bool => {
        expect(bool).toEqual(false);
      });
  });
});
