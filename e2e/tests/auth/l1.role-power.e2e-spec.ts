/**
 * Created by changdongmei on 2018/3/26.
 */

import { ServerConf } from '../../config/serverConf';
import { RolePage } from '../../page_objects/alaudaauth/role.page';
import { RolePowerPage } from '../../page_objects/alaudaauth/role.power.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import {
  k8s_type_clusterroles,
  k8s_type_namespaces,
} from '../../utility/resource.type.k8s';

describe('L1:权限规则的添加更新与删除', () => {
  const rolePage = new RolePage();
  const powerPage = new RolePowerPage();
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'roleprepare.yaml',
      {
        '${NAME1}': name1,
        '${NAME2}': name2,
        '${NAMESPACE}': namespace,
      },
      'l1.rolebinding-create.e2e-spec',
    );
  });

  afterAll(() => {
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  beforeEach(() => {
    // 点击左导航的角色按钮
    rolePage.navigateTo();
  });
  it('AldK8S-305:L1:进入角色详情页--点击一个角色权限后的删除操作--确认删除-验证删除成功', () => {
    rolePage.authTable.clickResourceNameByRow([name1]);
    rolePage.roleRuleTable.clickOperationButtonByRow(['*', '*', '*'], '删除');
    CommonPage.waitElementPresent(rolePage.confirmDialog.buttonCancel);
    rolePage.confirmDialog.clickConfirm();
    CommonPage.waitToastNotPresent();
    rolePage.roleRuleTable.getRowCount().then(num => {
      expect(num).toEqual(0);
    });
    rolePage.navigateTo();
    rolePage.authTable.clickResourceNameByRow([name2]);
    rolePage.roleRuleTable.clickOperationButtonByRow(['*', '*', '*'], '删除');
    CommonPage.waitElementPresent(rolePage.confirmDialog.buttonCancel);
    rolePage.confirmDialog.clickConfirm();
    CommonPage.waitToastNotPresent();
    rolePage.roleRuleTable.getRowCount().then(num => {
      expect(num).toEqual(0);
    });
  });
  it('AldK8S-83:L1:创建角色-添加权限为只读-验证权限添加成功', () => {
    rolePage.authTable.clickResourceNameByRow([name1]);
    rolePage.getButtonByText('添加权限规则').click();
    CommonPage.waitProgressbarNotDisplay();
    powerPage.addpower('只读', '推荐');
    powerPage.checkbox
      .allbechecked('操作')
      .getText()
      .then(text => {
        expect(text).toEqual(['get', 'list', 'proxy', 'watch']);
      });
    powerPage.getButtonByText('保存').click();
    CommonPage.waitElementPresent(
      rolePage.roleRuleTable.getRow(['get, list, proxy, watch']),
    );
    rolePage.roleRuleTable
      .getRow(['get, list, proxy, watch'])
      .getText()
      .then(text => {
        expect(text).toContain('get, list, proxy, watch');
      });
  });
  it('AldK8S-84:L1:创建角色-添加部分权限-验证权限添加成功', () => {
    rolePage.authTable.clickResourceNameByRow([name2]);
    rolePage.getButtonByText('添加权限规则').click();
    CommonPage.waitProgressbarNotDisplay();
    powerPage.addpower(
      '只读',
      '推荐',
      ['create'],
      [''],
      ['clusterrolebindings', 'clusterroles', 'secrets'],
    );
    powerPage.getButtonByText('保存').click();
    CommonPage.waitElementPresent(
      rolePage.roleRuleTable.getRow(['get, list, proxy, watch, create']),
    );
    rolePage.roleRuleTable
      .getRow(['get, list, proxy, watch, create'])
      .getText()
      .then(text => {
        expect(text).toContain('get, list, proxy, watch, create');
      });
  });

  it('AldK8S-85:L1:创建角色-添加全部的权限-验证权限添加成功', () => {
    rolePage.authTable.clickResourceNameByRow([name2]);
    rolePage.getButtonByText('添加权限规则').click();
    CommonPage.waitProgressbarNotDisplay();
    powerPage.addpower('全部', '全部');
    powerPage.getButtonByText('保存').click();
    CommonPage.waitElementPresent(rolePage.roleRuleTable.getRow(['*']));
    rolePage.roleRuleTable
      .getRow(['*'])
      .getText()
      .then(text => {
        expect(text).toContain('*');
      });
    rolePage.roleRuleTable.clickOperationButtonByRow(['*', '*', '*'], '删除');
    rolePage.confirmDialog.clickConfirm();
  });
  it('AldK8S-304:L1:进入角色详情页--点击一个角色权限后的更新操作--更新权限-验证更新成功', () => {
    rolePage.authTable.clickResourceNameByRow([name1]);
    rolePage.roleRuleTable.clickOperationButtonByRow(
      ['get, list, proxy, watch'],
      '更新',
    );
    CommonPage.waitProgressbarNotDisplay();
    powerPage.addpower('全部', '全部');
    powerPage.getButtonByText('保存').click();
    CommonPage.waitElementPresent(
      rolePage.roleRuleTable.getRow(['*', '*', '*']),
    );
    rolePage.roleRuleTable
      .getRow(['*'])
      .getText()
      .then(text => {
        expect(text).toBe('*\n*\n*');
      });
  });
});
