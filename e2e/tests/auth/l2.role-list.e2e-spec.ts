/**
 * Created by changdongmei on 2018/3/26.
 */

import { RolePage } from '../../page_objects/alaudaauth/role.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2:角色列表页面与详情页面', () => {
  const rolePage = new RolePage();
  const name = CommonMethod.random_generate_testData();

  beforeAll(() => {
    rolePage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'clusterrole.yaml',
      {
        '${NAME}': name,
      },
      'l2.role-list.e2e-spec',
    );
  });

  afterAll(() => {
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  beforeEach(() => {
    // 点击左导航的角色按钮
    rolePage.navigateTo();
  });
  it('L2:AldK8S-70:点击左导航角色-角色列表页面-验证面包屑-验证表格列名-验证搜索-验证排序功能', () => {
    // 验证面包屑
    expect(rolePage.breadcrumb.getText()).toEqual('管理员设置/角色');
    // 验证表格列名
    rolePage.authTable.getHeaderText().then(text => {
      expect(text).toEqual(['名称', '角色类型', '命名空间', '创建时间', '']);
    });
    // 搜索name，验证存在
    rolePage.searchBox.search(name);
    rolePage.authTable.getCell('名称', [name]).then(cell => {
      expect(cell.getText()).toContain(name);
    });
    // 搜索？，验证无角色
    rolePage.searchBox.search('?');
    expect(rolePage.authTable.noResult.getText()).toEqual('无角色');
    // 清空搜索框
    rolePage.navigateTo();
    // 单击命名空间列，验证升序排列
    rolePage.resourceTable.clickHeaderByName('命名空间');
    rolePage.resourceTable.verifyAscending('命名空间');
    // 单击命名空间列，验证降序排列
    rolePage.resourceTable.clickHeaderByName('命名空间');
    rolePage.resourceTable.verifyDescending('命名空间');
  });
  it('L2:AldK8S-71:点击角色名称-角色详情页面-验证详情信息', () => {
    // 进入基本信息页，并等待页面加载出来
    rolePage.authTable.clickResourceNameByRow([name]);

    // 验证基本信息页面的参数项
    rolePage.basicInfo.getAllKeyText().then(text => {
      expect(text).toEqual([
        '名称',
        '创建时间',
        '显示名称',
        '创建者',
        '角色类型',
        '命名空间',
        '描述',
      ]);
    });
    // 验证角色详情页的面包屑
    rolePage.breadcrumb.getText().then(text => {
      expect(text).toEqual(`管理员设置/角色/${name}`);
    });
    // 验证权限列表
    rolePage.roleRuleTable.getHeaderText().then(text => {
      expect(text).toEqual(['操作', '资源', 'API 组', '']);
    });
  });
});
