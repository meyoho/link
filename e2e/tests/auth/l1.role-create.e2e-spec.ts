/**
 * Created by changdongmei on 2018/3/26.
 */
import { RolePage } from '../../page_objects/alaudaauth/role.page';

import { ServerConf } from '../../config/serverConf';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import {
  k8s_type_clusterroles,
  k8s_type_namespaces,
} from '../../utility/resource.type.k8s';
describe('L1:角色的增删改', () => {
  const rolePage = new RolePage();
  const roleName = CommonMethod.random_generate_testData();
  const clusterRoleName = CommonMethod.random_generate_testData();
  const displayname = CommonMethod.random_generate_testData();
  const descripion = CommonMethod.random_generate_testData();

  beforeEach(() => {
    // 点击左导航的角色按钮
    rolePage.navigateTo();
  });
  afterEach(() => {});

  it('L1:AldK8S-72: 单击左导航 管理员设置/角色--单击创建角色按钮--角色类型选择【命名空间相关】，其它合法值，单击创建按钮--验证创建成功-验证信息正确', () => {
    // 创建角色
    rolePage.createRole(
      roleName,
      displayname,
      descripion,
      '命名空间相关',
      ServerConf.NAMESPACE,
    );

    // 验证创建成功后的角色
    rolePage.basicInfo
      .getElementByText('名称')
      .getText()
      .then(nametext => {
        expect(nametext).toEqual(roleName);
      });
    rolePage.basicInfo
      .getElementByText('显示名称')
      .getText()
      .then(nametext => {
        expect(nametext).toEqual(displayname);
      });
    rolePage.basicInfo
      .getElementByText('角色类型')
      .getText()
      .then(nametext => {
        expect(nametext).toEqual('命名空间相关');
      });
    rolePage.basicInfo
      .getElementByText('描述')
      .getText()
      .then(nametext => {
        expect(nametext).toEqual(descripion);
      });
  });
  it('L1:AldK8S-73:单击左导航 管理员设置/角色--单击创建角色按钮--角色类型选择【集群相关】，其它合法值，单击创建按钮--验证创建成功-验证信息正确', () => {
    // 创建
    rolePage.createRole(clusterRoleName, displayname, descripion);
    // 验证创建成功后的角色
    rolePage.basicInfo
      .getElementByText('名称')
      .getText()
      .then(nametext => {
        expect(nametext).toEqual(clusterRoleName);
      });
    rolePage.basicInfo
      .getElementByText('显示名称')
      .getText()
      .then(nametext => {
        expect(nametext).toEqual(displayname);
      });
    rolePage.basicInfo
      .getElementByText('角色类型')
      .getText()
      .then(nametext => {
        expect(nametext).toEqual('集群相关');
      });
    rolePage.basicInfo
      .getElementByText('描述')
      .getText()
      .then(nametext => {
        expect(nametext).toEqual(descripion);
      });
  });
  it('L1:AldK8S-74:更新角色-列表上点击更新-更新角色信息-保存-验证更新成功', () => {
    // 更新角色
    rolePage.updateRole(roleName, 'test update in list', 'test update in list');
    // 验证更新后的弹窗提示
    // expect();
    // 验证更新后的角色内容
    rolePage.authTable.clickResourceNameByRow([roleName]);
    rolePage.basicInfo
      .getElementByText('显示名称')
      .getText()
      .then(text => {
        expect(text).toEqual('test update in list');
      });
    rolePage.basicInfo
      .getElementByText('描述')
      .getText()
      .then(text => {
        expect(text).toEqual('test update in list');
      });
  });
  it('L1:AldK8S-75:删除角色-列表点击删除角色-验证提示信息-点击确认-验证删除成功', () => {
    rolePage.deleteRole(roleName);
    rolePage.searchBox.search(roleName);
    rolePage.authTable
      .getRow([roleName])
      .isPresent()
      .then(bool => {
        expect(bool).toEqual(false);
      });
    rolePage.resourceTable.noResult.getText().then(text => {
      expect(text).toBe('无角色');
    });
  });
  it('L1:AldK8S-76:更新角色-在详情页点击更新-更新信息-点击更新按钮-校验更新成功', () => {
    rolePage.authTable.clickResourceNameByRow([clusterRoleName]);
    // 更新角色
    rolePage.updateRole(
      clusterRoleName,
      'test update in basic-info',
      'test update in basic-info',
    );

    // 验证更新后的弹窗提示
    // expect();
    // 验证更新后的角色内容
    rolePage.basicInfo
      .getElementByText('显示名称')
      .getText()
      .then(text => {
        expect(text).toEqual('test update in basic-info');
      });
    rolePage.basicInfo
      .getElementByText('描述')
      .getText()
      .then(text => {
        expect(text).toEqual('test update in basic-info');
      });
  });
  it('L1:AldK8S-77:删除角色-在详情页点击删除角色-提示点击确认-验证角色被删除', () => {
    rolePage.authTable.clickResourceNameByRow([clusterRoleName]);
    rolePage.deleteRole(clusterRoleName);
    rolePage.searchBox.search(roleName);
    rolePage.authTable
      .getRow([clusterRoleName])
      .isPresent()
      .then(bool => {
        expect(bool).toEqual(false);
      });
    rolePage.resourceTable.noResult.getText().then(text => {
      expect(text).toBe('无角色');
    });
  });
});
