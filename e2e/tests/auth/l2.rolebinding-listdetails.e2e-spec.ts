/**
 * Created by changdongmei on 2018/3/26.
 */

import { ServerConf } from '../../config/serverConf';
import { RoleBindingPage } from '../../page_objects/alaudaauth/rolebinding.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2:角色绑定列表页面与详情页面', () => {
  const page = new RoleBindingPage();
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    page.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'rolebindingprepare.yaml',
      {
        '${NAME1}': name1,
        '${NAME2}': name2,
        '${NAMESPACE}': namespace,
      },
      'l2.rolebinding-listdetails.e2e-spec',
    );
  });

  afterAll(() => {
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  beforeEach(() => {
    // 点击左导航的角色按钮
    page.navigateTo();
  });
  it('AldK8S-96:L2:点击左导航-验证角色绑定面包屑-列表列名-搜索-排序', () => {
    // 验证面包屑
    expect(page.breadcrumb.getText()).toEqual('管理员设置/角色绑定');
    // 验证表格列名
    page.authTable.getHeaderText().then(text => {
      expect(text).toEqual([
        '角色绑定名称',
        '命名空间',
        '角色名称',
        '对象名称',
        '创建时间',
        '',
      ]);
    });
    // 搜索name，验证存在
    page.searchBox.search(name2);
    page.authTable.getCell('名称', [name2]).then(cell => {
      expect(cell.getText()).toContain(name2);
    });
    // 搜索？，验证无角色
    page.searchBox.search('?');
    expect(page.authTable.noResult.getText()).toEqual('无角色绑定');
    // 清空搜索框
    page.navigateTo();
    // 单击命名空间列，验证升序排列
    page.authTable.clickHeaderByName('命名空间');
    page.authTable.verifyAscending('命名空间');
    // 单击命名空间列，验证降序排列
    page.authTable.clickHeaderByName('命名空间');
    page.authTable.verifyDescending('命名空间');
  });
  it('AldK8S-295:L2：点击一个已存在的角色绑定名-验证详情页面信息', () => {
    // 进入基本信息页，并等待页面加载出来
    page.authTable.clickResourceNameByRow([name1]);

    // 验证基本信息页面的参数项
    page.basicInfo.getAllKeyText().then(text => {
      expect(text).toEqual([
        '名称',
        '创建时间',
        '显示名称',
        '创建者',
        '角色绑定类型',
        '命名空间',
        '角色名称',
        '描述',
      ]);
    });
    // 验证角色详情页的面包屑
    page.breadcrumb.getText().then(text => {
      expect(text).toEqual(`管理员设置/角色绑定/${name1}`);
    });
    // 验证权限列表
    page.authTable.getHeaderText().then(text => {
      expect(text).toEqual(['名称', '类型', '命名空间']);
    });
    page.authTable
      .getRow(['ServiceAccount'])
      .getText()
      .then(text => {
        expect(text).toBe(`test1\nServiceAccount\n${ServerConf.NAMESPACE}`);
      });
    page.authTable
      .getRow(['User'])
      .getText()
      .then(text => {
        expect(text).toBe(`test2\nUser\n-`);
      });
    page.authTable
      .getRow(['Group'])
      .getText()
      .then(text => {
        expect(text).toBe(`test3\nGroup\n-`);
      });
  });
});
