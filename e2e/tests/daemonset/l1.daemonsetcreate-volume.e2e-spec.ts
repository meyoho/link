import { browser, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { DaemonsetPage } from '../../page_objects/computer/daemonset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:守护进程集创建功能通过UI', () => {
  const page = new DaemonsetPage();
  const addVolumePage = new AddVolumePage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const praparename = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    page.navigateTo();
    // 创建一个守护进程集前先创建一个命名空间和一个配置文件
    this.testdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': praparename,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l1.daemonsetcreate-volume.e2e-spec',
    );
    this.podcount = JSON.parse(
      CommonKubectl.execKubectlCommand(`kubectl get node -o json`),
    ).items.length;
  });
  afterEach(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete DaemonSet ${name} -n ${namespace}`,
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-282:L1: 单击左导航计算/守护进程集--进入守护进程集列表--单击创建守护进程集--添加配置文件--点击创建--验证守护进程集成功有配置文件', () => {
    page.getButtonByText('创建守护进程集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建守护进程集
    page.fillinBasical(namespace, name, 'RollingUpdate');
    page.clickAddVolume();

    CommonPage.waitElementDisplay(addVolumePage.cancelButton);

    addVolumePage.addVolume('testvolume', '配置字典', praparename);
    page.fillinVolumeMounts('testvolume', 'test/config', '/test/config');
    browser.sleep(5000);
    page.fillincontainer(praparename, '1', name);

    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面

    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证守护进程集运行情况
    CommonPage.waitElementPresent(page.resourceTable.getCell('名称', [name]));
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });
  it('AldK8S-283:L1: 点击左导航计算 / 守护进程集 -- 进入守护进程集列表 -- 点击创建守护进程集 -- 点击右上角表单 -- 挂载保密字典其他都正确 -- 验证创建成功保密字典已挂载', () => {
    page.getButtonByText('创建守护进程集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建守护进程集
    page.fillinBasical(namespace, name, 'RollingUpdate');
    page.clickAddVolume();
    addVolumePage.addVolume('testsecret', '保密字典', praparename);
    CommonPage.waitElementDisplay(addVolumePage.cancelButton);

    page.fillinVolumeMounts('testsecret', 'test/secret', '/test/secret');
    page.fillincontainer(praparename, '1', name);

    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.yamlCreator.getYamlValue().then(yaml => {
      expect(yaml).toContain('secretName: ' + praparename);
      expect(yaml).toContain('name: testsecret');
      expect(yaml).toContain('mountPath: /test/secret');
      expect(yaml).toContain('subPath: test/secret');
    });
    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 验证守护进程集运行情况
    CommonPage.waitElementPresent(page.resourceTable.getCell('名称', [name]));
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });
  it('AldK8S-284:L1: 点击左导航计算 / 守护进程集 -- 进入守护进程集列表 -- 点击创建守护进程集 -- 点击右上角表单 -- 挂载pvc其他都正确 -- 验证创建成功PVC已挂载', () => {
    page.getButtonByText('创建守护进程集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建守护进程集
    page.fillinBasical(namespace, name, 'RollingUpdate');

    page.clickAddVolume();
    CommonPage.waitElementDisplay(addVolumePage.cancelButton);
    addVolumePage.addVolume('testpvc', '持久卷声明', praparename);
    browser.sleep(100);
    page.fillinVolumeMounts('testpvc', 'test/pvc', '/test/pvc');
    page.fillincontainer(praparename, '1', name);

    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.yamlCreator.getYamlValue().then(yaml => {
      expect(yaml).toContain('claimName: ' + praparename);
      expect(yaml).toContain('name: testpvc');
      expect(yaml).toContain('mountPath: /test/pvc');
      expect(yaml).toContain('subPath: test/pvc');
    });
  });
});
