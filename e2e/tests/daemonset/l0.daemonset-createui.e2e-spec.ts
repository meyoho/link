import { browser, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { DaemonsetPage } from '../../page_objects/computer/daemonset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:UI创建守护进程', () => {
  const page = new DaemonsetPage();
  const addVolumePage = new AddVolumePage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    page.navigateTo();
    // 创建一个Deployment前先创建一个配置文件和StorageClass、pv、pvc
    this.testdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.daemonset-createui.e2e-spec',
    );
    this.podcount = JSON.parse(
      CommonKubectl.execKubectlCommand(`kubectl get node -o json`),
    ).items.length;
  });
  afterEach(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete DaemonSet ${name} -n ${namespace}`,
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-277:L0: 单击左导航计算 / 守护进程集 -- 进入守护进程列表--单击创建守护进程--点击右上角表单--输入合法的参数--点击创建--验证创建成功', () => {
    page.getButtonByText('创建守护进程集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    browser.sleep(4000);
    page.fillinBasical(namespace, name, 'RollingUpdate', '1');
    page.fillincontainer(name, '1', name);
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    page.resourceBasicInfo.basicInfo
      .getElementByText('更新策略')
      .getText()
      .then(text => {
        expect('RollingUpdate').toBe(text);
      });
    // 进入列表页面
    page.navigateTo();
    // 等待列表中的数据加载
    page.resourceTable.getCell('名称', [name]).then(elem => {
      CommonPage.waitElementPresent(elem);
    });
    // 验证Deployment运行情况
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });

  it('AldK8S-278:L1: 单击左导航计算/守护进程集 -- 进入守护进程列表 -- 单击创建守护进程 -- 填写命令echo 其他参数正确 -- 点击创建验证命令已执行', () => {
    page.getButtonByText('创建守护进程集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(namespace, name, 'OnDelete');
    page.fillincontainer(name, '1', name);
    page.fillinContainerAdvanced('', 'echo hello');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    page.resourceBasicInfo.basicInfo
      .getElementByText('更新策略')
      .getText()
      .then(text => {
        expect('OnDelete').toBe(text);
      });
    // 进入列表页面
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    page.resourceBasicInfo.logviewer.getToolbarButton('查找').click();
    page.resourceBasicInfo.logviewer.waitLogDisplay('echo hello');
    page.resourceBasicInfo.logviewer.finder_inputbox.input('echo hello');
    page.resourceBasicInfo.logviewer.resultfinder.getText().then(result => {
      expect(result.includes('无结果')).toBeFalsy();
    });
    page.navigateTo();
    // 等待列表中的数据加载
    page.resourceTable.getCell('名称', [name]).then(elem => {
      CommonPage.waitElementPresent(elem);
    });
    // 验证Deployment运行情况
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });

  it('AldK8S-279:L1: 单击左导航计算/守护进程集--进入守护进程列表--单击创建守护进程--点击右上角表单--创建两个容器输入合法的参数--点击创建--验证创建成功', () => {
    page.getButtonByText('创建守护进程集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(namespace, name, 'RollingUpdate');
    page.fillincontainer('test1', '1', name);
    page.clickAddContaineButton();
    page.fillincontainer('test2', '1', name);
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    page.navigateTo();
    // 等待列表中的数据加载
    page.resourceTable.getCell('名称', [name]).then(elem => {
      CommonPage.waitElementPresent(elem);
    });
    // 验证Deployment运行情况
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });

  it('AldK8S-280:L1: 单击左导航的计算/守护进程集--进入守护进程列表--单击创建守护进程--点击右上角表单--填写资源限制其他参数正确--点击创建--验证创建成功', () => {
    page.getButtonByText('创建守护进程集').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(namespace, name, 'RollingUpdate');
    // delete
    browser.sleep(10000);
    page.fillincontainer(name, '1', name, '1');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    // 进入列表页面
    page.navigateTo();
    // 等待列表中的数据加载
    page.resourceTable.getCell('名称', [name]).then(elem => {
      CommonPage.waitElementPresent(elem);
    });
    // 验证Deployment运行情况
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });

  it('ACP-281:L1:单击左导航计算/守护进程集--进入守护进程集列表--单击创建守护进程集--输入合法参数--点击右上角yaml--验证页面输入参数以转换成yaml-点击取消-验证未创建', () => {
    page.getButtonByText('创建守护进程集').click();
    CommonPage.waitProgressbarNotDisplay();
    page.fillinBasical(namespace, 'testUItoyaml', 'OnDelete', '1');
    page.clickAddVolume();
    addVolumePage.addVolume('testvolume', '配置字典', name);
    browser.sleep(100);
    page.fillinContainerAdvanced('testchangtest', 'echo hello');
    page.fillinVolumeMounts('testvolume', 'test/config', '/test/config');
    page.fillincontainer('testUItoyaml', '1', name);
    // 切换到YAML创建
    page.clickYAMLbutton();
    CommonPage.waitElementPresent(page.yamlCreator.toolbar);
    page.yamlCreator.getYamlValue().then(text => {
      expect(text).toContain('namespace: ' + namespace);
      expect(text).toContain('name: ' + 'testUItoyaml');
      expect(text).toContain('OnDelete');
      expect(text).toContain('testchangtest');
      expect(text).toContain('cpu: 50m');
      expect(text).toContain('memory: 50Mi');
      expect(text).toContain('mountPath: /test/config');
      expect(text).toContain('subPath: test/config');
      expect(text).toContain('test: linkautotest');
      expect(text).toContain('linkautotest: linkautotest');
      expect(text).toContain(`${ServerConf.IMAGE}`);
    });
    page.clickLeftText('守护进程集');
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('确定取消创建守护进程集吗?');
    });
    page.warningDialog.clickConfirm();
  });
});
