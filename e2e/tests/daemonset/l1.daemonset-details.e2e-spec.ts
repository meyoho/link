/**
 * Created by liuwei on 2018/5/14.
 */

import { ServerConf } from '../../config/serverConf';
import { DaemonsetPage } from '../../page_objects/computer/daemonset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_daemonsets } from '../../utility/resource.type.k8s';

describe('L1:守护进程集更新功能', () => {
  const page = new DaemonsetPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const selectorkey = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    // 创建一个 ReplicaSets
    this.testdata = CommonKubectl.createResource(
      'daemonset.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
        '${SELECTORKEY}': selectorkey,
      },
      'l1.daemonset-details.e2e-spec',
    );

    this.podcount = JSON.parse(
      CommonKubectl.execKubectlCommand(`kubectl get node -o json`),
    ).items.length;
    page.navigateTo();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L1:AldK8S-151: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击YAML tab--验证YAML 显示正确', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.detailPage.yamlEditor.getYamlValue().then(function(textyaml) {
      expect(CommonMethod.parseYaml(textyaml).metadata.name).toBe(name);
    });
  });

  it('L1:AldK8S-152: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击配置管理 tab--验证配置管理 显示正确', () => {
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');

    // 验证Pod 详情页【配置管理】表格部分表头显示正确
    expect(page.detailPage.configmapList.getHeaderText()).toEqual([
      '名称',
      '值',
    ]);

    page.detailPage.configmapList.getCell('值', rowkey).then(function(elem) {
      expect(elem.getText()).toContain(name);
    });
  });

  it('L1:AldK8S-153: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击事件 tab--验证事件 显示正确', () => {
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');

    // 验证Pod 详情页【事件】表格部分表头显示正确
    expect(page.detailPage.eventList.getHeaderText()).toEqual([
      '消息',
      '来源',
      '子对象',
      '总数',
      '首次出现时间',
      '最近出现时间',
    ]);

    // 验证事件的数量大于0
    page.detailPage.eventList.getRowCount().then(rowcount => {
      expect(rowcount).toBeGreaterThan(0);
    });
  });

  it('L1:AldK8S-154: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击日志 tab--验证日志 显示正确', async () => {
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    page.resourceBasicInfo.logviewer.getToolbarButton('查找').click();
    page.resourceBasicInfo.logviewer.waitLogDisplay();
    page.resourceBasicInfo.logviewer.finder_inputbox.input('hehe');
    page.resourceBasicInfo.logviewer.resultfinder.getText().then(result => {
      expect(result.includes('无结果')).toBeFalsy();
    });
  });

  it('L1:AldK8S-155: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页--验证显示信息正确', () => {
    page.clickLeftNavByText('守护进程集');
    page.searchBox.search(name);
    page.resourceTable.getRowCount().then(count => {
      expect(count).toEqual(1);
    });
    page.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(
        elem,
        `${this.podcount} / ${this.podcount}`,
        20000,
      ).then(waitforbool => {
        if (waitforbool) {
          expect(elem.getText()).toBe(`${this.podcount} / ${this.podcount}`);
        } else {
          elem.getText().then(text => {
            console.log(
              `deamonset运行状态为： ${text}
              //*******************///有没有可能是哪个节点上有tatints///******************//`,
            );
          });
        }
      });
    });
    page.resourceTable.clickResourceNameByRow(rowkey);
    const resourceJson = CommonApi.getResourceJSON(
      name,
      k8s_type_daemonsets,
      namespace,
    );
    // 验证名称显示正确
    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('名称').getText(),
    ).toBe(resourceJson.metadata.name);

    // 验证命名空间显示正确
    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('命名空间').getText(),
    ).toBe(resourceJson.metadata.namespace);

    // 验证标签显示正确
    const rslabes = Object.keys(resourceJson.metadata.labels);
    for (const key of rslabes) {
      expect(
        page.resourceBasicInfo.basicInfo.getElementByText('标签').getText(),
      ).toContain(resourceJson.metadata.labels[key]);
    }

    // 验证注解显示正确
    const rsannotations = Object.keys(resourceJson.metadata.annotations);
    for (const key of rsannotations) {
      expect(
        page.resourceBasicInfo.basicInfo.getElementByText('注解').getText(),
      ).toContain(resourceJson.metadata.annotations[key]);
    }

    // 验证镜像显示正确
    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('镜像').getText(),
    ).toBe(resourceJson.spec.template.spec.containers[0].image);

    // 验证选择器显示正确
    const matchLabelsKey = Object.keys(resourceJson.spec.selector.matchLabels);
    for (const key of matchLabelsKey) {
      expect(
        page.resourceBasicInfo.basicInfo.getElementByText('选择器').getText(),
      ).toContain(resourceJson.spec.selector.matchLabels[key]);
    }

    // 验证创建时间正确
    page.resourceBasicInfo.basicInfo
      .getElementByText('创建时间')
      .getText()
      .then(ctrateTimeStr => {
        expect(new Date(ctrateTimeStr).getTime()).toBe(
          new Date(resourceJson.metadata.creationTimestamp).getTime(),
        );
      });

    page.detailPage.podList.getColumeTextByName('名称').then(nameList => {
      for (const containersName of nameList) {
        expect(containersName).toContain(name);
      }
    });

    expect(page.detailPage.serviceList.getRowCount()).toBe(1);

    page.detailPage.serviceList.getColumeTextByName('名称').then(nameList => {
      for (const containersName of nameList) {
        expect(containersName).toContain(name);
      }
    });
  });
  it('L1:AldK8S-158: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页-- 单击操作----选择更新删除--验证删除正确', () => {
    page.operation.select('删除');
    page.confirmDialog.clickConfirm();
    CommonPage.clickLeftNavByText('守护进程集');
    page.searchBox.search(name);
    expect(page.resourceTable.noResult.getText()).toBe('无守护进程集');
  });
});
