/**
 * Created by liuwei on 2018/5/10.
 */

import { ServerConf } from '../../config/serverConf';
import { DaemonsetPage } from '../../page_objects/computer/daemonset.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:守护进程集列表页创建功能', () => {
  const page = new DaemonsetPage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const selectorkey = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    page.navigateTo();
    // 创建一个Deployment前先创建一个命名空间和一个配置文件
    this.testdataprapare = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.daemonset-createprepare.e2e-spec',
    );
    // 创建1个命名空间，命名空间下面创建一个 replicasets.
    this.testdata = CommonMethod.generateStringYaml(
      'daemonsetcreate.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
        '${SELECTORKEY}': selectorkey,
      },
      'l0.daemonset-create.e2e-spec',
    );

    this.podcount = JSON.parse(
      CommonKubectl.execKubectlCommand(`kubectl get node -o json`),
    ).items.length;
    page.navigateTo();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdataprapare);
    CommonKubectl.deleteResourceByYmal(this.testdata.get('name'));
  });

  it('L0:AldK8S-150: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击创建守护进程集按钮--进入创建页--输入合法yaml, 单击创建按钮--验证创建成功', () => {
    page.getButtonByText('创建守护进程集').click();

    page.clickYAMLbutton();
    page.yamlCreator.clickToolbarByName('清空');

    // 写入yaml
    page.yamlCreator.setYamlValue(this.testdata.get('data'));
    page.yamlCreator.clickButtonCreate();

    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();

    // expect(page.toastsuccess.getText()).toContain('创建成功');
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();
    page.navigateTo();
    CommonPage.waitElementPresent(page.resourceTable.getCell('名称', [name]));
    page.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(
        elem,
        `${this.podcount} / ${this.podcount}`,
        20000,
      ).then(waitforbool => {
        if (waitforbool) {
          expect(elem.getText()).toBe(`${this.podcount} / ${this.podcount}`);
        } else {
          elem.getText().then(text => {
            console.log(
              `deamonset运行状态为： ${text}
              //*******************///有没有可能是哪个节点上有tatints///******************//`,
            );
          });
        }
      });
    });
  });
});
