/**
 * Created by liuwei on 2018/5/14.
 */

import { ServerConf } from '../../config/serverConf';
import { DaemonsetPage } from '../../page_objects/computer/daemonset.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2:守护进程集列表页的显示，排序功能', () => {
  const daemonsetPage = new DaemonsetPage();
  const namespace = ServerConf.NAMESPACE;

  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const selectorkey = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    daemonsetPage.navigateTo();

    // 创建一个daemonset
    this.testdata = CommonKubectl.createResource(
      'daemonset.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
        '${SELECTORKEY}': selectorkey,
      },
      'l2.daemonsets-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L2:AldK8S-159: 单击左导航计算／守护进程集 -- 进入守护进程集的资源列表页--验证列表默认按创建时间降序排序，可按名称、创建时间、命名空间 排序', () => {
    // 验证面包屑显示正确
    expect(daemonsetPage.breadcrumb.getText()).toEqual('计算/守护进程集');

    // 默认按创建时间降序排列
    daemonsetPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    daemonsetPage.resourceTable.clickHeaderByName('名称');
    daemonsetPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    daemonsetPage.resourceTable.clickHeaderByName('名称');
    daemonsetPage.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    daemonsetPage.resourceTable.clickHeaderByName('命名空间');
    daemonsetPage.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    daemonsetPage.resourceTable.clickHeaderByName('命名空间');
    daemonsetPage.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    daemonsetPage.resourceTable.clickHeaderByName('创建时间');
    daemonsetPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    daemonsetPage.resourceTable.clickHeaderByName('创建时间');
    daemonsetPage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-160: 单击左导航计算／守护进程集 -- 进入守护进程集列表页--单击一个副本集名称--进入详情页--验证功能菜单信息正确', () => {
    // 检索一个不存在的数据
    daemonsetPage.searchResource(CommonMethod.random_generate_testData());
    // 验证列表为空时显示‘无守护进程集’
    expect(daemonsetPage.resourceTable.noResult.getText()).toBe('无守护进程集');
    // 清空检索框
    daemonsetPage.searchResource(name);

    // 验证表头显示正确
    daemonsetPage.resourceTable.getHeaderText().then(headerText => {
      expect(headerText).toEqual([
        '名称',
        '命名空间',
        '状态',
        '镜像',
        '已使用资源',
        '创建时间',
        '',
      ]);
    });

    // 点击条目右侧按钮，可弹出操作菜单 '更新', '删除'
    daemonsetPage.resourceTable
      .clickOperationButtonByRow(rowkey, '更新标签')
      .then(text => {
        expect(text).toEqual([
          '更新',
          '查看日志',
          '更新标签',
          '更新注解',
          '删除',
        ]);
      });

    // 关闭更新标签dialog 页面
    daemonsetPage.resourcelabel.clickCancel();

    // 单击名称进入详情页，验证详情页tab 显示正确 '基本信息', 'YAML', '配置字典', '日志', '事件'
    daemonsetPage.resourceTable.clickResourceNameByRow(rowkey);
    daemonsetPage.resourceBasicInfo.resourceInfoTab
      .getTabText()
      .then(tabText => {
        expect(tabText).toEqual([
          '基本信息',
          'YAML',
          '配置管理',
          '日志',
          '事件',
        ]);
      });
    // 验证详情页基本信息页
    daemonsetPage.resourceBasicInfo.basicInfo.getAllKeyText().then(testlist => {
      expect(CommonMethod.converNodeListTextToArray(testlist).sort()).toEqual(
        [
          '名称',
          '命名空间',
          '标签',
          '注解',
          '选择器',
          '滚动更新策略',
          '镜像',
          '更新策略',
          '历史版本限制',
          '创建时间',
          '状态',
        ].sort(),
      );
    });
  });
});
