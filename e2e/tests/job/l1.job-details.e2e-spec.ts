/**
 * Created by liuwei on 2018/3/15.
 */
import { by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { JobPage } from '../../page_objects/computer/job.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L1:任务详情功能', () => {
  const jobPage = new JobPage();
  const name = CommonMethod.random_generate_testData();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    jobPage.navigateTo();
    // 准备deployment的测试数据
    this.testdata = CommonKubectl.createResource(
      'job.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${parallelismnum}': '1',
        '${completionsnum}': '1',
        '${backoffLimitnum}': '2',
      },
      'l1.job-details.e2e-spec',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-241:L1:单击计算/任务--进入任务列表--点击一个已存在的任务名称--进入详情页面--验证基本信息yaml配置管理日志事件', () => {
    jobPage.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
    jobPage.resourceTable.clickResourceNameByRow(rowkey);
    jobPage.resourceBasicInfo.basicInfo.getAllKeyText().then(text => {
      expect(text).toEqual([
        '名称',
        '命名空间',
        '标签',
        '注解',
        '镜像',
        '选择器',
        '超时时间',
        '重试次数',
        '预计完成数',
        '最大并行数',
        '状态',
        '创建者',
        '创建时间',
      ]);
    });
    jobPage.resourceBasicInfo.resourceInfoTab.getTabText().then(text => {
      expect(text).toEqual(['基本信息', 'YAML', '配置管理', '日志', '事件']);
    });
    jobPage.resourceTable.getRowCount().then(num => {
      expect(num).toEqual(1);
    });
  });
  it('AldK8S-275:L1:单击计算/任务进入列表-点击任务名称进入详情页面--点击标签后的蓝色笔-添加标签--验证标签添加成功', () => {
    jobPage.resourceBasicInfo.basicButtion.getElementByText('标签').click();
    jobPage.resourcelabel.newValue(
      'autotestupdatelabel',
      'autotestupdatelabel',
    );
    jobPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    jobPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel: autotestupdatelabel');
      });
  });
  it('AldK8S-276:L1:单击计算/任务进入列表-点击任务名称进入详情页面--点击注解后的蓝色笔--添加注解--验证注解添加成功', () => {
    jobPage.resourceBasicInfo.basicButtion.getElementByText('注解').click();
    jobPage.resourcelabel.newValue('autotestupdate', 'autotestupdate');
    jobPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    jobPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate: autotestupdate');
      });
  });
  it('AldK8S-240:L1: 单击计算/任务--进入任务列表--点击一个已存在任务的操作按钮--点击删除-点击确认--验证任务被删除', () => {
    jobPage.operation.select('删除');
    jobPage.confirmDialog.clickConfirm();
    CommonPage.waitElementPresent(jobPage.resourceTable);
    CommonPage.waitElementNotPresent(jobPage.toastsuccess.message);
    jobPage.navigateTo();

    jobPage.searchBox.search(name);
    expect(jobPage.resourceTable.noResult.getText()).toBe('无任务');
  });
});
