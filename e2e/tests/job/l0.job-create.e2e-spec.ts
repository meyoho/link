/**
 * Created by liuwei on 2018/3/15.
 */
import { browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { JobPage } from '../../page_objects/computer/job.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:任务创建功能', () => {
  const jobPage = new JobPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    jobPage.navigateTo();
    // 准备deployment的测试数据
    this.testdata = CommonMethod.generateStringYaml(
      'job.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${parallelismnum}': '1',
        '${completionsnum}': '1',
        '${backoffLimitnum}': '2',
      },
      'l0.job-create.e2e-spec',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata.get('name'));
  });
  it('AldK8S-234:L0:单击计算/任务--进入任务列表--点击创建任务--输入正确的yaml--点击创建--验证任务创建成功', () => {
    jobPage.getButtonByText('创建任务').click();
    jobPage.yamlCreator.waiteditorspinnerNotdisplay();
    jobPage.yamlCreator.setYamlValue(this.testdata.get('data'));
    jobPage.yamlCreator.clickButtonCreate();
    // 跳转回列表
    CommonPage.waitElementPresent(
      jobPage.resourceTable.getCell('名称', [name]),
    );
    jobPage.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
    jobPage.resourceBasicInfo.basicInfo
      .getElementByText('名称')
      .getText()
      .then(text => {
        expect(text).toBe(name);
      });
    jobPage.resourceBasicInfo.basicInfo
      .getElementByText('重试次数')
      .getText()
      .then(text => {
        expect(text).toBe('2');
      });
    jobPage.resourceBasicInfo.basicInfo
      .getElementByText('预计完成数')
      .getText()
      .then(text => {
        expect(text).toBe('1');
      });
    jobPage.resourceBasicInfo.basicInfo
      .getElementByText('最大并行数')
      .getText()
      .then(text => {
        expect(text).toBe('1');
      });
  });
});
