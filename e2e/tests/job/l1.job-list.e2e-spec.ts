/**
 * Created by liuwei on 2018/3/15.
 */
import { browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { JobPage } from '../../page_objects/computer/job.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L1:任务列表功能', () => {
  const jobPage = new JobPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    jobPage.navigateTo();
    // 准备deployment的测试数据
    this.testdata = CommonKubectl.createResource(
      'job.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${parallelismnum}': '1',
        '${completionsnum}': '1',
        '${backoffLimitnum}': '2',
      },
      'l1.job-list.e2e-spec',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-270:L1:单击计算/任务--进入任务列表--验证列表排序搜索', () => {
    jobPage.resourceTable.getHeaderText().then(text => {
      expect(text).toEqual([
        '名称',
        '命名空间',
        '状态',
        '镜像',
        '已使用资源',
        '创建时间',
        '',
      ]);
    });
    // 默认按创建时间降序排列
    jobPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    jobPage.resourceTable.clickHeaderByName('名称');
    jobPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    jobPage.resourceTable.clickHeaderByName('名称');
    jobPage.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    jobPage.resourceTable.clickHeaderByName('命名空间');
    jobPage.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    jobPage.resourceTable.clickHeaderByName('命名空间');
    jobPage.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    jobPage.resourceTable.clickHeaderByName('创建时间');
    jobPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    jobPage.resourceTable.clickHeaderByName('创建时间');
    jobPage.resourceTable.verifyAscending('创建时间');

    jobPage.searchBox.searchBox.search(name);
    CommonPage.waitProgressbarNotDisplay();
    expect(jobPage.resourceTable.getRowCount()).toBe(1);
  });
  it('AldK8S-271:L1:单击计算/任务--进入任务列表--点击任务列表上的操作--点击更新标签--更新标签--验证标签更新成功', () => {
    jobPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    jobPage.resourcelabel.newValue('autotestupdate', 'autotestupdatelabel');
    jobPage.resourcelabel.clickUpdate();
    jobPage.resourceTable.clickResourceNameByRow(rowkey);

    jobPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    jobPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel');
      });
  });
  it('AldK8S-272:L1:单击计算/任务--进入任务列表--点击任务列表上的操作--点击更新注解--更新注解--验证注解更新成功', () => {
    jobPage.navigateTo();

    jobPage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    jobPage.resourcelabel.newValue('autotestupdate', 'autotestupdate');
    jobPage.resourcelabel.clickUpdate();
    jobPage.resourceTable.clickResourceNameByRow(rowkey);
    browser.sleep(100);

    jobPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    jobPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate');
      });
  });
  it('AldK8S-273:L1:单击计算/任务--进入任务列表--点击任务列表上的操作--点击查看日志--验证日志', () => {
    jobPage.navigateTo();
    jobPage.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
    jobPage.resourceTable.clickOperationButtonByRow(rowkey, '查看日志');
    jobPage.resourceBasicInfo.logviewer.logTextarea.getText().then(text => {
      expect(text).toEqual('9\n8\n7\n6\n5\n4\n3\n2\n1');
    });
  });
  it('AldK8S-274:L1:单击计算 / 任务 -- 进入任务列表 --点击任务列表上的操作 -- 点击删除--确认删除--验证删除成功', () => {
    jobPage.navigateTo();
    jobPage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    jobPage.confirmDialog.clickConfirm();
    CommonPage.waitElementNotPresent(jobPage.toastsuccess.message);
    jobPage.searchBox.searchBox.search(name);
    CommonPage.waitElementPresent(jobPage.resourceTable.noResult);
    jobPage.resourceTable.noResult.getText().then(text => {
      expect(text).toBe('无任务');
    });
  });
});
