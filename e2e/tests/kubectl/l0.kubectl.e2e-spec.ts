/**
 * Created by liuwei on 2018/3/15.
 */

import { browser } from 'protractor';

import { KubectlPage } from '../../page_objects/component/kubectl.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:kubectl功能', () => {
  const page = new KubectlPage();
  afterAll(() => {
    // 回到默认窗口
    page.switchWindow(0);
  });
  it('AldK8S-372:L0:点击kubectl-验证kubect连接成功', () => {
    // 验证日志显示样式正确
    page.kubectlButton.click();
    page.switchWindow(1);
    CommonPage.waitElementPresent(page.textlayer);
    page.alkbreadcrumb.getText().then(text => {
      expect(text).toBe(`kubectl`);
    });
    page.connectionstatus.then(connection => {
      expect(connection).toBeTruthy();
    });
  });
});
