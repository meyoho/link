/**
 * Created by liuwei on 2018/5/23.
 */

import { ServerConf } from '../../config/serverConf';
import { PvcPage } from '../../page_objects/storage/pvc.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2: PVC列表页的显示', () => {
  const pvcPage = new PvcPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();

  // 根据【名称】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    pvcPage.navigateTo();
    this.testdata1 = CommonKubectl.createResource(
      'pvcprepare.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
        '${NAMESPACE}': namespace,
      },
      'l0.pvc-createprepare.e2e-spec',
    );
    this.testdata2 = CommonKubectl.createResource(
      'pvc.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
        '${NAMESPACE}': namespace,
      },
      'l2.pvc-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(
      `kubectl delete pvc ${name} -n ${namespace}`,
    );
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
    CommonKubectl.execKubectlCommand(`kubectl delete storageclass ${name}`);
  });

  it('L2:AldK8S-166:单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--验证列表默认按照创建时间倒序，可按名称，创建时间排序', () => {
    // 默认按创建时间降序排列
    pvcPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    pvcPage.resourceTable.clickHeaderByName('名称');
    pvcPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    pvcPage.resourceTable.clickHeaderByName('名称');
    pvcPage.resourceTable.verifyDescending('名称');

    // 单击创建时间列，验证降序排列
    pvcPage.resourceTable.clickHeaderByName('创建时间');
    pvcPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    pvcPage.resourceTable.clickHeaderByName('创建时间');
    pvcPage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-167: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--验证列表表头，操作列功能按钮按钮显示正确', () => {
    expect(pvcPage.resourceTable.getHeaderText()).toEqual([
      '名称',
      '命名空间',
      '状态',
      '关联持久卷',
      '存储类',
      '大小',
      '创建时间',
      '',
    ]);
    pvcPage.searchBox.search(name);
    expect(
      pvcPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签'),
    ).toEqual(['更新', '更新标签', '更新注解', '删除']);
    pvcPage.resourcelabel.clickCancel();
  });

  it('L2:AldK8S-169: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--在检索框中输入存在的持久卷声明--验证检索功能正常', () => {
    pvcPage.searchBox.search(name);
    expect(pvcPage.resourceTable.getRowCount()).toBe(1);
    pvcPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a').getText()).toBe(name);
    });
    pvcPage.resourceTable.getCell('大小', rowkey).then(cell => {
      expect(cell.getText()).toBe('100m');
    });
  });

  it('L2:AldK8S-170: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--单击名称进入详情页--验证详情页信息显示正确', () => {
    pvcPage.searchBox.search(name);
    pvcPage.resourceTable.clickResourceNameByRow(rowkey);
    expect(pvcPage.resourceBasicInfo.basicInfo.getAllKeyText()).toEqual([
      '名称',
      '命名空间',
      '标签',
      '注解',
      '访问模式',
      '大小',
      '关联持久卷',
      '存储类',
      '状态',
      '创建时间',
    ]);

    expect(
      pvcPage.resourceBasicInfo.basicInfo.getElementByText('名称').getText(),
    ).toBe(name);

    expect(
      pvcPage.resourceBasicInfo.basicInfo.getElementByText('标签').getText(),
    ).toContain('alauda.test: true');

    expect(
      pvcPage.resourceBasicInfo.basicInfo
        .getElementByText('访问模式')
        .getText(),
    ).toBe('ReadWriteOnce');
    expect(
      pvcPage.resourceBasicInfo.basicInfo
        .getElementByText('命名空间')
        .getText(),
    ).toBe(namespace);
    expect(
      pvcPage.resourceBasicInfo.basicInfo
        .getElementByText('关联持久卷')
        .getText(),
    ).toBe(name);
    expect(
      pvcPage.resourceBasicInfo.basicInfo.getElementByText('大小').getText(),
    ).toBe('100m');
    expect(
      pvcPage.resourceBasicInfo.basicInfo.getElementByText('存储类').getText(),
    ).toBe(name);
  });
});
