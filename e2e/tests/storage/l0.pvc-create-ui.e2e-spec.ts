/**
 * Created by liuwei on 2018/5/24.
 */

import { ServerConf } from '../../config/serverConf';
import { PvcPage } from '../../page_objects/storage/pvc.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:UI创建PVC功能', () => {
  const pvcPage = new PvcPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'pvcprepare.yaml',
      {
        '${NAME}': name,
        '${LABEL}': name,
        '${NAMESPACE}': namespace,
      },
      'l0.pvc-createprepare.e2e-spec',
    );
  });
  beforeEach(() => {
    pvcPage.navigateTo();
  });
  afterEach(() => {
    pvcPage.resourceTable.clickOperationButtonByRow([name], '删除');
    pvcPage.confirmDialog.clickConfirm();
  });
  afterAll(() => {
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
    CommonKubectl.execKubectlCommand(`kubectl delete storageclass ${name}`);
  });

  it('AldK8S-296:L0: 进入pvc列表页面--点击创建pvc--选择命名空间--填写名称--选择访问模式【读写】--选择存储卷模式【Filesystem】--选择存储类--大小50m--点击创建--验证存储卷创建成功', () => {
    pvcPage.getButtonByText('创建持久卷声明').click();
    pvcPage.createPVC(name, '读写', 'Filesystem');
    pvcPage.getButtonByText('创建').click();
    CommonPage.waitProgressbarNotDisplay();
    pvcPage.navigateTo();
    // 验证pv 创建成功
    pvcPage.searchBox.search(name);
    expect(pvcPage.resourceTable.getRowCount()).toBe(1);
    pvcPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=persistentvolumeclaim]').getText()).toBe(name);
    });
  });
  it('AldK8S-338:L1:进入pvc列表页面--点击创建pvc--选择命名空间--填写名称--选择访问模式【只读】--选择存储卷模式【Filesystem】--选择存储类--大小50m--点击创建--验证存储卷创建成功', () => {
    pvcPage.getButtonByText('创建持久卷声明').click();
    pvcPage.createPVC(name, '只读', 'Filesystem');
    pvcPage.getButtonByText('创建').click();
    CommonPage.waitProgressbarNotDisplay();
    pvcPage.navigateTo();
    // 验证pv 创建成功
    pvcPage.searchBox.search(name);
    expect(pvcPage.resourceTable.getRowCount()).toBe(1);
    pvcPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=persistentvolumeclaim]').getText()).toBe(name);
    });
  });
  it('AldK8S-339:L1:进入pvc列表页面--点击创建pvc--选择命名空间--填写名称--选择访问模式【共享】--选择存储卷模式【Block】--选择存储类--大小50m--点击创建--验证存储卷创建成功', () => {
    pvcPage.getButtonByText('创建持久卷声明').click();
    pvcPage.createPVC(name, '共享', 'Block');
    pvcPage.getButtonByText('创建').click();
    CommonPage.waitProgressbarNotDisplay();
    pvcPage.navigateTo();
    // 验证pv 创建成功
    pvcPage.searchBox.search(name);
    expect(pvcPage.resourceTable.getRowCount()).toBe(1);
    pvcPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=persistentvolumeclaim]').getText()).toBe(name);
    });
  });
});
