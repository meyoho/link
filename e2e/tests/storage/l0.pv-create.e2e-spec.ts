/**
 * Created by liuwei on 2018/5/23.
 */

import { PvPage } from '../../page_objects/storage/pv.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:PV创建功能', () => {
  const pvPage = new PvPage();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    pvPage.navigateTo();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
  });

  it('L0:AldK8S-161: 单击左导航存储／持久卷 -- 进入持久卷的列表页--单击创建持久卷--输入yaml 样例，修改名称，单击创建按钮--验证创建成功', () => {
    pvPage.getButtonByText('创建持久卷').click();
    pvPage.yamlCreator.labelwrite.click();
    pvPage.yamlCreator.waiteditorspinnerNotdisplay();
    pvPage.yamlCreator.getYamlValue().then((yaml: string) => {
      pvPage.yamlCreator.clickToolbarByName('清空');
      const yamlNew = yaml.replace(
        CommonMethod.parseYaml(yaml).metadata.name,
        name,
      );
      pvPage.yamlCreator.setYamlValue(yamlNew);
      pvPage.yamlCreator.clickButtonCreate();
      // expect(pvPage.toastsuccess.getText()).toContain('创建成功');
    });

    // 验证pv 创建成功
    pvPage.searchBox.search(name);
    expect(pvPage.resourceTable.getRowCount()).toBe(1);
    pvPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=persistentvolume]').getText()).toBe(name);
    });
  });
});
