/**
 * Created by liuwei on 2018/5/26.
 */
import { StorageClassPage } from '../../page_objects/storage/storageclass.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2: StorageClass列表页的显示', () => {
  const storgageClassPage = new StorageClassPage();
  const name = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    storgageClassPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'storageClass.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
      },
      'l2.storageclass-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete storageclass ${name}`);
  });

  it('L2:AldK8S-185:单击左导航存储／存储类 -- 进入存储类列表页--验证列表默认按照创建时间倒序，可按名称，创建时间排序', () => {
    // 默认按创建时间降序排列
    storgageClassPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    storgageClassPage.resourceTable.clickHeaderByName('名称');
    storgageClassPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    storgageClassPage.resourceTable.clickHeaderByName('名称');
    storgageClassPage.resourceTable.verifyDescending('名称');

    // 单击创建时间列，验证降序排列
    storgageClassPage.resourceTable.clickHeaderByName('创建时间');
    storgageClassPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    storgageClassPage.resourceTable.clickHeaderByName('创建时间');
    storgageClassPage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-186: 单击左导航存储／存储类 -- 进入存储类列表页--验证列表表头，操作列功能按钮按钮显示正确', () => {
    expect(storgageClassPage.resourceTable.getHeaderText()).toEqual([
      '名称',
      'Provisioner',
      '类型',
      '回收策略',
      '是否默认',
      '创建时间',
      '',
    ]);
    storgageClassPage.searchBox.search(name);
    expect(
      storgageClassPage.resourceTable.clickOperationButtonByRow(
        rowkey,
        '更新标签',
      ),
    ).toEqual(['设为默认', '更新', '更新标签', '更新注解', '删除']);
    storgageClassPage.resourcelabel.clickCancel();
  });

  it('L2:AldK8S-187: 单击左导航存储／存储类 -- 进入存储类列表页--在检索框中输入存在的存储类--验证检索功能正常', () => {
    storgageClassPage.searchBox.search(name);
    expect(storgageClassPage.resourceTable.getRowCount()).toBe(1);
    storgageClassPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a').getText()).toBe(name);
    });
    storgageClassPage.resourceTable.getCell('类型', rowkey).then(cell => {
      expect(cell.getText()).toBe('pd-ssd');
    });
  });

  it('L2:AldK8S-188: 单击左导航存储／存储类 -- 进入存储类列表页--单击名称进入详情页--验证详情页信息显示正确', () => {
    storgageClassPage.searchBox.search(name);
    storgageClassPage.resourceTable.clickResourceNameByRow(rowkey);
    expect(
      storgageClassPage.resourceBasicInfo.basicInfo.getAllKeyText(),
    ).toEqual([
      '名称',
      '是否默认',
      '标签',
      '注解',
      'Provisioner',
      '类型',
      '回收策略',
      '创建时间',
    ]);

    expect(
      storgageClassPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(name);

    expect(
      storgageClassPage.resourceBasicInfo.basicInfo
        .getElementByText('标签')
        .getText(),
    ).toContain(label);
    expect(
      storgageClassPage.resourceBasicInfo.basicInfo
        .getElementByText('注解')
        .getText(),
    ).toBe(`alaudaAnnotations: true`);

    expect(
      storgageClassPage.resourceBasicInfo.basicInfo
        .getElementByText('是否默认')
        .getText(),
    ).toBe('否');
    expect(
      storgageClassPage.resourceBasicInfo.basicInfo
        .getElementByText('Provisioner')
        .getText(),
    ).toBe('kubernetes.io/gce-pd');
    expect(
      storgageClassPage.resourceBasicInfo.basicInfo
        .getElementByText('类型')
        .getText(),
    ).toBe('pd-ssd');
  });
});
