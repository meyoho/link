/**
 * Created by liuwei on 2018/5/23.
 */

import { StorageClassPage } from '../../page_objects/storage/storageclass.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L1:StorageClass更新功能', () => {
  const storageClassPage = new StorageClassPage();
  const label = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    storageClassPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'storageClass.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
      },
      'l1.storageclass-update.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete StorageClass ${name}`);
  });

  it('L1:AldK8S-172: 单击左导航存储／存储类 -- 进入存储类列表页--选择一个存储类，单击操作列，选择更新标签--验证更新标签功能正确', () => {
    storageClassPage.resourceTable.clickOperationButtonByRow(
      rowkey,
      '更新标签',
    );
    storageClassPage.resourcelabel.newValue(label_newkey, label_newvalue);
    storageClassPage.resourcelabel.clickUpdate();
    expect(storageClassPage.toastsuccess.getText()).toContain('更新成功');
    expect(
      storageClassPage.resourceTable.getCellLabel(rowkey, '名称'),
    ).toContain(`${label_newkey}: ${label_newvalue}`);
  });

  it('L1:AldK8S-173: 单击左导航存储／存储类 -- 进入存储类列表页--选择一个存储类，单击操作列，选择更新注解--验证更新注解功能正确', () => {
    storageClassPage.resourceTable.clickOperationButtonByRow(
      rowkey,
      '更新注解',
    );
    storageClassPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    storageClassPage.resourceAnnotations.clickUpdate();
    expect(storageClassPage.toastsuccess.getText()).toContain('更新成功');
  });

  it('L1:AldK8S-174: 单击左导航存储／存储类 -- 进入存储类列表页--选择一个存储类，单击操作列，选择更新--验证更新功能正确', () => {
    storageClassPage.resourceTable.clickResourceNameByRow(rowkey);
    storageClassPage.operation.select('更新');
    storageClassPage.yamlCreator.getYamlValue().then(yaml => {
      storageClassPage.yamlCreator.setYamlValue(
        String(yaml).replace('storage: 5Gi', 'storage: 6Gi'),
      );
      storageClassPage.yamlCreator.clickButtonCreate();
      expect(storageClassPage.toastsuccess.getText()).toContain('更新成功');
    });
  });

  it('L1:AldK8S-175: 单击左导航存储／存储类 -- 进入存储类列表页--选择一个存储类，单击操作列，选择删除--验证删除功能正确', () => {
    storageClassPage.operation.select('删除');
    expect(storageClassPage.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 storageclass ${name} 吗?`,
    );
    storageClassPage.confirmDialog.clickConfirm();
    storageClassPage.searchBox.search(name);
    expect(storageClassPage.resourceTable.noResult.getText()).toBe('无存储类');
  });
});
