/**
 * Created by liuwei on 2018/5/26.
 */

import { StorageClassPage } from '../../page_objects/storage/storageclass.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:PV创建功能', () => {
  const storageClassPage = new StorageClassPage();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    storageClassPage.navigateTo();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete StorageClass ${name}`);
  });

  it('L0:AldK8S-171: 单击左导航存储／存储类 -- 进入存储类的列表页--单击创建存储类--输入yaml 样例，修改名称，单击创建按钮--验证创建成功', () => {
    storageClassPage.getButtonByText('创建存储类').click();
    storageClassPage.yamlCreator.waiteditorspinnerNotdisplay();
    storageClassPage.yamlCreator.labelwrite.click();
    storageClassPage.yamlCreator.getYamlValue().then((yaml: string) => {
      storageClassPage.yamlCreator.clickToolbarByName('清空');
      const yamlNew = yaml.replace(
        CommonMethod.parseYaml(yaml).metadata.name,
        name,
      );
      storageClassPage.yamlCreator.setYamlValue(yamlNew);
      storageClassPage.yamlCreator.clickButtonCreate();
      // expect(storageClassPage.toastsuccess.getText()).toContain('创建成功');
    });

    // 验证storageClass 创建成功
    storageClassPage.searchBox.search(name);
    expect(storageClassPage.resourceTable.getRowCount()).toBe(1);
    storageClassPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=storageclass]').getText()).toBe(name);
    });
  });
});
