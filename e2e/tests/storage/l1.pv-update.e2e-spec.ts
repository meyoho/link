/**
 * Created by liuwei on 2018/5/23.
 */

import { PvPage } from '../../page_objects/storage/pv.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L1:PV更新功能', () => {
  const pvPage = new PvPage();
  const annotations = CommonMethod.random_generate_testData();
  const accessmode = 'ReadWriteMany';
  const label = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    pvPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'pv.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
        '${ANNOTATIONS}': annotations,
        '${ACCESSMODE}': accessmode,
      },
      'L1.pvUpdate.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
  });

  it('L1:AldK8S-162: 单击左导航存储／持久卷 -- 进入持久卷列表页--选择一个持久卷，单击操作列，选择更新标签--验证更新标签功能正确', () => {
    pvPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    pvPage.resourcelabel.newValue(label_newkey, label_newvalue);
    pvPage.resourcelabel.clickUpdate();
    expect(pvPage.toastsuccess.getText()).toContain('更新成功');
    expect(pvPage.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `${label_newkey}: ${label_newvalue}`,
    );
  });

  it('L1:AldK8S-163: 单击左导航存储／持久卷 -- 进入持久卷列表页--选择一个持久卷，单击操作列，选择更新注解--验证更新注解功能正确', () => {
    pvPage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    pvPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    pvPage.resourceAnnotations.clickUpdate();
    expect(pvPage.toastsuccess.getText()).toContain('更新成功');
  });

  it('L1:AldK8S-164: 单击左导航存储／持久卷 -- 进入持久卷列表页--选择一个持久卷，单击操作列，选择更新--验证更新功能正确', () => {
    pvPage.resourceTable.clickResourceNameByRow(rowkey);
    pvPage.operation.select('更新');
    pvPage.yamlCreator.getYamlValue().then(yaml => {
      pvPage.yamlCreator.setYamlValue(
        String(yaml).replace('storage: 5Gi', 'storage: 6Gi'),
      );
      pvPage.yamlCreator.clickButtonCreate();
      expect(pvPage.toastsuccess.getText()).toContain('更新成功');
    });
  });

  it('L1:AldK8S-165: 单击左导航存储／持久卷 -- 进入持久卷列表页--选择一个持久卷，单击操作列，选择删除--验证删除功能正确', () => {
    pvPage.operation.select('删除');
    expect(pvPage.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 持久卷 ${name} 吗?`,
    );
    pvPage.confirmDialog.clickConfirm();
    pvPage.searchBox.search(name);
    expect(pvPage.resourceTable.noResult.getText()).toBe('无持久卷');
  });
});
