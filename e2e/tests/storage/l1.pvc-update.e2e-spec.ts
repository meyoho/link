/**
 * Created by liuwei on 2018/5/23.
 */
import { ServerConf } from '../../config/serverConf';
import { PvcPage } from '../../page_objects/storage/pvc.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:PVC更新功能', () => {
  const pvcPage = new PvcPage();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    this.testdata1 = CommonKubectl.createResource(
      'pvcprepare.yaml',
      {
        '${NAME}': name,
        '${LABEL}': name,
        '${NAMESPACE}': namespace,
      },
      'l0.pvc-createprepare.e2e-spec',
    );
    this.testdata2 = CommonKubectl.createResource(
      'pvc.yaml',
      {
        '${NAME}': name,
        '${LABEL}': name,
        '${NAMESPACE}': namespace,
      },
      'l1.pvc-update.e2e-spec',
    );
  });
  beforeEach(() => {
    pvcPage.navigateTo();
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(
      `kubectl delete pvc ${name} -n ${namespace}`,
    );
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
    CommonKubectl.execKubectlCommand(`kubectl delete storageclass ${name}`);
  });

  it('L1:AldK8S-181: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--选择一个持久卷声明，单击操作列，选择更新标签--验证更新标签功能正确', () => {
    pvcPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    pvcPage.resourcelabel.newValue(label_newkey, label_newvalue);
    pvcPage.resourcelabel.clickUpdate();
    expect(pvcPage.toastsuccess.getText()).toContain('更新成功');
    expect(pvcPage.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `${label_newkey}: ${label_newvalue}`,
    );
  });

  it('L1:AldK8S-182: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--选择一个持久卷声明，单击操作列，选择更新注解--验证更新注解功能正确', () => {
    pvcPage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    pvcPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    pvcPage.resourceAnnotations.clickUpdate();
    expect(pvcPage.toastsuccess.getText()).toContain('更新成功');
  });
  it('L1:AldK8S-184: 单击左导航存储／持久卷声明 -- 进入持久卷声明列表页--选择一个持久卷声明，单击操作列，选择删除--验证删除功能正确', () => {
    pvcPage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    expect(pvcPage.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 持久卷声明 ${name} 吗?`,
    );
    pvcPage.confirmDialog.clickConfirm();
    pvcPage.navigateTo();
    CommonPage.waitProgressbarNotDisplay();
    pvcPage.searchBox.search(name);
    expect(pvcPage.resourceTable.noResult.getText()).toBe('无持久卷声明');
  });
});
