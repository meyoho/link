/**
 * Created by liuwei on 2018/5/23.
 */

import { PvPage } from '../../page_objects/storage/pv.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2: PV列表页的显示', () => {
  const pvPage = new PvPage();
  const name = CommonMethod.random_generate_testData();
  const annotations = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const accessmode = 'ReadWriteOnce';

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    pvPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'pv.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
        '${ANNOTATIONS}': annotations,
        '${ACCESSMODE}': accessmode,
      },
      'l2.pv-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
  });

  it('L2:AldK8S-166:单击左导航存储／持久卷 -- 进入持久卷列表页--验证列表默认按照创建时间倒序，可按名称，创建时间排序', () => {
    // 默认按创建时间降序排列
    pvPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    pvPage.resourceTable.clickHeaderByName('名称');
    pvPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    pvPage.resourceTable.clickHeaderByName('名称');
    pvPage.resourceTable.verifyDescending('名称');

    // 单击创建时间列，验证降序排列
    pvPage.resourceTable.clickHeaderByName('创建时间');
    pvPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    pvPage.resourceTable.clickHeaderByName('创建时间');
    pvPage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-167: 单击左导航存储／持久卷 -- 进入持久卷列表页--验证列表表头，操作列功能按钮按钮显示正确', () => {
    expect(pvPage.resourceTable.getHeaderText()).toEqual([
      '名称',
      '状态',
      '关联持久卷声明',
      '存储类',
      '大小',
      '创建时间',
      '',
    ]);
    pvPage.searchBox.search(name);
    expect(
      pvPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签'),
    ).toEqual(['更新', '更新标签', '更新注解', '删除']);
    pvPage.resourcelabel.clickCancel();
  });

  it('L2:AldK8S-169: 单击左导航存储／持久卷 -- 进入持久卷列表页--在检索框中输入存在的持久卷--验证检索功能正常', () => {
    pvPage.searchBox.search(name);
    expect(pvPage.resourceTable.getRowCount()).toBe(1);
    pvPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a').getText()).toBe(name);
    });
    pvPage.resourceTable.getCell('大小', rowkey).then(cell => {
      expect(cell.getText()).toBe('100m');
    });
  });

  it('L2:AldK8S-170: 单击左导航存储／持久卷 -- 进入持久卷列表页--单击名称进入详情页--验证详情页信息显示正确', () => {
    pvPage.searchBox.search(name);
    pvPage.resourceTable.clickResourceNameByRow(rowkey);
    expect(pvPage.resourceBasicInfo.basicInfo.getAllKeyText()).toEqual([
      '名称',
      '状态',
      '标签',
      '注解',
      '访问模式',
      '回收策略',
      '关联持久卷声明',
      '大小',
      '存储类',
      '状态',
      '创建时间',
    ]);

    expect(
      pvPage.resourceBasicInfo.basicInfo.getElementByText('名称').getText(),
    ).toBe(name);

    expect(
      pvPage.resourceBasicInfo.basicInfo.getElementByText('标签').getText(),
    ).toContain(label);
    expect(
      pvPage.resourceBasicInfo.basicInfo.getElementByText('注解').getText(),
    ).toBe(`pvannotations: ${annotations}`);

    expect(
      pvPage.resourceBasicInfo.basicInfo.getElementByText('访问模式').getText(),
    ).toBe('ReadWriteOnce');
    expect(
      pvPage.resourceBasicInfo.basicInfo.getElementByText('回收策略').getText(),
    ).toBe('Delete');
    expect(
      pvPage.resourceBasicInfo.basicInfo
        .getElementByText('关联持久卷声明')
        .getText(),
    ).toBe('-');
    expect(
      pvPage.resourceBasicInfo.basicInfo.getElementByText('大小').getText(),
    ).toBe('100m');
    expect(
      pvPage.resourceBasicInfo.basicInfo.getElementByText('存储类').getText(),
    ).toBe('slow');
  });
});
