/**
 * Created by liuwei on 2018/9/11.
 */
import { browser } from 'protractor';

import { CommonKubectl } from '../../utility/common.kubectl';

describe('ACP Stability Test, monitor alauda basic components', () => {
  beforeAll(() => {});

  afterAll(() => {
    browser.sleep(10000);
  });

  it('监控 alauda-dashboard-link 是否处于running 状态', async done => {
    const message = CommonKubectl.isRunningState('alauda-dashboard-dashboard');
    CommonKubectl.sendMessage(message, done);
    expect('true' in message).toBeTruthy();
  });

  it('监控 metrics-server 是否处于running 状态', async done => {
    const message = CommonKubectl.isRunningState('metrics-server');
    CommonKubectl.sendMessage(message, done);
    expect('true' in message).toBeTruthy();
  });
});
