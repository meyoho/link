/**
 * Created by liuwei on 2018/3/10.
 */

import { ServerConf } from '../../config/serverConf';
import { OtherResourcePage } from '../../page_objects/otherResource/other.resource.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:其他资源的更新删除功能', () => {
  const page = new OtherResourcePage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const label_old = CommonMethod.random_generate_testData();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotationsl_newkey = 'newkey';
  const annotationsl_newvalue = CommonMethod.random_generate_testData();

  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    page.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'service-account.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l1.resource-update.e2e-spec',
    );
    page.namespace.select(namespace);
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
    page.namespace.select('所有命名空间');
  });

  it('L1:Ald-4: 进入资源的详情页---单击操作按钮---选择中更新--修改YAML-单击更新按钮更新成功', () => {
    // 找到资源所在行，单击操作列，
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新');

    // 获取 yaml值，修改label值，单击更新按钮，更新资源
    page.detailPage.yamlEditor.getYamlValue().then(function(ymalvalue) {
      page.detailPage.yamlEditor.setYamlValue(
        String(ymalvalue).replace(label, label_old),
      );
      page.detailPage.yamlEditor.clickButtonCreate();
      CommonPage.waitElementNotPresent(page.detailPage.yamlEditor.buttonCreate);
    });
    // 更新成功后，页面停留在yaml 编辑器的页面，需要单击左导航进入到列表页面
    CommonPage.clickLeftNavByText('命名空间相关资源');
    // 验证资源的label值等于修改的值
    expect(page.resourceTable.getCellLabel(rowkey)).toEqual([
      'name: ' + label_old,
    ]);
  });

  it('L1:Ald-6: 进入资源的详情页---单击操作按钮---选择更新标签--修改标签-单击保存按钮保存成功', () => {
    CommonPage.clickLeftNavByText('命名空间相关资源');

    // 找到资源所在行，单击操作列，
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');

    // // 修改已有标签
    // page.resourcelabel.setValue(label_old, label_new);

    // 新增一个标签
    page.resourcelabel.newValue(label_newkey, label_newvalue);

    // 单击更新按钮
    page.resourcelabel.clickUpdate();

    // 验证更新成功后，自动关闭更新标签页面
    expect(page.resourcelabel.buttonUpdate.isPresent()).toBeFalsy();

    // 更新成功后,单击左导航进入到列表页面
    CommonPage.clickLeftNavByText('命名空间相关资源');

    // 验证新增的标签成功显示在资源名称的下边
    page.resourceTable.getCellLabel(rowkey).then(function(text) {
      // 验证资源的label值等于修改的值
      // 注： 页面上只显示了第一个标签，所以只验证了第一
      expect(text[0]).toBe(label_newkey + ': ' + label_newvalue);
    });
  });

  it('L1:Ald-5: 进入资源的详情页---单击操作按钮---选择中更新注释--修改注释-单击保存 按钮保存成功', () => {
    // 找到资源所在行，单击操作列，
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');

    // 新增一个标签
    page.resourceAnnotations.newValue(
      annotationsl_newkey,
      annotationsl_newvalue,
    );

    // 单击更新按钮
    page.resourceAnnotations.clickUpdate();

    // 验证更新成功后，自动关闭更新标签页面
    expect(page.resourceAnnotations.buttonUpdate.isPresent()).toBeFalsy();

    // 更新成功后,单击左导航进入到列表页面
    CommonPage.clickLeftNavByText('命名空间相关资源');

    // 找到资源所在行，单击名称进入详情页，
    page.resourceTable.clickResourceNameByRow(rowkey);

    // 验证新增annotations 在详情页中
    page.detailPage.yamlEditor.getYamlValue().then(function(text) {
      // console.log(secret.metadata.annotations.newkey)
      const secret = CommonMethod.parseYaml(text);
      expect(secret.metadata.annotations.newkey).toBe(annotationsl_newvalue);
    });
  });

  it('L1:Ald-7:  进入资源的详情页---单击操作按钮---选择删除--确认框中单击确定按钮删除成功', () => {
    CommonPage.clickLeftNavByText('命名空间相关资源');
    // 找到资源所在行，单击操作列，
    page.resourceTable.clickOperationButtonByRow(rowkey, '删除');

    // 单击删除按钮后，会弹出一个弹出确认框
    page.confirmDialog.clickConfirm();

    // 选择namespace1
    CommonPage.clickLeftNavByText('命名空间相关资源');
    // 检索已经删除的资源
    page.searchBox.search(name);
    page.waitCreateSuccess(
      page.resourceTable.noResult,
      () => {
        CommonPage.clickLeftNavByText('副本集');
        CommonPage.clickLeftNavByText('命名空间相关资源');
        page.searchBox.search(name);
      },
      '无资源',
    );
    expect(page.resourceTable.noResult.getText()).toBe('无资源');
  });
});
