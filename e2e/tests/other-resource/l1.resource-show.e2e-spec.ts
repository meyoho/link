/**
 * Created by liuwei on 2018/3/4.
 */

import { by, element } from 'protractor';

import { OtherResourcePage } from '../../page_objects/otherResource/other.resource.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:其他资源的显示功能', () => {
  const page = new OtherResourcePage();
  const name_1 = CommonMethod.random_generate_testData();
  const namespace_1 = CommonMethod.random_generate_testData();

  const name_2 = CommonMethod.random_generate_testData();
  const namespace_2 = CommonMethod.random_generate_testData();

  const alaudaUserName = CommonMethod.random_generate_testData();

  const label = CommonMethod.random_generate_testData();

  beforeAll(() => {
    page.navigateTo();
    // 通过YAML 编辑器创建一个alauda-auth
    this.testdata1 = CommonKubectl.createResource(
      'namespace.yaml',
      {
        '${NAME}': namespace_1,
        '${LABEL}': label,
      },
      'l1.resource-show.e2e-spectest1',
    );
    this.testdata2 = CommonKubectl.createResource(
      'namespace.yaml',
      {
        '${NAME}': namespace_2,
        '${LABEL}': label,
      },
      'l1.resource-show.e2e-spectest1',
    );
    this.testdata3 = CommonKubectl.createResource(
      'service-account.yaml',
      {
        '${NAME}': name_1,
        '${NAMESPACE}': namespace_1,
        '${LABEL}': label,
      },
      'l1.resource-show.e2e-spectest1',
    );
    this.testdata4 = CommonKubectl.createResource(
      'service-account.yaml',
      {
        '${NAME}': name_2,
        '${NAMESPACE}': namespace_2,
        '${LABEL}': label,
      },
      'l1.resource-show.e2e-spectest2',
    );
    this.testdata5 = CommonKubectl.createResource(
      'alauda-user.yaml',
      {
        '${NAME}': alaudaUserName,
        '${LABEL}': label,
      },
      'l1.resource-show.e2e-spectest3',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata1);
    CommonKubectl.deleteResourceByYmal(this.testdata2);
    CommonKubectl.deleteResourceByYmal(this.testdata3);
    CommonKubectl.deleteResourceByYmal(this.testdata4);
    CommonKubectl.deleteResourceByYmal(this.testdata5);
    page.navigateTo();
    page.namespace.select('所有命名空间');
  });

  it('L1:Ald-1: 单击左导航命名空间相关资源---选择namespace1---验证仅展示当前命名空间下资源集合', () => {
    // 选择namespace1
    page.namespace.select(namespace_1);

    // 验证使用namespace1 创建的资源, 会显示出来
    page.resourceTable.getCell('名称', [name_1]).then(function(cell) {
      expect(cell.element(by.css('.aui-table__cell a')).getText()).toBe(name_1);
    });

    // 选择namespace2
    page.namespace.select(namespace_2);

    // 验证使用namespace2 创建的资源, 会显示出来
    page.resourceTable.getCell('名称', [name_2]).then(function(cell) {
      expect(cell.element(by.css('.aui-table__cell a')).getText()).toBe(name_2);
    });

    // 验证表头显示正确
    page.resourceTable.getHeaderText().then(function(text) {
      expect(text).toEqual(['名称', '类型', '版本', '创建时间', '']);
    });

    // 验证面包屑显示正确
    expect(page.breadcrumb.getText()).toEqual('其他资源/命名空间相关资源');
  });

  it('L1:Ald-2: 单击左导航集群相关资源---验证列表表头，列表仅展示当前集群下与集群相关资源集合', () => {
    // 单击左导航【集群相关资源】
    CommonPage.clickLeftNavByText('集群相关资源');
    CommonPage.waitProgressbarNotDisplay();
    element(by.css('mat-progress-bar[class*=alk-invisible]'))
      .isPresent()
      .then(ispresent => {
        if (ispresent) {
          console.log('----------等待progress-bar消失-成功-----------------');
        } else {
          console.log('----------等待progress-bar加载-失败-----------------');
        }
      });
    // 验证新创建的namespace 会显示在表格中
    page.resourceTable.getCell('名称', [alaudaUserName]).then(function(cell) {
      expect(cell.element(by.css('.aui-table__cell a')).getText()).toBe(
        alaudaUserName,
      );
    });

    // 验证表头显示正确
    page.resourceTable.getHeaderText().then(function(text) {
      expect(text).toEqual(['名称', '类型', '版本', '创建时间', '']);
    });
  });

  it('L1:Ald-19: 单击左导航集群相关资源---在检索框中输入资源名称检索---按资源名称可以查询出结果', () => {
    page.searchBox.search(alaudaUserName);

    // 验证新创建的namespace 会显示在表格中
    page.resourceTable.getCell('名称', [alaudaUserName]).then(function(cell) {
      expect(cell.element(by.css('.aui-table__cell a')).getText()).toBe(
        alaudaUserName,
      );
    });
    expect(page.resourceTable.getRowCount()).toBe(1);
    // 验证面包屑显示正确
    expect(page.breadcrumb.getText()).toEqual('其他资源/集群相关资源');
  });
});
