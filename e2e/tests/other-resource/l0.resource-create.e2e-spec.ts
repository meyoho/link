/**
 * Created by liuwei on 2018/3/9.
 */

import { by } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { OtherResourcePage } from '../../page_objects/otherResource/other.resource.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:其他资源的创建功能', () => {
  const page = new OtherResourcePage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();

  beforeAll(() => {
    page.navigateTo();
    CommonPage.waitElementPresent(page.resourceCreate.button);
    CommonPage.clickLeftNavByText('命名空间相关资源');
    // 通过YAML 编辑器创建一个rolebinding
    this.testdata = CommonMethod.generateStringYaml(
      'service-account.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.resource-create.e2e-spec',
    );
  });

  afterAll(() => {
    CommonKubectl.deleteResourceByYmal(this.testdata.get('name'));
  });

  it('L0:Ald-12: 单击左导航命名空间相关资源---单击创建资源按钮----输入正确格式的YAML---单击创建按钮-资源创建成功', () => {
    page.resourceCreate.click();
    page.yamlCreate.click();
    CommonPage.waitElementPresent(page.yamlCreator.buttonCreate);
    page.yamlCreator.waiteditorspinnerNotdisplay();
    page.yamlCreator.setYamlValue(this.testdata.get('data'));
    page.yamlCreator.clickButtonCreate();
    page.confirmDialog.clickConfirm();
    // expect(page.toastsuccess.getText()).toContain('创建成功');

    // 单击创建按钮后， 创建的diaglog 页面关闭
    expect(page.yamlCreator.buttonCreate.isPresent()).toBeFalsy();

    // 选择该命名空间
    page.namespace.select(namespace);

    // 验证新添加的资源存在
    page.resourceTable.getCell('名称', [name]).then(function(cell) {
      expect(cell.element(by.css('.aui-table__cell a')).getText()).toBe(name);
    });
  });
});
