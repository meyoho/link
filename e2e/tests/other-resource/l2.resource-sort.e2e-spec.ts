/**
 * 验证其他资源列表显示的排序规则
 * Created by liuwei on 2018/3/1.
 *
 */

import { OtherResourcePage } from '../../page_objects/otherResource/other.resource.page';
import { CommonPage } from '../../utility/common.page';

describe('L2:其它资源的排序功能', () => {
  const page = new OtherResourcePage();

  const name = '名称';
  const kind = '类型';
  const createTime = '创建时间';

  beforeAll(() => {
    page.navigateTo();
    page.namespace.select('default');
    CommonPage.waitElementNotDisplay(page.resourceTable.progressbar);
  });

  it('L2:Ald-21: 单击左导航命名空间相关资源---单击资源的列表名称列表头--表格生序展示', () => {
    // 默认按【创建时间】降序排序

    page.resourceTable.verifyDescending(createTime);

    // 单击名称表头，按名称列升序
    page.resourceTable.clickHeaderByName(name);
    page.resourceTable.verifyAscending(name);

    // 再次单击名称表头，按名称列降序
    page.resourceTable.clickHeaderByName(name);
    page.resourceTable.verifyDescending(name);

    // 单击类型表头，按类型列升序
    page.resourceTable.clickHeaderByName(kind);
    page.resourceTable.verifyAscending(kind);

    // 再次单击类型表头，按类型列降序
    page.resourceTable.clickHeaderByName(kind);
    page.resourceTable.verifyDescending(kind);

    // 单击【创建时间】表头降序
    page.resourceTable.clickHeaderByName(createTime);
    page.resourceTable.verifyDescending(createTime);

    // 再次单击【创建时间】表头升序
    page.resourceTable.clickHeaderByName(createTime);
    page.resourceTable.verifyAscending(createTime);
  });
});
