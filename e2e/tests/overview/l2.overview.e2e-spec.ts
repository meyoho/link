import { RolePage } from '../../page_objects/alaudaauth/role.page';
import { OverviewPage } from '../../page_objects/overview/overview.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:概览页面', () => {
  const page = new OverviewPage();
  let eventname;
  beforeEach(() => {
    page.navigateTo();
  });

  it('AldK8S-340:L2:概览页面-事件-点击事件-验证跳转正确若资源已删除提示‘无资源’', () => {
    page
      .getchartCard('事件')
      .geteventNum()
      .then(num => {
        console.log(num);
        expect(num).toBeLessThanOrEqual(10);
      });
    page
      .getchartCard('事件')
      .firstEvent.getText()
      .then(text => {
        eventname = text;
      });
    page.getchartCard('事件').firstEvent.click();
    CommonPage.waitProgressbarNotDisplay();
    CommonPage.waitElementDisplay(page.toastsuccess.message, 1000);
    page.toastsuccess.message.isPresent().then(isPresent => {
      if (!isPresent) {
        console.log(eventname.substring(0, eventname.indexOf(' ')));
        page.breadcrumb.getText().then(crumbname => {
          expect(crumbname).toContain(
            eventname.substring(0, eventname.indexOf(' ')),
          );
        });
      }
    });
  });

  it('AldK8S-341:L2:概览-点击命名空间-验证实际命名空间个数与实际一致', () => {
    page
      .getchartCard('命名空间')
      .namespaceNum.getText()
      .then(num => {
        page.getchartCard('命名空间').namespaceNum.click();
        CommonPage.waitElementTextChangeTo(page.breadcrumb, '集群/命名空间');
        CommonPage.waitProgressbarNotDisplay();
        page.resourceTable.totalNum.then(innernum => {
          expect(num).toBe(innernum);
        });
      });
  });

  it('AldK8S-342:L2:概览-主机节点个数与状态-点击状态条-验证跳转主机列表页面并且状态与概览页面显示一致', () => {
    page
      .getchartCard('主机节点')
      .statusHint.getText()
      .then(text => {
        page.getchartCard('主机节点').statusContainer.click();
        CommonPage.waitProgressbarNotDisplay();
        CommonPage.waitProgressbarNotDisplay();
        page.resourceTable.totalNum.then(innernum => {
          expect(
            text.substring(text.indexOf('共') + 1, text.indexOf('运')),
          ).toContain(innernum);
        });
      });
  });

  it('AldK8S-343:L2:概览-容器组-点击容器组的状态条-验证跳转容器组列表与状态', () => {
    page
      .getchartCard('容器组')
      .statusHint.getText()
      .then(text => {
        page.getchartCard('容器组').statusContainer.click();
        CommonPage.waitProgressbarNotDisplay();
        CommonPage.waitProgressbarNotDisplay();
        page.resourceTable.totalNum.then(innernum => {
          expect(
            text.substring(text.indexOf('共') + 1, text.indexOf('完')),
          ).toContain(innernum);
        });
      });
  });

  it('AldK8S-344:L2:概览-部署-点击部署状态条-验证跳转到部署列表与部署状态', () => {
    page
      .getchartCard('部署')
      .statusHint.getText()
      .then(text => {
        page.getchartCard('部署').statusContainer.click();
        CommonPage.waitProgressbarNotDisplay();
        CommonPage.waitProgressbarNotDisplay();
        page.resourceTable.totalNum.then(innernum => {
          expect(
            text.substring(text.indexOf('共') + 1, text.indexOf('运')),
          ).toContain(innernum);
        });
      });
  });

  it('AldK8S-345:L2:概览-有状态副本集-点击状态条-验证跳转有状态副本集和状态', () => {
    page
      .getchartCard('有状态副本集')
      .statusHint.getText()
      .then(text => {
        page.getchartCard('有状态副本集').statusContainer.click();
        CommonPage.waitProgressbarNotDisplay();
        CommonPage.waitProgressbarNotDisplay();
        page.resourceTable.totalNum.then(innernum => {
          expect(
            text.substring(text.indexOf('共') + 1, text.indexOf('运')),
          ).toContain(innernum);
        });
      });
  });

  it('AldK8S-346:L2:概览-守护进程集-点击状态条-验证跳转守护进程集和状态', () => {
    page
      .getchartCard('守护进程集')
      .statusHint.getText()
      .then(text => {
        page.getchartCard('守护进程集').statusContainer.click();
        CommonPage.waitProgressbarNotDisplay();
        CommonPage.waitProgressbarNotDisplay();
        page.resourceTable.totalNum.then(innernum => {
          expect(
            text.substring(text.indexOf('共') + 1, text.indexOf('运')),
          ).toContain(innernum);
        });
      });
  });
});
