import { browser } from 'protractor';

import { NodePage } from '../../page_objects/cluster/node.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L1:集群节点调度', () => {
  const page = new NodePage();
  let name;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  beforeAll(() => {
    page.navigateTo();
    page.nodeNameElem.getText().then(text => {
      name = text;
    });
  });
  it('AldK8S-396:L1:进入节点列表-点击列表上一个主机后的三点操作-停止调度-验证新pod不会调度到此机器但是不影响原有pod', () => {
    page.resourceTable.clickOperationButtonByRow([name], '停止调度');
    page.confirmDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('停止调度');
    });
    page.confirmDialog.confirmMassage.getText().then(text => {
      expect(text).toBe(`确定要将节点 ${name} 设置成不可调度模式吗?`);
    });
    page.confirmDialog.clickConfirm();
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('可调度')
      .getText()
      .then(text => {
        expect('否').toBe(text);
      });

    // 还原到未操作之前
    page.clickLeftNavByText('节点');
    page.resourceTable.clickOperationButtonByRow([name], '开启调度');
    page.confirmDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('开启调度');
    });
    page.confirmDialog.confirmMassage.getText().then(text => {
      expect(text).toBe(`确定要将节点 ${name} 设置成可调度模式吗?`);
    });
    page.confirmDialog.clickConfirm();
    // 开启调度 成功
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('可调度')
      .getText()
      .then(text => {
        expect('是').toBe(text);
      });
    page.clickLeftNavByText('节点');
  });
  xit('AldK8S-397:L1:进入节点列表-点击列表上一个主机后的三点操作-点击维护模式-点击确定-验证所有的pod被移除并且新的pod不能调度到这个节点', () => {
    page.resourceTable.clickOperationButtonByRow([name], '维护模式');
    page.confirmDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('维护模式');
    });
    page.confirmDialog.confirmMassage.getText().then(text => {
      expect(text).toBe(
        `确定要将节点 ${name} 设置成维护模式吗? 设置成维护模式后，节点将同步调整为不可调度状态，且该节点上的所有容器组将会被移除。`,
      );
    });
    page.confirmDialog.clickConfirm();
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('可调度')
      .getText()
      .then(text => {
        expect('否').toBe(text);
      });
    page.clickLeftNavByText('节点');
    page.resourceTable.clickOperationButtonByRow([name], '开启调度');
    page.confirmDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('开启调度');
    });
    page.confirmDialog.confirmMassage.getText().then(text => {
      expect(text).toBe(`确定要将节点 ${name} 设置成可调度模式吗?`);
    });
    page.confirmDialog.clickConfirm();
    // 开启调度 成功
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('可调度')
      .getText()
      .then(text => {
        expect('是').toBe(text);
      });
  });
});
