import { $$, browser } from 'protractor';

import { NodePage } from '../../page_objects/cluster/node.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L1:集群节点Taints', () => {
  const page = new NodePage();
  const taintsname = CommonMethod.random_generate_testData();
  let name;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  beforeAll(() => {
    page.navigateTo();
    page.nodeNameElem.getText().then(text => {
      name = text;
    });
  });
  it('AldK8S-348:L1:单击左导航集群/节点--点击一个节点名称-点击Taints后的蓝笔-更新Taints-验证Taints生效', () => {
    page.resourceTable.clickOperationButtonByRow([name], '更新 Taint');
    page
      .getAlaudaInputTable()
      .fillinTablenolabel([`${taintsname}1`, '', 'NoSchedule', '0']);
    page.getButtonByText('更新').click();
    browser.sleep(5000);
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('Taints')
      .getText()
      .then(function(labelText) {
        expect(labelText).toBe(`${taintsname}1=NoSchedule`);
      });
    page.clickBasicPencil('Taints');
    page.getAlaudaInputTable().clickDelete();
    browser.sleep(200);
    page.getButtonByText('更新').click();
    page.resourceBasicInfo.basicInfo
      .getElementByText('Taints')
      .getText()
      .then(function(labelText) {
        expect(labelText).toBe(`-`);
      });
  });
});
