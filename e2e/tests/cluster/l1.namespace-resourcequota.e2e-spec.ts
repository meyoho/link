import { browser } from 'protractor';

import { NamespacePage } from '../../page_objects/cluster/namespace.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:命名空间资源配额', () => {
  const namespacePage = new NamespacePage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    namespacePage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'namespace.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
      },
      'l1.namespace-resourcequota.e2e-spec',
    );
  });

  afterAll(() => {
    // 清除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespaces ${name}`);
  });
  it('AldK8S-208:L1:单击集群/命名空间--进入命名空间列表--点击列表已存在命名空间添加资源配额--填写表单--点击创建--验证命名空间资源配额创建成功', () => {
    namespacePage.searchBox.search(name);
    // 创建资源
    namespacePage.resourceTable.clickOperationButtonByRow(
      rowkey,
      '添加资源配额',
    );
    namespacePage.addresourcequota(name1, '1', '1', '1');
    namespacePage.clickaddButton();

    CommonPage.waitElementNotPresent(namespacePage.resourceQuotas.noResult);
    CommonPage.waitElementPresent(
      namespacePage.resourceQuotas.getRow(['持久卷声明']),
    );

    namespacePage.resourceQuotas.getRowCount().then(function(rowCount) {
      expect(rowCount).toEqual(7);
    });
    namespacePage.resourceQuotas.getColumeCount().then(function(columeCount) {
      expect(columeCount).toEqual(3);
    });
    browser.sleep(10000);
    namespacePage.resourceQuotas
      .getRow(['存储 / 请求值'])
      .getText()
      .then(text => {
        expect(text).toBe('存储 / 请求值\n1\n0');
      });
    namespacePage.resourceQuotas
      .getRow(['持久卷声明'])
      .getText()
      .then(text => {
        expect(text).toBe('持久卷声明\n1\n0');
      });
    namespacePage.resourceQuotas
      .getRow(['容器组'])
      .getText()
      .then(text => {
        expect(text).toBe('容器组\n1\n0');
      });
  });
  it('AldK8S-208:L1:单击集群/命名空间--进入命名空间列表--点击列表已存在命名空间名称--点击右上操作添加资源配额--填写表单--点击创建--验证命名空间资源配额创建成功', () => {
    namespacePage.navigateTo();
    namespacePage.resourceTable.clickResourceNameByRow(rowkey);
    namespacePage.operation.select('添加资源配额');
    namespacePage.addresourcequota(name2, '2', '2', '2', ['', ''], ['', '']);

    namespacePage.clickaddButton();
    // 选择刚创建的资源限额名称
    namespacePage.headerselect.selectinfrom('资源配额', name2);
    CommonPage.waitElementPresent(
      namespacePage.resourceQuotas.getRow(['持久卷声明']),
    );
    namespacePage.resourceQuotas.getRowCount().then(function(rowCount) {
      expect(rowCount).toEqual(3);
    });
    namespacePage.resourceQuotas
      .getRow(['存储 / 请求值'])
      .getText()
      .then(text => {
        expect(text).toBe('存储 / 请求值\n2\n0');
      });
    namespacePage.resourceQuotas
      .getRow(['持久卷声明'])
      .getText()
      .then(text => {
        expect(text).toBe('持久卷声明\n2\n0');
      });
    namespacePage.resourceQuotas
      .getRow(['容器组'])
      .getText()
      .then(text => {
        expect(text).toBe('容器组\n2\n0');
      });
    namespacePage.headerselect.selectinfrom('资源配额', '有效资源配额');
    CommonPage.waitElementPresent(
      namespacePage.resourceQuotasAll.getRow(['持久卷声明']),
    );
    namespacePage.resourceQuotasAll.getRowCount().then(function(rowCount) {
      expect(rowCount).toEqual(7);
    });
    namespacePage.resourceQuotasAll
      .getColumeCount()
      .then(function(columeCount) {
        expect(columeCount).toEqual(4);
      });
  });
});
