import { browser } from 'protractor';

import { NodePage } from '../../page_objects/cluster/node.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L1:集群节点详情', () => {
  const nodePage = new NodePage();
  let name;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    nodePage.navigateTo();
    nodePage.nodeNameElem.getText().then(text => {
      name = text;
    });
  });

  it('AldK8S-221:L1:单击左导航集群/节点--点击一个节点的名称--验证节点有基本信息-系统信息-使用情况-与节点下的服务', () => {
    nodePage.resourceTable.clickResourceNameByRow([name]);
    nodePage.resourceBasicInfo.basicInfo.getAllKeyText().then(text => {
      expect(text).toEqual([
        '名称',
        '可调度',
        '标签',
        '注解',
        '地址',
        '提供商 ID',
        '创建时间',
        'Taints',
      ]);
    });
    nodePage.resourceBasicInfo.basicInfo
      .getElementByText('名称')
      .getText()
      .then(text => {
        expect(text).toEqual(name);
      });
    nodePage.systemMassage.getAllKeyText().then(text => {
      expect(text).toEqual([
        '机器 ID',
        '系统 UUID',
        '启动 ID',
        '内核版本',
        '操作系统镜像',
        '容器运行时版本',
        'Kubelet 版本',
        'Kube-Proxy 版本',
        '操作系统',
        '架构',
      ]);
    });
    // nodePage.conditionTable.getRowCount().then(row => {
    //   expect(row).not.toEqual(0);
    // });
    // nodePage.podTable.getRowCount().then(row => {
    //   expect(row).not.toEqual(0);
    // });
  });
});
