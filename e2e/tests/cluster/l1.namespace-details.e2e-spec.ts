import { by } from 'protractor';

import { NamespacePage } from '../../page_objects/cluster/namespace.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:命名空间详情页', () => {
  const namespacePage = new NamespacePage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    namespacePage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'namespace.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
      },
      'l1.namespace-details.e2e-spec',
    );
  });
  beforeEach(() => {
    namespacePage.navigateTo();
  });
  afterAll(() => {
    // 清除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespaces ${name}`);
  });

  it('AldK8S-209:L1: 单击左导航集群 / 命名空间--进入命名空间列表--点击一个已存在的命名空间--验证基本信息', () => {
    namespacePage.searchBox.search(name);

    namespacePage.resourceTable.clickResourceNameByRow(rowkey);
    namespacePage.resourceBasicInfo.basicInfo.getAllKeyText().then(text => {
      expect(text).toEqual(['名称', '状态', '标签', '注解', '创建时间']);
    });
    namespacePage.resourceBasicInfo.basicInfo
      .getElementByText('名称')
      .getText()
      .then(text => {
        expect(text).toEqual(name);
      });
  });
  it('AldK8S-222:L2: 单击左导航集群/命名空间--点击一个已存在的命名空间的操作--更新标签--添加一个标签--点击更新--验证标签已添加成功', () => {
    namespacePage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    namespacePage.resourcelabel.newValue(
      'autotestupdate',
      'autotestupdatelabel',
    );
    namespacePage.resourcelabel.clickUpdate();
    namespacePage.resourceTable.clickResourceNameByRow(rowkey);

    namespacePage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    namespacePage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel');
      });

    // 验证详情页基本信息页
    expect(
      namespacePage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(name);
  });
  it('AldK8S-223:L2: 单击左导航集群 / 命名空间--点击一个已存在的命名空间的操作--更新注解--添加一个注解--点击更新--验证注解已添加成功', () => {
    namespacePage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    namespacePage.resourcelabel.newValue('autotestupdate', 'autotestupdate');
    namespacePage.resourcelabel.clickUpdate();
    namespacePage.resourceTable.clickResourceNameByRow(rowkey);

    namespacePage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    namespacePage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate');
      });

    // 验证详情页基本信息页
    expect(
      namespacePage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(name);
  });
  it('AldK8S-224:L2: 单击左导航集群 / 命名空间--点击一个已存在的命名空间的操作--删除--点击确认删除--验证命名空间已经成功删除', () => {
    namespacePage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    namespacePage.confirmDialog.clickConfirm();
    namespacePage.searchBox.search(name);
    namespacePage.resourceTable.noResult.isPresent().then(isPresent => {
      if (isPresent) {
        namespacePage.resourceTable.noResult.getText().then(text => {
          expect(text).toBe('无命名空间');
        });
      } else {
        namespacePage.resourceTable
          .getCell('状态', rowkey)
          .then(function(cell) {
            expect(cell.element(by.css('alk-namespace-status')).getText()).toBe(
              '终止中',
            );
          });
      }
    });
  });
});
