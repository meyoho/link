import { browser } from 'protractor';

import { NamespacePage } from '../../page_objects/cluster/namespace.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:命名空间资源限额', () => {
  const namespacePage = new NamespacePage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    namespacePage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'namespace.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
      },
      'l1.namespace-limitrange.e2e-spec',
    );
  });

  afterAll(() => {
    // 清除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete namespaces ${name}`);
  });
  it('AldK8S-208:L1:单击集群/命名空间--进入命名空间列表--点击列表已存在命名空间添加资源限额--填写表单--点击创建--验证命名空间资源限额创建成功', () => {
    namespacePage.searchBox.search(name);
    // 添加资源配额
    namespacePage.resourceTable.clickOperationButtonByRow(
      rowkey,
      '添加资源限额',
    );
    namespacePage.addlimitrange(name1);
    namespacePage.addlimitrangelimit('容器组', 'CPU', '10', '20', '1');
    namespacePage
      .getAlaudaInputTable()
      .getRowText(['容器组', 'CPU'])
      .then(text => {
        expect(text).toEqual('容器组 CPU 10 20 1');
      });
    namespacePage.addlimitrangelimit('容器组', '内存', '10', '20', '1');
    namespacePage
      .getAlaudaInputTable()
      .getRowText(['容器组', '内存'])
      .then(text => {
        expect(text).toEqual('容器组 内存 10 20 1');
      });
    namespacePage.addlimitrangelimit(
      '容器',
      'CPU',
      '10',
      '20',
      '1',
      '10',
      '20',
    );

    namespacePage.addlimitrangelimit(
      '容器',
      '内存',
      '10',
      '20',
      '1',
      '10',
      '20',
    );

    namespacePage.addlimitrangelimit(
      '持久卷声明',
      '存储',
      '1',
      '2',
      '1',
      '1',
      '2',
    );
    namespacePage
      .getAlaudaInputTable()
      .getRowText(['持久卷声明'])
      .then(text => {
        expect(text).toEqual('持久卷声明 存储 1 2 1 2 1');
      });
    namespacePage.clickaddButton();
    CommonPage.waitElementNotPresent(namespacePage.limitRanges.noResult);
    namespacePage.navigateTo();
    namespacePage.resourceTable.clickResourceNameByRow(rowkey);
    namespacePage.limitRanges.getRowCount().then(function(rowCount) {
      expect(rowCount).toEqual(5);
    });
    namespacePage.limitRanges.getColumeCount().then(function(columeCount) {
      expect(columeCount).toEqual(7);
    });
  });
  it('AldK8S-208:L1:单击集群/命名空间--进入命名空间列表--点击列表已存在命名空间名称--点击右上操作添加资源限额--填写表单--点击创建--验证命名空间资源限额创建成功', () => {
    namespacePage.navigateTo();
    namespacePage.resourceTable.clickResourceNameByRow(rowkey);
    namespacePage.operation.select('添加资源限额');
    namespacePage.addlimitrange(name2);
    namespacePage.addlimitrangelimit('容器组', 'CPU', '10', '20', '1');
    namespacePage.addlimitrangelimit('容器组', '内存', '10', '20', '1');
    namespacePage.clickaddButton();
    // 选择刚创建的资源限额名称
    namespacePage.toastsuccess.getText().then(text => {
      expect(text).toBe('创建成功: LimitRange ' + name2);
    });
    CommonPage.waitElementNotPresent(namespacePage.toastsuccess.message);
    namespacePage.navigateTo();
    namespacePage.resourceTable.clickResourceNameByRow(rowkey);
    namespacePage.headerselect.selectinfrom('资源限额', name2);

    namespacePage.limitRanges.getRowCount().then(function(rowCount) {
      expect(rowCount).toEqual(2);
    });
    namespacePage.headerselect.selectinfrom('资源限额', '有效资源限额');
    namespacePage.limitRanges.getRowCount().then(function(rowCount) {
      expect(rowCount).toEqual(5);
    });
  });
});
