/**
 * Created by liuwei on 2018/3/15.
 */

import { ServerConf } from '../../config/serverConf';
import { ConfigPage } from '../../page_objects/config/config.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L1:配置字典更新功能', () => {
  const configPage = new ConfigPage();
  const label = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    configPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'configmap.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
        '${NAMESPACE}': namespace,
      },
      'l1.config-update.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L1:AldK8S-121: 单击左导航配置／配置字典 -- 进入配置字典列表页--选择一个配置字段，单击操作列，选择更新标签--验证更新标签功能正确', () => {
    configPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    configPage.resourcelabel.newValue(label_newkey, label_newvalue);
    configPage.resourcelabel.clickUpdate();
    expect(configPage.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `${label_newkey}: ${label_newvalue}`,
    );
  });

  it('L1:AldK8S-122: 单击左导航配置／配置字典 -- 进入配置字典列表页--选择一个配置字段，单击操作列，选择更新注解--验证更新注解功能正确', () => {
    configPage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    configPage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    configPage.resourceAnnotations.clickUpdate();
    configPage.resourceTable.clickResourceNameByRow(rowkey);
    configPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).toContain(
          `${annotations_newkey}: ${annotations_newvalue}`,
        );
      });
  });

  it('L1:AldK8S-123: 单击左导航配置／配置字典 -- 进入配置字典列表页--选择一个配置字段，单击操作列，选择更新--验证更新功能正确', () => {
    configPage.navigateTo();
    configPage.resourceTable.clickResourceNameByRow(rowkey);
    configPage.operation.select('更新');
    configPage.clickYAMLbutton();

    configPage.yamlCreator.getYamlValue().then(yaml => {
      configPage.yamlCreator.setYamlValue(
        String(yaml).replace('port: 80', 'port: 8080'),
      );
      configPage.yamlCreator.clickButtonCreate();
      expect(configPage.toastsuccess.getText()).toBe('更新成功');
    });
  });

  it('L1:AldK8S-124: 单击左导航配置／配置字典 -- 进入配置字典列表页--选择一个配置字段，单击操作列，选择删除--验证删除功能正确', () => {
    configPage.operation.select('删除');
    expect(configPage.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 配置字典 ${name} 吗?`,
    );
    configPage.confirmDialog.clickConfirm();
    configPage.searchBox.search(name);
    expect(configPage.resourceTable.noResult.getText()).toBe('无配置字典');
  });
});
