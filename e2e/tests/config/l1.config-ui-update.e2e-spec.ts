import { browser, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { ConfigPage } from '../../page_objects/config/config.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L1:通过UI更新配置字典功能', () => {
  const page = new ConfigPage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    page.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'configmap.yaml',
      {
        '${NAME}': name,
        '${LABEL}': name,
        '${NAMESPACE}': namespace,
      },
      'l2.config-list.e2e-spec',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('AldK8S-316:L1:进入配置字典列表页面--点击已存在配置字典的更新--更新配置--验证更新配置字典成功', () => {
    page.resourceTable.clickOperationButtonByRow([name], '更新');
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.updateconfig(name, key, value);
    page.clickUpdateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('更新成功');
    page.configkey.then(text => {
      expect(text).toEqual(['cm2', `${key}`]);
    });
    page.configvalue.then(text => {
      expect(text).toEqual(['cm2', `${value}`]);
    });
  });
});
