import { browser, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { ConfigPage } from '../../page_objects/config/config.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:通过UI创建配置字典功能', () => {
  const page = new ConfigPage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    page.navigateTo();
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(
      `kubectl delete cm ${name} -n ${ServerConf.NAMESPACE}`,
    );
  });

  it('AldK8S-297:L0：进入配置字典列表--点击创建--选择命名空间--添加名称--添加配置项--点击创建--验证配置字典创建成功', () => {
    page.getButtonByText('创建配置字典').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.addconfig(name, key, value);
    page.clickCreateButton();
    // 进入列表页面
    page.navigateTo();
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    // 验证Deployment运行情况
    CommonPage.waitElementPresent(page.resourceTable.getCell('名称', [name]));
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });
});
