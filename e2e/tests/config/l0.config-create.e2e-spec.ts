/**
 * Created by liuwei on 2018/4/25.
 */

import { ServerConf } from '../../config/serverConf';
import { ConfigPage } from '../../page_objects/config/config.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:配置字典创建功能', () => {
  const configPage = new ConfigPage();
  const namespace = ServerConf.NAMESPACE;
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    configPage.navigateTo();
  });
  afterAll(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete cm ${name} -n ${namespace}`,
    );
  });

  it('L0:AldK8S-127: 单击左导航配置／配置字典 -- 进入配置字典列表页--单击创建配置字典--输入yaml 样例，修改名称，命名空间，单击创建按钮--验证创建成功', () => {
    configPage.getButtonByText('创建配置字典').click();
    configPage.clickYAMLbutton();
    configPage.yamlCreator.getYamlValue().then((yaml: string) => {
      configPage.yamlCreator.clickToolbarByName('清空');
      const yamlNew = yaml.replace(
        CommonMethod.parseYaml(yaml).metadata.name,
        name,
      );
      const yamlNew1 = yamlNew.replace(
        CommonMethod.parseYaml(yamlNew).metadata.namespace,
        namespace,
      );
      configPage.yamlCreator.setYamlValue(yamlNew1);
      configPage.yamlCreator.clickButtonCreate();
    });
    // 验证configmap 创建成功
    configPage.navigateTo();
    configPage.searchBox.search(name);
    expect(configPage.resourceTable.getRowCount()).toBe(1);
    configPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=config]').getText()).toBe(name);
    });
  });
});
