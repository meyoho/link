/**
 * Created by liuwei on 2018/3/15.
 */

import { ServerConf } from '../../config/serverConf';
import { ConfigPage } from '../../page_objects/config/config.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2: 配置字段列表页的显示', () => {
  const configPage = new ConfigPage();
  const name = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  const notExistName = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    configPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'configmap.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
        '${NAMESPACE}': namespace,
      },
      'l2.config-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L2:AldK8S-119: 单击左导航配置／配置字典 -- 进入配置字典列表页--验证列表默认按照创建时间倒序，可按名称，命名空间，创建时间排序', () => {
    // 默认按创建时间降序排列
    configPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    configPage.resourceTable.clickHeaderByName('名称');
    configPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    configPage.resourceTable.clickHeaderByName('名称');
    configPage.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    configPage.resourceTable.clickHeaderByName('命名空间');
    configPage.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    configPage.resourceTable.clickHeaderByName('命名空间');
    configPage.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    configPage.resourceTable.clickHeaderByName('创建时间');
    configPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    configPage.resourceTable.clickHeaderByName('创建时间');
    configPage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-125: 单击左导航配置／配置字典 -- 进入配置字典列表页--验证列表表头，操作列功能按钮按钮显示正确', () => {
    expect(configPage.resourceTable.getHeaderText()).toEqual([
      '名称',
      '命名空间',
      '数量',
      '创建时间',
      '',
    ]);
    configPage.searchBox.search(name);
    expect(
      configPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签'),
    ).toEqual(['更新', '更新标签', '更新注解', '删除']);
    configPage.resourcelabel.clickCancel();
  });

  it('L2:AldK8S-118: 单击左导航配置／配置字典 -- 进入配置字典列表页--检索一个不存在的配置字典--验证列表为空时，提示“无配置字典”', () => {
    configPage.searchBox.search(notExistName);
    expect(configPage.resourceTable.noResult.getText()).toBe('无配置字典');
  });

  it('L2:AldK8S-120: 单击左导航配置／配置字典 -- 进入配置字典列表页--在检索框中输入存在的配置字典--验证检索功能正常', () => {
    configPage.searchBox.search(name);
    expect(configPage.resourceTable.getRowCount()).toBe(1);
    configPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a').getText()).toBe(name);
    });
    configPage.resourceTable.getCell('数量', rowkey).then(cell => {
      expect(cell.$('a').getText()).toBe('2');
    });
  });

  it('L2:AldK8S-126: 单击左导航配置／配置字典 -- 进入配置字典列表页--单击名称进入详情页--验证详情页信息显示正确', () => {
    configPage.searchBox.search(name);
    configPage.resourceTable.clickResourceNameByRow(rowkey);
    expect(configPage.resourceBasicInfo.basicInfo.getAllKeyText()).toEqual([
      '名称',
      '命名空间',
      '标签',
      '注解',
      '创建时间',
    ]);

    expect(
      configPage.resourceBasicInfo.basicInfo.getElementByText('名称').getText(),
    ).toBe(name);
    expect(
      configPage.resourceBasicInfo.basicInfo
        .getElementByText('命名空间')
        .getText(),
    ).toBe(ServerConf.NAMESPACE);
    expect(
      configPage.resourceBasicInfo.basicInfo.getElementByText('标签').getText(),
    ).toContain(label);
    expect(
      configPage.resourceBasicInfo.basicInfo.getElementByText('注解').getText(),
    ).toBe('-');
    configPage.detailPage.dataViewer.extend_header('cm1').then(() => {
      expect(
        configPage.detailPage.dataViewer.get_dataByHeaderText('cm1').getText(),
      ).toBe('cm1');
    });

    configPage.detailPage.dataViewer.extend_header('cm2').then(() => {
      expect(
        configPage.detailPage.dataViewer.get_dataByHeaderText('cm2').getText(),
      ).toBe('cm2');
    });
  });
});
