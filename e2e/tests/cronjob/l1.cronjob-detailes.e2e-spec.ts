/**
 * Created by liuwei on 2018/3/15.
 */
import { by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { CronJobPage } from '../../page_objects/computer/cronjob.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L1:定时任务详情功能', () => {
  const cronjobPage = new CronJobPage();
  const namespace = ServerConf.NAMESPACE;
  const name = CommonMethod.random_generate_testData();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    cronjobPage.navigateTo();
    // 准备deployment的测试数据
    this.testdata = CommonKubectl.createResource(
      'cronjob.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
      },
      'l1.cronjob-detailes.e2e-spec',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-249:L2: 单击计算/定时任务 -- 点击已存在定时任务名称 -- 验证定时任务的基本信息与运行的任务列表-yaml-事件', () => {
    cronjobPage.resourceTable.clickResourceNameByRow(rowkey);
    cronjobPage.resourceBasicInfo.basicInfo.getAllKeyText().then(text => {
      expect(text).toEqual([
        '名称',
        '命名空间',
        '标签',
        '注解',
        '定时规则',
        '开始时间限制',
        '挂起',
        '并发策略',
        '镜像',
        '任务历史限制',
        '超时时间',
        '重试次数',
        '预计完成数',
        '最大并行数',
        '创建时间',
      ]);
    });
    cronjobPage.resourceBasicInfo.resourceInfoTab.getTabText().then(text => {
      expect(text).toEqual(['基本信息', 'YAML', '事件']);
    });
  });
  it('AldK8S-275:L1:单击计算/定时任务 进入列表-点击任务名称进入详情页面--点击标签后的蓝色笔-添加标签--验证标签添加成功', () => {
    cronjobPage.resourceBasicInfo.basicButtion.getElementByText('标签').click();
    cronjobPage.resourcelabel.newValue(
      'autotestupdatelabel',
      'autotestupdatelabel',
    );
    cronjobPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    cronjobPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel: autotestupdatelabel');
      });
  });
  it('AldK8S-276:L1:单击计算/定时任务 进入列表-点击任务名称进入详情页面--点击注解后的蓝色笔--添加注解--验证注解添加成功', () => {
    cronjobPage.resourceBasicInfo.basicButtion.getElementByText('注解').click();
    cronjobPage.resourcelabel.newValue('autotestupdate', 'autotestupdate');
    cronjobPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    cronjobPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate: autotestupdate');
      });
  });
  it('AldK8S-240:L1: 单击计算/定时任务--进入任务列表--点击一个已存在任务的操作按钮--点击删除-点击确认--验证任务被删除', () => {
    cronjobPage.operation.select('删除');
    cronjobPage.confirmDialog.clickConfirm();
    cronjobPage.navigateTo();
    cronjobPage.searchBox.search(name);
    expect(cronjobPage.resourceTable.noResult.getText()).toBe('无定时任务');
  });
});
