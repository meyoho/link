/**
 * Created by liuwei on 2018/3/15.
 */
import { browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { CronJobPage } from '../../page_objects/computer/cronjob.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L1:定时任务列表功能操作', () => {
  const cronjobPage = new CronJobPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    cronjobPage.navigateTo();
    // 准备deployment的测试数据
    this.testdata = CommonKubectl.createResource(
      'cronjob.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
      },
      'l1:cronjoblist.e2e-spec',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-270:L1:进入定时任务列表--验证列表排序搜索', () => {
    cronjobPage.resourceTable.getHeaderText().then(text => {
      expect(text).toEqual([
        '名称',
        '命名空间',
        '定时规则',
        '运行中',
        '最近执行时间',
        '创建时间',
        '',
      ]);
    });
    // 默认按创建时间降序排列
    cronjobPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    cronjobPage.resourceTable.clickHeaderByName('名称');
    cronjobPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    cronjobPage.resourceTable.clickHeaderByName('名称');
    cronjobPage.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    cronjobPage.resourceTable.clickHeaderByName('命名空间');
    cronjobPage.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    cronjobPage.resourceTable.clickHeaderByName('命名空间');
    cronjobPage.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    cronjobPage.resourceTable.clickHeaderByName('创建时间');
    cronjobPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    cronjobPage.resourceTable.clickHeaderByName('创建时间');
    cronjobPage.resourceTable.verifyAscending('创建时间');

    cronjobPage.searchBox.searchBox.search(name);
    CommonPage.waitProgressbarNotDisplay();
    expect(cronjobPage.resourceTable.getRowCount()).toBe(1);
  });
  it('AldK8S-271:L1:进入定时任务列表--点击任务列表上的操作--点击更新标签--更新标签--验证标签更新成功', () => {
    cronjobPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    cronjobPage.resourcelabel.newValue('autotestupdate', 'autotestupdatelabel');
    cronjobPage.resourcelabel.clickUpdate();
    cronjobPage.resourceTable.clickResourceNameByRow(rowkey);

    cronjobPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    cronjobPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel');
      });
  });
  it('AldK8S-272:L1:进入定时任务列表--点击任务列表上的操作--点击更新注解--更新注解--验证注解更新成功', () => {
    cronjobPage.navigateTo();

    cronjobPage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    cronjobPage.resourcelabel.newValue('autotestupdate', 'autotestupdate');
    cronjobPage.resourcelabel.clickUpdate();
    cronjobPage.resourceTable.clickResourceNameByRow(rowkey);
    browser.sleep(100);

    cronjobPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    cronjobPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate');
      });
  });
  it('AldK8S-274:L1:进入定时任务列表 --点击任务列表上的操作 -- 点击删除--确认删除--验证删除新成功', () => {
    cronjobPage.navigateTo();
    cronjobPage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    cronjobPage.confirmDialog.clickConfirm();
    cronjobPage.searchBox.searchBox.search(name);
    cronjobPage.resourceTable.noResult.getText().then(text => {
      expect(text).toBe('无定时任务');
    });
  });
});
