import { browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { CronJobPage } from '../../page_objects/computer/cronjob.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:定时任务创建功能', () => {
  const cronjobPage = new CronJobPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    cronjobPage.navigateTo();
    // 准备deployment的测试数据
    this.testdata = CommonMethod.generateStringYaml(
      'cronjob.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
      },
      'l0.cronjobcreate.e2e-spec',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata.get('name'));
  });
  it('AldK8S-243:L0:单击计算/定时任务--进入定时任务列表--点击创建定时任务--输入合法的yaml--点击创建--验证定时任务创建成功', () => {
    cronjobPage.getButtonByText('创建定时任务').click();
    cronjobPage.yamlCreator.waiteditorspinnerNotdisplay();
    cronjobPage.yamlCreator.setYamlValue(this.testdata.get('data')).then(() => {
      cronjobPage.yamlCreator.clickButtonCreate();
    });
    // 验证创建成功后，有成功的toast 提示
    // expect(deploymentPage.toastsuccess.message.isPresent()).toBeTruthy();
    expect(cronjobPage.toastsuccess.getText()).toContain('创建成功');
    // 跳转回列表页
    CommonPage.waitElementPresent(
      cronjobPage.resourceTable.getCell('名称', [name]),
    );
    cronjobPage.resourceTable.clickResourceNameByRow(rowkey);
    cronjobPage.alkjoblist.getHeaderText().then(text => {
      expect(text).toEqual([
        '名称',
        '状态',
        '镜像',
        '创建时间',
        '',
        '名称',
        '状态',
        '镜像',
        '创建时间',
        '',
      ]);
    });
    cronjobPage.navigateTo();
    cronjobPage.searchBox.search(name);
    cronjobPage.resourceTable.getRowCount().then(num => {
      expect(num).toEqual(1);
    });
  });
});
