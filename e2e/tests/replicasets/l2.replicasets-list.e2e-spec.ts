/**
 * Created by liuwei on 2018/3/15.
 */
import { ServerConf } from '../../config/serverConf';
import { ReplicasetPage } from '../../page_objects/computer/replicaset.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2:副本集列表页的显示，排序功能', () => {
  const replicasetPage = new ReplicasetPage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    replicasetPage.navigateTo();

    // 创建一个Deployment
    this.testdatafilename = CommonKubectl.createResource(
      'replicaset.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.replicasets-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdatafilename);
  });

  it('L2:AldK8S-98: 单击左导航计算／副本集 -- 进入副本集的资源列表页--验证列表默认按创建时间降序排序，可按名称、创建时间、命名空间 排序', () => {
    // 验证面包屑显示正确
    expect(replicasetPage.breadcrumb.getText()).toEqual('计算/副本集');

    // 默认按创建时间降序排列
    replicasetPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    replicasetPage.resourceTable.clickHeaderByName('名称');
    replicasetPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    replicasetPage.resourceTable.clickHeaderByName('名称');
    replicasetPage.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    replicasetPage.resourceTable.clickHeaderByName('命名空间');
    replicasetPage.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    replicasetPage.resourceTable.clickHeaderByName('命名空间');
    replicasetPage.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    replicasetPage.resourceTable.clickHeaderByName('创建时间');
    replicasetPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    replicasetPage.resourceTable.clickHeaderByName('创建时间');
    replicasetPage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-99: 单击左导航计算／副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页--验证功能菜单信息正确', () => {
    // 检索一个不存在的数据
    replicasetPage.searchResource(CommonMethod.random_generate_testData());
    // 验证列表为空时显示‘无副本集’
    expect(replicasetPage.resourceTable.noResult.getText()).toBe('无副本集');
    // 清空检索框
    replicasetPage.searchResource(name);

    // 验证表头显示正确
    replicasetPage.resourceTable.getHeaderText().then(headerText => {
      expect(headerText).toEqual([
        '名称',
        '命名空间',
        '状态',
        '镜像',
        '已使用资源',
        '创建时间',
        '',
      ]);
    });

    // 点击条目右侧按钮，可弹出操作菜单 '更新', '扩缩容', '查看日志', '更新标签', '更新注解', '删除'
    replicasetPage.resourceTable
      .clickOperationButtonByRow(rowkey, '更新标签')
      .then(text => {
        expect(text).toEqual([
          '更新',
          '扩缩容',
          '查看日志',
          '更新标签',
          '更新注解',
          '删除',
        ]);
      });

    // 关闭更新标签dialog 页面
    replicasetPage.resourcelabel.clickCancel();

    // 单击名称进入详情页，验证详情页tab 显示正确 '基本信息', 'YAML', '配置字典', '日志', '事件'
    replicasetPage.resourceTable.clickResourceNameByRow(rowkey);
    replicasetPage.resourceBasicInfo.resourceInfoTab
      .getTabText()
      .then(tabText => {
        expect(tabText).toEqual([
          '基本信息',
          'YAML',
          '配置管理',
          '日志',
          '事件',
        ]);
      });

    // 单击操作按钮，验证操作菜单显示正确 '更新', '扩缩容', '查看日志', '更新标签', '更新注解', '删除'
    replicasetPage.operation.select('扩缩容').then(text => {
      expect(text).toEqual(['更新', '扩缩容', '删除']);
    });

    // 关闭更新标签dialog 页面
    replicasetPage.resourcelabel.clickCancel();
    // 验证详情页基本信息页
    replicasetPage.resourceBasicInfo.basicInfo
      .getAllKeyText()
      .then(testlist => {
        expect(CommonMethod.converNodeListTextToArray(testlist).sort()).toEqual(
          [
            '名称',
            '命名空间',
            '标签',
            '注解',
            '镜像',
            '选择器',
            '状态',
            '创建者',
            '创建时间',
          ].sort(),
        );
      });
  });
});
