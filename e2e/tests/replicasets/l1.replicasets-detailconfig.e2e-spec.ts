/**
 * Created by liuwei on 2018/3/15.
 */
import { $$, browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { DaemonsetPage } from '../../page_objects/computer/daemonset.page';
import { ReplicasetPage } from '../../page_objects/computer/replicaset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_deployments } from '../../utility/resource.type.k8s';

describe('L1:副本集配置管理功能的验证', () => {
  const replicasetPage = new ReplicasetPage();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const configmap = name;
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    replicasetPage.navigateTo();
    // 创建一个 ReplicaSets

    this.testdata = CommonKubectl.createResource(
      'replicaset.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': name,
      },
      'l1.replicasets-detailconfig.e2e-spec',
    );

    replicasetPage.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-261:L1:单击左导航计算/副本集 -- 进入副本集列表页--单击一个副本集名称 --进入详情页面--点击配置管理--点击环境变量后的蓝色笔--添加与引用环境变量--验证环境变量与引用的', () => {
    replicasetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');
    replicasetPage.clickBasicPencil('环境变量', $$('div.aui-card__header'));
    replicasetPage
      .getAlaudaInputTable()
      .fillinTablenolabel([label_newkey, label_newvalue, '0']);
    replicasetPage.clickconfirmButton();
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    replicasetPage.detailPage.configmapList
      .getCell('名称', [label_newkey])
      .then(elem => {
        elem.getText().then(text => {
          expect(text).toBe(label_newkey);
        });
      });
    replicasetPage.detailPage.configmapList
      .getCell('值', [label_newvalue])
      .then(elem => {
        elem.getText().then(text => {
          expect(text).toBe(label_newvalue);
        });
      });
  });
  it('AldK8S-262:L1:单击左导航计算/副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页面--点击配置管理--点击配置引用后的蓝色笔--添加配引用--验证引用成功', () => {
    replicasetPage.navigateTo();
    replicasetPage.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
    replicasetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');

    replicasetPage.clickBasicPencil('配置引用', $$('div.aui-card__header'));
    replicasetPage.getmuitiSelect().select(name);
    replicasetPage.clickconfirmButton();
    browser.sleep(1000);
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    replicasetPage.detailPage.envtag.getTagText().then(text => {
      expect(text).toEqual([`配置字典 ${name}`]);
    });
  });
});
