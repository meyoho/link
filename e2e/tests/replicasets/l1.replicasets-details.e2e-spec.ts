/**
 * Created by liuwei on 2018/3/15.
 */
import { browser } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { ReplicasetPage } from '../../page_objects/computer/replicaset.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_replicasets } from '../../utility/resource.type.k8s';

describe('L1:副本集更新，删除功能', () => {
  const replicasetPage = new ReplicasetPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    // 创建一个 ReplicaSets
    this.testdatafilename = CommonKubectl.createResource(
      'replicaset.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l1.replicasets-details.e2e-spec',
    );

    replicasetPage.navigateTo();
    // 选择新创建的namespace
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdatafilename);
  });

  it('L1:AldK8S-54: 单击左导航计算／副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页-- 单击YAML tab--验证YAML 显示正确', () => {
    replicasetPage.resourceTable.clickResourceNameByRow(rowkey);
    replicasetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    replicasetPage.detailPage.yamlEditor
      .getYamlValue()
      .then(function(textyaml) {
        expect(CommonMethod.parseYaml(textyaml).metadata.name).toBe(name);
      });
  });

  it('L1:AldK8S-55: 单击左导航计算／副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页-- 单击配置管理 tab--验证配置管理 显示正确', () => {
    replicasetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');
    // 验证Pod 详情页【配置管理】表格部分表头显示正确
    expect(replicasetPage.detailPage.configmapList.getHeaderText()).toEqual([
      '名称',
      '值',
    ]);
    replicasetPage.detailPage.configmapList
      .getCell('值', rowkey)
      .then(function(elem) {
        expect(elem.getText()).toContain(name);
      });
  });

  it('L1:AldK8S-57: 单击左导航计算／副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页-- 单击事件 tab--验证事件 显示正确', () => {
    replicasetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');

    // 验证Pod 详情页【事件】表格部分表头显示正确
    expect(replicasetPage.detailPage.eventList.getHeaderText()).toEqual([
      '消息',
      '来源',
      '子对象',
      '总数',
      '首次出现时间',
      '最近出现时间',
    ]);

    // 验证事件的数量大于0
    replicasetPage.detailPage.eventList.getRowCount().then(rowcount => {
      expect(rowcount).toBeGreaterThan(0);
    });
  });

  it('L1:AldK8S-56: 单击左导航计算／副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页-- 单击日志 tab--验证日志 显示正确', async () => {
    replicasetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    replicasetPage.resourceBasicInfo.logviewer.getToolbarButton('查找').click();

    replicasetPage.resourceBasicInfo.logviewer.waitLogDisplay();

    replicasetPage.resourceBasicInfo.logviewer.finder_inputbox.input('hehe');
    replicasetPage.resourceBasicInfo.logviewer.resultfinder
      .getText()
      .then(result => {
        expect(result.includes('无结果')).toBeFalsy();
      });
  });

  it('L1:AldK8S-53: 单击左导航计算／副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页--验证显示信息正确', () => {
    replicasetPage.clickLeftNavByText('副本集');
    replicasetPage.resourceTable.getCell('状态', rowkey).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
    });
    replicasetPage.resourceTable.clickResourceNameByRow(rowkey);
    const resourceJson = CommonApi.getResourceJSON(
      name,
      k8s_type_replicasets,
      namespace,
    );

    // 验证名称显示正确
    expect(
      replicasetPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(resourceJson.metadata.name);

    // 验证命名空间显示正确
    expect(
      replicasetPage.resourceBasicInfo.basicInfo
        .getElementByText('命名空间')
        .getText(),
    ).toBe(resourceJson.metadata.namespace);

    // 验证标签显示正确
    const rslabes = Object.keys(resourceJson.metadata.labels);
    for (const key of rslabes) {
      expect(
        replicasetPage.resourceBasicInfo.basicInfo
          .getElementByText('标签')
          .getText(),
      ).toContain(resourceJson.metadata.labels[key]);
    }

    // 验证注解显示正确
    const rsannotations = Object.keys(resourceJson.metadata.annotations);
    for (const key of rsannotations) {
      expect(
        replicasetPage.resourceBasicInfo.basicInfo
          .getElementByText('注解')
          .getText(),
      ).toContain(resourceJson.metadata.annotations[key]);
    }

    // 验证镜像显示正确
    expect(
      replicasetPage.resourceBasicInfo.basicInfo
        .getElementByText('镜像')
        .getText(),
    ).toBe(resourceJson.spec.template.spec.containers[0].image);

    // 验证选择器显示正确
    const matchLabelsKey = Object.keys(resourceJson.spec.selector.matchLabels);
    for (const key of matchLabelsKey) {
      expect(
        replicasetPage.resourceBasicInfo.basicInfo
          .getElementByText('选择器')
          .getText(),
      ).toContain(resourceJson.spec.selector.matchLabels[key]);
    }

    // 验证状态显示正确
    expect(
      replicasetPage.resourceBasicInfo.basicInfo
        .getElementByText('状态')
        .getText(),
    ).toBe('1 / 1');

    expect(replicasetPage.detailPage.podList.getRowCount()).toBe(1);

    replicasetPage.detailPage.podList
      .getColumeTextByName('名称')
      .then(nameList => {
        for (const containersName of nameList) {
          expect(containersName).toContain(name);
        }
      });

    expect(replicasetPage.detailPage.serviceList.getRowCount()).toBe(1);

    replicasetPage.detailPage.serviceList
      .getColumeTextByName('名称')
      .then(nameList => {
        for (const containersName of nameList) {
          expect(containersName).toContain(name);
        }
      });
  });
  it('L1:AldK8S-60: 单击左导航计算／副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页-- 单击操作----选择扩缩容--验证更新正确', () => {
    replicasetPage.operation.select('扩缩容');
    CommonPage.waitElementPresent(replicasetPage.resourceScale.buttonConfirm);
    replicasetPage.resourceScale.inputScale('2');
    replicasetPage.resourceScale.clickConfirm();
    CommonPage.waitElementNotPresent(replicasetPage.toastsuccess.message);
    browser.driver
      .wait(() => {
        replicasetPage.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');
        replicasetPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
          '基本信息',
        );
        return replicasetPage.detailPage.podList.getRowCount().then(count => {
          return count === 2;
        });
      }, 120000)
      .then(
        () => true,
        err => {
          console.warn('扩容失败: [' + err + ']');
          return false;
        },
      );
    // 验证扩缩容修改正确
    expect(replicasetPage.detailPage.podList.getRowCount()).toBe(2);
  });

  it('L1:AldK8S-61: 单击左导航计算／副本集 -- 进入副本集列表页--单击一个副本集名称--进入详情页-- 单击操作----选择更新删除--验证删除正确', () => {
    replicasetPage.operation.select('删除');
    replicasetPage.confirmDialog.clickConfirm();
    CommonPage.clickLeftNavByText('副本集');
    replicasetPage.searchBox.search(name);
    expect(replicasetPage.resourceTable.noResult.getText()).toBe('无副本集');
  });
});
