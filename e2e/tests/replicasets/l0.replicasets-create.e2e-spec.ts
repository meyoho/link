/**
 * Created by liuwei on 2018/3/15.
 */

import { ServerConf } from '../../config/serverConf';
import { ReplicasetPage } from '../../page_objects/computer/replicaset.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:副本集列表页创建功能', () => {
  const replicasetPage = new ReplicasetPage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const replicasetName = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [replicasetName];

  beforeAll(() => {
    replicasetPage.navigateTo();

    // 创建1个命名空间，命名空间下面创建一个 replicasets.
    this.testdata = CommonMethod.generateStringYaml(
      'replicaset.yaml',
      {
        '${NAME}': replicasetName,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.replicasets-create.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata.get('name'));
  });

  it('L0:AldK8S-103: 单击左导航计算／副本集 -- 进入副本集列表页--单击创建副本集按钮--进入创建页--输入合法yaml, 单击创建按钮--验证创建成功', () => {
    replicasetPage.getButtonByText('创建副本集').click();
    replicasetPage.yamlCreator.waiteditorspinnerNotdisplay();
    // 默认空
    expect(replicasetPage.yamlCreator.getYamlValue()).toBe('');
    // 单击写入
    replicasetPage.yamlCreator.labelwrite.click();
    // 验证写入成功
    replicasetPage.yamlCreator
      .getYamlValue()
      .then(yaml => expect(String(yaml).length).toBeGreaterThan(0));
    // 清空yaml
    replicasetPage.yamlCreator.clickToolbarByName('清空');

    // 写入yaml
    replicasetPage.yamlCreator.setYamlValue(this.testdata.get('data'));
    replicasetPage.yamlCreator.clickButtonCreate();

    // 验证创建成功后，有成功的toast 提示
    replicasetPage.resourceTable.getCell('状态', rowkey).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });
});
