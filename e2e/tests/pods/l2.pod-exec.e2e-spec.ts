/**
 * Created by liuwei on 2018/3/15.
 */

import { ServerConf } from '../../config/serverConf';
import { PodPage } from '../../page_objects/computer/pod.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:容器组EXEC功能', () => {
  const podPage = new PodPage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    // 创建一个pod
    this.testdata = CommonKubectl.createResource(
      'pod.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.pod-exec.e2e-spec',
    );
    podPage.navigateTo();
    podPage.resourceTable.getCell('状态', rowkey).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '运行中');
    });
  });

  afterAll(() => {
    podPage.switchWindow(0);

    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-336:L2:进入一个容器的详情页面-点击exec-点击镜像名称-验证exec连接正确', () => {
    // 验证日志显示样式正确
    podPage.resourceTable.clickResourceNameByRow(rowkey);
    podPage.clickexecButton('nginx');
    podPage.switchWindow(1);
    CommonPage.waitElementPresent(podPage.exec.textlayer);
    podPage.exec.alkbreadcrumb.getText().then(text => {
      expect(text).toBe(`${name}\n/\nnginx`);
    });
    podPage.exec.connectionstatus.then(connection => {
      expect(connection).toBeTruthy();
    });
  });
});
