/**
 * Created by liuwei on 2018/3/15.
 */
import { browser } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { PodPage } from '../../page_objects/computer/pod.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:容器组的日志功能', () => {
  const podPage = new PodPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    // 创建一个 ReplicaSets
    this.testdatafilename = CommonKubectl.createResource(
      'pod.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.pod-log.e2e-spec',
    );

    podPage.navigateTo();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdatafilename);
  });

  it('L2:AldK8S-30: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击日志tab--验证日志显示正确', () => {
    // 进入日志页面
    podPage.resourceTable.getCell('状态', rowkey).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '运行中');
    });
    podPage.resourceTable.clickResourceNameByRow(rowkey);
    podPage.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');

    podPage.resourceBasicInfo.logviewer.waitLogDisplay();
    podPage.resourceBasicInfo.logviewer.logTextarea
      .getText()
      .then(function(text) {
        // 验证日志不为空
        expect(text.length).toBeGreaterThan(0);
      });
  });

  it('L2:AldK8S-31: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击日志tab--搜索日志--验证检索成功', () => {
    podPage.resourceBasicInfo.logviewer.getToolbarButton('查找').click();
    browser.sleep(200);
    podPage.resourceBasicInfo.logviewer.finder_inputbox.input('hehe');
    podPage.resourceBasicInfo.logviewer.resultfinder.getText().then(result => {
      expect(result.includes('无结果')).toBeFalsy();
    });
  });

  it('L2:AldK8S-36: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击日志tab--单击护眼模式--验证日志显示样式正确', () => {
    podPage.resourceBasicInfo.logviewer.getToolbarButton('日间').click();
    expect(podPage.resourceBasicInfo.logviewer.isNightMode()).toBeTruthy();
  });

  it('L2:AldK8S-33: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击日志tab--单击显示行号--验证行号显示正确', () => {
    podPage.resourceBasicInfo.logviewer.lineNumber.filter((elem, index) => {
      expect(elem.getText()).toBe(String(index));
      return true;
    });
  });

  it('L2:AldK8S-34: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击日志tab--单击自动更新开关--验证自动更新功能正确', () => {
    podPage.resourceBasicInfo.logviewer.checkBoxAutoRefresh.uncheck('自动更新');
    podPage.resourceBasicInfo.logviewer
      .getToolbarButton('最后一页')
      .click()
      .then(() => {
        browser.sleep(1000);
        podPage.resourceBasicInfo.logviewer.logTextarea.getText().then(text => {
          // 等待更新日志
          browser.sleep(10000);
          podPage.resourceBasicInfo.logviewer.logTextarea
            .getText()
            .then(newtext => {
              expect(text).toEqual(newtext);
            });
        });
      });
  });

  it('L2:AldK8S-35: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击日志tab--单击下载--验证日志下载成功', () => {
    podPage.resourceBasicInfo.logviewer.getToolbarButton('下载').click();

    // TO DO: 验证下载成功
  });
});
