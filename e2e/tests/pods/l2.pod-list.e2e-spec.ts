/**
 * Created by liuwei on 2018/3/15.
 */

import { ServerConf } from '../../config/serverConf';
import { PodPage } from '../../page_objects/computer/pod.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2:容器组列表页的显示，排序功能', () => {
  const podPage = new PodPage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    podPage.navigateTo();
    // 创建一个pod
    this.testdatafilename = CommonKubectl.createResource(
      'pod.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.pod-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdatafilename);
  });

  it('L2:AldK8S-41: 单击左导航计算／容器组 -- 进入容器组的资源列表页--验证列表默认按创建时间降序排序，可按名称、创建时间、状态、命名空间 排序', () => {
    // 默认按创建时间降序排列
    podPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    podPage.resourceTable.clickHeaderByName('名称');
    podPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    podPage.resourceTable.clickHeaderByName('名称');
    podPage.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    podPage.resourceTable.clickHeaderByName('命名空间');
    podPage.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    podPage.resourceTable.clickHeaderByName('命名空间');
    podPage.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    podPage.resourceTable.clickHeaderByName('创建时间');
    podPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    podPage.resourceTable.clickHeaderByName('创建时间');
    podPage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:Ald-40: 单击左导航计算／容器组 -- 进入容器组的资源列表页--验证列表为空时显示“无容器组”', () => {
    // 检索一个不存在的数据
    podPage.searchResource(CommonMethod.random_generate_testData());
    // 验证列表为空时显示‘无容器组’
    expect(podPage.resourceTable.noResult.getText()).toBe('无容器组');
    // 清空检索框
    podPage.searchResource(name);
  });

  it('L2:Ald-42: 单击左导航计算／容器组 -- 进入容器组的资源列表页--验证列表列显示正确', () => {
    // 验证表头显示正确
    podPage.resourceTable.getHeaderText().then(function(headerText) {
      expect(headerText).toEqual([
        '名称',
        '命名空间',
        '状态',
        '已使用资源',
        '重启',
        '创建时间',
        '',
      ]);
    });
  });

  it('L2:Ald-32: 单击左导航计算／容器组 -- 进入容器组的资源列表页--单击一个容器组名--进入详情页--单击日志tab--单击护眼模式--验证日志显示样式正确', () => {
    // 验证日志显示样式正确
    podPage.resourceTable.clickResourceNameByRow(rowkey);
    podPage.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');

    podPage.resourceBasicInfo.logviewer
      .getToolbarButton('日间')
      .isPresent()
      .then(ispresent => {
        if (ispresent) {
          expect(podPage.resourceBasicInfo.logviewer.isNightMode()).toBeFalsy();
          podPage.resourceBasicInfo.logviewer.getToolbarButton('日间').click();
          expect(
            podPage.resourceBasicInfo.logviewer.isNightMode(),
          ).toBeTruthy();
        } else {
          expect(
            podPage.resourceBasicInfo.logviewer.isNightMode(),
          ).toBeTruthy();
          podPage.resourceBasicInfo.logviewer.getToolbarButton('夜间').click();
          expect(podPage.resourceBasicInfo.logviewer.isNightMode()).toBeFalsy();
        }
      });
  });
});
