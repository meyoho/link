import { browser, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { SecretPage } from '../../page_objects/config/secret.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:通过UI创建保密字典功能', () => {
  const page = new SecretPage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    page.navigateTo();
  });
  afterAll(() => {
    // 删除测试数据\
    CommonKubectl.execKubectlCommand(
      `kubectl delete secert ${name} -n ${ServerConf.NAMESPACE}`,
    );
  });
  it('AldK8S-299:L0:进入保密字典列表--点击创建--选择命名空间--填写名称--选择opaque类型--填写保密项--点击创建--验证创建成功', () => {
    page.getButtonByText('创建保密字典').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建
    page.addSecret(name);
    page.addSecretdata('Opaque', key, value);
    page.getButtonByText('创建').click();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.navigateTo();
    // 查看创建后的列表中有
    CommonPage.waitElementPresent(page.resourceTable.getCell('名称', [name]));
    page.resourceTable.getCell('名称', [name]).then(elem => {
      expect(elem.getText()).toContain(name);
    });
  });
});
