/**
 * Created by liuwei on 2018/3/15.
 */

import { ServerConf } from '../../config/serverConf';
import { SecretPage } from '../../page_objects/config/secret.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2: 保密字典列表页的显示', () => {
  const secretPage = new SecretPage();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();

  const notExistName = CommonMethod.random_generate_testData();

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    secretPage.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'secret.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.secret-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L2:AldK8S-129: 单击左导航配置／保密字典 -- 进入保密字典列表页--验证列表默认按照创建时间倒序，可按名称，命名空间，创建时间排序', () => {
    // 默认按创建时间降序排列
    secretPage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    secretPage.resourceTable.clickHeaderByName('名称');
    secretPage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    secretPage.resourceTable.clickHeaderByName('名称');
    secretPage.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    secretPage.resourceTable.clickHeaderByName('命名空间');
    secretPage.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    secretPage.resourceTable.clickHeaderByName('命名空间');
    secretPage.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    secretPage.resourceTable.clickHeaderByName('创建时间');
    secretPage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    secretPage.resourceTable.clickHeaderByName('创建时间');
    secretPage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-130: 单击左导航配置／保密字典 -- 进入保密字典列表页--验证列表表头，操作列功能按钮按钮显示正确', () => {
    expect(secretPage.resourceTable.getHeaderText()).toEqual([
      '名称',
      '命名空间',
      '类型',
      '数量',
      '创建时间',
      '',
    ]);
    secretPage.searchBox.search(name);
    expect(
      secretPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签'),
    ).toEqual(['更新', '更新标签', '更新注解', '删除']);
    secretPage.resourcelabel.clickCancel();
  });

  it('L2:AldK8S-131: 单击左导航配置／保密字典 -- 进入保密字典列表页--检索一个不存在的保密字典--验证列表为空时，提示“无保密字典', () => {
    secretPage.searchBox.search(notExistName);
    expect(secretPage.resourceTable.noResult.getText()).toBe('无保密字典');
  });

  it('L2:AldK8S-132: 单击左导航配置／保密字典 -- 进入保密字典列表页--在检索框中输入存在的配置字典--验证检索功能正常', () => {
    secretPage.searchBox.search(name);
    expect(secretPage.resourceTable.getRowCount()).toBe(1);
    secretPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a').getText()).toBe(name);
    });
    secretPage.resourceTable.getCell('数量', rowkey).then(cell => {
      expect(cell.$('a').getText()).toBe('2');
    });
  });

  it('L2:AldK8S-133: 单击左导航配置／保密字典 -- 进入保密字典列表页--单击名称进入详情页--验证详情页信息显示正确', () => {
    secretPage.searchBox.search(name);
    secretPage.resourceTable.clickResourceNameByRow(rowkey);
    expect(secretPage.resourceBasicInfo.basicInfo.getAllKeyText()).toEqual([
      '名称',
      '命名空间',
      '标签',
      '注解',
      '类型',
      '创建时间',
    ]);

    this.secret = JSON.parse(
      CommonKubectl.execKubectlCommand(
        `kubectl get secrets ${name} -n ${namespace} -o json`,
      ),
    );

    expect(
      secretPage.detailPage.basicInfo.getElementByText('名称').getText(),
    ).toBe(name);
    expect(
      secretPage.detailPage.basicInfo.getElementByText('命名空间').getText(),
    ).toBe(namespace);
    expect(
      secretPage.detailPage.basicInfo.getElementByText('标签').getText(),
    ).toContain(label);

    let annotations = '';
    for (const key in this.secret.metadata.annotations) {
      if (key !== null) {
        annotations += `${key}: ${this.secret.metadata.annotations[key]}`;
      }
    }
    expect(
      secretPage.detailPage.basicInfo.getElementByText('注解').getText(),
    ).toBe(annotations);

    expect(
      secretPage.detailPage.basicInfo.getElementByText('类型').getText(),
    ).toBe(this.secret.type);

    // 验证数据正确

    expect(secretPage.detailPage.dataViewer.getAll_dataKeys()).toEqual([
      'password',
      'username',
    ]);
    expect(secretPage.detailPage.dataViewer.getAll_dataHeaders()).toEqual([
      'password\n: 12 字节',
      'username\n: 5 字节',
    ]);

    secretPage.detailPage.dataViewer.extend_header('password').then(() => {
      expect(
        secretPage.detailPage.dataViewer
          .get_dataByHeaderText('password')
          .getText(),
      ).toBe(CommonMethod.decode(this.secret.data.password));
    });

    secretPage.detailPage.dataViewer.extend_header('username').then(() => {
      expect(
        secretPage.detailPage.dataViewer
          .get_dataByHeaderText('username')
          .getText(),
      ).toBe(CommonMethod.decode(this.secret.data.username));
    });
  });
});
