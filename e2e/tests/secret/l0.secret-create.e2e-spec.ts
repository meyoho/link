/**
 * Created by liuwei on 2018/4/25.
 */

import { browser } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { SecretPage } from '../../page_objects/config/secret.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:保密字典创建功能', () => {
  const secretPage = new SecretPage();
  const namespace = ServerConf.NAMESPACE;
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    secretPage.navigateTo();
    secretPage.namespace.select(namespace);
  });

  afterAll(() => {
    // 删除测试数据
    secretPage.namespace.select('所有命名空间');
    CommonKubectl.execKubectlCommand(
      `kubectl delete secert ${name} -n ${ServerConf.NAMESPACE}`,
    );
  });

  it('L0:AldK8S-128: 单击左导航配置／保密字典 -- 进入保密字典列表页--单击创建保密字典按钮--输入yaml 样例，修改名称，命名空间，单击创建按钮--验证创建成功', () => {
    secretPage.clickCreateButton();
    secretPage.clickYAMLbutton();
    secretPage.yamlCreator.getYamlValue().then((yaml: string) => {
      secretPage.yamlCreator.clickToolbarByName('清空');
      const yamlNew = yaml.replace(
        CommonMethod.parseYaml(yaml).metadata.name,
        name,
      );
      secretPage.yamlCreator.setYamlValue(
        yamlNew.replace(
          CommonMethod.parseYaml(yamlNew).metadata.namespace,
          namespace,
        ),
      );
      secretPage.yamlCreator.clickButtonCreate();
    });
    secretPage.navigateTo();
    // 检索新创建的保密字段
    secretPage.searchBox.search(name);
    expect(secretPage.resourceTable.getRowCount()).toBe(1);
    // 验证保密字典创建成功
    secretPage.resourceTable.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a[href*=secret]').getText()).toBe(name);
    });
  });
});
