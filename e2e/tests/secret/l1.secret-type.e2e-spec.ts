import { browser, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { SecretPage } from '../../page_objects/config/secret.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:创建不同类型的保密字典功能', () => {
  const page = new SecretPage();
  const namespace = ServerConf.NAMESPACE;
  const name1 = CommonMethod.random_generate_testData();
  const name2 = CommonMethod.random_generate_testData();
  const name3 = CommonMethod.random_generate_testData();
  const name4 = CommonMethod.random_generate_testData();
  const tslcrt = `-----BEGIN CERTIFICATE-----
  MIIC2jCCAcKgAwIBAgIJAOHH79a7O5tPMA0GCSqGSIb3DQEBBQUAMBUxEzARBgNV
  BAMTCm1pbmlrdWJlQ0EwIBcNMTgwODE2MTAwODQ0WhgPMjI2NTAxMTMxMDA4NDRa
  MBIxEDAOBgNVBAMMB2t1YmUtY2EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
  AoIBAQCy0qqEKA3GmOwiT2HK4Fn25eH//rigZYYTyqq+TD9gKT03BBqKBD40CXZp
  8TVxiIREyNh0Akun4qWhhjr9GgqmFoBNRMcqa1MQYYjd2pBTUnzX1UC2g5dBIT6z
  UyHZaL59pjU2xD5eaz2IOvYNcJowPIj+0BXHoo4ZfzEYer7hP2wDQUqkMsyH2GRP
  7QoFqdrq3lzL7RiNpQHPxqFhtVarocBiKtq8XUnwfc+wLD9lJ8gbrWEpk0lHe21O
  zclguDInrxv4B9EkijQSJpwYKL1CZY0Sd8Fle7kpD3RD1zj1zEzZBN2i/zgL3Egm
  c4HDhC2ahhQdNXtu5U2ztATY45/TAgMBAAGjLjAsMAwGA1UdEwQFMAMBAf8wCwYD
  VR0PBAQDAgXgMA8GA1UdEQQIMAaHBMCoQAgwDQYJKoZIhvcNAQEFBQADggEBAKCR
  VAwsBnEhoKfc6ugh2XsBG14U5+E9GRaZIATb9jKnpNJ9sELEG1Q5EsKeGSANS+72
  7Ug40jLMIgPdDZM4sdA73d7RLf1M251cgsbjQwU/CwU4zF2KyOZ7g6R/GLTTKx8j
  4xlX5XZQNvXRujKcWd/fz/QlL7m9TvmoJw7eTa7JPqq2VsCTiMfif7aevWez9dqN
  ULWCGKKnfBDD2EczJ1EoENUTjmL5/s0faGdZ3aWAVUsWbWrxLwKaEM5baOTz9rfB
  I/1de+2RZwgYQPRNtAZg3wuZzGkBc7q4z/ZfX4+PDIjBcnO4dduXd7jbxMNI+0IA
  dgFdKtXA1cMHxhjzmlA=
  -----END CERTIFICATE-----`;
  const tslkey = `-----BEGIN PRIVATE KEY-----
  MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDAJ+6lxfagRJ/U
  E0N9HrjP07LieqwEAdoWkNcZ308VqJNqnvIQ8VIg9FbZmmx38Ymdt/vNM7vyrzaI
  Cc6cJo4LLoFZLRlKn/GIJuhurIF/U3jqcDC1A1PKA2yJzNRbsXsXtGcF3f54763M
  zCCorfsiBku4LLxKyhF0Da2/PDkfAkXxa4w5N/KN+5GPg2O9wifd9fs4byKlLfqL
  TUjwZcSIjlHOkgW8o5953Imnb9mV/2Fil61atzPqr21XF/rW9zrm/zdI1xCWblz1
  MK9hD20j8OXFlSFdwptfg33EH2ihEIoHvXEyW51JOs2YlGPzE4mfi3uSYXMmTqlB
  GzC5j0jbAgMBAAECggEBAJ/9H4A60VsjiAelel2kxK+VTa2wAaTcfd6n/dB203vu
  /b6aVGWy53S0u9M19fXvz2rYEvkJiAVJyRd/Qt1UZJP+xoefOx2dChcYfhIX40JG
  F7jqbENqX8slCqu5NV6a6SgVdmm2Kfg7ZvPLSa3oHd54a3cPGD8U+28rLGy7Ebw0
  T0TDM7V116gQMh5C7YXoZkc6GyoeXxrOZr52IZbzdbqMzQE/V+rIBhmbKx8hIH63
  j0+e9TjyObtmbPIMRhhWLTaF6ruWGP+LBimGfbYYMZJiZzgTIfqE1gUk3vFDl6CD
  RDpBNqvZf1fT4UBukMPNdtY5jZXIGDe/HQBwt4X3oOECgYEA9ErmXfJPfWYSzlM5
  PjYNQ5v4WHvvXEkmjXQS4Ox4hgQpSRPJ29eVLUQpgWfjxq3wOGOzz8svVpX82qzP
  EPBpX0AKbaL8FeB6mNGUECP17nOE7nBzoJWhQvLrerfHJtNb4Tto2JK2iAHiokGg
  dsPI70WixQig8E0bXmwivkWwP6MCgYEAyV1lMQOFPppRhkMDLtf4ZJyEdcfa2eYm
  qilXxnhin77BWPIfOWnzM+hR8QhwWTSk4vuJ/88+qeAhUgXGJlDTgtjQF//V8n4o
  9eUxMFFi6PqC1a6w877bHDGndyxZHbmTHt5NQRVYmHoQWukiLgLVQ4yff1JMl4eU
  Yq0yXCVNBWkCgYARrvpOAqROZ30lAlE6d58uLbQkc5Q4EfOAec6ECZoP2W1DpPMP
  vuNFi5GVlHeggibJbi4t9H9QvTq4eZnjmAOIqZIXrZyh2MYHGeSl5nR+9k3BFNLJ
  5QrvVbZGA9eer94Ox8YnJzefUMOfCzEaZ8G70EagDKfZvTsl8p3xOAoyEQKBgQCS
  O/tC7xWS8ysFYCEtobZICsq/ZlZvLA8kQEX4YnDtJfDwEVXPsjvmlG79pm1Au1SL
  arU7S1xsrdJE6OCP7t7MBXyKEUdkhW5ltP5V4qT8IQFnASiMuywQe5bsmw5U4fIP
  1s0v2LqwcOTzwJ8L3w7VsYgbRL9OxC1kw/bwqD4ZEQKBgDvfGSQSU9LVUxDRb59y
  sEeQdkol/6gHpmt1StKuNmjl4JjohIT5P5SUXx6psU+cU6JRbhHMOwId57Ctp1GK
  n5TUvumR02vZpnBFYu8ihzntWaFxqviE0ENfv9DDvyPQ+RwJPkmRz/Hap2utzuor
  bbfc9J7ICmNuROw2Dg+Uyh11
  -----END PRIVATE KEY-----`;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行

  beforeAll(() => {
    page.navigateTo();
  });
  it('AldK8S-301:L1:进入保密字典列表--点击创建--选择命名空间--填写名称--选择dockerconfigjson类型--填写证书和私钥--点击创建--验证创建成功', () => {
    page.getButtonByText('创建保密字典').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.addSecret(name2);
    page.addSecretdata('dockerconfigjson', name2 + 'alauda.cn', name2);
    page.getButtonByText('创建').click();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.navigateTo();
    // 验证列表页面
    CommonPage.waitProgressbarNotDisplay();
    page.resourceTable.getCell('名称', [name2]).then(elem => {
      expect(elem.getText()).toContain(name2);
    });
    CommonKubectl.execKubectlCommand(
      `kubectl delete secert ${name2} -n ${ServerConf.NAMESPACE}`,
    );
  });
  it('AldK8S-302:L1:进入保密字典列表--点击创建--选择命名空间--填写名称--选择dockerconfig类型--填写数据--点击创建--验证创建成功', () => {
    page.getButtonByText('创建保密字典').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.addSecret(name3);
    page.addSecretdata('dockercfg', name3 + 'alauda.cn', name3);
    page.getButtonByText('创建').click();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.navigateTo();
    CommonPage.waitProgressbarNotDisplay();

    page.resourceTable.getCell('名称', [name3]).then(elem => {
      expect(elem.getText()).toContain(name3);
    });
    CommonKubectl.execKubectlCommand(
      `kubectl delete secert ${name3} -n ${ServerConf.NAMESPACE}`,
    );
  });
  it('AldK8S-337:L1:进入保密字典列表--点击创建--选择命名空间--填写名称--添加一个自定义类型--选择自定义类型--填写数据-点击创建--验证创建成功', () => {
    page.getButtonByText('创建保密字典').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.addSecret(name4);
    page.addSecretdata('自定义', name4, '');
    page.getButtonByText('创建').click();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    // 进入列表页面
    page.navigateTo();
    // 验证Deployment运行情况
    CommonPage.waitProgressbarNotDisplay();
    page.resourceTable.getCell('名称', [name4]).then(elem => {
      expect(elem.getText()).toContain(name4);
    });
    CommonKubectl.execKubectlCommand(
      `kubectl delete secert ${name4} -n ${ServerConf.NAMESPACE}`,
    );
  });
});
