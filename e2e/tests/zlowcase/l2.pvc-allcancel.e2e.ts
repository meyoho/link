import { $$, by } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { PvcPage } from '../../page_objects/storage/pvc.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:持久卷声明取消操作', () => {
  const page = new PvcPage();
  const name = CommonMethod.random_generate_testData();
  const annotations = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const accessmode = 'ReadWriteOnce';
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata1 = CommonKubectl.createResource(
      'pvcprepare.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
        '${NAMESPACE}': namespace,
      },
      'l2.pvc-allcancel1.e2e',
    );
    this.testdata2 = CommonKubectl.createResource(
      'pvc.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
        '${NAMESPACE}': namespace,
      },
      'l2.pvc-allcancel2.e2e',
    );
  });
  beforeEach(() => {
    page.navigateTo();
    page.searchResource(name);
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(
      `kubectl delete pvc ${name} -n ${namespace}`,
    );
    CommonKubectl.execKubectlCommand(`kubectl delete pv ${name}`);
    CommonKubectl.execKubectlCommand(`kubectl delete storageclass ${name}`);
  });

  it('ACP-3050:进入持久卷声明列表-点击创建持久卷声明-输入参数-点击取消-弹框确认取消-验证弹窗信息正确持久卷声明未创建', () => {
    page.getButtonByText('创建持久卷声明').click();
    page.createPVC(name, '读写', 'Filesystem');
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('确定取消创建持久卷声明吗?');
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消创建后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    expect(page.resourceTable.getRow([`${name}1`]).isPresent()).toBe(false);
  });

  it('ACP-3051:进入持久卷声明列表-点击列表操作更新-输入参数-点击取消-弹框确认取消-验证弹窗信息正确持久卷声明未更新', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新');
    page.clickYAMLbutton();
    CommonPage.waitElementPresent(page.yamlCreator);
    page.yamlCreator.getYamlValue().then(yaml => {
      page.yamlCreator.setYamlValue(
        String(yaml).replace(`alauda.test: 'true'`, `${key}: ${value}`),
      );
      page.getButtonByText('取消').click();
    });
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消更新持久卷声明"${name}"吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消更新后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `alauda.test: true`,
    );
  });

  it('ACP-3052:进入持久卷声明列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });
  it('ACP-3053:进入持久卷声明列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3054:进入持久卷声明列表-点击操作-点击删除-弹出框-点击取消-验证持久卷声明未删除', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 持久卷声明 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });

  it('ACP-3055:进入配置字典详情页-点击右上角操作-点击更新-更新参数-点击取消-验证未更新', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('更新');
    page.clickYAMLbutton();
    CommonPage.waitProgressbarNotDisplay();
    CommonPage.waitElementPresent(page.yamlCreator);
    page.yamlCreator.getYamlValue().then(yaml => {
      page.yamlCreator.setYamlValue(
        String(yaml).replace(`alauda.test: 'true'`, `${key}: ${value}`),
      );
      page.getButtonByText('取消').click();
    });
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消更新持久卷声明"${name}"吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消更新后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).toContain(`alauda.test: true`);
      });
  });

  it('ACP-3058:进入持久卷声明详情页-点击右上角操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3056:进入持久卷声明详情页-点击右上角操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3057:进入持久卷声明详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 持久卷声明 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateTo();
    page.searchResource(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });
});
