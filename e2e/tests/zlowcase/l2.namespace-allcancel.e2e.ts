import { $$ } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { NamespacePage } from '../../page_objects/cluster/namespace.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:命名空间取消操作', () => {
  const page = new NamespacePage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'namespace.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
      },
      'l2.namespace-allcancel.e2e',
    );
  });
  beforeEach(() => {
    page.navigateTo();
  });
  afterAll(() => {
    page.navigateTo();
    page.namespace.select('所有命名空间');
    CommonPage.waitProgressbarNotDisplay();
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('ACP-2988:进入命名空间列表-点击创建命名空间-输入参数-点击取消-弹框确认取消-验证弹窗信息正确命名空间未创建', () => {
    page.getButtonByText('创建命名空间').click();
    page.addnamespace(`${name}1`);
    page.getButtonByText('取消').click();
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    expect(page.resourceTable.getRow([`${name}1`]).isPresent()).toBe(false);
  });

  it('ACP-2989:进入命名空间列表-点击列表页面操作-点击创建资源配额-修改参数-点击取消-验证创建资源配额未创建', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '添加资源配额');
    CommonPage.waitProgressbarNotDisplay();
    page.addresourcequota(name, '1', '1', '1');
    page.getButtonByText('取消').click();

    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消创建资源配额吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消创建后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    page.resourceTable.clickResourceNameByRow(rowkey);
    expect(page.resourceQuotas.getRow([name]).isPresent()).toBe(false);
  });

  it('ACP-2990:进入命名空间列表-点击列表页面操作-点击创建资源限额-修改参数-点击取消-验证创建资源限额未创建', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '添加资源限额');
    CommonPage.waitProgressbarNotDisplay();
    page.addlimitrange(name);
    page.getButtonByText('取消').click();

    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消创建资源限额吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消创建后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    page.resourceTable.clickResourceNameByRow(rowkey);
    expect(page.limitRanges.getRow([name]).isPresent()).toBe(false);
  });

  it('ACP-2991:进入命名空间列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });
  it('ACP-2992:进入命名空间列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-2993:进入命名空间列表-点击操作-点击删除-弹出框-点击取消-验证命名空间未删除', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 命名空间 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });

  it('ACP-2994:进入命名空间详情-点击右上角操作-点击创建资源配额-修改参数-点击取消-验证创建资源配额未创建', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('添加资源配额');
    CommonPage.waitProgressbarNotDisplay();
    page.addresourcequota(name, '1', '1', '1');
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消创建资源配额吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消创建后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    expect(page.resourceQuotas.getRow([name]).isPresent()).toBe(false);
  });

  it('ACP-2995:进入命名空间详情-点击右上角操作-点击创建资源限额-修改参数-点击取消-验证创建资源限额未创建', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('添加资源限额');
    CommonPage.waitProgressbarNotDisplay();
    page.addlimitrange(name);
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消创建资源限额吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消创建后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    expect(page.limitRanges.getRow([name]).isPresent()).toBe(false);
  });

  it('ACP-2996:进入命名空间详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 命名空间 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateTo();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });

  it('ACP-2997:点击进入命名空间详情页-点击标签后的蓝笔-添加标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-2998:点击进入命名空间详情页-点击注解后的蓝笔-添加注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });
});
