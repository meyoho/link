import { $$, by } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { ReplicasetPage } from '../../page_objects/computer/replicaset.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:副本集取消操作', () => {
  const page = new ReplicasetPage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'replicaset.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': name,
      },
      'l2.replicaset-allcancel.e2e',
    );
  });
  beforeEach(() => {
    page.navigateTo();
    page.searchResource(name);
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('ACP-3059:进入副本集列表-点击列表操作更新-输入参数-点击取消-弹框确认取消-验证弹窗信息正确副本集未更新', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新');
    CommonPage.waitElementPresent(page.yamlCreator);
    page.yamlCreator.getYamlValue().then(yaml => {
      page.yamlCreator.setYamlValue(
        String(yaml).replace(`alauda.test: 'true'`, `${key}: ${value}`),
      );
      page.getButtonByText('取消').click();
    });
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `alauda.test: true`,
    );
  });

  it('ACP-3060:进入部署列表-点击列表操作-点击扩缩容-扩容为2-点击取消-验证扩容为成功', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '扩缩容');
    page.resourceScale.inputScale('2');
    page.resourcelabel.clickCancel();
    // 验证未更新
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.yamlCreator.getYamlValue().then(text => {
      expect(text).not.toContain('replicas: 2');
    });
  });

  it('ACP-3061:进入副本集列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });

  it('ACP-3062:进入副本集列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3063:进入副本集列表-点击操作-点击删除-弹出框-点击取消-验证副本集未删除', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 副本集 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });

  it('ACP-3064:进入配置字典详情页-点击右上角操作-点击更新-更新参数-点击取消-验证未更新', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('更新');
    CommonPage.waitProgressbarNotDisplay();
    CommonPage.waitElementPresent(page.yamlCreator);
    page.yamlCreator.getYamlValue().then(yaml => {
      page.yamlCreator.setYamlValue(
        String(yaml).replace(`alauda.test: 'true'`, `${key}: ${value}`),
      );
      page.getButtonByText('取消').click();
    });
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).toContain(`alauda.test: true`);
      });
  });

  it('ACP-3065:进入部署详情页-点击右上角操作-点击扩缩容-扩容为2-点击取消-验证扩容为成功', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('扩缩容');
    page.resourceScale.inputScale('2');
    page.resourcelabel.clickCancel();
    // 验证未更新
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.yamlCreator.getYamlValue().then(text => {
      expect(text).not.toContain('replicas: 2');
    });
  });

  it('ACP-3066:进入副本集详情页-点击右上角操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3067:进入副本集详情页-点击右上角操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3068:进入副本集详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 副本集 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateTo();
    page.searchResource(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });
});
