import { $$ } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { IngressesPage } from '../../page_objects/network/ingresses.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:访问权取消操作', () => {
  const page = new IngressesPage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const host = name + 'alauda.cn';
  const tslhost = 'tsl' + name + 'alauda.cn';
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'ingresses-prapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
      },
      'l2.ingresses-allcancel1.e2e',
    );
    this.testdata = CommonKubectl.createResource(
      'ingress.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${TSLHOST}': tslhost,
        '${HOST}': host,
        '${SNAME}': name,
      },
      'l2.ingresses-allcancel2.e2e',
    );
  });
  beforeEach(() => {
    page.navigateTo();
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('ACP-2972:进入访问权列表-点击创建访问权-输入参数-点击取消-弹框确认取消-验证弹窗信息正确定时任务未创建', () => {
    page.getButtonByText('创建访问权').click();
    CommonPage.waitProgressbarNotDisplay();
    page.addingress(`${name}1`, name, '80');
    page.adddomain('alaudalinkauto1.test.com', [
      '/linktest1',
      `${name}`,
      '80',
      '',
    ]);
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('确定取消创建访问权吗?');
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消创建后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    expect(page.resourceTable.getRow([`${name}1`]).isPresent()).toBe(false);
  });

  it('ACP-2973:进入访问权列表-点击列表页面操作-点击更新-修改参数-点击取消-验证访问权未更新', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新');
    CommonPage.waitProgressbarNotDisplay();
    page.adddomain('alaudalinkauto2.test.com', [
      '/linktesttesttest',
      `${name}`,
      '80',
      '',
      '/linktesttesttest',
      `${name}`,
      '80',
      '',
    ]);
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消更新访问权"${name}"吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消更新后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceTable
      .getRow([host])
      .getText()
      .then(text => {
        expect(text).toEqual(`${host}\n路径 服务 端口\n/testpath ${name} 8090`);
      });
  });

  it('ACP-2974:进入访问权列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });
  it('ACP-2975:进入访问权列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-2976:进入访问权列表-点击操作-点击删除-弹出框-点击取消-验证访问权未删除', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 访问权 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });
  it('ACP-2977:进入访问权详情页-点击右上角操作-点击更新-更新参数-点击取消-验证未更新', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('更新');
    CommonPage.waitProgressbarNotDisplay();
    page.adddomain('alaudalinkauto2.test.com', [
      '/linktesttesttest',
      `${name}`,
      '80',
      '',
      '/linktesttesttest',
      `${name}`,
      '80',
      '',
    ]);
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消更新访问权"${name}"吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消更新后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    page.resourceTable
      .getRow([host])
      .getText()
      .then(text => {
        expect(text).toEqual(`${host}\n路径 服务 端口\n/testpath ${name} 8090`);
      });
  });

  it('ACP-2978:进入访问权详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 访问权 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateTo();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });

  it('ACP-2979:点击进入访问权详情页-点击标签后的蓝笔-添加标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-2980:点击进入访问权详情页-点击注解后的蓝笔-添加注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });
});
