import { $$, by } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { PodPage } from '../../page_objects/computer/pod.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:容器组取消操作', () => {
  const page = new PodPage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'pod.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.pods-allcancel.e2e',
    );
  });
  beforeEach(() => {
    page.navigateTo();
    page.searchResource(name);
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('ACP-3035:进入容器组列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });
  it('ACP-3036:进入容器组列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3037:进入容器组列表-点击操作-点击删除-弹出框-点击取消-验证容器组未删除', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 容器组 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
    page.resourceTable.getCell('状态', rowkey).then(function(cell) {
      expect(cell.element(by.css('alk-pod-status')).getText()).not.toBe(
        '删除中',
      );
    });
  });
  it('ACP-3038:进入容器组详情页-点击右上角操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3039:进入容器组详情页-点击右上角操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3040:进入容器组详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 容器组 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateTo();
    page.searchResource(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
    page.resourceTable.getCell('状态', rowkey).then(function(cell) {
      expect(cell.element(by.css('alk-pod-status')).getText()).not.toBe(
        '删除中',
      );
    });
  });
});
