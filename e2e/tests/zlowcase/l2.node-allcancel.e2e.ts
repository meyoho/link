import { $$, browser } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { NodePage } from '../../page_objects/cluster/node.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:节点取消操作', () => {
  const page = new NodePage();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  let name;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  beforeAll(() => {
    page.navigateTo();
    page.nodeNameElem.getText().then(text => {
      name = text;
    });
  });
  beforeEach(() => {
    page.navigateTo();
  });
  it('ACP-3008:进入节点列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickOperationButtonByRow([name], '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel([name], '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });

  it('ACP-3009:进入节点列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickOperationButtonByRow([name], '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });
  it('ACP-3010:进入节点列表-点击操作-点击更新taints-添加一个taints-点击取消-验证taints未添加', () => {
    page.resourceTable.clickOperationButtonByRow([name], '更新 Taint');
    page
      .getAlaudaInputTable()
      .fillinTablenolabel([name, '', 'NoSchedule', '0']);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('Taints')
      .getText()
      .then(function(labelText) {
        expect(labelText).toBe(`-`);
      });
  });
  it('ACP-3011:进入节点列表-点击操作-点击停止调度-点击取消-验证节点可调度', () => {
    page.resourceTable.clickOperationButtonByRow([name], '停止调度');
    CommonPage.waitElementDisplay(page.confirmDialog.buttonCancel);
    page.confirmDialog.clickCancel();
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('可调度')
      .getText()
      .then(text => {
        expect(text).toBe('是');
      });
  });
  it('ACP-3012:进入节点列表-点击操作-点击维护模式-点击取消-验证节点可调度', () => {
    page.resourceTable.clickOperationButtonByRow([name], '维护模式');
    CommonPage.waitElementDisplay(page.confirmDialog.buttonCancel);
    page.confirmDialog.clickCancel();
    page.resourceTable.clickResourceNameByRow([name]);
    page.resourceBasicInfo.basicInfo
      .getElementByText('可调度')
      .getText()
      .then(text => {
        expect(text).toBe('是');
      });
  });

  it('ACP-3014:进入节点详情页-点击右上角操作-点击停在调度-点击取消-验证节点可调度', () => {
    page.resourceTable.clickResourceNameByRow([name]);
    page.operation.select('停止调度');
    CommonPage.waitElementDisplay(page.confirmDialog.buttonCancel);
    page.confirmDialog.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('可调度')
      .getText()
      .then(text => {
        expect(text).toBe('是');
      });
  });
  it('ACP-3015:进入节点详情页-点击右上角操作-点击维护模式-点击取消-验证节点可调度', () => {
    page.resourceTable.clickResourceNameByRow([name]);
    page.operation.select('维护模式');
    CommonPage.waitElementDisplay(page.confirmDialog.buttonCancel);
    page.confirmDialog.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('可调度')
      .getText()
      .then(text => {
        expect(text).toBe('是');
      });
  });

  it('ACP-3016:点击进入节点详情页-点击标签后的蓝笔-添加标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickResourceNameByRow([name]);
    page.clickBasicPencil('标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3017:点击进入节点详情页-点击注解后的蓝笔-添加注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickResourceNameByRow([name]);
    page.clickBasicPencil('注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3018:进入节点详情页-点击右上角操作-Taints-添加一个taints-点击取消-验证taints未添加', () => {
    page.resourceTable.clickResourceNameByRow([name]);
    page.clickBasicPencil('Taints');
    page
      .getAlaudaInputTable()
      .fillinTablenolabel([name, '', 'NoSchedule', '0']);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('Taints')
      .getText()
      .then(function(labelText) {
        expect(labelText).toBe(`-`);
      });
  });
});
