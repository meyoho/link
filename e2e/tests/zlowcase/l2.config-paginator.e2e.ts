/**
 * Created by changdongmei on 2019/3/21.
 */
import { ServerConf } from '../../config/serverConf';
import { ConfigPage } from '../../page_objects/config/config.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:配置字典页面的分页', () => {
  const page = new ConfigPage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    // 点击左导航的角色按钮
    page.navigateTo();
    // 创建一个Deployment
    this.testdata = CommonKubectl.createResource(
      'config-paginator.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.config-paginator.e2e',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('ACP-3158:进入配置字典页面-验证分页控件页数及分页功能', () => {
    page.alaudaPaginator.selectedPagenum.then(text => {
      expect('1').toBe(text);
    });
    page.alaudaPaginator.pageSize.then(text => {
      expect('20').toBe(text);
    });
    page.resourceTable.getRowCount().then(num => {
      expect(num).toBe(20);
    });
    page.alaudaPaginator.frontbutton.isEnabled().then(isEnabled => {
      expect(false).toBe(isEnabled);
    });
    page.alaudaPaginator.pageNum.then(text => {
      // tslint:disable-next-line:radix
      if (parseInt(text) === 1) {
        page.alaudaPaginator.nextbutton.isEnabled().then(isEnabled => {
          expect(false).toBe(isEnabled);
        });
        // tslint:disable-next-line:radix
      } else if (parseInt(text) > 7) {
        page.alaudaPaginator.nextbutton.isEnabled().then(isEnabled => {
          expect(true).toBe(isEnabled);
        });
        expect(page.alaudaPaginator.fastnavigator.isPresent()).toBe(true);
      } else {
        page.alaudaPaginator.nextbutton.isEnabled().then(isEnabled => {
          expect(true).toBe(isEnabled);
        });
        expect(page.alaudaPaginator.fastnavigator.isPresent()).toBe(false);
      }

      // tslint:disable-next-line:radix
      if (parseInt(text) > 1) {
        page.alaudaPaginator.clickNextpage();
        page.alaudaPaginator.frontbutton.isEnabled().then(isEnabled => {
          expect(true).toBe(isEnabled);
        });
        page.alaudaPaginator.selectedPagenum.then(pagenum => {
          expect('2').toBe(pagenum);
        });
        page.alaudaPaginator.clickPageByText(text);

        page.alaudaPaginator.nextbutton.isEnabled().then(isEnabled => {
          expect(false).toBe(isEnabled);
        });
        page.alaudaPaginator.selectedPagenum.then(pagenum => {
          expect(text).toBe(pagenum);
        });
        page.alaudaPaginator.selectTotalofpage('10');
        page.resourceTable.getRowCount().then(num => {
          expect(10).toBe(num);
        });
      }
    });
  });
});
