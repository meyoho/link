import { $$, by } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { StorageClassPage } from '../../page_objects/storage/storageclass.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:存储类取消操作', () => {
  const page = new StorageClassPage();
  const name = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'storageClass.yaml',
      {
        '${NAME}': name,
        '${LABEL}': label,
      },
      'l2.storageclass-allcancel.e2e',
    );
  });
  beforeEach(() => {
    page.navigateTo();
    page.searchResource(name);
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.execKubectlCommand(`kubectl delete storageclass ${name}`);
  });

  it('ACP-3099:进入存储类列表-点击创建存储类-输入参数-点击取消-弹框确认取消-验证弹窗信息正确存储类未创建', () => {
    page.getButtonByText('创建存储类').click();
    page.yamlCreator.waiteditorspinnerNotdisplay();
    page.yamlCreator.labelwrite.click();
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      page.yamlCreator.clickToolbarByName('清空');
      const yamlNew = yaml.replace(
        CommonMethod.parseYaml(yaml).metadata.name,
        `${name}1`,
      );
      page.getButtonByText('取消').click();
    });
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    expect(page.resourceTable.getRow([`${name}1`]).isPresent()).toBe(false);
  });

  it('ACP-3100:进入存储类列表-点击列表操作更新-输入参数-点击取消-弹框确认取消-验证弹窗信息正确存储类未更新', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新');
    CommonPage.waitElementPresent(page.yamlCreator);
    page.yamlCreator.getYamlValue().then(yaml => {
      page.yamlCreator.setYamlValue(
        String(yaml).replace(`alauda.test: 'true'`, `${key}: ${value}`),
      );
      page.getButtonByText('取消').click();
    });
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `alauda.test: true`,
    );
  });

  it('ACP-3101:进入存储类列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });
  it('ACP-3102:进入存储类列表-点击列表操作-点击设为默认-点击取消-验证不是默认存储卷', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '设为默认');
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`设为默认`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toContain(`确定要将该 ${name} 设置为默认使用的存储类吗？`);
      expect(text).toContain(`修改后，原有的默认存储类将会被取消默认设置`);
    });
    page.warningDialog.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('是否默认')
      .getText()
      .then(text => {
        expect(text).toBe(`否`);
      });
  });

  it('ACP-3103:进入存储类列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3104:进入存储类列表-点击操作-点击删除-弹出框-点击取消-验证存储类未删除', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 storageclass ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });

  it('ACP-3105:进入配置字典详情页-点击右上角操作-点击更新-更新参数-点击取消-验证未更新', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('更新');
    CommonPage.waitProgressbarNotDisplay();
    CommonPage.waitElementPresent(page.yamlCreator);
    page.yamlCreator.getYamlValue().then(yaml => {
      page.yamlCreator.setYamlValue(
        String(yaml).replace(`alauda.test: 'true'`, `${key}: ${value}`),
      );
      page.getButtonByText('取消').click();
    });
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).toContain(`alauda.test: true`);
      });
  });

  it('ACP-3106:进入存储类详情页-点击右上角操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3107:进入存储类详情页-点击右上角操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3108:进入存储类详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 storageclass ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateTo();
    page.searchResource(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });
});
