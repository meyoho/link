import { $$ } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { ServicePage } from '../../page_objects/network/service.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:服务取消操作', () => {
  const page = new ServicePage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  const ipaddress = `10.96.${CommonMethod.randomNum(
    1,
    254,
  )}.${CommonMethod.randomNum(1, 254)}`;
  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'service.withoutSelector.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
        '${SERVICETYPE}': 'ClusterIP',
      },
      'l2.service-allcancel.e2e',
    );
  });
  beforeEach(() => {
    page.navigateTo();
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('ACP-3078:进入服务列表-点击创建服务-输入参数-点击取消-弹框确认取消-验证弹窗信息正确服务未创建', () => {
    page.getButtonByText('创建服务').click();
    CommonPage.waitProgressbarNotDisplay();
    page.addservice(`${name}1`, 'ClusterIP', '自动', ipaddress);
    page.addPortAndSelector(
      [`${name}`, 'TCP', '80', '80', '', '', 'TCP', '80', '80', '1'],
      ['app', `${name}`, '', '', '', '1'],
    );
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('确定取消创建服务吗?');
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消创建后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    expect(page.resourceTable.getRow([`${name}1`]).isPresent()).toBe(false);
  });

  it('ACP-3079:进入服务列表-点击列表页面操作-点击更新-修改参数-点击取消-验证服务未更新', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新');
    CommonPage.waitProgressbarNotDisplay();
    page.clickYAMLbutton();
    page.yamlCreator.getYamlValue().then(yaml => {
      page.yamlCreator.setYamlValue(
        String(yaml).replace('port: 80', 'port: 8080'),
      );
    });
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消更新服务"${name}"吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消更新后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('内部端点')
      .getText()
      .then(text => {
        expect(text).toEqual(`${name}.${namespace}:80 TCP`);
      });
  });

  it('ACP-3080:进入服务列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });

  it('ACP-3081:进入服务列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3082:进入服务列表-点击列表操作-点击更新选择器-添加一个选择器-点击取消-验证选择器未添加', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新选择器');
    page.resourceSelector.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey, '选择器')).not.toContain(
      `${key}: ${value}`,
    );
  });

  it('ACP-3083:进入服务列表-点击操作-点击删除-弹出框-点击取消-验证服务未删除', () => {
    page.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 服务 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });

  it('ACP-3084:进入服务详情页-点击右上角操作-点击更新-更新参数-点击取消-验证未更新', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('更新');
    CommonPage.waitProgressbarNotDisplay();
    page.clickYAMLbutton();
    page.yamlCreator.getYamlValue().then(yaml => {
      page.yamlCreator.setYamlValue(
        String(yaml).replace('port: 80', 'port: 8080'),
      );
    });
    page.getButtonByText('取消').click();
    CommonPage.waitElementPresent(page.warningDialog.confirmTitle);
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe(`确定取消更新服务"${name}"吗?`);
    });
    page.warningDialog.confirmMassage.getText().then(text => {
      expect(text).toBe('取消更新后，已编辑的内容不会生效');
    });
    page.warningDialog.clickConfirm();
    page.resourceBasicInfo.basicInfo
      .getElementByText('内部端点')
      .getText()
      .then(text => {
        expect(text).toEqual(`${name}.${namespace}:80 TCP`);
      });
  });

  it('ACP-3085:进入服务详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 服务 ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateTo();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey).isPresent()).toBe(true);
  });

  it('ACP-3086:点击进入服务详情页-点击标签后的蓝笔-添加标签-点击取消-验证标签未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3087:点击进入服务详情页-点击注解后的蓝笔-添加注解-点击取消-验证注解未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.clickBasicPencil('注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });

  it('ACP-3088:进入服务详情页-点击右上角操作-点击更新选择器-添加一个选择器-点击取消-验证选择器未添加', () => {
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('更新选择器');
    page.resourceSelector.newValue(key, value);
    page.resourceSelector.clickCancel();
    page.resourceBasicInfo.basicInfo
      .getElementByText('选择器')
      .getText()
      .then(text => {
        expect(text).not.toContain(`${key}: ${value}`);
      });
  });
});
