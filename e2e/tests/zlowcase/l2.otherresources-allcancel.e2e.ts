import { $$ } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { CronJobPage } from '../../page_objects/computer/cronjob.page';
import { OtherResourcePage } from '../../page_objects/otherResource/other.resource.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:其他资源取消操作', () => {
  const page = new OtherResourcePage();
  const name = CommonMethod.random_generate_testData();
  const key = CommonMethod.random_generate_testData();
  const value = CommonMethod.random_generate_testData();
  const label = CommonMethod.random_generate_testData();
  const alaudaUserName = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey1 = [name];
  const rowkey2 = [alaudaUserName];

  beforeAll(() => {
    this.testdata1 = CommonKubectl.createResource(
      'service-account.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.otherresources-allcancel1.e2e',
    );
    this.testdata2 = CommonKubectl.createResource(
      'alauda-user.yaml',
      {
        '${NAME}': alaudaUserName,
        '${LABEL}': label,
      },
      'l2.otherresources-allcancel2.e2e',
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata1);
    CommonKubectl.deleteResourceByYmal(this.testdata2);
  });

  it('ACP-3019:进入命名空间相关资源列表-点击操作按钮-点击更新-输入参数-点击取消-命名空间相关资源未更新', () => {
    page.navigateToAboutNamespace();
    page.searchResource(name);
    page.resourceTable.clickOperationButtonByRow(rowkey1, '更新');
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      page.yamlCreator.setYamlValue(
        yaml.replace(
          CommonMethod.parseYaml(yaml).metadata.label,
          `name: ${name}`,
        ),
      );
      page.getButtonByText('取消').click();
    });
    expect(page.resourceTable.getCellLabel(rowkey1, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });

  it('ACP-3020:进入命名空间相关资源列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.navigateToAboutNamespace();
    page.searchResource(name);
    page.resourceTable.clickOperationButtonByRow(rowkey1, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey1, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });
  it('ACP-3021:进入命名空间相关资源列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.navigateToAboutNamespace();
    page.searchResource(name);
    page.resourceTable.clickOperationButtonByRow(rowkey1, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey1);
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      expect(CommonMethod.parseYaml(yaml).metadata.label).not.toContain(value);
    });
  });

  it('ACP-3022:进入命名空间相关资源列表-点击操作-点击删除-弹出框-点击取消-验证命名空间相关资源未删除', () => {
    page.navigateToAboutNamespace();
    page.searchResource(name);
    page.resourceTable.clickOperationButtonByRow(rowkey1, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 serviceaccount ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey1).isPresent()).toBe(true);
  });
  it('ACP-3023:进入命名空间相关资源详情页-点击右上角操作-点击更新-输入参数-点击取消-命名空间相关资源未更新', () => {
    page.navigateToAboutNamespace();
    page.searchResource(name);
    page.resourceTable.clickResourceNameByRow(rowkey1);
    page.operation.select('更新');
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      page.yamlCreator.setYamlValue(
        yaml.replace(
          CommonMethod.parseYaml(yaml).metadata.label,
          `name: ${name}`,
        ),
      );
      page.getButtonByText('取消').click();
    });
    page.navigateToAboutNamespace();
    page.searchResource(name);
    expect(page.resourceTable.getCellLabel(rowkey1, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });

  it('ACP-3024:进入命名空间相关资源详情页-点击右上角操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.navigateToAboutNamespace();
    page.searchResource(name);
    page.resourceTable.clickResourceNameByRow(rowkey1);
    page.operation.select('更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      expect(CommonMethod.parseYaml(yaml).metadata.label).not.toContain(value);
    });
  });

  it('ACP-3025:进入命名空间相关资源详情页-点击右上角操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.navigateToAboutNamespace();
    page.searchResource(name);
    page.resourceTable.clickResourceNameByRow(rowkey1);
    page.operation.select('更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      expect(CommonMethod.parseYaml(yaml).metadata.label).not.toContain(value);
    });
  });

  it('ACP-3026:进入命名空间相关资源详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.navigateToAboutNamespace();
    page.searchResource(name);
    page.resourceTable.clickResourceNameByRow(rowkey1);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 serviceaccount ${name} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateToAboutNamespace();
    page.searchBox.search(name);
    expect(page.resourceTable.getRow(rowkey1).isPresent()).toBe(true);
  });
  // 集群相关资源修改
  it('ACP-3027:进入集群相关资源列表-点击操作按钮-点击更新-输入参数-点击取消-集群相关资源未更新', () => {
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    page.resourceTable.clickOperationButtonByRow(rowkey2, '更新');
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      page.yamlCreator.setYamlValue(
        yaml.replace(
          CommonMethod.parseYaml(yaml).metadata.label,
          `${key}: ${value}`,
        ),
      );
      page.getButtonByText('取消').click();
    });
    expect(page.resourceTable.getCellLabel(rowkey2, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });

  it('ACP-3028:进入集群相关资源列表-点击列表操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    page.resourceTable.clickOperationButtonByRow(rowkey2, '更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    expect(page.resourceTable.getCellLabel(rowkey2, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });
  it('ACP-3029:进入集群相关资源列表-点击操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    page.resourceTable.clickOperationButtonByRow(rowkey2, '更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.resourceTable.clickResourceNameByRow(rowkey2);
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      expect(CommonMethod.parseYaml(yaml).metadata.label).not.toContain(value);
    });
  });

  it('ACP-3030:进入集群相关资源列表-点击操作-点击删除-弹出框-点击取消-验证集群相关资源未删除', () => {
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    page.resourceTable.clickOperationButtonByRow(rowkey2, '删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 alaudauser ${alaudaUserName} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.searchBox.search(alaudaUserName);
    expect(page.resourceTable.getRow(rowkey2).isPresent()).toBe(true);
  });
  it('ACP-3031:进入集群相关资源详情页-点击右上角操作-点击更新-输入参数-点击取消-集群相关资源未更新', () => {
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    page.resourceTable.clickResourceNameByRow(rowkey2);
    page.operation.select('更新');
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      page.yamlCreator.setYamlValue(
        yaml.replace(
          CommonMethod.parseYaml(yaml).metadata.label,
          `${key}: ${value}`,
        ),
      );
      page.getButtonByText('取消').click();
    });
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    expect(page.resourceTable.getCellLabel(rowkey2, '名称')).not.toContain(
      `${key}: ${value}`,
    );
  });

  it('ACP-3032:进入集群相关资源详情页-点击右上角操作-点击更新标签-添加一个标签-点击取消-验证标签未添加', () => {
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    page.resourceTable.clickResourceNameByRow(rowkey2);
    page.operation.select('更新标签');
    page.resourcelabel.newValue(key, value);
    page.resourcelabel.clickCancel();
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      expect(CommonMethod.parseYaml(yaml).metadata.label).not.toContain(value);
    });
  });

  it('ACP-3033:进入集群相关资源详情页-点击右上角操作-点击更新注解-添加一个注解-点击取消-验证注解未添加', () => {
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    page.resourceTable.clickResourceNameByRow(rowkey2);
    page.operation.select('更新注解');
    page.resourceAnnotations.newValue(key, value);
    page.resourceAnnotations.clickCancel();
    page.yamlCreator.getYamlValue().then((yaml: string) => {
      expect(CommonMethod.parseYaml(yaml).metadata.label).not.toContain(value);
    });
  });

  it('ACP-3034:进入集群相关资源详情页-点击右上角操作-点击删除-点击取消-验证未删除', () => {
    page.navigateToAboutCluster();
    page.searchResource(alaudaUserName);
    page.resourceTable.clickResourceNameByRow(rowkey2);
    page.operation.select('删除');
    expect(page.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 alaudauser ${alaudaUserName} 吗?`,
    );
    page.confirmDialog.clickCancel();
    page.navigateToAboutCluster();
    page.searchBox.search(alaudaUserName);
    expect(page.resourceTable.getRow(rowkey2).isPresent()).toBe(true);
  });
});
