import { ServerConf } from '../../config/serverConf';
import { NetworkPoliciesPage } from '../../page_objects/network/networkpolicies.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:网络策略修改与删除', () => {
  const networkPoliciesPage = new NetworkPoliciesPage();
  const namespace = ServerConf.NAMESPACE;

  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'networkpolicy.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
      },
      'l1.networkpolicies-details.e2e-spec',
    );
  });
  beforeEach(() => {
    networkPoliciesPage.navigateTo();
  });
  afterAll(() => {
    // 清除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('AldK8S-217:L1单击做单航网络/网络策略--点击一个已存在的网络策略的操作--更新--更新网络策略的信息--点击更新--验证更新成功', () => {
    networkPoliciesPage.searchBox.search(name);
    // 添加资源配额
    networkPoliciesPage.resourceTable.clickOperationButtonByRow(rowkey, '更新');
    CommonPage.waitElementPresent(networkPoliciesPage.yamlCreator);
    networkPoliciesPage.yamlCreator.getYamlValue().then(yaml => {
      networkPoliciesPage.yamlCreator.setYamlValue(
        String(yaml).replace('port: 6379', 'port: 10092'),
      );
      networkPoliciesPage.yamlCreator.clickButtonCreate();
      expect(networkPoliciesPage.toastsuccess.getText()).toBe('更新成功');
    });
    networkPoliciesPage.resourceTable.clickResourceNameByRow(rowkey);
    CommonPage.waitElementPresent(
      networkPoliciesPage.resourceBasicInfo.resourceInfoTab,
    );
  });
  it('AldK8S-218:L2单击做单航网络/网络策略-点击一个已存在的网络策略的操作--更新标签--添加一个标签--点击更新--验证标签已添加', () => {
    networkPoliciesPage.resourceTable.clickOperationButtonByRow(
      rowkey,
      '更新标签',
    );
    networkPoliciesPage.resourcelabel.newValue(
      'autotestupdate',
      'autotestupdatelabel',
    );
    networkPoliciesPage.resourcelabel.clickUpdate();
    networkPoliciesPage.resourceTable.clickResourceNameByRow(rowkey);

    networkPoliciesPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
      '基本信息',
    );
    // 验证标签修改正确
    networkPoliciesPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel');
      });

    // 验证详情页基本信息页
    expect(
      networkPoliciesPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(name);
  });
  it(' AldK8S-219:L2: 单击做单航网络 / 网络策略 -- 点击一个已存在的网络策略的操作--更新注解--添加一个注解--点击更新--验证注解已添加', () => {
    networkPoliciesPage.resourceTable.clickOperationButtonByRow(
      rowkey,
      '更新注解',
    );
    networkPoliciesPage.resourcelabel.newValue(
      'autotestupdate',
      'autotestupdate',
    );
    networkPoliciesPage.resourcelabel.clickUpdate();
    networkPoliciesPage.resourceTable.clickResourceNameByRow(rowkey);

    networkPoliciesPage.resourceBasicInfo.resourceInfoTab.clickTabByText(
      '基本信息',
    );
    // 验证标签修改正确
    networkPoliciesPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate');
      });

    // 验证详情页基本信息页
    expect(
      networkPoliciesPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(name);
  });

  it('AldK8S-220:L2: 单击做单航网络 / 网络策略 -- 点击一个已存在的网络策略的操作--删除--点击确认删除--验证网络策略成功删除', () => {
    networkPoliciesPage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    networkPoliciesPage.confirmDialog.clickConfirm();
    networkPoliciesPage.searchBox.search(name);
    expect(networkPoliciesPage.resourceTable.noResult.getText()).toBe(
      '无网络策略',
    );
  });
});
