import { $, $$, browser, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { HelloPage } from '../../page_objects/network/helloworld.page';
import { ServicePage } from '../../page_objects/network/service.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:创建可访问服务并进行访问', () => {
  const page = new ServicePage();
  const hello = new HelloPage();

  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  const baseurl = browser.baseUrl;
  let port;
  let url: string;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'service-visit.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
      },
      'l1.ingresses-detailsservice.e2e-spec',
    );
    page.navigateTo();
    // 进入详情页面
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.detailPage.endpointList.getCell('主机端口', ['TCP']).then(elem => {
      elem.getText().then(text => {
        port = text;
      });
    });
    page.detailPage.podList.clickResourceNameByRow(['运行中']);
    page.resourceBasicInfo.basicInfo
      .getElementByText('节点')
      .$('a')
      .click();
    CommonPage.waitProgressbarNotDisplay();
    page.resourceBasicInfo.basicInfo
      .getElementByText('地址')
      .$$('div.plain-container__label')
      .first()
      .getText()
      .then(text => {
        if (baseurl === 'http://localhost:4200/') {
          url = process.env.CLUSTER_MASTER_IP;
        } else {
          url = text.substring(text.indexOf(':') + 1).trim();
        }
      });
  });

  afterAll(() => {
    // 清除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('AldK8S-411:l1:创建一个可访问的服务-验证可以访问', () => {
    console.log(`http://${url}:${port}`);
    browser.get(`http://${url}:${port}`).then(ii => {
      browser.getCurrentUrl().then(text => {
        expect(text).toBe(`http://${url}:${port}/`);
      });
      hello.titletext1.then(text => {
        expect(text).toBe('Hello World!');
      });
    });
  });
});
