import { browser } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { IngressesPage } from '../../page_objects/network/ingresses.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:访问权详情', () => {
  const ingressPage = new IngressesPage();
  const name = CommonMethod.random_generate_testData();
  const sname = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  const host = name + 'alauda.cn';
  const tslhost = 'tsl' + name + 'alauda.cn';
  beforeAll(() => {
    this.servicetestdata = CommonKubectl.createResource(
      'service.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': name,
        '${SERVICETYPE}': 'ClusterIP',
        '${clusterIP}': '',
      },
      'l1.ingresses-detailsservice.e2e-spec',
    );
    this.ingresstestdata = CommonKubectl.createResource(
      'ingress.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${TSLHOST}': tslhost,
        '${HOST}': host,
        '${SNAME}': sname,
      },
      'l1.ingresses-details.e2e-spec',
    );
    ingressPage.navigateTo();
  });
  afterAll(() => {
    // 清除测试数据
    CommonKubectl.deleteResourceByYmal(this.servicetestdata);
    CommonKubectl.deleteResourceByYmal(this.ingresstestdata);
  });

  it('AldK8S-269:L1:单击左导航网络 / 访问权 -- 点击一个访问权名称 -- 验证详情页面基本信息访问权规则tsl证书', () => {
    // 进入详情页面
    ingressPage.resourceTable.clickResourceNameByRow(rowkey);
    ingressPage.resourceBasicInfo.basicInfo.getAllKeyText().then(text => {
      expect(text).toEqual([
        '名称',
        '命名空间',
        '标签',
        '注解',
        '默认服务',
        '创建时间',
      ]);
    });
    ingressPage.resourceBasicInfo.resourceInfoTab.getTabText().then(text => {
      expect(text).toEqual(['基本信息', 'YAML', '事件']);
    });
    ingressPage.resourceTable.getHeaderText().then(text => {
      expect(text).toEqual(['域名', '规则', '保密字典', '域名']);
    });
    ingressPage.RuleTable.getHeaderText().then(text => {
      expect(text).toEqual(['路径', '服务', '端口']);
    });
    ingressPage.RuleTable.getCell('端口', [8090]).then(elem => {
      elem.getText().then(text => {
        expect(text).toEqual('8090');
      });
    });
  });
  it('AldK8S-267:L1:单击左导航网络 / 访问权 -- 点击访问权名称 -- 点击标签后的蓝笔-- 添加一个标签 -- 验证标签添加成功', () => {
    ingressPage.resourceBasicInfo.basicButtion.getElementByText('标签').click();
    ingressPage.resourcelabel.newValue(
      'autotestupdate1',
      'autotestupdatelabel1',
    );
    ingressPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    ingressPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel1');
      });
    CommonPage.waitToastNotPresent();
  });
  it('AldK8S-268:L1:单击左导航网络 / 访问权 -- 点击访问权名称 -- 点击注解后的蓝笔-- 添加一个注解-- 验证注解添加成功', () => {
    ingressPage.resourceBasicInfo.basicButtion.getElementByText('注解').click();
    ingressPage.resourcelabel.newValue('autotestupdate1', 'autotestupdate1');
    ingressPage.resourcelabel.clickUpdate();
    // 验证标签修改正确
    ingressPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate1');
      });
    CommonPage.waitToastNotPresent();
  });
  it('AldK8S-365:L1:进入访问权列表-点击一个名称-通过UI更新访问规则-验证更新成功', () => {
    ingressPage.operation.select('更新');
    ingressPage.adddomain('link.autotest.alauda.cn');
    ingressPage.clickupdate();
    ingressPage.resourceTable
      .getRow(['link.autotest.alauda.cn'])
      .getText()
      .then(text => {
        expect(text).toEqual(
          `link.autotest.alauda.cn\n路径 服务 端口\n/testpath ${name} 8090`,
        );
      });
    CommonPage.waitToastNotPresent();
  });
  it('AldK8S-266:L1:单击左导航网络/访问权--点击一个访问权名称--点击右上角删除--确认删除--验证删除成功', () => {
    ingressPage.operation.select('删除');
    ingressPage.confirmDialog.clickConfirm();
    browser.sleep(100);
    CommonPage.waitProgressbarNotDisplay();
    CommonPage.waitElementNotPresent(ingressPage.toastsuccess.message);
    ingressPage.searchBox.search(name);
    expect(ingressPage.resourceTable.noResult.getText()).toBe('无访问权');
    CommonPage.waitToastNotPresent();
  });
});
