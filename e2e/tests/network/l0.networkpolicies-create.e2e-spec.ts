import { ServerConf } from '../../config/serverConf';
import { NetworkPoliciesPage } from '../../page_objects/network/networkpolicies.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:网络策略创建功能', () => {
  const netPoliciesPage = new NetworkPoliciesPage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  const namespace = ServerConf.NAMESPACE;

  beforeAll(() => {
    netPoliciesPage.navigateTo();
  });

  afterAll(() => {
    // 清除测试数据
    CommonKubectl.execKubectlCommand(
      `kubectl delete networkpolicies ${name} -n ${namespace}`,
    );
  });
  it('AldK8S-216:L0:单击左导航网络/网络策略--点击创建网络策略--填写yaml--点击创建--验证网络策略创建成功', () => {
    netPoliciesPage.getButtonByText('创建网络策略').click();
    netPoliciesPage.yamlCreator.waiteditorspinnerNotdisplay();
    netPoliciesPage.yamlCreator.labelwrite.click();
    netPoliciesPage.yamlCreator.getYamlValue().then((yaml: string) => {
      netPoliciesPage.yamlCreator.clickToolbarByName('清空');
      const yamlNew = yaml.replace(
        CommonMethod.parseYaml(yaml).metadata.name,
        name,
      );
      netPoliciesPage.yamlCreator.setYamlValue(
        yamlNew.replace(
          CommonMethod.parseYaml(yamlNew).metadata.namespace,
          namespace,
        ),
      );
      netPoliciesPage.yamlCreator.clickButtonCreate();
      // expect(secretPage.toastsuccess.getText()).toContain('创建成功');
    });
    // 验证namespace 创建成功
    netPoliciesPage.searchBox.search(name);
    expect(netPoliciesPage.resourceTable.getRowCount()).toBe(1);
  });
});
