import { ServerConf } from '../../config/serverConf';
import { IngressesPage } from '../../page_objects/network/ingresses.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L0:访问权创建', () => {
  const ingressPage = new IngressesPage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    ingressPage.navigateTo();
  });

  afterAll(() => {
    // 清除测试数据
    CommonKubectl.execKubectlCommand(
      `kubectl delete ingress ${name} -n ${ServerConf.NAMESPACE}`,
    );
  });
  it('AldK8S-211:L0:单击左导航网络/访问权--点击创建访问权--点击写入修改name值--点击创建--验证访问权创建成功', () => {
    ingressPage.getButtonByText('创建访问权').click();
    ingressPage.clickYAMLbutton();
    ingressPage.yamlCreator.getYamlValue().then((yaml: string) => {
      ingressPage.yamlCreator.clickToolbarByName('清空');
      const yamlNew = yaml.replace(
        CommonMethod.parseYaml(yaml).metadata.name,
        name,
      );
      ingressPage.yamlCreator.setYamlValue(
        yamlNew.replace(
          CommonMethod.parseYaml(yamlNew).metadata.namespace,
          ServerConf.NAMESPACE,
        ),
      );
      ingressPage.yamlCreator.clickButtonCreate();
      // expect(secretPage.toastsuccess.getText()).toContain('创建成功');
    });
    ingressPage.navigateTo();
    // 验证namespace 创建成功
    ingressPage.searchBox.search(name);
    expect(ingressPage.resourceTable.getRowCount()).toBe(1);
  });
});
