import { ServerConf } from '../../config/serverConf';
import { ServicePage } from '../../page_objects/network/service.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L2:服务列表页的显示，排序功能', () => {
  const servicePage = new ServicePage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const serviceName = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [serviceName];

  beforeAll(() => {
    servicePage.navigateTo();

    this.testdata1 = CommonKubectl.createResource(
      'service.withoutSelector.yaml',
      {
        '${NAME}': serviceName,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
        '${SERVICETYPE}': 'ClusterIP',
      },
      'l2.service-list.e2e-spec',
    );
    this.testdata2 = CommonKubectl.createResource(
      'service-pod.yaml',
      {
        '${NAME}': serviceName,
        '${NAMESPACE}': namespace,
      },
      'l2.service-list-pod.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    servicePage.navigateTo();

    servicePage.namespace.select('所有命名空间');
    CommonKubectl.deleteResourceByYmal(this.testdata1);
    CommonKubectl.deleteResourceByYmal(this.testdata2);
  });

  it('L2:AldK8S-105: 单击左导航网络／服务 -- 进入服务列表页--验证Service 列表默认按照创建时间倒序,可按名称、创建时间、命名空间 排序', () => {
    // 默认按创建时间降序排列
    servicePage.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    servicePage.resourceTable.clickHeaderByName('名称');
    servicePage.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    servicePage.resourceTable.clickHeaderByName('名称');
    servicePage.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    servicePage.resourceTable.clickHeaderByName('命名空间');
    servicePage.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    servicePage.resourceTable.clickHeaderByName('命名空间');
    servicePage.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    servicePage.resourceTable.clickHeaderByName('创建时间');
    servicePage.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    servicePage.resourceTable.clickHeaderByName('创建时间');
    servicePage.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-104: 单击左导航网络／服务 -- 进入服务列表页--检索一个不存在的服务--验证Service 列表为空时，提示“无服务”', () => {
    servicePage.searchBox.search(CommonMethod.random_generate_testData());
    expect(servicePage.resourceTable.noResult.getText()).toBe('无服务');
  });

  it('L2:AldK8S-106: 单击左导航网络／服务 -- 进入服务列表页--检索一个存在的服务--验证检索功能正常', () => {
    servicePage.searchBox.search(serviceName);
    servicePage.resourceTable.getCell('名称', rowkey).then(cell => {
      CommonPage.waitElementPresent(cell.$('a'));
      expect(cell.$('a').getText()).toBe(serviceName);
    });
  });

  it('L2:AldK8S-107: 单击左导航网络／服务 -- 进入服务列表页--验证表头显示正确，Pod Selector 为空时，列表显示“-单击操作列按钮菜单显示正确，单击名称进入详细页，单击操作按钮--验证菜单显示正确', () => {
    // 验证表头显示正确
    expect(servicePage.resourceTable.getHeaderText()).toEqual([
      '名称',
      '命名空间',
      '类型',
      '集群IP',
      '内部端点',
      '选择器',
      '创建时间',
      '',
    ]);

    // Pod Selector 为空时，列表显示“-”
    servicePage.resourceTable.getCell('选择器', rowkey).then(cell => {
      expect(cell.getText()).toBe('-');
    });

    // 验证操作列的菜单显示正确
    expect(
      servicePage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签'),
    ).toEqual(['更新', '更新标签', '更新注解', '更新选择器', '删除']);

    servicePage.resourcelabel.clickCancel();
    // 进入service 的详情页
    servicePage.resourceTable.clickResourceNameByRow(rowkey);
    // Pod Selector 为空时, Endpoint 对象不会被创建
    CommonPage.waitElementPresent(servicePage.detailPage.podList.noResult);
    expect(servicePage.detailPage.podList.noResult.getText()).toBe('无容器组');
    // expect(servicePage.detailInfo_endpoint.noResult.getText()).toBe('无端点');
    // 验证操作下拉框的菜单显示正确
    expect(servicePage.operation.select('更新选择器')).toEqual([
      '更新',
      '更新选择器',
      '删除',
    ]);
    servicePage.resourcelabel.clickCancel();
  });
  it('AldK8S-377:L1:进入服务列表页-点击操作更新-通过UI添加一个选择器-点击更新-验证更新成功有选择器对应的pod', () => {
    servicePage.navigateTo();
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '更新');
    servicePage.addPortAndSelector(
      [],
      ['app', `${serviceName}`, '', '', '', '1'],
    );
    servicePage.clickUpdate();
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('选择器')
      .getText()
      .then(text => {
        expect(text).toBe(`app: ${serviceName}`);
      });
  });
});
