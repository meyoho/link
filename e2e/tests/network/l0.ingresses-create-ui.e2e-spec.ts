import { browser } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { IngressesPage } from '../../page_objects/network/ingresses.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:访问权UI创建', () => {
  const page = new IngressesPage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  const namespace = ServerConf.NAMESPACE;
  beforeAll(() => {
    page.navigateTo();
    this.testdata = CommonKubectl.createResource(
      'ingresses-prapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
      },
      'l0.ingress-create-ui.e2e-spec',
    );
  });
  afterEach(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete ingress ${name} -n ${namespace}`,
    );
  });
  afterAll(() => {
    // 清除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('AldK8S-364:L1:进入访问权列表-点击创建访问权-默认UI创建-选择命名空间-填写名称-选择默认服务-填写服务端口-添加一个范访问规则-点击创建-验证创建成功对应服务正确访问规则正确', () => {
    page.getButtonByText('创建访问权').click();
    page.addingress(name, name, '80');
    page.adddomain('alaudalinkauto1.test.com', [
      '/linktest1',
      `${name}1`,
      '80',
      '',
    ]);
    page.clickCreateButton();
    browser.sleep(100);
    CommonPage.waitElementDisplay(page.toastsuccess.message);
    page.toastsuccess.message.getText().then(text => {
      expect(text).toBe(`创建成功: Ingress ${name}`);
    });
    CommonPage.waitProgressbarNotDisplay();
    page.resourceBasicInfo.basicInfo
      .getElementByText('默认服务')
      .getText()
      .then(text => {
        expect(text).toEqual(`${name}\n: 80`);
      });
    page.resourceTable
      .getRow(['alaudalinkauto1.test.com'])
      .getText()
      .then(text => {
        expect(text).toEqual(
          `alaudalinkauto1.test.com\n路径 服务 端口\n/linktest1 ${name}1 80`,
        );
      });
  });
});
