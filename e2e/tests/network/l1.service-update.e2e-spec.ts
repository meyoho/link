/**
 * Created by liuwei on 2018/3/15.
 */

import { ServerConf } from '../../config/serverConf';
import { ServicePage } from '../../page_objects/network/service.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L1:服务更新功能', () => {
  const servicePage = new ServicePage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();

  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();

  const serviceName = CommonMethod.random_generate_testData();
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [serviceName];

  beforeAll(() => {
    servicePage.navigateTo();

    this.testdata = CommonKubectl.createResource(
      'service.withoutSelector.yaml',
      {
        '${NAME}': serviceName,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
        '${SERVICETYPE}': 'ClusterIP',
      },
      'l1.service-update.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    servicePage.namespace.select('所有命名空间');
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L1:AldK8S-109: 单击左导航网络／服务 -- 进入服务列表页--选择一个服务，单击操作列，选择更新标签--验证更新标签功能正确', () => {
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    servicePage.resourcelabel.newValue(label_newkey, label_newvalue);
    servicePage.resourcelabel.clickUpdate();
    expect(servicePage.toastsuccess.getText()).toBe('标签 更新成功');
    expect(servicePage.resourceTable.getCellLabel(rowkey, '名称')).toContain(
      `${label_newkey}: ${label_newvalue}`,
    );
  });

  it('L1:AldK8S-110: 单击左导航网络／服务 -- 进入服务列表页--选择一个服务，单击操作列，选择更新注解--验证更新注解功能正确', () => {
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    servicePage.resourceAnnotations.newValue(
      annotations_newkey,
      annotations_newvalue,
    );
    servicePage.resourceAnnotations.clickUpdate();
    expect(servicePage.toastsuccess.getText()).toBe('注解 更新成功');
  });

  it('L1:AldK8S-111: 单击左导航网络／服务 -- 进入服务列表页--选择一个服务，单击操作列，选择更新选择器--验证更新选择器功能正确', () => {
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '更新选择器');
    servicePage.resourceSelector.newValue('app', 'guestbook');
    servicePage.resourceSelector.clickUpdate();
    expect(servicePage.toastsuccess.getText()).toBe('更新成功');
  });

  it('L1:AldK8S-108: 单击左导航网络／服务 -- 进入服务列表页--选择一个服务单击进入详情页，单击操作，选择更新--验证更新功能正确', () => {
    servicePage.resourceTable.clickResourceNameByRow(rowkey);
    servicePage.operation.select('更新');
    servicePage.clickYAMLbutton();
    servicePage.yamlCreator.getYamlValue().then(yaml => {
      servicePage.yamlCreator.setYamlValue(
        String(yaml).replace('port: 80', 'port: 8080'),
      );
      servicePage.yamlCreator.clickButtonCreate();
      expect(servicePage.toastsuccess.getText()).toBe('更新成功');
    });
  });

  it('L1:AldK8S-113: 单击左导航网络／服务 -- 进入服务列表页--选择一个服务，单击名称进入详情页--验证详细页显示正确', () => {
    servicePage.clickLeftNavByText('服务');
    servicePage.namespace.select(namespace);
    servicePage.resourceTable.clickResourceNameByRow(rowkey);
    expect(
      servicePage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(serviceName);
    expect(
      servicePage.resourceBasicInfo.basicInfo
        .getElementByText('命名空间')
        .getText(),
    ).toBe(namespace);
    expect(
      servicePage.resourceBasicInfo.basicInfo
        .getElementByText('标签')
        .getText(),
    ).toContain(`${label_newkey}: ${label_newvalue}`);
    expect(
      servicePage.resourceBasicInfo.basicInfo
        .getElementByText('注解')
        .getText(),
    ).toContain(`${annotations_newkey}: ${annotations_newvalue}`);
    expect(
      servicePage.resourceBasicInfo.basicInfo
        .getElementByText('选择器')
        .getText(),
    ).toBe(`app: guestbook`);
    expect(
      servicePage.resourceBasicInfo.basicInfo
        .getElementByText('会话保持')
        .getText(),
    ).toBe('None');
    expect(
      servicePage.resourceBasicInfo.basicInfo
        .getElementByText('类型')
        .getText(),
    ).toBe('ClusterIP');
    expect(
      servicePage.resourceBasicInfo.basicInfo
        .getElementByText('内部端点')
        .getText(),
    ).toContain(`${serviceName}.${namespace}:8080 TCP`);
    servicePage.detailPage.podList.getCell('名称', rowkey).then(cell => {
      expect(cell.$('a').getText()).toContain(serviceName);
    });

    servicePage.detailPage.podList.getCell('状态', rowkey).then(cell => {
      expect(cell.getText()).toContain('运行中');
    });
  });

  it('L1:AldK8S-112: 单击左导航网络／服务 -- 进入服务列表页--选择一个服务，单击操作列，选择删除--验证删除功能正确', () => {
    servicePage.operation.select('删除');
    expect(servicePage.confirmDialog.confirmTitle.getText()).toBe(
      `确定删除 服务 ${serviceName} 吗?`,
    );
    servicePage.confirmDialog.clickConfirm();
    servicePage.searchBox.search(serviceName);
    expect(servicePage.resourceTable.noResult.getText()).toBe('无服务');
  });
});
