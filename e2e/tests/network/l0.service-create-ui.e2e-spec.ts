/**
 * Created by liuwei on 2018/3/15.
 */

import { browser } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { ServicePage } from '../../page_objects/network/service.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L0:UI服务创建功能', () => {
  const servicePage = new ServicePage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const ipaddress = `10.96.${CommonMethod.randomNum(
    1,
    254,
  )}.${CommonMethod.randomNum(1, 254)}`;
  const prot1 = CommonMethod.randomNum(31000, 32000);
  const prot2 = CommonMethod.randomNum(31000, 32000);
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  beforeAll(() => {
    servicePage.navigateTo();
    this.prapare = CommonKubectl.createResource(
      'serviceprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.servicecreate.prapare',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.prapare);
  });

  it('AldK8S-350:L1:单击左导航网络/服务-点击创建服务-选择命名空间-填写名称-选择类别ClusterIP-点击自动-亲和性为none-添加一个tcp端口-验证创建成功详细信息与所填写的一致', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.addservice(name, 'ClusterIP', '自动', ipaddress);
    servicePage.addPortAndSelector(
      [`${name}`, 'TCP', '80', '80', '', '', 'TCP', '80', '80', '1'],
      ['app', `${name}`, '', '', '', '1'],
    );
    servicePage.addMore();
    servicePage.getButtonByText('创建').click();
    browser.sleep(100);
    CommonPage.waitProgressbarNotDisplay();
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('内部端点')
      .getText()
      .then(text => {
        expect(text).toEqual(`${name}.${namespace}:80 TCP`);
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('类型')
      .getText()
      .then(text => {
        expect(text).toEqual('ClusterIP');
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('会话保持')
      .getText()
      .then(text => {
        expect(text).toEqual('None');
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('选择器')
      .getText()
      .then(text => {
        expect(text).toContain(`app: ${name}`);
      });
    servicePage.navigateTo();
    servicePage.searchBox.search(name);
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    servicePage.confirmDialog.clickConfirm();
  });
  it('AldK8S-352:L1:单击左导航网络/服务-点击创建服务-选择命名空间-填写名称-选择类别ClusterIP-点击None-亲和性为否-添加UDP与TCP端口-验证创建成功详细信息与所填写的一致', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.addservice(name, 'ClusterIP', 'None', ipaddress);
    servicePage.addPortAndSelector(
      [
        `${name}1`,
        'TCP',
        '80',
        '80',
        '',
        `${name}2`,
        'UDP',
        '8080',
        '8080',
        '',
      ],
      ['app', `${name}`, '', '', '', '1'],
    );
    servicePage.addMore();
    servicePage.getButtonByText('创建').click();
    browser.sleep(100);
    CommonPage.waitProgressbarNotDisplay();
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('内部端点')
      .getText()
      .then(text => {
        expect(text).toEqual(
          `${name}.${namespace}:80 TCP\n${name}.${namespace}:8080 UDP`,
        );
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('类型')
      .getText()
      .then(text => {
        expect(text).toEqual('ClusterIP');
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('会话保持')
      .getText()
      .then(text => {
        expect(text).toEqual('None');
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('选择器')
      .getText()
      .then(text => {
        expect(text).toContain(`app: ${name}`);
      });
    servicePage.navigateTo();
    servicePage.searchBox.search(name);
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    servicePage.confirmDialog.clickConfirm();
  });
  it('AldK8S-354:L1:单击左导航网络/服务-选择命名空间-填写名称-选择类型nodeport-集群IP手动填写-亲和选择是-添加端口-点击创建-验证服务创建成功并且详细信息与填写一致', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.addservice(name, 'NodePort', '手动', ipaddress, 'ClientIP');
    servicePage.addPortAndSelector(
      [
        `${name}1`,
        'TCP',
        '80',
        '80',
        prot1,
        '',
        `${name}2`,
        'UDP',
        '8080',
        '8080',
        prot2,
        '',
      ],
      ['app', `${name}`, '', '', '', '1'],
    );
    servicePage.addMore();
    servicePage.getButtonByText('创建').click();
    browser.sleep(100);
    CommonPage.waitProgressbarNotDisplay();
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('类型')
      .getText()
      .then(text => {
        expect(text).toEqual('NodePort');
      });
    // 检查端口正确
    servicePage.resourceTable
      .getRow([`${name}1`, prot1])
      .getText()
      .then(text => {
        expect(text).toEqual(`${name}1\nTCP\n80\n80\n${prot1}`);
      });
    servicePage.resourceTable
      .getRow([`${name}2`, prot2])
      .getText()
      .then(text => {
        expect(text).toEqual(`${name}2\nUDP\n8080\n8080\n${prot2}`);
      });
    servicePage.navigateTo();
    servicePage.searchBox.search(name);
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    servicePage.confirmDialog.clickConfirm();
  });
  it('AldK8S-356:L1:单击左导航网络/服务-选择命名空间-填写名称-选择类型loadblance-集群IP选择自动-亲和选择否-添加端口-点击创建-验证服务创建成功并且详细信息与填写一致', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.addservice(name, 'LoadBalancer', '手动', ipaddress);
    servicePage.addPortAndSelector(
      [
        `${name}`,
        'TCP',
        '80',
        '80',
        `${prot1}`,
        '',
        '',
        'TCP',
        '',
        '',
        '',
        '1',
      ],
      ['app', `${name}`, '', '', '', '1'],
    );
    servicePage.getButtonByText('创建').click();
    browser.sleep(100);
    CommonPage.waitProgressbarNotDisplay();
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('内部端点')
      .getText()
      .then(text => {
        expect(text).toEqual(`${name}.${namespace}:80 TCP`);
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('类型')
      .getText()
      .then(text => {
        expect(text).toEqual('LoadBalancer');
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('LoadBalancerIP')
      .getText()
      .then(text => {
        expect(text).toEqual('123.23.121.22');
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('选择器')
      .getText()
      .then(text => {
        expect(text).toContain(`app: ${name}`);
      });
    servicePage.detailInfo_endpoint
      .getRow([`${name}`])
      .getText()
      .then(text => {
        // console.log(text);
        expect(text).toEqual(`${name}\nTCP\n80\n80\n${prot1}`);
      });
    servicePage.navigateTo();
    servicePage.searchBox.search(name);
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    servicePage.confirmDialog.clickConfirm();
  });
  it('AldK8S-357:L1:单击左导航网络/服务-点击创建服-命名空间-名称-类型externalName-集群IP自动-亲和选择否-externalName填写:testalauda.com-验证服务创建并且与填写一致', () => {
    servicePage.getButtonByText('创建服务').click();
    servicePage.addservice(name, 'ExternalName', '自动');
    servicePage.getButtonByText('创建').click();
    browser.sleep(100);
    CommonPage.waitProgressbarNotDisplay();
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('类型')
      .getText()
      .then(text => {
        expect(text).toEqual('ExternalName');
      });
    servicePage.resourceBasicInfo.basicInfo
      .getElementByText('ExternalName')
      .getText()
      .then(text => {
        expect(text).toEqual('alauda.linkautotest.com');
      });
    servicePage.navigateTo();
    servicePage.searchBox.search(name);
    servicePage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    servicePage.confirmDialog.clickConfirm();
  });
});
