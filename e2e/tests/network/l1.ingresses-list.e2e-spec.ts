import { ServerConf } from '../../config/serverConf';
import { IngressesPage } from '../../page_objects/network/ingresses.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';

describe('L1:访问权列表', () => {
  const ingressPage = new IngressesPage();
  const name = CommonMethod.random_generate_testData();
  const sname = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];
  const host = name + 'alauda.cn';
  const tslhost = 'tsl' + name + 'alauda.cn';

  beforeAll(() => {
    this.testdata = CommonKubectl.createResource(
      'ingress.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${TSLHOST}': tslhost,
        '${HOST}': host,
        '${SNAME}': sname,
      },
      'l1.ingresses-list.e2e-spec',
    );
  });
  beforeEach(() => {
    ingressPage.navigateTo();
  });
  afterAll(() => {
    // 清除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('AldK8S-212:L1:单击左导航网络 / 访问权 -- 点击一个已存在的访问权后面的操作--更新--更新访问权--点击更新--验证更新成', () => {
    ingressPage.searchBox.search(name);
    // 添加资源配额
    ingressPage.resourceTable.clickOperationButtonByRow(rowkey, '更新');
    CommonPage.waitElementPresent(ingressPage.yamlCreator);
    ingressPage.clickYAMLbutton();
    // 清空yaml
    ingressPage.yamlCreator.getYamlValue().then(yaml => {
      ingressPage.yamlCreator.setYamlValue(
        String(yaml).replace(/resourceVersion: '(.*)'/g, ''),
      );
    });
    ingressPage.yamlCreator.getYamlValue().then(yaml => {
      ingressPage.yamlCreator.setYamlValue(
        String(yaml).replace('servicePort: 8090', 'servicePort: 8080'),
      );
      ingressPage.yamlCreator.clickButtonCreate();
    });
    // 进入详情页面 验证资源配额已生成
    ingressPage.resourceTable.clickResourceNameByRow(rowkey);
    CommonPage.waitElementPresent(
      ingressPage.resourceBasicInfo.resourceInfoTab,
    );
  });
  it('AldK8S-213:L2:单击左导航网络 / 访问权--点击一个已存在的访问权的操作--更新标签--添加一个标签--验证添加成功', () => {
    ingressPage.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');
    ingressPage.resourcelabel.newValue('autotestupdate', 'autotestupdatelabel');
    ingressPage.resourcelabel.clickUpdate();
    ingressPage.resourceTable.clickResourceNameByRow(rowkey);

    ingressPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    ingressPage.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdatelabel');
      });

    // 验证详情页基本信息页
    expect(
      ingressPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(name);
  });
  it('AldK8S-214:L2:单击左导航网络 / 访问权--点击一个已存在的访问权的操作--更新注解--添加一个注解--验证添加成功', () => {
    ingressPage.resourceTable.clickOperationButtonByRow(rowkey, '更新注解');
    ingressPage.resourcelabel.newValue('autotestupdate', 'autotestupdate');
    ingressPage.resourcelabel.clickUpdate();
    ingressPage.resourceTable.clickResourceNameByRow(rowkey);

    ingressPage.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    ingressPage.resourceBasicInfo.basicInfo
      .getElementByText('注解')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain('autotestupdate');
      });
    // 验证详情页基本信息页
    expect(
      ingressPage.resourceBasicInfo.basicInfo
        .getElementByText('名称')
        .getText(),
    ).toBe(name);
  });

  it('AldK8S-215:L2:单击左导航网络 / 访问权--点击一个已存在的访问权的操作--删除--点击确认删除--验证访问权限已删除', () => {
    ingressPage.resourceTable.clickOperationButtonByRow(rowkey, '删除');
    ingressPage.confirmDialog.clickConfirm();
    ingressPage.searchBox.search(name);
    CommonPage.waitElementPresent(ingressPage.resourceTable.noResult);
    expect(ingressPage.resourceTable.noResult.getText()).toBe('无访问权');
  });
});
