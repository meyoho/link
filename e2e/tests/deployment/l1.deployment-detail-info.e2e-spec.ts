/**
 * Created by liuwei on 2018/3/15.
 */
import { browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_deployments } from '../../utility/resource.type.k8s';

describe('L1:部署更新功能的验证', () => {
  const page = new DeploymentPage();
  const addVolumePage = new AddVolumePage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const annotations_newkey = CommonMethod.random_generate_testData();
  const annotations_newvalue = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const configmap = name;

  // 根据【名称，类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    page.navigateTo();

    // 创建一个Deployment
    this.testdata = CommonKubectl.createResource(
      'deployment.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l1.deployment-detail-info.e2e-spec',
    );
    // 选择新创建的namespace
  });

  afterAll(() => {
    // 删除测试数据
    page.namespace.select('所有命名空间');
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L1:AldK8S-43: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--验证显示信息正确', () => {
    page.clickLeftNavByText('部署');
    page.resourceTable.getCell('状态', rowkey).then(cell => {
      CommonPage.waitElementTextChangeTo(cell, '1 / 1');
      cell.getText().then(text => {
        console.log(text);
      });
    });
    page.resourceTable.clickResourceNameByRow(rowkey);
    // 验证详情页基本信息部分字段值是否正确
    const resourceJson = CommonApi.getResourceJSON(
      name,
      k8s_type_deployments,
      namespace,
    );

    // 验证详情页基本信息页
    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('名称').getText(),
    ).toBe(resourceJson.metadata.name);

    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('命名空间').getText(),
    ).toBe(resourceJson.metadata.namespace);

    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('标签').getText(),
    ).toContain(resourceJson.metadata.labels.deploy);

    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('注解').getText(),
    ).toContain(Object.keys(resourceJson.metadata.annotations)[0]);

    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('选择器').getText(),
    ).toContain(
      resourceJson.spec.selector.matchLabels[
        Object.keys(resourceJson.spec.selector.matchLabels)[0]
      ],
    );

    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('更新策略').getText(),
    ).toContain(resourceJson.spec.strategy.type);

    expect(
      page.resourceBasicInfo.basicInfo
        .getElementByText('滚动更新策略')
        .getText(),
    ).toContain(resourceJson.spec.strategy.rollingUpdate.maxSurge);

    expect(
      page.resourceBasicInfo.basicInfo
        .getElementByText('最小准备时间')
        .getText(),
    ).toBe('0');
    expect(
      page.resourceBasicInfo.basicInfo
        .getElementByText('历史版本限制')
        .getText(),
    ).toBe('2');

    expect(page.detailPage.podList.getRowCount()).toBe(1);
    expect(page.detailPage.podList.getRow(rowkey).getText()).toContain(
      resourceJson.metadata.name,
    );
  });

  it('L1:AldK8S-51: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击日志tab--验证日志显示正确', () => {
    page.clickLeftNavByText('部署');
    page.namespace.select(namespace);

    page.resourceTable.getCell('状态', rowkey).then(elem => {
      return CommonPage.waitElementTextChangeTo(elem, '1 / 1');
    });
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    page.resourceBasicInfo.logviewer.getToolbarButton('查找').click();
    page.resourceBasicInfo.logviewer.waitLogDisplay();
    page.resourceBasicInfo.logviewer.finder_inputbox.input('hehe');
    page.resourceBasicInfo.logviewer.resultfinder.getText().then(result => {
      expect(result.includes('无结果')).toBeFalsy();
    });
  });

  it('L1:AldK8S-44: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称操作--选择更新标签--验证更新标签成功', () => {
    page.clickLeftNavByText('部署');
    // 定位到deployment 所在行，单击操作按钮，选择【更新标签】
    page.resourceTable.clickOperationButtonByRow(rowkey, '更新标签');

    page.resourcelabel.newValue(label_newkey, label_newvalue);
    page.resourcelabel.clickUpdate();
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证标签修改正确
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain(label_newvalue);
      });

    // 验证详情页基本信息页
    expect(
      page.resourceBasicInfo.basicInfo.getElementByText('名称').getText(),
    ).toBe(name);
  });
  it('L1:AldK8S-47: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击操作--选择扩缩容--验证更新成功', () => {
    page.operation.select('扩缩容');
    CommonPage.waitElementPresent(page.resourceScale.buttonConfirm);
    page.resourceScale.inputScale('2');
    page.resourceScale.clickConfirm();
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    browser.driver
      .wait(() => {
        page.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');
        page.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
        return page.detailPage.podList.getRowCount().then(count => {
          return count === 2;
        });
      }, 120000)
      .then(
        () => true,
        err => {
          console.warn('扩容失败: [' + err + ']');
          return false;
        },
      );
    // 验证注解修改正确
    expect(page.detailPage.podList.getRowCount()).toBe(2);
  });

  it('L3:AldK8S-52: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击事件 tab--验证事件显示正确', () => {
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('事件');

    // 验证事件的数量大于0
    page.detailPage.eventList.getRowCount().then(rowcount => {
      expect(rowcount).toBeGreaterThan(0);
    });
  });

  it('L1:AldK8S-50: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击配置管理 tab--验证配置管理显示正确', () => {
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');

    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    page.detailPage.configmapList.getCell('值', [configmap]).then(elem => {
      elem.getText().then(text => {
        expect(text).toContain(configmap);
      });
    });
  });

  it('L1:AldK8S-49: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击YAML tab--验证显示成功', () => {
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    page.detailPage.yamlEditor.getYamlValue().then(textyaml => {
      // 验证YAML 显示正确
      expect(CommonMethod.parseYaml(textyaml).metadata.name).toBe(name);
    });
  });

  it('L1:AldK8S-46: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击操作--选择更新Deployment--验证更新成功', () => {
    page.operation.select('更新');

    page.clickYAMLbutton();

    page.detailPage.yamlEditor.getYamlValue().then(ymalvalue => {
      page.detailPage.yamlEditor.setYamlValue(
        String(ymalvalue).replace(label_newkey, label),
      );
    });
    page.getButtonByText('更新').click();
    CommonPage.waitProgressbarNotDisplay();
    // 验证标签修改正确
    page.resourceBasicInfo.basicInfo
      .getElementByText('标签')
      .getText()
      .then(function(labelText) {
        expect(labelText).toContain(label);
      });
  });
  it('AldK8S-191:L1:单击左导航计算/部署--进入Deployment列表--点击已有的Deployment的操作--点击更新--通过UI更新--验证更新成功', () => {
    page.operation.select('更新');
    page.clickAddVolume();
    CommonPage.waitElementDisplay(addVolumePage.cancelButton);
    addVolumePage.addVolume('updatevolume', '主机路径', 'test/hostpath');
    page.getButtonByText('更新').click();
    CommonPage.clickLeftNavByText('部署');
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('YAML');
    // 验证修改成功
    page.detailPage.yamlEditor.getYamlValue().then(ymalvalue => {
      expect(CommonMethod.parseYaml(ymalvalue).metadata.name).toBe(name);
    });
  });

  it('L1:AldK8S-48: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--单击操作--选择删除--验证删除成功', () => {
    CommonPage.clickLeftNavByText('部署');
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.operation.select('删除');
    page.confirmDialog.clickConfirm();
    CommonPage.clickLeftNavByText('部署');
    page.searchBox.search(name);
    // 验证删除功能正确
    expect(page.resourceTable.noResult.getText()).toBe('无部署');
  });
});
