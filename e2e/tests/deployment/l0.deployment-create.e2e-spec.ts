/**
 * Created by liuwei on 2018/3/15.
 */
import { by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:部署列表页的创建功能', () => {
  const page = new DeploymentPage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;
  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    page.navigateTo();
    // 创建一个Deployment前先创建一个命名空间和一个配置文件
    this.praparetestdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.deployment-createprepare.e2e-spec',
    );
    // 准备deployment的测试数据
    this.testdata = CommonMethod.generateStringYaml(
      'deploymentcreate.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l0.deployment-create.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.praparetestdata);
    CommonKubectl.deleteResourceByYmal(this.testdata.get('name'));
  });

  it('L0:AldK8S-102: 单击左导航计算／部署 -- 进入Deployment 列表页--单击创建部署按钮--进入创建页--输入合法yaml, 单击创建按钮--验证创建成功', () => {
    page.getButtonByText('创建部署').click();

    // 切换到YAML创建
    page.clickYAMLbutton();

    page.yamlCreator.clickToolbarByName('清空');

    page.yamlCreator.setYamlValue(this.testdata.get('data'));
    page.yamlCreator.clickButtonCreate();
    // 跳转回列表页
    page.navigateTo();
    expect(page.getButtonByText('创建').isPresent()).toBeFalsy();

    page.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });
});
