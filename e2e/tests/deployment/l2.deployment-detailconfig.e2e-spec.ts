/**
 * Created by liuwei on 2018/3/15.
 */
import { $$, browser, by, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_deployments } from '../../utility/resource.type.k8s';

describe('L1:部署配置管理功能与回滚的验证', () => {
  const page = new DeploymentPage();
  const label_newkey = CommonMethod.random_generate_testData();
  const label_newvalue = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const configmap = name;
  const namespace = ServerConf.NAMESPACE;
  // tslint:disable-next-line:prefer-const
  let oldReplicaSet;

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    page.navigateTo();
    // 创建一个Deployment
    this.testdata = CommonKubectl.createResource(
      'deployment.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': name,
      },
      'l2.deployment-detailconfig.e2e-spec',
    );
    // 选择新创建的namespace
    page.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });
  it('AldK8S-261:L1:单击左导航计算/部署--进入部署列表--点击部署名称--进入详情页面--点击配置管理--点击环境变量后的蓝色笔--添加与引用环境变量--验证环境变量与引用的', () => {
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');
    page.clickBasicPencil('环境变量', $$('div.aui-card__header'));
    page
      .getAlaudaInputTable()
      .fillinTablenolabel([label_newkey, label_newvalue, '0']);
    page.clickconfirmButton();
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    page.detailPage.configmapList.getCell('名称', [label_newkey]).then(elem => {
      elem.getText().then(text => {
        expect(text).toBe(label_newkey);
      });
    });
    page.detailPage.configmapList.getCell('值', [label_newvalue]).then(elem => {
      elem.getText().then(text => {
        expect(text).toBe(label_newvalue);
      });
    });
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    page.detailPage.old_replicasetList
      .getRow([name])
      .$('a')
      .getText()
      .then(text => {
        this.oldReplicaSet = text;
      });
  });
  it('AldK8S-262:L1:单击左导航计算/部署进入部署列表--点击部署名称进入详情页面--点击配置管理--点击配置引用后的蓝色笔--添加配引用--验证引用成功', () => {
    page.navigateTo();
    page.resourceTable.clickResourceNameByRow([name]);
    CommonPage.waitProgressbarNotDisplay();
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');
    page.clickBasicPencil('配置引用', $$('div.aui-card__header'));
    page.getmuitiSelect().select(name);
    page.clickconfirmButton();
    browser.sleep(1000);
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    page.detailPage.envtag.getTagText().then(text => {
      expect(text).toEqual([`配置字典 ${name}`]);
    });
  });
  it(' AldK8S-392:L1:进入部署详情页--点击副本集后面的回滚至--弹出对比差别框--点击确认--验证deployment已回滚致选中的副本集版本', () => {
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    page.detailPage.old_replicasetList.clickOperationButtonByRow(
      [this.oldReplicaSet],
      '回滚至此版本',
    );
    page.rollbackDialog.clickConfirm();
    page.confirmDialog.clickConfirm();
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('基本信息');
    // 验证Pod 详情页【配置管理】表格部分，名称值显示正确
    page.resourceBasicInfo.basicInfo
      .getElementByText('副本集')
      .getText()
      .then(text => {
        expect(text).toBe(this.oldReplicaSet);
      });
  });
});
