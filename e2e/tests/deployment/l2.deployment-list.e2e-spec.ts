/**
 * Created by liuwei on 2018/3/15.
 */

import { ServerConf } from '../../config/serverConf';
import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';

describe('L2:部署列表页的显示，排序功能', () => {
  const page = new DeploymentPage();
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  const namespace = ServerConf.NAMESPACE;

  // 根据【名称，命名空间， 类型】唯一确定资源列表的一行
  const rowkey = [name];

  beforeAll(() => {
    page.navigateTo();
    // 创建一个Deployment
    this.testdata = CommonKubectl.createResource(
      'deployment.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'l2.deployment-list.e2e-spec',
    );
  });

  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('L2:AldK8S-100: 单击左导航计算／部署 -- 进入部署的资源列表页--验证列表默认按创建时间降序排序，可按名称、创建时间、命名空间 排序', () => {
    // 验证面包屑显示正确
    expect(page.breadcrumb.getText()).toEqual('计算/部署');

    // 默认按创建时间降序排列
    page.resourceTable.verifyDescending('创建时间');

    // 单击名称列，验证生序排列
    page.resourceTable.clickHeaderByName('名称');
    page.resourceTable.verifyAscending('名称');

    // 再次单击名称列，验证降序排列
    page.resourceTable.clickHeaderByName('名称');
    page.resourceTable.verifyDescending('名称');

    // 单击命名空间列，验证生序排列
    page.resourceTable.clickHeaderByName('命名空间');
    page.resourceTable.verifyAscending('命名空间');

    // 再次单击命名空间列，验证降序排列
    page.resourceTable.clickHeaderByName('命名空间');
    page.resourceTable.verifyDescending('命名空间');

    // 单击创建时间列，验证降序排列
    page.resourceTable.clickHeaderByName('创建时间');
    page.resourceTable.verifyDescending('创建时间');

    // 再次单击创建时间列，验证升序排列
    page.resourceTable.clickHeaderByName('创建时间');
    page.resourceTable.verifyAscending('创建时间');
  });

  it('L2:AldK8S-101: 单击左导航计算／部署 -- 进入Deployment 列表页--单击一个Deployment名称--进入详情页--验证功能菜单显示信息正确', () => {
    // 检索一个不存在的数据
    page.searchResource('AldK8S-47');
    // 验证列表为空时显示‘无部署’
    expect(page.resourceTable.noResult.getText()).toBe('无部署');
    // 清空检索框
    page.searchResource(name);

    // 验证表头显示正确 '名称', '命名空间', '状态', '镜像', '创建时间', ''
    page.resourceTable.getHeaderText().then(headerText => {
      expect(headerText).toEqual([
        '名称',
        '命名空间',
        '状态',
        '镜像',
        '已使用资源',
        '创建时间',
        '',
      ]);
    });

    // 点击条目右侧按钮，可弹出操作菜单 更新 “扩缩容” “更新标签” “查看日志” “更新标签“ ”更新注解“ “删除”
    page.resourceTable
      .clickOperationButtonByRow(rowkey, '更新标签')
      .then(text => {
        expect(text).toEqual([
          '更新',
          '扩缩容',
          '查看日志',
          '更新标签',
          '更新注解',
          '删除',
        ]);
      });

    // 关闭更新标签dialog 页面
    page.resourcelabel.clickCancel();

    // 单击名称进入详情页，验证详情页tab 显示正确 '基本信息', 'YAML', '配置字典', '日志', '事件'
    page.resourceTable.clickResourceNameByRow(rowkey);
    page.resourceBasicInfo.resourceInfoTab.getTabText().then(tabText => {
      expect(tabText).toEqual(['基本信息', 'YAML', '配置管理', '日志', '事件']);
    });

    // 单击操作按钮，验证操作菜单显示正确 '更新', '扩缩容', '删除'
    page.operation.select('扩缩容').then(text => {
      expect(text).toEqual(['更新', '扩缩容', '删除']);
    });

    // 关闭更新标签dialog 页面
    page.resourcelabel.clickCancel();
    // 验证详情页基本信息页
    page.resourceBasicInfo.basicInfo.getAllKeyText().then(testlist => {
      expect(CommonMethod.converNodeListTextToArray(testlist).sort()).toEqual(
        [
          '名称',
          '命名空间',
          '标签',
          '注解',
          '副本集',
          '选择器',
          '镜像',
          '更新策略',
          '滚动更新策略',
          '最小准备时间',
          '创建时间',
          '历史版本限制',
          '状态',
        ].sort(),
      );
    });
  });
});
