import { browser, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AddVolumePage } from '../../page_objects/computer/addvolumes.page';
import { DeploymentPage } from '../../page_objects/computer/deployment.page';
import { CommonApi } from '../../utility/common.api';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { CommonPage } from '../../utility/common.page';
import { k8s_type_namespaces } from '../../utility/resource.type.k8s';

describe('L0:部署创建功能通过UI', () => {
  const page = new DeploymentPage();
  const addVolumePage = new AddVolumePage();
  const namespace = ServerConf.NAMESPACE;
  const label = CommonMethod.random_generate_testData();
  const name = CommonMethod.random_generate_testData();
  beforeAll(() => {
    page.navigateTo();
    // 创建一个Deployment前先创建一个命名空间和一个配置文件
    this.testdata = CommonKubectl.createResource(
      'deploymentprapare.yaml',
      {
        '${NAME}': name,
        '${NAMESPACE}': namespace,
        '${LABEL}': label,
      },
      'L0.DeploymentCreate.e2e-spec',
    );
  });
  afterEach(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete Deployment ${name} -n ${namespace}`,
    );
  });
  afterAll(() => {
    // 删除测试数据
    CommonKubectl.deleteResourceByYmal(this.testdata);
  });

  it('AldK8S-189:L0: 单击左导航计算 / 部署 -- 进入deploment列表--单击创建部署 -- 点击右上角表单 -- 输入合法的参数 -- 点击创建 -- 验证创建成功', () => {
    page.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillincontainer(name, '1');

    page.fillinBasical(name, '1', 'RollingUpdate');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    page.resourceBasicInfo.basicInfo
      .getElementByText('更新策略')
      .getText()
      .then(text => {
        expect('RollingUpdate').toBe(text);
      });
    page.detailPage.podList.getRowCount().then(num => {
      expect(1).toBe(num);
    });
    // 进入列表页面
    page.navigateTo();
    // 等待列表中的数据加载
    page.resourceTable.getCell('名称', [name]).then(elem => {
      CommonPage.waitElementPresent(elem);
    });
    // 验证Deployment运行情况
    page.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });

  it('AldK8S-192:L1: 单击左导航计算 / 部署 -- 进入deployment列表 -- 单击创建部署 -- 填写命令echo 其他参数正确 -- 点击创建验证命令已执行', () => {
    page.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(name, '2', 'Recreate');
    page.fillincontainer(name, '1');
    page.fillinContainerAdvanced('', 'echo hello');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    page.resourceBasicInfo.basicInfo
      .getElementByText('更新策略')
      .getText()
      .then(text => {
        expect('Recreate').toBe(text);
      });
    page.detailPage.podList.getRowCount().then(num => {
      expect(2).toBe(num);
    });
    // 进入详情页日志页面
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('日志');
    // 点击查找
    page.resourceBasicInfo.logviewer.getToolbarButton('查找').click();
    // 等待echo hello显示
    page.resourceBasicInfo.logviewer.waitLogDisplay('echo hello');
    // 输入echo hello进行查找
    page.resourceBasicInfo.logviewer.finder_inputbox.input('echo hello');
    // 判断找到结果
    page.resourceBasicInfo.logviewer.resultfinder.getText().then(result => {
      expect(result.includes('无结果')).toBeFalsy();
    });
    page.navigateTo();
  });
  it('AldK8S-194:L1: 单击左导航计算/部署--进入deploment列表--单击创建部署--点击右上角表单--创建两个容器输入合法的参数--点击创建--验证创建成功', () => {
    page.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(name, '1', 'RollingUpdate');
    page.fillincontainer(name, '1');
    page.clickAddContaineButton();
    page.fillincontainer(name + '1', '1');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    expect(page.toastsuccess.getText()).toContain('创建成功');
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    // 进入详情页日志页面
    page.resourceBasicInfo.resourceInfoTab.clickTabByText('配置管理');
    page.containerTitle.getText().then(text => {
      expect(text).toEqual([`容器 ${name}`, `容器 ${name}1`]);
    });
    // 进入列表页面
    page.navigateTo();
    // 等待列表中的数据加载
    page.resourceTable.getCell('名称', [name]).then(elem => {
      CommonPage.waitElementPresent(elem);
    });
    // 验证Deployment运行情况
    page.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });

  it('AldK8S-195:L1: 单击左导航的计算/部署--进入deployment列表--单击创建部署--点击右上角表单--填写资源限制其他参数正确--点击创建--验证创建成功', () => {
    page.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    // UI创建Deployment
    page.fillinBasical(name, '1', 'RollingUpdate');
    // delete
    browser.sleep(10000);

    page.fillincontainer(name, '1', '1');
    page.clickCreateButton();
    // 验证创建成功后，有成功的toast 提示
    // expect(page.toastsuccess.message.isPresent()).toBeTruthy();
    expect(page.toastsuccess.getText()).toContain('创建成功');
    expect(page.getButtonByText('取消').isPresent()).toBeFalsy();
    // 进入列表页面
    page.navigateTo();
    // 等待列表中的数据加载
    page.resourceTable.getCell('名称', [name]).then(elem => {
      CommonPage.waitElementPresent(elem);
    });
    // 验证Deployment运行情况
    page.resourceTable.getCell('状态', [name]).then(elem => {
      CommonPage.waitElementTextChangeTo(elem, '1 / 1');
      expect(elem.getText()).toBe('1 / 1');
    });
  });
  it('ACP-190:L1:单击左导航计算/部署-进入deployment列表-单击创建部署-点击右上角表单-输入合法参数-点击右上角yaml-验证页面转换成yaml-点击取消-验证未创建', () => {
    page.getButtonByText('创建部署').click();
    CommonPage.waitProgressbarNotDisplay();
    page.fillinBasical('testUItoyaml', '3', 'RollingUpdate');
    page.clickAddVolume();
    addVolumePage.addVolume('testvolume', '配置字典', name);
    browser.sleep(100);
    page.fillinContainerAdvanced('testchangtest', 'echo hello');
    page.fillinVolumeMounts('testvolume', 'test/config', '/test/config');
    page.fillincontainer(name, '1');
    // 切换到YAML创建
    page.clickYAMLbutton();
    CommonPage.waitElementPresent(page.yamlCreator.toolbar);
    page.yamlCreator.getYamlValue().then(text => {
      expect(text).toContain('namespace: ' + namespace);
      expect(text).toContain('name: ' + 'testUItoyaml');
      expect(text).toContain('replicas: ' + '3');
      // FIXME: disable for now since AUI changes value change behaviour
      // expect(text).toContain('RollingUpdate');
      expect(text).toContain('testchangtest');
      expect(text).toContain('cpu: 50m');
      expect(text).toContain('memory: 50Mi');
      expect(text).toContain('mountPath: /test/config');
      expect(text).toContain('subPath: test/config');
      expect(text).toContain(`${ServerConf.IMAGE}`);
    });
    page.clickLeftText('部署');
    page.warningDialog.confirmTitle.getText().then(text => {
      expect(text).toBe('确定取消创建部署吗?');
    });
    page.warningDialog.clickConfirm();
  });
});
