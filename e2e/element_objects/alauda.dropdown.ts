/**
 * 下拉框列表控件
 * Created by liuwei on 2018/3/14.
 */
import { $$, browser, by, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaDropdown {
  private _iconselector;
  private _itemlistselector;
  private _auifromitem;
  private _auifromlabel;

  constructor(
    iconselector,
    itemlistselector,
    auifromitem: string = 'aui-form-item .aui-form-item',
    auifromlabel: string = 'aui-form-item .aui-form-item__label',
  ) {
    this._iconselector = iconselector;
    this._itemlistselector = itemlistselector;
    this._auifromitem = auifromitem;
    this._auifromlabel = auifromlabel;
  }

  /**
   * 单击下拉框， 选择值
   *
   * @parameter {selectvalue} 从下拉框中要选择的值
   */
  select(
    selectvalue,
    wait = () => {
      CommonPage.waitProgressbarNotDisplay();
      browser.sleep(100);
    },
  ) {
    const select = element(this._iconselector);
    select.click();
    // 等待要选择的元素出现
    CommonPage.waitElementPresent(
      element
        .all(this._itemlistselector)
        .filter(elem => {
          return elem.getText().then(function(text) {
            return text.indexOf(selectvalue) !== -1;
          });
        })
        .first(),
    );

    // 添加debug 日志信息
    const textList = element.all(this._itemlistselector).getText();

    element
      .all(this._itemlistselector)
      .filter(elem => {
        return elem.getText().then(function(text) {
          return text.indexOf(selectvalue) !== -1;
        });
      })
      .first()
      .click();
    wait();

    // 如果没找到元素，单击下icon, 关闭下拉框
    element
      .all(this._itemlistselector)
      .count()
      .then(count => {
        if (count > 0) {
          select.click();
        }
      });
    return textList;
  }

  /**
   * 页面有多个此空件时使用
   * @param leftlabel 左侧文字
   * @param name 想要选择的文字
   */
  selectinfrom(leftlabel: string, name: string) {
    const row = $$(this._auifromitem)
      .filter(elem => {
        return elem
          .$(`${this._auifromlabel}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftlabel;
          });
      })
      .first();
    // 根据行将元素点击
    row.$(`${this._iconselector}`).click();
    browser.sleep(500);
    // 选择一个选项
    CommonPage.waitElementPresent(element(by.css(this._itemlistselector)));
    $$(this._itemlistselector)
      .filter(elem => {
        return elem.getText().then(innerText => {
          return innerText.trim() === name;
        });
      })
      .first()
      .click();
    browser.sleep(100);

    $$(this._itemlistselector)
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          row.$(`${this._iconselector}`).click();
        }
      });
  }

  /**
   * 页面有多个此控件并且一行有多个时使用
   * @param leftlabel 左侧文字
   * @param name 想要选择的文字
   */
  selectsinfrom(leftlabel: string, name) {
    const row = $$(this._auifromitem)
      .filter(elem => {
        return elem
          .$(`${this._auifromlabel}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftlabel;
          });
      })
      .first();
    const itemlistselector = this._itemlistselector;
    row
      .$$('div.aui-form-item__content>aui-select')
      .each(function(elemout, index) {
        // 根据行将元素点击
        elemout.click();
        // 选择一个选项
        CommonPage.waitElementPresent($$(itemlistselector));
        $$(itemlistselector)
          .filter(eleminner => {
            return eleminner.getText().then(innerText => {
              return innerText.trim() === name[index];
            });
          })
          .first()
          .click();
        browser.sleep(1000);

        $$(itemlistselector)
          .isPresent()
          .then(isPresent => {
            if (isPresent) {
              elemout.click();
            }
          });
      });
  }
}
