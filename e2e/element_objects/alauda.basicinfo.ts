/**
 * 基本信息页面
 * Created by liuwei on 2018/3/15.
 */

import { browser, by, element } from 'protractor';

import { TableComponent } from '../page_objects/component/table.component';

export class AlaudaBasicInfo {
  private _title;
  private _aui_card_selector;
  private _aui_card_header_selector;
  private _alk_basic_info_field;
  private _alk_basic_info_field_label_selector;
  private _alk_basic_info_field_content_selector;

  private _aui_table_header_cell;
  private _aui_table_cell;
  private _aui_table_row;
  private _parent_aui_table;

  private _root_elem;

  /**
   *  构造函数
   * @param title 标题， 例如基本信息
   * @param aui_card_selector aui_card css 选择器
   * @param aui_card_header_selector aui_card_header css 选择器
   * @param alk_basic_info_field_label_selector alk_basic_info_field_label css 选择器
   * @param alk_basic_info_field_content_selector alk_basic_info_field_content css 选择器
   */
  constructor(
    title: string = '基本信息',
    aui_card_selector: string = 'aui-card',
    aui_card_header_selector: string = '.aui-card__header',
    alk_basic_info_field: string = '.alk-basic-info-field',
    alk_basic_info_field_label_selector: string = '.alk-basic-info-field__label',
    alk_basic_info_field_content_selector: string = '.alk-basic-info-field__content',
    aui_table_header_cell: string = 'aui-table-header-cell',
    aui_table_cell: string = 'aui-table-cell',
    aui_table_row: string = 'aui-table-row',
  ) {
    this._title = title;
    this._aui_card_selector = aui_card_selector;
    this._aui_card_header_selector = aui_card_header_selector;

    (this._alk_basic_info_field = alk_basic_info_field),
      (this._alk_basic_info_field_label_selector = alk_basic_info_field_label_selector);
    this._alk_basic_info_field_content_selector = alk_basic_info_field_content_selector;

    this._aui_table_header_cell = aui_table_header_cell;
    this._aui_table_cell = aui_table_cell;
    this._aui_table_row = aui_table_row;

    this._root_elem = element
      .all(by.css(this._aui_card_selector))
      .filter((elem, index) => {
        return elem
          .$(this._aui_card_header_selector)
          .getText()
          .then(text => {
            return text === this._title;
          });
      })
      .first();
  }

  /**
   * 在基本信息页面上， 根据key,获得值元素， 例如获得名称后边的元素（k8s_staging_uie2e_cluster），
   * 基本信息页面的例子：
           名称 : k8s_staging_uie2e_cluster          云服务 ： 私有的
    Docker 版本 : 1.12.6                     Docker 安装目录 ： /var/lib/docker
        创建时间 ： 2018-03-14 17:36:21
   * @param labeltext item_key 的显示文本，例如， 名称，云服务，Docker 版本，Docker 安装目录
   * @example getElementByText(名称).getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(labeltext) {
    // 找到labeltext 所在的行
    const tempElem = this._root_elem
      .$$(this._alk_basic_info_field)
      .filter((elem, index) => {
        return elem
          .$(this._alk_basic_info_field_label_selector)
          .getText()
          .then(text => {
            return text.trim() === labeltext;
          });
      })
      .first();

    return tempElem.$(this._alk_basic_info_field_content_selector);
  }

  /**
   * 获得基本信息的所有keys值，例如 ：[ '名称', '云服务', 'Docker 版本', 'Docker 安装目录', '创建时间' ]
   * @example getAllKeyText().then((text) => { console.log(text); })
   */
  getAllKeyText() {
    return this._root_elem
      .$$(this._alk_basic_info_field_label_selector)
      .getText();
  }

  /**
   * 获得基本信息页的title 元素
   */
  getTitleText() {
    return this._root_elem.$(this._aui_card_header_selector);
  }

  getTable(parent_aui_able: string): TableComponent {
    // 找到labeltext 所在的行
    return new TableComponent(
      parent_aui_able,
      this._aui_table_header_cell,
      this._aui_table_cell,
      this._aui_table_row,
    );
  }
}
