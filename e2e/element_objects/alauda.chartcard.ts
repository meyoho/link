/**
 * 概览页面card
 */

import {
  $,
  $$,
  ElementArrayFinder,
  ElementFinder,
  browser,
  by,
  element,
} from 'protractor';

import { AlaudaElementBase } from '../element_object_new/alauda.elementbase';

export class AlaudaChartCard extends AlaudaElementBase {
  private _root: ElementArrayFinder;
  private _cardSelector: ElementFinder;
  private _title: string;

  constructor(
    title: string,
    root: ElementArrayFinder = $$(' alk-overview-page .overview-chart-card'),
  ) {
    super();
    this._root = root;
    this._cardSelector = root
      .filter((elem, index) => {
        return elem
          .$('.overview-chart-card__title')
          .getText()
          .then(function(text) {
            return text.trim() === title;
          });
      })
      .first();
  }

  /**
   * 获得 card 元素
   * @example getRadioElementByText('主机节点').then((elem) => {#####})
   */
  getCardBytitle() {
    const allcard = this._root;
    const title = this._title;
    return allcard
      .filter((elem, index) => {
        return elem
          .$('.overview-chart-card__title')
          .getText()
          .then(function(text) {
            return text.trim() === title;
          });
      })
      .first();
  }

  /**
   * 获得 card 元素 个数
   */
  getCardCount() {
    return this._root.count();
  }

  /**
   * 获取CPU与内存card上的使用率元素
   */
  get usagePieChart() {
    return this._cardSelector.$$('text.link-text');
  }

  /**
   * 获取CPU与内存card上的'共 5 运行中 5 失败 0'等状态
   */
  get statusHint() {
    return this._cardSelector.$(
      'alk-status-gauge-bar .status-gauge-status-hint',
    );
  }

  /**
   * 获取CPU与内存card上的状态条
   */
  get statusContainer() {
    return this._cardSelector.$('alk-status-gauge-bar .status-gauge-container');
  }

  /**
   * 返回namespace个数
   */
  get namespaceNum() {
    return $('a.overview-chart-card__number-status');
  }

  /**
   * 返回当前的页面事件条数
   */
  geteventNum() {
    return $$('div.event-list-item').count();
  }

  get firstEvent(): ElementFinder {
    return $$('alk-overview-event-list a').first();
  }
}
