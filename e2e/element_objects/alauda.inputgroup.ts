/**
 * 文本框控件
 * Created by liuwei on 2018/2/22.
 */

import { $$, browser, element } from 'protractor';

export class AlaudaInputGroup {
  private _inputGroup_selector;
  private _inputGrouplabel_selector;
  private _inputGroupInput_selector;

  constructor(
    inputGroupselector = 'alk-resources-form .input-groups-wrapper',
    inputGrouplabelselector = 'alk-resources-form .input-groups-label',
    inputGroupInputselector = 'alk-resources-form .aui-input',
  ) {
    this._inputGroup_selector = inputGroupselector;
    this._inputGrouplabel_selector = inputGrouplabelselector;
    this._inputGroupInput_selector = inputGroupInputselector;
  }
  /**
   * 填写一组InputGroup
   * @param leftLabelText inputGroup左侧的文字
   * @param value 文本框想要输入的值 应该传入一组数组
   */
  sendValue(leftLabelText: string, value) {
    const content = $$(this._inputGroup_selector)
      .filter((elem, index) => {
        return elem
          .$(`${this._inputGrouplabel_selector}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftLabelText;
          });
      })
      .first();
    content.$$(this._inputGroupInput_selector).each((elem, index) => {
      elem.clear();
      elem.sendKeys(value[index]);
    });
  }
}
