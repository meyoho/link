/**
 * 基本信息页面
 * Created by liuwei on 2018/3/15.
 */

import { browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaTab {
  private _alltab_selector;

  /**
   * 构造函数
   * @param _alltab_selector 定位所有tab 的selector
   */
  constructor(alltab_selector) {
    this._alltab_selector = alltab_selector;
  }

  /**
   * 获得 tab 元素
   * @param tabtext tab 上的显示文本，例如：主机，事件
   * @example getTabElementByText('主机').getText().then((text) => {console.log(text)})
   */
  getTabElementByText(tabtext) {
    const alltab = element.all(this._alltab_selector);

    return alltab
      .filter((elem, index) => {
        return elem.getText().then(function(text) {
          return text.trim() === tabtext;
        });
      })
      .first();
  }

  /**
   * 单击tab
   * @param tabtext tab 上的显示文本，例如：主机，事件
   * @example clickTabByText('主机')
   */
  clickTabByText(
    tabtext,
    wait = () => {
      browser.sleep(100);
      CommonPage.waitProgressbarNotDisplay();
    },
  ) {
    this.getTabElementByText(tabtext).click();
    wait();
  }

  /**
   * 获得 tab 的所有文字显示
   * @example getTabElementByText().getText().then((text) => {console.log(text)})
   */
  getTabText() {
    return element.all(this._alltab_selector).getText();
  }
}
