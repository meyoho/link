/**
 * 按钮控件
 * Created by liuwei on 2018/2/22.
 */

import { browser, element } from 'protractor';

export class AlaudaButton {
  private _buttonSelector;

  constructor(selector) {
    this._buttonSelector = selector;
  }

  get button() {
    return element(this._buttonSelector);
  }

  /**
   * 单击按钮
   *
   */
  click() {
    this.button.click();
    return browser.sleep(100);
  }

  /**
   * 获得button 上的文字
   *
   */
  getText() {
    return this.button.getText().then(function(text) {
      return text;
    });
  }

  /**
   * 判断按钮是否出现
   */
  isPresent() {
    return this.button.isPresent();
  }
}
