/**
 * CheckBox 按钮控件
 * Created by liuwei on 2018/5/6.
 */

import { $$, browser, by, element } from 'protractor';

export class AlaudaCheckBox {
  private _checkBoxSelector;
  private _auiformitem;
  private _auiitmeLabel;
  private _checked;

  /**
   * 构造函数
   * @param innerText checkbox 右侧文字
   * @param checkBoxSelector checkbox 选择器
   */
  constructor(
    checkBoxSelector: string = 'aui-checkbox .aui-checkbox',
    auiitme = 'aui-form-item .aui-form-item',
    auiitmeLabel = '.aui-form-item__label',
    bechecked: string = 'div.aui-checkbox.isChecked',
  ) {
    this._checkBoxSelector = checkBoxSelector;
    this._auiformitem = auiitme;
    this._auiitmeLabel = auiitmeLabel;
    this._checked = bechecked;
  }

  /**
   * 是否选中
   */
  ifChecked(Labeltext) {
    return element(by.cssContainingText(this._checked, Labeltext)).isPresent();
  }
  /**
   * 返回所有被选中的复选框
   * @param leftLabelText 左侧的文字
   */
  allbechecked(leftLabelText: string) {
    const content = $$(this._auiformitem)
      .filter((elem, index) => {
        return elem
          .$(`${this._auiitmeLabel}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftLabelText;
          });
      })
      .first();
    return content.$$(this._checked);
  }
  /**
   * 选中复选框
   */
  check(text) {
    if (text !== '') {
      this.ifChecked(text).then(ispresent => {
        if (!ispresent) {
          element(by.css(`${this._checkBoxSelector}`)).click();
        }
      });
    }
  }
  /**
   * 取消选中复选框
   */
  uncheck(Labeltext) {
    this.ifChecked(Labeltext).then(ispresent => {
      if (ispresent) {
        element(by.css(`${this._checkBoxSelector}`)).click();
      }
    });
  }

  /**
   * 在表格中多个checkbox
   * @param leftLabelText 左侧的文字
   * @param name radio名称
   */
  checkInform(leftLabelText, name = ['']) {
    const content = $$(this._auiformitem)
      .filter((elem, index) => {
        return elem
          .$(`${this._auiitmeLabel}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftLabelText;
          });
      })
      .first();
    const checkBoxSelector = this._checkBoxSelector;
    name.filter(function(value, index) {
      if (name[index] !== '') {
        content
          .element(by.cssContainingText(checkBoxSelector, name[index]))
          .click();
      }
    });
  }
}
