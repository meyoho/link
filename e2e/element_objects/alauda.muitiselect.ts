import { $$, ElementFinder, browser, by, element, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class MuitiSelect {
  private _icon;
  private _inputbox;
  private _itemlistselector;
  private _auifromitem;
  private _auifromlabel;

  constructor(
    icon: string = '.aui-multi-select__suffix',
    inputbox: string = 'aui-tag',
    itemlistselector: string = 'aui-option-group .aui-option-group__content .aui-option',
    auifromitem: string = 'aui-form-item .aui-form-item',
    auifromlabel: string = 'aui-form-item .aui-form-item__label',
  ) {
    this._icon = icon;
    this._inputbox = inputbox;
    this._itemlistselector = itemlistselector;
    this._auifromitem = auifromitem;
    this._auifromlabel = auifromlabel;
  }
  /**
   * 单击 aui-select, 选择item 再次单select收起下拉列表
   * @param text 要选择的item
   */
  select(text: string) {
    $$(this._icon).click();
    browser.sleep(100);
    const selecteditem = $$(this._itemlistselector)
      .filter(elem => {
        return elem.getText().then(innerText => {
          return innerText.trim() === text;
        });
      })
      .first();
    CommonPage.waitElementPresent(selecteditem);
    selecteditem.click();
    browser.sleep(100);
    $$(this._icon).click();
  }
  /**
   * 页面有多个此空件时使用
   * @param leftlabel 左侧文字
   * @param name 想要选择的文字
   */
  selectinfrom(leftlabel: string, name: string) {
    const row = $$(this._auifromitem)
      .filter((elem, index) => {
        return elem
          .$(`${this._auifromlabel}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftlabel;
          });
      })
      .first();
    row.$(`${this._icon}`).click();
    CommonPage.waitElementPresent(element(by.css(this._itemlistselector)));
    const selecteditem = $$(this._itemlistselector)
      .filter(elem => {
        return elem.getText().then(innerText => {
          return innerText.trim() === name;
        });
      })
      .first()
      .click();
    browser.sleep(100);
    row.$(`${this._icon}`).click();
  }
}
