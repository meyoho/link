/**
 * Created by liuwei on 2018/2/27.
 *
 */
import { $$, browser, by, element } from 'protractor';

import { CommonMethod } from '../utility/common.method';
import { CommonPage } from '../utility/common.page';

interface HeaderCell {
  index: number;
  name: string;
}

export class AlaudaTable {
  private _alauda_header_cell_selector;
  private _alauda_body_cell_selector;
  private _alauda_body_row_selector;

  constructor(
    header_cell_selector: string = '.aui-table__header-cell',
    body_cell_selector: string = '.aui-table__cell',
    body_row_selector: string = '.aui-table__row',
  ) {
    this._alauda_header_cell_selector = header_cell_selector;
    this._alauda_body_cell_selector = body_cell_selector;
    this._alauda_body_row_selector = body_row_selector;
  }

  /**
   * 根据列名获得表格共几列，{name}列在第几列
   *
   * @parameter {name} 列名， string 类型
   */
  _getColumeIndexByColumeName(name) {
    const headerCell = $$(this._alauda_header_cell_selector).map(
      (elem, index) => {
        return {
          index: index,
          name: elem.getText(),
        };
      },
    );
    // 遍历表头，返回{name}列在第几列，共几列
    return headerCell.then(headerCellList => {
      let [columeIndex] = [0];
      headerCellList.forEach((item: HeaderCell) => {
        if (item.name === name) {
          columeIndex = item.index;
          return;
        }
      });
      return { columeCount: headerCellList.length, columeIndex: columeIndex };
    });
  }

  /**
   * 获得表格的表头所有列名
   *
   */
  getHeaderText() {
    return $$(`${this._alauda_header_cell_selector}`).getText();
  }

  /**
   * 根据列名获得表头的单元格元素
   *
   * @parameter {name} 表格头的单元格显示的内容文本， string 类型
   */
  getHeaderCellByName(name) {
    return $$(this._alauda_header_cell_selector)
      .filter(elem => {
        return elem.getText().then(text => {
          return text === name;
        });
      })
      .first();
  }

  /**
   * 单击表头，等待表格数据加载完毕
   *
   * @parameter {name} 表格头的单元格显示的内容文本， string 类型
   */
  clickHeaderByName(name) {
    this.getHeaderCellByName(name).click();
    // console.log('单击表头 : ' + name);
    return browser.sleep(100);
  }

  /**
   * 根据列名获得该列的所有值
   *
   * @parameter {name} 表格的列名， string 类型
   */
  getColumeTextByName(name) {
    const body_cell_selector = this._alauda_body_cell_selector;
    return this._getColumeIndexByColumeName(name).then(table => {
      return $$(body_cell_selector)
        .filter((elem, index) => {
          return elem.getText().then(text => {
            return index % table.columeCount === table.columeIndex;
          });
        })
        .getText();
    });
  }

  /**
   * 根据关键字获得一行
   * @param keys 数组列型，根据keys里面的元素可以唯一确定一行
   * @return {ElementFinder}
   * @example getRow([secret_1,namespace2241_1,'Secret']).element(by.css('.aui-table__cell button')).click()
   */
  getRow(keys) {
    const rowlist = element.all(by.css(this._alauda_body_row_selector));
    // 根据关键字找到唯一行
    return rowlist
      .filter((elem, index) => {
        return elem.getText().then(text => {
          return keys.every((key: any) => text.includes(key));
        });
      })
      .first();
  }

  /**
   * 根据列名和关键字获得一个单元格
   *
   * @parameter {name} 列名
   * @parameter {keys} 数组列型，根据keys里面的元素可以唯一确定一行
   *
   * @example
   *
   * getCell('名称', [namespace1, 'Namespace']).then(function (cell) {
      expect(cell.element(by.css('.aui-table__cell a')).getText()).toBe(namespace1);
    });

   * @return 返回 {keys} 找到的唯一行，所在{name} 列的单元格
   */
  getCell(name, keys) {
    const body_cell_selector = this._alauda_body_cell_selector;
    CommonPage.waitElementPresent(this.getRow(keys));
    // 根据关键字找到唯一行
    const row = this.getRow(keys);
    // 根据列名，返回第几列单元格
    return this._getColumeIndexByColumeName(name).then(colume => {
      return row.$$(body_cell_selector).get(colume.columeIndex);
    });
  }

  /**
   * 获取table 有多少行
   *
   *
   * @example getRowCount().then(function(rowCount) {
      console.log('Row count is : ' + rowCount)
    })

   * @return 返回表格有多少行
   */
  getRowCount() {
    const rowlist = $$(this._alauda_body_row_selector);
    return rowlist.count().then(function(rowCount) {
      return rowCount;
    });
  }

  /**
   * 返回多少列
   * @return {wdpromise.Promise<number>}
   */
  getColumeCount() {
    return $$(this._alauda_header_cell_selector).count();
  }

  /**
   * 仅适用于列表的某个列的显示文本的生序排序的验证
   *
   * @parameter {columeName} 列名
   *
   * @example verifyAscending('名称')
   * @return void
   */
  verifyAscending(columeName) {
    const resourceTable = this.getColumeTextByName(columeName);
    resourceTable.then((columeTextlist: any) => {
      // 复制一个新数组
      const expect1: any = columeTextlist.slice();
      expect(expect1).toEqual(columeTextlist.sort());
    });
  }

  /**
   * 仅适用于列表的某个列的显示文本的降序排序的验证
   *
   * @parameter {columeName} 列名
   *
   * @example verifyAscending('名称')
   * @return void
   */
  verifyDescending(columeName) {
    const resourceTable = this.getColumeTextByName(columeName);
    resourceTable.then((columeTextlist: any) => {
      // 复制一个新数组
      const expect1 = columeTextlist.slice();
      expect(expect1).toEqual(columeTextlist.sort(CommonMethod.stringDown));
    });
  }
}
