/**
 * 下拉框列表控件
 * Created by liuwei on 2018/7/18.
 */
import { $$, ElementFinder, browser, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AuiSelect {
  private _icon;
  private _inputbox;
  private _itemlistselector;

  constructor(
    icon: ElementFinder,
    inputbox: ElementFinder,
    itemlistselector: string = '.aui-option-container .aui-option',
  ) {
    this._icon = icon;
    this._inputbox = inputbox;
    this._itemlistselector = itemlistselector;
  }

  /**
   * 在文本框中输入值
   * @param text 文本框中要输入的值
   */
  input(text: string) {
    this._inputbox.clear();
    this._inputbox.sendKeys(text);
    browser.sleep(100);
  }

  /**
   * 在文本框中输入值
   * @param text 文本框中要输入的值
   */
  getText(): promise.Promise<string> {
    return this._inputbox.getAttribute('value');
  }

  /**
   * 单击 aui-select, 选择item
   * @param text 要选择的item
   */
  select(text: string) {
    this._icon.click();
    browser.sleep(100);
    const selecteditem = $$(this._itemlistselector)
      .filter(elem => {
        return elem.getText().then(innerText => {
          return innerText.trim() === text;
        });
      })
      .first();
    CommonPage.waitElementPresent(selecteditem);
    selecteditem.click();
    browser.sleep(100);
    selecteditem.isPresent().then(bo => {
      if (bo) {
        this._icon.click();
      }
    });
  }
}
