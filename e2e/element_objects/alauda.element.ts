/**
 * Created by liuwei on 2018/3/30.
 */

import { $, $$, browser, by, element } from 'protractor';

export class AlaudaElement {
  private _parent_css_selector;
  private _label_css_selector;
  private _control_css_selector;

  /**
   * 根据左侧文字获得右侧元素
   * @param parent_selector  父节点的 css
   * @param label_css_selector 左侧label 的css
   * @param content_css_selector 右侧content 的css
   */
  constructor(
    parent_selector: string = '*[class*=field]',
    label_css_selector: string = '*[class*=label]',
    content_css_selector: string = '*[class*=content]',
  ) {
    this._parent_css_selector = parent_selector;
    this._label_css_selector = label_css_selector;
    this._control_css_selector = content_css_selector;
  }

  /**
   * 页面上根据左侧的文字，查找右侧控件
   * @param leftLabelText 左侧文字
   * @param tagnme 右侧控件tagname
   */
  getElementByText(leftLabelText: string, tagnme: string = 'input') {
    const content = $$(this._parent_css_selector)
      .filter((elem, index) => {
        return elem
          .$(`${this._label_css_selector}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftLabelText;
          });
      })
      .first();
    return content.$(tagnme);
  }
}
