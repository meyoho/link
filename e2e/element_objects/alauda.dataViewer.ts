/**
 * DataViewer 控件
 * Created by liuwei on 2018/5/4.
 */

import { $$, ElementFinder, browser, by, element, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaDataViewer {
  private _dataKeySelector;
  private _dataSectionSelector;
  private _sectionHeaderSelector;
  private _dataSelector;

  /**
   * dataViewer 的构造函数
   * @param dataKeySelector 所有dataKey 的选择器
   * @param dataSectionSelector 所有data(包含header, data 2 个部分) 的选择器
   * @param sectionHeaderSelector 所有dataHeader 的选择器
   * @param dataSelector 所有data 的选择器
   */
  constructor(
    dataKeySelector: string = 'div[class*=data-keys__label-key]',
    dataSectionSelector: string = 'div[class*=data-values]>div[class*=-data-section]',
    sectionHeaderSelector: string = 'div[class*=section__header]',
    dataSelector: string = `pre`,
  ) {
    this._dataKeySelector = dataKeySelector;
    this._dataSectionSelector = dataSectionSelector;
    this._sectionHeaderSelector = sectionHeaderSelector;
    this._dataSelector = dataSelector;
  }

  /**
   * 获得所有数据的key
   */
  getAll_dataKeys(): promise.Promise<string> {
    return $$(this._dataKeySelector).getText();
  }

  /**
   * 获得所有数据的headerText
   */
  getAll_dataHeaders(): promise.Promise<string> {
    return $$(this._sectionHeaderSelector).getText();
  }

  /**
   * 展开 header
   * @param headerText header 里显示的文字
   */
  extend_header(headerText: string): promise.Promise<void> {
    element(by.cssContainingText(this._dataKeySelector, headerText)).click();

    browser.driver
      .wait(() => {
        element(
          by.cssContainingText(this._dataSectionSelector, headerText),
        ).click();
        return element(
          by.cssContainingText(this._dataSectionSelector, headerText),
        )
          .$(this._dataSelector)
          .isPresent()
          .then(isPresent => isPresent);
      }, 30000)
      .then(
        () => true,
        err => {
          console.warn('wait error [' + err + ']');
          return false;
        },
      );

    return browser.sleep(100);
  }

  /**
   * 根据headerText 获得下面的数据文本
   * @param headerText 数据的headerText
   */
  get_dataByHeaderText(headerText: string): ElementFinder {
    const elem = element(
      by.cssContainingText(this._dataSectionSelector, headerText),
    );
    CommonPage.waitElementPresent(elem.$(this._dataSelector));
    return elem.$(this._dataSelector);
  }
}
