/**
 * 编辑YAML 的控件
 * Created by liuwei on 2018/2/22.
 */
import { browser, by, element } from 'protractor';

export class AlaudaYamlEditor {
  private _rootSelector;
  private _toolbarselector;
  private _code_editor_selector;

  /**
   * 编辑器
   *
   * @parameter {rootSelector} 包含yanl 编辑器的父节点 selector
   * @parameter {toolbarselector} string 类型 code 编辑器上面的 toolbar css
   *
   */

  /**
   * yaml 编辑器
   * @param rootSelector 包含yanl 编辑器的父节点 selector
   * @param toolbarselector string 类型 code 编辑器上面的 所有toolbar按钮的 css
   * @param codeEditor_selector string 类型 code 编辑器的 css
   */
  constructor(
    rootSelector = by.css('.layout-page-content'),
    toolbarselector = '.aui-code-editor-toolbar__control-button',
    codeEditor_selector = '.aui-code-editor',
  ) {
    this._rootSelector = rootSelector;
    this._toolbarselector = toolbarselector;
    this._code_editor_selector = codeEditor_selector;
  }

  get rootElement() {
    return element(this._rootSelector);
  }

  /**
   * 编辑器上面的toolbar,
   *
   * @return element list 对象, 获得toolbar 上的所有按钮
   */
  get toolbar() {
    return this.rootElement.$$(this._toolbarselector);
  }

  /**
   * 判断编辑器的模式，日间 or 夜间
   *
   * @return true 表示夜间，false 表示日间
   * @example  isDayMode.then((isNight) => {
   *               console.log('==== is night mode : ' + String(isNight))
   *           })
   */
  get isNightMode() {
    return this.rootElement
      .$(this._code_editor_selector)
      .getAttribute('class')
      .then(css => {
        return css.includes('--dark');
      });
  }

  /**
   * 单击 toolbar 上按钮
   * @param name toolbar 上显示的按钮文字
   */
  clickToolbarByName(name) {
    this.toolbar
      .filter(function(elem, index) {
        return elem.getText().then(function(text) {
          return text === name;
        });
      })
      .first()
      .click();
    return browser.sleep(100);
  }

  /**
   * 获取编辑器里面的yaml 值
   * 注意: 这里的javascript 只使用于发行版的项目，如果移到其它项目需要重写这个javascript
   *
   * @return Promise 对象
   * @example   getYamlValue().then((yaml) => {
   *                console.log(yaml)
   *            })
   */
  getYamlValue() {
    const queryscript =
      'return monaco.editor.getModels().find(' +
      'model => model.id === document.querySelector("aui-code-editor [model-id]").attributes["model-id"].value).getValue()';
    return browser.executeScript(queryscript);
  }

  /**
   * 编辑器里面的输入 yaml 值
   * 注意: 这里的javascript 只使用于发行版的项目，如果移到其它项目需要重写这个javascript
   *
   * @parameter {stringYaml} yaml 的值
   * @example let yamlstring = CommonMethod.readyamlfile('configmap.yaml', { '$NAME' : 'liuwei', '$NAMESPACE' : 'dddddd'});
   *          setYamlValue(yamlstring);
   *
   */
  setYamlValue(stringYaml) {
    const setscript =
      'return monaco.editor.getModels().find(model => model.id === ' +
      'document.querySelector("aui-code-editor [model-id]").attributes["model-id"].value).setValue(`' +
      stringYaml +
      '`)';
    browser.executeScript(setscript);
    return browser.sleep(100);
  }
}
