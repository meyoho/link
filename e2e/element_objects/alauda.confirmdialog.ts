/**
 * 确认对话框，包含确认提示内容，取消按钮，确认按钮
 * Created by liuwei on 2018/2/22.
 */

import { browser, by, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AlaudaButton } from './alauda.button';

export class AlaudaConfirmDialog {
  private _root_selector;
  private _confirm_title;
  private _confirm_ok;
  private _confirm_cancel;

  constructor(
    root_selector = by.css('aui-confirm-dialog'),
    confirm_title = '.aui-confirm-dialog__title',
    confirm_ok = '.aui-button--primary',
    confirm_cancel = '.aui-button--default',
  ) {
    this._root_selector = root_selector;
    this._confirm_title = confirm_title;
    this._confirm_ok = confirm_ok;
    this._confirm_cancel = confirm_cancel;
  }

  /**
   * 确认对话框的提示信息
   */
  get confirmTitle() {
    return element(this._root_selector).$(this._confirm_title);
  }

  /**
   * 确认对话框的提示信息
   */
  get confirmMassage() {
    return element(this._root_selector).$('.aui-confirm-dialog__content');
  }

  /**
   * 确认对话框的确认按钮
   */
  get buttonConfirm() {
    return element(this._root_selector).$(this._confirm_ok);
  }

  /**
   * 确认对话框的取消按钮
   */
  get buttonCancel() {
    return element(this._root_selector).$(this._confirm_cancel);
  }

  /**
   * 单击确认按钮
   *
   */
  clickConfirm() {
    CommonPage.waitElementPresent(this.buttonConfirm);
    this.buttonConfirm.click();
    CommonPage.waitToastNotPresent();
    CommonPage.waitProgressbarNotDisplay();
    browser.sleep(100);
  }

  /**
   * 单击取消按钮
   *
   */
  clickCancel() {
    CommonPage.waitElementPresent(this.buttonCancel);
    this.buttonCancel.click();
  }
}
