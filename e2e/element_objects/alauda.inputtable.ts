import { $, $$, ElementArrayFinder, browser, by, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AuiSelect } from './alauda.auiSelect';
export class AlaudaInputTable {
  private _trXpath;
  private _buttonSelector;
  private _tdselector;
  private _parent_css_selector;
  private _label_css_selector;
  private _trselector;
  private _th_selector;
  // table button
  // table th
  // table td input
  // //div[@class="bottom-control-buttons"]/../../preceding-sibling::tr[2]
  constructor(
    table_button_selector: string = 'table button',
    tdselector: string = 'table td',
    table_tr_xpath: string = '//div[@class="alk-form-table__bottom-control-buttons"]/../../preceding-sibling::tr[2]',
    parent_selector: string = 'aui-form-item .aui-form-item',
    label_css_selector: string = '.aui-form-item__label',
    trselector: string = 'table tr',
    thselector: string = 'table th',
  ) {
    this._trXpath = table_tr_xpath;
    this._buttonSelector = table_button_selector;
    this._parent_css_selector = parent_selector;
    this._label_css_selector = label_css_selector;
    this._tdselector = tdselector;
    this._trselector = trselector;
    this._th_selector = thselector;
  }
  /**
   * 用于没有左侧的标题的此类控件
   */
  fillinTablenolabel(value) {
    $(this._buttonSelector).click();
    element(by.xpath(this._trXpath))
      .all(by.tagName('td'))
      .each((elem, index) => {
        elem
          .$(':nth-child(1)')
          .getTagName()
          .then(text => {
            this.fillin(elem, value[index], text);
          });
      });
  }

  /**
   * 此方法用于页面上有多个此控件时使用。填写表格式的列，若是按钮时则需传一个‘1’或者'0',‘1’为点击 ‘0’不操作。
   * @param leftLabelText 左侧文字
   * @param value 要填写的数据
   */
  fillinTable(leftLabelText, value) {
    // .getTagName() 获取 tag的类型
    const content = $$(this._parent_css_selector)
      .filter((elem, index) => {
        return elem
          .$(`${this._label_css_selector}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftLabelText;
          });
      })
      .first();
    content.$$(this._tdselector).each((elem, index) => {
      elem
        .$(':nth-child(1)')
        .getTagName()
        .then(text => {
          this.fillin(elem, value[index], text);
        });
    });
  }

  /**
   * 此方法用于页面上有多个此控件时使用。填写表格式的列，若是按钮时则需传一个‘1’或者'0',‘1’为点击 ‘0’不操作。
   * @param leftLabelText 左侧文字
   * @param value 要填写的数据
   */
  addandfillinTable(leftLabelText, value = []) {
    // .getTagName() 获取 tag的类型
    if (value.length !== 0) {
      const content = $$(this._parent_css_selector)
        .filter((elem, index) => {
          return elem
            .$(`${this._label_css_selector}`)
            .getText()
            .then(text => {
              return text.replace('*\n', '').trim() === leftLabelText;
            });
        })
        .first();
      content.$(this._buttonSelector).click();
      content.$$(this._tdselector).each((elem, index) => {
        elem
          .$(':nth-child(1)')
          .getTagName()
          .then(text => {
            this.fillin(elem, value[index], text);
          });
      });
    }
  }
  /**
   * 根据type传入的名称填写表格一行的元素
   * @param elem 表格列的定位
   * @param value 想要填写的数据
   * @param type 列中输入的类型,根据getTagName所获得
   */
  fillin(elem, value, type) {
    if (type === 'input' || type === 'textarea') {
      if (value !== '') {
        elem.$(type).clear();
        elem.$(type).sendKeys(value);
      }
    } else if (type === 'aui-select') {
      if (value !== '') {
        const select = new AuiSelect(elem.$(type), elem.$('input'));
        select.select(value);
      }
    } else {
      if (value === '1') {
        elem.$(type).click();
      }
    }
  }
  /**
   * 此方法用于页面上有多个此控件时使用。填写表格式的列，若是按钮时则需传一个‘1’或者'0',‘1’为点击 ‘0’不操作。
   * @param leftLabelText 左侧文字
   * @param value 要填写的数据
   */
  clickAddButton(leftLabelText) {
    // .getTagName() 获取 tag的类型
    const content = $$(this._parent_css_selector)
      .filter((elem, index) => {
        return elem
          .$(`${this._label_css_selector}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftLabelText;
          });
      })
      .first();
    content.$(this._buttonSelector).click();
  }

  /**
   * 获得最后一行,并点击delete按钮
   */
  clickDelete() {
    element(by.xpath(this._trXpath))
      .$('.alk-form-table__action-col svg')
      .click();
  }
  /**
   * 根据关键字获得一行
   * @param keys 数组列型，根据keys里面的元素可以唯一确定一行
   * @return {ElementFinder}
   * @example getRow([secret_1,namespace2241_1,'Secret']).element(by.css('.aui-table__cell button')).click()
   */
  getRowText(keys) {
    const rowlist = element
      .all(by.css(this._trselector))
      .filter((elem, index) => {
        return elem.getText().then(text => {
          return keys.every((key: any) => text.includes(key));
        });
      })
      .first();
    return rowlist.getText();
  }
}
