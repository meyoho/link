/**
 *  单选框控件
 * Created by changdongmei on 2018/4/12.
 */

import { $, $$, browser, by, element } from 'protractor';

export class AlaudaRadio {
  private _radioinputSelector;
  private _auiformitem;
  private _auiitmeLabel;

  constructor(
    radio_item_selector = 'aui-radio .aui-radio',
    auiitme = 'aui-form-item .aui-form-item',
    auiitmeLabel = '.aui-form-item__label',
  ) {
    this._radioinputSelector = radio_item_selector;
    this._auiformitem = auiitme;
    this._auiitmeLabel = auiitmeLabel;
  }
  /**
   * 返回一个 radio
   */
  get radio() {
    return $(this._radioinputSelector);
  }
  /**
   * 获取单选框
   * @parameter {name}  单选框后面的文字， string 类型
   */
  getRadioByName(name) {
    return $$(this._radioinputSelector)
      .filter(elem => {
        return elem
          .$('span')
          .getText()
          .then(text => {
            return text === name;
          });
      })
      .first();
  }
  /**
   * 在表格中多个radio
   * @param leftLabelText 左侧的文字
   * @param name radio名称
   */
  getRadioInform(leftLabelText, name) {
    const content = $$(this._auiformitem)
      .filter((elem, index) => {
        return elem
          .$(`${this._auiitmeLabel}`)
          .getText()
          .then(text => {
            return text.replace('*\n', '').trim() === leftLabelText;
          });
      })
      .first();
    content
      .$$(this._radioinputSelector)
      .filter(elem => {
        return elem
          .$('span')
          .getText()
          .then(text => {
            return text === name;
          });
      })
      .first()
      .click();
  }
  /**
   * 获取单选框
   * @parameter {name}  单选框后面的文字， string 类型
   */
  getSelectbyName(name) {
    return $$(this._radioinputSelector)
      .filter(elem => {
        return elem.getText().then(text => {
          return text === name;
        });
      })
      .first();
  }

  /**
   * 根据文本返回是否被选中
   * @param name 后的文字
   */
  isChecked() {
    return element(by.css('aui-radio-button .isChecked'));
  }

  check(name) {
    this.getRadioByName(name).click();
  }
}
