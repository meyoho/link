/**
 * 检索框控件，由检索框和检索按钮组成
 * Created by liuwei on 2018/2/22.
 */
import { browser, protractor } from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AlaudaButton } from './alauda.button';
import { AlaudaInputbox } from './alauda.inputbox';

export class AlaudaSearchBox {
  public alauda_inputBox;
  public alauda_buttonSearch;

  constructor(inputboxselector, iconselector) {
    this.alauda_inputBox = new AlaudaInputbox(inputboxselector);
    this.alauda_buttonSearch = new AlaudaButton(iconselector);
  }

  /**
   * 在检索框中输入要检索的内容，单击检索按钮查询结果
   *
   * @parameter {value} 要检索的内容
   */
  search(value) {
    this.alauda_inputBox.input(value);
    this.alauda_buttonSearch.click();
    CommonPage.waitProgressbarNotDisplay();
    return browser.sleep(100);
  }

  /**
   * 在检索框中输入要检索的内容，按回车键查询结果
   *
   * @parameter {value} 要检索的内容
   */
  searchByPressEnter(value) {
    this.alauda_inputBox.input(value);
    this.alauda_inputBox.sendKeys(protractor.Key.ENTER);
    return browser.sleep(100);
  }
}
