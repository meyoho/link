import { $, $$, ElementFinder, browser, by, element } from 'protractor';

import { AuiSelect } from '../element_objects/alauda.auiSelect';
import { CommonPage } from '../utility/common.page';

import { AlaudaElementBase } from './alauda.elementbase';
export class AlaudaPaginator extends AlaudaElementBase {
  private _paginator_selector;
  private select;
  /**
   * 构造函数
   * @param _paginator_selector 定位所有Paginator 的selector
   */
  constructor(
    paginator_selector: ElementFinder = $('aui-paginator .aui-paginator'),
  ) {
    super();
    this._paginator_selector = paginator_selector;
    this.select = new AuiSelect(
      this._paginator_selector.$('.aui-select__label-container'),
      $('aui-select__input'),
    );
  }

  /**
   * 获得 当前页面的所有记录条数
   * @returns string类型
   */
  get totalNum() {
    let totalnum: string;
    this._paginator_selector
      .$('.aui-paginator__total')
      .getText()
      .then(text => {
        totalnum = text.substring(text.indexOf('共') + 1, text.indexOf('条'));
      });
    return browser.sleep(100).then(() => {
      return totalnum;
    });
  }

  selectTotalofpage(
    num: string,
    wait = () => {
      browser.sleep(100);
      CommonPage.waitProgressbarNotDisplay();
    },
  ) {
    this._paginator_selector.$('.aui-icon-caret_down_s').click();
    browser.sleep(100);
    const selecteditem = $$('.aui-option-container .aui-option')
      .filter(elem => {
        return elem.getText().then(innerText => {
          return innerText.trim() === num;
        });
      })
      .first();
    CommonPage.waitElementPresent(selecteditem);
    selecteditem.click();
    wait();
    selecteditem.isPresent().then(bo => {
      if (bo) {
        this._paginator_selector.$('.aui-select__label-container').click();
      }
    });
    wait();
  }

  /**
   * 单击Paginator
   * @param pagetext Paginator 上的显示文本，例如：主机，事件
   * @example clickPageByText(‘4’) 点击第4️页
   */
  clickPageByText(
    pagetext: string,
    wait = () => {
      browser.sleep(100);
      CommonPage.waitProgressbarNotDisplay();
    },
  ) {
    const paginatorpage = this._paginator_selector
      .$$('.aui-paginator__page')
      .filter((elem, index) => {
        return elem.getText().then(function(text) {
          return text.trim() === pagetext;
        });
      })
      .first();
    paginatorpage.click();
    wait();
  }

  /**
   * 共有几页
   */
  get pageNum() {
    return $('button.aui-paginator__page:nth-last-child(2)').getText();
  }

  /**
   * 返回当前选中页码
   */
  get selectedPagenum() {
    return $('button.aui-paginator__page.aui-button--primary').getText();
  }

  /**
   * 返回当前页面的总条数
   */
  get pageSize() {
    return this._paginator_selector.$('.aui-select__label').getText();
  }

  /**
   * 返回当前页面的总条数
   */
  get nextbutton() {
    return $('.aui-paginator__navigator:last-child');
  }

  /**
   * 返回当前页面的总条数
   */
  get frontbutton() {
    return $('.aui-paginator__navigator:first-child');
  }

  /**
   * 点击下一页
   */
  clickNextpage(
    wait = () => {
      browser.sleep(100);
      CommonPage.waitProgressbarNotDisplay();
    },
  ) {
    this.nextbutton.click();
    wait();
  }

  /**
   * 点击前一页
   */
  clicforkpage(
    wait = () => {
      browser.sleep(100);
      CommonPage.waitProgressbarNotDisplay();
    },
  ) {
    this.frontbutton.click();
    wait();
  }

  /**
   * 获取快速导航按钮
   */
  get fastnavigator() {
    return $('.aui-paginator__fast-navigator');
  }
}
