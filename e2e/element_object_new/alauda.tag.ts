import { $$, ElementArrayFinder, browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaTag {
  private _alltag_selector;

  /**
   * 构造函数
   * @param _alltag_selector 定位所有tag 的selector
   */
  constructor(alltag_selector: ElementArrayFinder = $$('aui-tag')) {
    this._alltag_selector = alltag_selector;
  }

  /**
   * 获得 tag 元素
   * @param tagtext tag 上的显示文本，例如：主机，事件
   * @example getTagElementByText('主机').getText().then((text) => {console.log(text)})
   */
  getTagElementByText(tagtext) {
    const alltag = this._alltag_selector;

    return alltag
      .filter((elem, index) => {
        return elem.getText().then(function(text) {
          return text.trim() === tagtext;
        });
      })
      .first();
  }

  /**
   * 单击tag
   * @param tagtext tag 上的显示文本，例如：主机，事件
   * @example clickTagByText('主机')
   */
  clickTagByText(
    tagtext,
    wait = () => {
      browser.sleep(100);
      CommonPage.waitProgressbarNotDisplay();
    },
  ) {
    this.getTagElementByText(tagtext).click();
    wait();
  }

  /**
   * 获得 tag 的所有文字显示
   * @example getTagElementByText().getText().then((text) => {console.log(text)})
   */
  getTagText() {
    return this._alltag_selector.getText();
  }
}
