import { $, ElementFinder } from 'protractor';

import { AlaudaButton } from './alauda.button';
import { AlaudaElementBase } from './alauda.elementbase';

export class AlaudaRadioButton extends AlaudaElementBase {
  private _root: ElementFinder;
  private _contentSelector: string;

  constructor(
    root: ElementFinder = $('aui-radio-group'),
    contentSelector: string = '.aui-radio__content',
  ) {
    super();
    this._root = root;
    this._contentSelector = contentSelector;
  }

  /**
   * aui_radio_group 控件
   */
  get aui_radio_group(): ElementFinder {
    this.waitElementPresent(this._root).then(isPresent => {
      if (!isPresent) {
        console.log('alauda.radioButton 控件没有出现');
      }
    });
    return this._root;
  }

  /**
   * 单击按钮
   */
  clickByName(name: string) {
    const temp = this.aui_radio_group
      .$$(`${this._contentSelector}`)
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === name;
        });
      })
      .first();
    const button = new AlaudaButton(temp);
    return button.click();
  }
}
