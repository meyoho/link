import { $$, ElementArrayFinder, browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AlaudaElementBase } from './alauda.elementbase';

export class AlaudaRadioGroup extends AlaudaElementBase {
  private _allradio_selector;

  /**
   * 构造函数
   * @param _allradio_selector 定位所有radio 的selector
   */
  constructor(
    allradio_selector: ElementArrayFinder = $$(
      'aui-radio-group .aui-radio-button',
    ),
  ) {
    super();
    this._allradio_selector = allradio_selector;
  }

  /**
   * 获得 radio 元素
   * @param radiotext radio 上的显示文本，例如：主机，事件
   * @example getRadioElementByText('主机').getText().then((text) => {console.log(text)})
   */
  getRadioElementByText(radiotext) {
    const allradio = this._allradio_selector;

    return allradio
      .filter((elem, index) => {
        return elem.getText().then(function(text) {
          return text.trim() === radiotext;
        });
      })
      .first();
  }

  /**
   * 单击radio
   * @param radiotext radio 上的显示文本，例如：主机，事件
   * @example clickRadioByText('主机')
   */
  clickRadioByText(
    radiotext,
    wait = () => {
      browser.sleep(100);
      CommonPage.waitProgressbarNotDisplay();
    },
  ) {
    this.getRadioElementByText(radiotext).click();
    wait();
  }

  /**
   * 获得 radio 的所有文字显示
   * @example getRadioElementByText().getText().then((text) => {console.log(text)})
   */
  getRadioText() {
    return this._allradio_selector.getText();
  }
}
