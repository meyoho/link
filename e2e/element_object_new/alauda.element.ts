/**
 * Created by liuwei on 2018/8/27.
 */

import { $$, ElementFinder } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaElement {
  private _parent_css_selector: string;
  private _label_css_selector: string;

  /**
   * 根据左侧文字获得右侧元素
   * @param parent_selector  父节点的 css
   * @param label_css_selector 左侧label 的css
   */
  constructor(
    parent_selector: string = 'aui-form-field[class]',
    label_css_selector: string = 'label[class*=aui-form-field]',
  ) {
    this._parent_css_selector = parent_selector;
    this._label_css_selector = label_css_selector;
  }

  /**
   * 页面上根据左侧的文字，查找右侧控件
   * @param leftLabelText 左侧文字
   * @param targetSelector 右侧控件css 选择器
   */
  getElementByText(
    leftLabelText: string,
    targetSelector: string = 'input',
  ): ElementFinder {
    CommonPage.waitElementPresent($$(`${this._parent_css_selector}`).first());
    const content = $$(this._parent_css_selector)
      .filter(elem => {
        return elem
          .$(`${this._label_css_selector}`)
          .getText()
          .then(text => {
            return (
              text
                .replace('*\n', '')
                .replace('：', '')
                .trim() === leftLabelText
            );
          });
      })
      .first();

    const temp = content
      .$(targetSelector)
      .isPresent()
      .then(isPresent => {
        return isPresent;
      });

    if (temp) {
      return content.$(targetSelector);
    } else {
      return content;
    }
  }
}
