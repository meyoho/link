/**
 * 管理配置文件
 * Created by liuwei on 2018/3/2.
 */

const conf = {
  TESTDATAPATH: process.cwd() + '/e2e/test_data/',
  IMAGE: process.env.IMAGE || 'index.alauda.cn/alaudaorg/qaimages:helloworld',
  NAMESPACE: process.env.TestDataName,
};

export let ServerConf = conf;
