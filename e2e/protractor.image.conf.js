const { config } = require('./protractor.conf');

exports.config = Object.assign({}, config, {
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: [
        '--headless',
        '--disable-gpu',
        '--window-size=2480,1940',
        '--ignore-certificate-errors',
        '--no-sandbox',
        "--proxy-server='direct://'",
        '--proxy-bypass-list=*',
      ],
    },
    acceptInsecureCerts: true,
  },
});
