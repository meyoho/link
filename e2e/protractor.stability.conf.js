const { config } = require('./protractor.conf');
var reportParser = require('./utility/testReport.parser');
var retry = require('protractor-retry').retry;

exports.config = Object.assign({}, config, {
  capabilities: {
    browserName: 'chrome',
    // shardTestFiles: true,
    // maxInstances: 2,
    chromeOptions: {
      args: [
        '--headless',
        '--disable-gpu',
        '--window-size=2920,2440',
        '--ignore-certificate-errors',
        '--no-sandbox',
        "--proxy-server='direct://'",
        '--proxy-bypass-list=*',
      ],
    },
    acceptInsecureCerts: true,
  },
  afterLaunch() {
    reportParser.Parser.parse();
    try {
      Recycler.TestData.clear();
    } catch (err) {
      // console.warn(err);
    }
    return retry.afterLaunch(2);
  },
});
