// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

// JUnit style XML reports
const { JUnitXmlReporter } = require('jasmine-reporters');
var Recycler = require('./utility/clear.testdata');
var Prapare = require('./utility/prapare.testdata');
var retry = require('protractor-retry').retry;
var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: ['./**/*.e2e-spec.ts'],
  suites: {
    smoketest: './**/l0.*.e2e-spec.ts',
    sanitytest: ['./**/l0.*.e2e-spec.ts', './**/l1.*.e2e-spec.ts'],
    fulltest: [
      './**/l0.*.e2e-spec.ts',
      './**/l1.*.e2e-spec.ts',
      './**/l2.*.e2e-spec.ts',
      './**/l3.*.e2e-spec.ts',
      './**/*.e2e.ts',
    ],
    stability: [
      './**/l0.deployment-create.e2e-spec.ts',
      './**/l0.namespace-create.e2e-spec.ts',
      './**/l0.config-ui-create.e2e-spec.ts',
      './**/l0.cronjob-create.e2e-spec.ts',
      './**/l0.daemonset-create.e2e-spec.ts',
      './**/l0.replicasets-create.e2e-spec.ts',
      './**/l0.secret-create-ui.e2e-spec.ts',
      './**/l0.statefulset-create.e2e-spec.ts'],
    monitor: ['./**/acp.e2e-monitor.ts'],
  },
  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 2,
  },
  verboseMultiSessions: true,
  directConnect: true,
  baseUrl: process.env.BASE_URL || 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 160000,
    print: function() {},
  },

  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.json',
    });
    jasmine
      .getEnv()
      .addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    jasmine.getEnv().addReporter(
      new JUnitXmlReporter({
        savePath: './e2e-reports',
        consolidateAll: true,
        consolidate: true,
      }),
    );
    jasmine.getEnv().addReporter(
      new HtmlReporter({
        baseDirectory: './e2e-reports',
        screenshotsSubfolder: 'images',
        takeScreenShotsOnlyForFailedSpecs: true,
        docTitle: '[LINK] Dashboard UI Auto Test reporter',
      }).getJasmine2Reporter(),
    );

    retry.onPrepare();

    if (Recycler.TestData.isOpenOIDC()) {
      Recycler.TestData.login(process.env.OIDC_ENABLED);
    } else {
      browser.get(`${browser.baseUrl}/?locale=zh`);
      browser.sleep(1000);
    }

    browser.waitForAngularEnabled(false);
  },

  onCleanUp(results) {
    retry.onCleanUp(results);
  },

  beforeLaunch() {
    process.env.TestDataName = Prapare.TestData.createName();
    //清理资源
    try {
      Recycler.TestData.clear();
    } catch (err) {
      // console.warn(err);
    }
    console.log(
      '\n\n*********************UI Auto must set Env*******************************',
    );
    console.log(
      `*        BASE_URL: ${process.env.BASE_URL || 'http://localhost:4200/'}`,
    );
    console.log(
      `*           IMAGE: ${process.env.IMAGE ||
        'index.alauda.cn/alaudaorg/qaimages:helloworld'}`,
    );
    console.log(`*      USER_TOKEN: ${process.env.USER_TOKEN}`);
    console.log(`*      NAMESPACE: ${process.env.TestDataName}`);
    console.log(
      '************************************************************************\n\n',
    );
    Prapare.TestData.createResource(
      'namespace.yaml',
      {
        '${NAME}': process.env.TestDataName ,
        '${LABEL}': process.env.TestDataName,
      },
      'praparenamespace',
    );
  },
  afterLaunch() {
    Prapare.TestData.clearPrapareData(process.env.TestDataName);
    //清理资源
    try {
      Recycler.TestData.clear();
    } catch (err) {
      // console.warn(err);
    }
    return retry.afterLaunch(1);
  },
};
